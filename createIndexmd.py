import  os
import urllib
def walk(root):
    ret = [root]
    for a in os.listdir(root):
        path = os.path.join(root,a)
        # print(a)
        if os.path.isdir(path):
            # print(a)
            ret.extend(walk(path))
        else:
            continue
    return ret
root = "/Users/jialaizhu/dev/hugo/sandbox/source/doc"
dirs = walk(root)
print(dirs)
from urllib import parse

def escapePath(path):
    return parse.quote(path)
    # path = path.replace('\\', "%5C")
    # path = path.replace(" ", "%20")
    # path = path.replace(":", "%3A")
    # path = path.replace(",", "%2C")
    # path = path.replace('"', "%22")
    # path = path.replace('#', "%23")
    # return path
def createindex(folder):
    ignore = list(map(lambda x:x.upper(), ["readme.md","_sidebar.md","index.md"]))
    ret = []
    retdir = []
    for a in os.listdir(folder):
        if os.path.isdir(os.path.join(folder,a)):
            retdir.append("- [**[%s]**](%s/)"%(a,escapePath(a)))
        elif a.upper() in  ignore:
            continue
        else:
            try:
                aa = a.split('.')
                if aa[-1]=="md":
                    aa.pop()
                    name = ".".join(aa)
                    line = "- [%s](%s.html)"%(name,escapePath(name))
                    ret.append(line) 
            except Exception as e:
                pass
    ret.extend(retdir)
    txt = "\n".join(ret)
    file  = os.path.join(folder,"index.md")
    print(file)
    open(file,"w").write(txt)

for a in dirs:
    createindex(a)

# \[!\[@([\w\d]+)\]\(https://[\d|\w&=./-\?]+\)\]
