---
date: 2023-04-12 22:36
title: 深入浅出 eBPF 安全项目 Tracee
tags:
- ebpf
---

[深入浅出 eBPF](https://www.ebpf.top/)

专注于 Linux 内核技术eBPF (Linux/Kernel/XDP/BCC/BPFTrace/Cilium)

[首页](https://www.ebpf.top/) [eBPF 技术报告](https://www.ebpf.top/what-is-ebpf/ "eBPF 技术报告") [关于](https://www.ebpf.top/about/ "关于")

## 文章目录

-   [1. Tracee 介绍](https://www.ebpf.top/post/tracee_intro/#1-tracee-%E4%BB%8B%E7%BB%8D)
-   [2. Tracee 的工作原理](https://www.ebpf.top/post/tracee_intro/#2-tracee-%E7%9A%84%E5%B7%A5%E4%BD%9C%E5%8E%9F%E7%90%86)
-   [3. Tracee 功能测试](https://www.ebpf.top/post/tracee_intro/#3-tracee-%E5%8A%9F%E8%83%BD%E6%B5%8B%E8%AF%95)
-   [4. 源码编译 eBPF 程序](https://www.ebpf.top/post/tracee_intro/#4-%E6%BA%90%E7%A0%81%E7%BC%96%E8%AF%91-ebpf-%E7%A8%8B%E5%BA%8F)
-   [5. 参考](https://www.ebpf.top/post/tracee_intro/#5-%E5%8F%82%E8%80%83)

# 深入浅出 eBPF 安全项目 Tracee

2021年9月7日 

| [security](https://www.ebpf.top/categories/security) [BPF](https://www.ebpf.top/categories/BPF)

 

|1085 阅读

## 1. Tracee 介绍

### 1.1 Tracee 介绍

`Tracee` 是一个用 于 `Linux` 的运行时安全和取证工具。它使用 `Linux eBPF` 技术在运行时跟踪系统和应用程序，并分析收集的事件以检测可疑的行为模式。`Tracee` 以 `Docker` 镜像的形式交付，监控操作系统并根据预定义的行为模式集检测可疑行为。官网文档参见[这里](https://aquasecurity.github.io/tracee/dev/)。

`Tracee` 由以下子项目组成：

-   [Trace-eBPF](https://github.com/aquasecurity/tracee/blob/main/tracee-ebpf) - 使用 `eBPF` 进行 `Linux` 追踪和取证，BPF 底层代码实现参见 [tracee.bpf.c](https://github.com/aquasecurity/tracee/blob/main/tracee-ebpf/tracee/tracee.bpf.c)；
    
-   [Trace-Rules](https://github.com/aquasecurity/tracee/blob/main/tracee-rules) - 运行时安全检测引擎，在真实的使用场景中通过管道的方式从 `tracee-ebpf` 中接受数据，具体运行命令[如下](https://github.com/aquasecurity/tracee/blob/main/entrypoint.sh)：
    ```bash
    $TRACEE_EBPF_EXE --output=format:gob --security-alerts | $TRACEE_RULES_EXE --input-tracee=file:stdin --input-tracee=format:gob $@
    ```
    
-   [libbpfgo](https://github.com/aquasecurity/libbpfgo) 基于 `Linux libbpf` 的 `Go` 的 `eBPF` 库，`Tracee` 程序通过 `cgo` 访问 `libbpf C` 语言库；
    

`Tracee` 运行系统最低内核版本要求 >= 4.18，可以根据是否开启 `CO-RE` 进行 `BPF` 底层代码编译。运行 `Tracee` 需要足够的权限才能运行，测试可以直接使用 `root` 用户运行或者在 `Docker` 模型下使用 `--privileged` 模式运行。

`Tracee` 在最近的版本中增加了一个非常有意思的功能抓取 `--capture`，可以将读写文件、内存文件、网络数据包等进行抓取并保存，该功能主要是用于取证相关功能。

### 1.2 Tracee 与 Falco 的区别

看到 `Tracee` 这款基于 `eBPF` 技术的安全产品，很自然想到的对应产品是 `Falco`，如果你对 `Falco` 不了解，那么可以参见[这篇文章](https://www.ebpf.top/post/hello_falco/)。 `Tracee` 与 `Falco` 还是有诸多类似的功能，只是从实现和架构上看， `Tracee` 更加直接和简单，也没有特别复杂的规则引擎，作者给出的与 `Falco` 定位不同如下，更加详细的可参见[这里](https://github.com/aquasecurity/tracee/issues/48)：

> Falco 是一个规则引擎，基于 sysdig 的开放源代码。它从 sysdig 获取原始事件，并与 yaml 文件中 falco 语言定义的规则相匹配。相比之下，Tracee 从 eBPF 中追踪事件，但不执行基于这些事件的规则。
> 
> 我们编写 Tracee 时考虑到了以下几点：
> 
> -   Tracee 从一开始就被设计成一个基于 eBPF 的轻量级事件追踪器。
> -   Tracee 建立在 bcc 的基础上，并没有重写低级别的 BPF 接口。
> -   Tracee 被设计成易于扩展，例如，在 tracee 中添加对新的系统调用的支持就像添加两行代码一样简单，在这里你可以描述系统调用的名称和参数类型。
> -   其他事件也被支持，比如内部内核函数。我们现在已经支持 cap_capable，我们正在增加对 security_bprm_check lsm 钩子的支持。由于 lsm 安全钩子是安全的战略要点，我们计划在不久的将来增加更多这样的钩子。

其实，从使用的场景上来说 `Tracee` 与 `Falco` 不是非 A 即 B 的功能， `Tracee` 也可以与 `FalcoSideKick` 进行集成，作为一个事件输入源使用。

从下面两者架构图的对比，我们也可以略微熟悉一二， `Tracee` 更加直接和简洁，规则引擎的维护也不是重点，而且规则引擎恰恰是 `Falco` 的重点。

`Falco` 的架构图如下：

![](https://www.ebpf.top/post/tracee_intro/imgs/libs_to_cncf_arch.png)

而 `Tracee` 的架构图如下：

![](https://www.ebpf.top/post/tracee_intro/imgs/architecture.png)

## 2. Tracee 的工作原理

`Tracee` 中的 `tracee-ebpf` 模块的核心能力包括： 事件跟踪（`trace`）、抓取（`capture`）和输出（`output`）三个能力。

`tracee-ebpf` 的核心能力在于底层 `eBPF` 程序抓取事件的能力，`tracee-ebpf` 默认实现了诸多的事件抓取功能，可以通过 `trace -l` 参看到底层支持的函数全集（ 0.6.1 版本大概 390 个函数，格式如下：


```bash
$  sudo docker run --name tracee-only --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -v /boot/:/boot tracee -l
System Calls:          Sets:                                    Arguments:
____________           ____                                     _________

read                   [syscalls fs fs_read_write]              (int fd, void* buf, size_t count)
write                  [syscalls fs fs_read_write]              (int fd, void* buf, size_t count)
open                   [default syscalls fs fs_file_ops]        (const char* pathname, int flags, mode_t mode)
openat                 [default syscalls fs fs_file_ops]        (int dirfd, const char* pathname, int flags, mode_t mode)
...
```

-   第一列为系统调用函数名字；
-   第二列为该函数归属为的子类（注可归属多个，比如 `read` 函数，归属于 `syscalls/fs/fs_read_write` 3 个子类，除了 `fs` 外，`net` 集合中也包含了许多的跟踪函数）；
-   第三列为该函数的原型，可以使用参数中的字段进行过滤，支持特定的运算，比如 `==` `!=` 等常见的逻辑操作符，对于字符串也支持通配符操作；

这里简单介绍两个样例，更加详细的可以使用 `tracee --trace help` 命令查看。

-   `--trace s=fs --trace e!=open,openat` 跟踪 `fs` 集合中的所有事件，但是不包括 `open,openat` 两个函数；
-   `--trace openat.pathname!=/tmp/1,/bin/ls` 这里表示不跟踪 `openat` 事件中，`pathname` 为 `/tmp/1,/bin/ls` 的事件，注意这里的 `openat.pathname` 为跟踪函数名与函数参数的组合；

以上跟踪事件的过滤条件通过接口设置进内核中对应的 `map` 结构中，在完成过滤和事件跟踪以后，通过 `perf_event` 的方式上报到用户空间程序中，可以保存到文件后续进行处理，或者直接通过管道发送至 `tracee-rule` 进行解析和进行更高级别的上报，详细参见上一章节的架构图。

## 3. Tracee 功能测试

### 3.1 功能测试

测试前需要保证内核版本及相关条件满足最小要求：

-   内核 >= 4.18，可选项启用了 `BTF`，`BTF` 启用可以通过 `/boot/config*` 文件检查 `CONFIG_DEBUG_INFO_BTF` 是否启用；(`grep CONFIG_DEBUG_INFO_BTF /boot/config-xx-yyy`)
-   `Linux` 内核头文件已经安装，Ubuntu/Debian/Arch/Manjaro`中为`linux-headers `包，`CentOS/Fedora`中为`kernel-headers`和`kernel-devel` 两个包；

如果内核启用了 BTF 功能，可以直接使用官方提供的镜像进行测试：


```bash
$ sudo docker run --name tracee --rm --privileged -it aquasec/tracee:latest trace
```

如果系统未启用 `BTF` 功能，则需要加载内核 `/lib/modules/` 和 `/usr/src` 目录，并运行以下命令：


```bash
$ sudo docker run --name tracee --rm --privileged --pid=host -v  /boot:/boot:ro -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -e TINI_SUBREAPER=true aquasec/tracee
Loaded signature(s):  [TRC-1 TRC-2 TRC-3 TRC-4 TRC-5 TRC-6 TRC-7]
```

挂载 `/boot` 目录方便读取 `/boot/config*` 等相关文件，`-e TINI_SUBREAPER=true` 是为了让 tini 作为父进程进行子进程回收的能力。

在运行以后我们可以发现最后有一系列签名输出 `TRC-1 TRC-2 TRC-3 TRC-4 TRC-5 TRC-6 TRC-7`，这些签名代表了对应检测的选项。我们可以使用 `--list` 选项进行查看，结果如下：


```bash
$ sudo docker run --name tracee --rm --privileged --pid=host -v  /boot:/boot:ro -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -e TINI_SUBREAPER=true aquasec/tracee --list
Loaded signature(s):  [TRC-1 TRC-2 TRC-3 TRC-4 TRC-5 TRC-6 TRC-7]
ID         NAME                                VERSION DESCRIPTION
TRC-1      Standard Input/Output Over Socket   0.1.0   Redirection of process's standard input/output to socket
TRC-2      Anti-Debugging                      0.1.0   Process uses anti-debugging technique to block debugger
TRC-3      Code injection                      0.1.0   Possible code injection into another process
TRC-4      Dynamic Code Loading                0.1.0   Writing to executable allocated memory region
TRC-5      Fileless Execution                  0.1.0   Executing a process from memory, without a file in the disk
TRC-6      kernel module loading               0.1.0   Attempt to load a kernel module detection
TRC-7      LD_PRELOAD                          0.1.0   Usage of LD_PRELOAD to allow hooks on process
```

使用 `elfexec` 进行测试：


```bash
# From https://github.com/abbat/elfexec/releases 
$ wget https://github.com/abbat/elfexec/releases/download/v0.3/elfexec.x64.glibc.xz
$ chmod u+x elfexec.x64.glib && mv ./elfexec.x64.glibc ./elfexec 
$ echo 'IyEvYmluL3NoCmVjaG8gIkhlbGxvISIK' | base64 -d|./elfexec
Hello!
$ echo 'IyEvYmluL3NoCmVjaG8gIkhlbGxvISIK' | base64 -d
#!/bin/sh
echo "Hello!"
```

上述命令就是将一个输出 `echo hello` 的脚本重定向到 `elfexec` 进行执行， 在上述命令运行后，输出以下信息：


```bash
$ sudo docker run --name tracee --rm --privileged --pid=host -v  /boot:/boot:ro -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -e TINI_SUBREAPER=true aquasec/tracee
Loaded signature(s):  [TRC-1 TRC-2 TRC-3 TRC-4 TRC-5 TRC-6 TRC-7]

*** Detection ***  
Time: 2021-09-10T09:10:25Z
Signature ID: TRC-5
Signature: Fileless Execution
Data: map[]
Command: elfexec
Hostname: VM-0-14-ubuntu
```

这里我们看到测试触发的签名为 `TRC-5`， 详细情况为 `”Fileless Execution“`，命令为 `”elfexec“`。

## 4. 源码编译 eBPF 程序

### 4.1 镜像方式编译

`Tracee` 支持我们自己基于系统编译 `eBPF` 程序，然后将编译后的 `eBPF` 字节码传递至 `Docker` 镜像进行运行。推荐 `eBPF` 程序编译通过 `Docker` 镜像进行，如果使用本机环境编译需要安装并保证 `GNU Make` >= 4.3 - `clang` >= 11。

推荐编译和运行基于 `Ubuntu` 系列的系统（`Ubuntu 20.04`），猜测 `Tracee` 的主要测试环境应该是在 `Ubuntu` 系列中，`CentOS` 系列测试偏少。

> 推荐 Ubuntu 20.04 版本，在 CentOS 5.4 内核中编译遇到不少问题，主要是环境差异导致，包括内核编译目录软连接，dockerfile 等问题，参见我提交的 [pr](https://github.com/aquasecurity/tracee/pull/1000)


```bash
$ git clone --recursive https://github.com/aquasecurity/tracee.git
$ make bpf DOCKER=1  # --just-print 只是打印，编译完成后可以在 dist 目录中看到编译好的字节码程序
$ ls -hl dist/
total 7.8M
-rw-r--r-- 1 root root 3.1M Sep 10 17:22 tracee.bpf.5_4_132-1_el7_elrepo_x86_64.v0_6_1-1-gce65764.o
-rw-r--r-- 1 root root 4.7M Sep 10 17:22 tracee.bpf.core.o

# 可以通过 TRACEE_BPF_FILE 环境变量指定我们需要加载的 eBPF 程序，这里使用目录 /tmp/tracee

$ sudo docker run --name tracee --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -e TRACEE_BPF_FILE=/tmp/tracee/tracee.bpf.core.o aquasec/tracee
```

> 如果编译 tracee-ebpf 我们也会发现，底层还是会依赖动态库，只是因为 tracee-ebpf 底层使用 cgo 机制使用 libbpf 库依赖的结果。
> 
```bash
$ file tracee-ebpf/dist/tracee-ebpf| tr , '\n'
tracee-ebpf/dist/tracee-ebpf: ELF 64-bit LSB executable
x86-64
version 1 (SYSV)
dynamically linked (uses shared libs)
for GNU/Linux 3.2.0
BuildID[sha1]=5bd7dbfd0475f015e268e321476dfc928d06d950
not stripped

$ # ldd tracee-ebpf/dist/tracee-ebpf
tracee-ebpf/dist/tracee-ebpf: /lib64/libc.so.6: version `GLIBC_2.22' not found (required by tracee-ebpf/dist/tracee-ebpf)
linux-vdso.so.1 =>  (0x00007ffe559d7000)
libelf.so.1 => /lib64/libelf.so.1 (0x00007f5d0c884000)
libz.so.1 => /lib64/libz.so.1 (0x00007f5d0c66e000)
libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f5d0c452000)
libc.so.6 => /lib64/libc.so.6 (0x00007f5d0c084000)
/lib64/ld-linux-x86-64.so.2 (0x00007f5d0ca9c000)

```

单独验证 `tracee-ebpf` 可以使用以下命令：


```bash
$ sudo docker run --name tracee-only --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -v /boot/:/boot tracee  --trace event=execve --output table-verbose --debug|more
```

### 4.2 编译错误处理

#### 4.2.1 “bind”: invalid mount path: ‘./’ mount path must be absolute

`tracee-ebpf` 使用 `Docker-builder` 路径加载报错如下：


```bash
Step 3/3 : WORKDIR /tracee
 ---> Using cache
 ---> 5e8a792b8117
Successfully built 5e8a792b8117
Successfully tagged tracee-builder:latest
docker run --rm -v /root/tracee/tracee-ebpf:./ -v /root/tracee/tracee-ebpf:/tracee/tracee-ebpf -w /tracee/tracee-ebpf --entrypoint make tracee-builder KERN_BLD_PATH=/usr/src/kernels/5.4.132-1.el7.elrepo.x86_64 KERN_SRC_PATH=build dist/tracee-ebpf VERSION=v0.6.1-1-gce65764
docker: Error response from daemon: invalid volume specification: '/root/tracee/tracee-ebpf:./': invalid mount config for type "bind": invalid mount path: './' mount path must be absolute.
See 'docker run --help'.
make: *** [dist/tracee-ebpf] Error 125
```

修改方式


```bash
+DOCKER_BUILDER_KERN_BLD ?= $(if $(shell readlink -f $(KERN_BLD_PATH)),$(shell readlink -f $(KERN_BLD_PATH)),$(KERN_BLD_PATH))
+DOCKER_BUILDER_KERN_SRC ?= $(if $(shell readlink -f $(KERN_SRC_PATH)),$(shell readlink -f $(KERN_SRC_PATH)),$(KERN_SRC_PATH))
```

主要差异点为系统不同，软链接的方式不同导致

CentOS


```bash
$ ls -hl /lib/modules/5.4.132-1.el7.elrepo.x86_64/source
lrwxrwxrwx 1 root root 5 Jul 22 15:12 /lib/modules/5.4.132-1.el7.elrepo.x86_64/source -> build
```

Ubuntu


```bash
$ ls -hl /lib/modules/5.4.0-42-generic/build
lrwxrwxrwx 1 root root 39 Jul 10  2020 /lib/modules/5.4.0-42-generic/build -> /usr/src/linux-headers-5.4.0-42-generic
```

#### 4.2.2 单独生成 tracee-ebpf 镜像时，make: uname: Operation not permitted 等问题


```bash
$ make docker
docker build --build-arg VERSION=v0.6.1-1-gce65764 -t tracee:latest .
Sending build context to Docker daemon  2.116GB
Step 1/16 : ARG BASE=fat
Step 2/16 : FROM golang:1.16-alpine as builder
ARG BASE=fat

FROM golang:1.16-alpine as builder
RUN apk --no-cache update && apk --no-cache add git clang llvm make gcc libc6-compat coreutils linux-headers musl-dev elfutils-dev libelf-static zlib-static
WORKDIR /tracee

// ...

make: uname: Operation not permitted
make: find: Operation not permitted
make: uname: Operation not permitted
make: /bin/sh: Operation not permitted
mkdir -p dist
make: mkdir: Operation not permitted
make: *** [Makefile:51: dist] Error 127
The command '/bin/sh -c make build VERSION=$VERSION' returned a non-zero code: 2
make: *** [docker] Error 2
```

该问题是 `builder` 基础镜像 `golang:1.16-alpine` 版本升级版本（alpine3.14`以后）导致的，明确指定为`golang:1.16-alpine3.13` 即可：


```bash
-FROM golang:1.16-alpine as builder
+FROM golang:1.16-alpine3.13 as builder
```

#### 4.2.3 failed to add kprobe ‘p:kprobes/psecurity_file_open security_file_open’: -17

异常退出后，再次运行可能会导致 `ailed to create kprobe event: -17` 的错误，错误的原因是使用了传统的 `kprobe` 方式，写入到 `/sys/kernel/debug/tracing/kprobe_events` 中，但是退出的时候未能够正常清理。


```bash
# docker run --name tracee --rm --privileged -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro -v /tmp/tracee:/tmp/tracee -it aquasec/tracee:latest
2021/09/07 02:46:41 [INFO]  : Enabled Outputs :
2021/09/07 02:46:41 [INFO]  : Falco Sidekick is up and listening on port 2801
failed to add kprobe 'p:kprobes/psecurity_file_open security_file_open': -17
failed to create kprobe event: -17
```

如果出现错误可以通过 `/sys/kernel/debug/tracing/kprobe_events` 文件进行查看：


```bash
$ cat /sys/kernel/debug/tracing/kprobe_events
p:kprobes/psecurity_mmap_addr security_mmap_addr
p:kprobes/psecurity_file_mprotect security_file_mprotect
p:kprobes/psecurity_bprm_check security_bprm_check
p:kprobes/pcap_capable cap_capable
p:kprobes/psecurity_inode_unlink security_inode_unlink
p:kprobes/psecurity_file_open security_file_open
```

修复 `sudo bash -c "echo""> /sys/kernel/debug/tracing/kprobe_events"`，参见 [issue 447](https://github.com/aquasecurity/tracee/issues/447) 和 [639](https://github.com/aquasecurity/tracee/issues/639)。

#### 4.2.4 ‘err’ may be used uninitialized in this function

编译 `libbpf` 的时候可能报错，需要修改 `Makefile` 文件中的 `CFLAGS ?= -g -O2 -Werror -Wall`，删除 `-Werror` 即可。


```bash
btf_dump.c: In function ‘btf_dump_dump_type_data.isra.24’:
btf_dump.c:2266:5: error: ‘err’ may be used uninitialized in this function [-Werror=maybe-uninitialized]
  if (err < 0)
     ^
cc1: all warnings being treated as errors
```

#### 4.2.5 golang 拉取包超时


```bash
// ... 
GOOS=linux GOARCH=amd64 CC=clang CGO_CFLAGS="-I /tracee/tracee-ebpf/dist/libbpf/usr/include" CGO_LDFLAGS="/tracee/tracee-ebpf/dist/libbpf/libbpf.a" go build -tags netgo -v -o dist/tracee-ebpf \
-ldflags "-w -extldflags \"\"-X main.version=v0.6.1-1-gce65764"
go: github.com/aquasecurity/libbpfgo@v0.2.1-libbpf-0.4.0: Get "https://proxy.golang.org/github.com/aquasecurity/libbpfgo/@v/v0.2.1-libbpf-0.4.0.mod": dial tcp 142.251.42.241:443: i/o timeout
make: *** [Makefile:59: dist/tracee-ebpf] Error 1
make: *** [dist/tracee-ebpf] Error 2
```

添加代理执行 `GOPROXY=https://goproxy.cn` 即可。


```bash
$ docker run --rm -v /usr/src/kernels:/usr/src/kernels/ -v /root/tracee/tracee-ebpf:/tracee/tracee-ebpf -w /tracee/tracee-ebpf --entrypoint make tracee-builder DOCKER_BUILDER_KERN_SRC=/usr/src/kernels/5.4.132-1.el7.elrepo.x86_64 KERN_SRC_PATH=/lib/modules/5.4.132-1.el7.elrepo.x86_64/source KERN_BLD_PATH=/usr/src/kernels/5.4.132-1.el7.elrepo.x86_64 KERN_SRC_PATH=/usr/src/kernels/5.4.132-1.el7.elrepo.x86_64 dist/tracee-ebpf VERSION=v0.6.1-1-gce65764  GOPROXY=https://goproxy.cn
```

详细修改参见这里：


```bash
-go_env := GOOS=linux GOARCH=$(ARCH:x86_64=amd64) CC=$(CMD_CLANG) CGO_CFLAGS="-I $(abspath $(LIBBPF_HEADERS))" CGO_LDFLAGS="$(abspath $(LIBBPF_OBJ))"
+go_env := GOOS=linux GOARCH=$(ARCH:x86_64=amd64) CC=$(CMD_CLANG) CGO_CFLAGS="-I $(abspath $(LIBBPF_HEADERS))" CGO_LDFLAGS="$(abspath $(LIBBPF_OBJ))" GOPROXY=https://goproxy.cn

-	--entrypoint make $(DOCKER_BUILDER) KERN_BLD_PATH=$(DOCKER_BUILDER_KERN_BLD) KERN_SRC_PATH=$(DOCKER_BUILDER_KERN_SRC) $(1)
+	--entrypoint make $(DOCKER_BUILDER) DOCKER_BUILDER_KERN_SRC=$(DOCKER_BUILDER_KERN_SRC) KERN_SRC_PATH=$(KERN_SRC_PATH) KERN_BLD_PATH=$(DOCKER_BUILDER_KERN_BLD) GOPROXY=https://goproxy.cn KERN_SRC_PATH=$(DOCKER_BUILDER_KERN_SRC) $(1)
```

## 5. 参考

1.  [Tracee: Tracing Containers with eBPF](https://blog.aquasec.com/ebpf-tracing-containers)
2.  [Tracee：如何使用 eBPF 来追踪容器和系统事件](https://cloud.tencent.com/developer/article/1770833)

-   **原文作者：**[DavidDi](https://www.ebpf.top/)
-   **原文链接：**[https://www.ebpf.top/post/tracee_intro/](https://www.ebpf.top/post/tracee_intro/)
-   **版权声明：**本作品采用[知识共享署名-非商业性使用-禁止演绎 4.0 国际许可协议](https://creativecommons.org/licenses/by-nc-nd/4.0/)进行许可，非商业转载请注明出处（作者，原文链接），商业转载请联系作者获得授权。

  

