---
title: "Linux namespaces"
layout: post
---
页码：1/1

https://en.wikipedia.org/wiki/Linux_namespaces

# Linux namespaces

For namespaces in general, see [Namespace](/wiki/Namespace).
<caption>namespaces</caption>|---|
|[Original author(s)](/wiki/Programmer)|Al Viro|
|[Developer(s)](/wiki/Programmer)|Eric W. Biederman, Pavel Emelyanov, Al Viro, Cyrill Gorcunov et al.|
|Initial release|2002; 20years ago|
|Written in|[C](/wiki/C_(programming_language))|
|[Operating system](/wiki/Operating_system)|[Linux](/wiki/Linux)|
|[Type](/wiki/Software_categories#Categorization_approaches)|[System software](/wiki/System_software)|
|[License](/wiki/Software_license)|[GPL](/wiki/GNU_General_Public_License) and [LGPL](/wiki/GNU_Lesser_General_Public_License)|

**Namespaces** are a feature of the [Linux kernel](/wiki/Linux_kernel) that partitions kernel resources such that one set of [processes](/wiki/Process_(computing)) sees one set of resources while another set of processes sees a different set of resources. The feature works by having the same namespace for a set of resources and processes, but those namespaces refer to distinct resources. Resources may exist in multiple spaces. Examples of such resources are process IDs, hostnames, user IDs, file names, and some names associated with network access, and [interprocess communication](/wiki/Interprocess_communication).

Namespaces are a fundamental aspect of [containers](/wiki/Linux_containers) on Linux.

The term "namespace" is often used for a type of namespace (e.g. process ID) as well as for a particular space of names.

A Linux system starts out with a single namespace of each type, used by all processes. Processes can create additional namespaces and join different namespaces.

## Contents

* 1History
* 2Namespace kinds

  * 2.1Mount (mnt)
  * 2.2Process ID (pid)
  * 2.3Network (net)
  * 2.4Interprocess Communication (ipc)
  * 2.5UTS
  * 2.6User ID (user)
  * 2.7Control group (cgroup) Namespace
  * 2.8Time Namespace
  * 2.9Proposed namespaces

    * 2.9.1syslog namespace
* 3Implementation details

  * 3.1Syscalls
  * 3.2Destruction
* 4Adoption
* 5References
* 6External links

## History

|                                                              |                                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![[icon]](//upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Wiki_letter_w_cropped.svg/20px-Wiki_letter_w_cropped.svg.png)](/wiki/File:Wiki_letter_w_cropped.svg) | This section **needs expansion**. You can help by [adding to it](https://en.wikipedia.org/w/index.php?title=Linux_namespaces&action=edit&section=). *(September 2016)* |

Linux namespaces were inspired by the wider namespace functionality used heavily throughout [Plan 9 from Bell Labs](/wiki/Plan_9_from_Bell_Labs).<sup>\[1\]</sup>

The Linux Namespaces originated in 2002 in the 2.4.19 kernel with work on the mount namespace kind. Additional namespaces were added beginning in 2006<sup>\[2\]</sup> and continuing into the future.

Adequate [containers](/wiki/Container_(virtualization)) support functionality was finished in kernel version 3.8 with the introduction of User namespaces.<sup>\[3\]</sup>

## Namespace kinds

Since [kernel](/wiki/Kernel_(operating_system)) version 5.6, there are 8 kinds of namespaces. Namespace functionality is the same across all kinds: each process is associated with a namespace and can only see or use the resources associated with that namespace, and descendant namespaces where applicable. This way each process (or process group thereof) can have a unique view on the resources. Which resource is isolated depends on the kind of namespace that has been created for a given process group.

### Mount (mnt)

Mount namespaces control mount points. Upon creation the mounts from the current mount namespace are copied to the new namespace, but mount points created afterwards do not propagate between namespaces (using shared subtrees, it is possible to propagate mount points between namespaces<sup>\[4\]</sup>).

The clone flag used to create a new namespace of this type is CLONE\_NEWNS - short for "NEW NameSpace". This term is not descriptive (it does not tell which kind of namespace is to be created) because mount namespaces were the first kind of namespace and designers did not anticipate there being any others.

### Process ID (pid)

The [PID](/wiki/Process_identifier) namespace provides processes with an independent set of process IDs (PIDs) from other namespaces. PID namespaces are nested, meaning when a new process is created it will have a PID for each namespace from its current namespace up to the initial PID namespace. Hence the initial PID namespace is able to see all processes, albeit with different PIDs than other namespaces will see processes with.

The first process created in a PID namespace is assigned the process id number 1 and receives most of the same special treatment as the normal [init](/wiki/Init) process, most notably that [orphaned processes](/wiki/Orphan_process) within the namespace are attached to it. This also means that the termination of this PID 1 process will immediately terminate all processes in its PID namespace and any descendants.<sup>\[5\]</sup>

### Network (net)

Network namespaces virtualize the [network stack](/wiki/Network_stack). On creation a network namespace contains only a [loopback](/wiki/Localhost) interface.

Each network interface (physical or virtual) is present in exactly 1 namespace and can be moved between namespaces.

Each namespace will have a private set of [IP addresses](/wiki/IP_address), its own [routing table](/wiki/Routing_table), [socket](/wiki/Network_socket) listing, connection tracking table, [firewall](/wiki/Firewall_(computing)), and other network-related resources.

Destroying a network namespace destroys any virtual interfaces within it and moves any physical interfaces within it back to the initial network namespace.

### Interprocess Communication (ipc)

IPC namespaces isolate processes from [SysV](/wiki/UNIX_System_V) style inter-process communication. This prevents processes in different IPC namespaces from using, for example, the SHM family of functions to establish a range of shared memory between the two processes. Instead each process will be able to use the same identifiers for a shared memory region and produce two such distinct regions.

### UTS

UTS (UNIX [Time-Sharing](/wiki/Time-sharing)) namespaces allow a single system to appear to have different [host](/wiki/Hostname) and [domain names](/wiki/Domain_name) to different processes. "When a process creates a new UTS namespace ... the hostname and domain of the new UTS namespace are copied from the corresponding values in the caller's UTS namespace."<sup>\[6\]</sup>

### User ID (user)

User namespaces are a feature to provide both privilege isolation and user identification segregation across multiple sets of processes available since kernel 3.8.<sup>\[7\]</sup> With administrative assistance it is possible to build a container with seeming administrative rights without actually giving elevated privileges to user processes. Like the PID namespace, user namespaces are nested and each new user namespace is considered to be a child of the user namespace that created it.

A user namespace contains a mapping table converting user IDs from the container's point of view to the system's point of view. This allows, for example, the [root](/wiki/Superuser) user to have user id 0 in the container but is actually treated as user id 1,400,000 by the system for ownership checks. A similar table is used for group id mappings and ownership checks.

To facilitate privilege isolation of administrative actions, each namespace type is considered owned by a user namespace based on the active user namespace at the moment of creation. A user with administrative privileges in the appropriate user namespace will be allowed to perform administrative actions within that other namespace type. For example, if a process has administrative permission to change the IP address of a network interface, it may do so as long as its own user namespace is the same as (or ancestor of) the user namespace that owns the network namespace. Hence the initial user namespace has administrative control over all namespace types in the system.<sup>\[8\]</sup>

### Control group (cgroup) Namespace

The [cgroup](/wiki/Cgroup) namespace type hides the identity of the [control group](/wiki/Cgroups) of which process is a member. A process in such a namespace, checking which control group any process is part of, would see a path that is actually relative to the control group set at creation time, hiding its true control group position and identity. This namespace type has existed since March 2016 in Linux 4.6.<sup>\[9\]</sup><sup>\[10\]</sup>

### Time Namespace

The time namespace allows processes to see different system times in a way similar to the UTS namespace. It was proposed in 2018 and landed on Linux 5.6, which was released in March 2020.<sup>\[11\]</sup>

### Proposed namespaces

#### syslog namespace

The syslog namespace was proposed by Rui Xiang, an engineer at [Huawei](/wiki/Huawei), but wasn't merged into the linux kernel.<sup>\[12\]</sup> [systemd](/wiki/Systemd) implemented a similar feature called “journal namespace” in February 2020.<sup>\[13\]</sup>

## Implementation details

The kernel assigns each process a symbolic link per namespace kind in `/proc/<pid>/ns/`. The inode number pointed to by this symlink is the same for each process in this namespace. This uniquely identifies each namespace by the inode number pointed to by one of its symlinks.

Reading the symlink via readlink returns a string containing the namespace kind name and the inode number of the namespace.

### Syscalls

Three syscalls can directly manipulate namespaces:

* clone, flags to specify which new namespace the new process should be migrated to.
* unshare, allows a process (or thread) to disassociate parts of its execution context that are currently being shared with other processes (or threads)
* setns, enters the namespace specified by a file descriptor.

### Destruction

If a namespace is no longer referenced, it will be deleted, the handling of the contained resource depends on the namespace kind. Namespaces can be referenced in three ways:

1. by a process belonging to the namespace
2. by an open filedescriptor to the namespace's file (`/proc/<pid>/ns/<ns-kind>`)
3. a bind mount of the namespace's file (`/proc/<pid>/ns/<ns-kind>`)

## Adoption

Various container software use Linux namespaces in combination with [cgroups](/wiki/Cgroups) to isolate their processes, including [Docker](/wiki/Docker_(software))<sup>\[14\]</sup> and [LXC](/wiki/LXC).

Other applications, such as [Google Chrome](/wiki/Google_Chrome) make use of namespaces to isolate its own processes which are at risk from attack on the internet.<sup>\[15\]</sup>

There is also an unshare wrapper in [util-linux](/wiki/Util-linux). An example to its use is:

```
SHELL=/bin/sh unshare --map-root-user --fork --pid chroot "${chrootdir}" "$@"
```

## References

1. <cite>["The Use of Name Spaces in Plan 9"](https://web.archive.org/web/20140906153815/http://www.cs.bell-labs.com/sys/doc/names.html). 1992. Archived from [the original](http://www.cs.bell-labs.com/sys/doc/names.html) on 2014-09-06. Retrieved 2016-03-24.</cite>none
2. <cite>["Linux kernel source tree"](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=071df104f808b8195c40643dcb4d060681742e29). kernel.org. 2016-10-02.</cite>none
3. <cite>["Namespaces in operation, part 5: User namespaces \[LWN.net\]"](https://lwn.net/Articles/532593/).</cite>none
4. <cite>["Documentation/filesystems/sharedsubtree.txt"](https://www.kernel.org/doc/Documentation/filesystems/sharedsubtree.txt). 2016-02-25. Retrieved 2017-03-06.</cite>none
5. <cite>["Namespaces in operation, part 3: PID namespaces"](https://lwn.net/Articles/531419/). lwn.net. 2013-01-16.</cite>none
6. <cite>["uts\_namespaces(7) - Linux manual page"](https://www.man7.org/linux/man-pages/man7/uts_namespaces.7.html). *www.man7.org*. Retrieved 2021-02-16.</cite>none
7. <cite>["Namespaces in operation, part 5: User namespaces \[LWN.net\]"](https://lwn.net/Articles/532593/).</cite>none
8. <cite>["Namespaces in operation, part 5: User namespaces"](https://lwn.net/Articles/532593/). lwn.net. 2013-02-27.</cite>none
9. <cite>Heo, Tejun (2016-03-18). ["\[GIT PULL\] cgroup namespace support for v4.6-rc1"](https://lkml.org/lkml/2016/3/18/564). *lkml* (Mailing list).</cite>none
10. <cite>[Torvalds, Linus](/wiki/Linus_Torvalds) (2016-03-26). ["Linux 4.6-rc1"](https://lkml.org/lkml/2016/3/26/132). *lkml* (Mailing list).</cite>none
11. <cite>["It's Finally Time: The Time Namespace Support Has Been Added To The Linux 5.6 Kernel - Phoronix"](https://www.phoronix.com/scan.php?page=news_item&px=Time-Namespace-In-Linux-5.6). *www.phoronix.com*. Retrieved 2020-03-30.</cite>none
12. <cite>["Add namespace support for syslog \[LWN.net\]"](https://lwn.net/Articles/562389/). *lwn.net*. Retrieved 2022-07-11.</cite>none
13. <cite>["journal: add concept of "journal namespaces" by poettering · Pull Request #14178 · systemd/systemd"](https://github.com/systemd/systemd/pull/14178). *GitHub*. Retrieved 2022-07-11.</cite>none
14. <cite>["Docker security"](https://docs.docker.com/engine/security/security/). docker.com. Retrieved 2016-03-24.</cite>none
15. <cite>["Chromium Linux Sandboxing"](https://chromium.googlesource.com/chromium/src.git/+/master/docs/linux_sandboxing.md). Retrieved 2019-12-19.</cite>none

## External links

* [namespaces manpage](http://man7.org/linux/man-pages/man7/namespaces.7.html)
* [Namespaces — The Linux Kernel documentation](https://www.kernel.org/doc/html/latest/admin-guide/namespaces/index.html)
* [Linux kernel Namespaces and cgroups by Rami Rosen](http://www.haifux.org/lectures/299/netLec7.pdf)
* [Namespaces and cgroups, the basis of Linux containers (including cgroups v2) - slides of a talk by Rami Rosen, Netdev 1.1, Seville, Spain (2016)](http://www.netdevconf.org/1.1/proceedings/slides/rosen-namespaces-cgroups-lxc.pdf)
* [Containers and Namespaces in the Linux Kernel by Kir Kolyshkin](https://events.static.linuxfound.org/slides/lfcs2010_kolyshkin.pdf)
* [https://medium.com/@teddyking/linux-namespaces-850489d3ccf](https://medium.com/@teddyking/linux-namespaces-850489d3ccf)
* [https://medium.com/@teddyking/namespaces-in-go-basics-e3f0fc1ff69a](https://medium.com/@teddyking/namespaces-in-go-basics-e3f0fc1ff69a)
* [https://medium.com/@teddyking/namespaces-in-go-user-a54ef9476f2a](https://medium.com/@teddyking/namespaces-in-go-user-a54ef9476f2a)
* [https://medium.com/@teddyking/namespaces-in-go-reexec-3d1295b91af8](https://medium.com/@teddyking/namespaces-in-go-reexec-3d1295b91af8)
* [https://medium.com/@teddyking/namespaces-in-go-mount-e4c04fe9fb29](https://medium.com/@teddyking/namespaces-in-go-mount-e4c04fe9fb29)
* [https://medium.com/@teddyking/namespaces-in-go-network-fdcf63e76100](https://medium.com/@teddyking/namespaces-in-go-network-fdcf63e76100)
* [https://medium.com/@teddyking/namespaces-in-go-uts-d47aebcdf00e](https://medium.com/@teddyking/namespaces-in-go-uts-d47aebcdf00e)

|                                                            |
| ---------------------------------------------------------- |
| * [<abbr>v</abbr>](/wiki/Template:Virtualization_software) |
* [<abbr>t</abbr>](/wiki/Template_talk:Virtualization_software)
* [<abbr>e</abbr>](https://en.wikipedia.org/w/index.php?title=Template:Virtualization_software&action=edit)[Virtualization](/wiki/Virtualization) software|
|[Comparison of platform virtualization software](/wiki/Comparison_of_platform_virtualization_software)|
|[Hardware](/wiki/Hardware_virtualization)  
([hypervisors](/wiki/Hypervisor))|||
|---|
|Native|* [Adeos](/wiki/Adaptive_Domain_Environment_for_Operating_Systems)
* [CP/CMS](/wiki/CP/CMS)
* [Hyper-V](/wiki/Hyper-V)
* [KVM](/wiki/Kernel-based_Virtual_Machine)

  * [oVirt](/wiki/OVirt)
  * [Red Hat Virtualization](/wiki/Red_Hat_Virtualization)
* [LDoms / Oracle VM Server for SPARC](/wiki/Oracle_VM_Server_for_SPARC)
* [Logical partition](/wiki/Logical_partition) (LPAR)
* [LynxSecure](/wiki/LynxSecure)
* [PikeOS](/wiki/PikeOS)
* [Proxmox VE](/wiki/Proxmox_Virtual_Environment)
* [QNX](/wiki/QNX)
* [SIMMON](/wiki/SIMMON)
* [VMware ESXi](/wiki/VMware_ESXi)

  * [VMware vSphere](/wiki/VMware_vSphere)
  * [vCloud](/wiki/VCloud_Air)
* [VMware Infrastructure](/wiki/VMware_Infrastructure)
* [Xen](/wiki/Xen)

  * [Oracle VM Server for x86](/wiki/Oracle_VM_Server_for_x86)
  * [XenServer](/wiki/XenServer)
* [XtratuM](/wiki/XtratuM)
* [z/VM](/wiki/Z/VM)|
|Hosted|||
|---|
|Specialized|* [Basilisk II](/wiki/Basilisk_II)
* [Bochs](/wiki/Bochs)
* [Cooperative Linux](/wiki/Cooperative_Linux)
* [DOSBox](/wiki/DOSBox)
* [DOSEMU](/wiki/DOSEMU)
* [PCem](/wiki/PCem)
* [PikeOS](/wiki/PikeOS)
* [SheepShaver](/wiki/SheepShaver)
* [SIMH](/wiki/SIMH)
* [Windows on Windows](/wiki/Windows_on_Windows)

  * [Virtual DOS machine](/wiki/Virtual_DOS_machine)
* [Win4Lin](/wiki/Win4Lin)|
|Independent|* [bhyve](/wiki/Bhyve)
* [Microsoft Virtual Server](/wiki/Microsoft_Virtual_Server)
* [Parallels Workstation](/wiki/Parallels_Workstation) ([Extreme](/wiki/Parallels_Workstation_Extreme))
* [Parallels Desktop for Mac](/wiki/Parallels_Desktop_for_Mac)
* [Parallels Server for Mac](/wiki/Parallels_Server_for_Mac)
* [PearPC](/wiki/PearPC)
* [QEMU](/wiki/QEMU)
* [VirtualBox](/wiki/VirtualBox)
* [Virtual Iron](/wiki/Virtual_Iron)
* [VMware Fusion](/wiki/VMware_Fusion)
* [VMware Server](/wiki/VMware_Server)
* [VMware Workstation](/wiki/VMware_Workstation) ([Player](/wiki/VMware_Workstation_Player))
* [Windows Virtual PC](/wiki/Windows_Virtual_PC)||
|Tools|* [Ganeti](/wiki/Ganeti)
* [System Center Virtual Machine Manager](/wiki/System_Center_Virtual_Machine_Manager)
* [Virtual Machine Manager](/wiki/Virtual_Machine_Manager)||
|[Operating  
system](/wiki/OS-level_virtualization)|||
|---|
|OS containers|* [FreeBSD jail](/wiki/FreeBSD_jail)
* [iCore Virtual Accounts](/wiki/ICore_Virtual_Accounts)
* [Linux-VServer](/wiki/Linux-VServer)
* [LXC](/wiki/LXC)
* [OpenVZ](/wiki/OpenVZ)
* [Solaris Containers](/wiki/Solaris_Containers)
* [Virtuozzo](/wiki/Virtuozzo_(company)#Software)
* [Workload Partitions](/wiki/Workload_Partitions)|
|Application containers|* [Docker](/wiki/Docker_(software))
* [lmctfy](/wiki/Lmctfy)
* [rkt](/wiki/Container_Linux#Overview)|
|Virtual kernel architectures|* [Rump kernel](/wiki/Rump_kernel)
* [User-mode Linux](/wiki/User-mode_Linux)
* [vkernel](/wiki/Vkernel)|
|Related kernel features|* [BrandZ](/wiki/Solaris_Containers#Branded_zones)
* [cgroups](/wiki/Cgroups)
* [chroot](/wiki/Chroot)
* [namespaces]()
* [seccomp](/wiki/Seccomp)|
|Orchestration|* [Amazon ECS](/wiki/Amazon_Web_Services)
* [Kubernetes](/wiki/Kubernetes)
* [OpenShift](/wiki/OpenShift)||
|[Desktop](/wiki/Desktop_virtualization)|* [Citrix Virtual Apps](/wiki/Citrix_Virtual_Apps)
* [Citrix Virtual Desktops](/wiki/Citrix_Virtual_Desktops)
* [Remote Desktop Services](/wiki/Remote_Desktop_Services)
* [VMware Horizon](/wiki/VMware_Horizon)|
|[Application](/wiki/Application_virtualization)|* [Ceedo](/wiki/Ceedo)
* [Citrix Virtual Apps](/wiki/Citrix_Virtual_Apps)
* [Dalvik](/wiki/Dalvik_(software))
* [InstallFree](/wiki/InstallFree)
* [Microsoft App-V](/wiki/Microsoft_App-V)
* [Remote Desktop Services](/wiki/Remote_Desktop_Services)
* [Symantec Workspace Virtualization](/wiki/Symantec_Workspace_Virtualization)
* [Turbo](/wiki/Turbo_(software))
* [VMware ThinApp](/wiki/VMware_ThinApp)
* [ZeroVM](/wiki/ZeroVM)|
|[Network](/wiki/Network_virtualization)|* [Distributed Overlay Virtual Ethernet](/wiki/Distributed_Overlay_Virtual_Ethernet) (DOVE)
* [Ethernet VPN](/wiki/Ethernet_VPN) (EVPN)
* [NVGRE](/wiki/Network_Virtualization_using_Generic_Routing_Encapsulation)
* [Open vSwitch](/wiki/Open_vSwitch)
* [Virtual security switch](/wiki/Virtual_security_switch)
* [Virtual Extensible LAN](/wiki/Virtual_Extensible_LAN) (VXLAN)|
|See also|* [BlueStacks](/wiki/BlueStacks)|
|See also: [List of emulators](/wiki/List_of_emulators), [List of computer system emulators](/wiki/List_of_computer_system_emulators)|

|                                                 |
| ----------------------------------------------- |
| * [<abbr>v</abbr>](/wiki/Template:Linux_kernel) |
* [<abbr>t</abbr>](/wiki/Template_talk:Linux_kernel)
* [<abbr>e</abbr>](https://en.wikipedia.org/w/index.php?title=Template:Linux_kernel&action=edit)[Linux kernel](/wiki/Linux_kernel)|
|Organization|||
|---|
|Kernel|* [Linux Foundation](/wiki/Linux_Foundation)
* [Linux Mark Institute](/wiki/Linux_Mark_Institute)
* [Linus's law](/wiki/Linus%27s_law)
* [Tanenbaum–Torvalds debate](/wiki/Tanenbaum%E2%80%93Torvalds_debate)
* [Tux](/wiki/Tux_(mascot))
* [SCO disputes](/wiki/SCO%E2%80%93Linux_disputes)
* [Linaro](/wiki/Linaro)
* [GNU GPL v2](/wiki/GNU_General_Public_License#Version_2)
* [menuconfig](/wiki/Menuconfig)
* [Supported computer architectures](/wiki/List_of_Linux-supported_computer_architectures)
* [Kernel names](/wiki/List_of_Linux_kernel_names)
* [Criticism](/wiki/Criticism_of_Linux)|
  |Support|* Developers
  * *[The Linux Programming Interface](/wiki/The_Linux_Programming_Interface)*
  * [kernel.org](/wiki/Kernel.org)
  * [LKML](/wiki/Linux_kernel_mailing_list)
  * [Linux conferences](/wiki/Linux_conference)
* Users
  * [Linux User Group (LUG)](/wiki/Linux_user_group)||
  |Technical|||
  |---|
  |Debugging|* [CRIU](/wiki/CRIU)
* [ftrace](/wiki/Ftrace)
* [kdump](/wiki/Kdump_(Linux))
* [Linux kernel oops](/wiki/Linux_kernel_oops)
* [SystemTap](/wiki/SystemTap)
* [BPF](/wiki/Berkeley_Packet_Filter)|
|[Startup](/wiki/Linux_startup_process)|* [vmlinux](/wiki/Vmlinux)
* [System.map](/wiki/System.map)
* [dracut](/wiki/Dracut_(software))
* [initrd](/wiki/Initrd)
* [initramfs](/wiki/Initramfs)|
|[ABIs](/wiki/Linux_kernel_interfaces)|* [Linux Standard Base](/wiki/Linux_Standard_Base)
* [x32 ABI](/wiki/X32_ABI)|
  |[APIs](/wiki/Linux_kernel_interfaces)||||
  |---|---|
  |Kernel|||
  |---|
  |[System Call  
  Interface](/wiki/Linux_kernel_interfaces#SCI)|* [POSIX](/wiki/POSIX)

  * [ioctl](/wiki/Ioctl)
  * [select](/wiki/Select_(Unix))
  * [open](/wiki/Open_(system_call))
  * [read](/wiki/Read_(system_call))
  * [close](/wiki/Close_(system_call))
  * [sync](/wiki/Sync_(Unix))
  * …
* [Linux-only](/wiki/Linux_kernel_interfaces#Additions_to_POSIX)

  * [futex](/wiki/Futex)
  * [epoll](/wiki/Epoll)
  * [splice](/wiki/Splice_(system_call))
  * [dnotify](/wiki/Dnotify)
  * [inotify](/wiki/Inotify)
  * [readahead](/wiki/Readahead)
  * …|
|[In-kernel](/wiki/Linux_kernel_interfaces#In–kernel_APIs)|* [ALSA](/wiki/Advanced_Linux_Sound_Architecture)
* [Crypto API](/wiki/Crypto_API_(Linux))
* [io uring](/wiki/Io_uring)
* [DRM](/wiki/Direct_Rendering_Manager)
* [kernfs](/wiki/Kernfs_(Linux))
* [Memory barrier](/wiki/Memory_barrier)
* [New API](/wiki/New_API)
* [RCU](/wiki/Read-copy-update)
* [Video4Linux](/wiki/Video4Linux)||
|[Userspace](/wiki/User_space)|||
|---|
|[Daemons](/wiki/Daemon_(computing)),  
[File systems](/wiki/Virtual_file_system)|* [devfs](/wiki/Devfs)
* [devpts](/wiki/Devpts)
* [debugfs](/wiki/Debugfs)
* [FUSE](/wiki/Filesystem_in_userspace)
* [procfs](/wiki/Procfs)
* [sysfs](/wiki/Sysfs)
* [systemd](/wiki/Systemd)

  * [udev](/wiki/Udev)
* [Kmscon](/wiki/Kmscon)|
  |[Wrapper  
  libraries](/wiki/Wrapper_library)|* [C standard library](/wiki/C_standard_library)

  * [glibc](/wiki/GNU_C_Library)
  * [uClibc](/wiki/UClibc)
  * [Bionic](/wiki/Bionic_(software))

    * [libhybris](/wiki/Hybris_(software))
  * [dietlibc](/wiki/Dietlibc)
  * [EGLIBC](/wiki/Embedded_GLIBC)
  * [klibc](/wiki/Klibc)
  * [musl](/wiki/Musl)
  * [Newlib](/wiki/Newlib)
* [libcgroup](/wiki/Cgroups)
* [libdrm](/wiki/Direct_Rendering_Manager)
* [libalsa](/wiki/Advanced_Linux_Sound_Architecture)
* [libevdev](/wiki/Evdev)
* [libusb](/wiki/Libusb)
* [liburing](/wiki/Io_uring)|||
|Components|* [Kernel modules](/wiki/Loadable_kernel_module)
* [BlueZ](/wiki/BlueZ)
* [cgroups](/wiki/Cgroups)
* [Console](/wiki/Linux_console)
* [bcache](/wiki/Bcache)
* [Device mapper](/wiki/Device_mapper)
* [dm-cache](/wiki/Dm-cache)
* [dm-crypt](/wiki/Dm-crypt)
* [DRM](/wiki/Direct_Rendering_Manager)
* [EDAC](/wiki/EDAC_(Linux))
* [evdev](/wiki/Evdev)
* [Kernel same-page merging](/wiki/Kernel_same-page_merging) (KSM)
* [LIO](/wiki/LIO_(SCSI_target))
* [Framebuffer](/wiki/Linux_framebuffer)
* [LVM](/wiki/Logical_Volume_Manager_(Linux))
* [KMS driver](/wiki/KMS_driver)
* [Netfilter](/wiki/Netfilter)
* [Netlink](/wiki/Netlink)
* [nftables](/wiki/Nftables)
* [Network scheduler](/wiki/Network_scheduler)
* [perf](/wiki/Perf_(Linux))
* [SLUB](/wiki/SLUB_(software))
* [zram](/wiki/Zram)
* [zswap](/wiki/Zswap)

* [Process and I/O schedulers](/wiki/Scheduling_(computing)#Linux):
* [O(n) scheduler](/wiki/O(n)_scheduler)
* [O(1) scheduler](/wiki/O(1)_scheduler)
* [Completely Fair Scheduler](/wiki/Completely_Fair_Scheduler) (CFS)
* [Brain Fuck Scheduler](/wiki/Brain_Fuck_Scheduler)
* [Noop scheduler](/wiki/Noop_scheduler)
* [SCHED\_DEADLINE](/wiki/SCHED_DEADLINE)

* [Security Modules](/wiki/Linux_Security_Modules): [AppArmor](/wiki/AppArmor)
* [Exec Shield](/wiki/Exec_Shield)
* [seccomp](/wiki/Seccomp)
* [SELinux](/wiki/Security-Enhanced_Linux)
* [Smack](/wiki/Smack_(software))
* [Tomoyo Linux](/wiki/Tomoyo_Linux)
* [Linux PAM](/wiki/Linux_PAM)

* [Device drivers](/wiki/Device_driver)

  * [802.11](/wiki/Comparison_of_open-source_wireless_drivers)
  * [graphics](/wiki/Free_and_open-source_graphics_device_driver)
* [Raw device](/wiki/Raw_device)

* [initramfs](/wiki/Initramfs)
* [KernelCare](/wiki/KernelCare)
* [kexec](/wiki/Kexec)
* [kGraft](/wiki/KGraft)
* [kpatch](/wiki/Kpatch)
* [Ksplice](/wiki/Ksplice)|
  |Variants|* [Mainline](/wiki/Mainline_Linux)

  * [Linux kernel](/wiki/Linux_kernel)
  * [Linux-libre](/wiki/Linux-libre)
* [High-performance computing](/wiki/High-performance_computing)

  * [INK](/wiki/INK_(operating_system))
  * [Compute Node Linux](/wiki/Compute_Node_Linux)
  * [SLURM](/wiki/Slurm_Workload_Manager)
* [Real-time computing](/wiki/Real-time_computing)

  * [RTLinux](/wiki/RTLinux)
  * [RTAI](/wiki/RTAI)
  * [Xenomai](/wiki/Xenomai)
  * [Carrier Grade Linux](/wiki/Carrier_Grade_Linux)
* [MMU](/wiki/Memory_management_unit)\-less
  * [μClinux](/wiki/%CE%9CClinux)
  * [PSXLinux](/wiki/PSXLinux)||
  |---|
  |[Virtualization](/wiki/Virtualization)|* [Hypervisor](/wiki/Hypervisor)

  * [KVM](/wiki/Kernel-based_Virtual_Machine)
  * [Xen](/wiki/Xen)
* [OS-level virtualization](/wiki/OS-level_virtualization)

  * [Linux-VServer](/wiki/Linux-VServer)
  * [Lguest](/wiki/Lguest)
  * [LXC](/wiki/LXC)
  * [OpenVZ](/wiki/OpenVZ)
* Other
  * [L4Linux](/wiki/L4Linux)
  * [ELinOS](/wiki/ELinOS)
  * [User-mode Linux](/wiki/User-mode_Linux)
  * [MkLinux](/wiki/MkLinux)
  * [coLinux](/wiki/Cooperative_Linux)|||
  |[Adoption](/wiki/Linux_adoption)|||
  |---|
  |[Range  
  of use](/wiki/Linux_range_of_use)|* [Desktop](/wiki/Linux_desktop_environments)
* [Embedded](/wiki/Linux_on_embedded_systems)
* [Gaming](/wiki/Linux_gaming)
* Thin client:
  * [LTSP](/wiki/Linux_Terminal_Server_Project)
* Server:
  * [LAMP](/wiki/LAMP_(software_bundle))
  * [LYME-LYCE](/wiki/LYME_(software_bundle))
* [Devices](/wiki/Linux-powered_device)|
|Adopters|* [List of Linux adopters](/wiki/List_of_Linux_adopters)
* [GENIVI Alliance](/wiki/GENIVI_Alliance)
* [Proprietary software for Linux](/wiki/List_of_proprietary_software_for_Linux)||