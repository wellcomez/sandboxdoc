---
date: 2023-06-02 16:30
title: setns - reassociate thread with a namespace
tags:
- setns
---



#### **NAME**

setns - reassociate thread with a namespace

  

#### **SYNOPSIS**

**#define** **_GNU_SOURCE** /* See [feature_test_macros](https://manpages.ubuntu.com/manpages/xenial/en/man7/feature_test_macros.7.html)(7) */

**#include** **<[sched.h](file:///usr/include/sched.h)>**

**int** **setns(int** fd**,** **int** nstype**);**

  

#### **DESCRIPTION**

Given a file descriptor referring to a namespace, reassociate the calling thread with that namespace.

The fd argument is a file descriptor referring to one of the namespace entries in a [/proc/](file:///proc/)[pid]/ns/ directory; see **[namespaces](https://manpages.ubuntu.com/manpages/xenial/en/man7/namespaces.7.html)**(7) for further information on [/proc/](file:///proc/)[pid]/ns/.

  

The calling thread will be reassociated with the corresponding namespace, subject to any constraints imposed by the nstype argument.

The nstype argument specifies which type of namespace the calling thread may be reassociated with. This argument can have one of the following values:

**0** Allow any type of namespace to be joined.

**CLONE_NEWIPC** (since Linux 3.0)

fd must refer to an IPC namespace.

**CLONE_NEWNET** (since Linux 3.0)

fd must refer to a network namespace.

**CLONE_NEWNS** (since Linux 3.8)

fd must refer to a mount namespace.

**CLONE_NEWPID** (since Linux 3.8)

fd must refer to a descendant PID namespace.

**CLONE_NEWUSER** (since Linux 3.8)

fd must refer to a user namespace.

**CLONE_NEWUTS** (since Linux 3.0)

fd must refer to a UTS namespace.

Specifying nstype as 0 suffices if the caller knows (or does not care) what type of namespace is referred to by fd. Specifying a nonzero value for nstype is useful if the caller does not know what type of namespace is referred to by fd and wants to ensure that the namespace is of a particular type. (The caller might not know the type of the namespace referred to by fd if the file descriptor was opened by another process and, for example, passed to the caller via a UNIX domain socket.)

**CLONE_NEWPID** behaves somewhat differently from the other nstype values: reassociating the calling thread with a PID namespace changes only the PID namespace that child processes of the caller will be created in; it does not change the PID namespace of the caller itself. Reassociating with a PID namespace is allowed only if the PID namespace specified by fd is a descendant (child, grandchild, etc.) of the PID namespace of the caller. For further details on PID namespaces, see **[pid_namespaces](https://manpages.ubuntu.com/manpages/xenial/en/man7/pid_namespaces.7.html)**(7)[[pid_namespaces]].

A process reassociating itself with a user namespace must have the **CAP_SYS_ADMIN** capability in the target user namespace. <mark style="background: #FF5582A6;">Upon successfully joining a user namespace, a process is granted all capabilities in that namespace, regardless of its user and group IDs.</mark> A<mark style="background: #35E708E3;"> multithreaded process may not change user namespace with **setns**()</mark>. It is not permitted to use **setns**() to reenter the caller's current user namespace. This prevents a caller that has dropped capabilities from regaining those capabilities via a call to **setns**(). For security reasons, a process can't join a new user namespace if it is sharing filesystem-related attributes (the attributes whose sharing is controlled by the **[clone](https://manpages.ubuntu.com/manpages/xenial/en/man2/clone.2.html)**(2) **CLONE_FS** flag) with another process. For further details on user namespaces, see **[user_namespaces](https://manpages.ubuntu.com/manpages/xenial/en/man7/user_namespaces.7.html)**(7).

A process may not be reassociated with a new mount namespace if it is multithreaded. <mark style="background: #E77C08E3;">Changing the mount namespace requires that the caller possess both **CAP_SYS_CHROOT** and **CAP_SYS_ADMIN** capabilities in its own user namespace and **CAP_SYS_ADMIN** in the target mount namespace. </mark>See **[user_namespaces](https://manpages.ubuntu.com/manpages/xenial/en/man7/user_namespaces.7.html)**(7) for details on the interaction of user namespaces and mount namespaces.

  

#### **RETURN** **VALUE**

On success, **setns**() returns 0. On failure, -1 is returned and errno is set to indicate the error.

  

#### **ERRORS**

**EBADF** fd is not a valid file descriptor.

**EINVAL** fd refers to a namespace whose type does not match that specified in nstype.

**EINVAL** There is problem with reassociating the thread with the specified namespace.

**EINVAL** The caller tried to join an ancestor (parent, grandparent, and so on) PID namespace.

**EINVAL** The caller attempted to join the user namespace in which it is already a member.

**EINVAL** The caller shares filesystem (**CLONE_FS**) state (in particular, the root directory)

with other processes and tried to join a new user namespace.

**EINVAL** The caller is multithreaded and tried to join a new user namespace.

**ENOMEM** Cannot allocate sufficient memory to change the specified namespace.

**EPERM** The calling thread did not have the required capability for this operation.

  

#### **VERSIONS**

The **setns**() system call first appeared in Linux in kernel 3.0; library support was added to glibc in version 2.14.

  

#### **CONFORMING** **TO**

The **setns**() system call is Linux-specific.

  

#### **NOTES**

Not all of the attributes that can be shared when a new thread is created using **[clone](https://manpages.ubuntu.com/manpages/xenial/en/man2/clone.2.html)**(2) can be changed using **setns**().

  

#### **EXAMPLE**

The program below takes two or more arguments. The first argument specifies the pathname of a namespace file in an existing [/proc/](file:///proc/)[pid]/ns/ directory. The remaining arguments specify a command and its arguments. The program opens the namespace file, joins that namespace using **setns**(), and executes the specified command inside that namespace.

The following shell session demonstrates the use of this program (compiled as a binary named ns_exec) in conjunction with the **CLONE_NEWUTS** example program in the **[clone](https://manpages.ubuntu.com/manpages/xenial/en/man2/clone.2.html)**(2) man page (complied as a binary named newuts).

We begin by executing the example program in **[clone](https://manpages.ubuntu.com/manpages/xenial/en/man2/clone.2.html)**(2) in the background. That program creates a child in a separate UTS namespace. The child changes the hostname in its namespace, and then both processes display the hostnames in their UTS namespaces, so that we can see that they are different.
~~~