---
date: 2023-08-02 11:14
title: CentOS7系统——systemd特性
tags:
- 
---



# CentOS7系统——systemd特性

 原创

[183530300](https://blog.51cto.com/183530300)2016-09-20 10:59:16博主文章分类：[linux系统管理](https://blog.51cto.com/183530300/category4)©著作权

_文章标签_[新特性](https://blog.51cto.com/topic/xintexing.html)[Systemd](https://blog.51cto.com/topic/systemd.html)_文章分类_[开源](https://blog.51cto.com/nav/open-source)_阅读数_1440

## 一、init发展史：

Centos 5 ：SysV init

Centos 6 ：Upstart

Centos 7 ：Systemd



## 二、Systemd的特性：

1 系统引导时实现服务并行启动

2 按需启动守护进程

3 自动化的服务依赖关系管理

4 同时采用D-Bus总线式和Socket式激活服务

5 系统状态快照



## 三、核心概念：unit

unit由基于相关配置文件进行标识，识别和配置；文件中主要包含了系统服务、监听的socket、保存的快照以及其它init相关的信息，这些配置文件主要保存在：

/usr/lib/systemd/system：每个服务最主要的启动脚本设置，类似于之前的/etc/init.d/

/run/systemd/system：系统执行过程中所产生的服务脚本，比上面目录优先运行

/etc/systemd/system：管理员建立的执行脚本，类似于/etc/rc.d/rcN.d/Sxx类的功能，比上面目录优先运行



## 四、unit常见类型：

Service unit：文件扩展名为.service,用于定义系统服务

Target unit：文件扩展名为.target,用于模拟运行级别

Device unit：文件扩展名为.device,用于定义内核识别的设备

Mount unit：文件扩展名为.mount,定义文件系统挂载点

Socket unit：文件扩展名为.socket,用于标识进程间通信用到的socket文件，也可在系统启动时，延迟启动服务，实现按需启动

Swap unit：文件扩展名为.swap,用于标识swap设备

Snapshot unit：文件扩展名为.snapshot,用于管理系统快照

Path unit：文件扩展名为.path,用于定义文件系统中的一个文件或目录,常用于当文件系统变化时，延迟激活服务，如：spool 目录

Automount unit：.automount，文件系统的自动挂载点



## 五、关键特性：

1. 基于socket的激活机制，socket与服务程序分离

2. 基于d-bus激活机制

3. 基于device的激活机制

4. 基于path的激活机制

5. 系统快照，保存unit的当前状态信息持久存储设备中

6. 向后兼容SysV init脚本：/etc/init.d



## 六、Systemd也有不兼容的情况：

service的脚本格式不完全固定，可以添加

直接使用程序本身的可执行程序启动的服务，使用service命令可以探测到工作状态并进行管理

systemctl的unit格式是固定不变的，不可以自行添加格式

非由systemd启动的服务，systemctl无法控制此服务

（可以自行编写unit脚本，就可以使用systemctl进行管理了）



## 七、Centos 7 service类型的systemctl命令

1. systemctl [option] COMMAND [name]

	启动：service service-name start=>systemctl start service-name
	
	停止：service service-name stop=>systemctl stop service-name
	
	重启：service service-name restart=>systemctl restart service-name
	
	查看状态：service service-name status=>systemctl status service-name
	
	查看unit类型：Systemctl –t help 
	
	条件式重启：service service-name condrestart=>systemctl try-restart service-name
	
	重载或重启：systemctl reload-or-restart service-name
	
	重载或条件重启：systemctl reload-or-try-restart service-name



2. 状态相关命令：

	查看所有服务：systemctl list-units -t service -a
	
	只查看服务单元的状态：systemctl -t service
	
	查看某服务当前激活与否的状态：systemctl is-active service-name
	
	查看已激活的所有服务：systemctl list-units -t service
	
	列出失败的服务：systemctl --failed -t service
	
	查看所有服务的开机自启状态：chkconfig --list ==> systemctl list-unit-files -t service
	
	查看服务是否开机自启：systemctl is-enabled name.service
	
	
	
	设置服务开机自启：chkconfig service-name on=>systemctl enable service-name
	
	禁止服务开机自启：chkconfig service-name off=>systemctl disable service-name
	
	禁止服务设定为开机自启：systemctl mask service-name
	
	取消禁止服务设定为开机自启：systemctl umask service-name
	
	查看某服务的依赖关系：sysemctl list-dependencies service-name
	
	杀掉进程：systemctl kill 进程名



3. 服务状态指示

	systemctl list-units -t service -a 显示所有服务状态
	
	loaded：Unit配置文件已处理
	
	active(running)：一次或多次持续处理的运行
	
	active(exited)：成功完成一次性的配置
	
	active(waiting)：运行中，等待一个事件
	
	inactive：不运行
	
	enabled：开机启动
	
	disabled：开机不启动
	
	static：开机不启动，但可被另一个启用的服务激活



### 八、管理target units

1. 运行级别：0-6

	0=>runlevel0.target,poweroff.target
	
	1=>runlevel1.target,rescue.target
	
	2=>runlevel2.target,multi-user.target
	
	3=>runlevel3.target,multi-user.target
	
	4=>runlevel4.target,multi-user.target
	
	5=>runlevel5.target,graphical.target
	
	6=>runlevel6.target,reboot.target



2. 级别查看及切换：
	级别切换：init N=>systemctl isolate name.target
	注意：只有当前target文件中AllowIsolate=yes才能切换
	级别查看：runlevel=>systemctl list-units -t target
	查看所有级别：systemctl list-units -t target -a
	查看默认运行级别：systemctl get-default
	修改默认运行级别：systemctl set-default name.target
	切换至救援模式：systemctl rescuce
	切换至紧急模式：systemctl emergency
	驱动安装有问题时，可以进入此模式卸载驱动（最简洁的模式）



3. 其它常用命令：  

	关机：systemctl poweroff|halt
	
	重启：systemctl reboot
	
	挂起：systemctl suspend
	
	快照：systemctl hipernate
	
	快照并挂起：systemctl hybrid-sleep



### 九、service unit文件

1. 通常由三部分组成：  

	[unit]:定义与unit类型无关的通用选项，用于提供unit的描述信息、unit行为及依赖关系等
	
	[service]:定义与特定类型相关的专用选项，此处为service类型
	
	[Install]:定义由"systemctl enable"以及"systemctl disable"命令实现服务启用或禁用时用到的一些选项



2. unit段的常用选项：
	
	Description:描述信息，意义性描述
	
	After：定义unit的启动顺序，表示当前unit应该晚于哪些unit启动，其功能与before相反
	
	Requies：依赖到的其它unit，强依赖，被依赖的unit无法激活时，当前unit就无法激活
	
	Wants：依赖的其它unit，弱依赖，被依赖的unit无法激活时，当前unit仍然可以激活
	
	Conflicts：定义unit间的冲突



 3. service段的常用选项：

	Type:用于定义影响ExecStart及相关参数的功能的unit进程启动类型
	
	simple：默认值，这个daemon主要由ExecStart接的指令串来启动，启动后常驻于内存中
	
	forking：由ExecStart启动的程序透过spawns延伸出其他子程序来作为此daemon的主要服务。原生父程序在启动结束后就会终止
	
	oneshot：与simple类似，不过这个程序在工作完毕后就结束了，不会常驻在内存中
	
	dbus：与simple类似，但这个daemon必须要在取得一个D-Bus的名称后，才会继续运作.因此通常也要同时设定BusNname= 才行
	
	notify：在启动完成后会发送一个通知消息。还需要配合NotifyAccess 来让 Systemd 接收消息
	
	idle：与simple类似，要执行这个daemon必须要所有的工作都顺利执行完毕后才会执行。这类的daemon通常是开机到最后才执行即可的服务
	
	Environmentfile：环境启动文件
	
	ExeStart：启动unit要运行的命令或脚本
	
	ExeStartPre：启动unit之前预先运行的命令或脚本
	
	ExeStartPost：启动unit之后要运行的命令或脚本
	
	ExeStop：停止unit后要运行的命令或脚本
	
	Restart：重启unit后要运行的命令或脚本



4. Install段的常用选项：

	Alias：当前unit的别名
	
	RequiredBy：被哪些unit所依赖，强依赖
	
	WanteBy：被哪些unit所依赖，弱依赖
	
	Also：安装本服务时还要安装哪些相关服务



注意：对于新建的unit文件或修改后的unit文件，需要通知systemd重载此配置文件

 重载命令：systemctl daemon-reload