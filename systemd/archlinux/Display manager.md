---
date: 2023-08-11 09:23
title: archlinux-wiki Display manager
tags:
- 
---



# Display manager

https://wiki.archlinux.org/title/Display_manager


- [Page](https://wiki.archlinux.org/title/Display_manager "View the content page [alt-shift-c]")
- [Discussion](https://wiki.archlinux.org/title/Talk:Display_manager "Discussion about the content page [alt-shift-t]")

- [Read](https://wiki.archlinux.org/title/Display_manager)
- [View source](https://wiki.archlinux.org/index.php?title=Display_manager&action=edit "This page is protected.
    You can view its source [alt-shift-e]")
- [View history](https://wiki.archlinux.org/index.php?title=Display_manager&action=history "Past revisions of this page [alt-shift-h]")

Related articles

- [Start X at login](https://wiki.archlinux.org/title/Start_X_at_login "Start X at login")

A [display manager](https://en.wikipedia.org/wiki/X_display_manager_(program_type) "wikipedia:X display manager (program type)"), or login manager, is typically a graphical user interface that is displayed at the end of the boot process in place of the default shell. There are various implementations of display managers, just as there are various types of [window managers](https://wiki.archlinux.org/title/Window_managers "Window managers") and [desktop environments](https://wiki.archlinux.org/title/Desktop_environments "Desktop environments"). There is usually a certain amount of customization and themeability available with each one.

## 1. List of display managers

### 1.1. Console

- **[CDM](https://wiki.archlinux.org/title/CDM "CDM")** — Login manager written in Bash.

	[https://github.com/evertiro/cdm](https://github.com/evertiro/cdm) || [cdm](https://aur.archlinux.org/packages/cdm/)AUR

- **[Console TDM](https://wiki.archlinux.org/title/Console_TDM "Console TDM")** — Extension for _xinit_ written in pure Bash.

	[https://github.com/dopsi/console-tdm](https://github.com/dopsi/console-tdm) || [console-tdm](https://aur.archlinux.org/packages/console-tdm/)AUR

- **nodm** — Display manager for automatic logins. Discontinued since 2017.

	[https://github.com/spanezz/nodm](https://github.com/spanezz/nodm) || [nodm](https://archlinux.org/packages/?name=nodm)

- **Ly** — TUI (ncurses-like) display manager for Linux and BSD. Supports X and Wayland sessions.

	[https://github.com/nullgemm/ly](https://github.com/nullgemm/ly) || [ly](https://archlinux.org/packages/?name=ly)

- **tbsm** — A Bash session or application launcher. Supports X and Wayland sessions.

	[https://loh-tar.github.io/tbsm/](https://loh-tar.github.io/tbsm/) || [tbsm](https://aur.archlinux.org/packages/tbsm/)AUR

- **emptty** — Simple CLI Display Manager on TTY with X and Wayland support.

	[https://github.com/tvrzna/emptty/](https://github.com/tvrzna/emptty/) || [emptty-git](https://aur.archlinux.org/packages/emptty-git/)AUR

- **loginx** — A getty/login/xinit combination with a friendly curses ui.

	[https://sourceforge.net/projects/loginx/](https://sourceforge.net/projects/loginx/) || [loginx](https://aur.archlinux.org/packages/loginx/)AUR

- **Lemurs** — TUI display manager for Linux, written in Rust. Supports X, Wayland and TTY shell sessions.

	[https://github.com/coastalwhite/lemurs](https://github.com/coastalwhite/lemurs) || [lemurs-git](https://aur.archlinux.org/packages/lemurs-git/)AUR

### 1.2. Graphical

- **[Entrance](https://wiki.archlinux.org/title/Entrance "Entrance")** — [Enlightenment](https://wiki.archlinux.org/title/Enlightenment "Enlightenment") display manager. Highly experimental, and does not have proper systemd support.

	[https://github.com/Obsidian-StudiosInc/entrance](https://github.com/Obsidian-StudiosInc/entrance) || [entrance-git](https://aur.archlinux.org/packages/entrance-git/)AUR

- **[GDM](https://wiki.archlinux.org/title/GDM "GDM")** — [GNOME](https://wiki.archlinux.org/title/GNOME "GNOME") display manager.

	[https://wiki.gnome.org/Projects/GDM](https://wiki.gnome.org/Projects/GDM) || [gdm](https://archlinux.org/packages/?name=gdm)

- **[LightDM](https://wiki.archlinux.org/title/LightDM "LightDM")** — Cross-desktop display manager, can use various front-ends written in any toolkit.

	[https://github.com/CanonicalLtd/lightdm/](https://github.com/CanonicalLtd/lightdm/) || [lightdm](https://archlinux.org/packages/?name=lightdm)

- **[LXDM](https://wiki.archlinux.org/title/LXDM "LXDM")** — [LXDE](https://wiki.archlinux.org/title/LXDE "LXDE") display manager. Can be used independent of the LXDE desktop environment.

	[https://sourceforge.net/projects/lxdm/](https://sourceforge.net/projects/lxdm/) || [lxdm](https://archlinux.org/packages/?name=lxdm), [lxdm-gtk3](https://archlinux.org/packages/?name=lxdm-gtk3)

- **[SDDM](https://wiki.archlinux.org/title/SDDM "SDDM")** — QML-based display manager and successor to KDM; recommended for [Plasma](https://wiki.archlinux.org/title/Plasma "Plasma") and [LXQt](https://wiki.archlinux.org/title/LXQt "LXQt").

	[https://github.com/sddm/sddm](https://github.com/sddm/sddm) || [sddm](https://archlinux.org/packages/?name=sddm)

- **[SLiM](https://wiki.archlinux.org/title/SLiM "SLiM")** — Lightweight and elegant graphical login solution. Discontinued since 2013, not fully compatible with systemd.

	[https://sourceforge.net/projects/slim.berlios/](https://sourceforge.net/projects/slim.berlios/) || [slim](https://archlinux.org/packages/?name=slim)

- **[XDM](https://wiki.archlinux.org/title/XDM "XDM")** — X display manager with support for XDMCP, host chooser.

	[xdm(8)](https://man.archlinux.org/man/xdm.8) || [xorg-xdm](https://archlinux.org/packages/?name=xorg-xdm)

### 1.3. Login daemons

- **[greetd](https://wiki.archlinux.org/title/Greetd "Greetd")** — Login daemon which supports both console and graphical greeters.

[https://git.sr.ht/~kennylevinsen/greetd](https://git.sr.ht/~kennylevinsen/greetd) || [greetd](https://archlinux.org/packages/?name=greetd)

## 2. Loading the display manager

To enable graphical login, [enable](https://wiki.archlinux.org/title/Enable "Enable") the appropriate systemd service. For example, for [SDDM](https://wiki.archlinux.org/title/SDDM "SDDM"), enable `sddm.service`.

This should work out of the box. If not, you might have to reset a custom `default.target` symlink to point to the default `graphical.target`. See [systemd#Change default target to boot into](https://wiki.archlinux.org/title/Systemd#Change_default_target_to_boot_into "Systemd").

After enabling [SDDM](https://wiki.archlinux.org/title/SDDM "SDDM") a symlink `display-manager.service` should be set in `/etc/systemd/system/`. You may need to use `--force` to override old symlinks.

~~~
$ file /etc/systemd/system/display-manager.service

/etc/systemd/system/display-manager.service: symbolic link to /usr/lib/systemd/system/sddm.service

~~~
### 2.1. Using systemd-logind

In order to check the status of your user session, you can use _loginctl_. All [polkit](https://wiki.archlinux.org/title/Polkit "Polkit") actions like suspending the system or mounting external drives will work out of the box.

~~~
$ loginctl show-session $XDG_SESSION_ID
~~~

## 3. Session configuration

Many display managers read available sessions from `/usr/share/xsessions/` directory. It contains standard [desktop entry files](https://specifications.freedesktop.org/desktop-entry-spec/latest/) for each desktop environment or window manager. Some display managers use a separate `/usr/share/wayland-sessions/` to list [Wayland](https://wiki.archlinux.org/title/Wayland "Wayland")-specific sessions.

To add/remove entries to your display manager's session list, create/remove the _.desktop_ files in `/usr/share/xsessions/` as desired. A typical _.desktop_ file will look something like:

~~~
[Desktop Entry]
Name=Openbox
Comment=Log in using the Openbox window manager (without a session manager)
Exec=/usr/bin/openbox-session
TryExec=/usr/bin/openbox-session
Icon=openbox.png
Type=Application
~~~

### 3.1. Run ~/.xinitrc as a session

Installing [xinit-xsession](https://aur.archlinux.org/packages/xinit-xsession/)AUR will provide an option to run your [xinitrc](https://wiki.archlinux.org/title/Xinitrc "Xinitrc") as a session. Simply set `xinitrc` as the session in your display manager's settings and make sure that the `~/.xinitrc` file is executable.

### 3.2. Starting applications without a window manager

You can also launch an application without any decoration, desktop, or window management. For example to launch [google-chrome](https://aur.archlinux.org/packages/google-chrome/)AUR create a `web-browser.desktop` file in `/usr/share/xsessions/` like this:

~~~
[Desktop Entry]
Name=Web Browser
Comment=Use a web browser as your session
Exec=/usr/bin/google-chrome --auto-launch-at-startup
TryExec=/usr/bin/google-chrome --auto-launch-at-startup
Icon=google-chrome
Type=Application
~~~

In this case, once you login, the application set with `Exec` will be launched immediately. When you close the application, you will be taken back to the login manager (same as logging out of a normal desktop environment or window manager).

It is important to remember that most graphical applications are not intended to be launched this way and you might have manual tweaking to do or limitations to live with (there is no window manager, so do not expect to be able to move or resize _any_ windows, including dialogs; nonetheless, you might be able to set the window geometry in the application's configuration files).

See also [xinitrc#Starting applications without a window manager](https://wiki.archlinux.org/title/Xinitrc#Starting_applications_without_a_window_manager "Xinitrc").

## 4. Tips and tricks

### 4.1. Autostarting

Most display managers source `/etc/xprofile`, `~/.xprofile` and `/etc/X11/xinit/xinitrc.d/`. For more details, see [xprofile](https://wiki.archlinux.org/title/Xprofile "Xprofile").

### 4.2. Set language for user session

For display managers that use [AccountsService](https://freedesktop.org/wiki/Software/AccountsService/) the [locale](https://wiki.archlinux.org/title/Locale "Locale") for the user session can be set by editing:

~~~
/var/lib/AccountsService/users/$USER

[User]
Language=_your_locale_
~~~

where _your_locale_ is a value such as `en_GB.UTF-8`.

Alternatively, you can achieve this using [D-Bus](https://wiki.archlinux.org/title/D-Bus "D-Bus"): `busctl call org.freedesktop.Accounts /org/freedesktop/Accounts/User$UID org.freedesktop.Accounts.User SetLanguage s _your_locale_`

Log out and then back in again for the changes to take effect.