---
date: 2023-08-11 10:13
title: systemd
tags:
- systemd
- Drop-in files
---

# systemd
https://wiki.archlinux.org/title/Systemd


- [Page](https://wiki.archlinux.org/title/Systemd "View the content page [alt-shift-c]")
- [Discussion](https://wiki.archlinux.org/title/Talk:Systemd "Discussion about the content page [alt-shift-t]")

- [Read](https://wiki.archlinux.org/title/Systemd)
- [View source](https://wiki.archlinux.org/index.php?title=Systemd&action=edit "This page is protected.
    You can view its source [alt-shift-e]")
- [View history](https://wiki.archlinux.org/index.php?title=Systemd&action=history "Past revisions of this page [alt-shift-h]")

Related articles

- [systemd/User](https://wiki.archlinux.org/title/Systemd/User "Systemd/User")
- [systemd/Timers](https://wiki.archlinux.org/title/Systemd/Timers "Systemd/Timers")
- [systemd/Journal](https://wiki.archlinux.org/title/Systemd/Journal "Systemd/Journal")
- [systemd/FAQ](https://wiki.archlinux.org/title/Systemd/FAQ "Systemd/FAQ")
- [init](https://wiki.archlinux.org/title/Init "Init")
- [udev](https://wiki.archlinux.org/title/Udev "Udev")
- [Improving performance/Boot process](https://wiki.archlinux.org/title/Improving_performance/Boot_process "Improving performance/Boot process")
- [Allow users to shutdown](https://wiki.archlinux.org/title/Allow_users_to_shutdown "Allow users to shutdown")

From the [project web page](https://systemd.io/):

_systemd_ is a suite of basic building blocks for a Linux system. It provides a system and service manager that runs as PID 1 and starts the rest of the system. _systemd_ provides aggressive parallelization capabilities, uses socket and [D-Bus](https://wiki.archlinux.org/title/D-Bus "D-Bus") activation for starting services, offers on-demand starting of daemons, keeps track of processes using Linux [control groups](https://wiki.archlinux.org/title/Control_groups "Control groups"), maintains mount and automount points, and implements an elaborate transactional dependency-based service control logic. _systemd_ supports SysV and LSB init scripts and works as a replacement for sysvinit. Other parts include a logging daemon, utilities to control basic system configuration like the hostname, date, locale, maintain a list of logged-in users and running containers and virtual machines, system accounts, runtime directories and settings, and daemons to manage simple network configuration, network time synchronization, log forwarding, and name resolution.

Historically, what systemd calls "service" was named [daemon](https://en.wikipedia.org/wiki/Daemon_(computing) "wikipedia:Daemon (computing)"): any program that runs as a "background" process (without a terminal or user interface), commonly waiting for events to occur and offering services. A good example is a web server that waits for a request to deliver a page, or a ssh server waiting for someone trying to log in. While these are full featured applications, there are daemons whose work is not that visible. Daemons are for tasks like writing messages into a log file (e.g. `syslog`, `metalog`) or keeping your system time accurate (e.g. [ntpd](https://wiki.archlinux.org/title/Ntpd "Ntpd")). For more information see [daemon(7)](https://man.archlinux.org/man/daemon.7).

**Note:** For a detailed explanation of why Arch moved to _systemd_, see [this forum post](https://bbs.archlinux.org/viewtopic.php?pid=1149530#p1149530).

## 1. Basic systemctl usage

The main command used to introspect and control _systemd_ is _systemctl_. Some of its uses are examining the system state and managing the system and services. See [systemctl(1)](https://man.archlinux.org/man/systemctl.1) for more details.

**Tip:**

- You can use all of the following _systemctl_ commands with the `-H _user_@_host_` switch to control a _systemd_ instance on a remote machine. This will use [SSH](https://wiki.archlinux.org/title/SSH "SSH") to connect to the remote _systemd_ instance.
- [Plasma](https://wiki.archlinux.org/title/Plasma "Plasma") users can install [systemdgenie](https://archlinux.org/packages/?name=systemdgenie) as a graphical frontend for _systemctl_. After installing, the module will be added under _System_.

### 1.1. Using units

Units commonly include, but are not limited to, services (_.service_), mount points (_.mount_), devices (_.device_) and sockets (_.socket_).

When using _systemctl_, you generally have to specify the complete name of the unit file, including its suffix, for example `sshd.socket`. There are however a few short forms when specifying the unit in the following _systemctl_ commands:

- If you do not specify the suffix, systemctl will assume _.service_. For example, `netctl` and `netctl.service` are equivalent.
- Mount points will automatically be translated into the appropriate _.mount_ unit. For example, specifying `/home` is equivalent to `home.mount`.
- Similar to mount points, devices are automatically translated into the appropriate _.device_ unit, therefore specifying `/dev/sda2` is equivalent to `dev-sda2.device`.

See [systemd.unit(5)](https://man.archlinux.org/man/systemd.unit.5) for details.

**Note:** Some unit names contain an `@` sign (e.g. `name@_string_.service`): this means that they are [instances](http://0pointer.net/blog/projects/instances.html) of a _template_ unit, whose actual file name does not contain the `_string_` part (e.g. `name@.service`). `_string_` is called the _instance identifier_, and is similar to an argument that is passed to the template unit when called with the _systemctl_ command: in the unit file it will substitute the `%i` specifier. To be more accurate, _before_ trying to instantiate the `name@._suffix_` template unit, _systemd_ will actually look for a unit with the exact `name@_string_._suffix_` file name, although by convention such a "clash" happens rarely, i.e. most unit files containing an `@` sign are meant to be templates. Also, if a template unit is called without an instance identifier, it will generally fail (except with certain _systemctl_ commands, like `cat`).

The commands in the below table operate on **system units** since `--system` is the implied default for _systemctl_. To instead operate on **user units** (for the _calling user_), use [systemctl --user](https://wiki.archlinux.org/title/Systemctl_--user "Systemctl --user") without root privileges. See also [systemd/User#Basic setup](https://wiki.archlinux.org/title/Systemd/User#Basic_setup "Systemd/User") to enable/disable user units for _all users_.

**Tip:**

- Most commands also work if multiple units are specified, see [systemctl(1)](https://man.archlinux.org/man/systemctl.1) for more information.
- The `--now` switch can be used in conjunction with `enable`, `disable`, and `mask` to respectively start, stop, or mask the unit _immediately_ rather than after rebooting.
- A package may offer units for different purposes. If you just installed a package, `pacman -Qql _package_ | grep -Fe .service -e .socket` can be used to check and find them.

|Action|Command|Note|
|---|---|---|
|Analyzing the system state|   |   |
|**Show system status**|`systemctl status`||
|**List running** units|`systemctl` or  <br>`systemctl list-units`||
|**List failed** units|`systemctl --failed`||
|**List installed** unit files1|`systemctl list-unit-files`||
|**Show process status** for a PID|`systemctl status _pid_`|[cgroup slice](https://wiki.archlinux.org/title/Cgroups "Cgroups"), memory and parent|
|Checking the unit status|   |   |
|**Show a manual page** associated with a unit|`systemctl help _unit_`|as supported by the unit|
|**Status** of a unit|`systemctl status _unit_`|including whether it is running or not|
|**Check** whether a unit is enabled|`systemctl is-enabled _unit_`||
|Starting, restarting, reloading a unit|   |   |
|**Start** a unit immediately|`systemctl start _unit_` as root||
|**Stop** a unit immediately|`systemctl stop _unit_` as root||
|**Restart** a unit|`systemctl restart _unit_` as root||
|**Reload** a unit and its configuration|`systemctl reload _unit_` as root||
|**Reload systemd manager** configuration2|`systemctl daemon-reload` as root|scan for new or changed units|
|Enabling a unit|   |   |
|**Enable** a unit to start automatically at boot|`systemctl enable _unit_` as root||
|**Enable** a unit to start automatically at boot and **start** it immediately|`systemctl enable --now _unit_` as root||
|**Disable** a unit to no longer start at boot|`systemctl disable _unit_` as root||
|**Reenable** a unit3|`systemctl reenable _unit_` as root|i.e. disable and enable anew|
|Masking a unit|   |   |
|**Mask** a unit to make it impossible to start4|`systemctl mask _unit_` as root||
|**Unmask** a unit|`systemctl unmask _unit_` as root||

1. See [systemd.unit(5) § UNIT FILE LOAD PATH](https://man.archlinux.org/man/systemd.unit.5#UNIT_FILE_LOAD_PATH) for the directories where available unit files can be found.
2. This does not ask the changed units to reload their own configurations (see the **Reload** action).
3. For example, in case its `[Install]` section has changed since last enabling it.
4. Both manually and as a dependency, which makes masking dangerous. Check for existing masked units with:
    
    $ systemctl list-unit-files --state=masked
    

### 1.2. Power management

[polkit](https://wiki.archlinux.org/title/Polkit "Polkit") is necessary for power management as an unprivileged user. If you are in a local _systemd-logind_ user session and no other session is active, the following commands will work without root privileges. If not (for example, because another user is logged into a tty), _systemd_ will automatically ask you for the root password.

|Action|Command|
|---|---|
|Shut down and reboot the system|`systemctl reboot`|
|Shut down and power-off the system|`systemctl poweroff`|
|Suspend the system|`systemctl suspend`|
|Put the system into hibernation|`systemctl hibernate`|
|Put the system into hybrid-sleep state (or suspend-to-both)|`systemctl hybrid-sleep`|

## 2. Writing unit files

The syntax of _systemd'_s unit files ([systemd.unit(5)](https://man.archlinux.org/man/systemd.unit.5)) is inspired by [XDG Desktop Entry Specification .desktop files](https://wiki.archlinux.org/title/Desktop_entries "Desktop entries"), which are in turn inspired by [Microsoft Windows .ini files](https://en.wikipedia.org/wiki/INI_file "wikipedia:INI file"). Unit files are loaded from multiple locations (to see the full list, run `systemctl show --property=UnitPath`), but the main ones are (listed from lowest to highest precedence):

- `/usr/lib/systemd/system/`: units provided by installed packages
- `/etc/systemd/system/`: units installed by the system administrator

**Note:**

- The load paths are completely different when running _systemd_ in [user mode](https://wiki.archlinux.org/title/Systemd/User#How_it_works "Systemd/User").
- _systemd_ unit names may only contain ASCII alphanumeric characters, underscores and periods. All other characters must be replaced by C-style "\x2d" escapes, or employ their predefined semantics ('@', '-'). See [systemd.unit(5)](https://man.archlinux.org/man/systemd.unit.5) and [systemd-escape(1)](https://man.archlinux.org/man/systemd-escape.1) for more information.

Look at the units installed by your packages for examples, as well as [systemd.service(5) § EXAMPLES](https://man.archlinux.org/man/systemd.service.5#EXAMPLES).

**Tip:** Comments prepended with `#` may be used in unit-files as well, but only in new lines. Do not use end-line comments after _systemd_ parameters or the unit will fail to activate.

### 2.1. Handling dependencies

With _systemd_, dependencies can be resolved by designing the unit files correctly. The most typical case is when unit _A_ requires unit _B_ to be running before _A_ is started. In that case add `Requires=_B_` and `After=_B_` to the `[Unit]` section of _A_. If the dependency is optional, add `Wants=_B_` and `After=_B_` instead. Note that `Wants=` and `Requires=` do not imply `After=`, meaning that if `After=` is not specified, the two units will be started in parallel.

Dependencies are typically placed on services and not on [\#Targets](https://wiki.archlinux.org/title/Systemd#Targets). For example, `network.target` is pulled in by whatever service configures your network interfaces, therefore ordering your custom unit after it is sufficient since `network.target` is started anyway.

### 2.2. Service types

There are several different start-up types to consider when writing a custom service file. This is set with the `Type=` parameter in the `[Service]` section:

- `Type=simple` (default): _systemd_ considers the service to be started up immediately. The process must not fork. Do not use this type if other services need to be ordered on this service, unless it is socket activated.
- `Type=forking`: _systemd_ considers the service started up once the process forks and the parent has exited. For classic daemons, use this type unless you know that it is not necessary. You should specify `PIDFile=` as well so _systemd_ can keep track of the main process.
- `Type=oneshot`: this is useful for scripts that do a single job and then exit. You may want to set `RemainAfterExit=yes` as well so that _systemd_ still considers the service as active after the process has exited. Setting `RemainAfterExit=yes` is appropriate for the units which change the system state (e.g., mount some partition).
- `Type=notify`: identical to `Type=simple`, but with the stipulation that the daemon will send a signal to _systemd_ when it is ready. The reference implementation for this notification is provided by _libsystemd-daemon.so_.
- `Type=dbus`: the service is considered ready when the specified `BusName` appears on DBus's system bus.
- `Type=idle`: _systemd_ will delay execution of the service binary until all jobs are dispatched. Other than that behavior is very similar to `Type=simple`.

See the [systemd.service(5) § OPTIONS](https://man.archlinux.org/man/systemd.service.5#OPTIONS) man page for a more detailed explanation of the `Type` values.

### 2.3. Editing provided units

To avoid conflicts with pacman, unit files provided by packages should not be directly edited. There are two safe ways to modify a unit without touching the original file: create a new unit file which [overrides the original unit](https://wiki.archlinux.org/title/Systemd#Replacement_unit_files) or create [drop-in snippets](https://wiki.archlinux.org/title/Systemd#Drop-in_files) which are applied on top of the original unit. For both methods, you must reload the unit afterwards to apply your changes. This can be done either by editing the unit with `systemctl edit` (which reloads the unit automatically) or by reloading all units with:

~~~
$ systemctl daemon-reload
~~~

**Tip:**

- You can use _systemd-delta_ to see which unit files have been overridden or extended and what exactly has been changed.
- Use `systemctl cat _unit_` to view the content of a unit file and all associated drop-in snippets.

#### 2.3.1. Replacement unit files

To replace the unit file `/usr/lib/systemd/system/_unit_`, create the file `/etc/systemd/system/_unit_` and [reenable](https://wiki.archlinux.org/title/Systemd#Using_units) the unit to update the symlinks.

Alternatively, run:

$ systemctl edit --full _unit_

This opens `/etc/systemd/system/_unit_` in your editor (copying the installed version if it does not exist yet) and automatically reloads it when you finish editing.

**Note:** The replacement units will keep on being used even if Pacman updates the original units in the future. This method makes system maintenance more difficult and therefore the next approach is preferred.

#### 2.3.2. Drop-in files

To create drop-in files for the unit file `/usr/lib/systemd/system/_unit_`, create the directory `/etc/systemd/system/_unit_.d/` and place _.conf_ files there to override or add new options. _systemd_ will parse and apply these files on top of the original unit.

The easiest way to do this is to run:

~~~
$ systemctl edit _unit_ --drop-in=_drop_in_name_
~~~

This opens the file `/etc/systemd/system/_unit_.d/_drop_in_name_.conf` in your text editor (creating it if necessary) and automatically reloads the unit when you are done editing. Omitting `--drop-in=` option will result in systemd using the default file name `override.conf` .

**Note:**

- The key must be still placed in the appropriate section in the override file.
- Not all keys can be overridden with drop-in files. For example, for changing `Conflicts=` a replacement file [is necessary](https://lists.freedesktop.org/archives/systemd-devel/2017-June/038976.html).

#### 2.3.3. Revert to vendor version

To revert any changes to a unit made using `systemctl edit` do:
	$ systemctl revert _unit_

#### 2.3.4. Examples

For example, if you simply want to add an additional dependency to a unit, you may create the following file:

~~~
/etc/systemd/system/_unit_.d/customdependency.conf

[Unit]
Requires=_new dependency_
After=_new dependency_
~~~

As another example, in order to replace the `ExecStart` directive, create the following file:

~~~
/etc/systemd/system/_unit_.d/customexec.conf

[Service]
ExecStart=
ExecStart=_new command_
~~~
Note how `ExecStart` must be cleared before being re-assigned [[1]](https://bugzilla.redhat.com/show_bug.cgi?id=756787#c9). The same holds for every item that can be specified multiple times, e.g. `OnCalendar` for timers.

One more example to automatically restart a service:

/etc/systemd/system/_unit_.d/restart.conf

[Service]
Restart=always
RestartSec=30

## 3. Targets

_systemd_ uses _targets_ to group units together via dependencies and as standardized synchronization points. They serve a similar purpose as [runlevels](https://en.wikipedia.org/wiki/Runlevel "wikipedia:Runlevel") but act a little differently. Each _target_ is named instead of numbered and is intended to serve a specific purpose with the possibility of having multiple ones active at the same time. Some _target_s are implemented by inheriting all of the services of another _target_ and adding additional services to it. There are _systemd_ _target_s that mimic the common SystemVinit runlevels so you can still switch _target_s using the familiar `telinit RUNLEVEL` command.

### 3.1. Get current targets

The following should be used under _systemd_ instead of running `runlevel`:

~~~
$ systemctl list-units --type=target
~~~

### 3.2. Create custom target

The runlevels that held a defined meaning under sysvinit (i.e., 0, 1, 3, 5, and 6); have a 1:1 mapping with a specific _systemd_ _target_. Unfortunately, there is no good way to do the same for the user-defined runlevels like 2 and 4. If you make use of those it is suggested that you make a new named _systemd_ _target_ as `/etc/systemd/system/_your target_` that takes one of the existing runlevels as a base (you can look at `/usr/lib/systemd/system/graphical.target` as an example), make a directory `/etc/systemd/system/_your target_.wants`, and then symlink the additional services from `/usr/lib/systemd/system/` that you wish to enable.

### 3.3. Mapping between SysV runlevels and systemd targets

|SysV Runlevel|systemd Target|Notes|
|---|---|---|
|0|runlevel0.target, poweroff.target|Halt the system.|
|1, s, single|runlevel1.target, rescue.target|Single user mode.|
|2, 4|runlevel2.target, runlevel4.target, multi-user.target|User-defined/Site-specific runlevels. By default, identical to 3.|
|3|runlevel3.target, multi-user.target|Multi-user, non-graphical. Users can usually login via multiple consoles or via the network.|
|5|runlevel5.target, graphical.target|Multi-user, graphical. Usually has all the services of runlevel 3 plus a graphical login.|
|6|runlevel6.target, reboot.target|Reboot|
|emergency|emergency.target|Emergency shell|

### 3.4. Change current target

In _systemd_, targets are exposed via _target units_. You can change them like this:

~~~
$ systemctl isolate graphical.target
~~~

This will only change the current target, and has no effect on the next boot. This is equivalent to commands such as `telinit 3` or `telinit 5` in Sysvinit.

### 3.5. Change default target to boot into

The standard target is `default.target`, which is a symlink to `graphical.target`. This roughly corresponds to the old runlevel 5.

To verify the current target with _systemctl_:

~~~
$ systemctl get-default
~~~

To change the default target to boot into, change the `default.target` symlink. With _systemctl_:

~~~
$ systemctl set-default multi-user.target
~~~

Removed /etc/systemd/system/default.target.
Created symlink /etc/systemd/system/default.target -> /usr/lib/systemd/system/multi-user.target.

Alternatively, append one of the following [kernel parameters](https://wiki.archlinux.org/title/Kernel_parameters "Kernel parameters") to your boot loader:

- `systemd.unit=multi-user.target` (which roughly corresponds to the old runlevel 3),
- `systemd.unit=rescue.target` (which roughly corresponds to the old runlevel 1).

### 3.6. Default target order

_systemd_ chooses the `default.target` according to the following order:

1. Kernel parameter shown above
2. Symlink of `/etc/systemd/system/default.target`
3. Symlink of `/usr/lib/systemd/system/default.target`

## 4. systemd components

Some (not exhaustive) components of _systemd_ are:

- [kernel-install](https://wiki.archlinux.org/title/Kernel-install "Kernel-install") — a script used to automatically move [kernels](https://wiki.archlinux.org/title/Kernel "Kernel") and their respective [initramfs](https://wiki.archlinux.org/title/Initramfs "Initramfs") images to the boot partition;
- [systemd-boot](https://wiki.archlinux.org/title/Systemd-boot "Systemd-boot") — simple UEFI [boot manager](https://wiki.archlinux.org/title/Boot_manager "Boot manager");
- [systemd-creds](https://wiki.archlinux.org/title/Systemd-creds "Systemd-creds") — to securely store and retrieve credentials used by systemd units;
- [systemd-cryptenroll(1)](https://man.archlinux.org/man/systemd-cryptenroll.1) — Enroll PKCS#11, FIDO2, TPM2 token/devices to LUKS2 encrypted volumes;
- [systemd-firstboot](https://wiki.archlinux.org/title/Systemd-firstboot "Systemd-firstboot") — basic system setting initialization before first boot;
- [systemd-homed](https://wiki.archlinux.org/title/Systemd-homed "Systemd-homed") — portable human-user [accounts](https://wiki.archlinux.org/title/Users_and_groups "Users and groups");
- [systemd-logind(8)](https://man.archlinux.org/man/systemd-logind.8) — [session management](https://dvdhrm.wordpress.com/2013/08/24/session-management-on-linux/);
- [systemd-networkd](https://wiki.archlinux.org/title/Systemd-networkd "Systemd-networkd") — [network configuration](https://wiki.archlinux.org/title/Network_configuration "Network configuration") management;
- [systemd-nspawn](https://wiki.archlinux.org/title/Systemd-nspawn "Systemd-nspawn") — light-weight namespace container;
- [systemd-resolved](https://wiki.archlinux.org/title/Systemd-resolved "Systemd-resolved") — network [name resolution](https://wiki.archlinux.org/title/Domain_name_resolution "Domain name resolution");
- [systemd-stub(7)](https://man.archlinux.org/man/systemd-stub.7) — a UEFI boot stub used for creating [unified kernel images](https://wiki.archlinux.org/title/Unified_kernel_image "Unified kernel image");
- [systemd-sysusers(8)](https://man.archlinux.org/man/systemd-sysusers.8) — creates system users and groups and adds users to groups at package installation or boot time;
- [systemd-timesyncd](https://wiki.archlinux.org/title/Systemd-timesyncd "Systemd-timesyncd") — [system time](https://wiki.archlinux.org/title/System_time "System time") synchronization across the network;
- [systemd/Journal](https://wiki.archlinux.org/title/Systemd/Journal "Systemd/Journal") — system logging;
- [systemd/Timers](https://wiki.archlinux.org/title/Systemd/Timers "Systemd/Timers") — monotonic or realtime timers for controlling _.service_ files or events, reasonable alternative to [cron](https://wiki.archlinux.org/title/Cron "Cron").

### 4.1. systemd.mount - mounting

_systemd_ is in charge of mounting the partitions and filesystems specified in `/etc/fstab`. The [systemd-fstab-generator(8)](https://man.archlinux.org/man/systemd-fstab-generator.8) translates all the entries in `/etc/fstab` into _systemd_ units; this is performed at boot time and whenever the configuration of the system manager is reloaded.

_systemd_ extends the usual [fstab](https://wiki.archlinux.org/title/Fstab "Fstab") capabilities and offers additional mount options. These affect the dependencies of the mount unit. They can, for example, ensure that a mount is performed only once the network is up or only once another partition is mounted. The full list of specific _systemd_ mount options, typically prefixed with `x-systemd.`, is detailed in [systemd.mount(5) § FSTAB](https://man.archlinux.org/man/systemd.mount.5#FSTAB).

An example of these mount options is _automounting_, which means mounting only when the resource is required rather than automatically at boot time. This is provided in [fstab#Automount with systemd](https://wiki.archlinux.org/title/Fstab#Automount_with_systemd "Fstab").

#### 4.1.1. GPT partition automounting

On UEFI-booted systems, if specific conditions are met, [systemd-gpt-auto-generator(8)](https://man.archlinux.org/man/systemd-gpt-auto-generator.8) will automount GPT partitions following the [Discoverable Partitions Specification](https://uapi-group.org/specifications/specs/discoverable_partitions_specification/). Automounted partitions can thus be omitted from [fstab](https://wiki.archlinux.org/title/Fstab "Fstab"), and if the root partition is automounted, then `root=` can be omitted from the kernel command line.

The prerequisites are:

- The boot loader must set the [LoaderDevicePartUUID](https://systemd.io/BOOT_LOADER_INTERFACE/) EFI variable, so that the used EFI system partition can be identified. This is supported by [systemd-boot](https://wiki.archlinux.org/title/Systemd-boot "Systemd-boot"), [systemd-stub(7)](https://man.archlinux.org/man/systemd-stub.7), [GRUB](https://wiki.archlinux.org/title/GRUB "GRUB") (with _grub-mkconfig_ generated `grub.cfg`; custom `grub.cfg` requires [loading the bli module](https://wiki.archlinux.org/title/GRUB#LoaderDevicePartUUID "GRUB")) and [rEFInd (not enabled by default)](https://wiki.archlinux.org/title/REFInd#LoaderDevicePartUUID "REFInd"). This can be verified by running `bootctl` and checking the status of `Boot loader sets ESP information`.
- The root partition must be on the same physical disk as the used EFI system partition. Other partitions that will be automounted must be on the same physical disk as the root partition. This basically means that all automounted partitions must share the same physical disk with the ESP.
- The `/efi` mount point must be manually created (if desired), otherwise `systemd-gpt-auto-generator` will use `/boot`.

**Warning:** Be very careful when creating `/efi` on an existing system when using GPT automounting. `/efi` will be used as the default mount point on the next boot, which might leave your system in an inconsistent state with an empty `/boot` directory. You will most likely need to reinstall your kernel(s) and/or microcode, regenerate your initramfs, etc..

**Tip:** The automounting of a partition can be disabled by changing the partition's [type GUID](https://en.wikipedia.org/wiki/GUID_Partition_Table#Partition_type_GUIDs "wikipedia:GUID Partition Table") or setting the partition attribute bit 63 "do not automount", see [gdisk#Prevent GPT partition automounting](https://wiki.archlinux.org/title/Gdisk#Prevent_GPT_partition_automounting "Gdisk").

##### 4.1.1.1. /var

For `/var` automounting to work, the PARTUUID must match the SHA256 HMAC hash of the partition type UUID (`4d21b016-b534-45c2-a9fb-5c16e091fd2d`) keyed by the machine ID. The required PARTUUID can be obtained using:

~~~
$ systemd-id128 -u --app-specific=4d21b016-b534-45c2-a9fb-5c16e091fd2d machine-id
~~~

**Note:** [systemd-id128(1)](https://man.archlinux.org/man/systemd-id128.1) reads the machine ID from `/etc/machine-id`, this makes it impossible to know the needed PARTUUID before the system is installed.

### 4.2. systemd-sysvcompat

The primary role of [systemd-sysvcompat](https://archlinux.org/packages/?name=systemd-sysvcompat) (required by [base](https://archlinux.org/packages/?name=base)) is to provide the traditional linux [init](https://wiki.archlinux.org/title/Init "Init") binary. For _systemd_-controlled systems, `init` is just a symbolic link to its `systemd` executable.

In addition, it provides four convenience shortcuts that [SysVinit](https://wiki.archlinux.org/title/SysVinit "SysVinit") users might be used to. The convenience shortcuts are [halt(8)](https://man.archlinux.org/man/halt.8), [poweroff(8)](https://man.archlinux.org/man/poweroff.8), [reboot(8)](https://man.archlinux.org/man/reboot.8) and [shutdown(8)](https://man.archlinux.org/man/shutdown.8). Each one of those four commands is a symbolic link to `systemctl`, and is governed by _systemd_ behavior. Therefore, the discussion at [#Power management](https://wiki.archlinux.org/title/Systemd#Power_management) applies.

_systemd_-based systems can give up those System V compatibility methods by using the `init=` [boot parameter](https://wiki.archlinux.org/title/Kernel_parameters#Parameter_list "Kernel parameters") (see, for example, [/bin/init is in systemd-sysvcompat ?](https://bbs.archlinux.org/viewtopic.php?id=233387)) and _systemd_ native `systemctl` command arguments.

### 4.3. systemd-tmpfiles - temporary files

_systemd-tmpfiles_ creates, deletes and cleans up volatile and temporary files and directories. It reads configuration files in `/etc/tmpfiles.d/` and `/usr/lib/tmpfiles.d/` to discover which actions to perform. Configuration files in the former directory take precedence over those in the latter directory.

Configuration files are usually provided together with service files, and they are named in the style of `/usr/lib/tmpfiles.d/_program_.conf`. For example, the [Samba](https://wiki.archlinux.org/title/Samba "Samba") daemon expects the directory `/run/samba` to exist and to have the correct permissions. Therefore, the [samba](https://archlinux.org/packages/?name=samba) package ships with this configuration:

/usr/lib/tmpfiles.d/samba.conf

D /run/samba 0755 root root

Configuration files may also be used to write values into certain files on boot. For example, if you used `/etc/rc.local` to disable wakeup from USB devices with `echo USBE > /proc/acpi/wakeup`, you may use the following tmpfile instead:

/etc/tmpfiles.d/disable-usb-wake.conf

~~~
#    Path                  Mode UID  GID  Age Argument
w    /proc/acpi/wakeup     -    -    -    -   USBE
~~~

See the [systemd-tmpfiles(8)](https://man.archlinux.org/man/systemd-tmpfiles.8) and [tmpfiles.d(5)](https://man.archlinux.org/man/tmpfiles.d.5) man pages for details.

**Note:** This method may not work to set options in `/sys` since the _systemd-tmpfiles-setup_ service may run before the appropriate device modules are loaded. In this case, you could check whether the module has a parameter for the option you want to set with `modinfo _module_` and set this option with a [config file in /etc/modprobe.d](https://wiki.archlinux.org/title/Kernel_modules#Setting_module_options "Kernel modules"). Otherwise, you will have to write a [udev rule](https://wiki.archlinux.org/title/Udev_rule "Udev rule") to set the appropriate attribute as soon as the device appears.

## 5. Tips and tricks

[![Tango-view-fullscreen.png](https://wiki.archlinux.org/images/3/38/Tango-view-fullscreen.png)](https://wiki.archlinux.org/title/File:Tango-view-fullscreen.png)**This article or section needs expansion.**[![Tango-view-fullscreen.png](https://wiki.archlinux.org/images/3/38/Tango-view-fullscreen.png)](https://wiki.archlinux.org/title/File:Tango-view-fullscreen.png)

**Reason:** We should document explicitly somewhere the benefits of socket activation over dumb service starting. This is mentioned briefly at the beginning of the page and in related pages like [Avahi](https://wiki.archlinux.org/title/Avahi "Avahi"). (Discuss in [Talk:Systemd](https://wiki.archlinux.org/title/Talk:Systemd))

### 5.1. GUI configuration tools

- **systemadm** — Graphical browser for _systemd_ units. It can show the list of units, possibly filtered by type.

[https://cgit.freedesktop.org/systemd/systemd-ui/](https://cgit.freedesktop.org/systemd/systemd-ui/) || [systemd-ui](https://archlinux.org/packages/?name=systemd-ui)

- **SystemdGenie** — _systemd_ management utility based on KDE technologies.

[https://invent.kde.org/system/systemdgenie](https://invent.kde.org/system/systemdgenie) || [systemdgenie](https://archlinux.org/packages/?name=systemdgenie)

### 5.2. Running services after the network is up

To delay a service until after the network is up, include the following dependencies in the _.service_ file:

/etc/systemd/system/_foo_.service

[Unit]
...
Wants=network-online.target
After=network-online.target
...

The network wait service of the [network manager](https://wiki.archlinux.org/title/Network_manager "Network manager") in use must also be enabled so that `network-online.target` properly reflects the network status.

- If using [NetworkManager](https://wiki.archlinux.org/title/NetworkManager "NetworkManager"), `NetworkManager-wait-online.service` should be enabled together with `NetworkManager.service`. Check if this is the case with `systemctl is-enabled NetworkManager-wait-online.service`. If it is not enabled, then [reenable](https://wiki.archlinux.org/title/Systemd#Using_units) `NetworkManager.service`.
- In the case of [netctl](https://wiki.archlinux.org/title/Netctl "Netctl"), [enable](https://wiki.archlinux.org/title/Systemd#Using_units) the `netctl-wait-online.service` (unless you are using _netctl-auto_; see [FS#75836](https://bugs.archlinux.org/task/75836)).
- If using [systemd-networkd](https://wiki.archlinux.org/title/Systemd-networkd "Systemd-networkd"), `systemd-networkd-wait-online.service` should be enabled together with `systemd-networkd.service`. Check if this is the case with `systemctl is-enabled systemd-networkd-wait-online.service`. If it is not enabled, then [reenable](https://wiki.archlinux.org/title/Systemd#Using_units) `systemd-networkd.service`.

For more detailed explanations, see the discussion in the [Network configuration synchronization points](https://systemd.io/NETWORK_ONLINE/#discussion).

If a service needs to perform DNS queries, it should additionally be ordered after `nss-lookup.target`:

/etc/systemd/system/_foo_.service

[Unit]
...
Wants=network-online.target
After=network-online.target nss-lookup.target
...

See [systemd.special(7) § Special Passive System Units](https://man.archlinux.org/man/systemd.special.7#Special_Passive_System_Units).

For `nss-lookup.target` to have any effect it needs a service that pulls it in via `Wants=nss-lookup.target` and orders itself before it with `Before=nss-lookup.target`. Typically this is done by local [DNS resolvers](https://wiki.archlinux.org/title/DNS_resolver "DNS resolver").

Check which active service, if any, is pulling in `nss-lookup.target` with:

$ systemctl list-dependencies --reverse nss-lookup.target

### 5.3. Enable installed units by default

[![Tango-view-fullscreen.png](https://wiki.archlinux.org/images/3/38/Tango-view-fullscreen.png)](https://wiki.archlinux.org/title/File:Tango-view-fullscreen.png)**This article or section needs expansion.**[![Tango-view-fullscreen.png](https://wiki.archlinux.org/images/3/38/Tango-view-fullscreen.png)](https://wiki.archlinux.org/title/File:Tango-view-fullscreen.png)

**Reason:** How does it work with instantiated units? (Discuss in [Talk:Systemd](https://wiki.archlinux.org/title/Talk:Systemd))

Arch Linux ships with `/usr/lib/systemd/system-preset/99-default.preset` containing `disable *`. This causes _systemctl preset_ to disable all units by default, such that when a new package is installed, the user must manually enable the unit.

If this behavior is not desired, simply create a symlink from `/etc/systemd/system-preset/99-default.preset` to `/dev/null` in order to override the configuration file. This will cause _systemctl preset_ to enable all units that get installed—regardless of unit type—unless specified in another file in one _systemctl preset'_s configuration directories. User units are not affected. See [systemd.preset(5)](https://man.archlinux.org/man/systemd.preset.5) for more information.

**Note:** Enabling all units by default may cause problems with packages that contain two or more mutually exclusive units. _systemctl preset_ is designed to be used by distributions and spins or system administrators. In the case where two conflicting units would be enabled, you should explicitly specify which one is to be disabled in a preset configuration file as specified in the [systemd.preset(5)](https://man.archlinux.org/man/systemd.preset.5) man page.

### 5.4. Sandboxing application environments

[![Tango-go-next.png](https://wiki.archlinux.org/images/f/f0/Tango-go-next.png)](https://wiki.archlinux.org/title/File:Tango-go-next.png)**This article or section is a candidate for moving to [systemd/Sandboxing](https://wiki.archlinux.org/index.php?title=Systemd/Sandboxing&action=edit&redlink=1 "Systemd/Sandboxing (page does not exist)").**[![Tango-go-next.png](https://wiki.archlinux.org/images/f/f0/Tango-go-next.png)](https://wiki.archlinux.org/title/File:Tango-go-next.png)

**Notes:** The topic is broad enough for a dedicated page. See [User:NetSysFire/systemd sandboxing](https://wiki.archlinux.org/title/User:NetSysFire/systemd_sandboxing "User:NetSysFire/systemd sandboxing") for a proposed draft. (Discuss in [Talk:Security#systemd unit hardening and system.conf tweaks](https://wiki.archlinux.org/title/Talk:Security#systemd_unit_hardening_and_system.conf_tweaks "Talk:Security"))

A unit file can be created as a sandbox to isolate applications and their processes within a hardened virtual environment. _systemd_ leverages [namespaces](https://en.wikipedia.org/wiki/Linux_namespaces "wikipedia:Linux namespaces"), a list of allowed/denied [capabilities](https://wiki.archlinux.org/title/Capabilities "Capabilities"), and [control groups](https://wiki.archlinux.org/title/Control_groups "Control groups") to container processes through an extensive execution environment configuration—[systemd.exec(5)](https://man.archlinux.org/man/systemd.exec.5).

The enhancement of an existing _systemd_ unit file with application sandboxing typically requires trial-and-error tests accompanied by the generous use of [strace](https://archlinux.org/packages/?name=strace), [stderr](https://en.wikipedia.org/wiki/Standard_streams#Standard_error_.28stderr.29 "wikipedia:Standard streams") and [journalctl(1)](https://man.archlinux.org/man/journalctl.1) error logging and output facilities. You may want to first search upstream documentation for already done tests to base trials on. To get a starting point for possible hardening options, run

$ systemd-analyze security _unit_

Some examples of how sandboxing with _systemd_ can be deployed:

- `CapabilityBoundingSet` defines a list of [capabilities(7)](https://man.archlinux.org/man/capabilities.7) that are allowed or denied for a unit. See [systemd.exec(5) § CAPABILITIES](https://man.archlinux.org/man/systemd.exec.5#CAPABILITIES).
    - The `CAP_SYS_ADM` capability, for example, which should be one of the [goals of a secure sandbox](https://lwn.net/Articles/486306/): `CapabilityBoundingSet=~ CAP_SYS_ADM`

### 5.5. Notifying about failed services

In order to notify about service failures, a `OnFailure=` directive needs to be added to the according service file, for example by using a [drop-in configuration file](https://wiki.archlinux.org/title/Systemd#Drop-in_files). Adding this directive to every service unit can be achieved with a top-level drop-in configuration file. For details about top-level drop-ins, see [systemd.unit(5)](https://man.archlinux.org/man/systemd.unit.5).

Create a top-level drop-in for services:

/etc/systemd/system/service.d/toplevel-override.conf

[Unit]
OnFailure=failure-notification@%n

This adds `OnFailure=failure-notification@%n` to every service file. If _some_service_unit_ fails, `failure-notification@_some_service_unit_` will be started to handle the notification delivery (or whatever task it is configured to perform).

Create the `failure-notification@` template unit:

/etc/systemd/system/failure-notification@.service

[Unit]
Description=Send a notification about a failed systemd unit
After=network.target

[Service]
Type=simple
ExecStart=/_path_/_to_/failure-notification.sh %i

You can create the `failure-notification.sh` script and define what to do or how to notify (mail, gotify, xmpp, etc.). The `%i` will be the name of the failed service unit and will be passed as argument to the script.

In order to prevent a recursion for starting instances of `failure-notification@.service` again and again if the start fails, create an empty drop-in configuration file with the same name as the top-level drop-in (the empty service-level drop-in configuration file takes precedence over the top-level drop-in and overrides the latter one):

~~~
# mkdir -p /etc/systemd/system/failure-notification@.service.d
# touch /etc/systemd/system/failure-notification@.service.d/toplevel-override.conf
~~~

### 5.6. Automatically turn off an external HDD at shutdown

[![Merge-arrows-2.png](https://wiki.archlinux.org/images/c/c9/Merge-arrows-2.png)](https://wiki.archlinux.org/title/File:Merge-arrows-2.png)**This article or section is a candidate for merging with [Udisks#Troubleshooting](https://wiki.archlinux.org/title/Udisks#Troubleshooting "Udisks").**[![Merge-arrows-2.png](https://wiki.archlinux.org/images/c/c9/Merge-arrows-2.png)](https://wiki.archlinux.org/title/File:Merge-arrows-2.png)

**Notes:** This is mainly centered around [udisks](https://wiki.archlinux.org/title/Udisks "Udisks") usage, it is probably a better fit for this content instead of shoving it inside the systemd page. (Discuss in [Talk:Systemd](https://wiki.archlinux.org/title/Talk:Systemd))

If an external HDD is not powered off properly at system shutdown, it may be desirable to fix the issue. The most convenient way to do this is using [udisks](https://wiki.archlinux.org/title/Udisks "Udisks").

[Enable](https://wiki.archlinux.org/title/Enable "Enable") `udisks2.service`.

A service to invoke our script might look like so:

/etc/systemd/system/handle_external_hdds.service

[Unit]
Requires=udisks2.service
Requires=graphical.target
After=graphical.target
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStop=/usr/local/bin/handle_external_hdds.sh
[Install]
WantedBy=graphical.target

[Enable](https://wiki.archlinux.org/title/Enable "Enable") `handle_external_hdds.service`

Do a systemd [daemon-reload](https://wiki.archlinux.org/title/Daemon-reload "Daemon-reload") to apply the new setting.

Reboot or restart `graphical.target` to check if works.

An example script to handle an arbitrary amount of partitions on a single disk looks like so:

~~~
/usr/local/bin/handle_external_hdds.sh

#!/bin/bash -u

declare -a uuids=(_uuid_list_)

# Only proceed if the drive is present.
if [[ ! -L "/dev/disk/by-uuid/${uuids[0]}" ]]; then
  exit 0
fi

for uuid in "${uuids[@]}"; do
  if findmnt "/dev/disk/by-uuid/$uuid"; then
    umount "/dev/disk/by-uuid/$uuid"
  fi
done

# udisksctl powers off proper drive even if its partition is supplied
udisksctl power-off -b "/dev/disk/by-uuid/${uuids[0]}"
~~~

_uuid_list_ is a list of space delimited UUIDs corresponding to partitions of the device to check, e.g. `"_uuid_1_" "_uuid_2_"`.

## 6. Troubleshooting

### 6.1. Investigating failed services

To find the _systemd_ services which failed to start:

$ systemctl --state=failed

To find out why they failed, examine their log output. See [systemd/Journal#Filtering output](https://wiki.archlinux.org/title/Systemd/Journal#Filtering_output "Systemd/Journal") for details.

### 6.2. Diagnosing boot problems

_systemd_ has several options for diagnosing problems with the boot process. See [boot debugging](https://wiki.archlinux.org/title/Boot_debugging "Boot debugging") for more general instructions and options to capture boot messages before _systemd_ takes over the [boot process](https://wiki.archlinux.org/title/Boot_process "Boot process"). Also see freedesktop.org's [systemd debugging documentation](https://freedesktop.org/wiki/Software/systemd/Debugging/).

### 6.3. Diagnosing a service

If some _systemd_ service misbehaves or you want to get more information about what is happening, set the `SYSTEMD_LOG_LEVEL` [environment variable](https://wiki.archlinux.org/title/Environment_variable "Environment variable") to `debug`. For example, to run the _systemd-networkd_ daemon in debug mode:

Add a [drop-in file](https://wiki.archlinux.org/title/Systemd#Drop-in_files) for the service adding the two lines:

[Service]
Environment=SYSTEMD_LOG_LEVEL=debug

Or equivalently, set the environment variable manually:

~~~
# SYSTEMD_LOG_LEVEL=debug /lib/systemd/systemd-networkd
~~~

then [restart](https://wiki.archlinux.org/title/Restart "Restart") _systemd-networkd_ and watch the journal for the service with the `-f`/`--follow` option.

### 6.4. Shutdown/reboot takes terribly long

If the shutdown process takes a very long time (or seems to freeze), most likely a service not exiting is to blame. _systemd_ waits some time for each service to exit before trying to kill it. To find out whether you are affected, see [Shutdown completes eventually](https://freedesktop.org/wiki/Software/systemd/Debugging/#shutdowncompleteseventually) in the _systemd_ wiki.

A common problem is a stalled shutdown or suspend process. To verify whether that is the case, you could run either of these commands and check the outputs

~~~
$ systemctl poweroff
~~~

Failed to power off system via logind: There's already a shutdown or sleep operation in progress

~~~
$ systemctl list-jobs
~~~

  JOB UNIT                    TYPE  STATE  
...
21593 systemd-suspend.service start running
21592 suspend.target          start waiting
..

The [solution](https://unix.stackexchange.com/a/579531) to this would be to cancel these jobs by running

~~~
$ systemctl cancel
~~~
~~~
$ systemctl stop systemd-suspend.service
~~~

and then trying shutdown or reboot again.

### 6.5. Short lived processes do not seem to log any output

If running `journalctl -u foounit` as root does not show any output for a short lived service, look at the PID instead. For example, if `systemd-modules-load.service` fails, and `systemctl status systemd-modules-load` shows that it ran as PID 123, then you might be able to see output in the journal for that PID, i.e. by running `journalctl -b _PID=123` as root. Metadata fields for the journal such as `_SYSTEMD_UNIT` and `_COMM` are collected asynchronously and rely on the `/proc` directory for the process existing. Fixing this requires fixing the kernel to provide this data via a socket connection, similar to `SCM_CREDENTIALS`. In short, it is a [bug](https://github.com/systemd/systemd/issues/2913). Keep in mind that immediately failed services might not print anything to the journal as per design of _systemd_.

### 6.6. Boot time increasing over time

[![Tango-inaccurate.png](https://wiki.archlinux.org/images/d/d6/Tango-inaccurate.png)](https://wiki.archlinux.org/title/File:Tango-inaccurate.png)**The factual accuracy of this article or section is disputed.**[![Tango-inaccurate.png](https://wiki.archlinux.org/images/d/d6/Tango-inaccurate.png)](https://wiki.archlinux.org/title/File:Tango-inaccurate.png)

**Reason:** NetworkManager issues are not _systemd_'s fault, the alleged reports are missing. Slow `systemctl status` or `journalctl` do not affect boot time. (Discuss in [Talk:Systemd](https://wiki.archlinux.org/title/Talk:Systemd))

After using `systemd-analyze` a number of users have noticed that their boot time has increased significantly in comparison with what it used to be. After using `systemd-analyze blame` [NetworkManager](https://wiki.archlinux.org/title/NetworkManager "NetworkManager") is being reported as taking an unusually large amount of time to start.

The problem for some users has been due to `/var/log/journal` becoming too large. This may have other impacts on performance, such as for `systemctl status` or `journalctl`. As such the solution is to remove every file within the folder (ideally making a backup of it somewhere, at least temporarily) and then setting a journal file size limit as described in [Systemd/Journal#Journal size limit](https://wiki.archlinux.org/title/Systemd/Journal#Journal_size_limit "Systemd/Journal").
HomePackagesForumsWikiBugsSecurityAURDownload
Jump to content
Toggle sidebar
Search ArchWiki
Create account

Personal tools

Contents hide

Beginning
1Basic systemctl usage
Toggle Basic systemctl usage subsection
2Writing unit files
Toggle Writing unit files subsection
3Targets
Toggle Targets subsection
4systemd components
Toggle systemd components subsection
5Tips and tricks
Toggle Tips and tricks subsection
6Troubleshooting
Toggle Troubleshooting subsection
7See also

9 languages
systemd
Page
Discussion
Read
View source
View history

Related articles

systemd/User
systemd/Timers
systemd/Journal
systemd/FAQ
init
udev
Improving performance/Boot process
Allow users to shutdown
From the project web page:

systemd is a suite of basic building blocks for a Linux system. It provides a system and service manager that runs as PID 1 and starts the rest of the system. systemd provides aggressive parallelization capabilities, uses socket and D-Bus activation for starting services, offers on-demand starting of daemons, keeps track of processes using Linux control groups, maintains mount and automount points, and implements an elaborate transactional dependency-based service control logic. systemd supports SysV and LSB init scripts and works as a replacement for sysvinit. Other parts include a logging daemon, utilities to control basic system configuration like the hostname, date, locale, maintain a list of logged-in users and running containers and virtual machines, system accounts, runtime directories and settings, and daemons to manage simple network configuration, network time synchronization, log forwarding, and name resolution.
Historically, what systemd calls "service" was named daemon: any program that runs as a "background" process (without a terminal or user interface), commonly waiting for events to occur and offering services. A good example is a web server that waits for a request to deliver a page, or a ssh server waiting for someone trying to log in. While these are full featured applications, there are daemons whose work is not that visible. Daemons are for tasks like writing messages into a log file (e.g. syslog, metalog) or keeping your system time accurate (e.g. ntpd). For more information see daemon(7).

Note: For a detailed explanation of why Arch moved to systemd, see this forum post.
Basic systemctl usage
The main command used to introspect and control systemd is systemctl. Some of its uses are examining the system state and managing the system and services. See systemctl(1) for more details.

Tip:
You can use all of the following systemctl commands with the -H user@host switch to control a systemd instance on a remote machine. This will use SSH to connect to the remote systemd instance.
Plasma users can install systemdgenie as a graphical frontend for systemctl. After installing, the module will be added under System.
Using units
Units commonly include, but are not limited to, services (.service), mount points (.mount), devices (.device) and sockets (.socket).

When using systemctl, you generally have to specify the complete name of the unit file, including its suffix, for example sshd.socket. There are however a few short forms when specifying the unit in the following systemctl commands:

If you do not specify the suffix, systemctl will assume .service. For example, netctl and netctl.service are equivalent.
Mount points will automatically be translated into the appropriate .mount unit. For example, specifying /home is equivalent to home.mount.
Similar to mount points, devices are automatically translated into the appropriate .device unit, therefore specifying /dev/sda2 is equivalent to dev-sda2.device.
See systemd.unit(5) for details.

Note: Some unit names contain an @ sign (e.g. name@string.service): this means that they are instances of a template unit, whose actual file name does not contain the string part (e.g. name@.service). string is called the instance identifier, and is similar to an argument that is passed to the template unit when called with the systemctl command: in the unit file it will substitute the %i specifier. To be more accurate, before trying to instantiate the name@.suffix template unit, systemd will actually look for a unit with the exact name@string.suffix file name, although by convention such a "clash" happens rarely, i.e. most unit files containing an @ sign are meant to be templates. Also, if a template unit is called without an instance identifier, it will generally fail (except with certain systemctl commands, like cat).
The commands in the below table operate on system units since --system is the implied default for systemctl. To instead operate on user units (for the calling user), use systemctl --user without root privileges. See also systemd/User#Basic setup to enable/disable user units for all users.

Tip:
Most commands also work if multiple units are specified, see systemctl(1) for more information.
The --now switch can be used in conjunction with enable, disable, and mask to respectively start, stop, or mask the unit immediately rather than after rebooting.
A package may offer units for different purposes. If you just installed a package, pacman -Qql package | grep -Fe .service -e .socket can be used to check and find them.
Action	Command	Note
Analyzing the system state
Show system status	systemctl status	
List running units	systemctl or
systemctl list-units	
List failed units	systemctl --failed	
List installed unit files1	systemctl list-unit-files	
Show process status for a PID	systemctl status pid	cgroup slice, memory and parent
Checking the unit status
Show a manual page associated with a unit	systemctl help unit	as supported by the unit
Status of a unit	systemctl status unit	including whether it is running or not
Check whether a unit is enabled	systemctl is-enabled unit	
Starting, restarting, reloading a unit
Start a unit immediately	systemctl start unit as root	
Stop a unit immediately	systemctl stop unit as root	
Restart a unit	systemctl restart unit as root	
Reload a unit and its configuration	systemctl reload unit as root	
Reload systemd manager configuration2	systemctl daemon-reload as root	scan for new or changed units
Enabling a unit
Enable a unit to start automatically at boot	systemctl enable unit as root	
Enable a unit to start automatically at boot and start it immediately	systemctl enable --now unit as root	
Disable a unit to no longer start at boot	systemctl disable unit as root	
Reenable a unit3	systemctl reenable unit as root	i.e. disable and enable anew
Masking a unit
Mask a unit to make it impossible to start4	systemctl mask unit as root	
Unmask a unit	systemctl unmask unit as root	
See systemd.unit(5) § UNIT FILE LOAD PATH for the directories where available unit files can be found.
This does not ask the changed units to reload their own configurations (see the Reload action).
For example, in case its [Install] section has changed since last enabling it.
Both manually and as a dependency, which makes masking dangerous. Check for existing masked units with:
$ systemctl list-unit-files --state=masked
Power management
polkit is necessary for power management as an unprivileged user. If you are in a local systemd-logind user session and no other session is active, the following commands will work without root privileges. If not (for example, because another user is logged into a tty), systemd will automatically ask you for the root password.

Action	Command
Shut down and reboot the system	systemctl reboot
Shut down and power-off the system	systemctl poweroff
Suspend the system	systemctl suspend
Put the system into hibernation	systemctl hibernate
Put the system into hybrid-sleep state (or suspend-to-both)	systemctl hybrid-sleep
Writing unit files
The syntax of systemd's unit files (systemd.unit(5)) is inspired by XDG Desktop Entry Specification .desktop files, which are in turn inspired by Microsoft Windows .ini files. Unit files are loaded from multiple locations (to see the full list, run systemctl show --property=UnitPath), but the main ones are (listed from lowest to highest precedence):

/usr/lib/systemd/system/: units provided by installed packages
/etc/systemd/system/: units installed by the system administrator
Note:
The load paths are completely different when running systemd in user mode.
systemd unit names may only contain ASCII alphanumeric characters, underscores and periods. All other characters must be replaced by C-style "\x2d" escapes, or employ their predefined semantics ('@', '-'). See systemd.unit(5) and systemd-escape(1) for more information.
Look at the units installed by your packages for examples, as well as systemd.service(5) § EXAMPLES.

Tip: Comments prepended with # may be used in unit-files as well, but only in new lines. Do not use end-line comments after systemd parameters or the unit will fail to activate.
Handling dependencies
With systemd, dependencies can be resolved by designing the unit files correctly. The most typical case is when unit A requires unit B to be running before A is started. In that case add Requires=B and After=B to the [Unit] section of A. If the dependency is optional, add Wants=B and After=B instead. Note that Wants= and Requires= do not imply After=, meaning that if After= is not specified, the two units will be started in parallel.

Dependencies are typically placed on services and not on \#Targets. For example, network.target is pulled in by whatever service configures your network interfaces, therefore ordering your custom unit after it is sufficient since network.target is started anyway.

Service types
There are several different start-up types to consider when writing a custom service file. This is set with the Type= parameter in the [Service] section:

Type=simple (default): systemd considers the service to be started up immediately. The process must not fork. Do not use this type if other services need to be ordered on this service, unless it is socket activated.
Type=forking: systemd considers the service started up once the process forks and the parent has exited. For classic daemons, use this type unless you know that it is not necessary. You should specify PIDFile= as well so systemd can keep track of the main process.
Type=oneshot: this is useful for scripts that do a single job and then exit. You may want to set RemainAfterExit=yes as well so that systemd still considers the service as active after the process has exited. Setting RemainAfterExit=yes is appropriate for the units which change the system state (e.g., mount some partition).
Type=notify: identical to Type=simple, but with the stipulation that the daemon will send a signal to systemd when it is ready. The reference implementation for this notification is provided by libsystemd-daemon.so.
Type=dbus: the service is considered ready when the specified BusName appears on DBus's system bus.
Type=idle: systemd will delay execution of the service binary until all jobs are dispatched. Other than that behavior is very similar to Type=simple.
See the systemd.service(5) § OPTIONS man page for a more detailed explanation of the Type values.

Editing provided units
To avoid conflicts with pacman, unit files provided by packages should not be directly edited. There are two safe ways to modify a unit without touching the original file: create a new unit file which overrides the original unit or create drop-in snippets which are applied on top of the original unit. For both methods, you must reload the unit afterwards to apply your changes. This can be done either by editing the unit with systemctl edit (which reloads the unit automatically) or by reloading all units with:
~~~
$ systemctl daemon-reload
~~~
Tip:
You can use systemd-delta to see which unit files have been overridden or extended and what exactly has been changed.
Use systemctl cat unit to view the content of a unit file and all associated drop-in snippets.
Replacement unit files
To replace the unit file /usr/lib/systemd/system/unit, create the file /etc/systemd/system/unit and reenable the unit to update the symlinks.

Alternatively, run:

~~~
$ systemctl edit --full unit
~~~
This opens /etc/systemd/system/unit in your editor (copying the installed version if it does not exist yet) and automatically reloads it when you finish editing.

Note: The replacement units will keep on being used even if Pacman updates the original units in the future. This method makes system maintenance more difficult and therefore the next approach is preferred.
Drop-in files
To create drop-in files for the unit file /usr/lib/systemd/system/unit, create the directory /etc/systemd/system/unit.d/ and place .conf files there to override or add new options. systemd will parse and apply these files on top of the original unit.

The easiest way to do this is to run:

$ systemctl edit unit --drop-in=drop_in_name
This opens the file /etc/systemd/system/unit.d/drop_in_name.conf in your text editor (creating it if necessary) and automatically reloads the unit when you are done editing. Omitting --drop-in= option will result in systemd using the default file name override.conf .

Note:
The key must be still placed in the appropriate section in the override file.
Not all keys can be overridden with drop-in files. For example, for changing Conflicts= a replacement file is necessary.
Revert to vendor version
To revert any changes to a unit made using systemctl edit do:

$ systemctl revert unit
Examples
For example, if you simply want to add an additional dependency to a unit, you may create the following file:

~~~
/etc/systemd/system/unit.d/customdependency.conf
[Unit]
Requires=new dependency
After=new dependency
~~~
As another example, in order to replace the ExecStart directive, create the following file:

~~~
/etc/systemd/system/unit.d/customexec.conf
[Service]
ExecStart=
ExecStart=new command
~~~
Note how ExecStart must be cleared before being re-assigned [1]. The same holds for every item that can be specified multiple times, e.g. OnCalendar for timers.

One more example to automatically restart a service:

~~~
/etc/systemd/system/unit.d/restart.conf
[Service]
Restart=always
RestartSec=30
Targets
~~~
systemd uses targets to group units together via dependencies and as standardized synchronization points. They serve a similar purpose as runlevels but act a little differently. Each target is named instead of numbered and is intended to serve a specific purpose with the possibility of having multiple ones active at the same time. Some targets are implemented by inheriting all of the services of another target and adding additional services to it. There are systemd targets that mimic the common SystemVinit runlevels so you can still switch targets using the familiar telinit RUNLEVEL command.

Get current targets
The following should be used under systemd instead of running runlevel:

~~~
$ systemctl list-units --type=target
~~~
Create custom target
The runlevels that held a defined meaning under sysvinit (i.e., 0, 1, 3, 5, and 6); have a 1:1 mapping with a specific systemd target. Unfortunately, there is no good way to do the same for the user-defined runlevels like 2 and 4. If you make use of those it is suggested that you make a new named systemd target as /etc/systemd/system/your target that takes one of the existing runlevels as a base (you can look at /usr/lib/systemd/system/graphical.target as an example), make a directory /etc/systemd/system/your target.wants, and then symlink the additional services from /usr/lib/systemd/system/ that you wish to enable.

Mapping between SysV runlevels and systemd targets
SysV Runlevel	systemd Target	Notes
0	runlevel0.target, poweroff.target	Halt the system.
1, s, single	runlevel1.target, rescue.target	Single user mode.
2, 4	runlevel2.target, runlevel4.target, multi-user.target	User-defined/Site-specific runlevels. By default, identical to 3.
3	runlevel3.target, multi-user.target	Multi-user, non-graphical. Users can usually login via multiple consoles or via the network.
5	runlevel5.target, graphical.target	Multi-user, graphical. Usually has all the services of runlevel 3 plus a graphical login.
6	runlevel6.target, reboot.target	Reboot
emergency	emergency.target	Emergency shell
Change current target
In systemd, targets are exposed via target units. You can change them like this:

~~~
$ systemctl isolate graphical.target
~~~
This will only change the current target, and has no effect on the next boot. This is equivalent to commands such as telinit 3 or telinit 5 in Sysvinit.

Change default target to boot into
The standard target is default.target, which is a symlink to graphical.target. This roughly corresponds to the old runlevel 5.

To verify the current target with systemctl:

$ systemctl get-default
To change the default target to boot into, change the default.target symlink. With systemctl:

$ systemctl set-default multi-user.target
Removed /etc/systemd/system/default.target.
Created symlink /etc/systemd/system/default.target -> /usr/lib/systemd/system/multi-user.target.
Alternatively, append one of the following kernel parameters to your boot loader:

systemd.unit=multi-user.target (which roughly corresponds to the old runlevel 3),
systemd.unit=rescue.target (which roughly corresponds to the old runlevel 1).
Default target order
systemd chooses the default.target according to the following order:

Kernel parameter shown above
Symlink of /etc/systemd/system/default.target
Symlink of /usr/lib/systemd/system/default.target
systemd components
Some (not exhaustive) components of systemd are:

kernel-install — a script used to automatically move kernels and their respective initramfs images to the boot partition;
systemd-boot — simple UEFI boot manager;
systemd-creds — to securely store and retrieve credentials used by systemd units;
systemd-cryptenroll(1) — Enroll PKCS#11, FIDO2, TPM2 token/devices to LUKS2 encrypted volumes;
systemd-firstboot — basic system setting initialization before first boot;
systemd-homed — portable human-user accounts;
systemd-logind(8) — session management;
systemd-networkd — network configuration management;
systemd-nspawn — light-weight namespace container;
systemd-resolved — network name resolution;
systemd-stub(7) — a UEFI boot stub used for creating unified kernel images;
systemd-sysusers(8) — creates system users and groups and adds users to groups at package installation or boot time;
systemd-timesyncd — system time synchronization across the network;
systemd/Journal — system logging;
systemd/Timers — monotonic or realtime timers for controlling .service files or events, reasonable alternative to cron.
systemd.mount - mounting
systemd is in charge of mounting the partitions and filesystems specified in /etc/fstab. The systemd-fstab-generator(8) translates all the entries in /etc/fstab into systemd units; this is performed at boot time and whenever the configuration of the system manager is reloaded.

systemd extends the usual fstab capabilities and offers additional mount options. These affect the dependencies of the mount unit. They can, for example, ensure that a mount is performed only once the network is up or only once another partition is mounted. The full list of specific systemd mount options, typically prefixed with x-systemd., is detailed in systemd.mount(5) § FSTAB.

An example of these mount options is automounting, which means mounting only when the resource is required rather than automatically at boot time. This is provided in fstab#Automount with systemd.

GPT partition automounting
On UEFI-booted systems, if specific conditions are met, systemd-gpt-auto-generator(8) will automount GPT partitions following the Discoverable Partitions Specification. Automounted partitions can thus be omitted from fstab, and if the root partition is automounted, then root= can be omitted from the kernel command line.

The prerequisites are:

The boot loader must set the LoaderDevicePartUUID EFI variable, so that the used EFI system partition can be identified. This is supported by systemd-boot, systemd-stub(7), GRUB (with grub-mkconfig generated grub.cfg; custom grub.cfg requires loading the bli module) and rEFInd (not enabled by default). This can be verified by running bootctl and checking the status of Boot loader sets ESP information.
The root partition must be on the same physical disk as the used EFI system partition. Other partitions that will be automounted must be on the same physical disk as the root partition. This basically means that all automounted partitions must share the same physical disk with the ESP.
The /efi mount point must be manually created (if desired), otherwise systemd-gpt-auto-generator will use /boot.
Warning: Be very careful when creating /efi on an existing system when using GPT automounting. /efi will be used as the default mount point on the next boot, which might leave your system in an inconsistent state with an empty /boot directory. You will most likely need to reinstall your kernel(s) and/or microcode, regenerate your initramfs, etc..
Tip: The automounting of a partition can be disabled by changing the partition's type GUID or setting the partition attribute bit 63 "do not automount", see gdisk#Prevent GPT partition automounting.
/var
For /var automounting to work, the PARTUUID must match the SHA256 HMAC hash of the partition type UUID (4d21b016-b534-45c2-a9fb-5c16e091fd2d) keyed by the machine ID. The required PARTUUID can be obtained using:

$ systemd-id128 -u --app-specific=4d21b016-b534-45c2-a9fb-5c16e091fd2d machine-id
Note: systemd-id128(1) reads the machine ID from /etc/machine-id, this makes it impossible to know the needed PARTUUID before the system is installed.
systemd-sysvcompat
The primary role of systemd-sysvcompat (required by base) is to provide the traditional linux init binary. For systemd-controlled systems, init is just a symbolic link to its systemd executable.

In addition, it provides four convenience shortcuts that SysVinit users might be used to. The convenience shortcuts are halt(8), poweroff(8), reboot(8) and shutdown(8). Each one of those four commands is a symbolic link to systemctl, and is governed by systemd behavior. Therefore, the discussion at #Power management applies.

systemd-based systems can give up those System V compatibility methods by using the init= boot parameter (see, for example, /bin/init is in systemd-sysvcompat ?) and systemd native systemctl command arguments.

systemd-tmpfiles - temporary files
systemd-tmpfiles creates, deletes and cleans up volatile and temporary files and directories. It reads configuration files in /etc/tmpfiles.d/ and /usr/lib/tmpfiles.d/ to discover which actions to perform. Configuration files in the former directory take precedence over those in the latter directory.

Configuration files are usually provided together with service files, and they are named in the style of /usr/lib/tmpfiles.d/program.conf. For example, the Samba daemon expects the directory /run/samba to exist and to have the correct permissions. Therefore, the samba package ships with this configuration:

~~~
/usr/lib/tmpfiles.d/samba.conf
D /run/samba 0755 root root
~~~
Configuration files may also be used to write values into certain files on boot. For example, if you used /etc/rc.local to disable wakeup from USB devices with echo USBE > /proc/acpi/wakeup, you may use the following tmpfile instead:
~~~
/etc/tmpfiles.d/disable-usb-wake.conf
#    Path                  Mode UID  GID  Age Argument
w    /proc/acpi/wakeup     -    -    -    -   USBE
~~~
See the systemd-tmpfiles(8) and tmpfiles.d(5) man pages for details.

Note: This method may not work to set options in /sys since the systemd-tmpfiles-setup service may run before the appropriate device modules are loaded. In this case, you could check whether the module has a parameter for the option you want to set with modinfo module and set this option with a config file in /etc/modprobe.d. Otherwise, you will have to write a udev rule to set the appropriate attribute as soon as the device appears.
Tips and tricks
Tango-view-fullscreen.pngThis article or section needs expansion.Tango-view-fullscreen.png

Reason: We should document explicitly somewhere the benefits of socket activation over dumb service starting. This is mentioned briefly at the beginning of the page and in related pages like Avahi. (Discuss in Talk:Systemd)
GUI configuration tools
systemadm — Graphical browser for systemd units. It can show the list of units, possibly filtered by type.
https://cgit.freedesktop.org/systemd/systemd-ui/ || systemd-ui
SystemdGenie — systemd management utility based on KDE technologies.
https://invent.kde.org/system/systemdgenie || systemdgenie
Running services after the network is up
To delay a service until after the network is up, include the following dependencies in the .service file:

/etc/systemd/system/foo.service
[Unit]
...
Wants=network-online.target
After=network-online.target
...
The network wait service of the network manager in use must also be enabled so that network-online.target properly reflects the network status.

If using NetworkManager, NetworkManager-wait-online.service should be enabled together with NetworkManager.service. Check if this is the case with systemctl is-enabled NetworkManager-wait-online.service. If it is not enabled, then reenable NetworkManager.service.
In the case of netctl, enable the netctl-wait-online.service (unless you are using netctl-auto; see FS#75836).
If using systemd-networkd, systemd-networkd-wait-online.service should be enabled together with systemd-networkd.service. Check if this is the case with systemctl is-enabled systemd-networkd-wait-online.service. If it is not enabled, then reenable systemd-networkd.service.
For more detailed explanations, see the discussion in the Network configuration synchronization points.

If a service needs to perform DNS queries, it should additionally be ordered after nss-lookup.target:

~~~
/etc/systemd/system/foo.service
[Unit]
...
Wants=network-online.target
After=network-online.target nss-lookup.target
...
~~~
See systemd.special(7) § Special Passive System Units.

For nss-lookup.target to have any effect it needs a service that pulls it in via Wants=nss-lookup.target and orders itself before it with Before=nss-lookup.target. Typically this is done by local DNS resolvers.

Check which active service, if any, is pulling in nss-lookup.target with:

$ systemctl list-dependencies --reverse nss-lookup.target
Enable installed units by default
Tango-view-fullscreen.pngThis article or section needs expansion.Tango-view-fullscreen.png

Reason: How does it work with instantiated units? (Discuss in Talk:Systemd)
Arch Linux ships with /usr/lib/systemd/system-preset/99-default.preset containing disable *. This causes systemctl preset to disable all units by default, such that when a new package is installed, the user must manually enable the unit.

If this behavior is not desired, simply create a symlink from /etc/systemd/system-preset/99-default.preset to /dev/null in order to override the configuration file. This will cause systemctl preset to enable all units that get installed—regardless of unit type—unless specified in another file in one systemctl preset's configuration directories. User units are not affected. See systemd.preset(5) for more information.

Note: Enabling all units by default may cause problems with packages that contain two or more mutually exclusive units. systemctl preset is designed to be used by distributions and spins or system administrators. In the case where two conflicting units would be enabled, you should explicitly specify which one is to be disabled in a preset configuration file as specified in the systemd.preset(5) man page.
Sandboxing application environments
Tango-go-next.pngThis article or section is a candidate for moving to systemd/Sandboxing.Tango-go-next.png

Notes: The topic is broad enough for a dedicated page. See User:NetSysFire/systemd sandboxing for a proposed draft. (Discuss in Talk:Security#systemd unit hardening and system.conf tweaks)
A unit file can be created as a sandbox to isolate applications and their processes within a hardened virtual environment. systemd leverages namespaces, a list of allowed/denied capabilities, and control groups to container processes through an extensive execution environment configuration—systemd.exec(5).

The enhancement of an existing systemd unit file with application sandboxing typically requires trial-and-error tests accompanied by the generous use of strace, stderr and journalctl(1) error logging and output facilities. You may want to first search upstream documentation for already done tests to base trials on. To get a starting point for possible hardening options, run

$ systemd-analyze security unit
Some examples of how sandboxing with systemd can be deployed:

CapabilityBoundingSet defines a list of capabilities(7) that are allowed or denied for a unit. See systemd.exec(5) § CAPABILITIES.
The CAP_SYS_ADM capability, for example, which should be one of the goals of a secure sandbox: CapabilityBoundingSet=~ CAP_SYS_ADM
Notifying about failed services
In order to notify about service failures, a OnFailure= directive needs to be added to the according service file, for example by using a drop-in configuration file. Adding this directive to every service unit can be achieved with a top-level drop-in configuration file. For details about top-level drop-ins, see systemd.unit(5).

Create a top-level drop-in for services:

/etc/systemd/system/service.d/toplevel-override.conf
[Unit]
OnFailure=failure-notification@%n
This adds OnFailure=failure-notification@%n to every service file. If some_service_unit fails, failure-notification@some_service_unit will be started to handle the notification delivery (or whatever task it is configured to perform).

Create the failure-notification@ template unit:

/etc/systemd/system/failure-notification@.service
[Unit]
Description=Send a notification about a failed systemd unit
After=network.target

[Service]
Type=simple
ExecStart=/path/to/failure-notification.sh %i
You can create the failure-notification.sh script and define what to do or how to notify (mail, gotify, xmpp, etc.). The %i will be the name of the failed service unit and will be passed as argument to the script.

In order to prevent a recursion for starting instances of failure-notification@.service again and again if the start fails, create an empty drop-in configuration file with the same name as the top-level drop-in (the empty service-level drop-in configuration file takes precedence over the top-level drop-in and overrides the latter one):

~~~
# mkdir -p /etc/systemd/system/failure-notification@.service.d
# touch /etc/systemd/system/failure-notification@.service.d/toplevel-override.conf
~~~
Automatically turn off an external HDD at shutdown
Merge-arrows-2.pngThis article or section is a candidate for merging with Udisks#Troubleshooting.Merge-arrows-2.png

Notes: This is mainly centered around udisks usage, it is probably a better fit for this content instead of shoving it inside the systemd page. (Discuss in Talk:Systemd)
If an external HDD is not powered off properly at system shutdown, it may be desirable to fix the issue. The most convenient way to do this is using udisks.

Enable udisks2.service.

A service to invoke our script might look like so:

~~~
/etc/systemd/system/handle_external_hdds.service
[Unit]
Requires=udisks2.service
Requires=graphical.target
After=graphical.target
[Service]
Type=oneshot
RemainAfterExit=yes
ExecStop=/usr/local/bin/handle_external_hdds.sh
[Install]
WantedBy=graphical.target
~~~
Enable handle_external_hdds.service

Do a systemd daemon-reload to apply the new setting.

Reboot or restart graphical.target to check if works.

An example script to handle an arbitrary amount of partitions on a single disk looks like so:

~~~
/usr/local/bin/handle_external_hdds.sh
#!/bin/bash -u

declare -a uuids=(uuid_list)

# Only proceed if the drive is present.
if [[ ! -L "/dev/disk/by-uuid/${uuids[0]}" ]]; then
  exit 0
fi

for uuid in "${uuids[@]}"; do
  if findmnt "/dev/disk/by-uuid/$uuid"; then
    umount "/dev/disk/by-uuid/$uuid"
  fi
done

# udisksctl powers off proper drive even if its partition is supplied
udisksctl power-off -b "/dev/disk/by-uuid/${uuids[0]}"
uuid_list is a list of space delimited UUIDs corresponding to partitions of the device to check, e.g. "uuid_1" "uuid_2".

~~~
Troubleshooting
Investigating failed services
To find the systemd services which failed to start:

~~~
$ systemctl --state=failed
~~~
To find out why they failed, examine their log output. See systemd/Journal#Filtering output for details.

Diagnosing boot problems
systemd has several options for diagnosing problems with the boot process. See boot debugging for more general instructions and options to capture boot messages before systemd takes over the boot process. Also see freedesktop.org's systemd debugging documentation.

Diagnosing a service
If some systemd service misbehaves or you want to get more information about what is happening, set the SYSTEMD_LOG_LEVEL environment variable to debug. For example, to run the systemd-networkd daemon in debug mode:

Add a drop-in file for the service adding the two lines:

~~~
[Service]
Environment=SYSTEMD_LOG_LEVEL=debug
Or equivalently, set the environment variable manually:

# SYSTEMD_LOG_LEVEL=debug /lib/systemd/systemd-networkd
~~~
then restart systemd-networkd and watch the journal for the service with the -f/--follow option.

Shutdown/reboot takes terribly long
If the shutdown process takes a very long time (or seems to freeze), most likely a service not exiting is to blame. systemd waits some time for each service to exit before trying to kill it. To find out whether you are affected, see Shutdown completes eventually in the systemd wiki.

A common problem is a stalled shutdown or suspend process. To verify whether that is the case, you could run either of these commands and check the outputs

~~~
$ systemctl poweroff
Failed to power off system via logind: There's already a shutdown or sleep operation in progress
$ systemctl list-jobs
  JOB UNIT                    TYPE  STATE  
...
21593 systemd-suspend.service start running
21592 suspend.target          start waiting
..
~~~
The solution to this would be to cancel these jobs by running

~~~
$ systemctl cancel
$ systemctl stop systemd-suspend.service
~~~
and then trying shutdown or reboot again.

Short lived processes do not seem to log any output
If running journalctl -u foounit as root does not show any output for a short lived service, look at the PID instead. For example, if systemd-modules-load.service fails, and systemctl status systemd-modules-load shows that it ran as PID 123, then you might be able to see output in the journal for that PID, i.e. by running journalctl -b _PID=123 as root. Metadata fields for the journal such as _SYSTEMD_UNIT and _COMM are collected asynchronously and rely on the /proc directory for the process existing. Fixing this requires fixing the kernel to provide this data via a socket connection, similar to SCM_CREDENTIALS. In short, it is a bug. Keep in mind that immediately failed services might not print anything to the journal as per design of systemd.

Boot time increasing over time
Tango-inaccurate.pngThe factual accuracy of this article or section is disputed.Tango-inaccurate.png

Reason: NetworkManager issues are not systemd's fault, the alleged reports are missing. Slow systemctl status or journalctl do not affect boot time. (Discuss in Talk:Systemd)
After using systemd-analyze a number of users have noticed that their boot time has increased significantly in comparison with what it used to be. After using systemd-analyze blame NetworkManager is being reported as taking an unusually large amount of time to start.

The problem for some users has been due to /var/log/journal becoming too large. This may have other impacts on performance, such as for systemctl status or journalctl. As such the solution is to remove every file within the folder (ideally making a backup of it somewhere, at least temporarily) and then setting a journal file size limit as described in Systemd/Journal#Journal size limit.

systemd-tmpfiles-setup.service fails to start at boot
Starting with systemd 219, /usr/lib/tmpfiles.d/systemd.conf specifies ACL attributes for directories under /var/log/journal and, therefore, requires ACL support to be enabled for the filesystem the journal resides on.

See Access Control Lists#Enable ACL for instructions on how to enable ACL on the filesystem that houses /var/log/journal.

Disable emergency mode on remote machine
You may want to disable emergency mode on a remote machine, for example, a virtual machine hosted at Azure or Google Cloud. It is because if emergency mode is triggered, the machine will be blocked from connecting to network.

To disable it, mask emergency.service and emergency.target.

See also
Wikipedia:systemd
Official web site
systemd optimizations
systemd FAQ
systemd Tips and tricks
systemd(1)
Other distributions
Gentoo:Systemd
Fedora:Systemd
Fedora:How to debug Systemd problems
Fedora:SysVinit to Systemd Cheatsheet
Debian:systemd
Lennart's blog story, update 1, update 2, update 3, summary
Debug Systemd Services
systemd for Administrators (PDF)
How To Use Systemctl to Manage Systemd Services and Units
Session management with systemd-logind
Emacs Syntax highlighting for Systemd files
Two part introductory article in The H Open magazine.
Category: Init
This page was last edited on 21 June 2023, at 11:55.
Content is available under GNU Free Documentation License 1.3 or later unless otherwise noted.
Privacy policyAbout ArchWikiDisclaimers

### 6.7. systemd-tmpfiles-setup.service fails to start at boot

Starting with _systemd_ 219, `/usr/lib/tmpfiles.d/systemd.conf` specifies ACL attributes for directories under `/var/log/journal` and, therefore, requires ACL support to be enabled for the filesystem the journal resides on.

See [Access Control Lists#Enable ACL](https://wiki.archlinux.org/title/Access_Control_Lists#Enable_ACL "Access Control Lists") for instructions on how to enable ACL on the filesystem that houses `/var/log/journal`.

### 6.8. Disable emergency mode on remote machine

You may want to disable emergency mode on a remote machine, for example, a virtual machine hosted at Azure or Google Cloud. It is because if emergency mode is triggered, the machine will be blocked from connecting to network.

To disable it, [mask](https://wiki.archlinux.org/title/Systemd#Using_units) `emergency.service` and `emergency.target`.

## 7. See also

- [Wikipedia:systemd](https://en.wikipedia.org/wiki/systemd "wikipedia:systemd")
- [Official web site](https://systemd.io/)
    - [systemd optimizations](https://www.freedesktop.org/wiki/Software/systemd/Optimizations)
    - [systemd FAQ](https://www.freedesktop.org/wiki/Software/systemd/FrequentlyAskedQuestions)
    - [systemd Tips and tricks](https://www.freedesktop.org/wiki/Software/systemd/TipsAndTricks)
- [systemd(1)](https://man.archlinux.org/man/systemd.1)
- Other distributions
    - [Gentoo:Systemd](https://wiki.gentoo.org/wiki/Systemd "gentoo:Systemd")
    - [Fedora:Systemd](https://fedoraproject.org/wiki/Systemd "fedora:Systemd")
    - [Fedora:How to debug Systemd problems](https://fedoraproject.org/wiki/How_to_debug_Systemd_problems "fedora:How to debug Systemd problems")
    - [Fedora:SysVinit to Systemd Cheatsheet](https://fedoraproject.org/wiki/SysVinit_to_Systemd_Cheatsheet "fedora:SysVinit to Systemd Cheatsheet")
    - [Debian:systemd](https://wiki.debian.org/systemd "debian:systemd")
- [Lennart's blog story](http://0pointer.de/blog/projects/systemd.html), [update 1](http://0pointer.de/blog/projects/systemd-update.html), [update 2](http://0pointer.de/blog/projects/systemd-update-2.html), [update 3](http://0pointer.de/blog/projects/systemd-update-3.html), [summary](http://0pointer.de/blog/projects/why.html)
- [Debug Systemd Services](https://containersolutions.github.io/runbooks/posts/linux/debug-systemd-service-units)
- [systemd for Administrators (PDF)](http://0pointer.de/public/systemd-ebook-psankar.pdf)
- [How To Use Systemctl to Manage Systemd Services and Units](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)
- [Session management with systemd-logind](https://dvdhrm.wordpress.com/2013/08/24/session-management-on-linux/)
- [Emacs Syntax highlighting for Systemd files](https://wiki.archlinux.org/title/Emacs#Syntax_highlighting_for_systemd_Files "Emacs")
- [Two](http://www.h-online.com/open/features/Control-Centre-The-systemd-Linux-init-system-1565543.html) [part](http://www.h-online.com/open/features/Booting-up-Tools-and-tips-for-systemd-1570630.html) introductory article in _The H Open_ magazine.

[Category](https://wiki.archlinux.org/title/Special:Categories "Special:Categories"): 

- [Init](https://wiki.archlinux.org/title/Category:Init "Category:Init")

- This page was last edited on 21 June 2023, at 11:55.
- Content is available under [GNU Free Documentation License 1.3 or later](https://www.gnu.org/copyleft/fdl.html) unless otherwise noted.

- [Privacy policy](https://terms.archlinux.org/docs/privacy-policy/)
- [About ArchWiki](https://wiki.archlinux.org/title/ArchWiki:About)
- [Disclaimers](https://wiki.archlinux.org/title/ArchWiki:General_disclaimer)