---
date: 2023-08-10 23:53
title: archlinux systemd User
tags:
- systemd-user-service
- systemd
---

https://wiki.archlinux.org/title/Systemd/User

Related articles

- [systemd](https://wiki.archlinux.org/title/Systemd "Systemd")
- [Automatic login to virtual console](https://wiki.archlinux.org/title/Automatic_login_to_virtual_console "Automatic login to virtual console")[[getty#Automatic login to virtual console]]
- [Start X at login](https://wiki.archlinux.org/title/Start_X_at_login "Start X at login")

[systemd](https://wiki.archlinux.org/title/Systemd "Systemd") offers the ability to manage services under the user's control with a per-user systemd instance, enabling them to start, stop, enable, and disable their own **user units**. This is convenient for daemons and other services that are commonly run for a single user, such as [mpd](https://wiki.archlinux.org/title/Mpd "Mpd"), or to perform automated tasks like fetching mail.

## 1. How it works

As per default configuration in `/etc/pam.d/system-login`, <mark style="background: #35E708E3;">the `pam_systemd` module automatically launches a `systemd --user` instance when the user logs in for the first time. </mark>This process will survive as long as there is some session for that user, and will be killed as soon as the last session for the user is closed. When [[#Automatic start-up of systemd user instances]] is enabled, <mark style="background: #D2B3FFA6;">the instance is started on boot and will not be killed. The systemd user instance is responsible for managing user services, which can be used to run daemons or automated tasks with all the benefits of systemd, such as socket activation, timers, dependency system, and strict process control via cgroups.</mark>

Similar to system units, user units are located in the following directories (ordered by ascending precedence):

- `/usr/lib/systemd/user/` where units provided by installed packages belong.
- `~/.local/share/systemd/user/` where units of packages that have been installed in the home directory belong.
- `/etc/systemd/user/` where system-wide user units are placed by the system administrator.
- `~/.config/systemd/user/` where the user puts their own units.

<mark style="background: #D2B3FFA6;">When a systemd user instance starts, it brings up the per user target `default.target`. Other units can be controlled manually with `systemctl --user`.</mark> See [systemd.special(7) § UNITS MANAGED BY THE USER SERVICE MANAGER](https://man.archlinux.org/man/systemd.special.7#UNITS_MANAGED_BY_THE_USER_SERVICE_MANAGER).[[archlinux-wiki-systemd.special - Special systemd units#5. UNITS MANAGED BY THE USER SERVICE MANAGER]]

**Note:**

- <mark style="background: #ABF7F7A6;">Be aware that the `systemd --user` instance is a per-user process, and not per-session. The rationale is that most resources handled by user services, like sockets or state files will be per-user (live on the user's home directory) and not per session.</mark> This means that all user services run outside of a session. As a consequence, programs that need to be run inside a session will probably break in user services. The way systemd handles user sessions is pretty much in flux. See [[1]](https://mail.gnome.org/archives/desktop-devel-list/2014-January/msg00079.html) and [[2]](https://lists.freedesktop.org/archives/systemd-devel/2014-March/017552.html) for some hints on where things are going.
- `systemd --user` runs as a separate process from the `systemd --system` process. User units can not reference or depend on system units or units of other users.

## 2. Basic setup

All the user units will be placed in `~/.config/systemd/user/`. If you want to start units on first login, execute `systemctl --user enable unit` for any unit you want to be autostarted.

**Tip:** If you want to enable a unit for all users rather than the user executing the _systemctl_ command, run `systemctl --global enable unit` as root. Similarly for `disable`.

### 2.1. Environment variables

The user instance of systemd does not inherit any of the [environment variables](https://wiki.archlinux.org/title/Environment_variables "Environment variables") set in places like `.bashrc` etc. There are several ways to set environment variables for the systemd user instance:

- For users with a `$HOME` directory, create a _.conf_ file in the `~/.config/environment.d/` directory with lines of the form `NAME=VAL`. Affects only that user's user unit. See [environment.d(5)](https://man.archlinux.org/man/environment.d.5) for more information.
- Use the `DefaultEnvironment` option in `/etc/systemd/user.conf` file. Affects all user units.
- Add a drop-in configuration file in `/etc/systemd/system/user@.service.d/`. Affects all user units; see [#Service example](https://wiki.archlinux.org/title/systemd/User#Service_example)
- At any time, use `systemctl --user set-environment` or `systemctl --user import-environment`. Affects all user units started after setting the environment variables, but not the units that were already running.
- Using the `dbus-update-activation-environment --systemd --all` command provided by [dbus](https://wiki.archlinux.org/title/Dbus "Dbus"). Has the same effect as `systemctl --user import-environment`, but also affects the D-Bus session. You can add this to the end of your shell initialization file.
- For "global" environment variables for the user environment you can use the `environment.d` directories which are parsed by some generators. See [environment.d(5)](https://man.archlinux.org/man/environment.d.5) and [systemd.generator(7)](https://man.archlinux.org/man/systemd.generator.7) for more information.
- You can also write a [systemd.environment-generator(7)](https://man.archlinux.org/man/systemd.environment-generator.7) script which can produce environment variables that vary from user to user, this is probably the best way if you need per-user environments (this is the case for `XDG_RUNTIME_DIR`, `DBUS_SESSION_BUS_ADDRESS`, etc).

One variable you may want to set is `PATH`.

After configuration, the command `systemctl --user show-environment` can be used to verify that the values are correct.

#### 2.1.1. Service example

Create the [drop-in](https://wiki.archlinux.org/title/Systemd#Drop-in_files "Systemd") directory `/etc/systemd/system/user@.service.d/` and inside create a file that has the extension _.conf_ (e.g. `local.conf`):

****/etc/systemd/system/user@.service.d/local.conf
~~~
[Service]
Environment="PATH=/usr/lib/ccache/bin:/usr/local/bin:/usr/bin:/bin"
Environment="EDITOR=nano -c"
Environment="BROWSER=firefox"
Environment="NO_AT_BRIDGE=1"
~~~

#### 2.1.2. DISPLAY and XAUTHORITY

`DISPLAY` is used by any X application to know which display to use and `XAUTHORITY` to provide a path to the user's <mark style="background: #ABF7F7A6;">`.Xauthority` file and thus the cookie needed to access the X server.</mark> If you plan on launching X applications from systemd units, these variables need to be set. <mark style="background: #ABF7F7A6;">Systemd provides a script in `/etc/X11/xinit/xinitrc.d/50-systemd-user.sh` to import those variables into the systemd user session on X launch.</mark> [[3]](https://github.com/systemd/systemd/blob/v219/NEWS#L194) So unless you start X in a nonstandard way, user services should be aware of the `DISPLAY` and `XAUTHORITY`.

#### 2.1.3. PATH

If you customize your `PATH` and plan on launching applications that make use of it from systemd units, you should make sure the modified `PATH` is set on the systemd environment. Assuming you set your `PATH` in `.bash_profile`, the best way to make systemd aware of your modified `PATH` is by adding the following to `.bash_profile` after the `PATH` variable is set:

****~/.bash_profile
~~~bash

systemctl --user import-environment PATH
~~~

**Note:**

- This will not affect systemd services started before `PATH` is imported.
- _systemd_ does not look at the set `PATH` when resolving non-absolute binaries itself.

#### 2.1.4. pam_env

**Note:** This way of setting environment variables per user is deprecated and will be removed.

Environment variables can be made available through use of the `pam_env.so` module. See [Environment variables#Using pam_env](https://wiki.archlinux.org/title/Environment_variables#Using_pam_env "Environment variables") for configuration details.

### 2.2. Automatic start-up of systemd user instances

<mark style="background: #ABF7F7A6;">The systemd user instance is started after the first login of a user and killed after the last session of the user is closed</mark>. Sometimes it may be useful to start it right after boot, and keep the systemd user instance running after the last session closes, for instance to have some user process running without any open session. Lingering is used to that effect. Use the following command to enable lingering for specific user:
~~~
# loginctl enable-linger username
~~~

**Warning:** systemd services are **not** sessions, they run outside of _logind_. Do not use lingering to enable automatic login as it will [break the session](https://wiki.archlinux.org/title/General_troubleshooting#Session_permissions "General troubleshooting").

## 3. Writing user units

See [systemd#Writing unit files](https://wiki.archlinux.org/title/Systemd#Writingunitfiles "Systemd") for general information about writing systemd unit files.

### 3.1. Example

The following is an example of a user version of the mpd service:

- **~/.config/systemd/user/mpd.service**
	~~~
	
	[Unit]
	Description=Music Player Daemon
	
	[Service]
	ExecStart=/usr/bin/mpd --no-daemon
	
	[Install]
	WantedBy=default.target
	~~~

### 3.2. Example with variables

The following is a user service used by [foldingathome](https://aur.archlinux.org/packages/foldingathome/)AUR, which takes into account variable home directories where [Folding@home](https://wiki.archlinux.org/title/Folding@home "Folding@home") can find certain files:

**~/.config/systemd/user/foldingathome-user.service**
~~~

[Unit]
Description=Folding@home distributed computing client
After=network.target

[Service]
Type=simple
WorkingDirectory=%h/.config/fah
ExecStart=/usr/bin/FAHClient
CPUSchedulingPolicy=idle
IOSchedulingClass=3

[Install]
WantedBy=default.target
~~~

As detailed in [systemd.unit(5) § SPECIFIERS](https://man.archlinux.org/man/systemd.unit.5#SPECIFIERS), the `%h` variable is replaced by the home directory of the user running the service. There are other variables that can be taken into account in the [systemd](https://wiki.archlinux.org/title/Systemd "Systemd") manpages.

### 3.3. Reading the journal

The journal for the user can be read using the analogous command:

~~~
$ journalctl --user
~~~

To specify a unit, one can use

~~~
$ journalctl --user-unit myunit.service
~~~

Or, equivalently:

~~~
$ journalctl --user -u myunit.service
~~~

**Note:** journald will not write user journals for users with UIDs below 1000, instead [directing](https://github.com/systemd/systemd/blob/a33687b792908aa6c9f4c0b22e8935643ee0ddb6/src/journal/journald-server.c#L402) everything to the system journal.

## 4. Temporary files

_systemd-tmpfiles_ allows users to manage custom volatile and temporary files and directories just like in the system-wide way (see [systemd#systemd-tmpfiles - temporary files](https://wiki.archlinux.org/title/Systemd#systemd-tmpfiles_-_temporary_files "Systemd")). User-specific configuration files are read from `~/.config/user-tmpfiles.d/` and `~/.local/share/user-tmpfiles.d/`, in that order. For this functionality to be used, it is needed to enable the necessary systemd user units for your user:

$ systemctl --user enable systemd-tmpfiles-setup.service systemd-tmpfiles-clean.timer

The syntax of the configuration files is the same than those used system-wide. See the [systemd-tmpfiles(8)](https://man.archlinux.org/man/systemd-tmpfiles.8) and [tmpfiles.d(5)](https://man.archlinux.org/man/tmpfiles.d.5) man pages for details.

## 5. Xorg and systemd

**This article or section needs expansion.**

**Reason:** Cover `graphical-session.target`: [systemd.special(7) § Special Passive User Units](https://man.archlinux.org/man/systemd.special.7#Special_Passive_User_Units), [[4]](https://goral.net.pl/post/systemd-x-sessions/). (Discuss in [Talk:Systemd/User](https://wiki.archlinux.org/title/Talk:Systemd/User))

<mark style="background: #D2B3FFA6;">There are several ways to run xorg within systemd unit</mark>s. Below there are two options, either by starting a new user session with an xorg process, or by launching xorg from a systemd user service.

### 5.1. Automatic login into Xorg without display manager

**The factual accuracy of this article or section is disputed.**

**Reason:** This setup ends up with two user D-Bus buses, one for the desktop, and an other for systemd. Why cannot we use the systemd one alone? (Discuss in [Talk:Systemd/User](https://wiki.archlinux.org/title/Talk:Systemd/User))

This option will launch a system unit that will start a user session with an xorg server and then run the usual `~/.xinitrc` to launch the window manager, etc. You need to have [xlogin-git](https://aur.archlinux.org/packages/xlogin-git/)AUR installed. Set up your xinitrc as specified in the [Xinit#xinitrc](https://wiki.archlinux.org/title/Xinit#xinitrc "Xinit") section.

The session will use its own dbus daemon, but various systemd utilities will automatically connect to the `dbus.service` instance. Finally, [enable](https://wiki.archlinux.org/title/Enable "Enable") the `xlogin@_username_` service for automatic login at boot. The user session lives entirely inside a systemd scope and everything in the user session should work just fine.

### 5.2. Xorg as a systemd user service

Alternatively, [xorg](https://wiki.archlinux.org/title/Xorg "Xorg") can be run from within a systemd user service. This is nice since other X-related units can be made to depend on xorg, etc, but on the other hand, it has some drawbacks explained below.

[xorg-server](https://archlinux.org/packages/?name=xorg-server) provides integration with systemd in two ways:

- Can be run unprivileged, delegating device management to logind (see Hans de Goede commits around [this commit](https://gitlab.freedesktop.org/xorg/xserver/-/commit/82863656ec449644cd34a86388ba40f36cea11e9)).
- Can be made into a socket activated service (see [this commit](https://gitlab.freedesktop.org/xorg/xserver/-/commit/b3d3ffd19937827bcbdb833a628f9b1814a6e189)).

Unfortunately, to be able to run xorg in unprivileged mode, it needs to run inside a session. <mark style="background: #D2B3FFA6;">So, right now the handicap of running xorg as user service is that it must be run with root privileges (like before 1.16), and cannot take advantage of the unprivileged mode introduced in 1.16.
</mark>
**Note:** This is not a fundamental restriction imposed by logind, but the reason seems to be that xorg needs to know which session to take over, and right now it gets this information calling [logind](https://www.freedesktop.org/wiki/Software/systemd/logind)'s `GetSessionByPID` using its own pid as argument. See [this thread](https://lists.x.org/archives/xorg-devel/2014-February/040476.html) and [xorg sources](https://gitlab.freedesktop.org/xorg/xserver/-/blob/master/hw/xfree86/os-support/linux/systemd-logind.c). It seems likely that xorg could be modified to get the session from the tty it is attaching to, and then it could run unprivileged from a user service outside a session.

**Warning:** On xorg 1.18 socket activation seems to be broken. The client triggering the activation deadlocks. See the upstream bug report [[5]](https://gitlab.freedesktop.org/xorg/xserver/issues/491). As a temporary workaround you can start the xorg server without socket activation, making sure the clients connect after a delay, so the server is fully started. There seems to be no nice mechanism to get a startup notification for the X server.

<mark style="background: #ABF7F7A6;">This is how to launch xorg from a user service:</mark>

1. Make xorg run with root privileges for any user, by editing `/etc/X11/Xwrapper.config`. This builds on [Xorg#Xorg as Root](https://wiki.archlinux.org/title/Xorg#Xorg_as_Root "Xorg") by adding the stipulation that this need not be done from a physical console. That is, `allowed_user`'s default of `console` is being overwritten with `anybody`; see [Xorg.wrap(1)](https://man.archlinux.org/man/Xorg.wrap.1).
	**/etc/X11/Xwrapper.config**
	~~~
	
	allowed_users=anybody
	needs_root_rights=yes
	~~~

2. Add the following units to `~/.config/systemd/user`
	**~/.config/systemd/user/xorg@.socket**
	~~~
	
	[Unit]
	Description=Socket for xorg at display %i
	
	[Socket]
	ListenStream=/tmp/.X11-unix/X%i
	~~~
	
	****~/.config/systemd/user/xorg@.service
	~~~
	
	[Unit]
	Description=Xorg server at display %i
	
	Requires=xorg@%i.socket
	After=xorg@%i.socket
	
	[Service]
	Type=simple
	SuccessExitStatus=0 1
	
	ExecStart=/usr/bin/Xorg :%i -nolisten tcp -noreset -verbose 2 "vt${XDG_VTNR}"
	~~~
	
	where `${XDG_VTNR}` is the virtual terminal where xorg will be launched, either hard-coded in the service unit, or set in the systemd environment with
	
	~~~
	$ systemctl --user set-environment XDG_VTNR=1
	~~~
	
	**Note:** xorg should be launched at the same virtual terminal where the user logged in. Otherwise logind will consider the session inactive.

3. Make sure to configure the `DISPLAY` environment variable as explained [above](https://wiki.archlinux.org/title/systemd/User#DISPLAY_and_XAUTHORITY).

4. Then, to enable socket activation for xorg on display 0 and tty 2 one would do:
	~~~
	$ systemctl --user set-environment XDG_VTNR=2     # So that xorg@.service knows which vt use
	$ systemctl --user start xorg@0.socket            # Start listening on the socket for display 0
	~~~
	
	Now running any X application will launch xorg on virtual terminal 2 automatically.
	
	The environment variable `XDG_VTNR` can be set in the systemd environment from `.bash_profile`, and then one could start any X application, including a window manager, as a systemd unit that depends on `xorg@0.socket`.
	
	**Warning:** Currently running a window manager as a user service means it runs outside of a session with the problems this may bring: [break the session](https://wiki.archlinux.org/title/General_troubleshooting#Session_permissions "General troubleshooting"). However, it seems that systemd developers intend to make something like this possible. See [[6]](https://mail.gnome.org/archives/desktop-devel-list/2014-January/msg00079.html) and [[7]](https://lists.freedesktop.org/archives/systemd-devel/2014-March/017552.html)

## 6. Some use cases

### 6.1. Window manager

To run a window manager as a systemd service, you first need to run [#Xorg as a systemd user service](https://wiki.archlinux.org/title/systemd/User#Xorg_as_a_systemd_user_service). In the following we will use [awesome](https://wiki.archlinux.org/title/Awesome "Awesome") as an example:

****~/.config/systemd/user/awesome.service
~~~

[Unit]
Description=Awesome window manager
After=xorg.target
Requires=xorg.target

[Service]
ExecStart=/usr/bin/awesome
Restart=always
RestartSec=10
 
[Install]
WantedBy=wm.target
~~~

**Note:** The `[Install]` section includes a `WantedBy` part. When using `systemctl --user enable` it will link this as `~/.config/systemd/user/wm.target.wants/window_manager.service`, allowing it to be started at login. Is recommended to enable this service, not to link it manually.

### 6.2. Persistent terminal multiplexer

Rather than logging you into a window manager session for your user session by default, you may want to automatically run a terminal multiplexer (such as [screen](https://wiki.archlinux.org/title/Screen "Screen") or [tmux](https://wiki.archlinux.org/title/Tmux "Tmux")) in the background.

[Create](https://wiki.archlinux.org/title/Create "Create") the following:

****~/.config/systemd/user/multiplexer.target
~~~

[Unit]
Description=Terminal multiplexer
Documentation=info:screen man:screen(1) man:tmux(1)
After=cruft.target
Wants=cruft.target

[Install]
Alias=default.target
~~~

Separating login from X login is most likely only useful for those who boot to a TTY instead of to a display manager (in which case you can simply bundle everything you start in `mystuff.target`).

The dependency `cruft.target`, like the `mystuff.target` above, allows starting anything which should run before the multiplexer starts (or which you want started at boot regardless of timing), such as a GnuPG daemon session.

You then need to create a service for your multiplexer session. Here is a sample service, using tmux as an example and sourcing a gpg-agent session which wrote its information to `/tmp/gpg-agent-info`. This sample session, when you start X, will also be able to run X programs, since `$DISPLAY` is set:

****~/.config/systemd/user/tmux.service
~~~

[Unit]
Description=tmux: A terminal multiplexer 
Documentation=man:tmux(1)
After=gpg-agent.service
Wants=gpg-agent.service

[Service]
Type=forking
ExecStart=/usr/bin/tmux start
ExecStop=/usr/bin/tmux kill-server
Environment=DISPLAY=:0
EnvironmentFile=/tmp/gpg-agent-info

[Install]
WantedBy=multiplexer.target
~~~

[Enable](https://wiki.archlinux.org/title/Enable "Enable") `tmux.service`, `multiplexer.target` and any services you created to be run by `cruft.target`, [start](https://wiki.archlinux.org/title/Start "Start") `user@.service` as usual and you should be done.

## 7. Kill user processes on logout

Arch Linux builds the [systemd](https://archlinux.org/packages/?name=systemd) package with `--without-kill-user-processes`, setting `KillUserProcesses` to `no` by default. This setting causes user processes not to be killed when the user logs out. To change this behavior in order to have all user processes killed on the user's logout, set `KillUserProcesses=yes` in `/etc/systemd/logind.conf`.

Note that changing this setting breaks terminal multiplexers such as [tmux](https://wiki.archlinux.org/title/Tmux "Tmux") and [GNU Screen](https://wiki.archlinux.org/title/GNU_Screen "GNU Screen"). If you change this setting, you can still use a terminal multiplexer by using `systemd-run` as follows:

~~~
$ systemd-run --scope --user _command_ _args_
~~~

For example, to run `screen` you would do:
~~~

$ systemd-run --scope --user screen -S _foo_
~~~

Using `systemd-run` will keep the process running after logout only while the user is logged in at least once somewhere else in the system and `user@.service` is still running.

After the user logs out of all sessions, `user@.service` will be terminated too, by default, unless the user has "lingering" enabled [[8]](https://github.com/systemd/systemd/blob/76153ad45f09b6ae45464f2e03d3afefbb4b2afe/NEWS#L274). To effectively allow users to run long-term tasks even if they are completely logged out, lingering must be enabled for them. See [#Automatic start-up of systemd user instances](https://wiki.archlinux.org/title/systemd/User#Automatic_start-up_of_systemd_user_instances) and [loginctl(1)](https://man.archlinux.org/man/loginctl.1) for details.

## 8. Troubleshooting

### 8.1. Runtime directory '/run/user/1000' is not owned by UID 1000, as it should

systemd[1867]: pam_systemd(systemd-user:session): Runtime directory '/run/user/1000' is not owned by UID 1000, as it should.
systemd[1867]: Trying to run as user instance, but $XDG_RUNTIME_DIR is not set

If you see errors such as this and your login session is broken, it is possible that another system (non-user) service on your system is creating this directory. This can happen for example if you use a [docker](https://wiki.archlinux.org/title/Docker "Docker") container that has a bind mount to `/run/user/1000`. To fix this, you can either fix the container by removing the mount, or disable/delay the docker service.

## 9. See also

- [KaiSforza's Bitbucket wiki](https://bitbucket.org/KaiSforza/systemd-user-units/wiki/Home)
- [Zoqaeski's units on GitHub](https://github.com/zoqaeski/systemd-user-units)
- [Arch forum thread about changes in systemd 206 user instances](https://bbs.archlinux.org/viewtopic.php?id=167115)

[Category](https://wiki.archlinux.org/title/Special:Categories "Special:Categories"): 

- [System administration](https://wiki.archlinux.org/title/Category:System_administration "Category:System administration")

- This page was last edited on 25 December 2022, at 03:03.
- Content is available under [GNU Free Documentation License 1.3 or later](https://www.gnu.org/copyleft/fdl.html) unless otherwise noted.

- [Privacy policy](https://terms.archlinux.org/docs/privacy-policy/)
- [About ArchWiki](https://wiki.archlinux.org/title/ArchWiki:About)
- [Disclaimers](https://wiki.archlinux.org/title/ArchWiki:General_disclaimer)