---
date: 2023-08-11 10:44
title: User NetSysFire systemd sandboxing
tags:
- 
---



# User:NetSysFire/systemd sandboxing

- [User page](https://wiki.archlinux.org/title/User:NetSysFire/systemd_sandboxing "View the user page [alt-shift-c]")
- [Discussion](https://wiki.archlinux.org/title/User_talk:NetSysFire/systemd_sandboxing "Discussion about the content page [alt-shift-t]")

- [Read](https://wiki.archlinux.org/title/User:NetSysFire/systemd_sandboxing)
- [View source](https://wiki.archlinux.org/index.php?title=User:NetSysFire/systemd_sandboxing&action=edit "This page is protected.
    You can view its source [alt-shift-e]")
- [View history](https://wiki.archlinux.org/index.php?title=User:NetSysFire/systemd_sandboxing&action=history "Past revisions of this page [alt-shift-h]")

< [User:NetSysFire](https://wiki.archlinux.org/title/User:NetSysFire "User:NetSysFire")

Related articles

- [Firejail](https://wiki.archlinux.org/title/Firejail "Firejail")
- [AppArmor](https://wiki.archlinux.org/title/AppArmor "AppArmor")

**This article or section is a candidate for moving to

**Notes:** Draft. Do not move yet. (Discuss in [Talk:Security#systemd unit hardening and system.conf tweaks](https://wiki.archlinux.org/title/Talk:Security#systemd_unit_hardening_and_system.conf_tweaks "Talk:Security"))

## 1. Sandboxing systemd service units

systemd enables users to harden and sandbox systemd system service units. Because of technical limitations, and ironically security reasons, user units can not be hardened or sandboxed properly since this would make privilege escalation issues possible. This does not affect system units which use the `User=` directive.

**Tip:** [Firejail](https://wiki.archlinux.org/title/Firejail "Firejail") can sandbox applications run as an unprivileged and explicitely allowed user but uses a SUID binary, which is in theory susceptible to privilege escalation vulnerabilities.

Because of the nature of other unit types, only service units can be hardened/sandboxed in the traditional sense. See [systemd.exec(5)](https://man.archlinux.org/man/systemd.exec.5) for more information.

### 1.1. General

Since hardening/sandboxing effectively restricts an application, it is not possible to use all the sandboxing directives. A webserver for example should not use `ProtectNetwork=true` since it usually needs network access.

`systemd-analyze security _unit_` generates a score for the unit showing all the used directives, which can be helpful to determine what settings to try next.

**Warning:** The score is slightly misleading. Only a simple `Hello world` can achieve a near perfect score. No application can use _all_ the sandboxing settings.

Unfortunately, systemd's error messages on misconfigurations relating to sandboxing are sometimes vague and/or misleading. Setting the log level temporarily to `debug` may help getting actually relevant information.
~~~
# systemctl log-level debug
~~~

### 1.2. Common directives

Most of these directives can be applied to most applications without causing too many problems.

**Note:** "Impact" and "Potential for breakage" are all _relative_ since they depend on the environment and the unit.

#### 1.2.1. Without special configuration

Simple boolean settings which can either be enabled or not. They can not be configured.

|Directive|Impact1|Breakage2|Notes|
|---|---|---|---|
|`LockPersonality`|Medium|Low||
|`MemoryDenyWriteExecute`|Medium3|Medium||
|`NoNewPrivileges`|High|Low||
|`PrivateDevices`|Medium|Low|`/dev/null` and similar will still be there|
|`PrivateNetwork`|High|Very high|Disallows _any_ network access.|
|`PrivateTmp`|Medium|Low||
|`PrivateUsers`|High|High||
|`ProtectClock`5|Low|Medium4||
|`ProtectControlGroups`5|Medium|Low|Highly recommended since no service should write to that|
|`ProtectHostname`5|Low|Low||
|`ProtectKernelLogs`5,6|Low|Low||
|`ProtectKernelModules`5|Medium|Low||
|`ProtectKernelTunables`5|Low|Low||
|`RestrictRealtime`|Low|Low|May prevent denial-of-service situations|
|`RestrictSUIDSGID`|Medium|Low|Best used with `NoNewPrivileges`|

1. How effective the directive is
2. How likely the directive is to break something
3. Can be enhanced with `SystemCallFilter`
4. Some users reported `smartctl` can not run when this is set, but this should be relatively safe.
5. Even when running as another `User=`, systemd setups seccomp filters, which can e.g catch the application running `sudo modprobe` when `ProtectKernelModules` is set to `true`
6. All official kernels have set `SECURITY_DMESG_RESTRICT` to `y`, but this is still defense in depth.

#### 1.2.2. Configurable directives

|Directive|Value|Impact1|Breakage2|Notes|
|---|---|---|---|---|
|`ProtectSystem`|`strict`|Very high|Very high|Usually used with `ReadWritePaths=`|
|`full`|High|Medium|May break e.g webservers using [ACME](https://wiki.archlinux.org/title/ACME "ACME") to renew their own keys which _may_ be in `/etc`|
|`true`|High|Medium|There are in theory few applications which write to `/boot` and `/usr`|
|`ProtectHome`|`true`|Medium|Medium|Some applications may need persistent data stored in `XDG_CONFIG_HOME`3|
|`tmpfs`|Low|Low||
|`read-only`|Low|Low|Ideal for backup services|
|`ProtectProc`|`invisible`|High|Medium|Defaults to the hidepid value of the /proc mount, which is usually 0 (unrestricted)|
|`noaccess`|Medium|Medium||

1. How effective the directive is
2. How likely the directive is to break something
3. `StateDirectory=` can be used to mitigate some of the negative consequences

### 1.3. Advanced directives

~~~
Syscall filter etc
~~~

### 1.4. chroot jail

It is possible to severely restrict what a process can see by specifying `TemporaryFileSystem=/:ro` and mounting required paths into this chroot-like jail. `RootDirectory` requires a directory to be present, whereas `TemporaryFileSystem` does not and will override / seamlessly. Both, and especially the latter, appear to be secure chroot-like directives, which can not be broken out easily, as they do not use the `chroot` syscall.

**Warning:** `ProtectSystem` and `ProtectHome` are incompatible with `TemporaryFileSystem=/:ro` and will cause the latter to be undone, making / visible again. However, these directives are not needed since paths will be whitelisted anyways.

All required paths must be mounted into this jail via `BindReadOnlyPaths` and `BindPaths`:

~~~
example_jailed_unit.service

[Unit]
Description=Example unit

[Service]
ExecStart=/home/someuser/executable
User=someuser
Group=someuser
TemporaryFileSystem=/:ro
PrivateTmp=true
BindReadOnlyPaths=/usr/lib /lib64 /lib
BindPaths=/home/someuser/executable
~~~

This is a minimal example and most application will need more paths whitelisted. Some common paths include:

- /etc/ca-certificates, /etc/ssl
- /etc/resolv.conf
- /usr/share/zoneinfo
- Any sockets you need, e.g /var/run/mysqld/mysqld.sock

It will be likely that debugging is at some point necessary when trying to sandbox a unit for the first time. If a unit can not be started at all and fails with `status=203/EXEC`, either the executable itself or required libraries are not accessible. Starting with broad paths at first (e.g allowing the entirety of /usr) and narrowing it down later can help, too.

## 2. system.conf

Changes to `/etc/systemd/system.conf` are global, so they will affect every unit. See [systemd-system.conf(5)](https://man.archlinux.org/man/systemd-system.conf.5)

### 2.1. Disabling non-native syscalls

Non-native binaries, in almost all cases 32-bit binaries, may partially compromise the security of the system because they [do not have access to more hardening](https://wiki.archlinux.org/title/Security#Userspace_ASLR_comparison "Security"). There have been some relatively minor vulnerabilities, like [CVE-2009-0835](https://www.cvedetails.com/cve/CVE-2009-0835/), which affected non-native syscalls.

**Warning:** This **will** break 32-bit binaries. Trying to execute such a binary will result in a file not found error.

~~~
/etc/systemd/system.conf

SystemCallArchitectures=native
~~~

This works well on most systems, but it needs to be at least partially disabled if e.g [multilib](https://wiki.archlinux.org/title/Multilib "Multilib") is in use. Especially gaming with [Wine](https://wiki.archlinux.org/title/Wine "Wine") may be impacted. Using `systemd-run` or modifying the session slice to override `SystemCallArchitectures` can be used to disable restrictions partially.

### 2.2. Enabling more unit statistics

systemd does not track all resource usage of a unit by default. Enable `Default*Accounting` to get more statistics in the `systemctl status` output and the journal. This is not strictly a security setting, but it will certainly make debugging easier and can provide useful insights into resource usage.
~~~
/etc/systemd/system.conf

DefaultCPUAccounting=yes
DefaultIOAccounting=yes
DefaultIPAccounting=yes
DefaultBlockIOAccounting=yes
DefaultMemoryAccounting=yes
DefaultTasksAccounting=yes
~~~