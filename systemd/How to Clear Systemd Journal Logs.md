---
date: 2023-09-08 16:39
title: How to Clear Systemd Journal Logs
tags:
  - systemd
  - Journal
---



Please enter at least 3 characters

TipsCommandsTutorialQuick TipExplainNewsletter

Nov 11, 2022•6 min read

# How to Clear Systemd Journal Logs

This quick tutorial shows you two ways to clear systemd journal logs from your Linux system.


Table of Contents

1. [Clearing systemd journal logs](https://linuxhandbook.com/clear-systemd-journal-logs/#clearing-systemd-journal-logs)
    1. [1. Clear journal log older than x days](https://linuxhandbook.com/clear-systemd-journal-logs/#1-clear-journal-log-older-than-x-days)
    2. [2. Restrict logs to a certain size](https://linuxhandbook.com/clear-systemd-journal-logs/#2-restrict-logs-to-a-certain-size)
    3. [3. Restrict number of log files](https://linuxhandbook.com/clear-systemd-journal-logs/#3-restrict-number-of-log-files)
2. [Automatically clearing old log files [Requires intermediate knowledge of command line]](https://linuxhandbook.com/clear-systemd-journal-logs/#automatically-clearing-old-log-files-requires-intermediate-knowledge-of-command-line-)

_**__This quick tutorial shows you two ways to clear systemd journal logs from your Linux system.__**_

The [systemd journal](https://wiki.archlinux.org/index.php/Systemd/Journal?ref=linuxhandbook.com) is systemd’s own logging system. It is equivalent to the [syslog in the init system](https://linuxhandbook.com/syslog-guide/). It collects and stored kernel logging data, system log messages, standard output, and errors for various systemd services.

A Linux machine with systemd writes the logs to /var/log/journal directory. If you remember the [Linux directory structure](https://linuxhandbook.com/linux-directory-structure/), /var is where the system logs are stored.

You can either manually view the log files [using less command](https://linuxhandbook.com/less-command/) or [use the journalctl command](https://linuxhandbook.com/journalctl-command/). To view all the latest logs, use the command with the reverse option.

```sh
journalctl -r
```

The thing with logging is that over time, it starts to grow big. And if you [check the disk space in Linux](https://linuxhandbook.com/df-command/), you’ll see that sometimes, it takes several GB of space.

Let me show you how to clean systemd journal logs and free up disk space on your Linux system.

## Clearing systemd journal logs


First, check the space taken by journal logs with the [du command](https://linuxhandbook.com/find-directory-size-du-command/):

```sh
du -sh /var/log/journal/
```

You can also use the journalctl command for the same task:

```sh
journalctl --disk-usage
```

Both commands should give approximately the same result:

```sh
abhishek@linuxhandbook:~$ journalctl --disk-usage
Archived and active journals take up 1.6G in the file system.

abhishek@linuxhandbook:~$ sudo du -sh /var/log/journal/
1.7G	/var/log/journal/
```

Now that you know how much space the journal logs take, you can decide if you want to clear the logs or not. If you decide to clear the journal logs, let me show you a couple of ways of doing it.

You can, of course, use the rm command to [delete the files](https://linuxhandbook.com/remove-files-directories/) in the log folder but I won’t advise that. The journalctl command gives you the proper way of handling old logs.

First thing you should do is to rotate journal files. This will mark the currently active journal logs as archive and create fresh new logs. It’s optional but a good practice to do so.

```sh
sudo journalctl --rotate
```

Now you have three ways to clear old journal logs. You delete logs older than a certain time, or you delete older log files so that the total log size is limited to the predefined disk space, or you limit the number of log files. Let’s see how to use all three methods.

### 1. Clear journal log older than x days

Keep in mind that logs are important for auditing purposes so you should not delete all of them at the same time. Let’s say you want to keep the log history of just two days. To delete all entries older than two days, use this command:

```
sudo journalctl --vacuum-time=2d
```

Here’s what the output may look like:

```sh
Vacuuming done, freed 1.6G of archived journals from /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c
```

You can also change the provide time frame in hours like 2h, in minutes like 2m, in seconds like 2s. If you want bigger time units, you can 2weeks, 2months as well.

### 2. Restrict logs to a certain size

Another way is to restrict the log size. This will delete the journal log files until the disk space taken by journal logs falls below the size you specified.

```sh
sudo journalctl --vacuum-size=100M
```

This will reduce the log size to around 100 MB.

```sh
Vacuuming done, freed 40.0M of archived journals from /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c.
```

You can specify the size in GB with G, MB with M, KB with K etc.

### 3. Restrict number of log files

The third way is to limit the number of log files. The journalctl usually has log files for the system and for the users. As the logs get old, they are archived in various files.

You can limit the number of archive log files. Let’s say you want to have only five log files.

```
journalctl --vacuum-files=5
```

It will remove the older archive log files leaving only the specified number of log files.

```sh
Deleted archived journal /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c/system@d9fbc18533be4cb69483adf2a61505ac-00000000001e0bba-00059745988c0982.journal (8.0M).
Deleted archived journal /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c/user-1000@f571e91e5c6748a8a74666a448df78dd-00000000001e21d4-00059747a2ebd5a3.journal (48.0M).
Deleted archived journal /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c/system@d9fbc18533be4cb69483adf2a61505ac-00000000001e2414-00059747a32024d0.journal (48.0M).
Vacuuming done, freed 104.0M of archived journals from /var/log/journal/1b9ab93094fa4978beba80fd3c48a18c.
```

## Automatically clearing old log files [Requires intermediate knowledge of command line]

What you just did will clean the log files for now. In a month, the logs will increase again. You can manually clean them with one of the methods described above. But that is a tedious task and you may not remember to do that regularly.

The good thing is that you can configure systemd to automatically handle old log files.

The journalctl has configuration file at /etc/systemd/journald.conf. There are settings which are commented out. The commented lines basically indicate the default value of those settings parameters (even if they are commented out).

You can change some of these default settings to automatically clean the log files.

You would want to change the following settings:

|Setting|Description|
|---|---|
|SystemMaxUse|Max disk space logs can take|
|SystemMaxFileSize|Max size of an INDIVIDUAL log file|
|SystemMaxFiles|Max number of log files|

Attention!

Please note that you should be careful while editing configuration files. You must be comfortable using a terminal-based text editor like Vim, [Emacs](https://linuxhandbook.com/basic-emacs-guide/) or Nano so that you don’t make silly mistakes while editing the conf file.

I advise to make a backup of the configuration file first:

```sh
cp /etc/systemd/journald.conf /etc/systemd/journald.conf.back
```

Now, you should uncomment (remove the # at the beginning of the line) the setting you want to use. For example, I want to restrict the maximum disk space taken by the log files to 250 MB.

You’ll have to [use Vim](https://linuxhandbook.com/basic-vim-commands/) or some other terminal based editor in order to edit this configuration file. Here’s what it looks like me after editing the file.

```sh
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See journald.conf(5) for details.

[Journal]
#Storage=auto
#Compress=yes
#Seal=yes
#SplitMode=uid
#SyncIntervalSec=5m
#RateLimitIntervalSec=30s
#RateLimitBurst=1000
SystemMaxUse=250M
#SystemKeepFree=
#SystemMaxFileSize=
#SystemMaxFiles=100
```

Keep in mind that after editing the configuration file, you should load the changes:

```sh
sudo systemctl restart systemd-journald
```

The journald.conf file can be used to tweak the journalctl settings even further. You can even set the levels of log (info, debug, error etc) you want to see. It’s up to you if you want to change those settings as well.

I hope you like this tip to clear systemd journal log files. If you have any questions or suggestions, please leave a comment below.