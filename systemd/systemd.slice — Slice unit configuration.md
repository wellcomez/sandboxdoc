---
date: 2023-08-01 16:02
title: systemd.slice — Slice unit configuration
tags:
- systemd
- systemd-slice
---



[Index](https://www.freedesktop.org/software/systemd/man/index.html) · [Directives](https://www.freedesktop.org/software/systemd/man/systemd.directives.html)systemd 254

---

## Name

systemd.slice — Slice unit configuration

## Synopsis

``_`slice`_.slice``

## Description[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#Description "Permalink to this headline")

A unit configuration file whose name ends in "`.slice`" encodes information about a slice unit. A slice unit is a concept for hierarchically managing resources of a group of processes. This management is performed by creating a node in the Linux Control Group (cgroup) tree. Units that manage processes (primarily scope and service units) may be assigned to a specific slice. For each slice, certain resource limits may be set that apply to all processes of all units contained in that slice. Slices are organized hierarchically in a tree. The name of the slice encodes the location in the tree. The name consists of a dash-separated series of names, which describes the path to the slice from the root slice. The root slice is named `-.slice`. Example: `foo-bar.slice` is a slice that is located within `foo.slice`, which in turn is located in the root slice `-.slice`.

Note that slice units cannot be templated, nor is possible to add multiple names to a slice unit by creating additional symlinks to its unit file.

By default, service and scope units are placed in `system.slice`, virtual machines and containers registered with [systemd-machined(8)](https://www.freedesktop.org/software/systemd/man/systemd-machined.html#) are found in `machine.slice`, and user sessions handled by [systemd-logind(8)](https://www.freedesktop.org/software/systemd/man/systemd-logind.html#) in `user.slice`. See [systemd.special(7)](https://www.freedesktop.org/software/systemd/man/systemd.special.html#) for more information.

See [systemd.unit(5)](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#) for the common options of all unit configuration files. The common configuration items are configured in the generic [Unit] and [Install] sections. The slice specific configuration options are configured in the [Slice] section. Currently, only generic resource control settings as described in [systemd.resource-control(5)](https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html#) are allowed.

See the [New Control Group Interfaces](https://www.freedesktop.org/wiki/Software/systemd/ControlGroupInterface) for an introduction on how to make use of slice units from programs.

## Automatic Dependencies[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#Automatic%20Dependencies "Permalink to this headline")

### Implicit Dependencies[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#Implicit%20Dependencies "Permalink to this headline")

The following dependencies are implicitly added:

- Slice units automatically gain dependencies of type `After=` and `Requires=` on their immediate parent slice unit.
    

### Default Dependencies[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#Default%20Dependencies "Permalink to this headline")

The following dependencies are added unless `DefaultDependencies=no` is set:

- Slice units will automatically have dependencies of type `Conflicts=` and `Before=` on `shutdown.target`. These ensure that slice units are removed prior to system shutdown. Only slice units involved with late system shutdown should disable `DefaultDependencies=` option.
    

## Options[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#Options "Permalink to this headline")

Slice unit files may include [Unit] and [Install] sections, which are described in [systemd.unit(5)](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#). No options specific to this file type are supported.

## See Also[](https://www.freedesktop.org/software/systemd/man/systemd.slice.html#See%20Also "Permalink to this headline")

[systemd(1)](https://www.freedesktop.org/software/systemd/man/systemd.html#), [systemd.unit(5)](https://www.freedesktop.org/software/systemd/man/systemd.unit.html#), [systemd.resource-control(5)](https://www.freedesktop.org/software/systemd/man/systemd.resource-control.html#), [systemd.service(5)](https://www.freedesktop.org/software/systemd/man/systemd.service.html#), [systemd.scope(5)](https://www.freedesktop.org/software/systemd/man/systemd.scope.html#), [systemd.special(7)](https://www.freedesktop.org/software/systemd/man/systemd.special.html#), [systemd.directives(7)](https://www.freedesktop.org/software/systemd/man/systemd.directives.html#)