---
date: 2023-08-07 16:16
title: ioctl_tty
tags:
- tty
---



# ioctl_tty(2) — Linux manual page

ioctl_tty(2)               System Calls Manual              ioctl_tty(2)

## NAME         
 ioctl_tty - ioctls for terminals and serial lines
## LIBRARY         
 Standard C library (_libc_, _-lc_)
## SYNOPSIS 
\#include <sys/ioctl.h>

\#include <asm/termbits.h>   /* Definition of **struct termios**,
                                      **struct termios2**, and
                                      **Bnnn**, **BOTHER**, **CBAUD**, **CLOCAL**,
                                      **TC***{**FLUSH**,**ON**,**OFF**} and other constants */

~~~c
int ioctl(int** _fd_**, int** _cmd_**, ...);
~~~
## DESCRIPTION         

The [ioctl(2)](https://man7.org/linux/man-pages/man2/ioctl.2.html) call for terminals and serial ports accepts many
possible command arguments.  Most require a third argument, of
varying type, here called _argp_ or _arg_.

Use of **ioctl**() makes for nonportable programs.  Use the POSIX
interface described in [termios(3)](https://man7.org/linux/man-pages/man3/termios.3.html) whenever possible.

Please note that **struct termios** from _<asm/termbits.h>_ is
different and incompatible with **struct termios** from _<termios.h>_.
These ioctl calls require **struct termios** from _<asm/termbits.h>_.

- **Get and set terminal attributes**
	- **TCGETS** 
	
		Argument: **struct termios** _argp_
		
		Equivalent to _tcgetattr(fd, argp)_.
		Get the current serial port settings.

	- **TCSETS** 
	
		Argument: **const struct termios** _argp_
		
		Equivalent to _tcsetattr(fd, TCSANOW, argp)_.
		Set the current serial port settings.

	- **TCSETSW**
	
		Argument: **const struct termios** _argp_
		
		Equivalent to _tcsetattr(fd, TCSADRAIN, argp)_.
		Allow the output buffer to drain, and set the current
		serial port settings.

	- **TCSETSF**
	
		Argument: **const struct termios** _argp_
		
		Equivalent to _tcsetattr(fd, TCSAFLUSH, argp)_.
		Allow the output buffer to drain, discard pending input,and set the current serial port settings.

	The following four ioctls, added in Linux 2.6.20, are just like **TCGETS**, **TCSETS**, **TCSETSW**, **TCSETSF**, except that they take a _struct_ _termios2 *_ instead of a _struct termios *_.  If the structure member **c_cflag** contains the flag **BOTHER**, then the baud rate is stored in the structure members **c_ispeed** and **c_ospeed** as integer values.  These ioctls are not supported on all architectures.
		**TCGETS2    struct termios2** argp
		**TCSETS2    const struct termios2** argp
		**TCSETSW2   const struct termios2** argp
		**TCSETSF2   const struct termios2** argp
	
	The following four ioctls are just like **TCGETS**, **TCSETS**, **TCSETSW**, **TCSETSF**, except that they take a _struct termio *_ instead of a _struct termios *_.
		**TCGETA    struct termio** argp
		**TCSETA    const struct termio** argp
		**TCSETAW   const struct termio** argp
		**TCSETAF   const struct termio** argp

- **Locking the termios structure**
	The _termios_ structure of a terminal can be locked.  The lock is itself a _termios_ structure, with nonzero bits or fields indicating a locked value.
	- **TIOCGLCKTRMIOS**
	
		Argument: **struct termios** _argp_
		
		Gets the locking status of the _termios_ structure of the terminal.
	
	- **TIOCSLCKTRMIOS**
	  
		Argument: **const struct termios** _argp_
		
		Setb the locking status of the _termios_ structure of the terminal.  Only a process with the **CAP_SYS_ADMIN**	  capability can do this.

- **Get and set window size**
	Window sizes are kept in the kernel, but not used by the kernel (except in the case of virtual consoles, where the kernel will update the window size when the size of the virtual console changes, for example, by loading a new font).
	- **TIOCGWINSZ**
	  
	  Argument: **struct winsize** _argp_
	  
	  Get window size.
	
	- **TIOCSWINSZ**
	  
	  Argument: **const struct winsize** _argp_
	  
	  Set window size.
		The struct used by these ioctls is defined as
		
		struct winsize {
		   unsigned short ws_row;
		   unsigned short ws_col;
		   unsigned short ws_xpixel;   /* unused */
		   unsigned short ws_ypixel;   /* unused */
		};
		
		When the window size changes, a **SIGWINCH** signal is sent to the
		foreground process group.

- **Sending a break**
	- **TCSBRK** 
	
	  Argument: **int** _arg_
	  
	  Equivalent to _tcsendbreak(fd, arg)_.
	  If the terminal is using asynchronous serial data
	  transmission, and _arg_ is zero, then send a break (a stream
	  of zero bits) for between 0.25 and 0.5 seconds.  If the
	  terminal is not using asynchronous serial data
	  transmission, then either a break is sent, or the function
	  returns without doing anything.  When _arg_ is nonzero,
	  nobody knows what will happen.
	
	  (SVr4, UnixWare, Solaris, and Linux treat
	  _tcsendbreak(fd,arg)_ with nonzero _arg_ like _tcdrain(fd)_.
	  SunOS treats _arg_ as a multiplier, and sends a stream of
	  bits _arg_ times as long as done for zero _arg_.  DG/UX and
	  AIX treat _arg_ (when nonzero) as a time interval measured
	  in milliseconds.  HP-UX ignores _arg_.)
	- **TCSBRKP**
	  
		Argument: **int** _arg_
	 
	   So-called "POSIX version" of **TCSBRK**.  It treats nonzero
	  _arg_ as a time interval measured in deciseconds, and does
	  nothing when the driver does not support breaks.
	- **TIOCSBRK**
		
		Argument: **void**
		
		Turn break on, that is, start sending zero bits.
	-  **TIOCCBRK**
		
		Argument: **void**
		Turn break off, that is, stop sending zero bits.

- **Software flow control**
	- **TCXONC** 
		
		Argument: **int** _arg_
	 
	   Equivalent to _tcflow(fd, arg)_.
	
	  See [tcflow(3)](https://man7.org/linux/man-pages/man3/tcflow.3.html) for the argument values **TCOOFF**, **TCOON**,
	  **TCIOFF**, **TCION**.
	
- **Buffer count and flushing**
	- **FIONREAD**
		
		Argument: **int** _argp_
		
		Get the number of bytes in the input buffer.
	
	- **TIOCINQ**
		
		Argument: **int** _argp_
		
		Same as **FIONREAD**.
	
	- **TIOCOUTQ**
		
		Argument: **int** _argp_
		
		Get the number of bytes in the output buffer.
	
	- **TCFLSH** 
		
		Argument: **int** _arg_
	  
		Equivalent to _tcflush(fd, arg)_.
	  See [tcflush(3)](https://man7.org/linux/man-pages/man3/tcflush.3.html) for the argument values **TCIFLUSH**, **TCOFLUSH**,**TCIOFLUSH**.
	
	- **TIOCSERGETLSR**
	  
		Argument: **int** _argp_
	  
		Get line status register.  Status register has **TIOCSER_TEMT** bit set when output buffer is empty and also	  hardware transmitter is physically empty.
	
	  Does not have to be supported by all serial tty drivers.
	
	  [tcdrain(3)](https://man7.org/linux/man-pages/man3/tcdrain.3.html) does not wait and returns immediately when
	  **TIOCSER_TEMT** bit is set.

-  **Faking input**
	- **TIOCSTI**
	  
	  Argument: **const char** _argp_

	  Insert the given byte in the input queue.

- **Redirecting console output**
	- **TIOCCONS**
	  
	  Argument: **void**
	  
	  Redirect output that would have gone to _/dev/console_ or
	  _/dev/tty0_ to the given terminal.  If that was a
	  pseudoterminal master, send it to the slave.  Before Linux
	  2.6.10, anybody can do this as long as the output was not
	  redirected yet; since Linux 2.6.10, only a process with
	  the **CAP_SYS_ADMIN** capability may do this.  If output was
	  redirected already, then **EBUSY** is returned, but
	  redirection can be stopped by using this ioctl with _fd_
	  pointing at _/dev/console_ or _/dev/tty0_.

- **Controlling terminal**
	- **TIOCSCTTY**
	  
	  Argument: **int** _arg_
	
	  Make the given terminal the controlling terminal of the
	  calling process.  The calling process must be a session
	  leader and not have a controlling terminal already.  For
	  this case, _arg_ should be specified as zero.
	
	  If this terminal is already the controlling terminal of a
	  different session group, then the ioctl fails with **EPERM**,
	  unless the caller has the **CAP_SYS_ADMIN** capability and _arg_
	  equals 1, in which case the terminal is stolen, and all
	  processes that had it as controlling terminal lose it.
	
	- **TIOCNOTTY**
	  
	  Argument: **void**
	
	  If the given terminal was the controlling terminal of the
	  calling process, give up this controlling terminal.  If
	  the process was session leader, then send **SIGHUP** and
	  **SIGCONT** to the foreground process group and all processes
	  in the current session lose their controlling terminal.

- **Process group and session ID**
	- **TIOCGPGRP**
	  
	  Argument: **pid_t** _argp_
	
	  When successful, equivalent to _*argp = tcgetpgrp(fd)_.
	
	  Get the process group ID of the foreground process group
	  on this terminal.
	
	- **TIOCSPGRP**
	  
	  Argument: **const pid_t** _argp_
	
	  Equivalent to _tcsetpgrp(fd, *argp)_.
	
	  Set the foreground process group ID of this terminal.
	
	- **TIOCGSID**
	  
	  Argument: **pid_t** _argp_
	
	  When successful, equivalent to _*argp = tcgetsid(fd)_.
	
	  Get the session ID of the given terminal.  This fails with
	  the error **ENOTTY** if the terminal is not a master
	  pseudoterminal and not our controlling terminal.  Strange.

- **Exclusive mode**
	- **TIOCEXCL**
	  
	  Argument: **void**
	  
	  Put the terminal into exclusive mode.  No further [open(2)](https://man7.org/linux/man-pages/man2/open.2.html)
	  operations on the terminal are permitted.  (They fail with
	  **EBUSY**, except for a process with the **CAP_SYS_ADMIN**
	  capability.)
	
	- **TIOCGEXCL**
	  
	  Argument: **int** _argp_
	  
	  (since Linux 3.8) If the terminal is currently in
	  exclusive mode, place a nonzero value in the location
	  pointed to by _argp_; otherwise, place zero in _*argp_.
	
	- **TIOCNXCL**
	  
	  Argument: **void**
	
	  Disable exclusive mode.

- **Line discipline**
	- **TIOCGETD**
	  
	  Argument: **int** _argp_
	  
	  Get the line discipline of the terminal.
	
	- **TIOCSETD**
		
		Argument: **const int** _argp_
		
		Set the line discipline of the terminal.

- **Pseudoterminal ioctls**
	- **TIOCPKT**
	  
	  Argument: **const int** _argp_
	  
	  Enable (when *_argp_ is nonzero) or disable packet mode.
	  Can be applied to the master side of a pseudoterminal only
	  (and will return **ENOTTY** otherwise).  In packet mode, each
	  subsequent [read(2)](https://man7.org/linux/man-pages/man2/read.2.html) will return a packet that either
	  contains a single nonzero control byte, or has a single
	  byte containing zero ('\0') followed by data written on
	  the slave side of the pseudoterminal.  If the first byte
	  is not **TIOCPKT_DATA** (0), it is an OR of one or more of the
	  following bits:
	
	  - **TIOCPKT_FLUSHREAD**    The read queue for the
						   terminal is flushed.
	  - **TIOCPKT_FLUSHWRITE**   The write queue for the
						   terminal is flushed.
	  - **TIOCPKT_STOP**         Output to the terminal
						   is stopped.
	  - **TIOCPKT_START**        Output to the terminal
						   is restarted.
	
	  - **TIOCPKT_DOSTOP**       The start and stop
						   characters are **^S**/**^Q**.
	  - **TIOCPKT_NOSTOP**       The start and stop
						   characters are not
						   **^S**/**^Q**.
	
	  While packet mode is in use, the presence of control
	  status information to be read from the master side may be
	  detected by a [select(2)](https://man7.org/linux/man-pages/man2/select.2.html) for exceptional conditions or a
	  [poll(2)](https://man7.org/linux/man-pages/man2/poll.2.html) for the **POLLPRI** event.
	
	  This mode is used by **rlogin**(1) and **rlogind**(8) to implement
	  a remote-echoed, locally **^S**/**^Q** flow-controlled remote
	  login.
	
	- 	**TIOCGPKT**
		
		Argument: **const int** _argp_
		
		(since Linux 3.8) Return the current packet mode setting in the integer pointed to by _argp_.
	
	- **TIOCSPTLCK**
	  
	  Argument: **int** _argp_
	  
	  Set (if _*argp_ is nonzero) or remove (if _*argp_ is zero) the lock on the pseudoterminal slave device.  (See also
	  [unlockpt(3)](https://man7.org/linux/man-pages/man3/unlockpt.3.html).)
	
	- **TIOCGPTLCK**
		
		Argument: **int** _argp_
		
		(since Linux 3.8) Place the current lock state of the
		  pseudoterminal slave device in the location pointed to by _argp_.
	
	- **TIOCGPTPEER**
	  
	  Argument: **int** _flags_
	
	  (since Linux 4.13) Given a file descriptor in _fd_ that
	  refers to a pseudoterminal master, open (with the given
	  [open(2)](https://man7.org/linux/man-pages/man2/open.2.html)-style _flags_) and return a new file descriptor that
	  refers to the peer pseudoterminal slave device.  This
	  operation can be performed regardless of whether the
	  pathname of the slave device is accessible through the
	  calling process's mount namespace.
	
	  Security-conscious programs interacting with namespaces
	  may wish to use this operation rather than [open(2)](https://man7.org/linux/man-pages/man2/open.2.html) with
	  the pathname returned by [ptsname(3)](https://man7.org/linux/man-pages/man3/ptsname.3.html), and similar library
	  functions that have insecure APIs.  (For example,
	  confusion can occur in some cases using [ptsname(3)](https://man7.org/linux/man-pages/man3/ptsname.3.html) with a
	  pathname where a devpts filesystem has been mounted in a
	  different mount namespace.)
	  
	  The BSD ioctls **TIOCSTOP**, **TIOCSTART**, **TIOCUCNTL**, and **TIOCREMOTE** have not been implemented under Linux.

- **Modem control**
	- **TIOCMGET**
		  
		Argument: **int** _argp_
		
		Get the status of modem bits.
	
	- **TIOCMSET**
		
		Argument: **const int** _argp_
		
		Set the status of modem bits.
	
	- **TIOCMBIC**
		
		Argument: **const int** _argp_
		
		Clear the indicated modem bits.
	
	- **TIOCMBIS**
		
		Argument: **const int** _argp_
		
		Set the indicated modem bits.
		The following bits are used by the above ioctls:
		
		**TIOCM_LE**    DSR (data set ready/line enable)
		
		**TIOCM_DTR**   DTR (data terminal ready)
		
		**TIOCM_RTS**   RTS (request to send)
		
		**TIOCM_ST**    Secondary TXD (transmit)
		
		**TIOCM_SR**    Secondary RXD (receive)
		
		**TIOCM_CTS**   CTS (clear to send)
		
		**TIOCM_CAR**   DCD (data carrier detect)
		
		**TIOCM_CD**    see TIOCM_CAR
		
		**TIOCM_RNG**   RNG (ring)
		
		**TIOCM_RI**    see TIOCM_RNG
		
		**TIOCM_DSR**   DSR (data set ready)

	- **TIOCMIWAIT**
		
		Argument: **int** _arg_
	   
	   Wait for any of the 4 modem bits (DCD, RI, DSR, CTS) to change.  The bits of interest are specified as a bit mask in _arg_, by ORing together any of the bit values,	 **TIOCM_RNG**, **TIOCM_DSR**, **TIOCM_CD**, and **TIOCM_CTS**.  The caller	 should use **TIOCGICOUNT** to see which bit has changed.
	
	- **TIOCGICOUNT**
		
		Argument: **struct serial_icounter_struct** _argp_
		
		Get counts of input serial line interrupts (DCD, RI, DSR, CTS).  The counts are written to the	  _serial_icounter_struct_ structure pointed to by	  Note: both 1->0 and 0->1 transitions are counted, except	  for RI, where only 0->1 transitions are counted.

- **Marking a line as local**
	- **TIOCGSOFTCAR**
	 
	   Argument: **int** _argp_
	
	  ("Get software carrier flag") Get the status of the CLOCAL
	  flag in the c_cflag field of the _termios_ structure.
	
	- **TIOCSSOFTCAR**
	  
	  Argument: **const int** _argp_
	
	  ("Set software carrier flag") Set the CLOCAL flag in the
	  _termios_ structure when *_argp_ is nonzero, and clear it
	  otherwise.
	
	If the **CLOCAL** flag for a line is off, the hardware carrier detect
	(DCD) signal is significant, and an [open(2)](https://man7.org/linux/man-pages/man2/open.2.html) of the corresponding
	terminal will block until DCD is asserted, unless the **O_NONBLOCK**
	flag is given.  If **CLOCAL** is set, the line behaves as if DCD is
	always asserted.  The software carrier flag is usually turned on
	for local devices, and is off for lines with modems.

- **Linux-specific**
	For the **TIOCLINUX** ioctl, see [ioctl_console(2)](https://man7.org/linux/man-pages/man2/ioctl_console.2.html).

- **Kernel debugging**
	**#include <linux/tty.h>**
	- **TIOCTTYGSTRUCT**
	  
	  Argument: **struct tty_struct** _argp_
	  
	  Get the _tty_struct_ corresponding to _fd_.  This command was removed in Linux 2.5.67.

## RETURN VALUE         

   The [ioctl(2)](https://man7.org/linux/man-pages/man2/ioctl.2.html) system call returns 0 on success.  On error, it
   returns -1 and sets _[errno](https://man7.org/linux/man-pages/man3/errno.3.html)_ to indicate the error.

## ERRORS         

**EINVAL** Invalid command parameter.

**ENOIOCTLCMD**
	  Unknown command.

**ENOTTY** Inappropriate _fd_.

**EPERM**  Insufficient permission.

## EXAMPLES         

Check the condition of DTR on the serial port.
~~~c
       #include <fcntl.h>
       #include <stdio.h>
       #include <sys/ioctl.h>
       #include <unistd.h>

       int
       main(void)
       {
           int fd, serial;

           fd = open("/dev/ttyS0", O_RDONLY);
           ioctl(fd, TIOCMGET, &serial);
           if (serial & TIOCM_DTR)
               puts("TIOCM_DTR is set");
           else
               puts("TIOCM_DTR is not set");
           close(fd);
       }

       Get or set arbitrary baudrate on the serial port.

       /* SPDX-License-Identifier: GPL-2.0-or-later */

       #include <asm/termbits.h>
       #include <fcntl.h>
       #include <stdio.h>
       #include <stdlib.h>
       #include <sys/ioctl.h>
       #include <unistd.h>

       int
       main(int argc, char *argv[])
       {
       #if !defined BOTHER
           fprintf(stderr, "BOTHER is unsupported\n");
           /* Program may fallback to TCGETS/TCSETS with Bnnn constants */
           exit(EXIT_FAILURE);
       #else
           /* Declare tio structure, its type depends on supported ioctl */
       # if defined TCGETS2
           struct termios2 tio;
       # else
           struct termios tio;
       # endif
           int fd, rc;

           if (argc != 2 && argc != 3 && argc != 4) {
               fprintf(stderr, "Usage: %s device [output [input] ]\n", argv[0]);
               exit(EXIT_FAILURE);
           }

           fd = open(argv[1], O_RDWR | O_NONBLOCK | O_NOCTTY);
           if (fd < 0) {
               perror("open");
               exit(EXIT_FAILURE);
           }

           /* Get the current serial port settings via supported ioctl */
       # if defined TCGETS2
           rc = ioctl(fd, TCGETS2, &tio);
       # else
           rc = ioctl(fd, TCGETS, &tio);
       # endif
           if (rc) {
               perror("TCGETS");
               close(fd);
               exit(EXIT_FAILURE);
           }

           /* Change baud rate when more arguments were provided */
           if (argc == 3 || argc == 4) {
               /* Clear the current output baud rate and fill a new value */
               tio.c_cflag &= ~CBAUD;
               tio.c_cflag |= BOTHER;
               tio.c_ospeed = atoi(argv[2]);

               /* Clear the current input baud rate and fill a new value */
               tio.c_cflag &= ~(CBAUD << IBSHIFT);
               tio.c_cflag |= BOTHER << IBSHIFT;
               /* When 4th argument is not provided reuse output baud rate */
               tio.c_ispeed = (argc == 4) ? atoi(argv[3]) : atoi(argv[2]);

               /* Set new serial port settings via supported ioctl */
       # if defined TCSETS2
               rc = ioctl(fd, TCSETS2, &tio);
       # else
               rc = ioctl(fd, TCSETS, &tio);
       # endif
               if (rc) {
                   perror("TCSETS");
                   close(fd);
                   exit(EXIT_FAILURE);
               }

               /* And get new values which were really configured */
       # if defined TCGETS2
               rc = ioctl(fd, TCGETS2, &tio);
       # else
               rc = ioctl(fd, TCGETS, &tio);
       # endif
               if (rc) {
                   perror("TCGETS");
                   close(fd);
                   exit(EXIT_FAILURE);
               }
           }

           close(fd);

           printf("output baud rate: %u\n", tio.c_ospeed);
           printf("input baud rate: %u\n", tio.c_ispeed);

           exit(EXIT_SUCCESS);
       #endif
       }
~~~

## SEE ALSO     

[ldattach(8)](https://man7.org/linux/man-pages/man8/ldattach.8.html), [ioctl(2)](https://man7.org/linux/man-pages/man2/ioctl.2.html), [ioctl_console(2)](https://man7.org/linux/man-pages/man2/ioctl_console.2.html), [termios(3)](https://man7.org/linux/man-pages/man3/termios.3.html), [pty(7)](https://man7.org/linux/man-pages/man7/pty.7.html) Linux man-pages 6.04           2023-02-05                   ioctl_tty(2)
Pages that refer to this page: [ioctl(2)](https://man7.org/linux/man-pages/man2/ioctl.2.html),  [ioctl_console(2)](https://man7.org/linux/man-pages/man2/ioctl_console.2.html),  [poll(2)](https://man7.org/linux/man-pages/man2/poll.2.html),  [termios(3)](https://man7.org/linux/man-pages/man3/termios.3.html),  [tty(4)](https://man7.org/linux/man-pages/man4/tty.4.html),  [pty(7)](https://man7.org/linux/man-pages/man7/pty.7.html),  [termio(7)](https://man7.org/linux/man-pages/man7/termio.7.html)
