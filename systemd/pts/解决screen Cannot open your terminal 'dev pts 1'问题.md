---
date: 2023-08-30 22:03
title: 解决screen Cannot open your terminal 'dev pts 1'问题
tags:
- pts 
- tty
---




# 解决screen Cannot open your terminal '/dev/pts/1'问题

转自:  http://blog.sina.com.cn/s/blog_704836f401010osn.html  

### 问题描述:

 userA首先登录系统，使用screen开启了一个[session](https://so.csdn.net/so/search?q=session&spm=1001.2101.3001.7020)，然后detach这个窗口。

 userB然后登录系统，通过su -userA 变成userA，然后使用screen-r 恢复之前detached窗口，这时系统报如下错误:

 

~~~sh
 Cannot open your terminal'/dev/pts/1' - please check.
~~~

 

### 解决方法:

  userB在su -userA以后，执行如下命令即可:
 
~~~sh
  script /dev/null
~~~

 

注意: 有人提到 chmod 777/dev/pts/1，这么干的人真是误人子弟，虽然这么做的确能解决这个问题，但是会带来极大的安全问题！！！

 

为什么这条命令能解决问题?

 

一般人看到上面这里估计就马上回去试验了，但是，等等，你不想知道为什么这个命令会有作用吗？它是怎么起作用的呢？

 

我们来过一遍整个的操作步骤:

 

首先，usera登录到系统中，我们使用tty命令查看一下分配给他的tty，然后看一下这个tty的权限，然后用户执行screen命令。

 

~~~sh
usera@localhost ~ $ ssh usera@remotehost  
usera@remotehost ~ $ tty  
/dev/pts/1  
usera@remotehost ~ $ ls -l /dev/pts/1  
crw--w---- 1 usera tty 136, 12011-01-09 20:14 /dev/pts/1  
usera@remotehost ~ $ screen
~~~

 

我们观察上边的输出，发现usera对于/dev/pts/1具有读写权限，它所在组成员对这个tty具有写权限，其他用户不能访问这个tty。

 

然后，userb也登录到系统中，同样我们使用tty命令查看一下分配给他的tty，然后看一下这个tty的权限

~~~sh
userb@localhost ~ $ ssh userb@remotehost  
userb@remotehost ~ $ tty  
/dev/pts/2  
userb@remotehost ~ $ ls -l /dev/pts/2  
crw--w---- 1 userb tty 136, 22011-01-09 20:20 /dev/pts/2
~~~

 

观察输出，userb被分配了/dev/pts/2，也是对于/dev/pts/2具有读写权限，它所在组成员对这个tty具有写权限，其他用户不能访问这个tty。

 

然后userb通过su -usera命令变成usera，同样我们使用tty命令查看一下分配给他的tty，然后看一下这个tty的权限

~~~sh
userb@remotehost ~ $ sudo su - usera  
[sudo] password for userb:  
usera@remotehost ~ $ tty  
/dev/pts/2  
usera@remotehost ~ $ ls -l /dev/pts/2  
crw--w---- 1 userb tty 136, 22011-01-09 20:20 /dev/pts/2
~~~

 

AHA!!  注意了，我们看到虽然userb已经变成了usera，但是他所使用的tty并没有改变，仍然是/dev/pts/2。这就是为什么执行screen命令会报错的原因了，因为所有命令此时是使用usera帐户执行的，但是/dev/pts/2的读写权限属于userb，所以所有试图控制/dev/pts/2的访问都被拒绝了！

 

那么我们接下来看一下 script/dev/null做了些什么，使得screen命令能执行呢？

 

~~~sh
usera@remotehost ~ $ script /dev/null  
Script started, file is /dev/null  
usera@remotehost ~ $ tty  
/dev/pts/3  
usera@remotehost ~ $ ls -l /dev/pts/3  
crw--w---- 1 usera tty 136, 32011-01-09 20:36 /dev/pts/3
~~~

 

AHA!!! 看到了吗？我们实际上是得到了一个新的tty ---> /dev/pts/3，因此screen命令能够执行了，因为 /dev/pts/3这个tty的所有者是usera！