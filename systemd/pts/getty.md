---
date: 2023-08-11 00:00
title: archlinuxwiki.getty
tags:
- tty
- getty 
---



# getty

- [Page](https://wiki.archlinux.org/title/Getty "View the content page [alt-shift-c]")
- [Discussion](https://wiki.archlinux.org/title/Talk:Getty "Discussion about the content page [alt-shift-t]")

- [Read](https://wiki.archlinux.org/title/Getty)
- [View source](https://wiki.archlinux.org/index.php?title=Getty&action=edit "This page is protected.
    You can view its source [alt-shift-e]")
- [View history](https://wiki.archlinux.org/index.php?title=Getty&action=history "Past revisions of this page [alt-shift-h]")

	(Redirected from [Automatic login to virtual console](https://wiki.archlinux.org/index.php?title=Automatic_login_to_virtual_console&redirect=no "Automatic login to virtual console")Related articles

- [Display manager](https://wiki.archlinux.org/title/Display_manager "Display manager")

A [getty](https://en.wikipedia.org/wiki/getty_(Unix) "w:getty (Unix)") is the generic name for a program which manages a terminal line and its connected terminal. Its purpose is to protect the system from unauthorized access. Generally, each getty process is started by [systemd](https://wiki.archlinux.org/title/Systemd "Systemd") and manages a single terminal line.

## 1. Installation

_agetty_ is the default getty in Arch Linux, as part of the [util-linux](https://archlinux.org/packages/?name=util-linux) package.

Alternatives include:

- **mingetty** — A minimal getty which allows automatic logins.

	[mingetty](https://aur.archlinux.org/packages/mingetty/)AUR || [mingetty](https://aur.archlinux.org/packages/mingetty/)AUR

- **mgetty** — A versatile program to handle all aspects of a modem under Unix.

	[http://mgetty.greenie.net/](http://mgetty.greenie.net/) || [mgetty](https://aur.archlinux.org/packages/mgetty/)AUR

## 2. Tips and tricks

### 2.1. Staircase effect

_agetty_ modifies the TTY settings while waiting for a login so that the newlines are not translated to CR-LFs. This tends to cause a "staircase effect" for messages printed to the console.

It is entirely harmless, but in the event it persists once logged, you can fix this behavior with:
~~~sh
$ stty onlcr
~~~

See this [forums discussion](https://bbs.archlinux.org/viewtopic.php?id=264094) on the subject.

### 2.2. Add additional virtual consoles

Agetty manages virtual consoles and six of these virtual consoles are provided by default in Arch Linux. They are usually accessible by pressing `Ctrl+Alt+F1` through `Ctrl+Alt+F6`.

Open the file `/etc/systemd/logind.conf` and set the option `NAutoVTs=6` to the number of virtual terminals that you want at boot.

If needed, it is possible to temporarily [start](https://wiki.archlinux.org/title/Start "Start") a `getty@tty_N_.service` service directly.

### 2.3. Automatic login to virtual console

Configuration relies on systemd unit [drop-in files](https://wiki.archlinux.org/title/Drop-in_file "Drop-in file") to override the default parameters passed to _agetty_.

Configuration differs for virtual versus serial consoles. In most cases, you want to set up automatic login on a virtual console, (whose device name is `tty_N_`, where `_N_` is a number). The configuration of automatic login for serial consoles will be slightly different. Device names of the serial consoles look like `ttyS_N_`, where `_N_` is a number.

**Tip:** Consider using [greetd](https://wiki.archlinux.org/title/Greetd "Greetd")'s auto-login feature. It will not auto-login a second time if the initial session exits, but will show a login screen instead.

#### 2.3.1. Virtual console

Create a [drop-in file](https://wiki.archlinux.org/title/Drop-in_file "Drop-in file") for `getty@tty1.service` with the following contents:
~~~sh
/etc/systemd/system/getty@tty1.service.d/autologin.conf

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --autologin _username_ %I $TERM
~~~

**Tip:**

- The option `Type=idle` found in the default `getty@.service` will delay the service startup until all jobs (state change requests to units) are completed in order to avoid polluting the login prompt with boot-up messages. When [starting X automatically](https://wiki.archlinux.org/title/Start_X_at_login "Start X at login"), it may be useful to start `getty@tty1.service` immediately by adding `Type=simple` into the drop-in file. Both the init system and _startx_ can be [silenced](https://wiki.archlinux.org/title/Silent_boot "Silent boot") to avoid the interleaving of their messages during boot-up.
- See [Silent boot#agetty](https://wiki.archlinux.org/title/Silent_boot#agetty "Silent boot") for an example virtual console drop-in file which hides the login prompt completely.
- The above snippet will cause the loginctl session type to be set to `tty`. If desirable (for example if [starting X automatically](https://wiki.archlinux.org/title/Start_X_at_login "Start X at login")), it is possible to manually set the session type to `wayland` or `x11` by adding `Environment=XDG_SESSION_TYPE=x11` or `Environment=XDG_SESSION_TYPE=wayland` into this file.

If you do not want full automatic login, but also do not want to type your username, see [#Prompt only the password for a default user in virtual console login](https://wiki.archlinux.org/title/Getty#Prompt_only_the_password_for_a_default_user_in_virtual_console_login).

If you want to use a _tty_ other than _tty1_, see [systemd/FAQ#How do I change the default number of gettys?](https://wiki.archlinux.org/title/Systemd/FAQ#How_do_I_change_the_default_number_of_gettys? "Systemd/FAQ").

#### 2.3.2. Serial console

Create a [drop-in file](https://wiki.archlinux.org/title/Drop-in_file "Drop-in file"):

~~~sh
/etc/systemd/system/serial-getty@ttyS0.service.d/autologin.conf

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --keep-baud --autologin _username_ 115200,57600,38400,9600 - $TERM

~~~
#### 2.3.3. Nspawn console

To configure auto-login for a [systemd-nspawn](https://wiki.archlinux.org/title/Systemd-nspawn "Systemd-nspawn") container, override `console-getty.service` by creating a [drop-in file](https://wiki.archlinux.org/title/Drop-in_file "Drop-in file"):

~~~sh
/etc/systemd/system/console-getty.service.d/autologin.conf

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --keep-baud --autologin _username_ - 115200,38400,9600 $TERM
~~~

If `machinectl login _my-container_` method is used to access the container, also adjust the `container-getty@.service` template that manages `pts/[0-9]` pseudo ttys:

~~~sh
/etc/systemd/system/container-getty@.service.d/autologin.conf

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --keep-baud --autologin _username_ - 115200,38400,9600 $TERM
~~~

### 2.4. Prompt only the password for a default user in virtual console login

Getty can be used to login from a virtual console with a default user, typing the password but without needing to insert the username. For instance, to prompt the password for `_username_` on `tty1`:

~~~sh
/etc/systemd/system/getty@tty1.service.d/skip-username.conf

[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -- _username_' --noclear --skip-login - $TERM
~~~

and then [enable](https://wiki.archlinux.org/title/Enable "Enable") `getty@tty1`.

### 2.5. Have boot messages stay on tty1

By default, Arch has the `getty@tty1` service enabled. The service file already passes `--noclear`, which stops agetty from clearing the screen. However [systemd](https://wiki.archlinux.org/title/Systemd "Systemd") clears the screen before starting it. To disable this behavior, create a [drop-in file](https://wiki.archlinux.org/title/Drop-in_file "Drop-in file"):

~~~sh
/etc/systemd/system/getty@tty1.service.d/noclear.conf

[Service]
TTYVTDisallocate=no
~~~

**Note:**

- Make sure to remove `quiet` from the [kernel parameters](https://wiki.archlinux.org/title/Kernel_parameter "Kernel parameter").
- Late KMS starting may cause the first few boot messages to clear. See [NVIDIA#Early loading](https://wiki.archlinux.org/title/NVIDIA#Early_loading "NVIDIA").

## 3. See also

- [systemd#Change default target to boot into](https://wiki.archlinux.org/title/Systemd#Change_default_target_to_boot_into "Systemd")
- [The TTY demystified](https://www.linusakesson.net/programming/tty/)
- [Wikipedia:tty (unix)](https://en.wikipedia.org/wiki/tty_(unix) "wikipedia:tty (unix)")