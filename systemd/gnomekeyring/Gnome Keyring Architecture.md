---
date: 2023-09-25 09:59
title: Gnome Keyring Architecture
tags:
  - keyring
---



# Gnome Keyring Architecture

Gnome keyring has many parts which work together to provide the services related to certificate, key, and secret storage. Below you'll find an overview image of how things work together, and after that a breakdown of each component.

Loose coupling of components (using published standards or public APIs) are shown with dotted lines.

[![attachment:gnome-keyring-architecture.png](https://wiki.gnome.org/Projects/GnomeKeyring/Architecture?action=AttachFile&do=get&target=gnome-keyring-architecture-small.png "attachment:gnome-keyring-architecture.png")](https://wiki.gnome.org/Projects/GnomeKeyring/Architecture?action=AttachFile&do=view&target=gnome-keyring-architecture.png "{{attachment:gnome-keyring-architecture-small.png}}")

---

## daemon-auth

/daemon/pkcs11/

Loads and authenticates various PKCS#11 components running inside the daemon.

- Loads various underlying PKCS#11 components when the daemon starts.
- Is a PKCS#11 wrapper of a lower level PKCS#11 module.
- Adds 'protected authentication path' and prompts using the ui code when the lower PKCS#11 module needs unlocking or other authentication.
- Automatically unlocks objects or tokens in the PKCS#11 module if their information is present in the 'login' keyring.
- Enters the global async lock when needed.

---

## daemon-io

/daemon/

- Implements the server side of the private binary protocol with which we communicate with the library and pam components.
- Lives inside the global async lock.

---

## gcr

/gcr/

A public library that implements UI bits for certificates, keys, and does parsing, importing and related operations.

- Depends on gp11, libgcrypt, libtasn1, glib and gtk+.
- Designed to be (eventually) agnostic of the PKCS#11 provider in use.

---

## gp11

/gp11/

A public library with GObject based bindings for PKCS#11.

- Depends on glib.
- Supports asynchronous calls in gio style.
- Fully multi-thread capable.

---

## keyrings

/keyrings/

Manages the keyrings that store secrets (ie: passwords).

- GObject based.
- Documentation about [keyring formats](https://wiki.gnome.org/GnomeKeyring/KeyringFormats).
    
- Lives inside the global async lock.

---

## library

/library/

The libgnome-keyring.so library.

- Public library linked to dbus and glib.
- All the public gnome_keyring_* API lives here.
- Communicates with the daemon using a private binary protocol similar to SSH agent protocol.
- Contains public functions for allocating non-pageable memory.

---

## pam

/pam/

A PAM module which starts the daemon and unlocks the 'login' keyring.

- Module is a pure C with no extra dependencies.
- Standard PAM module which implements auth, session and password hooks.
- More [info about the PAM module](https://wiki.gnome.org/GnomeKeyring/Pam).
    
- Information about [how the daemon is started](https://wiki.gnome.org/GnomeKeyring/RunningDaemon).
    
- Implements a small part of gnome keyring binary protocol rather than linking to libgnome-keyring.so.

---

## plex-layer

/pkcs11/plex-layer/

Combines multiple PKCS#11 modules into a single PKCS#11 module with multiple slots.

- Session, object and slot handles are mapped up and down on each call.

---

## rpc-layer

/pkcs11/rpc-layer/

Marshalls PKCS#11 API between applications and the daemon. Consists of a server component, and a public module loaded by applications.

- This is the gnome-keyring-pkcs11.so that PKCS#11 aware libraries actually load.
- Uses unix sockets to communicate.
- Uses a private binary protocol, similar to the SSH agent protocol.
- Each simultaneous call from an application comes in on a seperate connection, up to a certain limit.
- Each connection is serviced by a different thread.
- Server component does its own locking.

---

## roots-store

/pkcs11/roots-store/

A read-only PKCS#11 module that loads trusted root certificate authority certificates.

- Reads from a directory similar to OpenSSL (ie: /etc/ssl/certs).
- Configured at build time which directory, usually only root writable.
- Implement netscape trust objects so that trust is exported to NSS apps.

---

## ssh-agent

/pkcs11/ssh-agent/

An openssh compatible SSH agent that uses PKCS#11 for key storage and crypto operations.

- Uses gp11 code to ease PKCS#11 usage.
- Does its own locking.
- Each connection to the agent is a thread.

---

## ssh-store

/pkcs11/ssh-store/

A read-only PKCS#11 module that loads keys from ~/.ssh/id_?sa*

- Each private key must have a public key with *.pub extension.

---

## ui

/daemon/ui/

Displays the various gnome-keyring prompts.

- Depends on GTK+
- A different gnome-keyring-ask is run to show each prompt.
- Daemon runs ask tool and sends it prompt information via stdin, using the desktop file format.
- Tool responds via stdout to the daemon.
- Only shows one prompt at a time and tries to prevent repeated cancelled prompts for same thing.

---

## user-store

/pkcs11/user-store/

A writable PKCS#11 module that stores certificates and keys.

- Stores in ~/.gnome2/keyrings
- Stores objects in standard public formats like PKCS#8, but has metadata in its own private database.

Projects/GnomeKeyring/Architecture (last edited 2013-11-26 20:21:43 by [WilliamJonMcCann](https://wiki.gnome.org/WilliamJonMcCann "WilliamJonMcCann"))

  

Copyright © 2005 - 2023 [The GNOME Project](https://www.gnome.org/). Hosted by [Red Hat](http://www.redhat.com/).