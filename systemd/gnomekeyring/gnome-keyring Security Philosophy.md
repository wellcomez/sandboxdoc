---
date: 2023-09-25 10:06
title: gnome-keyring Security Philosophy
tags:
  - keyring
---



# gnome-keyring Security Philosophy

Here's the security philosophy of the gnome-keyring project.

## Security Theater

It is a goal of gnome-keyring to not do things which make the user feel secure but are not actually 'secure'.

An example of security theater is giving the illusion that somehow one application running in a security context (such as your user session) can keep information from another application running in the same security context.

Another example of security theater might be: obscuring, but not actually encrypting secrets.

Sadly there are a few remnants of security theater left in gnome-keyring from previous versions, but we're actively tackling those issues and looking for better solutions.

## Passive Attacks

A passive attack is one where the attacker cannot modify anything inside the security context. In the context of gnome-keyring a passive attacker would not have access to your user session.

gnome-keyring's goal is to protect you from passive attacks. Passive attacks are those that could take place while you're not logged in, or from another user (non-root) running on the same computer.

Some steps that gnome-keyring takes to prevent passive attacks are:

- Encrypt your secrets (passwords and keys) so that if your computer is stolen, or hard disk is copied they won't be available to the attacker.
- Try to keep your secrets from being swapped out or otherwise written to disk.
- Hunkering down and discarding all secrets when your computer is locked.
- Only allow secrets to be used and dispensed by the same user.

We're not absolutely perfect in this regard yet, but we'd like to think we're getting close.

## Active Attacks

An active attack is where the attacker can change something in your security context. In the context of gnome-keyring an active attacker would have access to your user session in some way. An active attacker might install an application on your computer, display a window, listen into the X events going to another window, read through your memory, snoop on you from a root account etc.

While it'd be nice for gnome-keyring to someday be hardened against active attacks originating from the user's session, the reality is that the free software "desktop" today just isn't architected with those things in mind. We need completion and integration things like the following. Kudos to the great folks working on parts of this stuff:

- Trusted X (for prompting)
- Pervasive use of security contexts for different apps (SELinux, [AppArmor](https://wiki.gnome.org/AppArmor))
    
- Application signing (for ACLs)

We're not against the goal of protecting against active attacks, but without hardening of the desktop in general, such efforts amount to security theater.

## Discussion and Progress

gnome-keyring is not opinionated software. Did you have a new great idea for how we can solve a security problem? Do you think we should be doing things differently? Is our understanding of security stupid and broken?

The [GNOME discourse instance](https://discourse.gnome.org/) is the place to discuss ideas, rants, questions. Join in there, and either we'll gain a great new idea, or you'll gain a fuller understanding of the issue.

And if you want to join in and help with the code: Awesome!

Projects/GnomeKeyring/SecurityPhilosophy (last edited 2020-06-19 11:50:45 by [NielsDeGraef](https://wiki.gnome.org/NielsDeGraef "nielsdg"))