---
date: 2023-09-25 10:04
title: GNOME Keyring archlinux
tags:
  - keyring
---



# GNOME/Keyring

- [Page](https://wiki.archlinux.org/title/GNOME/Keyring "View the content page [alt-shift-c]")
- [Discussion](https://wiki.archlinux.org/title/Talk:GNOME/Keyring "Discussion about the content page [alt-shift-t]")

- [Read](https://wiki.archlinux.org/title/GNOME/Keyring)
- [View source](https://wiki.archlinux.org/index.php?title=GNOME/Keyring&action=edit "This page is protected.
    You can view its source [alt-shift-e]")
- [View history](https://wiki.archlinux.org/index.php?title=GNOME/Keyring&action=history "Past revisions of this page [alt-shift-h]")


[GNOME Keyring](https://wiki.gnome.org/Projects/GnomeKeyring) is "a collection of components in GNOME that store secrets, passwords, keys, certificates and make them available to applications."

## Installation

[gnome-keyring](https://archlinux.org/packages/?name=gnome-keyring) is a member of the [gnome](https://archlinux.org/groups/x86_64/gnome/) group is thus usually present on systems running GNOME. The package can otherwise be [installed](https://wiki.archlinux.org/title/Install "Install") on its own. [libsecret](https://archlinux.org/packages/?name=libsecret) should also be installed to grant other applications access to your keyrings. Although [libgnome-keyring](https://archlinux.org/packages/?name=libgnome-keyring) is deprecated (and superseded by _libsecret_), it may still be required by certain applications.

The gnome-keyring-daemon is automatically started via a systemd user service upon logging in. It can also be started upon request via a socket.

Extra utilities related to GNOME Keyring include:

- **secret-tool** — Access the GNOME Keyring (and any other service implementing the [DBus Secret Service API](https://specifications.freedesktop.org/secret-service/)) from the command line.

[https://wiki.gnome.org/Projects/Libsecret](https://wiki.gnome.org/Projects/Libsecret) || [libsecret](https://archlinux.org/packages/?name=libsecret)

- **lssecret** — List all secret items using _libsecret_ (e.g. GNOME Keyring).

[https://gitlab.com/GrantMoyer/lssecret](https://gitlab.com/GrantMoyer/lssecret) || [lssecret-git](https://aur.archlinux.org/packages/lssecret-git/)AUR

## Manage using GUI

You can manage the contents of GNOME Keyring using Seahorse; [install](https://wiki.archlinux.org/title/Install "Install") the [seahorse](https://archlinux.org/packages/?name=seahorse) package.

Passwords for keyrings (e.g., the default keyring, "Login") can be changed and even removed. See [Create a new keyring](https://help.gnome.org/users/seahorse/stable/keyring-create.html) and [Update the keyring password](https://help.gnome.org/users/seahorse/stable/keyring-update-password.html) in GNOME Help for more information.

## Using the keyring

The [PAM](https://wiki.archlinux.org/title/PAM "PAM") module _pam_gnome_keyring.so_ initialises GNOME Keyring partially, unlocking the default _login_ keyring in the process. The gnome-keyring-daemon is automatically started with a systemd user service.

### PAM step

**Note:** To use automatic unlocking **without automatic login**, the password for the user account should be the same as the default keyring. See [#Automatically change keyring password with user password](https://wiki.archlinux.org/title/GNOME/Keyring#Automatically_change_keyring_password_with_user_password).

**Tip:**

- To use automatic unlocking with automatic login, you can set a blank password for the default keyring. Note that the contents of the keyring are stored unencrypted in this case.
- Alternatively, if using GDM and LUKS, GDM can unlock your keyring if it matches your LUKS password. For this to work, you need to use the [systemd init in your mkinitcpio.conf](https://wiki.archlinux.org/title/Mkinitcpio#Common_hooks "Mkinitcpio") as well as the [appropriate kernel parameters](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#Using_systemd-cryptsetup-generator "Dm-crypt/System configuration"). See [[1]](https://reddit.com/r/Fedora/comments/jwnqq5/) for more details.
- If you desire to skip the PAM step, the default keyring must be unlocked manually or via another method. See [#Launching gnome-keyring-daemon outside desktop environments (KDE, GNOME, XFCE, ...)](https://wiki.archlinux.org/title/GNOME/Keyring#Launching_gnome-keyring-daemon_outside_desktop_environments_(KDE,_GNOME,_XFCE,_...)) and the [GnomeKeyring wiki](https://wiki.gnome.org/Projects/GnomeKeyring/RunningDaemon).

When using a display manager, the keyring works out of the box for most cases. [GDM](https://wiki.archlinux.org/title/GDM "GDM"), [LightDM](https://wiki.archlinux.org/title/LightDM "LightDM"), [LXDM](https://wiki.archlinux.org/title/LXDM "LXDM"), and [SDDM](https://wiki.archlinux.org/title/SDDM "SDDM") already have the necessary PAM configuration. For a display manager that does not automatically unlock the keyring edit the appropriate file instead of `/etc/pam.d/login` as mentioned below.

<mark style="background: #D2B3FFA6;">When using console-based login, edit `/etc/pam.d/login`:</mark>

<mark style="background: #35E708E3;">Add `auth optional pam_gnome_keyring.so` at the end of the `auth` section and `session optional pam_gnome_keyring.so auto_start` at the end of the `session` section.
</mark>
/etc/pam.d/login

#%PAM-1.0

auth       required     pam_securetty.so
auth       requisite    pam_nologin.so
auth       include      system-local-login
**auth       optional     pam_gnome_keyring.so**
account    include      system-local-login
session    include      system-local-login
**session    optional     pam_gnome_keyring.so auto_start**

## SSH keys

GNOME Keyring can act as a wrapper around [ssh-agent](https://wiki.archlinux.org/title/SSH_keys#ssh-agent "SSH keys"). In that mode, it will display a GUI password entry dialog each time you need to unlock an SSH key. The dialog includes an checkbox to remember the password you type, which, if selected, will allow fully passwordless use of that key in the future as long as your login keyring is unlocked.

From GNOME 41 (released September 2021) until at least this writing (April 2023), the SSH functionality of gnome-keyring-daemon has been [duplicated](https://gitlab.gnome.org/GNOME/gcr/-/merge_requests/67) into `/usr/lib/gcr-ssh-agent`, which is part of [gcr](https://archlinux.org/packages/?name=gcr). The eventual [plan](https://fedoraproject.org/wiki/Changes/ModularGnomeKeyring) is to remove the implementation in gnome-keyring-daemon, but that effort seems to have stalled. Until that happens, you should be aware of which implementation you're using, since each is set up and used in a different way:

### gcr-ssh-agent

The gcr-ssh-agent implementation is the more plug-and-play of the two. Since it will also eventually be the only implementation, you should use it unless you have good reason not to.

All you need to do to use this implementation is

1. <mark style="background: #35E708E3;">Enable the `gcr-ssh-agent.socket` systemd user unit</mark>, e.g. by running `systemctl --user enable gcr-ssh-agent.socket`.
2.<mark style="background: #FFB8EBA6;"> Run SSH commands with the `SSH_AUTH_SOCK` environment variable set to `$XDG_RUNTIME_DIR/gcr/ssh`.</mark> There are [many ways](https://wiki.archlinux.org/title/Environment_variables#Defining_variables "Environment variables") to set environment variables, and the one you use will depend on your specific setup and preferences.

### gnome-keyring-daemon "ssh" component

If you do decide to use the older gnome-keyring-daemon implementation, you'll need to ensure `gnome-keyring-daemon` gets started with **ssh** in its `--components` argument. For desktop environments using [XDG Autostart](https://wiki.archlinux.org/title/XDG_Autostart "XDG Autostart"), that's done via `/etc/xdg/autostart/gnome-keyring-ssh.desktop` automatically. For others, you'll need to edit `ExecStart` of the `gnome-keyring-daemon.service` user unit, since it doesn't start the SSH component by default. See [Systemd#Editing provided units](https://wiki.archlinux.org/title/Systemd#Editing_provided_units "Systemd") for guidance.

Once you've ensured the component runs, set `SSH_AUTH_SOCK` to `$XDG_RUNTIME_DIR/keyring/ssh` in the same manner described above. Note that gcr-ssh-agent and gnome-keyring-daemon listen on different socket paths despite containing nearly-identical implementations.

### Using

Regardless of the implementation you choose, you can run

~~~sh
$ ssh-add -L
~~~

to list loaded SSH keys in the running agent. This can help ensure you started the appropriate service and set `SSH_AUTH_SOCK` correctly.

To permanently save the a passphrase in the keyring, use ssh-askpass from the [seahorse](https://archlinux.org/packages/?name=seahorse) package:

~~~sh
$ /usr/lib/seahorse/ssh-askpass my_key
~~~

To manually add an SSH key from another directory:

~~~sh
$ ssh-add ~/.private/id_rsa
Enter passphrase for ~/.private/id_rsa:
~~~

**Note:** You have to have the corresponding _.pub_ file in the same directory as the private key (`~/.ssh/id_rsa.pub` in the example). Also, make sure that the public key is the file name of the private key plus _.pub_ (for example, `my_key.pub`).

To disable all manually added keys:

~~~sh
$ ssh-add -D
~~~

### Disabling

If you wish to run an alternative SSH agent (e.g. [ssh-agent](https://wiki.archlinux.org/title/SSH_keys#ssh-agent "SSH keys") directly or [gpg-agent](https://wiki.archlinux.org/title/Gpg-agent "Gpg-agent")), it's a good idea to disable GNOME Keyring's ssh-agent wrapper. Doing so isn't strictly necessary, since each agent listens on a different socket and `SSH_AUTH_SOCK` can be used to choose between them, but it can make debugging issues easier.

To disable the gcr-ssh-agent implementation, ensure `gcr-ssh-agent.socket` and `gcr-ssh-agent.service` are both disabled and stopped in systemd.

To disable the gnome-keyring-daemon implementation in an account-local way, copy `/etc/xdg/autostart/gnome-keyring-ssh.desktop` to `~/.config/autostart/` and then append the line `Hidden=true` to the copied file. Also undo any `gnome-keyring-daemon.service` edits you made as per the above instructions.

## Tips and tricks

### Integration with applications

- [Chromium](https://wiki.archlinux.org/title/Chromium#Force_a_password_store "Chromium")
	#### Force a password store
	
	Chromium uses a password store to store your passwords and the _Chromium Safe Storage_ key, which is used to encrypt cookie values. [[11]](https://codereview.chromium.org/24734007)
	
	By default Chromium auto-detects which password store to use, which can lead to you apparently losing your passwords and cookies when switching to another desktop environment or window manager.
	
	You can force Chromium to use a specific password store by launching it with the `--password-store` flag with one of following the values [[12]](https://chromium.googlesource.com/chromium/src/+/master/docs/linux/password_storage.md):
	
	- `gnome-libsecret`, uses [Gnome Keyring](https://wiki.archlinux.org/title/Gnome_Keyring "Gnome Keyring") via [libsecret](https://gitlab.gnome.org/GNOME/libsecret).
	- `kwallet5`, uses [KDE Wallet](https://wiki.archlinux.org/title/KDE_Wallet "KDE Wallet")
	- `basic`, saves the passwords and the cookies' encryption key as plain text in the file `Login Data`
	- `detect`, the default auto-detect behavior
	
	For example, to force Chromium to use Gnome Keyring in another desktop or WM use `--password-store=gnome-libsecret`, see [#Making flags persistent](https://wiki.archlinux.org/title/Chromium#Making_flags_persistent) for making it permanent.
	
	When using a password store of another desktop environment you probably also want to unlock it automatically. See [GNOME/Keyring#Using the keyring](https://wiki.archlinux.org/title/GNOME/Keyring#Using_the_keyring "GNOME/Keyring") and [KDE Wallet#Unlock KDE Wallet automatically on login](https://wiki.archlinux.org/title/KDE_Wallet#Unlock_KDE_Wallet_automatically_on_login "KDE Wallet").
### Flushing passphrases

~~~sh
$ gnome-keyring-daemon -r -d
~~~

This command starts gnome-keyring-daemon, shutting down previously running instances.

### Git integration

The GNOME keyring is useful in conjunction with [Git](https://wiki.archlinux.org/title/Git "Git") when you are pushing over HTTPS. The [libsecret](https://archlinux.org/packages/?name=libsecret) package [needs to be installed for this functionality to be available](https://wiki.archlinux.org/title/GNOME/Keyring#Installation).

Configure Git to use the _libsecret_ helper:

~~~sh
$ git config --global credential.helper /usr/lib/git-core/git-credential-libsecret
~~~

The next time you run `git push`, you will be asked to unlock your keyring if it is not already unlocked.

### GnuPG integration

Several applications which use GnuPG require a `pinentry-program` to be set. Set the following to use GNOME 3 pinentry for GNOME Keyring to manage passphrase prompts.

~~~sh
~/.gnupg/gpg-agent.conf
~~~

pinentry-program /usr/bin/pinentry-gnome3

Another option is to [force loopback for GPG](https://wiki.archlinux.org/title/GnuPG#Unattended_passphrase "GnuPG") which should allow the passphrase to be entered in the application.

### Renaming a keyring

The display name for a keyring (i.e., the name that appears in Seahorse and from `file`) can be changed by [changing the value of display-name in the unencrypted keyring file](https://ttboj.wordpress.com/2013/01/27/renaming-a-gnome-keyring-for-seahorse-the-passwords-and-keyrings-application/). Keyrings will usually be stored in `~/.local/share/keyrings/` with the _.keyring_ file extension.

### Automatically change keyring password with user password

**Note:** This only affects the default keyring.

Add `password optional pam_gnome_keyring.so` to the end of `/etc/pam.d/passwd`.

/etc/pam.d/passwd

\#%PAM-1.0

\#password	required	pam_cracklib.so difok=2 minlen=8 dcredit=2 ocredit=2 retry=3
\#password	required	pam_unix.so sha512 shadow use_authtok
password	required	pam_unix.so sha512 shadow nullok
**password	optional	pam_gnome_keyring.so**

### Launching gnome-keyring-daemon outside desktop environments (KDE, GNOME, XFCE, ...)

**The factual accuracy of this article or section is disputed.**

**Reason:** At least [xinit](https://wiki.archlinux.org/title/Xinit "Xinit") and [SDDM](https://wiki.archlinux.org/title/SDDM "SDDM") execute all scripts from `/etc/X11/xinit/xinitrc.d/` and [sway](https://wiki.archlinux.org/title/Sway "Sway") provides `/etc/sway/config.d/50-systemd-user.conf` so the problem is not being "outside desktop environments". If gnome-keyring requires [XDG Autostart](https://wiki.archlinux.org/title/XDG_Autostart "XDG Autostart"), the installation/configuration section should say so. (Discuss in [Talk:GNOME/Keyring#Launching gnome-keyring-daemon outside desktop environments (KDE,_GNOME,_XFCE,_...)](https://wiki.archlinux.org/title/Talk:GNOME/Keyring#Launching_gnome-keyring-daemon_outside_desktop_environments_(KDE,_GNOME,_XFCE,_...) "Talk:GNOME/Keyring"))

If you are using sway, i3, or any window manager that does not execute

- `/etc/xdg/autostart/gnome-keyring-*.desktop`
- `/etc/X11/xinit/xinitrc.d/50-systemd-user.sh`

<mark style="background: #35E708E3;">your window manager needs to execute the following commands during window manager startup. The commands do not need to be executed in any specific order.</mark>
~~~sh
dbus-update-activation-environment DISPLAY XAUTHORITY WAYLAND_DISPLAY
~~~

or

~~~sh
dbus-update-activation-environment --all
~~~

<mark style="background: #35E708E3;">This command passes environment variables from the window manager to session dbus. Without this, GUI prompts cannot be triggered over DBus. For example, this is required for seahorse password prompt.</mark>

<mark style="background: #FF5582A6;">This is required because session dbus is started before graphical environment is started. Thus, session dbus does not know about the graphical environment you are in. </mark>Someone or something has to teach session dbus about the graphical environment by passing environment variables describing the graphical environment to session dbus.
~~~sh
gnome-keyring-daemon --start --components=secrets
~~~
During login, PAM starts `gnome-keyring-daemon --login` which is responsible for keeping gnome-keyring unlocked with login password. 
- If `gnome-keyring-daemon --login` is not connected to session dbus within a few minutes, `gnome-keyring-daemon --login` dies.
- If `gnome-keyring-daemon --start ...` is started against session dbus in a window manager, `gnome-keyring-daemon --login` is connected to session dbus.
- If your login session does not start `gnome-keyring-daemon --start ...` before `gnome-keyring-daemon --login` quits, you can also use any program that uses gnome-keyring or secret service API before `gnome-keyring-daemon --login` dies.

## Troubleshooting

### Passwords are not remembered

If you are prompted for a password after logging in and you find that your passwords are not saved, then you may need to create/set a default keyring. To do this using Seahorse (a.k.a. Passwords and Keys), see [Create a new keyring](https://help.gnome.org/users/seahorse/stable/keyring-create.html) and [Change the default keyring](https://help.gnome.org/users/seahorse/stable/keyring-change-default.html) in GNOME Help.

### Resetting the keyring

You will need to [change your login keyring password](https://wiki.archlinux.org/title/GNOME/Keyring#Manage_using_GUI) if you receive the following error message: "The password you use to login to your computer no longer matches that of your login keyring".

Alternatively, you can remove the `login.keyring` and `user.keystore` files from `~/.local/share/keyrings/`. Be warned that this will permanently delete all saved keys. After removing the files, simply log out and log in again.

### Unable to locate daemon control file

The following error may appear in the [journal](https://wiki.archlinux.org/title/Journal "Journal") after logging in:

gkr-pam: unable to locate daemon control file

This message "can be safely ignored" if there are no other related issues [[2]](https://bbs.archlinux.org/viewtopic.php?pid=1940190#p1940190).

### No such secret collection at path: /

If you use a custom `~/.xinitrc` and receive this error when trying to create a new keyring with Seahorse, this may be solved by adding the following line [[3]](https://bbs.archlinux.org/viewtopic.php?pid=1640822#p1640822)

~~~sh
~/.xinitrc

source /etc/X11/xinit/xinitrc.d/50-systemd-user.sh
~~~

### Terminal gives the message "discover_other_daemon: 1"

This is caused by _gnome-keyring-daemon_ being started for the second time. Since a systemd service is delivered together with the daemon, you do not need to start it another way. So make sure to remove the start command from your `.zshenv`, `.bash_profile`, `.xinitrc`, `config.fish` or similar. Alternatively you can [disable](https://wiki.archlinux.org/title/Disable "Disable") the `gnome-keyring-daemon.service` and `gnome-keyring-daemon.socket` [user units](https://wiki.archlinux.org/title/User_unit "User unit").

## See also

- [https://help.gnome.org/users/seahorse/stable/](https://help.gnome.org/users/seahorse/stable/)
- [GNOME Wiki page](https://wiki.gnome.org/action/show/Projects/GnomeKeyring)

[Category](https://wiki.archlinux.org/title/Special:Categories "Special:Categories"): 

- [GNOME](https://wiki.archlinux.org/title/Category:GNOME "Category:GNOME")

- This page was last edited on 2 September 2023, at 06:06.
- Content is available under [GNU Free Documentation License 1.3 or later](https://www.gnu.org/copyleft/fdl.html) unless otherwise noted.

- [Privacy policy](https://terms.archlinux.org/docs/privacy-policy/)
- [About ArchWiki](https://wiki.archlinux.org/title/ArchWiki:About)
- [Disclaimers](https://wiki.archlinux.org/title/ArchWiki:General_disclaimer)