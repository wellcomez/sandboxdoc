---
title: "Bash script invoked by SystemD that starts GUI app"
layout: post
---


# Bash script invoked by SystemD that starts GUI app
<a style="text-decoration:underline" href="https://unix.stackexchange.com/questions/676251/bash-script-invoked-by-systemd-that-starts-gui-app">Bash script invoked by SystemD that starts GUI app</a><br>

I'm doing something wrong here. I'd like SystemD to run a bash script. The bash script checks if a GUI app is running, and if it's not running, then the bash script starts the GUI app; if the GUI app is running, the bash script exits.

I can run the bash script manually, but it doesn't run with `systemctl --user start make-run.sh`. For the purpose of this question, the GUI app is `xterm`. But in reality, the target GUI app is [Chatterino](https://chatterino.com/). I changed it to `xterm`, because I assume most people don't use Chatterino. Bash script is executable. My unit file is located in `~/.config/systemd/user/`. I've also tried `Type=oneshot` in the Unit file to no luck.

### Bash script

```
#!/bin/bash
# ensure a process is always running

export DISPLAY=:0 # needed if running a gui app

process=xterm
makerun="/usr/bin/xterm"

if ps ax | grep -v grep | grep $process > /dev/null
then
    exit
else
    $makerun &
fi

exit

```

### Unit file

```
[Unit]
Description=Make run

[Service]
Environment="DISPLAY=:0"
ExecStart=/usr/bin/bash /home/ladlelord/bin/make-run.sh

[Install]
WantedBy=graphical.target

```

`asked Nov 5, 2021 at 1:58`

In your bash script, replace `export DISPLAY=:0` by:

```
export DISPLAY=:0.0

```

Edit the `make-run.sh.service` as follows:

```
[Unit]

Description=Make run

[Service]
Environment="DISPLAY=:0"
ExecStart=/usr/bin/bash /home/ladlelord/bin/make-run.sh
Restart=on-failure
Type=forking

[Install]
WantedBy=graphical.target

```

Then:

```
systemctl --user daemon-reload
systemctl --user restart make-run.sh.service

```

**Edit**

The `graphical.target` is more suitable according to [@Stewart](https://unix.stackexchange.com/users/272848/stewart) [recommendations](https://unix.stackexchange.com/a/676418/153195).

**answered Nov 5, 2021 at 9:11[GAD3R](https://unix.stackexchange.com/users/153195/gad3r)**



GAD3R is close, but I would recommend:

1.  Don't set `DISPLAY` at all. The `--user` bus sets this implicitly. By not setting it at all, you can get this to work, even if you change your display settings and end up logging into a different display. This means: delete the `DISPLAY` lines from both the service and the script.
2.  In the `[Install]` section, use `graphical-session.target`. This target is raised after the graphical environment is ready for that user. Common inappropriate mistakes are:
    -   `graphical.target` is only on the system bus and is raised when your login screen appears (i.e. GDM or lightdm). This is too early for a GUI user app to launch.
    -   `multi-user.target` is also on the system bus and is raised as soon as anyone can log in (including via ssh or TTY). That's certainly not appropriate as there is no garuntee that the user is logged in or a graphical environment is available.
    -   `default.target` is on the user bus but can be raised if you log in via ssh (with no graphical session). Your service will fail in some cases.
3.  Your script simply runs `xterm` if there is no instance (regardless of user) running already. That makes it a little unnecessary and actually makes it possible for the service to immediately exit without doing anything (which is probably not what you want when you start that service). You could simplify this by deleting your script and using `ExecStart=/usr/bin/xterm` instead.
4.  If you really want to keep your script, consider using `Type=forking` instead of the implicit `Type=simple`. You run `bash`, which starts `xterm`, then exits. Because `Type=simple`, `bash` is the main process. When `bash` ends, `systemd` sees the main process exit and should consider the service finished (inactive-dead). It will then proceed to clean up (kill) any orphaned processes including `xterm`. When you use `Type=forking`, you tell `systemd` to watch for spawned processes and let those become the main process. `PIDFile=` can be used to explicitly define which spawned process becomes the main one. But all of this is unnecessary if you just `ExecStart=/usr/bin/xterm`.

This would be my full recommendation (assuming you delete the script):

```
[Unit]
Description=Make run

[Service]
ExecStart=/usr/bin/xterm
Restart=on-failure

[Install]
WantedBy=graphical-session.target

```

or this (with the script):

```
[Unit]
Description=Make run

[Service]
ExecStart=/usr/bin/bash /home/ladlelord/bin/make-run.sh
Restart=on-failure
Type=forking

[Install]
WantedBy=graphical-session.target

```

**answered Nov 6, 2021 at 9:07[Stewart](https://unix.stackexchange.com/users/272848/stewart)**
