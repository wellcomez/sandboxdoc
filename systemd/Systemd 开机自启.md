---
date: 2023-07-17 18:15
title: Systemd 开机自启
tags:
- systemd
- linux-boot
---



# Systemd 开机自启


[吴隐之](https://www.zhihu.com/people/oror-43)

想去自驾游

​关注他

4 人赞同了该文章

Ubuntu 18.04.5 LTS版本，网上很多或简单或复杂的开机自启案例都不起作用，因此记录一下自己能运行成功的。

中文文档

[systemd 中文手册 [金步国]​www.jinbuguo.com/systemd/systemd.html](https://link.zhihu.com/?target=http%3A//www.jinbuguo.com/systemd/systemd.html)

### 一、权限问题

Linux 系统的四（六）个类似却又不同的文件

- /etc/profile：系统配置文件，用户登录时读取一次
- /etc/bash.bashrc：系统配置文件 ，用户登录时读取一次，每次打开一个新终端会话时读取一次
- ~/.profile（~/.bash_profile、~/.bash_login）：用户配置文件，用户登录时读取一次
- ~/.bashrc：用户配置文件，用户登录时读取一次，每次打开一个新终端会话时读取一次
- systemd有系统和用户区分；系统（/user/lib/systemd/system/）、用户（/etc/lib/systemd/user/）
- 一般系统管理员手工创建的单元文件建议存放在/etc/systemd/system/目录下面

  

### 二、服务文件

脚本.service 为脚本配置文件，文件应放置在 /etc/systemd/system/ 路径下

  

```ps1con
[Unit]
Description=/etc/rc.local Compatibility
ConditionPathExists=/etc/rc.local  #脚本文件位置
 
[Service]
Type=forking
ExecStart=/etc/rc.local start  #配置的脚本文件rc.local为start
TimeoutSec=0    
StandardOutput=tty  #标准输出
RemainAfterExit=yes
SysVStartPriority=99  #优先级，当有多个开机启动文件时可以设置不同的值
 
[Install]
WantedBy=multi-user.target
```

#### **[Unit]**

- Description : 服务的简单描述

- Documentation ： 服务文档

- Before、After:定义启动顺序。Before=xxx.service,代表本服务在xxx.service启动之前启动。After=xxx.service,代表本服务在xxx.service之后启动。

- Requires：这个单元启动了，它需要的单元也会被启动；它需要的单元被停止了，这个单元也停止了。

- Wants：推荐使用。这个单元启动了，它需要的单元也会被启动；它需要的单元被停止了，对本单元没有影响。

#### **[Service]**

- Type=simple（默认值）：systemd认为该服务将立即启动。服务进程不会fork。如果该服务要启动其他服务，不要使用此类型启动，除非该服务是socket激活型。

- Type=forking：systemd认为当该服务进程fork，且父进程退出后服务启动成功。对于常规的守护进程（daemon），除非你确定此启动方式无法满足需求，使用此类型启动即可。使用此启动类型应同时指定 PIDFile=，以便systemd能够跟踪服务的主进程。

- Type=oneshot：这一选项适用于只执行一项任务、随后立即退出的服务。可能需要同时设置 RemainAfterExit=yes 使得 systemd 在服务进程退出之后仍然认为服务处于激活状态。

- Type=notify：与 Type=simple 相同，但约定服务会在就绪后向 systemd 发送一个信号。这一通知的实现由 libsystemd-daemon.so 提供。

- Type=dbus：若以此方式启动，当指定的 BusName 出现在DBus系统总线上时，systemd认为服务就绪。

- Type=idle: systemd会等待所有任务(Jobs)处理完成后，才开始执行idle类型的单元。除此之外，其他行为和Type=simple 类似。

- PIDFile：该服务PID文件的路径(一般位于 `/run/` 目录下)。强烈建议在 `Type=forking` 的情况下明确设置此选项。如果设为相对路径，那么表示相对于 `/run/` 目录。systemd 将会在此服务启动完成之后，从此文件中读取主服务进程的PID 。systemd 不会写入此文件，但会在此服务停止后删除它(若仍然存在)。PID文件的拥有者不必是特权用户，但是如果拥有者是非特权用户，那么必须施加如下安全限制：(1)不能是一个指向其他拥有者文件的软连接(无论直接还是间接)； (2)其中的PID必须指向一个属于该服务的进程。

- ExecStart：指定启动单元的命令或者脚本，ExecStartPre和ExecStartPost节指定在ExecStart之前或者之后用户自定义执行的脚本。Type=oneshot允许指定多个希望顺序执行的用户自定义命令。

- ExecReload：指定单元停止时执行的命令或者脚本。

- ExecStop：指定单元停止时执行的命令或者脚本。

- PrivateTmp：True表示给服务分配独立的临时空间

- Restart：这个选项如果被允许，服务重启的时候进程会退出，会通过systemctl命令执行清除并重启的操作。

- RemainAfterExit：当该服务的所有进程全部退出之后,是否依然将此服务视为活动(active)状态。默认值为 no。

- StandardOutput : 设置进程的标准输出(STDOUT)。可设为 `inherit`, `null`, `tty`, `journal`,`syslog`, `kmsg`, `journal+console`,`syslog+console`, `kmsg+console`, `file:_path_`, `append:_path_`, `socket`, `fd:_name_` 之一。
	1. `inherit` 表示继承 `StandardInput=` 的值。
	2. `null` 表示 `/dev/null` ，也就是所有输出都会被丢弃。
	3. `tty` 表示 TTY(由 `TTYPath=` 设置)。如果仅用于输出，那么进程将无需取得终端的控制权， 亦无需等待其他进程释放终端控制权。
	4. `journal` 表示 systemd 日志服务(通过[journalctl(1)](https://link.zhihu.com/?target=http%3A//www.jinbuguo.com/systemd/journalctl.html%23) 访问)。注意，所有发到 `syslog` 或 `kmsg` 的日志都会隐含的复制一份到 `journal` 中。
	5. `syslog` 表示 [syslog(3)](https://link.zhihu.com/?target=http%3A//man7.org/linux/man-pages/man3/syslog.3.html)日志服务。注意，此时所有日志都会隐含的复制一份到 `journal` 中。
	6. `kmsg` 表示内核日志缓冲区(通过 [dmesg(1)](https://link.zhihu.com/?target=http%3A//man7.org/linux/man-pages/man1/dmesg.1.html) 访问)。注意，此时所有日志都会隐含的复制一份到 `journal` 中。
	7. `journal+console`, `syslog+console`, `kmsg+console`与上面三个值类似，不同之处在于所有日志都会再复制一份到系统的控制台上。
	8. `file:_path_` 用于将指定的文件系统对象连接到标准输出。其含义与上`StandardInput=` 中的解释完全相同。如果 `_path_` 是文件系统上的一个普通文件，那么它将被打开(若不存在则创建)，并从头开始写入(但并不清空已有内容)。如果将标准输入与标准输出都指定为同一个文件，那么此文件仅打开一次，并同时用于输入与输出。这种用法主要用于文件系统上的 `AF_UNIX` 套接字，因为在这种情况下，只能创建一个同时用于输入与输出的流连接。
	9. `append:_path_` 与 `file:_path_` 类似，不同之处仅在于以附加(append)模式打开文件。
	10. `socket` 仅可用于基于套接字启动的服务单元。其含义与上文 `StandardInput=` 中的解释完全相同。
	11. `fd:_name_` 将标准输出连接到某个套接字单元提供的名为 `_name_` 的文件描述符。省略 `_name_` 表示使用隐含的默认名称"`stdout`" (也就是 "`fd`" 等价于 "`fd:stdout`")。必须通过套接字单元的 `Sockets=` 选项定义至少一个指定名称的文件描述符，并且文件描述符的名称可以与定义它的套接字单元的名称不同。如果找到多个匹配的文件描述符，那么仅使用第一个匹配项。详见[systemd.socket(5)](https://link.zhihu.com/?target=http%3A//www.jinbuguo.com/systemd/systemd.socket.html%23) 手册中的 `FileDescriptorName=` 选项，以了解有关命名文件描述符及其顺序的详细信息。

#### **[Install]**

- Alias：为单元提供一个空间分离的附加名字。

- RequiredBy：单元被允许运行需要的一系列依赖单元，RequiredBy列表从Require获得依赖信息。

- WantBy：单元被允许运行需要的弱依赖性单元，Wantby从Want列表获得依赖信息。

- Also：指出和单元一起安装或者被协助的单元。

- DefaultInstance：实例单元的限制，这个选项指定如果单元被允许运行默认的实例。


systemctl enable nginx.service

就会在/etc/systemd/system/multi-user.target.wants/目录下新建一个/usr/lib/systemd/system/nginx.service 文件的链接。

```text
#重启
$ sudo systemctl restart nginx.service
  
#重载
$ sudo systemctl reload nginx.service
  
#停止
$ sudo systemctl stop nginx.service

#查看服务的 log 信息
$ journalctl -u redshift.service
```

### 三、写入样例

自己写入脚本命名为 rc.local

```shell
$ sudo vi /etc/systemd/system/rc-local.service

[Unit]
Description=/etc/rc.local Compatibility
ConditionPathExists=/etc/rc.local  #脚本文件位置
 
[Service]
Type=forking
ExecStart=/etc/rc.local start  #配置的脚本文件rc.local为start
TimeoutSec=0    
StandardOutput=tty  #标准输出
RemainAfterExit=yes
SysVStartPriority=99  #优先级，当有多个开机启动文件时可以设置不同的值
 
[Install]
WantedBy=multi-user.target
```

脚本文件：

```shell
$ sudo vi /etc/rc.local
#!/bin/bash
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.
 
echo "test....." > /var/test.log
 
 
#your shell
#excample:
/home/inspur/Documents/frp/frpc -c /home/inspur/Documents/frp/frpc.ini
#或
#打开新终端运行;分号隔开多条命令;exec bash防止终端一闪消失
gnome-terminal -- bash -c "cd /home/inspur/Documents/frp/;./frpc -c ./frpc.ini;exec bash"
 
 
exit 0
```

更改权限：

> sudo chmod 775 /etc/rc.local  

开启服务：

> sudo systemctl enable rc-local  
> sudo systemctl start rc-local.service

链接：

> sudo ln -fs /lib/systemd/system/rc-local.service /etc/systemd/system/rc-local.service  

重启电脑查看：

> sudo reboot  
> cat /var/test.log  

  

  

### 参考

[Linux开机自动化执行脚本的四种方法（真实案例分享）_清风阁-CSDN博客_linux开机自动运行脚本](https://link.zhihu.com/?target=https%3A//blog.csdn.net/abc_12366/article/details/87552848%3Futm_medium%3Ddistribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control%26dist_request_id%3D1328680.140.16160771699057743%26depth_1-utm_source%3Ddistribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-1.control)

[systemd添加自定义系统服务设置自定义开机启动的方法_Linux_脚本之家](https://link.zhihu.com/?target=https%3A//www.jb51.net/article/100457.htm)

[ubuntu 20.04.1 LTS 开机自启动脚本服务开启 打开终端运行](https://link.zhihu.com/?target=https%3A//blog.csdn.net/qq_42371913/article/details/113995745)

[Systemd 入门教程：命令篇](https://link.zhihu.com/?target=http%3A//www.ruanyifeng.com/blog/2016/03/systemd-tutorial-commands.html)

编辑于 2021-03-19 00:09

