---
date: 2023-08-01 13:02
title: systemd-logind.service, systemd-logind — Login manager
tags:
- systemd
- systemd-logind
---



[Index](https://www.freedesktop.org/software/systemd/man/index.html) · [Directives](https://www.freedesktop.org/software/systemd/man/systemd.directives.html)systemd 254

---

## Name

systemd-logind.service, systemd-logind — Login manager

## Synopsis

`systemd-logind.service`

`/usr/lib/systemd/systemd-logind`

## Description[¶](https://www.freedesktop.org/software/systemd/man/systemd-logind.html#Description "Permalink to this headline")

**systemd-logind** is a system service that manages user logins. It is responsible for:

- Keeping track of users and sessions, their processes and their idle state. This is implemented by allocating a systemd slice unit for each user below `user.slice`, and a scope unit below it for each concurrent session of a user. Also, a per-user service manager is started as system service instance of `user@.service` for each logged in user.
    
- Generating and managing session IDs. If auditing is available and an audit session ID is already set for a session, then this ID is reused as the session ID. Otherwise, an independent session counter is used.
    
- Providing [polkit](https://www.freedesktop.org/wiki/Software/polkit)-based access for users for operations such as system shutdown or sleep
    
- Implementing a shutdown/sleep inhibition logic for applications
    
- Handling of power/sleep hardware keys
    
- Multi-seat management
    
- Session switch management
    
- Device access management for users
    
- Automatic spawning of text logins (gettys) on virtual console activation and user runtime directory management
    

User sessions are registered with logind via the [pam_systemd(8)](https://www.freedesktop.org/software/systemd/man/pam_systemd.html#) PAM module.

See [logind.conf(5)](https://www.freedesktop.org/software/systemd/man/logind.conf.html#) for information about the configuration of this service.

See [sd-login(3)](https://www.freedesktop.org/software/systemd/man/sd-login.html#) for information about the basic concepts of logind such as users, sessions and seats.

See [org.freedesktop.login1(5)](https://www.freedesktop.org/software/systemd/man/org.freedesktop.login1.html#) and [org.freedesktop.LogControl1(5)](https://www.freedesktop.org/software/systemd/man/org.freedesktop.LogControl1.html#) for information about the D-Bus APIs `systemd-logind` provides.

For more information on the inhibition logic see the [Inhibitor Lock Developer Documentation](https://www.freedesktop.org/wiki/Software/systemd/inhibit).

If you are interested in writing a display manager that makes use of logind, please have look at [Writing Display Managers](https://www.freedesktop.org/wiki/Software/systemd/writing-display-managers). If you are interested in writing a desktop environment that makes use of logind, please have look at [Writing Desktop Environments](https://www.freedesktop.org/wiki/Software/systemd/writing-desktop-environments).

## See Also[](https://www.freedesktop.org/software/systemd/man/systemd-logind.html#See%20Also "Permalink to this headline")

[systemd(1)](https://www.freedesktop.org/software/systemd/man/systemd.html#), [systemd-user-sessions.service(8)](https://www.freedesktop.org/software/systemd/man/systemd-user-sessions.service.html#), [loginctl(1)](https://www.freedesktop.org/software/systemd/man/loginctl.html#), [logind.conf(5)](https://www.freedesktop.org/software/systemd/man/logind.conf.html#), [pam_systemd(8)](https://www.freedesktop.org/software/systemd/man/pam_systemd.html#), [sd-login(3)](https://www.freedesktop.org/software/systemd/man/sd-login.html#)