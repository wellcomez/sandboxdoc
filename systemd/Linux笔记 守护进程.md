---
date: 2023-08-07 15:03
title: Linux笔记 守护进程
tags:
- 守护进程
- tty
---
# Linux笔记:守护进程

分类专栏： [linux](https://blog.csdn.net/qdx411324962/category_2782193.html) 文章标签： [linux](https://so.csdn.net/so/search/s.do?q=linux&t=all&o=vip&s=&l=&f=&viparticle=) [守护进程](https://so.csdn.net/so/search/s.do?q=%E5%AE%88%E6%8A%A4%E8%BF%9B%E7%A8%8B&t=all&o=vip&s=&l=&f=&viparticle=)

# 什么是守护进程？

[守护进程（Daemon Process）](http://baike.baidu.com/link?url=IcWqjC9d2Seh1YbNmbv4RdTbQ6PHDjQrqU1mxoTCalSOGlTm1ehoJIYnxVEgAqTDByUfSlH7rMdat00N1IKJva)，也就是通常说的 Daemon 进程（精灵进程），是 Linux 中的后台服务进程。它是一个生存期较长的进程，通常独立于控制终端并且周期性地执行某种任务或等待处理某些发生的事件。

  

[守护进程](http://baike.baidu.com/link?url=IcWqjC9d2Seh1YbNmbv4RdTbQ6PHDjQrqU1mxoTCalSOGlTm1ehoJIYnxVEgAqTDByUfSlH7rMdat00N1IKJva)是个特殊的[孤儿进程](http://blog.csdn.net/tennysonsky/article/details/45969569)，这种进程脱离终端，为什么要脱离终端呢？之所以脱离于终端是为了避免进程被任何终端所产生的信息所打断，其在执行过程中的信息也不在任何终端上显示。由于在 Linux 中，每一个系统与用户进行交流的界面称为终端，每一个从此终端开始运行的进程都会依附于这个终端，这个终端就称为这些进程的控制终端，当控制终端被关闭时，相应的进程都会自动关闭。

  

Linux 的大多数服务器就是用守护进程实现的。比如，Internet 服务器 inetd，Web 服务器 httpd 等。  

  

# 如何查看守护进程

在终端敲：ps axj

- a 表示不仅列当前用户的进程，也列出所有其他用户的进程
- x 表示不仅列有控制终端的进程，也列出所有无控制终端的进程
- j 表示列出与作业控制相关的信息

![](https://img-blog.csdn.net/20150603135158218)  

  

**<mark style="background: #FF5582A6;">从上图可以看出守护进行的一些特点</mark>：**

- 守护进程基本上都是以超级用户启动（ UID 为 0 ）
- 没有控制终端（ TTY 为 ？）
- 终端进程组 ID 为 -1 （ TPGID 表示终端进程组 ID）

  

一般情况下，守护进程可以通过以下方式启动：  

- 在系统启动时由启动脚本启动，这些启动脚本通常放在 /etc/rc.d 目录下；  
    
- 利用 inetd 超级服务器启动，如 telnet 等；  
    
- 由 cron 定时启动以及在终端用 nohup 启动的进程也是守护进程。

  

# 如何编写守护进程？

下面是编写守护进程的基本过程：

1. 屏蔽一些控制终端操作的信号

  

	<mark style="background: #E73308E3;">这是为了防止守护进行在没有运行起来前，控制终端受到干扰退出或挂起。</mark>关于信号的更详细用法，[请看《信号中断处理》](http://blog.csdn.net/lianghe_work/article/details/46804469)。


~~~cpp
1. signal(SIGTTOU,SIG_IGN);   
2. signal(SIGTTIN,SIG_IGN);   
3. signal(SIGTSTP,SIG_IGN);   
4. signal(SIGHUP ,SIG_IGN);  
~~~

  

2. 在后台运行
	这是为避免挂起控制终端将守护进程放入后台执行。方法是在进程中调用 fork() 使父进程终止， 让守护进行在子进程中后台执行。 
	
	~~~cpp
	1. if( pid = fork() ){ // 父进程  
	2.     exit(0);        //结束父进程，子进程继续  
	3. }  
	~~~
	
	  

3. 脱离控制终端、登录会话和进程组
	有必要先介绍一下 Linux 中的进程与控制终端，登录会话和进程组之间的关系：进程属于一个进程组，进程组号（GID）就是进程组长的进程号（PID）。登录会话可以包含多个进程组。这些进程组共享一个控制终端。这个控制终端通常是创建进程的 shell 登录终端。 控制终端、登录会话和进程组通常是从父进程继承下来的。我们的目的就是要摆脱它们 ，使之不受它们的影响。因此需要调用 setsid() 使子进程成为新的会话组长，示例代码如下：
	`
	~~~cpp
	1. setsid();  
	~~~
	setsid() 调用成功后，进程成为新的会话组长和新的进程组长，并与原来的登录会话和进程组脱离。由于会话过程对控制终端的独占性，进程同时与控制终端脱离。 `

  

4. 禁止进程重新打开控制终端
	现在，进程已经成为无终端的会话组长，但它可以重新申请打开一个控制终端。可以通过使进程不再成为会话组长来禁止进程重新打开控制终端，采用的方法是再次创建一个子进程，示例代码如下：
	~~~cpp
	1. if( pid=fork() ){ // 父进程  
	2.     exit(0);      // 结束第一子进程，第二子进程继续（第二子进程不再是会话组长）   
	3. }  
	~~~
	
	  

5. 关闭打开的文件描述符

	进程从创建它的父进程那里继承了打开的文件描述符。如不关闭，将会浪费系统资源，造成进程所在的文件系统无法卸下以及引起无法预料的错误。按如下方法关闭它们：
	
	~~~c
	1. // NOFILE 为 <sys/param.h> 的宏定义  
	2. // NOFILE 为文件描述符最大个数，不同系统有不同限制  
	3. for(i=0; i< NOFILE; ++i){// 关闭打开的文件描述符  
	4.     close(i);  
	5. }  
	~~~
	
	  

  
6. 改变当前工作目录
	
	进程活动时，其工作目录所在的文件系统不能卸下。一般需要将工作目录改变到根目录。对于需要转储核心，写运行日志的进程将工作目录改变到特定目录如 /tmp。示例代码如下：
	
	
	~~~c
	1. chdir("/");  
	~~~
	
	  

7. 重设文件创建掩模
	
	进程从创建它的父进程那里继承了文件创建掩模。它可能修改守护进程所创建的文件的存取权限。为防止这一点，将文件创建掩模清除：
	~~~c

	1. umask(0);  
	~~~

  
8. 处理 SIGCHLD 信号

	但对于某些进程，特别是服务器进程往往在请求到来时生成子进程处理请求。如果父进程不等待子进程结束，子进程将成为僵尸进程（zombie）从而占用系统资源（关于僵尸进程的更多详情，[请看《僵尸进程》](http://blog.csdn.net/lianghe_work/article/details/46803445)）[[。如果父进程等待子进程结束，将增加父进程的负担，影响服务器进程的并发性能。在 Linux 下可以简单地将 SIGCHLD 信号的操作设为 SIG_IGN 。关于信号的更详细用法，[请看《信号中断处理》](http://blog.csdn.net/lianghe_work/article/details/46804469)。
	
	~~~c
	1. signal(SIGCHLD, SIG_IGN);  
	~~~
	
	  
	这样，内核在子进程结束时不会产生僵尸进程。  

  

9.  示例代码如下：
~~~c
1. #include <unistd.h>   
2. #include <signal.h>   
3. #include <fcntl.h>  
4. #include <sys/syslog.h>  
5. #include <sys/param.h>   
6. #include <sys/types.h>   
7. #include <sys/stat.h>   
8. #include <stdio.h>  
9. #include <stdlib.h>  
10. #include <time.h>  

12. int init_daemon(void)  
13. {   
14.     int pid;   
15.     int i;  

17.     // 1）屏蔽一些控制终端操作的信号  
18.     signal(SIGTTOU,SIG_IGN);   
19.     signal(SIGTTIN,SIG_IGN);   
20.     signal(SIGTSTP,SIG_IGN);   
21.     signal(SIGHUP ,SIG_IGN);  

23.     // 2）在后台运行  
24.     if( pid=fork() ){ // 父进程  
25.         exit(0); //结束父进程，子进程继续  
26.     }else if(pid< 0){ // 出错  
27.         perror("fork");  
28.         exit(EXIT_FAILURE);  
29.     }  

31.     // 3）脱离控制终端、登录会话和进程组  
32.     setsid();    

34.     // 4）禁止进程重新打开控制终端  
35.     if( pid=fork() ){ // 父进程  
36.         exit(0);      // 结束第一子进程，第二子进程继续（第二子进程不再是会话组长）   
37.     }else if(pid< 0){ // 出错  
38.         perror("fork");  
39.         exit(EXIT_FAILURE);  
40.     }    

42.     // 5）关闭打开的文件描述符  
43.     // NOFILE 为 <sys/param.h> 的宏定义  
44.     // NOFILE 为文件描述符最大个数，不同系统有不同限制  
45.     for(i=0; i< NOFILE; ++i){  
46.         close(i);  
47.     }  

49.     // 6）改变当前工作目录  
50.     chdir("/tmp");   

52.     // 7）重设文件创建掩模  
53.     umask(0);    

55.     // 8）处理 SIGCHLD 信号  
56.     signal(SIGCHLD,SIG_IGN);  

58.     return 0;   
59. }   

61. int main(int argc, char *argv[])   
62. {  
63.     init_daemon();  

65.     while(1);  

67.     return 0;  
68. }  
~~~

  
运行结果如下：

![](https://img-blog.csdn.net/20150525185822387)

  

转自:http://blog.csdn.net/lianghe_work/article/details/47659889

首先，我们得搞清楚这些代码都干了什么：  
1. 第一次fork，确保当前的process一定不是session leader，关于session，继续往下看  
2. setsid，创建新的session，因为session leader调用setsid不会创建新session，所以我们才需要之前的那个fork。  
	- 那session到底是什么？
	当你通过控制台登录时，你就创建了一个session，一个session就是一个controlling terminal、一个controlling process group，再加上一堆后台process group，
	- 其中controlling process一般就是login shell，controlling terminal就是你在敲键盘看显示器的那个“终端”。在shell中，你可以用"&"将一个命令在后台运行，或者你可以按Ctrl-Z，然后用bg命令将其放入后台，这时你可以用jobs命令看到后台运行的进程，这些后台进程就是background process group  
	- 看看当你logout的时候会发生什么，
		此时controlling terminal会被关闭，这个session中所有进程都会收到SIGHUP和SIGTERM/SIGQUIT，对于这些信号的缺省操作就是结束进程。  
		作为一个daemon，你当然不希望用户logout的时候就退出，解决方案有几种，
		1. 一种就是忽略上述所有信号，例如nohup程序干的就是这个，但这样做有个小问题，很多程序使用信号作为一种简单的IPC机制，忽略这些信号会导致这种IPC失效；
		2. 另外一种方案就是让进程从这个controlling terminal上脱离，setsid将会创建一个新的session，使当前进程成为新session的leader，并且不再关联之前session的controlling terminal。  
1. 第二次fork，这件事情有点晦涩，其实很多文档上并没说的太清楚，
	- 具体原因是这样的，即使一个进程创建了新的session，它依然有可能获得一个controlling terminal，比如你可以用ioctl(TIOCSCTTY)，这样做的后果就是，新的session有了controlling terminal，然后前面我们提到的所有问题依然可能发生。
	- 为了彻底解决这个问题，我们需要第二次fork，因为ioctl(TIOCSCTTY)手册中在非常不起眼的地方提到了，只有session leader才能为session打开controlling terminal，<mark style="background: #FF5582A6;">第二次fork之后，子进程就是session中第二个进程，它这辈子再没机会成为session leader了（除非它调用setsid），也就再没能力打开</mark>controlling terminal了。这样我们就一劳永逸的解决了所有问题。  
	所以，如果你的程序行为很固定，你知道它不会无聊到去打开controlling terminal的话，第二次fork其实是不必要的，但是如果你是在写一个库，而且不知道使用它的程序到底会有什么样的行为，你最好还是再fork一次