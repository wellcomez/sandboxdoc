---
date: 2023-07-17 18:18
title: init和systemd：Linux守护进程
tags:
- systemd
- linux-boot
---



首发于[coldwind的专栏](https://www.zhihu.com/column/c_190978740)

写文章


# init和systemd：Linux守护进程


[Coldwind](https://www.zhihu.com/people/jimzeus)

程序员

​关注他

3 人赞同了该文章

**init** 和 **systemd** 都是 Linux的 init 守护进程，但最好使用后者，因为它在最近的 Linux 发行版中很常用。init 使用service，而Systemd用**systemctl**管理 Linux 服务。

### **1．init**

**init** 守护进程是 Linux 内核执行的第一个进程，它的进程 ID (PID) 始终为 1。它的目的是初始化、管理和跟踪系统服务和守护进程。换句话说，init 守护进程是系统上所有进程的父进程。

要创建一个服务，需要编写shell脚本，并存储在/etc/init.d/目录下，通过service命令启动、停止、重新启动服务。例如如下的/etc/init.d/myservice脚本：

```bash
#!/bin/bash
# chkconfig: 2345 20 80
# description: Description comes here....
 
# Source function library.
. /etc/init.d/functions
 
start() {
    # TODO: code to start app comes here 
}
 
stop() {
    # TODO: code to stop app comes here 
}
 
case "$1" in 
    start)
       start
       ;;
    stop)
       stop
       ;;
    restart)
       stop
       start
       ;;
    status)
       # TODO: code to check status of app comes here 
       ;;
    *)
       echo "Usage: $0 {start|stop|restart|status}"
esac
 
exit 0
```

### **2．systemd**

而**systemd**（system daemon）是现在的Linux系统所使用的守护进程（pid也为1），其功能更加强大。要编写systemd服务，需要在/etc/systemd/system/目录下编写一个.service文件。

有了.service文件之后，可以通过systemctl命令来启动、停止、重新启动服务。

以下为一个myservice.service文件的样例：

```shell
[Unit]
Description=Some Description
Requires=syslog.target
After=syslog.target
 
[Service]
ExecStart=/usr/sbin/<command-to-start>
ExecStop=/usr/sbin/<command-to-stop>
 
[Install]
WantedBy=multi-user.target
```

发布于 2022-09-06 14:23

