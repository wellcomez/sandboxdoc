---
date: 2023-08-07 16:03
title: Linux系统编程——特殊进程之僵尸进程
tags:
- 僵尸进程
- Zombie Process
---

# Linux系统编程——特殊进程之僵尸进程

分类专栏： [Linux_系统编程](https://blog.csdn.net/lianghe_work/category_2871249.html) 
文章标签： [僵尸进程](https://so.csdn.net/so/search/s.do?q=%E5%83%B5%E5%B0%B8%E8%BF%9B%E7%A8%8B&t=all&o=vip&s=&l=&f=&viparticle=)



## 僵尸进程（Zombie Process）

进程已运行结束，但进程的占用的资源未被回收，这样的进程称为僵尸进程。

在每个进程退出的时候，内核释放该进程所有的资源、包括打开的文件、占用的内存等。 但是仍然为其保留一定的信息，这些信息主要主要指进程控制块的信息（包括进程号、退出状态、运行时间等）。直到父进程通过 wait() 或 waitpid() 来获取其状态并释放（[具体用法，请看《等待进程结束》](http://blog.csdn.net/lianghe_work/article/details/46640373)）。 这样就会导致一个问题，如果进程不调用wait() 或 waitpid() 的话， 那么保留的那段信息就不会释放，其进程号就会一直被占用，但是系统所能使用的进程号是有限的，如果大量的产生僵死进程，将因为没有可用的进程号而导致系统不能产生新的进程.此即为僵尸进程的危害，应当避免。

子进程已运行结束，父进程未调用 wait() 或 waitpid() 函数回收子进程的资源是子进程变为僵尸进程的原因。

僵尸进程测试程序如下：

~~~c
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
int main(int argc, char *argv[]){
  pid_t pid;
  pid = fork();  // 创建进程
  if (pid < 0) { // 出错
    perror("fork error:");
    exit(1);
  } else if (0 == pid) { // 子进程
    printf("I am child process.I am exiting.\n");
    printf("[son id]: %d\n", getpid());
    exit(0);
  } else if (pid >
             0) { // 父进程		// 父进程没有调用 wati() 或 watipid()
    sleep(1);     // 保证子进程先运行
    printf("I am father process.I will sleep two seconds\n");
    printf("[father id]: %d\n", getpid());
    while (1)
      ; // 不让父进程退出
  }
  return 0;}
~~~

  
运行结果：

![](https://img-blog.csdn.net/20150708150527353)  

  

在终端敲：`ps -ef | grep defunct`，后面尖括号里是 defunct 的都是 #僵尸进程 #defunct。 

我们另启一个终端，查看进程的状态，有哪些是僵尸进程：  
![](https://img-blog.csdn.net/20150708150838674)  
  
或者：用 `ps -e`

![](https://img-blog.csdn.net/20150708151714687)

## 如何避免僵尸进程？

1. 最简单的方法，父进程通过 wait() 和 waitpid() 等函数等待子进程结束，但是，这会导致父进程挂起。具体用法，[请看《进程的控制：结束进程、等待进程结束》](http://blog.csdn.net/lianghe_work/article/details/46640373)。  
  
  
2. 如果父进程要处理的事情很多，不能够挂起，通过 signal()  函数人为处理信号 #SIGCHLD ， 只要有子进程退出自动调用指定好的回调函数，因为子进程结束后， 父进程会收到该信号 SIGCHLD ，可以在其回调函数里调用 wait() 或 waitpid() 回收。关于信号的更详细用法，请看《[信号中断处理](http://blog.csdn.net/lianghe_work/article/details/46804469)》。  
	测试代码如下：  
	
	~~~c
	#include <errno.h>
	#include <signal.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	void sig_child(int signo){
	  pid_t pid; // 处理僵尸进程, -1 代表等待任意一个子进程, WNOHANG代表不阻塞
	  while ((pid = waitpid(-1, NULL, WNOHANG)) > 0) {
	    printf("child %d terminated.\n", pid);
	  }
	} 
	int main(){
	  pid_t pid; // 创建捕捉子进程退出信号	//
	             // 只要子进程退出，触发SIGCHLD，自动调用sig_child()
	  signal(SIGCHLD, sig_child);
	  pid = fork();  // 创建进程
	  if (pid < 0) { // 出错
	    perror("fork error:");
	    exit(1);
	  } else if (pid == 0) { // 子进程
	    printf("I am child process,pid id %d.I am exiting.\n", getpid());
	    exit(0);
	  } else if (pid > 0) { // 父进程
	    sleep(2);           // 保证子进程先运行
	    printf("I am father, i am exited\n\n");
	    system("ps -ef | grep defunct"); // 查看有没有僵尸进程
	  }
	  return 0;
	}
	~~~
	
	  
运行结果：
	
![](https://img-blog.csdn.net/20150813095628247)  
	
	  

3. 如果父进程不关心子进程什么时候结束，那么可以用signal（SIGCHLD, SIG_IGN）通知内核，自己对子进程的结束不感兴趣，父进程忽略此信号，那么子进程结束后，内核会回收， 并不再给父进程发送信号。关于信号的更详细用法，请看《[信号中断处理](http://blog.csdn.net/lianghe_work/article/details/46804469)》。  
	
	~~~c
	#include <errno.h>
	#include <signal.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	int main(){
	  pid_t pid; // 忽略子进程退出信号的信号	//
	             // 那么子进程结束后，内核会回收， 并不再给父进程发送信号
	  signal(SIGCHLD, SIG_IGN);
	  pid = fork();  // 创建进程
	  if (pid < 0) { // 出错
	    perror("fork error:");
	    exit(1);
	  } else if (pid == 0) { // 子进程
	    printf("I am child process,pid id %d.I am exiting.\n", getpid());
	    exit(0);
	  } else if (pid > 0) { // 父进程
	    sleep(2);           // 保证子进程先运行
	    printf("I am father, i am exited\n\n");
	    system("ps -ef | grep defunct"); // 查看有没有僵尸进程
	  }
	  return 0;}
	~~~
	
	  
	运行结果：
	
![](https://img-blog.csdn.net/20150813100254707)  
	
	  

4. 还有一些技巧，就是 fork()  两次，父进程 fork() 一个子进程，然后继续工作，子进程 fork() 一 个孙进程后退出，那么孙进程被 init 接管，孙进程结束后，init （1 号进程）会回收。不过子进程的回收还要自己做。《UNIX环境高级编程》8.6节说的非常详细。原理是将子进程成为孤儿进程，从而其的父进程变为 init 进程（1 号进程），通过 init 进程（1 号进程）可以处理僵尸进程。更多详情，请看《特殊进程之孤儿进程》。  
	
	源码如下：
	
	~~~c
	#include <errno.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	int main(){
	  pid_t pid; // 创建第一个子进程
	  pid = fork();
	  if (pid < 0) { // 出错
	    perror("fork error:");
	    exit(1);
	  } else if (pid == 0) {
	    // 子进程            //子进程再创建子进程
	    printf("I am the first child process.pid:%d\tppid:%d\n", getpid(),
	           getppid());
	    pid = fork();
	    if (pid < 0) {
	      perror("fork error:");
	      exit(1);
	    } else if (pid ==
	               0) { // 子进程
	                    // //睡眠3s保证下面的父进程退出，这样当前子进程的父亲就是
	                    // init 进程
	      sleep(3);
	      printf("I am the second child process.pid: %d\tppid:%d\n", getpid(),
	             getppid());
	      exit(0);
	    } else if (pid > 0) { // 父进程退出
	      printf("first procee is exited.\n");
	      exit(0);
	    }
	  } else if (pid > 0) { // 父进程			//
	                        // 父进程处理第一个子进程退出，回收其资源
	    //
	    if (waitpid(pid, NULL, 0) != pid) {
	      perror("waitepi error:");
	      exit(1);
	    }
	    exit(0);
	  }
	  //
	  return 0;}
	~~~
	
	  
运行结果：
![](https://img-blog.csdn.net/20150813103249206)  
[所有源码下载：http://download.csdn.net/download/lianghe_work/8999129](http://download.csdn.net/download/lianghe_work/8999129)
