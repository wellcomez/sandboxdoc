---
date:: 2014-09-05 
title:: 简介SELinux的在CentOS 7  - 第2部分：文件和进程
tags:
- selinux
---



# 简介SELinux的在CentOS 7  - 第2部分：文件和进程

2014-09-05 分类：[系统运维](https://www.youcl.com/node/44) 阅读(64) 评论(0)

### 介绍

在[第一部分](https://www.youcl.com/info/10511)我们SELinux的系列，我们看到了如何启用和禁用SELinux，如何改变一些使用布尔值的策略设置。 在第二部分中，我们将讨论文件和进程安全上下文。

从前面的教程刷新你的记忆，一个文件的安全上下文是一个_类型_ ，一个过程的安全上下文是一个_域_ 。

> **注意**  
> 本教程中显示的命令，包和文件在CentOS 7上进行了测试。其他发行版的概念保持不变。

在本教程中，除非另有说明，否则我们将以root用户身份运行命令。 如果您没有访问到root帐户，并使用使用sudo权限的其他帐户，您需要与先于命令`sudo`关键字。

## 创建测试用户帐户

首先，让我们创建四个用户帐户来演示SELinux的功能。

-   普通用户
-   切换用户
-   访客
-   restricteduser

目前应该是**root**用户。 让我们运行下面的命令来添加**regularuser**帐户：

```
useradd -c "Regular User" regularuser
```

然后，我们运行`passwd`命令来修改自己的口令：

```
passwd regularuser
```

输出将要求我们输入新密码。 一旦提供，该帐户将准备登录：

```
Changing password for user regularuser.
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
```

让我们创建其他帐户：

```
useradd -c "Switched User" switcheduser

passwd switcheduser
```

```
useradd -c "Guest User" guestuser

passwd guestuser 
```

```
useradd -c "Restricted Role User" restricteduser

passwd restricteduser 
```

## SELinux用于进程和文件

SELinux的目的是保护进程在Linux环境中访问文件的方式。 没有SELinux，像Apache守护进程那样的进程或应用程序将在启动它的用户的上下文中运行。 因此，如果您的系统受到root用户下运行的流氓应用程序的攻击，应用程序可以做任何它想要的，因为root对每个文件拥有所有权限。

SELinux尝试进一步，消除这种风险。 使用SELinux，进程或应用程序将只具有它所需的功能和更多的权限。 SELinux策略的应用程序将决定它需要什么类型的文件获得什么处理它可以_转换_到。 SELinux策略由应用程序开发人员编写，并随支持它的Linux发行版一起提供。 策略基本上是一组将流程和用户映射到其权限的规则。

我们通过了解SELinux _内容_和_域_意味着开始教程的这一部分的讨论。

安全的第一部分放置一个_标签_上在Linux系统中的每个实体。 标签与任何其他文件或进程属性（所有者，组，创建日期等）相同; 它显示了资源的_情况下_ 。 那么什么是上下文？ 简单来说，上下文是一个安全相关信息的集合，帮助SELinux做出访问控制决策。 Linux系统中的一切都可以有安全上下文：用户帐户，文件，目录，守护程序或端口都可以有其安全上下文。 然而，安全上下文对于不同类型的对象意味着不同的东西。

### SELinux文件上下文

让我们从理解SELinux文件上下文开始。 让我们看看常规ls -l命令对/ etc目录的输出。

```
ls -l /etc/*.conf
```

这将向我们展示一个熟悉的输出：

```
... 
-rw-r--r--. 1 root root    19 Aug 19 21:42 /etc/locale.conf
-rw-r--r--. 1 root root   662 Jul 31  2013 /etc/logrotate.conf
-rw-r--r--. 1 root root  5171 Jun 10 07:35 /etc/man_db.conf
-rw-r--r--. 1 root root   936 Jun 10 05:59 /etc/mke2fs.conf
...
```

简单，对吧？ 让我们现在添加-Z标志：

```
ls -Z /etc/*.conf
```

我们现在在用户和组所有权后还有一个额外的信息列：

```
...
-rw-r--r--. root root system_u:object_r:locale_t:s0    /etc/locale.conf
-rw-r--r--. root root system_u:object_r:etc_t:s0       /etc/logrotate.conf
-rw-r--r--. root root system_u:object_r:etc_t:s0       /etc/man_db.conf
-rw-r--r--. root root system_u:object_r:etc_t:s0       /etc/mke2fs.conf
...
```

此列显示文件的安全上下文。 一个文件，据说已被_贴上_了其安全性上下文，当你有相关的可用信息。 让我们仔细看看一个安全上下文。

```
-rw-r--r--. root    root  system_u:object_r:etc_t:s0       /etc/logrotate.conf
```

安全上下文是这部分：

```
system_u:object_r:etc_t:s0
```

有四个部分，安全上下文的每个部分由冒号（:)分隔。 第一部分是为文件的SELinux _用户_上下文。 稍后我们将讨论SELinux的用户，但现在，我们可以看到，它的**system_u。** 每个Linux用户帐户映射到SELinux的用户，并且在这种情况下，拥有该文件的**根**用户被映射到**system_u** SELinux的用户。 此映射由SELinux策略完成。

第二部分规定了SELinux的_作用_ ，这也是**object_r。** 要刷新SELinux角色，请回顾第一个SELinux文章。

什么是最重要的这里是第三部分，这是这里列为**etc_t**文件的_类型_ 。 这是定义什么_类型_的文件或目录所属的一部分。 我们可以看到，大部分文件属于在该**etc_t**类型`/etc`目录中。 假想，你能想到类型的作为一种“基团”或_属性_的文件的：它的文件进行分类的方法。

我们还可以看到一些文件可能属于其他类型，如`locale.conf`具有**locale_t**类型。 即使当此处列出的所有文件具有相同的用户和组所有者时，其类型也可能不同。

作为另一个例子，让我们检查用户主目录的类型上下文：

```
ls -Z /home
```

主目录将有一个不同的上下文类型： **用户_主_ dir_t**

```
drwx------. guestuser      guestuser      unconfined_u:object_r:user_home_dir_t:s0      guestuser
drwx------. root           root           system_u:object_r:lost_found_t:s0 lost+found
drwx------. regularuser    regularuser    unconfined_u:object_r:user_home_dir_t:s0      regularuser
drwx------. restricteduser restricteduser unconfined_u:object_r:user_home_dir_t:s0      restricteduser
drwx------. switcheduser   switcheduser   unconfined_u:object_r:user_home_dir_t:s0      switcheduser
drwx------. sysadmin       sysadmin       unconfined_u:object_r:user_home_dir_t:s0      sysadmin
```

安全上下文的第四部分**，S0，**具有_多级安全_或MLS做。 基本上，这是实施SELinux安全策略的另一种方式，这部分显示了资源**（S0）**的_灵敏度_ 。 稍后我们将简要讨论灵敏度和类别。 对于SELinux的大多数香草设置，前三个安全上下文更重要。

### SELinux过程上下文

让我们来谈谈过程安全上下文。

启动Apache和SFTP服务。 我们在第一个SELinux教程中安装了这些服务。

```
service httpd start

service vsftpd start
```

我们可以运行`ps`与几个标志，以显示我们的服务器上运行的Apache和SFTP进程的命令：

```
ps -efZ | grep 'httpd\|vsftpd'
```

同样，-Z标志用于显示SELinux上下文。 输出显示运行进程，进程ID和父进程ID的用户：

```
system_u:system_r:httpd_t:s0            root        7126    1       0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0            apache      7127    7126    0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0            apache      7128    7126    0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0            apache      7129    7126    0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0            apache      7130    7126    0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:httpd_t:s0            apache      7131    7126    0 16:50 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
system_u:system_r:ftpd_t:s0-s0:c0.c1023 root        7209    1       0 16:54 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023 root 7252 2636  0 16:57 pts/0 00:00:00 grep --color=auto httpd\|vsftpd

```

安全上下文是这部分：

```
system_u:system_r:httpd_t:s0
```

安全上下文有四个部分：用户，角色，域和敏感性。 用户，角色和敏感性就像文件的相同上下文一样工作（在上一节中解释）。 域是进程独有的。

在上面的例子中，我们可以看到，几个进程在**httpd_t**域内运行，而一个是**ftpd_t**域内运行。

那么，域为流程做什么？ 它给进程一个在其中运行的上下文。 这就像周围的_地限制_它的进程泡沫。 它告诉过程它可以做什么和它不能做什么。 这个限制确保每个进程域只能处理某些类型的文件，没有更多。

使用这个模型，即使一个进程被另一个恶意进程或用户劫持，它可以做的最糟糕的事情是损坏它访问的文件。 例如，vsftp守护程序将无权访问say，sendmail或samba使用的文件。 此限制是从内核级实现的：它是执行作为SELinux策略加载到存储器中，并因此访问控制成为_必需_ 。

## 命名约定

在我们再进一步之前，这里有一个关于SELinux命名约定的注释。 SELinux的用户由_“U”_为Stapling_，角色由“r”_和类型（文件）或域（进程）通过_Stapling_ “_t”Stapling。

## 如何处理访问资源

到目前为止，我们已经看到文件和进程可以具有不同的上下文，并且它们仅限于它们自己的类型或域。 那么进程如何运行？ 要运行，进程需要访问其文件并对其执行一些操作（打开，读取，修改或执行）。 我们还了解到每个进程只能访问某些类型的资源（文件，目录，端口等）。

SELinux在策略中规定这些访问规则。 访问规则遵循标准_允许声明_结构：

```
allow <domain> <type>:<class> { <permissions> };
```

我们已经谈到了域和类型。 **类**定义了资源实际上代表（文件，目录，符号链接，设备，港口，光标等）

下面是这个泛型允许语句的含义：

-   如果一个进程有一定的域
-   它试图访问的资源对象是某种类和类型
-   然后允许访问
-   否则拒绝访问

为了看看这是如何工作的，让我们考虑在我们的CentOS 7系统上运行的httpd守护进程的安全上下文：

```
system_u:system_r:httpd_t:s0     7126 ?        00:00:00 httpd
system_u:system_r:httpd_t:s0     7127 ?        00:00:00 httpd
system_u:system_r:httpd_t:s0     7128 ?        00:00:00 httpd
system_u:system_r:httpd_t:s0     7129 ?        00:00:00 httpd
system_u:system_r:httpd_t:s0     7130 ?        00:00:00 httpd
system_u:system_r:httpd_t:s0     7131 ?        00:00:00 httpd
```

Web服务器的默认主目录是`/var/www/html` 。 让我们在该目录中创建一个文件并检查其上下文：

```
touch /var/www/html/index.html

ls -Z /var/www/html/*
```

对于我们的Web内容的文件上下文将被**_SYS_的httpd content_t：**

```
-rw-r--r--. root root unconfined_u:object_r:httpd_sys_content_t:s0 /var/www/html/index.html
```

我们可以使用`sesearch`命令检查允许httpd守护进程的访问类型：

```
sesearch --allow --source httpd_t --target httpd_sys_content_t --class file
```

使用命令使用的标志是相当不言自明：源域是**httpd_t，Apache**是运行在同一个域中我们有兴趣有关的文件和必须**的httpd _SYS_ content_t**的类型范围内的目标资源。 您的输出应如下所示：

```
Found 4 semantic av rules:
   allow httpd_t httpd_sys_content_t : file { ioctl read getattr lock open } ;
   allow httpd_t httpd_content_type : file { ioctl read getattr lock open } ;
   allow httpd_t httpd_content_type : file { ioctl read getattr lock open } ;
   allow httpd_t httpdcontent : file { ioctl read write create getattr setattr lock append unlink link rename execute open } ;
```

注意第一行：

```
allow httpd_t httpd_sys_content_t : file { ioctl read getattr lock open } ;
```

这是说httpd守护进程（Apache Web服务器）的I / O控制，读取，获得属性，锁定和开放接入到**httpd _SYS_内容**类型的文件。 在这种情况下，我们`index.html`文件具有相同的类型。

往前一步，让我们先修改网页（ `/var/www/html/index.html` ）。 编辑文件以包含此内容：

```
<html>
    <title>
        This is a test web page
    </title>
    <body>
        <h1>This is a test web page</h1>
    </body>
</html>
```

下一步，我们将改变许可`/var/www/`文件夹及其内容，其次是httpd守护进程的重新启动：

```
chmod -R 755 /var/www
```

```
service httpd restart
```

我们将尝试从浏览器访问它：

![使用正确的SELiunux设置访问网页](https://www.youcl.com/uploads/1/image/public/201806/20180601164123_2kpq0t3bn1.jpg)

> **注意**  
> 根据服务器的设置，您可能必须在IPTables防火墙中启用端口80，以允许来自服务器外部的传入HTTP流量。 我们不会在这里介绍在IPTables中启用端口的详细信息。 有一些优秀的DigitalOcean [文章](https://www.youcl.com/info/10655)可以在其上使用的话题。

到现在为止还挺好。 httpd守护程序有权访问特定类型的文件，我们可以在通过浏览器访问时看到它。 接下来，让我们通过改变文件的上下文来改变一些东西。 我们将使用`chcon`命令吧。 该`--type`为命令标志允许我们指定目标资源的新类型。 在这里，我们正在改变文件类型**var_t。**

```
chcon --type var_t /var/www/html/index.html
```

我们可以确认类型更改：

```
ls -Z /var/www/html/
```

```
-rwxr-xr-x. root root unconfined_u:object_r:var_t:s0   index.html
```

接下来，当我们试图访问网页（即httpd守护进程试图读取该文件），你可能会得到一个**禁止**错误，或者您可能会看到通用CentOS的“测试123”页面：

![使用不正确的SELinux设置访问网页](https://www.youcl.com/uploads/1/image/public/201806/20180601164123_tjq6ko5bxd.jpg)

那么这里发生了什么？ 显然一些访问现在被拒绝，但它的访问是什么？ 至于SELinux的而言，Web服务器被授权访问仅某些类型的文件和变种_t不这些上下文中的一个。_ _既然我们改变了index.html文件为VAR T的背景下_ ，Apache可以不再读它，我们得到一个错误。

为了让事情再次合作，让我们改变的文件类型`restorecon`命令。 -v开关显示上下文标签的更改：

```
restorecon -v /var/www/html/index.html
```

```
restorecon reset /var/www/html/index.html context unconfined_u:object_r:var_t:s0->unconfined_u:object_r:httpd_sys_content_t:s0
```

如果我们现在尝试访问该页面，它将再次显示我们的“这是一个测试网页”文本。

这是一个重要的概念要理解：确保文件和目录有正确的上下文是确保SELinux行为应该是关键。 我们将在本节结尾处看到一个实际的用例，但在此之前，让我们再谈谈一些事情。

## 文件和目录的上下文继承

SELinux强制执行我们可以称为“上下文继承”的东西。 这意味着除非策略指定，过程和文件是使用父对象的上下文创建的。

因此，如果我们有一个过程称为“PROC _A”产卵名为“PROC B”anoher过程中_ ，生成的进程将在同一个域中“proc_a”除非SELinux策略另有规定执行。

同样，如果我们有一个类型的“一些_背景_ T”的目录，下创建的任何文件或目录，除非将政策说，否则有相同类型的上下文。

为了说明这一点，让我们检查的背景下`/var/www/`目录：

```
ls -Z /var/www
```

在`html`中的目录`/var/www/`有**httpd的_SYS_ content_t**类型的上下文。 正如我们前面所看到的， `index.html`在它的文件具有相同背景下（即父的上下文中）：

```
drwxr-xr-x. root root system_u:object_r:httpd_sys_script_exec_t:s0 cgi-bin
drwxr-xr-x. root root system_u:object_r:httpd_sys_content_t:s0 html
```

将文件复制到其他位置时，不会保留此继承。 在复制操作中，复制的文件或目录将采用目标位置的类型上下文。 在下面的代码片段中，我们复制`index.html`文件（“httpd的_SYS_ content_t”类型的上下文中）到`/var/`目录：

```
cp /var/www/html/index.html /var/
```

如果我们检查复制的文件的情况下，我们将看到它已更改为**var_t，**其当前的父目录的背景下：

```
ls -Z /var/index.html
```

```
-rwxr-xr-x. root root unconfined_u:object_r:var_t:s0   /var/index.html
```

上下文的这种变化可以通过覆盖`--preserver=context`中的条款`cp`命令。

移动文件或目录时，将保留原始上下文。 在下面的命令，我们正在移动`/var/index.html`到`/etc/`目录下：

```
mv  /var/index.html  /etc/
```

当我们检查移动文件的背景下，我们看到**var_t**方面已经下保存`/etc/`目录下：

```
ls -Z /etc/index.html
```

```
-rwxr-xr-x. root root unconfined_u:object_r:var_t:s0   /etc/index.html
```

那么为什么我们这么关心文件上下文？ 为什么这个复制和移动的概念很重要？ 想想吧：也许你决定把所有的网络服务器的HTML文件复制到根文件夹下的一个单独的目录。 您已经做到这一点，以简化您的备份过程，并收紧安全性：您不希望任何黑客轻松猜到您的网站的文件的位置。 您已更新目录的访问控制，更改了Web配置文件以指向新位置，重新启动服务，但它仍然不工作。 也许你可以查看目录及其文件的上下文作为下一个故障排除步骤。 让我们运行它作为一个实际的例子。

### SELinux在操作中：测试文件上下文错误

首先，让我们创建一个名为`www`的根目录下。 我们还将创建一个文件夹，名为`html`下的`www` 。

```
mkdir -p /www/html
```

如果我们运行`ls -Z`命令，我们将看到这些目录已与上下文**的default_t**创建：

```
ls -Z /www/
```

```
drwxr-xr-x. root root unconfined_u:object_r:default_t:s0 html
```

接下来，我们复制的内容`/var/www/html`目录`/www/html` ：

```
cp /var/www/html/index.html /www/html/
```

复制的文件**的default_t**有一个背景。 这是父目录的上下文。

我们现在编辑`httpd.conf`文件指向这个新的目录作为网站的根文件夹。 我们还将放宽此目录的访问权限。

```
vi /etc/httpd/conf/httpd.conf
```

首先，我们注释掉文档根现有的位置，并添加新`DocumentRoot`指令`/www/html` ：

```
# DocumentRoot "/var/www/html"

DocumentRoot "/www/html"
```

我们还注释了现有文档根目录的访问权限部分，并添加了一个新的部分：

```
#<Directory "/var/www">
#    AllowOverride None
    # Allow open access:
#    Require all granted
#</Directory>

<Directory "/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>
```

我们离开的位置`cgi-bin`目录，因为它是。 我们不在这里进入详细的Apache配置; 我们只是希望我们的网站为SELinux的目的工作。

最后，重新启动httpd守护进程：

```
service httpd restart
```

一旦服务器重新启动，访问网页会给我们以前看到的“403 Forbidden”错误（或默认的“Testing 123”页面）。

该错误是因为发生了`index.html`文件的背景下复制操作过程中改变。 它需要改回原来的上下文（httpd的_SYS_ content_t）。

但是我们怎么做呢？

## 更改和恢复SELinux文件上下文

在前面的代码示例，我们看到了改变文件内容的两个命令： `chcon`和`restorecon` 。 运行`chcon`是一项临时措施。 您可以使用它临时更改文件或目录上下文以解决访问拒绝错误。 但是，这种方法只是暂时的：一个文件系统重新标记或运行`restorecon`命令将恢复该文件返回到原来的上下文。

此外，运行`chcon`要求您知道该文件的正确的上下文; 在`--type`标志指定目标的上下文。 `restorecon`不需要此规定。 如果您运行`restorecon` ，该文件将具有正确的上下文重新应用所做的更改将成为永久性。

但是，如果你不知道文件的正确的情况下，系统怎样知道当它运行到适用的情况下`restorecon` ？

方便地，SELinux“记住”服务器中每个文件或目录的上下文。 在CentOS 7，在系统中已存在的文件的上下文中列出了`/etc/selinux/targeted/contexts/files/file_contexts`文件。 它是一个大文件，它列出了每个与Linux发行版支持的每个应用程序相关联的文件类型。 的新的目录和文件的上下文被记录在`/etc/selinux/targeted/contexts/files/file_contexts.local`文件。 所以，当我们运行`restorecon`命令，SELinux的将查找正确的上下文这两个文件中的一个，并将其应用到目标。

以下代码段显示了从其中一个文件中提取的内容：

```
cat /etc/selinux/targeted/contexts/files/file_contexts
```

```
...
/usr/(.*/)?lib(/.*)?    system_u:object_r:lib_t:s0
/opt/(.*/)?man(/.*)?    system_u:object_r:man_t:s0
/dev/(misc/)?agpgart    -c      system_u:object_r:agp_device_t:s0
/usr/(.*/)?sbin(/.*)?   system_u:object_r:bin_t:s0
/opt/(.*/)?sbin(/.*)?   system_u:object_r:bin_t:s0
/etc/(open)?afs(/.*)?   system_u:object_r:afs_config_t:s0
...
```

要永久地改变我们的下index.html文件的情况下`/www/html` ，我们必须遵循两个步骤的过程。

-   首先，我们运行`semanage fcontext`命令。 这将写入新背景下的`/etc/selinux/targeted/contexts/files/file_contexts.local`文件。 但它不会重新标记文件本身。 我们将为这两个目录。

```
semanage fcontext --add --type httpd_sys_content_t "/www(/.*)?"
```

```
semanage fcontext --add --type httpd_sys_content_t "/www/html(/.*)?"
```

为了确保，我们可以检查文件上下文数据库（请注意，我们使用的是`file_contexts.local`文件）：

```
cat /etc/selinux/targeted/contexts/files/file_contexts.local
```

您应该会看到更新的上下文：

```
# This file is auto-generated by libsemanage
# Do not edit directly.

/www(/.*)?    system_u:object_r:httpd_sys_content_t:s0
/www/html(/.*)?    system_u:object_r:httpd_sys_content_t:s0
```

接下来，我们将运行`restorecon`命令。 这将使用上一步骤中记录的内容重新标记文件或目录：

```
restorecon -Rv /www
```

这应该重置背景下三个层次：顶层`/www`目录， `/www/html`其下的目录和`index.html`下档`/www/html` ：

```
restorecon reset /www context unconfined_u:object_r:default_t:s0->unconfined_u:object_r:httpd_sys_content_t:s0
restorecon reset /www/html context unconfined_u:object_r:default_t:s0->unconfined_u:object_r:httpd_sys_content_t:s0
restorecon reset /www/html/index.html context unconfined_u:object_r:default_t:s0->unconfined_u:object_r:httpd_sys_content_t:s0
```

如果我们现在尝试访问网页，它应该工作。

有一个叫极好的工具`matchpathcon` ，可以帮助解决环境有关的问题。 此命令将查看资源的当前上下文，并将其与SELinux上下文数据库中列出的内容进行比较。 如果不同，它将建议所需的更改。 让我们来测试这与`/www/html/index.html`文件。 我们将使用`-V`的验证环境标志：

```
matchpathcon -V /www/html/index.html
```

所述`matchpathcon`输出应显示的上下文进行验证。

```
/www/html/index.html verified.
```

对于未正确标记的文件，消息将说明上下文应该是什么：

```
/www/html/index.html has context unconfined_u:object_r:default_t:s0, should be system_u:object_r:httpd_sys_content_t:s0 
```

## 域转换

到目前为止，我们已经看到了进程如何访问文件系统资源。 我们现在将看到进程如何访问其他进程。

_域转换_是一个进程改变了它的上下文从一个域到另一个方法。 为了了解它，让我们说你有一个名为PROC _正在运行的contexta_吨_的范围内_处理。 随着域转换，PROC _一个可以运行的应用程序（程序或可执行脚本）称为_ X _程序_ ，将_产生_另一个进程。 这种新工艺可称为PROC _B，它可以在contextb_牛逼_域内运行_ 。 所以有效，contexta _t为_通过__转换__应用_X_ _要contextb_吨_。_ _该应用程序_ X可执行工作作为一个_入口点_到contextb_t。 流程可以如下图所示：

![SELinux域转换](https://www.youcl.com/uploads/1/image/public/201806/20180601164124_3dfqanubr3.jpg)

域转换的情况在SELinux中是相当常见的。 让我们考虑在我们的服务器上运行的vsftpd进程。 如果它没有运行，我们就可以运行`service vsftpd start`命令，启动守护进程。

接下来我们考虑systemd进程。 这是所有进程的祖先。 这是**init_t**的范围内更换系统V init进程并运行的。 ：

```
ps -eZ  | grep init
```

```
system_u:system_r:init_t:s0         1 ?        00:00:02 systemd
system_u:system_r:mdadm_t:s0      773 ?        00:00:00 iprinit
```

在**init_t**域内运行的进程是一个短命的：它会调用二进制可执行文件`/usr/sbin/vsftpd` ，其中有**ftpd的_EXEC_吨**类型的上下文。 当二进制可执行文件开始，它成为vsftpd的守护程序本身和**ftpd_t**域内运行。

我们可以检查文件和进程的域上下文：

```
ls -Z /usr/sbin/vsftpd
```

显示我们：

```
-rwxr-xr-x. root root system_u:object_r:ftpd_exec_t:s0 /usr/sbin/vsftpd
```

检查过程：

```
ps -eZ | grep vsftpd
```

显示我们：

```
system_u:system_r:ftpd_t:s0-s0:c0.c1023 7708 ? 00:00:00 vsftpd
```

所以在这里的**init_t**域中运行的进程正在执行与**ftpd的_EXEC_ T**形的二进制文件。 该文件将启动**ftpd_t**域内的守护进程。

这种转换不是应用程序或用户可以控制的。 这在SELinux策略中规定，在系统引导时加载到内存中。 在非SELinux服务器中，用户可以通过切换到更强大的帐户（假如她或他有权这样做）来启动进程。 在SELinux中，这种访问由预先编写的策略控制。 这是SELinux被称为实施强制访问控制的另一个原因。

域转换受三个严格规则约束：

-   源域的父进程必须有两个应用程序域之间坐在执行权限（这是_入口点_ ）。
    
-   该应用程序的文件上下文必须被确定为目标域的_入口点_ 。
    
-   必须允许原始域转换到目标域。
    

考虑上述vsftpd的守护进程的例子，让我们运行`sesearch`用不同的开关命令，看是否符合守护这三个规则。

首先，源域初始化_牛逼需要有与ftpd服务_ exec_t上下文_中的应用程序入口点执行权限_ 。 所以如果我们运行以下命令：

```
sesearch -s init_t -t ftpd_exec_t -c file -p execute -Ad
```

结果表明，初始化_牛逼域内_进程_可以读，得到的属性，执行和ftpd_ exec_t _上下文打开文件_ ：

```
Found 1 semantic av rules:
   allow init_t ftpd_exec_t : file { read getattr execute open } ;
```

接下来，我们检查二进制文件是否是目标域的入口点ftpd_t：

```
sesearch -s ftpd_t -t ftpd_exec_t -c file -p entrypoint -Ad
```

事实上是这样的：

```
Found 1 semantic av rules:
   allow ftpd_t ftpd_exec_t : file { ioctl read getattr lock execute execute_no_trans entrypoint open } ;
```

最后，源域初始化_牛逼需要有权限过渡到目标域ftpd的_ T：

```
sesearch -s init_t -t ftpd_t -c process -p transition -Ad
```

如下图所示，源域具有该权限：

```
Found 1 semantic av rules:
   allow init_t ftpd_t : process transition ;
```

## 无限域名

当我们介绍领域的概念时，我们将其与流程中的假设泡沫进行比较：规定了流程可以做什么和不能做什么的事情。 这就是限制这个过程。

SELinux还有在无限域内运行的进程。 可以想象，无约束的进程将拥有系统中的所有类型的访问。 即使这样，这个完全访问不是任意的：完全访问也在SELinux策略中指定。

无约束进程域的示例是unconfined_t。 这是默认情况下在用户运行其进程时记录的同一域。 我们将在后续章节中讨论用户及其对进程域的访问。

## 结论

我们今天在这里介绍了一些非常重要的SELinux概念。 管理文件和流程上下文是成功的SELinux实现的核心。 正如我们将在未来看到[这个系列的最后一部分](https://www.youcl.com/info/10513) ，还有另一块剩余拼图：SELinux的用户。


