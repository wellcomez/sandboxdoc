---
date: 2023-04-13 14:30
title: 简介SELinux的在CentOS 7  - 第3部分 用户
tags:
- 
---



# 简介SELinux的在CentOS 7  - 第3部分：用户

2014-09-05 分类：[系统运维](https://www.youcl.com/node/44) 阅读(40) 评论(0)

### 介绍

在我们的SELinux教程的最后一部分，我们将讨论SELinux用户以及如何微调他们的访问。 我们还将了解SELinux错误日志以及如何了解错误消息。

> **注意**  
> 本教程中显示的命令，包和文件在CentOS 7上进行了测试。其他发行版的概念保持不变。

在本教程中，除非另有说明，否则我们将以root用户身份运行命令。 如果您没有访问到root帐户，并使用使用sudo权限的其他帐户，您需要与先于命令`sudo`关键字。

## SELinux用户

SELinux用户是与普通Linux用户帐户不同的实体，包括root帐户。 SELinux用户不是使用特殊命令创建的，也不具有对服务器的自己的登录访问权限。 相反，SELinux用户在引导时加载到内存的策略中定义，并且只有少数用户。 用户名称以`_u` ，就像类型或域名结束与`_t`和角色结束`_r` 。 不同的SELinux用户在系统中具有不同的权限，这就是它们的用途。

在文件安全上下文的第一部分中列出的SELinux用户是拥有该文件的用户。 这就像你会看到文件的所有者从一个普通`ls -l`命令的输出。 进程上下文中的用户标签显示进程正在运行的SELinux用户权限。

当强制执行SELinux时，每个常规Linux用户帐户都映射到SELinux用户帐户。 可以有多个用户帐户映射到同一个SELinux用户。 此映射使常规帐户能够继承其SELinux对应方的权限。

要查看该映射，我们就可以运行`semanage login -l`命令：

```
semanage login -l
```

在CentOS 7中，我们可以看到：

```
Login Name           SELinux User         MLS/MCS Range        Service

__default__          unconfined_u         s0-s0:c0.c1023       *
root                 unconfined_u         s0-s0:c0.c1023       *
system_u             system_u             s0-s0:c0.c1023       *
```

此表中的第一列“登录名”表示本地Linux用户帐户。 但是这里只列出了三个，你可能会问，我们在本教程的第二部分中没有创建几个帐户吗？ 是的，并且它们被示出为****默认****的条目表示。 任何普通的Linux用户帐户被首先映射到****默认****登录。 然后将其映射到名为unconfined_u的SELinux用户。 在我们的例子中，这是第一行的第二列。 第三列显示用户的多级安全/多类别安全（MLS / MCS）类。 现在，让我们忽略该部分，以及之后的列（服务）。

接下来，我们有**根**用户。 请注意，这不是映射到“ **默认** ”的登录，而是被赋予了其自身的条目。 同样，root也映射到unconfined_u SELinux用户。

system_u是不同类的用户，用于运行进程或守护程序。

要了解SELinux的用户在系统中可用，我们可以运行`semanage user`命令：

```
semanage user -l
```

我们的CentOS 7系统的输出应该是这样：

```
                 Labeling   MLS/       MLS/
SELinux User    Prefix     MCS Level  MCS Range                      SELinux Roles

guest_u         user       s0         s0                             guest_r
root            user       s0         s0-s0:c0.c1023                 staff_r sysadm_r system_r unconfined_r
staff_u         user       s0         s0-s0:c0.c1023                 staff_r sysadm_r system_r unconfined_r
sysadm_u        user       s0         s0-s0:c0.c1023                 sysadm_r
system_u        user       s0         s0-s0:c0.c1023                 system_r unconfined_r
unconfined_u    user       s0         s0-s0:c0.c1023                 system_r unconfined_r
user_u          user       s0         s0                             user_r
xguest_u        user       s0         s0                             xguest_r
```

这个更大的表是什么意思？ 首先，它显示策略定义的不同SELinux用户。 我们曾经看过用户喜欢unconfined_u和system_u，但我们现在看到的其他类型的用户，如guest_u，staff_u，sysadm_u，user_u等。 这些名称在某种程度上表示与他们相关的权利。 例如，我们可以假设sysadm_u用户比guest_u拥有更多的访问权限。

为了验证我们的客人，让我们来看看第五列，SELinux角色。 如果你还记得本教程的第一部分，SELinux角色就像是用户和进程之间的网关。 我们还比较了它们的过滤器：用户可以_输入_一个角色，只要角色授予它。 如果角色被授权访问进程域，与该角色关联的用户将能够进入该进程域。

现在从该表中可以看到`unconfined_u`用户被映射到`system_r`和`unconfined_r`角色。 虽然在这里没有明显，SELinux策略实际上允许这些角色在运行过程中`unconfined_t`域。 同样，用户`sysadm_u`被授权为_SYSADMř作用，但客人_ u被映射到guest_r作用。 这些角色中的每一个都将拥有为其授权的不同域。

现在，如果我们退后一步，我们也从第一代码片断看到， ****默认****的登录映射到潜水_-u用户，就像root用户映射到用户unconfined_u。_ _由于**_ _默认_ _ **登录表示任何普通的Linux用户帐户，这些帐户将被授权system_r和unconfined_r角色为好。

所以这真的意味着任何映射到unconfined_u用户的Linux用户都有权限运行在unconfined_t域中运行的任何应用程序。

为了证明这一点，让我们运行`id -Z`命令作为根用户：

```
id -Z
```

这表明**root**的SELinux的安全上下文：

```
unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

因此，root帐户映射到unconfined_u SELinux用户，unconfined_u对unconfined_r角色授权，而unconfined_r角色又被授权运行unconfined_t域中的进程。

我们建议您现在花时间与从独立终端窗口创建的四个用户开始四个新的SSH会话。 这将有助于我们在需要时在不同的帐户之间切换。

-   普通用户
-   切换用户
-   访客
-   restricteduser

接下来，我们切换到以常规用户身份登录的终端会话。 如果你还记得，我们创建了一些用户账户中的[第二个教程](https://www.youcl.com/info/10512) ，并regularuser是其中之一。 如果您尚未这样做，请打开一个单独的终端窗口，以常规用户身份连接到CentOS 7系统。 如果我们执行相同的`id -Z`从那里指挥，输出会是这样的：

```
[regularuser@localhost ~]$ id -Z
```

```
unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

在这种情况下，regulauser帐户映射到unconfined_u SELinux用户帐户，它可以承担unconfined_r角色。 角色可以在无约束域中运行进程。 这与root帐户也映射到的SELinux用户/角色/域相同。 这是因为SELinux目标策略允许登录用户在无限制域中运行。

我们以前看过一些SELinux用户的列表：

-   **guest_u：**该用户还没有访问的X-Window系统（GUI）或联网和不能执行ス/Sudo命令。
-   **xguest_u：**此用户可以访问GUI工具和网络通过Firefox浏览器是可用的。
-   **user_u：**此用户拥有比来宾帐户（GUI和网络）更多的机会，但不能运行su或sudo的切换用户。
-   **staff_u：**同样的权利，user_u，它除了可以执行sudo命令有root权限。
-   **system_u：**该用户是指对于运行系统的服务，而不是被映射到一般用户帐户。

## SELinux在操作1：限制交换式用户访问

要了解SELinux如何为用户帐户强制执行安全性，让我们考虑常规用户帐户。 作为系统管理员，你现在知道用户具有相同_的SELinux_不受限制_的特权_作为root帐户，你想改变这种状况。 具体来说，您不希望用户能够切换到其他帐户，包括root帐户。

让我们先检查用户切换到其他帐户的能力。 在以下代码段中，常规用户切换到switcheduser帐户。 我们假设他知道switcheduser的密码：

```
[regularuser@localhost ~]$ su - switcheduser
Password:
[switcheduser@localhost ~]$
```

接下来，我们再回到登录为**root**用户，并改变regularuser的SELinux的用户映射终端窗口。 我们将把regularuser映射到user_u。

```
semanage login -a -s user_u regularuser
```

那么我们在这里做什么？ 我们将（-a）常规用户帐户添加到SELinux（-s）用户帐户user_u。 该更改将在常规用户注销并重新登录后才生效。

回到常规用户的终端窗口，我们首先从切换的用户切换回：

```
[switcheduser@localhost ~]$ logout
```

接下来，regularuser也注销：

```
[regularuser@localhost ~]$ logout
```

然后我们打开一个新的终端窗口，以正常用户身份连接。 接下来，我们尝试再次更改为switcheduser：

```
[regularuser@localhost ~]$ su - switcheduser
```

```
Password:
```

这是我们现在看到的：

```
su: Authentication failure
```

如果我们现在运行`id -Z`再次命令可以查看regularuser SELinux上下文，我们将看到输出从我们之前看到的完全不同：regularuser现在映射到user_u。

```
[regularuser@localhost ~]$ id -Z
```

```
user_u:user_r:user_t:s0
```

那么你将在哪里使用这些限制？ 您可以考虑在您的IT组织内部的应用程序开发团队。 您可以在该团队中有一些开发人员和测试人员为您的公司编写和测试最新的应用程序。 作为系统管理员，您知道开发人员正在从其帐户切换到某些高特权帐户，以便对您的服务器进行特别更改。 您可以通过限制其切换帐户的能力来阻止这种情况的发生。 （注意，虽然，它仍然不会阻止他们直接登录为高权限的用户）。

## SELinux在操作2中：限制运行脚本的权限

让我们看看另一个通过SELinux限制用户访问的例子。 运行从**根**会话这些命令。

默认情况下，SELinux允许映射到guest_t帐户的用户从其主目录执行脚本。 我们可以运行`getsebool`命令检查布尔值：

```
getsebool allow_guest_exec_content
```

输出显示标志已打开。

```
guest_exec_content --> on
```

为了验证它的效果，让我们首先更改我们在本教程开头创建的guestuser帐户的SELinux用户映射。 我们将以root用户身份。

```
semanage login -a -s guest_u guestuser
```

我们可以通过运行验证动作的`semanage login -l`再次命令：

```
semanage login -l
```

我们可以看到，guestuser现在映射到guest_u SELinux用户帐户。

```
Login Name           SELinux User         MLS/MCS Range        Service

__default__          unconfined_u         s0-s0:c0.c1023       *
guestuser            guest_u              s0                   *
regularuser          user_u               s0                   *
root                 unconfined_u         s0-s0:c0.c1023       *
system_u             system_u             s0-s0:c0.c1023       *
```

如果我们有一个终端窗口作为guestuser打开，我们将从它注销，并作为guestuser回到一个新的终端窗口。

接下来，我们将在用户的主目录中创建一个非常简单的bash脚本。 以下代码块首先检查主目录，然后创建文件并在控制台上读取它。 最后更改执行权限。

验证您是否在`guestuser`主目录：

```
[guestuser@localhost ~]$ pwd
```

```
/home/guestuser
```

创建脚本：

```
[guestuser@localhost ~]$ vi myscript.sh
```

脚本内容：

```
#!/bin/bash
echo "This is a test script"
```

使脚本可执行：

```
chmod u+x myscript.sh
```

当我们尝试以guestuser身份执行脚本时，它会按预期工作：

```
[guestuser@localhost ~]$ ~/myscript.sh
```

```
This is a test script
```

接下来我们回到根终端窗口，改变布尔设置allow_guest_exec_content至`off`和验证：

```
setsebool allow_guest_exec_content off
getsebool allow_guest_exec_content
```

```
guest\_exec\_content --> off
```

回到以guestuser身份登录的控制台，我们尝试再次运行脚本。 这一次，访问被拒绝：

```
[guestuser@localhost ~]$ ~/myscript.sh
```

```
-bash: /home/guestuser/myscript.sh: Permission denied
```

所以这就是SELinux如何在DAC之上应用额外的安全层。 即使用户对在其自己的主目录中创建的脚本进行了完全读取，写入和执行访问，它们仍然可以停止执行它。 你需要在哪里？ 好了，想想一个生产系统。 你知道开发人员可以像为你公司工作的一些承包商一样。 您希望他们访问服务器以查看错误消息和日志文件，但不希望它们执行任何shell脚本。 为此，您可以先启用SELinux，然后确保设置相应的布尔值。

我们将谈论的SELinux错误消息不久，但现在，如果我们急于看到此拒绝被记录，我们可以看看`/var/log/messages`的文件。 从根会话执行此操作：

```
grep "SELinux is preventing" /var/log/messages
```

在我们的CentOS 7服务器文件中的最后两个消息显示访问拒绝：

```
Aug 23 12:59:42 localhost setroubleshoot: SELinux is preventing /usr/bin/bash from execute access on the file . For complete SELinux messages. run sealert -l 8343a9d2-ca9d-49db-9281-3bb03a76b71a
Aug 23 12:59:42 localhost python: SELinux is preventing /usr/bin/bash from execute access on the file .
```

该消息还显示了一个很长的ID值，并建议我们运行`sealert`这个ID命令的详细信息。 以下命令显示此（使用您自己的警报ID）：

```
sealert -l 8343a9d2-ca9d-49db-9281-3bb03a76b71a
```

事实上，输出向我们显示了有关错误的更多详细信息：

```
SELinux is preventing /usr/bin/bash from execute access on the file .

*****  Plugin catchall_boolean (89.3 confidence) suggests   ******************

If you want to allow guest to exec content
Then you must tell SELinux about this by enabling the 'guest\_exec\_content' boolean.
You can read 'None' man page for more details.
Do
setsebool -P guest\_exec\_content 1

*****  Plugin catchall (11.6 confidence) suggests   **************************

...

```

这是一个大量的输出，但请注意开始时的几行：

**SELinux阻止/ usr / bin / bash对文件执行访问。**

这给我们一个很好的想法，错误是从哪里来的。

接下来的几行也告诉你如何解决错误：

```
If you want to allow guest to exec content
Then you must tell SELinux about this by enabling the 'guest\_exec\_content' boolean.
...
setsebool -P guest\_exec\_content 1
```

## SELinux在操作3：限制对服务的访问

在[本系列的第一部分，](https://www.youcl.com/info/10511)我们谈到了SELinux的角色时，我们引入了用户，角色，域和类型的基本术语。 现在让我们看看角色如何在限制用户访问方面发挥作用。 正如我们之前所说，SELinux中的角色位于用户和进程域之间，并控制用户进程可以进入的域。 当我们在文件安全上下文中看到它们时，角色并不重要。 对于文件，它列出了一个通用的值object_r。 在处理用户和进程时，角色变得重要。

让我们首先确保httpd守护程序没有在系统中运行。 作为root用户，您可以运行以下命令以确保进程已停止：

```
service httpd stop
```

接下来，我们切换到我们以restricteduser身份登录的终端窗口，并尝试查看它的SELinux安全上下文。 如果您没有打开终端窗口，请针对系统启动新的终端会话，并以我们在本教程开头创建的restricteduser帐户登录。

```
[restricteduser@localhost ~]$ id -Z
unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
```

因此，该帐户具有作为unconfined_u用户运行并且具有访问unconfined_r角色的默认行为。 但是，此帐户没有权限在系统中启动任何进程。 以下代码块显示restricteduser尝试启动httpd守护程序并获得访问被拒绝错误：

```
[restricteduser@localhost ~]$ service httpd start
Redirecting to /bin/systemctl start  httpd.service
Failed to issue method call: Access denied
```

接下来，我们回到root用户终端窗口，并确保restricteduser帐户已添加到/ etc / sudoers文件。 此操作将使restricteduser帐户能够使用root权限。

```
visudo
```

然后在文件中，添加以下行，保存并退出：

```
restricteduser ALL=(ALL)      ALL
```

如果我们现在注销restricteduser终端窗口并重新登录，我们可以使用sudo权限启动和停止httpd服务：

```
[restricteduser@localhost ~]$ sudo service httpd start
```

```

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for restricteduser:
Redirecting to /bin/systemctl start  httpd.service
```

用户也可以立即停止服务：

```
[restricteduser@localhost ~]$ sudo service httpd stop
```

```
Redirecting to /bin/systemctl stop  httpd.service
```

这是非常正常的：系统管理员给sudo访问他们信任的用户帐户。 但是，如果要停止此特定用户启动httpd服务，即使用户的帐户在sudoers文件中列出，该怎么办？

要了解如何实现这一点，让我们切换回root用户的终端窗口，并将restricteduser映射到SELinux user_r帐户。 这是我们在另一个例子中为常客用户帐户做的。

```
semanage login -a -s user_u restricteduser
```

回到restricteduser的终端窗口，我们注销并在新的终端会话中作为restricteduser重新登录。

现在，restricteduser已被限制user_u（这意味着角色角色user_r和域为user_t），我们可以使用验证其访问`seinfo`命令从我们的根用户的窗口：

```
seinfo -uuser_u -x
```

输出显示user_u可以承担的角色。 这些是object_r和user_r：

```
   user_u
      default level: s0
      range: s0
      roles:
         object_r
         user_r
```

把它一步，我们就可以运行`seinfo`命令检查什么域user_r角色被授权进入：

```
seinfo -ruser_r -x
```

有多个域user_r有权输入：

```
   user_r
      Dominated Roles:
         user_r
      Types:
         git_session_t
         sandbox_x_client_t
         git_user_content_t
         virt_content_t
         policykit_grant_t
         httpd_user_htaccess_t
         telepathy_mission_control_home_t
         qmail_inject_t
         gnome_home_t
         ...
         ...
```

但是这个列表是否显示httpd_t作为域之一？ 让我们使用一个过滤器尝试相同的命令：

```
seinfo -ruser_r -x | grep httpd
```

角色有权访问的许多httpd相关域，但httpd_t不是其中之一：

```
         httpd_user_htaccess_t
         httpd_user_script_exec_t
         httpd_user_ra_content_t
         httpd_user_rw_content_t
         httpd_user_script_t
         httpd_user_content_t
```

以此为例，如果restricteduser帐户尝试启动httpd守护程序，则应拒绝访问，因为httpd进程在httpd_t域中运行，并且不是user_r角色被授权访问的域之一。 而且我们知道user_u（映射到restricteduser）可以假设user_r角色。 即使restricteduser帐户已被授予sudo权限，这也应该失败。

回到restricteduser帐户的终端窗口，我们尝试立即启动httpd守护程序（我们能够阻止它，因为该帐户被授予sudo权限）：

```
[restricteduser@localhost ~]$ sudo service httpd start
```

访问被拒绝：

```
sudo: PERM_SUDOERS: setresuid(-1, 1, -1): Operation not permitted
```

所以，还有另一个例子，说明SELinux如何工作，就像一个网守。

## SELinux审计日志

作为系统管理员，您将有兴趣查看由SELinux记录的错误消息。 这些消息记录在特定文件中，并且可以提供有关拒绝访问的详细信息。 在CentOS 7系统中，您可以查看两个文件：

-   `/var/log/audit/audit.log`
-   `/var/log/messages`

这些文件分别由auditd守护程序和rsyslogd守护程序填充。 那么这些守护进程做什么？ 手册页说，auditd守护程序是Linux审核系统的用户空间组件，rsyslogd是提供消息日志记录支持的系统实用程序。 简单地说，这些守护程序在这两个文件中记录错误消息。

该`/var/log/audit/audit.log`如果auditd守护进程运行时文件将被使用。 将`/var/log/messages` ，如果auditd调用停止并rsyslogd正在运行使用的文件。 如果这两个守护程序正在运行，这两个文件被使用： `/var/log/audit/audit.log`记录的详细信息，而一个易于阅读的版本保存在`/var/log/messages` 。

### 解密SELinux错误消息

我们在前面的章节中查看了一个SELinux错误消息（请参考“SELinux in Action 2：Restricting Permissions to Run Scripts”）。 然后我们使用`grep`命令来筛选， `/var/log/messages`的文件。 幸运的是，SELinux提供了一些工具，使生活更容易一些。 默认情况下不安装这些工具，并且需要安装一些软件包，您应该在本教程的第一部分中安装这些软件包。

第一个命令是`ausearch` 。 如果auditd守护程序正在运行，我们可以使用此命令。 在下面的代码片段中，我们试图查看与httpd守护程序相关的所有错误消息。 确保您在您的根帐户：

```
ausearch -m avc -c httpd
```

在我们的系统中列出了一些条目，但我们将集中在最后一个：

```
----
time->Thu Aug 21 16:42:17 2014
...
type=AVC msg=audit(1408603337.115:914): avc:  denied  { getattr } for  pid=10204 comm="httpd" path="/www/html/index.html" dev="dm-0" ino=8445484 scontext=system_u:system_r:httpd_t:s0 tcontext=unconfined_u:object_r:default_t:s0 tclass=file
```

即使有经验的系统管理员可能会被这样的消息困惑，除非他们知道他们在寻找什么。 为了理解它，让我们分开每个字段：

-   类型= AVC和AVC：AVC代表_访问矢量缓存_ 。 SELinux缓存资源和进程的访问控制决策。 此缓存称为访问矢量缓存（AVC）。 这就是为什么SELinux访问拒绝消息也称为“AVC拒绝”。 这两个信息字段表示条目来自AVC日志，它是AVC事件。
    
-   denied {getattr}：尝试的权限和它获得的结果。 在这种情况下，get属性操作被拒绝。
    
-   pid = 10204。 这是尝试访问的进程的进程标识。
    
-   comm：进程id本身并不意味着太多。 comm属性显示了进程命令。 在这种情况下，它是httpd。 马上我们知道错误是从Web服务器。
    
-   path：被访问的资源的位置。 在这种情况下，它是/www/html/index.html下的文件。
    
-   dev和ino：目标资源所在的设备及其inode地址。
    
-   scontext：进程的安全上下文。 我们可以看到源在httpd_t域下运行。
    
-   tcontext：目标资源的安全上下文。 在这种情况下，文件类型为default_t。
    
-   tclass：目标资源的类。 在这种情况下，它是一个文件。
    

如果仔细看，进程域是httpd_t，文件的类型上下文是default_t。 由于httpd守护程序在受限域中运行，而SELinux策略规定此域不具有对default_t类型的文件的任何访问权，因此访问被拒绝。

我们已经看到了`sealert`工具。 该命令可以用值记录在错误信息的ID被用来`/var/log/messages`的文件。

在下面的代码片段，我们再次`grep`通过的`/var/log/message` SELinux的相关的错误文件：

```
cat /var/log/messages | grep "SELinux is preventing"
```

在我们的系统中，我们看看最后一个错误。 这是当我们的restricteduser尝试运行httpd守护程序时记录的错误：

```
...
Aug 25 11:59:46 localhost setroubleshoot: SELinux is preventing /usr/bin/su from using the setuid capability. For complete SELinux messages. run sealert -l e9e6c6d8-f217-414c-a14e-4bccb70cfbce
```

至于建议，我们跑`sealert`的ID值，并能看到细节（你的ID值应该是唯一的对您的系统）：

```
sealert -l e9e6c6d8-f217-414c-a14e-4bccb70cfbce
```

```
SELinux is preventing /usr/bin/su from using the setuid capability.

...

Raw Audit Messages
type=AVC msg=audit(1408931985.387:850): avc:  denied  { setuid } for  pid=5855 comm="sudo" capability=7  scontext=user_u:user_r:user_t:s0 tcontext=user_u:user_r:user_t:s0 tclass=capability


type=SYSCALL msg=audit(1408931985.387:850): arch=x86_64 syscall=setresuid success=no exit=EPERM a0=ffffffff a1=1 a2=ffffffff a3=7fae591b92e0 items=0 ppid=5739 pid=5855 auid=1008 uid=0 gid=1008 euid=0 suid=0 fsuid=0 egid=0 sgid=1008 fsgid=0 tty=pts2 ses=22 comm=sudo exe=/usr/bin/sudo subj=user_u:user_r:user_t:s0 key=(null)

Hash: su,user_t,user_t,capability,setuid
```

我们已经看到的输出的前几行如何`sealert`告诉我们有关的补救措施。 但是，如果我们现在看到输出流的末尾，我们可以看到“原始审计消息”部分。 这里的项目从未来的`audit.log`文件，这是我们前面所讨论的，所以你可以使用该部分来帮助你在这里解释输出。

## 多级安全

多级安全性或**MLS**是一个SELinux安全上下文的细粒度的一部分。

到目前为止，在讨论进程，用户或资源的安全上下文时，我们一直在谈论三个属性：SELinux用户，SELinux角色和SELinux类型或域。 安全上下文的第四字段示出了_灵敏度_和任选的资源的_类别_ 。

为了理解它，让我们考虑FTP守护进程的配置文件的安全上下文：

```
ls -Z /etc/vsftpd/vsftpd.conf
```

安全上下文的第四个字段显示s0的敏感性。

```
-rw-------. root root system_u:object_r:etc_t:s0       /etc/vsftpd/vsftpd.conf
```

灵敏度是_分层_多级安全机制的一部分。 按层次结构，我们的意思是敏感级别可以更深入和更深的文件系统中更安全的内容。 级别0（由s0描绘）是最低灵敏度级别，可以说是“公共”。 可以存在具有较高s值的其他灵敏度水平：例如，内部，机密或调节可以分别由s1，s2和s3描述。 此映射不受策略规定：系统管理员可以配置每个敏感级别的含义。

当启用了SELinux系统使用MLS其策略类型（在所配置`/etc/selinux/config`文件），它可以与灵敏度的特定级别标记某些文件和进程。 最低电平称为“电流灵敏度”，最高电平称为“间隙灵敏度”。

去手在手的灵敏度是资源的_种类_ ，用C表示。 类别可以被视为分配给资源的标签。 类别的示例可以是部门名称，客户名称，项目等。分类的目的是进一步微调访问控制。 例如，您可以对来自两个不同内部部门的用户的具有机密敏感性的某些文件进行标记。

对于SELinux安全上下文，敏感性和类别在实现类别时一起工作。 当使用灵敏度级别范围时，格式是显示由连字符分隔的灵敏度级别（例如，s0-s2）。 使用类别时，范围将在其间显示一个点。 灵敏度和类别值用冒号（:)分隔。

以下是灵敏度/类别对的示例：

```
user_u:object_r:etc_t:s0:c0.c2  
```

这里只有一个灵敏度级别，这是s0。 类别级别也可以写为c0-c2。

那么，您在哪里分配您的类别级别？ 让我们来看看从细节`/etc/selinux/targeted/setrans.conf`文件：

```
cat /etc/selinux/targeted/setrans.conf
```

```
#
# Multi-Category Security translation table for SELinux
#
#
# Objects can be categorized with 0-1023 categories defined by the admin.
# Objects can be in more than one category at a time.
# Categories are stored in the system as c0-c1023.  Users can use this
# table to translate the categories into a more meaningful output.
# Examples:
# s0:c0=CompanyConfidential
# s0:c1=PatientRecord
# s0:c2=Unclassified
# s0:c3=TopSecret
# s0:c1,c3=CompanyConfidentialRedHat
s0=SystemLow
s0-s0:c0.c1023=SystemLow-SystemHigh
s0:c0.c1023=SystemHigh
```

我们不会在这里讨论敏感性和类别的细节。 只知道被允许的处理读访问资源，只有当它的灵敏度和类别水平比资源（即过程域_支配_着资源类型）的高。 当其敏感度/类别级别小于资源的敏感度/类别级别时，该进程可以写入资源。

### 结论

我们试图在这个三部分系列的短期内涵盖关于Linux安全的一个广泛的话题。 如果我们现在看看我们的系统，我们有一个简单的Apache Web服务器，其内容从自定义目录提供。 我们还有一个FTP守护进程在我们的服务器上运行。 已创建几个用户的访问受到限制的用户。 在我们一起工作时，我们使用SELinux软件包，文件和命令来满足我们的安全需求。 一路上，我们还学习了如何查看SELinux错误消息并了解它们。

整个书已经写在SELinux主题上，你可以花费几个小时试图找出不同的包，配置文件，命令及其对安全性的影响。 那么你从这里去哪里？

我要做的一件事是小心你不要在生产系统上测试任何东西。 一旦掌握了基础知识，就可以通过在生产箱的测试副本上启用它来开始使用SELinux。 确保审计守护程序正在运行，并留意错误消息。 检查任何拒绝服务启动的拒绝。 使用布尔设置。 列出保护系统的可能步骤，例如创建映射到最低权限SELinux帐户的新用户或将正确的上下文应用于非标准文件位置。 了解如何解密错误日志。 检查各种守护程序的端口：如果使用非标准端口，请确保它们已正确分配给策略。

它将与时间和实践一起。 :)