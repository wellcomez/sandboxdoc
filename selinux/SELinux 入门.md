---
date: 2023-04-13 13:48
title: SELinux 入门
tags:
- selinux
---

# SELinux 入门


## 一、前言
https://zhuanlan.zhihu.com/p/30483108

安全增强型 Linux（Security-Enhanced Linux）简称 SELinux，它是一个 Linux 内核模块，也是 Linux 的一个安全子系统。

SELinux 主要由美国国家安全局开发。2.6 及以上版本的 Linux 内核都已经集成了 SELinux 模块。

SELinux 的结构及配置非常复杂，而且有大量概念性的东西，要学精难度较大。很多 Linux 系统管理员嫌麻烦都把 SELinux 关闭了。

如果可以熟练掌握 SELinux 并正确运用，我觉得整个系统基本上可以到达“坚不可摧”的地步了（请永远记住没有绝对的安全）。

掌握 SELinux 的基本概念以及简单的配置方法是每个 Linux 系统管理员的必修课。

本文均在 CentOS 7.4.1708 系统中操作。

本文纯属个人学习经验分享交流，出错再所难免，仅供参考！如果发现错误的地方，可以的话麻烦指点下，特别感谢！

## 二、SELinux 的作用及权限管理机制

### 2.1 SELinux 的作用

SELinux 主要作用就是最大限度地减小系统中服务进程可访问的资源（最小权限原则）。

设想一下，如果一个以 root 身份运行的网络服务存在 0day 漏洞，黑客就可以利用这个漏洞，以 root 的身份在您的服务器上为所欲为了。是不是很可怕？

SELinux 就是来解决这个问题的。

### 2.2 DAC

在没有使用 SELinux 的操作系统中，决定一个资源是否能被访问的因素是：某个资源是否拥有对应用户的权限（读、写、执行）。

只要访问这个资源的进程符合以上的条件就可以被访问。

而最致命问题是，root 用户不受任何管制，系统上任何资源都可以无限制地访问。

这种权限管理机制的主体是用户，也称为自主访问控制（DAC）。

### 2.3 MAC

在使用了 SELinux 的操作系统中，决定一个资源是否能被访问的因素除了上述因素之外，还需要判断每一类进程是否拥有对某一类资源的访问权限。

这样一来，即使进程是以 root 身份运行的，也需要判断这个进程的类型以及允许访问的资源类型才能决定是否允许访问某个资源。进程的活动空间也可以被压缩到最小。

即使是以 root 身份运行的服务进程，一般也只能访问到它所需要的资源。即使程序出了漏洞，影响范围也只有在其允许访问的资源范围内。安全性大大增加。

这种权限管理机制的主体是进程，也称为强制访问控制（MAC）。

而 MAC 又细分为了两种方式，一种叫类别安全（MCS）模式，另一种叫多级安全（MLS）模式。

下文中的操作均为 MCS 模式。

### 2.4 DAC 和 MAC 的对比

这里引用一张图片来说明。

![](https://pic2.zhimg.com/80/v2-5803dd1a71dccea8e7b732447212f9b5_1440w.webp)

￼

可以看到，在 DAC 模式下，只要相应目录有相应用户的权限，就可以被访问。而在 MAC 模式下，还要受进程允许访问目录范围的限制。

## 三、SELinux 基本概念

### 3.1 主体（Subject）

可以完全等同于进程。

> 注：为了方便理解，如无特别说明，以下均把进程视为主体。

### 3.2 对象（Object）

被主体访问的资源。可以是文件、目录、端口、设备等。

> 注：为了方便理解，如无特别说明，以下均把文件或者目录视为对象。

### 3.3 政策和规则（Policy & Rule）

系统中通常有大量的文件和进程，为了节省时间和开销，通常我们只是选择性地对某些进程进行管制。

<mark style="background: #FF5582A6;">而哪些进程需要管制、要怎么管制是由政策决定的。</mark>

一套政策里面有多个规则。<mark style="background: #ABF7F7A6;">部分规则可以按照需求启用或禁用（以下把该类型的规则称为布尔型规则）。</mark>

规则是模块化、可扩展的。在安装新的应用程序时，应用程序可通过添加新的模块来添加规则。用户也可以手动地增减规则。

<mark style="background: #ADCCFFA6;">在 CentOS 7 系统中，有三套政策，分别是：</mark>

1.  `targeted`：对大部分网络服务进程进行管制。这是系统默认使用的政策（下文均使用此政策）。
2.  `minimum`：以 `targeted` 为基础，仅对选定的网络服务进程进行管制。一般不用。
3.  `mls`：<mark style="background: #ABF7F7A6;">多级安全保护。对所有的进程进行管制。这是最严格的政策，配置难度非常大。一般不用，除非对安全性有极高的要求。</mark>

<mark style="background: #FFF3A3A6;">政策可以在 `/etc/selinux/config` 中设定。</mark>

### 3.4 安全上下文（Security Context）

安全上下文是 SELinux 的核心。

<mark style="background: #ABF7F7A6;">安全上下文我自己把它分为「进程安全上下文」和「文件安全上下文」。</mark>

一个「进程安全上下文」一般对应多个「文件安全上下文」。

<mark style="background: #ABF7F7A6;">只有两者的安全上下文对应上了，进程才能访问文件。它们的对应关系由政策中的规则决定。</mark>

<mark style="background: #D2B3FFA6;">文件安全上下文由文件创建的位置和创建文件的进程所决定</mark>。而且<mark style="background: #FF5582A6;">系统有一套默认值</mark>，用户也可以对默认值进行设定。

需要注意的是，<mark style="background: #FFF3A3A6;">单纯的移动文件操作并不会改变文件的安全上下文</mark>。

安全上下文的结构及含义

<mark style="background: #ABF7F7A6;">安全上下文有四个字段，分别用冒号隔开。形如：`system_u:object_r:admin_home_t:s0`。</mark>

![](https://pic1.zhimg.com/80/v2-bb095bfd7450ecbcc799323ef62f66cc_1440w.webp)

### 3.5 SELinux 的工作模式

<mark style="background: #BBFABBA6;">SELinux 有三种工作模式，分别是：</mark>

1.  `enforcing`：强制模式。违反 SELinux 规则的行为将被阻止并记录到日志中。
2.  `permissive`：宽容模式。违反 SELinux 规则的行为只会记录到日志中。一般为调试用。
3.  `disabled`：关闭 SELinux。

<mark style="background: #3BFF9BD9;">SELinux 工作模式可以在</mark> `/etc/selinux/config` 中设定。

<mark style="background: orange;">如果想从 `disabled` 切换到 `enforcing` 或者 `permissive` 的话，需要重启系统。反过来也一样。

`enforcing` 和 `permissive` 模式可以通过 `setenforce 1|0` 命令快速切换</mark>。

<mark style="background: #ABF7F7A6;">需要注意的是，如果系统已经在关闭 SELinux 的状态下运行了一段时间，在打开 SELinux 之后的第一次重启速度可能会比较慢。因为系统必须为磁盘中的文件创建安全上下文（我表示我重启了大约 10 分钟，还以为是死机了……）。</mark>

SELinux 日志的记录需要借助 `auditd.service` 这个服务，请不要禁用它。

### 3.6 SELinux 工作流程

这里引用一张图片，不必过多解释。
![](https://pic2.zhimg.com/80/v2-117fd49ecd657af89d7b614351d046dd_1440w.webp)
> 注：上面的安全文本指的就是安全上下文。

## 四、SELinux 基本操作

### 4.1 查询文件或目录的安全上下文

命令基本用法

```c
ls -Z <文件或目录>
```

用法举例

查询 `/etc/hosts` 的安全上下文。

```c
ls -Z /etc/hosts
```

执行结果

```c
-rw-r--r--. root root system_u:object_r:net_conf_t:s0  /etc/hosts
```

### 4.2 查询进程的安全上下文

命令基本用法

```c
ps auxZ | grep -v grep | grep <进程名>
```

用法举例

查询 Nginx 相关进程的安全上下文。

```c
ps auxZ | grep -v grep | grep nginx
```

执行结果

```c
system_u:system_r:httpd_t:s0    root       7997  0.0  0.0 122784  2156 ?        Ss   14:31   0:00 nginx: master process /usr/sbin/nginx
system_u:system_r:httpd_t:s0    nginx      7998  0.0  0.0 125332  7560 ?        S    14:31   0:00 nginx: worker process
```

### 4.3 手动修改文件或目录的安全上下文

命令基本用法

```c
chcon <选项> <文件或目录 1> [<文件或目录 2>...]
```

![[Pasted image 20221202124713.png]]
用法举例

修改 `test` 的安全上下文为 `aaa_u:bbb_r:ccc_t:s0`。

```c
chcon -u aaa_u -r bbb_r -t ccc_t test
```

### 4.4 把文件或目录的安全上下文恢复到默认值

命令基本用法

```c
restorecon [选项] <文件或目录 1> [<文件或目录 2>...]
```

![](https://pic2.zhimg.com/80/v2-a479da951cd960c8c0beefe819db4181_1440w.webp)
用法举例

添加一些网页文件到 Nginx 服务器的目录之后，为这些新文件设置正确的安全上下文。

```c
restorecon -R /usr/share/nginx/html/
```

### 4.5 查询系统中的布尔型规则及其状态

命令基本用法

```c
getsebool -a
```

由于该命令要么查询所有规则，要么只查询一个规则，所以一般都是先查询所有规则然后用 `grep` 筛选。

用法举例

查询与 `httpd` 有关的布尔型规则。

```c
getsebool -a | grep httpd
```

执行结果

```c
httpd_anon_write --> off
httpd_builtin_scripting --> on
httpd_can_check_spam --> off
httpd_can_connect_ftp --> off
#以下省略
```

### 4.6 开关一个布尔型规则

命令基本用法

```c
setsebool [选项] <规则名称> <on|off>
```

![](https://pic3.zhimg.com/80/v2-c57d16eb99e1ca0f8e83a8b50b4e54da_720w.webp)
用法举例

开启 `httpd_anon_write` 规则。

```c
setsebool -P httpd_anon_write on
```

### 4.7 添加目录的默认安全上下文

命令基本用法

（如果提示找不到命令的话请安装 `policycoreutils-python` 软件包，下同。）

```c
semanage fcontext -a -t <文件安全上下文中的类型字段> "<目录（后面不加斜杠）>(/.*)?"
```

> 注：目录或文件的默认安全上下文可以通过 `semanage fcontext -l` 命令配合 `grep` 过滤查看。

用法举例

为 Nginx 新增一个网站目录 `/usr/share/nginx/html2` 之后，需要为其设置与原目录相同的默认安全上下文。

```c
semanage fcontext -a -t httpd_sys_content_t "/usr/share/nginx/html2(/.*)?"
```

### 4.8 添加某类进程允许访问的端口

命令基本用法

```c
semanage port -a -t <服务类型> -p <协议> <端口号>
```

> 注：各种服务类型所允许的端口号可以通过 `semanage port -l` 命令配合 `grep` 过滤查看。

用法举例

为 Nginx 需要使用 `10080` 的端口用于 HTTP 服务。

```c
semanage port -a -t http_port_t -p tcp 10080
```

## 五、SELinux 错误分析和解决

### 5.1 认识 SELinux 日志

当开启了 SELinux 之后，很多服务的一些正常行为都会被视为违规行为（标题及下文中的错误均指违规行为）。

这时候我们就需要借助 SELinux 违规日志来分析解决。

SELinux 违规日志保存在 `/var/log/audit/audit.log` 中。

`/var/log/audit/audit.log` 的内容大概是这样的。

```c
type=LOGIN msg=audit(1507898701.391:515): pid=8523 uid=0 subj=system_u:system_r:crond_t:s0-s0:c0.c1023 old-auid=4294967295 auid=0 tty=(none) old-ses=4294967295 ses=25 res=1
type=USER_START msg=audit(1507898701.421:516): pid=8523 uid=0 auid=0 ses=25 subj=system_u:system_r:crond_t:s0-s0:c0.c1023 msg='op=PAM:session_open grantors=pam_loginuid,pam_keyinit,pam_limits,pam_systemd acct="root" exe="/usr/sbin/crond" hostname=? addr=? terminal=cron res=success'
...
```

该文件的内容很多，而且混有很多与 SELinux 错误无关的系统审计日志。我们要借助 `sealert` 这个实用工具来帮忙分析（如果提示找不到命令的话请安装 `setroubleshoot` 软件包）。

### 5.2 使用 `sealert` 分析错误

命令基本用法

```c
sealert -a /var/log/audit/audit.log
```

执行完命令之后，系统需要花一段时间去分析日志中的违规行为并给出分析报告。分析报告的结构讲解请看下图：

![](https://pic4.zhimg.com/80/v2-c4f4de75b276e647841a860bc6c3aed3_720w.webp)

### 5.3 个人解决 SELinux 错误的思路

当发现一个服务出现错误的时候，请先检查一下 `sealert` 的分析报告里面是否有该服务进程名称的关键字。如果没有，说明不是 SELinux 造成的错误，请往其他方面排查。

接下来就是阅读 `sealert` 的分析报告了。

首先需要了解的就是违规原因。如果违规原因里面出现了该服务不该访问的文件或资源，那就要小心了。有可能是服务的配置有问题或者服务本身存在漏洞，请优先排查服务的配置文件。

在分析报告中，对于一个违规行为，通常会有 2~3 个解决方案。请优先选择修改布尔值、设置默认安全上下文之类操作简单、容易理解的解决方案。

如果没有这种操作简单、容易理解的解决方案，请先去搜索引擎搜索一下有没有其他更好的解决方案。

需要注意的是，可信度只是一个参考值，并不代表使用可信度最高的解决方案就一定可以解决问题。我个人感觉使用可信度越高的解决方案对系统的改动就越小。

不过请记住，在执行解决方案之前，一定要先搞清楚解决方案中的命令是干什么的！

最后，请谨慎使用 `audit2allow` 这个命令。这个命令的作用非常简单粗暴，就是强制允许所遇到的错误然后封装成一个 SELinux 模块，接着让 SELinux 加载这个模块来达到消除错误的目的。不是万不得已建议不要随便使用 `audit2allow`。

## 六、参考文献

1.  [NSA —— SELinux 官方介绍](https://link.zhihu.com/?target=https%3A//www.nsa.gov/what-we-do/research/selinux/)
2.  [Linux 中国 —— SELinux 入门](https://link.zhihu.com/?target=https%3A//linux.cn/article-1144-1.html)
3.  [Linux 中国 —— 从蓝瘦“想哭”到 SELinux 看操作系统安全何在](https://link.zhihu.com/?target=https%3A//linux.cn/article-8534-1.html)
4.  [鸟哥的Linux私房菜：基础学习篇 第四版 —— SELinux 初探](https://link.zhihu.com/?target=https%3A//wizardforcel.gitbooks.io/vbird-linux-basic-4e/content/143.html)
5.  [Howtoing —— 简介SELinux的在CentOS 7 - 第1部分：基本概念](https://link.zhihu.com/?target=https%3A//www.howtoing.com/an-introduction-to-selinux-on-centos-7-part-1-basic-concepts/)
6.  [Howtoing —— 简介SELinux的在CentOS 7 - 第2部分：文件和进程](https://link.zhihu.com/?target=https%3A//www.howtoing.com/an-introduction-to-selinux-on-centos-7-part-2-files-and-processes/)

