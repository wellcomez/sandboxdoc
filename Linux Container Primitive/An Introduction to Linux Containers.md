---
title: "An Introduction to Linux Containers"
layout: post
---
 [www.schutzwerk.com /en/43/posts/linux\_container\_intro/](https://www.schutzwerk.com/en/43/posts/linux_container_intro/)

# An Introduction to Linux Containers

SCHUTZWERK GmbH 4-5 minutes

With the steadily grow­ing spread of con­tainer­iza­tion now and in the fu­ture, it be­comes in­creas­ingly nec­es­sary to prop­erly un­der­stand the in­ter­nals and po­ten­tial se­cu­rity threats re­sult­ing from as­pects like ker­nel vul­ner­a­bil­i­ties, con­tainer mis­con­fig­u­ra­tions and wrong use. This also in­cludes op­ti­miz­ing the process of de­ploy­ing and dis­trib­ut­ing con­tain­ers and their en­vi­ron­ments to in­crease the pro­duc­tiv­ity and ef­fi­ciency, which di­rectly im­pacts the cost fac­tor. There are many ways con­tainer­iza­tion can be im­ple­mented - this blog post se­ries fo­cusses on Linux name­spaces and con­trol groups. These fea­tures are cur­rently being used by LXC and Docker.

A con­tainer is a set of processes that’s iso­lated from the host en­vi­ron­ment, processes, file hi­er­ar­chy and net­work stack. Often con­tain­ers are being cre­ated using _im­ages_. These are min­i­mal root filesys­tems that in­clude the re­quired bi­na­ries, de­pen­den­cies and con­fig­u­ra­tion files for the con­tainer to run. There’s no ad­di­tional ab­strac­tion layer be­tween the ker­nel and the ap­pli­ca­tion and there are no de­vices being vir­tu­al­ized. In­stead, the ker­nel of the host sys­tem is being _shared_ with the iso­lated processes. Iso­la­tion is being im­ple­mented using prim­i­tives avail­able in the Linux ker­nel.

Be­cause of these as­pects con­tain­ers are ef­fi­cient:

*   **Stor­age**: To build con­tainer im­ages, often min­i­mal base im­ages of Linux dis­tri­b­u­tions are being used. For ex­am­ple, an Ubuntu base image is 188 megabytes in size. That’s only a small frac­tion of the size of a Ubuntu vir­tual ma­chine. More­over, there are even smaller base im­ages like Alpine Linux with five megabytes and _Busy­Box_ with only two megabytes in size. On top of that, base im­ages can be reused for mul­ti­ple im­ages. If for in­stance mul­ti­ple im­ages are mak­ing use of the same base image, the base image only has to be stored once if the con­tainer en­gines sup­ports lay­ered im­ages.
    
*   **Per­for­mance**: Be­cause of the shared ker­nel, there’s min­i­mal ad­di­tional cost when run­ning con­tain­ers com­pared to the ex­e­cu­tion with­out any con­tainer­iza­tion. Con­tain­ers are able to be added and re­moved in sec­onds, mak­ing them a handy tool for agile ap­pli­ca­tion de­ploy­ment.
    
*   **Com­plex­ity**: Every prim­i­tive that’s being used to iso­late processes is al­ready in­cluded in the op­er­at­ing sys­tem ker­nel. With­out the re­quire­ment of an ad­di­tional hy­per­vi­sor layer, the op­er­at­ing sys­tem is aware of all parts of the con­tainer­iza­tion process and can han­dle the ex­e­cu­tion na­tively. For ex­am­ple, mem­ory man­age­ment for con­tain­ers can be as sim­ple as man­ag­ing the mem­ory of a sin­gle na­tive process.
    

The Linux Con­tainer blog se­ries takes the ker­nel prim­i­tives re­spon­si­ble for con­tainer­iza­tion into ac­count. By using in­for­ma­tion on these mech­a­nisms in com­bi­na­tion with de­tails to the cor­re­spond­ing ker­nel code, the func­tion­al­ity in re­gard to these mech­a­nisms is being doc­u­mented and il­lus­trated. This ex­plains how the op­er­a­tion of con­tain­ers is pos­si­ble and de­scribes the in­ter­nal processes of cre­at­ing them. As the ker­nel source code is con­stantly sub­ject to changes, all pro­vided in­for­ma­tion is to be un­der­stood in re­gard to Linux ver­sion `4.19-rc3` (com­mit `11da3a7f84f1`).

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [Linux Ca­pa­bil­i­ties](https://www.schutzwerk.com/en/43/posts/linux_container_capabilities/)

## Ref­er­ences

*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)
