---
title: "Linux Container Primitives cgroup Kernel View and Usage in Containerization"
layout: post
---
  
 [www.schutzwerk.com /en/43/posts/control\_04\_groups\_kernel/](https://www.schutzwerk.com/en/43/posts/control_04_groups_kernel/)

# Linux Container Primitives: cgroup Kernel View and Usage in Containerization

SCHUTZWERK GmbH 7-8 minutes


## cgroup Ker­nel View

In the ker­nel source code, con­trol groups are rep­re­sented by the `cgroup` struc­ture de­fined in `linux/cgroup-defs.h`. Every `cgroup` in­cludes a unique ID, start­ing from the value `1`, al­ways using the small­est value pos­si­ble. When ap­ply­ing changes to the con­trol group hi­er­ar­chy, checks have to be per­formed on a reg­u­lar basis to de­ter­mine whether a group is a de­scen­dant of an­other group. To avoid the re­quire­ment to tra­verse in the con­trol group tree, an in­te­ger value `level` is pre­sent to solve this prob­lem using nu­mer­i­cal com­par­isons.

The logic to ini­tial­ize a con­trol group is im­ple­mented in `cgroup_init` (`kernel/cgroup/cgroup.c`):

```
[...]
BUG_ON(cgroup_setup_root(&cgrp_dfl_root, 0, 0));
[...]
for_each_subsys(ss, ssid) {
    [...]
    cgroup_init_subsys(ss, false);
    [...]
    css_populate_dir(init_css_set.subsys[ssid]);
    [...]
}
[...]
WARN_ON(sysfs_create_mount_point(fs_kobj, "cgroup"));
WARN_ON(register_filesystem(&cgroup_fs_type));
WARN_ON(register_filesystem(&cgroup2_fs_type));
WARN_ON(!proc_create_single("cgroups", 0, NULL,
    proc_cgroupstats_show));
[...]
```

The func­tion `cgroup_setup_root` ini­tially sets up the con­trol group root which is rep­re­sented by a `cgroup_root` struc­ture. In­ter­nally this in­cludes the `cgroup` struc­ture dis­cussed above. The setup rou­tine is also re­spon­si­ble for cre­at­ing the `kernfs` - the vir­tual filesys­tem ex­port­ing the files re­sid­ing in `/sys/fs/cgroup`. After that, all con­trol group sub­sys­tems are being en­abled. With `init_and_link_css` called in `cgroup_init_subsys`, point­ers to the re­spec­tive chil­dren, sib­lings and par­ent nodes are cre­ated. The ab­bre­vi­a­tion `css` stands for _cgroup sub­sys­tem state_ in this con­text and is being used to map a spe­cific thread to a set of con­trol groups \[1\]. The func­tion `css_populate_dir` cre­ates a vir­tual filesys­tem for each con­troller in the `kernfs` cre­ated be­fore. Fi­nally, the `kernfs` is mounted in `sysfs_create_mount_point`. For each con­trol group ver­sion a filesys­tem is reg­is­tered and the vir­tual `/proc/cgroups` file is being cre­ated.

There ex­ists a global array of all sub­sys­tems, called `cgroup_subsys` which is de­fined using an `include` di­rec­tive for `cgroup_subsys.h` as can be seen below. This is the file hold­ing all avail­able con­trollers sup­ported by the ker­nel.

```
structure cgroup_subsys *cgroup_subsys[] = {
    #include <linux/cgroup_subsys.h>
};
```

The con­trollers are im­ple­mented using an­other ker­nel struc­ture: `cgroup_subsys`. This struc­ture pro­vides a com­mon in­ter­face for all im­ple­mented re­source con­trollers. An­other com­mon in­ter­face for all sub­sys­tems is the `cftype_ss` struc­ture which en­ables all con­trollers to de­fine own vir­tual files to ex­port data. For ex­am­ple the `cpuset` con­troller ex­ports these files \[2\]:

![Control Group Exports](https://www.schutzwerk.com/en/43/img/ControlGroups/cg_export.jpg)

Sim­i­lar to the im­ple­men­ta­tion of name­spaces in the ker­nel, the `task_struct` struc­ture also holds in­for­ma­tion re­gard­ing con­trol groups \[2\]:

![Control Group Structures](https://www.schutzwerk.com/en/43/img/ControlGroups/cg_struct.jpg)

As can be ob­served in the fig­ure above, the `css_set` struc­ture as­so­ci­ates a set of con­trol group sub­sys­tems to a task. Every task with the same _cgroup_ sub­sys­tem set has a pointer to the same `css_set`. This is being used to save space in the task struc­ture which ef­fec­tively speeds up `fork` calls.

In­ter­nally there’s a `MxN` re­la­tion­ship be­tween `cgroup`s and `css_set`s. To link the ker­nel struc­tures ef­fi­ciently, the fol­low­ing link struc­ture is in place:

![Control Group Structure Links](https://www.schutzwerk.com/en/43/img/ControlGroups/cg_struct_links.jpg)

For each process, there ex­ists ex­actly one _leader_ thread whose thread ID is equal to the PID of the whole process. It’s pos­si­ble that a `css_set` is being linked to mul­ti­ple con­trol groups be­cause every sin­gle task can be pre­sent in var­i­ous _cgroups_. For this rea­son and to be able to tra­verse the link struc­ture the other way around, be­gin­ning from a `cgroup`, the link­ing struc­ture `cgrp_cset_link` as­so­ci­ates both ker­nel struc­tures. The la­bels of the ar­rows are to be in­ter­preted as UML as­so­ci­a­tions. As shown above, there also exist mul­ti­ple short­cuts in the struc­ture link­age to allow an ef­fi­cient di­rect ac­cess to as­so­ci­ated struc­tures with­out hav­ing to tra­verse mul­ti­ple lists. For ex­am­ple the link from `css_set` to `cgroup` by­passes the link­ing struc­ture in be­tween.

## Usage in Con­tainer­iza­tion

Con­tainer en­gines like LXC and Docker sup­port the con­fig­u­ra­tion on con­trol group set­tings. Sim­i­lar to the con­fig­u­ra­tion of name­spaces, it’s pos­si­ble to sup­ply com­mand line pa­ra­me­ters to the Docker CLI while con­fig­u­ra­tion files are being used for LXC.

When start­ing a Docker con­tainer with­out any ad­di­tional con­trol group con­fig­u­ra­tion, a `docker` group is cre­ated for each con­troller type men­tioned in this chap­ter using per­mis­sive de­fault val­ues. Ad­di­tional con­fig­u­ra­tion can then be ap­plied after start­ing a con­tainer using the mech­a­nisms that have al­ready been dis­cussed. Fur­ther­more the com­mand line op­tions allow _cgroup_ con­fig­u­ra­tion with­out the re­quire­ment of in­ter­act­ing with the vir­tual con­trol group filesys­tem. For ex­am­ple, the amount of CPUs a con­tainer may use is being con­fig­ured by pass­ing a nu­mer­i­cal value along with the `--cpus` op­tion. An­other con­ve­nient fea­ture is the abil­ity to in­te­grate a con­tainer into an ex­ist­ing par­ent con­trol group. There­fore con­trol groups are able to be pre­pared in order to use it as par­ent group for a con­tainer later on. With the abil­ity to use per­sis­tent _cgroups_, con­tain­ers can be re­stricted in an au­to­mated way upon boot­ing a ma­chine by as­sign­ing a par­ent con­trol group.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Con­trol groups, part 6: A look under the hood](https://lwn.net/Articles/606925/)
*   [2 - Linux Con­tain­ers: Basic Con­cepts](https://www.cl.cam.ac.uk/~lc525/files/Linux_Containers.pdf)
*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)
