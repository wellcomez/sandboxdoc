---
title: "Linux Container Primitives Memory, CPU, Freezer and Device Control Groups"
layout: post
---
 [www.schutzwerk.com /en/43/posts/control\_groups\_03\_memory\_cpu\_freezer\_dev/](https://www.schutzwerk.com/en/43/posts/control_groups_03_memory_cpu_freezer_dev/)

# Linux Container Primitives: Memory, CPU, Freezer and Device Control Groups

SCHUTZWERK GmbH 7-8 minutes

* * *

After dis­cussing the [Net­work and Block I/O con­trollers](https://www.schutzwerk.com/en/43/posts/control_groups_02_network_block_io/), this post con­sid­ers the Mem­ory, CPU, Freezer and De­vice con­trollers. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.


* * *

## The Mem­ory Con­troller (v2)

Processes can be ac­counted and lim­ited re­gard­ing their mem­ory usage. The fol­low­ing types of mem­ory us­ages are cur­rently being tracked \[1\]:

*   Con­sumed mem­ory in user-space
*   Mem­ory usage in ker­nel-space, as in ker­nel data struc­tures
*   TCP socket buffers

The mem­ory con­sumed by a con­trol group can be read using the `memory.current` file. One of the most com­mon ways to limit mem­ory usage in­cludes set­ting the `memory.max` value to de­fine an upper mem­ory limit for all processes re­sid­ing in a con­trol group.

A _cgroup_ is charged for its mem­ory usage when al­lo­cat­ing mem­ory. In turn, this ac­count­ing gets re­moved once `free()` or sim­i­lar mech­a­nisms to free pre­vi­ously al­lo­cated mem­ory are being used. When mov­ing a process that has al­lo­cated mem­ory in the name of a con­trol group to an­other group, the _orig­i­nal_ group is being charged for the al­lo­cated mem­ory since these map­pings are not being trans­ferred.

## The CPU Con­troller (v1/v2)

Var­i­ous con­trollers exist for ver­sion 1 and 2 of the con­trol group im­ple­men­ta­tion to man­age CPU uti­liza­tion. There are three con­trollers for `v1`:

With `cpu` a con­trol group is sup­plied with a guar­an­teed min­i­mum time of CPU uti­liza­tion. The `cpuset` con­troller al­lows spec­i­fy­ing a set of proces­sors a process is al­lowed to be ex­e­cuted on. For the cur­rent process this can be ex­am­ined with `cat /proc/$/status | grep Cpus`. Changes to this set­ting prop­a­gate to all de­scen­dants in the con­trol group hi­er­ar­chy. Ac­count­ing is per­formed with `cpuacct`, for ex­am­ple the file `cpuacct.usage` gives in­for­ma­tion on the con­sumed CPU time of all processes in a con­trol group in nanosec­onds.

Con­trol group ver­sion 2 al­lows weight and ab­solute CPU lim­it­ing mod­els with the `cpu` sub­sys­tem. In con­trast to `v1`, the newer con­trol group ver­sion does _not_ sup­port real-time processes. There­fore, all real-time processes have to be moved to the root con­trol group first be­fore ac­ti­vat­ing the `cpu v2` sub­sys­tem in the _cgroup_ tree.

## The Freezer Con­troller (v1)

This con­trol group does not limit or ac­count re­source usage - it rather al­lows _freez­ing_ a process. Freez­ing a process ul­ti­mately stops the ex­e­cu­tion and sus­pends it. This al­lows an­a­lyz­ing the cur­rent state of a process with the abil­ity to un­freeze it af­ter­wards and con­tinue the ex­e­cu­tion with­out side ef­fects. By cre­at­ing a check­point with the `freezer` sub­sys­tem it’s also pos­si­ble to move an en­tire run­ning process, in­clud­ing its chil­dren, to an­other ma­chine or restart a process from a spe­cific state \[2\].

For this, the vir­tual file `freezer.state` ex­ists that can re­ceive ei­ther `FROZEN` or `THAWED` as input val­ues to freeze and un­freeze a process. This works by walk­ing down the con­trol group hi­er­ar­chy and mark­ing all de­scen­dants of a process with the de­sired state. Ad­di­tion­ally, all processes man­aged by the af­fected groups have to be moved in or out of the `freezer` group, de­pend­ing on the de­sired freez­ing state. Freez­ing it­self is done by send­ing a sig­nal to the af­fected processes. Also, the `freezer` has to fol­low all child process of the af­fected processes that may re­sult from call­ing `fork` and freeze these as well to pre­vent freeze es­capes \[3\].

## The De­vices Con­troller (v1)

This con­troller type al­lows to im­ple­ment ac­cess con­trols for de­vices. One can use whitelist and black­list ap­proaches to only block or allow very spe­cific ac­cesses by defin­ing ex­cep­tions. Child con­trol groups are forced to have the exact same or a sub­set of the ex­cep­tion list of the par­ent. This re­sults in faster checks whether a rule can be added to the ex­cep­tion list be­cause only the list of the child has to checked and _not_ the whole group tree. This con­troller is one of the few that makes use of the hi­er­ar­chi­cal or­ga­ni­za­tion in order to pass con­fig­u­ra­tion in­for­ma­tion to its child groups.

For the the fol­low­ing ex­am­ple, the `devices` con­troller will be used to re­strict a process from ac­cess­ing `/dev/null`.

To limit the usage of de­vices, their major and minor num­bers have to be used. These num­bers are the re­spec­tive iden­ti­fiers of a de­vice in the filesys­tem tree. The major num­ber de­scribes the dri­ver that’s re­quired and is used by the ker­nel in order to ac­cess a spe­cific de­vice. The minor num­ber is used by the de­vice dri­ver to dis­tin­guish log­i­cal and phys­i­cal de­vices re­sult­ing from the ex­is­tence of a cer­tain de­vice. In the above ex­am­ple for `/dev/null` these num­bers can be iden­ti­fied using `stat -c "major: %t minor: %T" /dev/null` which yields the val­ues `1` for major and `3` for minor.

First, a de­vice con­trol group has to be cre­ated with `cgcreate -g devices:nodevnull` with the iden­ti­fier of the con­trol group being `nodevnull`. To add the cur­rent shell process to this group, the com­mand `cgclassify -g devices:nodevnull $` will be in­voked. The process iden­ti­fier of the cur­rent shell process is `$`. To fi­nally deny ac­cess­ing the `/dev/null` de­vice, this com­mand will be ex­e­cuted: `cgset -r devices.deny="c 1:3 rwm" nodevnull`. The for­mat of the pa­ra­me­ter for `devices.deny` is as fol­lows:

![](https://www.schutzwerk.com/en/43/img/ControlGroups/major-minor.jpg)

The de­vice type is de­ter­mined by using the first char­ac­ter of the out­put of `ls -la /dev/null`, which shows that it’s a char­ac­ter de­vice.

Now ac­cess­ing the spe­cific de­vice is being blocked, even for processes run­ning as `root` user:

```
root@box:~# echo "a" > /dev/zero # Allowed
root@box:~# echo "a" > /dev/null # Denied
bash: /dev/null: Operation not permitted
```

  

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [“cgroup Ker­nel View and Usage in Con­tainer­iza­tion”](https://www.schutzwerk.com/en/43/posts/control_04_groups_kernel/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Con­trol Group v2](https://www.kernel.org/doc/Documentation/cgroup-v2.txt)
*   [2 - freezer-sub­sys­tem](https://www.kernel.org/doc/Documentation/cgroup-v1/freezer-subsystem.txt)
*   [3 - Con­trol groups, part 3: First steps to con­trol](https://lwn.net/Articles/605039/)
*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)
