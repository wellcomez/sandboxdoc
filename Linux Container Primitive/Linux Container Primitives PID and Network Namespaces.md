---
title: "Linux Container Primitives PID and Network Namespaces"
layout: post
---
 [www.schutzwerk.com /en/43/posts/namespaces\_03\_pid\_net/](https://www.schutzwerk.com/en/43/posts/namespaces_03_pid_net/)

# Linux Container Primitives: PID and Network Namespaces

SCHUTZWERK GmbH 10-13 minutes

* * *

After dis­cussing the mount name­space and an in­for­ma­tion leak issue in Docker in the [pre­vi­ous blog post](https://www.schutzwerk.com/en/43/posts/namespaces_02_mnt/), this part il­lus­trates the PID and net­work name­space types. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.


* * *

## PID Name­spaces

By cre­at­ing a PID name­space, the process ID num­ber space gets iso­lated. This makes it pos­si­ble that processes in a con­tainer have PIDs start­ing from the value `1` whereas the _real_ PID out­side of the name­space of the same process is an en­tirely dif­fer­ent num­ber. There­fore mul­ti­ple processes can have the same PID value on a sys­tem while they re­side in dif­fer­ent PID name­spaces. With this name­space type processes of a con­tainer can be sus­pended and re­sumed, mak­ing the PIDs of the processes unique and per­sis­tent within such a name­space. Fur­ther­more, con­tain­ers can have their own `init` processes with a PID value of `1` that pre­pare the con­tainer in­ter­nals and reap child processes them­selves rather than del­e­gat­ing this task to the sys­tem wide `init` process.

Join­ing name­spaces of this type lim­its process com­mu­ni­ca­tion: It’s not pos­si­ble to in­ter­act with processes of other PID name­spaces in a way that re­quires a process iden­ti­fier when is­su­ing sys­tem calls. For ex­am­ple, it’s im­por­tant to note that processes can only send sig­nals to other processes that re­side in their cur­rent name­space or in name­spaces de­scend­ing from their cur­rent name­space. This iso­la­tion is one of the main use-cases for this name­space type in con­tainer­iza­tion.

PID name­spaces allow nest­ing up to a depth of 32, re­sult­ing in one PID in the orig­i­nal name­space and one in the new name­space for each process. This cre­ates one PID value in each name­space that’s a di­rect an­ces­tor walk­ing back to the root name­space. Chang­ing PID name­spaces is a one-way op­er­a­tion. This means that it’s not pos­si­ble to move a process up to an an­ces­tor name­space.

Mov­ing a process to its own PID name­space does _not_ iso­late the list of processes vis­i­ble for that process. This can be ver­i­fied by cre­at­ing a shell in its own PID name­space and ex­am­in­ing the process list:

```
user@box:~$ sudo unshare -fp /bin/bash
root@box:~# ps aux
USER  PID [...] COMMAND
[...]
user  5380 [...] /usr/lib/[...]/chromium-browser --enable-pinch
user  5388 [...] /usr/lib/[...]/chromium-browser --type=zygote
[...]
```

As can be seen in the list­ing above, processes re­sid­ing in other name­spaces are still vis­i­ble. The process list is de­ter­mined by pro­cess­ing the `/proc/<PID>` fold­ers to gather the dis­played in­for­ma­tion and this is not being iso­lated by join­ing a new PID name­space. By ap­pend­ing the op­tion `--mount-proc` to the com­mand used above, this issue is being re­solved by cre­at­ing a new mount name­space and re­mount­ing the `/proc` di­rec­tory using `mount -t proc proc /proc`. This ef­fec­tively iso­lates the di­rec­tory con­tents of `/proc` and pre­vents a con­tainer from ac­cess­ing the in­for­ma­tion stored in the `/proc` di­rec­tory for processes re­sid­ing in other name­spaces. Em­ploy­ing this re­mount­ing mech­a­nism is a part of the de­fault be­hav­ior of Docker and other con­tainer en­gines. There­fore, leak­ing in­for­ma­tion of host process and ac­cess­ing parts of these processes by shar­ing `/proc` is pre­vented.

A sim­ple C usage ex­am­ple for this name­space type can be ex­am­ined below:

```
[...]
int run(void *) {
    std::cout << "[Child] PID: " << getpid() << std::endl;
    std::cout << "[Child] Parent PID: " << getppid() << std::endl;
    system("/bin/sh");
    return 0;
}

int main(int argc, char const *argv[]) {
    [...]
    childPid = clone(run, childStackTop, CLONE_VFORK | CLONE_NEWPID,
        0);
    std::cout << "[Parent] Child PID: " << childPid << std::endl;
    [...]
    return 0;
}
```

After com­pil­ing and ex­e­cut­ing this codes it be­comes clear that the child is run­ning with PID `1` in the iso­lated en­vi­ron­ment and a dif­fer­ent, much higher iden­ti­fier out­side of the name­space.

A process in a new PID name­space can be cre­ated by pro­vid­ing the `CLONE_NEWPID` flag. When using the same flag in com­bi­na­tion with `unshare` or when call­ing `setns`, a new PID name­space will be cre­ated. This name­space is pre­sent in the `/proc/<PID>/ns/pid_for_children` file. The process call­ing `unshare` or `setns` will _stay_ in its cur­rent name­space. In­stead, the first child of this process will be placed in the new name­space with a PID value of `1`, mak­ing it the con­tainer’s `init` process. This re­sults from the as­sump­tion of many li­braries and ap­pli­ca­tions: PIDs of processes are not sub­ject to change. By join­ing a new name­space at run­time, the iden­ti­fier of a process would change. In fact, even C li­brary func­tions like `getpid` cache the PIDs of processes \[1\].

Ever since PID name­spaces have been im­ple­mented, PIDs are rep­re­sented by a ker­nel struc­ture called `pid`. In com­bi­na­tion with the `upid` struc­ture (`include/linux/pid.h`) it’s pos­si­ble to de­ter­mine the cor­rect `pid` struc­ture as seen in a spe­cific PID name­space.

## Net­work Name­spaces

This name­space can en­able processes to have their own pri­vate net­work stack, in­clud­ing in­ter­faces, rout­ing ta­bles and sock­ets \[2\]. The cor­re­spond­ing `clone` flag is `CLONE_NEWNET` and the `ip` CLI ap­pli­ca­tion is avail­able to man­age net­work name­spaces eas­ily. The `ip netns` com­mand is able to cre­ate a new per­ma­nent net­work name­space. This is being ac­com­plished by bind-mount­ing `/proc/self/ns/net` to `/var/run/netns/<Name of namespace>}`. Using this, con­fig­u­ra­tion can take place with­out mov­ing processes to the name­space first.

To cre­ate a net­work name­space from the com­mand line, the `ip netns add` is used as fol­lows:

```
root@box:~# ip netns add one # Create a network namespace
root@box:~# strace ip netns add two # Trace the creation
[...]
unshare(CLONE_NEWNET) = 0
mount("/proc/self/ns/net", "/var/run/netns/two, [...]) = 0
[...]
root@box:~# ip netns exec one ip link # Execute command in NS
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT [...]
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
```

As seen in the out­put listed above, adding a new net­work name­space is ac­com­plished by fol­low­ing these steps:

*   En­ter­ing a new net­work name­space by using `unshare` with the `CLONE_NEWNET` flag
*   Ex­e­cut­ing the mount op­er­a­tion men­tioned above

Delet­ing a net­work name­space can be done anal­o­gous. How­ever, a name­space only gets de­stroyed in case all processes re­sid­ing in the name­space are ter­mi­nated. Oth­er­wise it’s marked for dele­tion.

By de­fault a new net­work name­space comes with its own local in­ter­face which is down by de­fault. To per­form the con­fig­u­ra­tion of a name­space it may be con­ve­nient to spawn a shell in the de­sired net­work name­space with `ip netns exec <name> bash`. Among oth­ers, the fol­low­ing con­fig­u­ra­tion sce­nar­ios are pos­si­ble:

*   Link­ing net­work name­spaces in order to pro­vide a way for dif­fer­ent name­spaces to com­mu­ni­cate by cre­at­ing a net­work bridge.
*   Rout­ing a net­work name­space through the host net­work stack to en­able in­ter­net ac­cess.
*   As­sign­ing a phys­i­cal in­ter­face to a name­space. Please note that each net­work in­ter­face be­longs to ex­actly one name­space at a given point of time. The same is true for sock­ets.

To en­able the com­mu­ni­ca­tion be­tween the de­fault net­work name­space and an­other name­space, vir­tual in­ter­faces can be of use. These in­ter­faces come in pairs of two - In this ex­am­ple `veth0` and `veth1` al­low­ing a pipe-like com­mu­ni­ca­tion:

```
# Create the virtual interface pair
root@box:~# ip link add veth0 type veth peer name veth1
# Move veth1 to the namespace named `one`
root@box:~# ip link set veth1 netns one
# Set the IP for veth1 in the new namespace
root@box:~# ip netns exec one ifconfig veth1 10.1.1.1/24 up
# Set the IP in the default namespace
root@box:~# ifconfig veth0 10.1.1.2/24 up
```

As seen above it’s pos­si­ble to move an in­ter­face to a dif­fer­ent name­space. This can for ex­am­ple be used to en­able the com­mu­ni­ca­tion be­tween con­tain­ers, sim­i­lar to the func­tion­al­ity pro­vided by `docker --link`. There­fore pri­vate con­tainer net­works can be built that are iso­lated from the host and other con­tain­ers on a host. Also, in­ter­net ac­cess for con­tain­ers can be pro­vided this way. While the Docker plat­form does this by de­fault using a bridge net­work, it may be nec­es­sary to con­fig­ure parts of this as­pect man­u­ally for other con­tainer en­gines.

Mov­ing an in­ter­face back to the de­fault net­work name­space is ac­com­plished with the com­mand `ip link set eth0 netns 1`: It de­tects the cor­rect name­space ac­cord­ing to the spec­i­fied PID with value `1`, al­low­ing ad­min­is­tra­tors to iden­tify name­spaces by the process iden­ti­fiers pre­sent in a given name­space. Mov­ing in­ter­faces be­tween name­spaces is im­ple­mented in the `dev_change_net_namespace` func­tion of the Linux ker­nel \[3\]. Be­sides stop­ping and mov­ing the de­sired in­ter­face it also no­ti­fies all processes using the in­ter­face in order to flush the mes­sage chains.

A com­mon use-case for net­work name­spaces is run­ning mul­ti­ple ser­vices bind­ing to the same port on a sin­gle ma­chine. This works by al­lo­cat­ing a dif­fer­ent port in the host’s net­work name­space for each port that’s being ex­posed by a con­tainer net­work name­space. There­fore all con­tainer­ized web ser­vices can be con­fig­ured to run on the de­fault port `80` while they are being ex­posed on an en­tirely dif­fer­ent port. Con­se­quently ap­pli­ca­tions do not re­quire in­for­ma­tion on the real port that’s being used to ex­pose the ser­vice and the host sys­tem can map ports freely. This pro­vides a cer­tain de­gree of porta­bil­ity when de­ploy­ing con­tain­ers that use net­work func­tion­al­ity.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [The User Name­space](https://www.schutzwerk.com/en/43/posts/namespaces_04_user/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Name­spaces in op­er­a­tion, part 4: more on PID name­spaces](https://lwn.net/Articles/532748/)
*   [2 - Name­spaces in op­er­a­tion, part 7: Net­work name­spaces](https://lwn.net/Articles/580893/)
*   [3 - dev.c - Pro­to­col in­de­pen­dent de­vice sup­port rou­tines](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/net/core/dev.c?h=master#n9098)
*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)

