---
title: "Linux Container Primitives An Introduction to Namespaces"
layout: post
---
 [www.schutzwerk.com /en/43/posts/namespaces\_01\_intro/](https://www.schutzwerk.com/en/43/posts/namespaces_01_intro/)

# Linux Container Primitives: An Introduction to Namespaces

SCHUTZWERK GmbH 13-16 minutes



Being in­tro­duced first in Linux ker­nel ver­sion `2.4.19` in 2002, name­spaces de­fine groups of processes that share a com­mon view re­gard­ing spe­cific sys­tem re­sources. This ul­ti­mately iso­lates the view on a sys­tem re­source a group of processes may have, mean­ing that a process can for in­stance have its own host­name while the _real_ host­name of the sys­tem may have an en­tirely dif­fer­ent value.

There exist var­i­ous name­spaces types – as of Linux ker­nel ver­sion `4.19` the fol­low­ing types are avail­able:

*   UTS
*   Mount
*   PID
*   Net­work
*   IPC (Inter Process Com­mu­ni­ca­tion)
*   Con­trol Group
*   User

Con­sider the UTS name­space as an ex­am­ple: Every process in a _sin­gle_ UTS name­space shares the host­name with every other process in the same UTS name­space. Oth­er­wise, the value of the host­name is iso­lated be­tween dif­fer­ent UTS name­spaces. The code list­ing below il­lus­trates this:

```
root@box :~# hostname # Get the hostname
box
root@box :~# unshare -u # Create a sub shell in a new UTS namespace
$ hostname # Get the hostname in the created UTS namespace
box
$ hostname anotherbox # Change the hostname in the UTS namespace
$ hostname # Verify that it has been changed
anotherbox
$ exit # Exit the UTS namespace shell
root@box :~# hostname # Verify that the real hostname of the parent UTS namespace hasn't changed
box
```

Name­spaces can also be used in com­bi­na­tion by mak­ing a process a mem­ber of mul­ti­ple new name­spaces at once. This is use­ful for con­tainer­iza­tion where mul­ti­ple re­sources have to be iso­lated at once.

It’s im­por­tant to note that by de­fault every process is a mem­ber of a name­space of each type listed above. These name­spaces are called `default`, `init` or `root` name­spaces. In case no ad­di­tional name­space con­fig­u­ra­tion is in place, processes and all their di­rect chil­dren will re­side in this exact name­space. This can be ver­i­fied by ex­e­cut­ing `lsns` to list all name­spaces in two dif­fer­ent ter­mi­nals and com­par­ing the name­space iden­ti­fiers which will be equal.

The iso­la­tion pro­vided by name­spaces is highly con­fig­urable and flex­i­ble. For in­stance, it’s pos­si­ble for a data­base ap­pli­ca­tion and a web ap­pli­ca­tion to share the same net­work name­space, al­low­ing both processes to com­mu­ni­cate while other processes that re­side in other net­work name­spaces are not able to do so.

## Sys­tem Calls

Three sys­tem calls are com­monly being used in con­junc­tion with name­spaces:

*   `clone`: Cre­ate child processes
*   `unshare`: This dis­ables shar­ing a name­space with the par­ent process, ef­fec­tively _un­shar­ing_ a name­space and its as­so­ci­ated re­sources. Please note that this sys­tem call changes the shared name­spaces in-place with­out the re­quire­ment of spawn­ing a new process – with the PID name­space being an ex­cep­tion in this case as dis­cussed in one of the next blog posts.
*   `setns`: At­taches a process to an al­ready ex­ist­ing name­space.

Sim­i­lar to the `fork` sys­tem call, `clone` is used to cre­ate child processes. There are mul­ti­ple dif­fer­ences be­tween the two calls: The most sig­nif­i­cant dif­fer­ence is that `clone` can be highly pa­ram­e­trized using flags. For ex­am­ple, it al­lows shar­ing the ex­e­cu­tion con­text, for in­stance the process mem­ory space, with a child process. There­fore `clone` can also cre­ate threads and is more ver­sa­tile than the legacy `fork` call. The `fork` call does not sup­port most of this be­hav­ior. In­stead, `fork` is es­sen­tially being used to cre­ate child processes as copies of a par­ent process. Be­fore going into more de­tail about the dif­fer­ences and sim­i­lar­i­ties it’s first im­por­tant to un­der­stand what’s hap­pen­ing when `fork`, `clone` or a sys­tem call in gen­eral is being in­voked in a C pro­gram.

### fork() != fork

By using one of these two calls in C code the ac­tual code that will be ex­e­cuted is _not_ the sys­tem call it­self as de­fined in the sys­tem call table. The code that will be called in­stead is a wrap­per around the ac­tual sys­tem call of the C li­brary which is often named after the wrapped sys­tem call. These wrap­pers exist be­cause using them is more con­ve­nient for de­vel­op­ers than using sys­tem calls di­rectly. For in­stance, to use a sys­tem call it’s nec­es­sary to setup reg­is­ters, switch to ker­nel mode, han­dle the call re­sults and switch back the user mode \[1\]. This can be sim­pli­fied for by im­ple­ment­ing a wrap­per and doing these tasks in the wrap­per’s code.

When in­spect­ing the `fork` wrap­per func­tion it be­comes clear that the ac­tual `fork` sys­tem call that’s sup­posed to be wrapped is not being used it all. In­stead, the `ARCH_FORK` macro gets called which is an in­line sys­tem call to `clone` de­fined as:

```
#define ARCH_FORK() \
  INLINE_SYSCALL (clone, 4, \
		  CLONE_CHILD_SETTID | CLONE_CHILD_CLEARTID | SIGCHLD, 0, \
		  NULL, &THREAD_SELF->tid)
```

This re­sults in the `clone` and `fork` li­brary func­tions call­ing the same sys­tem call, namely `clone`. This can be ver­i­fied by com­pil­ing a C ap­pli­ca­tion con­tain­ing a call to `fork` and using `strace` to trace the re­sult­ing sys­tem calls when ex­e­cut­ing the re­sult­ing bi­nary: No calls to `fork` are pre­sent, only a call to `clone` with the `SIGCHILD` flag. This ul­ti­mately im­ple­ments the legacy `fork` call with a call to `clone`. The rea­son for that re­sults in `clone` being a more pow­er­ful and con­fig­urable call than `fork`, mak­ing it pos­si­ble to re­place `fork` en­tirely with `clone` to spawn processes _and_ threads.

### Using clone()

The `clone` sys­tem call ac­cepts var­i­ous flags to con­fig­ure the process cre­ation. For the usage of name­spaces a sub­set of these flags can be used to spec­ify the new name­spaces a process will join. By de­fault the child processes are being ini­tial­ized with a mod­i­fied copy of the par­ent’s name­space con­fig­u­ra­tion when sup­ply­ing such a flag. This takes the de­sired name­spaces con­fig­u­ra­tion into ac­count and makes the child process a mem­ber of the new name­spaces that are rep­re­sented as flags. If a spe­cific flag of a name­space type is _not_ spec­i­fied, then the process is part of the par­ent’s name­space for this spe­cific type, pro­vid­ing no ad­di­tional level of iso­la­tion. Con­sider the UTS name­space ex­am­ple from above: The `clone` flag re­spon­si­ble for the cre­ation of such a name­space is `NEW_UTS`.

The `clone` call has the fol­low­ing pro­to­type, al­low­ing to spec­ify the flags de­scribed above:

```
int clone(int (*child_func)(void *), void *child_stack, int flags, void *arg)
```

*   `child_func`: Func­tion pointer to the func­tion being ex­e­cuted by the child process.
*   `child_stack`: The down­wardly grow­ing stack the child will op­er­ate on.
*   `flags`: In­te­ger value rep­re­sent­ing all used flags as con­fig­ured using an OR-Con­junc­tion of flags.
*   `arg`: Ad­di­tional ar­gu­ments

The fol­low­ing code snip­pet shows a min­i­mal ex­am­ple of using `clone` to spawn a shell process in a new UTS name­space. Please note that this min­i­mal ex­am­ple does not pro­vide in­cludes, error han­dling and does not check re­turn val­ues.

```
#define STACKSIZE 8192
char *childStack;
char *childStackTop;
pid_t childPid;

// Will be executed by the child process
int run(void *) {
    system("/bin/sh");
    return 0;
}

int main(int argc, char const *argv[]) {
    childStack = (char *)malloc(STACKSIZE);
    // stack grows downward
    childStackTop = childStack + STACKSIZE;
    childPid = clone(run, childStackTop,
        CLONE_VFORK | CLONE_NEWUTS, 0);
    return 0;
}
```

After com­pil­ing and run­ning this ex­am­ple a shell in a new UTS name­space is being spawned.

### un­share()

This sys­tem call al­lows processes to dis­able shar­ing name­spaces _after_ they have been cre­ated. In con­trast, `clone` causes child processes to be moved to name­spaces while cre­at­ing them. There ex­ists a CLI ap­pli­ca­tion avail­able called `unshare` that cre­ates a new process\` and un­shares spe­cific sys­tem re­sources, whereas using the sys­tem call in a C pro­gram iso­lates a process at run­time _with­out_ al­lo­cat­ing a new process:

```
int main(int argc, char const *argv[]) {
    unshare(CLONE_NEWUTS); // No error handling
    // Print hostname
    system("hostname");
    // Set hostname
    system("hostname testname");
    // Print hostname again
    system("hostname");
    // Print the available namespaces
    system("lsns");
    // Spawn a shell
    execvp("/bin/sh", 0);
    return 0;
}
```

When com­pil­ing and run­ning the pro­gram a sim­i­lar out­put to the fol­low­ing list­ing con­tent can be ob­served:

```
root@box:~# ./a.out
box
testname
[...]
NS         TYPE      NPROCS  PID USER           COMMAND
[...]
4026531835 cgroup    336     1 root             /sbin/init splash
4026531836 pid       293     1 root             /sbin/init splash
4026531837 user      293     1 root             /sbin/init splash
4026531838 uts       333     1 root             /sbin/init splash
4026531839 ipc       336     1 root             /sbin/init splash
4026531840 mnt       326     1 root             /sbin/init splash
4026532009 net       292     1 root             /sbin/init splash
[...]
4026532436 uts         3 27750 root             ./a.out
[...]
root@box:~# lsns | grep uts
4026531838 uts       134   803 user             /bin/sh
```

As seen above, the cre­ated process is able to set its own host­name, ul­ti­mately iso­lat­ing the value of this set­ting in-place. Ad­di­tion­ally, the spawned process is pre­sent in _two_ UTS name­spaces. The par­ent process is only a mem­ber of one UTS name­space. Ac­cord­ing to the name­space IDs, one of these UTS name­spaces is shared as it’s the de­fault name­space of that type. The other name­space is the one that has been cre­ated using the `unshare` call.

The com­mand `lsns` gath­ers the dis­played in­for­ma­tion by check­ing the con­tents of the `/proc/<PID>/ns` di­rec­tory for each PID. In the con­text of con­tain­ers it should not be pos­si­ble to get in­for­ma­tion on par­ent name­spaces like it’s pos­si­ble in this ex­am­ple. When cre­at­ing a con­tainer, the `/proc` di­rec­tory or sen­si­tive parts of it should there­fore be iso­lated to pre­vent this in­for­ma­tion leak.

### setns()

To add processes to al­ready ex­ist­ing name­spaces, `setns` is being uti­lized. It dis­as­so­ci­ates a process from its orig­i­nal name­space and as­so­ci­ates it with an­other name­space of the same type. The pro­to­type of this sys­tem call is as fol­lows:

```
int setns(int fd, int nstype)
```

*   `fd`: File de­scrip­tor of a sym­bolic link rep­re­sent­ing a spe­cific name­space as rep­re­sented in `/proc/<PID>/ns`.
*   `nstype`: This pa­ra­me­ter is des­ig­nated for checks re­gard­ing the name­space type. By pass­ing a `CLONE_NEW*` flag, the name­space type of the first pa­ra­me­ter is checked be­fore en­ter­ing the name­space. This makes sure that the passed file de­scrip­tor in­deed points to the de­sired name­space type. To dis­able this check, a zero value can also be used for this pa­ra­me­ter.

A sim­ple ex­am­ple that in­vokes a com­mand in a given name­space can be ex­am­ined in the fol­low­ing code list­ing (\[2\] – mod­i­fied):

```
[...]
// Get namespace file descriptor
int fd = open(argv[1], O_RDONLY);
// Join the namespace
setns(fd, 0);
// Execute a command in the namespace
execvp(argv[2], &argv[2]);
[...]
```

The code above launches a child process that ex­e­cutes the spec­i­fied com­mand and re­sides in a dif­fer­ent name­space as the par­ent.

To per­form this from a CLI the `nsenter` ap­pli­ca­tion can be of use. Cosider a shell process re­sid­ing in a sep­a­rate UTS name­space: By ex­e­cut­ing `nsenter -a -t 1` the process is being moved to all name­spaces orig­i­nat­ing from the sys­tem ini­tial­iza­tion process with PID `1`. This ef­fec­tively re­verts the call to `unshare`, mak­ing the _orig­i­nal_ UTS name­space avail­able to the shell process. Now chang­ing the host­name from the shell process will af­fect the host­name of the de­fault name­space and there­fore the sys­tem’s host­name. As a re­sult, it’s im­por­tant to pre­vent these types of `setns` calls by iso­lat­ing the ex­posed name­spaces. This il­lus­trates that by using name­spaces alone it may not be pos­si­ble to pre­vent sys­tem mod­i­fi­ca­tions.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [“The Mount Name­space and a De­scrip­tion of a Re­lated In­for­ma­tion Leak in Docker”](https://www.schutzwerk.com/en/43/posts/namespaces_02_mnt/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - LWN: Glibc and the ker­nel user-space API](https://lwn.net/Articles/534682/)
*   [2 - LWN: Name­spaces in op­er­a­tion, part 2: the name­spaces API](https://lwn.net/Articles/531381/)
*   [Title Image](https://unsplash.com/photos/uBe2mknURG4)
