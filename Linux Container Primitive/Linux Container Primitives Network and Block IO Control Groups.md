---
title: "Linux Container Primitives Network and Block IO Control Groups"
layout: post
---
 [www.schutzwerk.com /en/43/posts/control\_groups\_02\_network\_block\_io/](https://www.schutzwerk.com/en/43/posts/control_groups_02_network_block_io/)

# Linux Container Primitives: Network and Block I/O Control Groups

SCHUTZWERK GmbH 8-10 minutes

* * *

In the pre­vi­ous post of the Linux Con­tainer Prim­i­tives se­ries, the ba­sics of con­trol groups were cov­ered. This post il­lus­trates the pur­pose of two _cgroup_ con­trollers: Net­work and Block I/O. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.


* * *

## The Net­work Con­troller (v1)

This sec­tion cov­ers the re­source con­trollers `net_cl` and `net_prio`. Both cause iden­ti­fiers to be at­tached to sock­ets once they are cre­ated by a process that’s being man­aged by one of the two con­trollers. The dif­fer­ence be­tween the two con­trollers is that `net_prio` as­signs an ID that’s unique for each con­trol group whereas `net_cl` uses a spec­i­fied iden­ti­fier that does not have to be unique for each _cgroup_, al­low­ing flex­i­ble tag­ging of sock­ets in classes \[1\]. Adding these iden­ti­fiers al­lows quick checks to de­ter­mine whether a socket orig­i­nates from the same con­trol group or class. This is more ef­fi­cient than search­ing in the con­trol group tree, for ex­am­ple using the _cgroup_ func­tion `is_descendant()` to per­form these checks - es­pe­cially if this has to be per­formed reg­u­larly.

There are mul­ti­ple use-cases for these ad­di­tional socket at­trib­utes. Among oth­ers, some of them are:

*   Set­ting the pri­or­ity of net­work pack­ets orig­i­nat­ing from a spe­cific socket or de­vice by using the net­work pri­or­ity set by `net_prio`.
*   Using `iptables` in com­bi­na­tion with the `net_cl` class iden­ti­fier to fil­ter and route pack­ets based on the con­trol group mem­ber­ship.
*   Sched­ul­ing net­work pack­ets based on class iden­ti­fiers.

A sim­ple usage ex­am­ple of `net_cls` that drops all IP based traf­fic for all processes not pre­sent in a spe­cific con­trol group can be seen below \[2\]:

```
root@box :~# mkdir /sys/fs/cgroup/net_cls # Create mountpoint
root@box :~# mount -t cgroup \ # Mount the controller
    -o net_cls net_cls /sys/fs/cgroup/net_cls
root@box :~# mkdir /sys/fs/cgroup/net_cls/IPAllowed # Create cgroup
root@box :~# echo 0x100001 > \ # Set the fixed class identifier
    /sys/fs/cgroup/net_cls/IPAllowed/net_cls.classid
root@box :~# tc qdisc add dev <interface> root handle 10: htb
root@box :~# tc class add dev <interface> parent 10: classid 10:1 \
        htb rate 40mbit
root@box :~# tc filter add dev <interface> parent 10: protocol ip \
        prio 10 handle 1: cgroup
root@box :~# iptables -A OUTPUT -m cgroup ! \
        --cgroup 0x100001 -j DROP # Disallow IP for all non-members
root@box :~# echo $ > \ # Add process to cgroup
        /sys/fs/cgroup/net_cls/IPAllowed/cgroup.procs
-- Filtering active --
root@box :~# tc qdisc del dev <interface> root; \ # Revert settings
        tc qdisc add dev <interface> root pfifo
```

The `tc` (_Traf­fic Con­trol_) com­mands listed above are being used to set up a `qdisc` (Queue­ing-Dis­ci­pline) that uses the fixed con­trol group class to clas­sify the traf­fic orig­i­nat­ing from a con­trol group on a net­work in­ter­face by as­sign­ing it to a han­dle called `cgroup`. This fil­ter­ing is ac­com­plished by using a HTB (Hi­er­ar­chi­cal Token Bucket) fil­ter. With `iptables` it’s then pos­si­ble to use the `cgroup` han­dle to add rules for a con­trol group, e.g. al­low­ing net­work ac­cess.

This con­troller type is an ex­am­ple where child con­trol groups are not au­to­mat­i­cally af­fected by the `net_*` con­trollers, mean­ing that this set­ting is not in­her­ited through­out the hi­er­ar­chy.

## The Block IO Con­troller (v1/v2)

The `blkio` (`v1`) / `io` (`v2`) con­troller is being uti­lized to en­able I/O re­source usage poli­cies. The most com­mon use-cases to limit these as­pects are:

*   Spec­i­fy­ing upper band­width lim­its, for ex­am­ple in the `blkio.throttle.read_bps_device` file to spec­ify the max­i­mum band­width for a de­vice in bits per sec­ond. Al­ter­na­tively, the `rbps` pa­ra­me­ter in con­junc­tion with the `io.max` file is the equiv­a­lent for ver­sion 2.
    
*   Deny­ing ac­cess to a spe­cific de­vice.
    
*   Lim­it­ing with pro­por­tional time based di­vi­sion: This al­lows set­tings weights for var­i­ous con­trol groups that will be used to pri­or­i­tize all de­vice ac­cesses when per­form­ing I/O op­er­a­tions. The `blkio.weight` file is pre­sent for this pur­pose in _cgroup_ `v1` whereas this is con­fig­ured with `io.weight` in ver­sion 2.
    

### En­forc­ing Lim­its in the Ker­nel

En­forc­ing band­width lim­its is im­ple­mented in `blk_throtl_bio` which re­sides in `block/blk-throttle.c`. This func­tion makes use of `throtl_charge_bio` to ul­ti­mately charge for the data vol­ume used in an I/O op­er­a­tion. De­pend­ing on the re­source usage, an I/O op­er­a­tion can be ex­e­cuted di­rectly or may have to be de­layed using a queue to meet the re­source lim­i­ta­tions. For de­layed op­er­a­tions, a dis­patcher func­tion will then cause pend­ing op­er­a­tions to be ex­e­cuted using pre-cal­cu­lated timers in order to throt­tle re­quested op­er­a­tions. With `throtl_trim_slice` the re­quired time lim­it­ing is cal­cu­lated, yield­ing the time slice the op­er­a­tion may be ex­e­cuted in.

To allow or deny ac­cess­ing a spe­cific de­vice, func­tions of `security/device_cgroup.c` come to use. When pass­ing _cgroup_ con­fig­u­ra­tion strings to the files pre­sent in the vir­tual con­trol group file sys­tem `devcgroup_update_access` parses this in­for­ma­tion and con­fig­ures the con­trol group ac­cord­ingly, e.g. by set­ting flags in­di­cat­ing whether ac­cess­ing a de­vice is al­lowed or de­nied for processes of a _cgroup_. This builds an ex­cep­tion list as seen in the list­ing fol­low­ing below. Upon ac­cess­ing a block de­vice, `__blkdev_get` (`fs/block_dev.c`) is being called which per­forms ac­cess checks prior to al­low­ing ac­cess. To per­form these checks, `__devcgroup_check_permission` (`security/device_cgroup.c`) is called, re­sult­ing in the fol­low­ing checks being per­formed using the in­ter­nal ex­cep­tion list:

```
// current is the current task_struct
dev_cgroup = task_devcgroup(current);
if (dev_cgroup->behavior == DEVCG_DEFAULT_ALLOW)
    // perform checks based on the exception list
    rc = !match_exception_partial(&dev_cgroup->exceptions,
        type, major, minor, access);
else
    rc = match_exception(&dev_cgroup->exceptions, type, major,
        minor, access);
if (!rc)
    return -EPERM; // deny access
```

The de­fault sched­uler for I/O op­er­a­tions in the Linux ker­nel is the CFQ sched­uler. It was ex­tended to sup­port the I/O re­lated _cgroup_ con­trollers after con­trol groups have been in­tro­duced in the ker­nel. This makes it pos­si­ble to ac­count and con­strain processes re­gard­ing their con­sumed I/O re­sources, for ex­am­ple using pre-de­fined weights. The CFQ I/O sched­uler is im­ple­mented in `block/cfq-iosched.h` - not to be con­fused with `kernel/sched/fair.c` where process-re­lated CFQ sched­ul­ing is im­ple­mented. The ker­nel struc­ture `cfq_group` maps var­i­ous set­tings per _cgroup_\-de­vice re­la­tion­ship. This in­cludes ap­plied poli­cies and weights which are con­sid­ered in order to sched­ule I/O op­er­a­tions.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [“The Mem­ory, CPU, Freezer and De­vice Con­trollers”](https://www.schutzwerk.com/en/43/posts/control_groups_03_memory_cpu_freezer_dev/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Con­trol groups, part 3: First steps to con­trol](https://lwn.net/Articles/605039/)
*   [2 - Net­work clas­si­fier cgroup](https://www.kernel.org/doc/Documentation/cgroup-v1/net_cls.txt)
*   [Title Image by Tom Fisk via Pex­els (cropped)](https://www.pexels.com/de-de/foto/meer-stadt-strasse-verkehr-2226457)

