---
title: "Linux Container Basics Capabilities"
layout: post
---
 [www.schutzwerk.com /en/43/posts/linux\_container\_capabilities/](https://www.schutzwerk.com/en/43/posts/linux_container_capabilities/)

# Linux Container Basics: Capabilities

SCHUTZWERK GmbH 12-15 minutes

* * *

The tra­di­tional way of han­dling per­mis­sions in Linux in­volves ex­actly two process types: _Priv­i­leged_ and _un­priv­i­leged_ processes. When dis­re­gard­ing ca­pa­bil­i­ties, a process is priv­i­leged in case the ef­fec­tive UID is equal to `0` – pro­vid­ing per­mis­sions com­monly re­ferred to as _root priv­i­leges_. The Linux ker­nel makes a sharp dis­tinc­tion be­tween priv­i­leged and un­priv­i­leged processes. For ex­am­ple, priv­i­leged processes are al­lowed to by­pass var­i­ous ker­nel per­mis­sion checks \[1\]. This re­sults in a se­vere se­cu­rity vi­o­la­tion in case un­trusted ap­pli­ca­tions are al­lowed to run with `root` priv­i­leges.

In prac­tice, many sys­tem ser­vices and ap­pli­ca­tions have to be ex­e­cuted with `root` priv­i­leges. Oth­er­wise the per­mis­sions for priv­i­leged ac­tions may be miss­ing for a given process. For in­stance, con­sider send­ing ICMP net­work pack­ets with the `ping` com­mand: Be­fore ca­pa­bil­i­ties were in­tro­duced in the Linux ker­nel, it was re­quired to ex­e­cute ap­pli­ca­tions that use raw sock­ets with el­e­vated priv­i­leges – for ex­am­ple using the SUID bit. This the­o­ret­i­cally en­ables the `ping` process that’s being ex­e­cuted as `root` to per­form priv­i­leged sys­tem changes or ac­tions that may or may not be de­sired – ad­di­tion­ally to per­form­ing the task the process is des­ig­nated for. Trac­ing other processes, mount­ing de­vices and load­ing ar­bi­trary ker­nel mod­ules are some of the ac­tions that will be al­lowed, ad­di­tion­ally to using raw sock­ets. There­fore, the re­sult­ing at­tack sce­nario in­volves ex­e­cut­ing un­trusted bi­na­ries or run­ning po­ten­tially vul­ner­a­ble sys­tem ser­vices with `root` priv­i­leges that may be used by a ma­li­cious actor to harm a sys­tem.

Be­cause of the se­cu­rity con­cerns re­sult­ing from this, _ca­pa­bil­i­ties_ have been in­tro­duced start­ing from ker­nel ver­sion `2.2`. The basic idea is to split the `root` per­mis­sion into small pieces that are able to be dis­trib­uted in­di­vid­u­ally on a thread basis with­out hav­ing to grant all per­mis­sions to a spe­cific process at once. A com­plete list of all avail­able ca­pa­bil­i­ties is pre­sent in the ca­pa­bil­ity man­ual page \[1\]. In par­tic­u­lar, a pow­er­ful ca­pa­bil­ity is `CAP_SYS_ADMIN` which al­lows to per­form com­pre­hen­sive ac­tions on a sys­tem. Please note that this ca­pa­bil­ity is over­loaded and its use is dis­cour­aged in case bet­ter al­ter­na­tives are avail­able, for ex­am­ple using only a min­i­mal set of ca­pa­bil­i­ties. How­ever, some­times using this ca­pa­bil­ity is re­quired – for ex­am­ple `mount` op­er­a­tions re­quire this spe­cific ca­pa­bil­ity. Con­sid­er­ing the ex­am­ple with `ping` would in­volve adding the `CAP_NET_RAW` ca­pa­bil­ity to the process mak­ing use of raw sock­ets with­out hav­ing to grant a full set of `root` priv­i­leges.

There are two ways a process can ob­tain a set of ca­pa­bil­i­ties:

*   **In­her­ited ca­pa­bil­i­ties**: A process can in­herit a sub­set of the par­ent’s ca­pa­bil­ity set. To in­spect the avail­able ca­pa­bil­i­ties for a process, the `/proc/<PID>/status` file can be ex­am­ined.
    
*   **File ca­pa­bil­i­ties**: It’s pos­si­ble to as­sign ca­pa­bil­i­ties to a bi­nary, e.g. using `setcap`. The process cre­ated when ex­e­cut­ing a bi­nary of this type is then al­lowed to use the spec­i­fied ca­pa­bil­i­ties on run­time. This re­quires that the process is _ca­pa­bil­ity-aware_, mean­ing that it ex­plic­itly re­quests the ker­nel to allow it to use these ca­pa­bil­i­ties. In case a bi­nary was de­vel­oped be­fore ca­pa­bil­i­ties were in­tro­duced in the ker­nel, this is not the case as the re­quired sys­tem calls for this task were not avail­able at the time the bi­nary was com­piled. Hence, the process can fail to per­form its des­ig­nated task be­cause of miss­ing per­mis­sions. The Linux ker­nel man­ual calls these bi­na­ries _ca­pa­bil­ity-dumb bi­na­ries_. A so­lu­tion for this kind of prob­lem will be dis­cussed later on. Query­ing a bi­nary for file ca­pa­bil­i­ties is ac­com­plished with the `getcap` util­ity. Per­form­ing this on Arch Linux and the in­cluded `ping` bi­nary shows that the `CAP_NET_RAW` file ca­pa­bil­ity is pre­sent, al­low­ing pings with­out grant­ing `root` per­mis­sions to the process.
    

## The `execve` Sys­tem Call

In the Linux ker­nel, the two main op­er­a­tions re­gard­ing processes are `fork` and `exec`. The `fork` sys­tem call cre­ates a new process as a copy of the par­ent process. On the other hand, `exec` re­places the cur­rent process with the con­tents re­sult­ing from ex­e­cut­ing an­other pro­gram. Both mech­a­nisms are often used in com­bi­na­tion, for ex­am­ple when in­vok­ing a com­mand in a ter­mi­nal that’s not a shell-builtin:

1.  `fork` is called to cre­ate a child process.
2.  The child in­vokes `exec` to re­place its con­tents with the de­sired pro­gram.
3.  The par­ent waits for the child to exit and re­trieves the exit code.

Please note that `fork` is re­quired in the sce­nario above be­cause the orig­i­nal process is not ready to exit yet – it rather in­vokes an ex­ter­nal com­mand and con­tin­ues its ex­e­cu­tion af­ter­wards. With­out call­ing `fork` the _cur­rent_ process would be re­placed with the de­sired pro­gram. This process exits im­me­di­ately after its ex­e­cu­tion which is not de­sired in the con­text of a shell.

Dis­cussing `exec` above does not refer to a sin­gle sys­tem call. It rather de­scribes the usage of a sys­tem call of the `exec`\-fam­ily. While there are mul­ti­ple `exec` calls, most of them are built by mak­ing use of `execve`. The dif­fer­ences in the func­tion API man­i­fest in the pro­vided pa­ra­me­ters for each call.

The `execve` call is par­tic­u­larly im­por­tant re­gard­ing the var­i­ous ca­pa­bil­ity sets and thereto re­lated rules which will be dis­cussed fur­ther on.

## Ca­pa­bil­ity Sets

There exist five dif­fer­ent ca­pa­bil­ity sets for each process, each rep­re­sented by a 64 bit value:

*   **Cap­Eff**: The _ef­fec­tive_ ca­pa­bil­ity set rep­re­sents all ca­pa­bil­i­ties the process is using at the mo­ment. For file ca­pa­bil­i­ties the ef­fec­tive set is in fact a sin­gle bit in­di­cat­ing whether the ca­pa­bil­i­ties of the per­mit­ted set will be moved to the ef­fec­tive set upon run­ning a bi­nary. This makes it pos­si­ble for bi­na­ries that are not ca­pa­bil­ity-aware to make use of file ca­pa­bil­i­ties with­out is­su­ing spe­cial sys­tem calls.
*   **Cap­Prm**: The _per­mit­ted_ set in­cludes all ca­pa­bil­i­ties a process may use. These ca­pa­bil­i­ties are al­lowed to be copied to the ef­fec­tive set and used after that.
*   **Cap­Inh**: Using the _in­her­ited_ set all ca­pa­bil­i­ties that are al­lowed to be in­her­ited from a par­ent process can be spec­i­fied. This pre­vents a process from re­ceiv­ing any ca­pa­bil­i­ties it does not need. This set is pre­served across an `execve` and is usu­ally set by a process _re­ceiv­ing_ ca­pa­bil­i­ties rather than by a process that’s hand­ing out ca­pa­bil­i­ties to its chil­dren.
*   **CapBnd**: With the _bound­ing_ set it’s pos­si­ble to re­strict the ca­pa­bil­i­ties a process may ever re­ceive. Only ca­pa­bil­i­ties that are pre­sent in the bound­ing set will be al­lowed in the in­her­i­ta­ble and per­mit­ted sets.
*   **Ca­pAmb**: The _am­bi­ent_ ca­pa­bil­ity set ap­plies to all non-SUID bi­na­ries with­out file ca­pa­bil­i­ties. It pre­serves ca­pa­bil­i­ties when call­ing `execve`. How­ever, not all ca­pa­bil­i­ties in the am­bi­ent set may be pre­served be­cause they are being dropped in case they are not pre­sent in ei­ther the in­her­i­ta­ble or per­mit­ted ca­pa­bil­ity set. This set is pre­served across `execve` calls.

The sets dis­cussed above allow a fine grained con­trol over how ca­pa­bil­i­ties are being dis­trib­uted and in­her­ited across processes and bi­na­ries. For back­wards com­pat­i­bil­ity all ca­pa­bil­i­ties are being granted to processes run­ning as `root` user, in­clud­ing SUID bi­na­ries.

## Ca­pa­bil­ity Rules

Child processes cre­ated using `fork` in­herit the full set of the par­ent’s ca­pa­bil­i­ties. More­over, there exist rules \[1\] to de­ter­mine whether and how ca­pa­bil­i­ties of a process are being in­her­ited or mod­i­fied upon call­ing `execve` when cre­at­ing a child process:

![](https://www.schutzwerk.com/en/blog/linux-container-capabilities/capability-rules.jpg)

In con­se­quence of the rules above, UID changes have an ef­fect on the avail­able ca­pa­bil­i­ties. If there’s a tran­si­tion from a zero to a non-zero ef­fec­tive UID value, the ef­fec­tive and per­mit­ted ca­pa­bil­ity sets are being cleared.

There may be sce­nar­ios where a process re­quires a ca­pa­bil­ity to per­form ini­tial con­fig­u­ra­tion of the sys­tem and is then able to drop spe­cific ca­pa­bil­i­ties be­cause their usage is not re­quired any­more. In gen­eral it’s a good soft­ware de­sign to drop all un­nec­es­sary ca­pa­bil­i­ties – con­tainer en­gines like Docker do this by de­fault. To per­form this task, the `prctl` util­ity can be of use. By uti­liz­ing this tool, the _se­cure bits_ of a process are being ma­nip­u­lated. These flags con­trol how ca­pa­bil­i­ties are being han­dled for the `root` user. One flag, namely `SECBIT_KEEP_CAPS`, con­trols whether ca­pa­bil­i­ties are being cleared in case a UID tran­si­tion, for ex­am­ple with `setuid`, takes place. By set­ting this bit the cur­rent ca­pa­bil­ity set is being re­tained when switch­ing to an­other user from a process. For ex­am­ple, the im­ple­men­ta­tion of `ntp` \[2\], the NTP dae­mon drops its ca­pa­bil­i­ties as fol­lows:

*   Set the `PR_SET_KEEPCAPS` se­cure bit using `prctl`. At this point `ntpd` is still being ex­e­cuted with `root` priv­i­leges.
*   Use a `setuid` call to switch to an un­priv­i­leged user. Be­cause of the pre­vi­ous step all ca­pa­bil­i­ties are re­tained across the UID change.
*   At this point the NTP dae­mon is run­ning as an un­priv­i­leged user. How­ever, all ca­pa­bil­i­ties are still avail­able to the `ntpd` process. With `cap_set_proc` of the `libcap` li­brary only a se­lected sub­set of the cur­rently avail­able ca­pa­bil­i­ties are being ex­plic­itly used while all oth­ers are being dropped au­to­mat­i­cally.

Pre­vent­ing a cer­tain ca­pa­bil­ity from being pre­sent in a ca­pa­bil­ity set of a given process can be achieved with the `PR_CAPBSET_DROP` flag. It ef­fec­tively drops a ca­pa­bil­ity from the bound­ing set and drops priv­i­leges prior to being added to one of the other ca­pa­bil­ity sets.

While con­tainer run­times can ef­fec­tively make use of ca­pa­bil­i­ties, the gen­eral adop­tion of this se­cu­rity mech­a­nism that was once the hope for an in­no­va­tion in priv­i­lege man­age­ment was hin­dered \[3\]. This re­sults from ca­pa­bil­i­ties being con­sid­ered too com­plex and un­handy to use in the past. After all, spe­cific ca­pa­bil­ity sets and thereto re­lated mech­a­nisms, such as the am­bi­ent set that was added in Linux `4.3` (2015), were added long after the ini­tial ca­pa­bil­ity im­ple­men­ta­tion. With the cur­rent ca­pa­bil­ity im­ple­men­ta­tion and API many of the orig­i­nal points of crit­i­cism can be dis­qual­i­fied.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [An In­tro­duc­tion to Name­spaces](https://www.schutzwerk.com/en/43/posts/namespaces_01_intro/)

## Ref­er­ences / Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

*   [1 - Linux man­ual pages: Overview of Linux ca­pa­bil­i­ties](http://man7.org/linux/man-pages/man7/capabilities.7.html)
*   [2 - Source Code: ntpd.c](https://github.com/ntp-project/ntp/blob/47e9cc7fd5b31417ac34b1b97b7f72fad0fb03dc/ntpd/ntpd.c#L938)
*   [3 - LWN: In­her­it­ing ca­pa­bil­i­ties](https://lwn.net/Articles/632520/)
*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)
