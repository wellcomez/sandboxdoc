---
title: "Linux Container Primitives Namespaces Kernel View and Usage in Containerization"
layout: post
---
 [www.schutzwerk.com /en/43/posts/namespaces\_05\_kernel/](https://www.schutzwerk.com/en/43/posts/namespaces_05_kernel/)

# Linux Container Primitives: Namespaces Kernel View and Usage in Containerization

SCHUTZWERK GmbH 8-11 minutes

* * *

After dis­cussing the var­i­ous types of name­spaces in the [pre­vi­ous part](https://www.schutzwerk.com/en/43/posts/namespaces_04_user/) of the Linux Con­tainer Prim­i­tive se­ries, this post de­scribes the in­ter­nals of name­spaces in the Linux ker­nel. Also, prac­ti­cal use-cases for name­spaces in terms of con­tainer­iza­tion are con­sid­ered. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.


* * *

## Name­spaces Ker­nel View

This sec­tion cov­ers as­pects of the Linux ker­nel code re­gard­ing the im­ple­men­ta­tion of name­spaces. For this, var­i­ous points, such as _tasks_, _cre­den­tials_ and _name­space prox­ies_ are being taken into ac­count. In the ker­nel source code, these are all rep­re­sented by C struc­tures \[1\]:

![Namespaces Structures](https://www.schutzwerk.com/en/43/img/Namespaces/ns-krnl-v1.png)

From a ker­nel point of view, processes are often called tasks. The ker­nel keeps track of them by stor­ing task in­for­ma­tion in a dou­bly linked list. This list is called task list and con­tains el­e­ments of the `task_struct` type, as de­fined in `linux/sched.h`. These struc­tures act as task de­scrip­tors and con­tain all rel­e­vant in­for­ma­tion on a task, for ex­am­ple:

*   The process iden­ti­fier (`pid_t pid`)
*   Man­aged file de­scrip­tors (`files_struct *files`)
*   Pointer to the par­ent task (`task_struct *parent`)
*   Name­space proxy (`nsproxy *nsproxy`)
*   Task cre­den­tials (`cred *cred`)

Using the cre­den­tial in­for­ma­tion listed above, the Linux ker­nel man­ages the as­so­ci­ated own­er­ship in­for­ma­tion of cer­tain ob­jects, such as files, sock­ets and tasks. Upon re­quest­ing an ac­tion to be per­formed on an ob­ject, for ex­am­ple writ­ing to a file, the ker­nel ex­e­cutes a se­cu­rity check in order to de­ter­mine whether a user is al­lowed to per­form an ac­tion in re­gard to the task’s as­so­ci­ated cre­den­tials. The `cred` struc­ture is de­fined in `linux/cred.h` and con­tains a `user_namespace` pointer. This is re­quired be­cause cre­den­tials are rel­a­tive to the spe­cific user name­space that’s cur­rently ac­tive.

To as­so­ci­ate a task to the name­space it cur­rently runs in, the `nsproxy` ker­nel struc­ture con­tains a pointer to each per-process name­space. The PID and user name­space are an ex­cep­tion: The `pid_ns_for_children` pointer is the PID name­space the _chil­dren_ of the process will use. The cur­rently ac­tive PID name­space for a task can be found in the `task_active_pid_ns` pointer. More­over, the user name­space is a part of the `cred` struc­ture as seen above.

For an ex­am­ple of the linked ker­nel struc­tures, con­sider the as­so­ci­a­tion of a task to a spe­cific UTS name­space in the fig­ure \[1\] below:

![Namespaces UTS Structures](https://www.schutzwerk.com/en/43/img/Namespaces/ns-krnl-uts.png)

This im­plies that other parts of the ker­nel code also have to be aware of name­spaces, e.g. when re­turn­ing a host­name upon ac­cess­ing this in­for­ma­tion from a task. This is im­ple­mented by mod­i­fy­ing the orig­i­nal code of `gethostname` in order to read the host­name of the cur­rent name­space and not from the sys­tem’s host­name. The name­space un­aware ver­sion of this func­tion is listed below:

```
[...]
i = 1 + strlen(
    system_utsname.nodename);
[...]
if (copy_to_user(
    name,
    system_utsname.nodename,
    i))
[...]
```

The mod­i­fied ver­sion for name­spaces fol­lows below:

```

[...]
u = utsname();
i = 1 + strlen(u->nodename);
[...]
memcpy(tmp, u->nodename, i);
[...]
if (copy_to_user(name, tmp,
    i)
)
[...]
```

The call to `utsname` is being used in name­space aware ver­sions of the `gethostname` call to de­ter­mine the de­sired host­name value using the `nsproxy` ker­nel struc­ture men­tioned above:

```
static inline struct new_utsname *utsname(void)
{
    return &current->nsproxy->uts_ns->name;
}
```

Upon cre­at­ing a child process, the el­e­ments of the `nsproxy` struc­ture will be copied as the child will re­side in the exact same name­spaces the par­ent lives in. This can be ob­served in the `copy_process` func­tion called in `do_fork`. By using `copy_namespaces`, the mem­bers of the par­ent’s `nsproxy` struc­ture are being du­pli­cated:

```
static struct nsproxy *create_new_namespaces(unsigned long flags,
  struct task_struct *tsk, struct user_namespace *user_ns,
  struct fs_struct *new_fs) {
    struct nsproxy *new_nsp;
    int err;
    // Create a new namespace proxy
    new_nsp = create_nsproxy();
    [...]
    // Copy the existing UTS namespace (among others)
    new_nsp->uts_ns = copy_utsname(flags, user_ns,
        tsk->nsproxy->uts_ns);
    [...]
}
```

Upon using `unshare`, the `ksys_unshare` func­tion is being ex­e­cuted. This calls `unshare_nsproxy_namespaces` which ef­fec­tively leads to the `nsproxy` struc­ture being copied, mod­i­fied ac­cord­ing to the `unshare` call and re­placed as soon as one el­e­ment of the struc­ture is being mod­i­fied.

Every `nsproxy` struc­ture con­tains a `count` el­e­ment. This counter keeps track of how many tasks refer to the same `nsproxy`. After a task ter­mi­nates, the `count` value of the as­so­ci­ated `nsproxy` gets decre­mented and in case there’s no other task using a spe­cific `nsproxy`, it gets freed along with all con­tained name­spaces:

```
void free_nsproxy(struct nsproxy *ns) {
    // Check if the namespace is still in use, free if unsued
	if (ns->mnt_ns)
		put_mnt_ns(ns->mnt_ns);
	if (ns->uts_ns)
		put_uts_ns(ns->uts_ns);
	if (ns->ipc_ns)
        put_ipc_ns(ns->ipc_ns);

	if (ns->pid_ns_for_children)
		put_pid_ns(ns->pid_ns_for_children);
	put_cgroup_ns(ns->cgroup_ns);
	put_net(ns->net_ns);
	kmem_cache_free(nsproxy_cachep, ns);
}
```

The `put_*_ns` func­tions seen above are re­spon­si­ble for de­stroy­ing un­used name­spaces of a spe­cific type. Of course, this only hap­pens in case no other task uses the par­tic­u­lar name­space any­more.

When en­ter­ing a name­space using `setns`, the func­tion `switch_task_namespaces` is being used:

```
void switch_task_namespaces(struct task_struct *p,
  struct nsproxy *new) {
    struct nsproxy *ns;
    [...]
    // lock the task before switching namespace
    task_lock(p);
    // switch namespace
    ns = p->nsproxy;
    p->nsproxy = new;
    task_unlock(p);

    // delete the old namespace proxy if unused
    if (ns && atomic_dec_and_test(&ns->count))
        free_nsproxy(ns);
}
```

Mov­ing a task to a spe­cific name­space can be as sim­ple as set­ting the re­spec­tive point­ers of the `nsproxy` struc­ture.

## Usage in Con­tainer­iza­tion

Both LXC and Docker apply a stan­dard name­space con­fig­u­ra­tion in case no fur­ther con­fig­u­ra­tion is sup­plied. This in­volves set­ting up a new name­space for each type that has been dis­cussed in this chap­ter and that’s also sup­ported by the ker­nel. Spe­cific name­space con­fig­u­ra­tion can be ap­plied using the Docker CLI and LXC con­fig­u­ra­tion files.

Ad­di­tional con­fig­u­ra­tion may in­clude sup­ply­ing a spe­cific name­space a con­tainer is join­ing. For ex­am­ple, the `--net=host` op­tion dis­ables cre­at­ing a new net­work name­space for a Docker con­tainer, al­low­ing it to join the ini­tial net­work name­space. There­fore no net­work iso­la­tion is in place and for in­stance `localhost` in a con­tainer points to `localhost` of the host, cre­at­ing a be­hav­ior as if a process is run­ning with­out con­tainer­iza­tion in re­gard to its avail­able net­work en­vi­ron­ment. This al­lows flex­i­ble se­tups by al­low­ing the user to choose the iso­lated re­sources freely.

Docker con­fig­ures user name­spaces in a way that maps a non-ex­is­tent UID as `root` in a con­tainer. There­fore, es­ca­lat­ing to an­other user name­space by abus­ing a po­ten­tial vul­ner­a­bil­ity does not add any priv­i­leges \[2\]. This is being per­formed by using the `/etc/sub{u, g}id` files. By cre­at­ing a _sub­or­di­nate_ ID map­ping in these files, a range of non-ex­ist­ing IDs is as­signed to each real user of the host. This en­sures that the ranges do not over­lap and are in fact dis­junct. Using this range non-ex­ist­ing users are being mapped into a con­tainer, start­ing from a zero value which cor­re­sponds to `root`.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [An In­tro­duc­tion to Con­trol Groups](https://www.schutzwerk.com/en/43/posts/cgroups_01_intro/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Linux Con­tain­ers: Basic Con­cepts](https://www.cl.cam.ac.uk/~lc525/files/Linux_Containers.pdf)
*   [2 - Iso­late con­tain­ers with a user name­space](https://docs.docker.com/engine/security/userns-remap/#disable-namespace-remapping-for-a-container)
*   [Title Image](https://unsplash.com/photos/hqGfTe2Ri9s) by [Francesco Un­garo/Un­splash](https://unsplash.com/@francesco_ungaro)

