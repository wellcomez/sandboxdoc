---
title: "Linux Container Primitives User Namespaces"
layout: post
---
 [www.schutzwerk.com /en/43/posts/namespaces\_04\_user/](https://www.schutzwerk.com/en/43/posts/namespaces_04_user/)

# Linux Container Primitives: User Namespaces

SCHUTZWERK GmbH 12-16 minutes

* * *

In the [pre­vi­ous part](https://www.schutzwerk.com/en/43/posts/namespaces_03_pid_net/) of the Linux Con­tainer Prim­i­tive se­ries, the PID and net­work name­spaces were dis­cussed. This post cov­ers one of the most im­por­tant name­space types in de­tail – the user name­space. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.


* * *

## User Name­spaces

This name­space type in­tro­duces map­ping user and group IDs and the iso­la­tion of ca­pa­bil­i­ties per-name­space. For in­stance, a process can run with a non-zero UID out­side of a user name­space while hav­ing a UID of zero in a name­space. This ul­ti­mately en­ables un­priv­i­leged users to have root priv­i­leges in iso­lated en­vi­ron­ments. More­over, this is the only name­space type not re­quir­ing the `CAP_SYS_ADMIN` ca­pa­bil­ity, al­low­ing un­priv­i­leged users to cre­ate this name­space type.

By map­ping the UID in the name­space to the orig­i­nal UID, all changes and ac­tions in­tro­duced by a user will be mapped to the set of priv­i­leges a user holds in the par­ent name­space. This pre­vents ac­tions in the global sys­tem con­text that the user nor­mally would not be al­lowed to per­form. This in­cludes send­ing sig­nals to processes out­side of the cur­rent name­space and ac­cess­ing files \[1\]. For ex­am­ple, a user with root priv­i­leges in a name­space is not able to read the `/etc/shadow` file be­cause the orig­i­nal priv­i­leges do not allow to do so. The same ap­plies to the map­ping of GID val­ues. Ap­plied changes, for ex­am­ple cre­at­ing new files, will be shown as orig­i­nat­ing from the user out­side of the name­space. Like­wise, call­ing `stat` will map IDs in the op­po­site di­rec­tion in order to cre­ate a cor­re­la­tion in re­gard to the ap­plied map­pings.

This name­space type can also be nested, sim­i­lar to PID name­spaces. Using `ioctl` op­er­a­tions it’s pos­si­ble to in­spect the re­la­tion­ships be­tween dif­fer­ent user name­spaces.

By de­fault, a process only has a spe­cific ca­pa­bil­ity in a user name­space in case it’s a mem­ber of the user name­space and the ca­pa­bil­ity is pre­sent in the cor­re­spond­ing ca­pa­bil­ity set.

Linux ca­pa­bil­i­ties are aware of user name­spaces as can be seen in the func­tion `has_ns_capability` of `kernel/capability.c`: With this func­tion the ker­nel is able to check whether a process, as rep­re­sented by a `task_struct` in the ker­nel code, has a ca­pa­bil­ity in a spe­cific user name­space. By de­fault, the first process in a new user name­space is granted a full set of ca­pa­bil­i­ties in that name­space. While hav­ing all ca­pa­bil­i­ties in the user name­space, the process does not have _any_ ca­pa­bil­i­ties in the par­ent user name­space. This means:

*   A process with all ca­pa­bil­i­ties in a user name­space is able to per­form priv­i­leged op­er­a­tions on re­sources that are solely gov­erned by this spe­cific user name­space.
*   The same process is not able to per­form changes to re­sources gov­erned by the par­ent name­space, the ini­tial name­space or out­side of name­spaces. For ex­am­ple even with `CAP_SYS_TIME` in the user name­space it’s not pos­si­ble to alter the sys­tem clock be­cause this is not gov­erned by a name­space type as of now and there­fore `CAP_SYS_TIME` in the ini­tial name­space is re­quired in this ex­am­ple.

In case a process has the `CAP_SYS_ADMIN` ca­pa­bil­ity it’s pos­si­ble for it to enter an ar­bi­trary al­ready ex­ist­ing user name­space using `setns`. In that case, this process also gains a full ca­pa­bil­ity set in the en­tered name­space. Also, if a process pos­sesses a ca­pa­bil­ity in a par­ent user name­space, it also has this ca­pa­bil­ity in all child name­spaces. All processes that run with the same UID as the process that cre­ated a user name­space have a full ca­pa­bil­ity set in a name­space and all of its chil­dren. This can be ver­i­fied by cre­at­ing two name­spaces as sib­lings. Processes in the par­ent name­space are able to use `setns` to switch to each of the name­spaces. How­ever, it’s not pos­si­ble for one of the child name­spaces to do switch to a sib­ling name­space be­cause of the lack of `CAP_SYS_ADMIN` in the tar­get name­space.

In the sce­nario of con­tain­ers it’s com­mon prac­tice to re­duce the set of avail­able ca­pa­bil­i­ties a con­tainer and all its processes may have. The re­duced ca­pa­bil­ity set that’s in place by de­fault for Docker con­tain­ers is pre­sent in the OCI spec­i­fi­ca­tion’s source code \[2\]. Miss­ing ca­pa­bil­i­ties can be added and re­moved on de­mand when start­ing a new con­tainer – for sim­ple use-cases the pre-de­fined set can serve as a se­cure base. All ca­pa­bil­i­ties can be granted to a process with the `privileged` flag al­though its use should be eval­u­ated care­fully in terms of se­cu­rity.

With­out re­duc­ing the ca­pa­bil­ity set or by grant­ing all priv­i­leges to a con­tainer, dras­tic changes to a sys­tem and its state may be em­ployed in case of com­pro­mise. This in­cludes:

*   Load­ing cus­tom ker­nel mod­ules
*   Trac­ing ar­bi­trary processes to in­ter­fere with their pro­gram flow or to leak sen­si­tive in­for­ma­tion
*   Chang­ing the own­er­ship of ar­bi­trary files
*   Send ar­bi­trary sig­nals to processes
*   Spoof net­work pack­ets using raw sock­ets

As the list above sug­gests, re­duc­ing the ca­pa­bil­ity set via user name­spaces is cru­cial for con­tainer­iza­tion. In cases where un­trusted code is being ex­e­cuted in con­tain­ers or com­pro­mised con­tain­ers are part of the threat model, changes to the host sys­tem must not be pos­si­ble with­out ex­plic­itly al­low­ing this kind of mod­i­fi­ca­tion.

Every process in a user name­space has two files which can be used to per­form a UID and GID map­ping:

*   `/proc/<PID>/uid_map`
*   `/proc/<PID>/gid_map`

By de­fault, these files are empty. The ker­nel ex­pects lines fol­low­ing the for­mat

```
ID-inside-ns ID-outside-ns length
```

to be pre­sent in these files. The length pa­ra­me­ter is used to cre­ate a range of pos­si­ble IDs, start­ing from `ID-inside-ns` re­spec­tively `ID-outside-ns` with the max­i­mum value ac­cord­ing to the `length` field value. In case no map­ping is being per­formed for an ID, the value of `/proc/sys/kernel/overflow{u, g}id` is used. This causes an ID to be mapped to the `nobody` ID. Also, the ker­nel silently pre­vents el­e­vat­ing priv­i­leges when set-user-ID bi­na­ries are being ex­e­cuted. Please note that an ID map­ping can only be per­formed _once_ and is lim­ited to a max­i­mum of 340 lines.

Con­sider the list­ing below which helps to un­der­stand the map­ping process:

```
int run(void *) {
    while (true) {
        std::cout << "[Child] EUID/EUID "
            << geteuid()
            << "/" << geteuid()
            << std::endl;
    }
    return 0;
}

int main(int argc, char const *argv[]) {
    [...]
    childPid = clone(run, childStackTop, CLONE_NEWUSER, 0);
    std::cout << "[Parent] Child PID: " << childPid << std::endl;
    sleep(5);

    // echo "0 1000 1" >> /proc/<PID>/uid_map
    std::string cmd = "echo '0 " +
                      std::to_string(getuid()) +
                      " 1' >> /proc/" +
                      std::to_string(childPid) +
                      "/uid_map";

    system(cmd.c_str());
    while (true) { [...] }
    [...]
    return 0;
}
```

The code spawns a child process that prints its ef­fec­tive UID and GID val­ues in an end­less loop. After five sec­onds have passed, the par­ent process will per­form a UID map­ping in order to map the root user in the new name­space to the user ex­e­cut­ing this code. This pro­duces the fol­low­ing out­put:

```
[Parent] Child PID: 22338
[Child] EUID/EUID 65534/65534
[...] // five seconds pass, mapping is done
[Child] EUID/EUID 0/0
[...]
```

This pro­duces the same UID map­ping as ex­e­cut­ing `unshare -r /bin/bash` does. With the new map­ping val­ues it’s now pos­si­ble for the ker­nel to check the priv­i­leges a user pos­sesses in the name­space by map­ping the ID val­ues back to the orig­i­nal val­ues and per­form­ing per­mis­sion checks with the val­ues out­side of the name­space. One of the use-cases for this is to allow un­priv­i­leged users on the host to be priv­i­leged in a con­tainer. After some ad­di­tional con­fig­u­ra­tion, this for ex­am­ple en­ables these users to con­fig­ure parts of the con­tainer and in­stall soft­ware pack­ages. Con­tainer en­gines like Docker per­form this task by de­fault.

There exist cer­tain ad­di­tional rules for ap­ply­ing UID/GID map­pings:

*   The UID map­ping file is being owned by the user that cre­ated the name­space. Be­cause of this, only this user and `root` are able to write to this file. How­ever, there’s an ex­cep­tion as men­tioned below.
    
*   To write to the map­ping files, the ca­pa­bil­ity `CAP_SET{U, G}ID` has to be pre­sent in the con­text of the tar­get process.
    
*   If the data to be writ­ten to one of the files only con­tains a sin­gle line: The ini­tial process in a user name­space is al­lowed to map its _own_ ef­fec­tive UID/GID val­ues from within the name­space.
    
*   Oth­er­wise: Ar­bi­trary map­pings can be added by a process re­sid­ing in the par­ent name­space.
    

De­pend­ing on the name­space a process re­sides in, there exist dif­fer­ences in how the `ID-outside-ns` val­ues are being in­ter­preted. Con­sider the fig­ure below il­lus­trat­ing this sce­nario. A user with UID `1000` cre­ates two name­spaces with dif­fer­ent ID map­pings. After that, the ini­tial processes of both name­spaces read the ap­plied map­pings of each other:

![](https://www.schutzwerk.com/en/43/img/Namespaces/user-mappings.jpg)

Con­sider the ini­tial process writ­ing to its own map­ping file: To map the user iden­ti­fier `1000` to its `root` user, a name­space has to use the same con­fig­u­ra­tion as the one which name­space `1` re­ceives above. This re­sults in the `ID-outside-ns` value hav­ing to be in­ter­preted as a UID of the par­ent name­space in case two processes re­side in the same name­space.

How­ever, if they are in dif­fer­ent name­spaces, as seen above, some­thing else can be ob­served: `ID-outside-ns` is in­ter­preted as being rel­a­tive to the name­space the process that’s being read from is being placed in. Name­space `1` reads `200:0:1` from name­space `2` – this means that the UID `0` in name­space `1` cor­re­sponds to the ID `200` of name­space `2` and both orig­i­nate from the user iden­ti­fier `1000`. The same ap­plies to name­space `2` the other way around.

It’s pos­si­ble to com­bine user name­spaces with other name­space types. This en­ables users to cre­ate name­spaces that would re­quire `CAP_SYS_ADMIN` with­out being priv­i­leged. To ac­com­plish this, one can use `clone` and a com­bi­na­tion of `CLONE_NEWUSER` and other `clone` flags. The ker­nel processes the flag to cre­ate a new user name­space first and processes the re­main­ing `CLONE_NEW*` flags in­side the new user name­space.

Being priv­i­leged in a user name­space does not imply su­pe­ruser ac­cess on the whole sys­tem. Nev­er­the­less, the abil­ity to per­form ac­tions an un­priv­i­leged user would not be able to ex­e­cute with­out user name­spaces also broad­ens the at­tack sur­face. For ex­am­ple un­priv­i­leged users are able to ex­e­cute cer­tain mount op­er­a­tions that can be the tar­get of ker­nel ex­ploits \[3\]. There may re­main more po­ten­tial se­cu­rity is­sues re­gard­ing user name­spaces to be un­cov­ered in the fu­ture. For ex­am­ple, it was pre­vi­ously dis­cov­ered that the com­bi­na­tion of user name­spaces and the `CLONE_FS` flag can lead to a priv­i­lege es­ca­la­tion issue \[4\]. More re­cently it was dis­cov­ered that when the limit for the num­ber UID/GID map­pings a name­space can have was in­creased from 5 to 340, a se­cu­rity issue \[5\] was in­tro­duced: When switch­ing to dif­fer­ent data struc­tures to store the map­pings in the ker­nel once the num­ber of map­pings ex­ceeds five, this data is being ac­cessed in a wrong way. This re­sults in processes of nested user name­space being able to ac­cess files being owned by other name­spaces, e.g. `/etc/shadow` of the ini­tial user name­space.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [Name­spaces Ker­nel View and Usage in Con­tainer­iza­tion](https://www.schutzwerk.com/en/43/posts/namespaces_05_kernel/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Name­spaces in op­er­a­tion, part 5: User name­spaces](https://lwn.net/Articles/532593/)
*   [2 - OCI de­faults.go](https://github.com/moby/moby/blob/master/oci/defaults.go)
*   [3 - ker­nel: tmpfs use-af­ter-free](https://lwn.net/Articles/540083/)
*   [4 - Anatomy of a user name­spaces vul­ner­a­bil­ity](https://lwn.net/Articles/543273/)
*   [5 - Linux: bro­ken uid/gid map­ping for nested user name­spaces with >5 ranges](https://bugs.chromium.org/p/project-zero/issues/detail?id=1712)
*   [Title Image by Tom Fisk / Pex­els (cropped)](https://www.pexels.com/photo/aerial-view-of-cargo-ship-1554646/)

