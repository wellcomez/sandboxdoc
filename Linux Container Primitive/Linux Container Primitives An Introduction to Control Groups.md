---
title: "Linux Container Primitives An Introduction to Control Groups"
layout: post
---
 [www.schutzwerk.com /en/43/posts/cgroups\_01\_intro/](https://www.schutzwerk.com/en/43/posts/cgroups_01_intro/)

# Linux Container Primitives: An Introduction to Control Groups

SCHUTZWERK GmbH 10-13 minutes


* * *

## Con­trol Groups

The goal of _cgroups_ is to en­able fine-grained con­trol over re­sources con­sumed by processes ad­di­tion­ally to re­source mon­i­tor­ing. Be­fore this Linux ker­nel fea­ture was avail­able, other mech­a­nisms such as `nice` or `setrlimit` had to be used to repli­cate a sub­set of the fea­tures that are being of­fered di­rectly by today’s ker­nels. How­ever, with­out the abil­ity to group processes and re­store pre­vi­ously ap­plied set­tings this was not as con­ve­nient as using con­trol groups is today. Next to name­spaces this ker­nel fea­ture is one of the main prim­i­tives that are being used to build con­tainer en­vi­ron­ments. Lim­it­ing and mon­i­tor­ing of con­tain­ers with the con­trol groups de­scribed in this sec­tion can be ap­plied to processes as well as con­tain­ers.

By or­ga­niz­ing con­trol groups in a vir­tual filesys­tem called `cgroupfs`, tak­ing ad­van­tage of the hi­er­ar­chi­cal struc­ture of con­trol groups in im­ple­men­ta­tions be­comes pos­si­ble. Con­trol groups are cre­ated, deleted and mod­i­fied by al­ter­ing the struc­ture and files of this filesys­tem. Also, sub-groups are pos­si­ble that allow the in­her­i­tance of _cgroup_ at­trib­utes. This im­plies that lim­its in par­ent groups can also apply in child groups. It’s also pos­si­ble that this af­fects child processes - for ex­am­ple a `fork` call can cause the newly cre­ated process to be af­fected by the same lim­its as the par­ent. Please note that, de­pend­ing on the type of con­trol group that’s in use, these as­pects may or may not be ap­plic­a­ble.

Con­sider the fol­low­ing use-case: On Ubuntu `systemd` pre­vents sys­tems from being crashed by fork bombs by au­to­mat­i­cally cre­at­ing a de­fault con­trol group for each user. This be­hav­ior re­sults from the ef­fect of the `.slice` sub-group that’s lim­it­ing the num­ber of processes a sin­gle user may cre­ate, pre­vent­ing users from spawn­ing processes in an in­fi­nite loop. By ex­e­cut­ing `systemctl status user-$UID.slice` the cur­rent limit is shown.

Spe­cific parts of the of­fered con­trol group con­fig­u­ra­tion, like the limit de­scribed above, are con­fig­urable in a gran­u­lar way using _con­trollers_. Con­troller types are also called _sub­sys­tems_ of _cgroups_ and con­trol the as­pect of re­source usage that may be lim­ited or mon­i­tored. Through­out this chap­ter, var­i­ous con­troller types will be dis­cussed. The ker­nel code re­gard­ing _cgroups_ is re­spon­si­ble for group­ing processes whereas the in­di­vid­ual con­troller im­ple­men­ta­tions takes care of the re­source mon­i­tor­ing and lim­it­ing func­tion­al­ity it­self.

Mod­i­fi­ca­tions to con­trol groups can be ap­plied once the vir­tual filesys­tem of the con­troller to use is mounted. One may choose to mount a sin­gle con­troller type or mount every avail­able type at once. In any case, mount­ing con­trollers usu­ally takes place in `/sys/fs/cgroup` where at least one folder for each con­troller is being cre­ated by call­ing `mount`. It’s im­por­tant to note that it’s not pos­si­ble to mount an al­ready mounted con­troller to a dif­fer­ent lo­ca­tion with­out also mount­ing all other con­trollers that are al­ready mounted to pre­vent am­bi­gu­ity. The fol­low­ing fig­ure il­lus­trates the mount­ing pro­ce­dure \[1\]:

![Control Group Hierarchy](https://www.schutzwerk.com/en/43/img/ControlGroups/cg_hier.jpg)

Ini­tially, all processes be­long to the root con­trol group. Cre­at­ing a new _cgroup_ is being ac­com­plished by cre­at­ing a di­rec­tory for the con­trol group under `/sys/fs/cgroup/<controller type>/<cgroup name>`. To con­fig­ure the newly cre­ated _cgroup_, two tasks have to be per­formed:

1.  Adding _cgroup_ con­fig­u­ra­tion by cre­at­ing an at­tribute file with de­sired val­ues. For ex­am­ple, to set a mem­ory limit of 100MB for all processes in the con­trol group `mem`, the file `/sys/fs/cgroup/memory/mem/memory.limit_in_bytes` has to be cre­ated with con­tent `100000000` \[2\]. For each con­troller, there exist mul­ti­ple pos­si­ble at­tribute files that can be used to con­fig­ure a con­trol group.
    
2.  Mov­ing a process to a con­trol group by writ­ing the tar­get PID to `/sys/fs/cgroup/memory/mem/cgroup.procs`.
    

The `cgroup.procs` file men­tioned above may be uti­lized by any process with write ac­cess to the re­spec­tive file to move an­other process into a con­trol group. How­ever, when a PID is writ­ten to the file, an ad­di­tional check is being per­formed to de­ter­mine whether the writ­ing user is priv­i­leged or the same user that’s the owner of the process that’s about to be moved.

Con­trol groups can also be nested by cre­at­ing sub-di­rec­to­ries under al­ready ex­ist­ing _cgroup_ fold­ers. It should be noted that changes made to processes and the con­trol group con­fig­u­ra­tion are _not_ per­sis­tent. After re­boot­ing a ma­chine, the con­fig­u­ra­tion will be lost in case `cgconfig` or sim­i­lar mech­a­nisms have not been used to make the changes per­sis­tent. Per­sis­tence is achieved by re-ap­ply­ing con­trol group con­fig­u­ra­tions upon startup. Re-map­ping processes of a spe­cific user to a con­trol group works by using the dae­mon `cgrulesengd` in com­bi­na­tion with a rule set. To re­move a _cgroup_, the as­so­ci­ated di­rec­tory has to be re­moved which ren­ders the con­trol group in­vis­i­ble. As soon as all processes of the _cgroup_ ter­mi­nate or are con­sid­ered zom­bies, the con­trol group will fi­nally be re­moved by the ker­nel.

### Two cgroup ver­sions

Due to the way con­trol groups were orig­i­nally im­ple­mented, many in­con­sis­ten­cies were in­tro­duced and the code was rather com­plex in re­gard to the of­fered fea­tures \[3\]. Un­for­tu­nately, the de­vel­op­ment of _cgroups_ was not co­or­di­nated in an op­ti­mal man­ner which lead to the prob­lem­atic first ver­sion of con­trol groups. Since Linux `4.5`, the _cgroups_ `v2` im­ple­men­ta­tion is avail­able with the in­tent to re­place the orig­i­nal `v1` im­ple­men­ta­tion. How­ever, as of now not all re­source con­trollers of the orig­i­nal im­ple­men­ta­tion are avail­able in the newer ver­sion. Be­cause of this, a par­al­lel usage of both ver­sions has to be taken into ac­count in case spe­cific `v1` fea­tures are re­quired. Please note that a sin­gle con­troller can not be used in both `v1` and `v2` si­mul­ta­ne­ously. Some major dif­fer­ences be­tween `v1` and `v2` are listed in the table below:

v1

v2

Mul­ti­ple cgroup hi­er­ar­chies pos­si­ble.

Only a sin­gle, uni­fied hi­er­ar­chy is al­lowed.

Dis­tin­guishes threads and processes. Threads of a sin­gle process can be mapped into dif­fer­ent cgroups.

No con­trol of sin­gle threads. There­fore no `tasks` file. Ex­cep­tion: Thread mode (see below).

Processes can be mapped to cgroups dis­re­gard­ing whether a group is a leaf node of the hi­er­ar­chy or not.

No in­ter­nal processes rule for non-root cgroups: A group can not have con­trollers en­abled and have child groups and processes mapped to it at the same time.

The re­lease\_a­gent can be used in order to get no­ti­fied in case a group gets empty.

The `events` file can be used for the same pur­pose.

Im­ple­ments own mount op­tions.

Dep­re­cates all `v1` mount op­tions.

Uses `/proc/cgroups` to keep track of all con­trollers sup­ported by the ker­nel.

Uses the `cgroup.controllers` file for the same pur­pose.

Ad­di­tion­ally to the _cgroups_ file men­tioned above, the fol­low­ing files are pre­sent to or­ga­nize the con­trol group func­tion­al­ity:

*   `/sys/kernel/cgroup/delegate`: Since Linux `4.15` it’s pos­si­ble to del­e­gate the man­age­ment of a con­trol group to an un­priv­i­leged user. This file lists all _cgroup_ ver­sion 2 files that the del­e­ga­tee should own after a del­e­ga­tion process.
    
*   `/sys/kernel/cgroup/features`: This file en­ables user-space ap­pli­ca­tions to list all con­trol group fea­tures the ker­nel sup­ports.
    
*   `/proc/<PID>/cgroup`: This con­tains in­for­ma­tion on all _cgroups_ a process re­sides in.
    

#### Thread Mode

The ini­tial con­trol group im­ple­men­ta­tion al­lows dis­trib­ut­ing the threads of a process into in­di­vid­ual con­trol groups. At first, this seemed to pro­vide a max­i­mum of flex­i­bil­ity. How­ever, there are cases where this re­sulted in prob­lems: Threads of a sin­gle process are run­ning in the same mem­ory space. When lim­it­ing the avail­able mem­ory of one thread, this also af­fects all other threads that may have other mem­ory re­stric­tions im­posed by other _cgroup_ mem­ber­ships. Un­de­fined be­hav­ior re­sults as mul­ti­ple set­tings are ac­tive at the same time for the threads of a sin­gle process. Also, when mov­ing a process into a con­trol group after start­ing it, it’s often nec­es­sary to move all child threads too. A helper util­ity for this task is `cgexec` which au­to­mat­i­cally moves all threads of the cre­ated process in the cor­rect groups.

After re­mov­ing the abil­ity to man­age con­trol groups on a per-thread basis in `v2`, some im­por­tant use-cases could not be ful­filled any­more. This in­cludes the abil­ity of the `cpu` con­troller to man­age threads. Be­cause of that, Linux `4.14` added the _thread mode_ to relax the re­stric­tions im­posed by the tran­si­tion to `v2`.

This new mode al­lows to use _threaded sub-trees_, en­abling threads of mul­ti­ple processes to be mapped into dif­fer­ent con­trol groups within such a sub-tree. There are now two types of re­source con­trollers:

*   Do­main: Con­trollers of this type are not thread aware, mean­ing that they can only work on process level. All threads of a process have to re­side in the same con­trol group. This con­troller type can not be en­abled in threaded sub-trees.
*   Threaded: This is the new con­troller type that al­lows con­trol group man­age­ment on a thread-level. It can be used in com­bi­na­tion with threaded sub-trees.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [The Net­work and Block I/O Con­trollers](https://www.schutzwerk.com/en/43/posts/control_groups_02_network_block_io/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Linux Con­tain­ers: Basic Con­cepts](https://www.cl.cam.ac.uk/~lc525/files/Linux_Containers.pdf)
*   [2 - cgroups (Arch Wiki)](https://wiki.archlinux.org/index.php/cgroups)
*   [3 - Linux con­trol groups](http://man7.org/linux/man-pages/man7/cgroups.7.html)
*   Title Image by [AdamLowly](https://www.pexels.com/@adamlowly) from [Pex­els](https://www.pexels.com/photo/no-trespassing-sign-5042833/) (Cropped)
