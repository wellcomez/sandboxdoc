---
title: "Linux Container Primitives Mount Namespaces and Information Leaks"
layout: post
---
 [www.schutzwerk.com /en/43/posts/namespaces\_02\_mnt/](https://www.schutzwerk.com/en/43/posts/namespaces_02_mnt/)

# Linux Container Primitives: Mount Namespaces and Information Leaks

SCHUTZWERK GmbH 10-12 minutes

* * *

In the [pre­vi­ous part](https://www.schutzwerk.com/en/43/posts/namespaces_01_intro/) of the Linux Con­tainer Prim­i­tive se­ries, basic in­for­ma­tion re­gard­ing name­spaces were cov­ered. This post dis­cusses the mount name­space type and its usage in de­tail. Also, an in­for­ma­tion leak that’s re­lated to the usage of mount name­spaces in Docker is de­scribed. The fol­low­ing list shows the top­ics of all blog posts of the se­ries.

* * *

## Mount Name­spaces

This name­space type was the first to be added to the Linux ker­nel `2.4.19` in 2002 \[1\]. The goal of mount name­spaces is to re­strict the view of the global file hi­er­ar­chy by pro­vid­ing each name­space with its own set of mount points.

A newly cre­ated name­space ini­tially uses a copy of the par­ent’s mount tree. To add and re­move mount points, the `mount` and `umount` com­mands are avail­able. The im­ple­men­ta­tion of these com­mands had to be mod­i­fied in order to be aware of name­spaces and work in com­bi­na­tion with mount name­spaces.

The ini­tial im­ple­men­ta­tion of mount name­spaces caused a us­abil­ity issue that ul­ti­mately re­duced the ef­fi­ciency of these name­spaces: To make a de­vice avail­able for all or a sub­set of all name­spaces it was re­quired to ex­e­cute one `mount` op­er­a­tion for each name­space. How­ever, the op­ti­mal ap­proach would re­quire only a sin­gle mount op­er­a­tion to per­form the same task. Ad­di­tion­ally, a so­lu­tion to man­age mount­points in all name­spaces at once would pro­vide more con­ve­nience. For these rea­sons, _shared sub-trees_ have been in­tro­duced \[2\].

The basic idea of shared sub-trees is that a _prop­a­ga­tion type_ is as­so­ci­ated with each mount point. This con­fig­ures how each `mount` op­er­a­tion within a mount point will be prop­a­gated to other re­lated mount points. In­ter­nally the ker­nel uses _peer groups_ in order to de­ter­mine whether a `mount` event gets prop­a­gated to a spe­cific mount point. A peer group con­sists of a set of mount points that share mount events with each other. Mount points get added to a peer group in case the spe­cific _shared_ mount point is being repli­cated by join­ing a new mount name­space or when bind-mount­ing a mount point. Peer group mem­bers are being re­moved in case an un­mount op­er­a­tion is being is­sued or as soon as a mount name­space gets de­stroyed.

The fol­low­ing prop­a­ga­tion types are avail­able:

*   `MS_SHARED`: This shares the mount with all mount points re­sid­ing in the same peer group. Mount op­er­a­tions on a peer’s mount will also be prop­a­gated to the orig­i­nal mount.
*   `MS_PRIVATE`: No mount events are being prop­a­gated and no events are being re­ceived. Other mount name­spaces will not be able to ac­cess the mount point.
*   `MS_SLAVE`: Mounts of this type only re­ceive events. They do not share events and ef­fec­tively pro­vide pri­vate mounts for mounts cre­ated using this prop­a­ga­tion type.
*   `MS_UNBINDABLE`: This type is sim­i­lar to the pri­vate type with an ad­di­tion: Mounts of this type can not be used to cre­ate bind mounts. An ex­am­ple usage of this type can be found below.

The types can be pre­fixed with an `r` to make its ef­fect re­cur­sive for all child mounts of a mount point.

Con­sider the fol­low­ing sce­nario \[1\]: Two users, namely Alice and Bob, are shar­ing parts of the same file hi­er­ar­chy and are being placed in their own mount name­spaces. They are then being pro­vided with their own view of the sys­tem di­rec­to­ries. To per­form this task, the sys­tem’s di­rec­to­ries have been bind-mounted into the user spe­cific di­rec­to­ries of the sub-tree, as shown on the left fig­ure \[1\] below. Note that the fold­ers of Alice and Bob are repli­cated in the bind-mounted sub-tree di­rec­to­ries which is not de­sired.

![](https://www.schutzwerk.com/en/43/img/Namespaces/subtrees-bm.png)

By using the `MS_UNBINDABLE` flag this is pre­vented, ef­fec­tively cre­at­ing the hi­er­ar­chy as can bee seen on the sec­ond fig­ure below \[1\]:

![](https://www.schutzwerk.com/en/43/img/Namespaces/subtrees-ub.png)

To make use of the ad­van­tages pro­vided by shared sub-trees, a `mount` now has to be shared with both users. As can be seen on the first fig­ure below, a data medium was in­serted. How­ever, it’s only pre­sent out­side of the mount name­spaces of Alice and Bob.

![](https://www.schutzwerk.com/en/43/img/Namespaces/subtrees-mnt.png)

By ex­e­cut­ing a `mount` com­mand with the `--make-shared` flag, the mount of the data medium is now also pre­sent in the mount name­spaces of both users:

![](https://www.schutzwerk.com/en/43/img/Namespaces/subtrees-mnt-share.png)

More­over, every mount op­er­a­tion ex­e­cuted in the `/mnt` di­rec­tory will now au­to­mat­i­cally be prop­a­gated to the mount name­spaces of Alice and Bob.

This ap­proach also en­ables Alice and Bob to have pri­vate mounts that are not being shared with other users. This can be achieved by ei­ther mak­ing the sub-tree mounts of the users `slaves` or by cre­at­ing pri­vate mounts in­side of the re­spec­tive user di­rec­to­ries.

The mount points avail­able for a spe­cific process of a name­space can be found along with their prop­a­ga­tion types in the `/proc/self/mountinfo` file.

By de­fault Docker uses the pri­vate prop­a­ga­tion type when mount­ing di­rec­to­ries with the `-v` pa­ra­me­ter. This can be ver­i­fied by ex­am­in­ing the out­put of `docker inspect <Containername>`:

```
[...]
"Mounts": [
    {
        "Type": "bind",
        "Source": "/tmp/test",
        "Destination": "/tmp/test",
        "Mode": "",
        "RW": true,
        "Propagation": "rprivate"
    }
],
[...]
```

This iso­lates the mount points of both the host and a con­tainer and no mount events are being prop­a­gated be­tween a con­tainer and the host. There­fore mount points pre­sent on the host are not avail­able for con­tainer­ized processes and the other way around be­cause sep­a­rate mount name­spaces are in place.

Con­sider shar­ing a host di­rec­tory with a con­tainer after start­ing an ini­tial process in it: In this sce­nario mount­ing takes place across two mount name­spaces - the host name­space and the re­spec­tive con­tainer name­space. The `nsenter` util­ity as­sists when per­form­ing this task by using `setns` to join the con­tainer’s mount name­space in order to allow cre­at­ing mount points in the con­tainer. Be­cause the ca­pa­bil­i­ties in the host’s mount name­space are being pre­served, a folder gov­erned by the host can there­fore be mounted into the con­tainer, ef­fec­tively shar­ing it be­tween the host and the con­tainer.

For con­tain­ers, host paths can also be masked to pro­vide each con­tainer with its own ver­sion of a spe­cific folder. For ex­am­ple by mask­ing the `/proc/acpi` it’s not pos­si­ble for a con­tainer­ized process to en­able host in­ter­faces like the Blue­tooth de­vice. Is­sues can be pre­sent in case no proper mask­ing is taken into ac­count \[3\]. The Docker plat­form au­to­mat­i­cally masks cer­tain paths under `/proc` and `/sys` to counter these po­ten­tial is­sues.

### Dis­cov­er­ing an In­for­ma­tion Leak in Docker

Dur­ing an as­sess­ment of the Docker plat­form, two in­for­ma­tion leaks re­gard­ing the `/proc/asound` path were dis­cov­ered in the as­so­ci­ated OCI con­tainer spec­i­fi­ca­tion:

#### Leak of audio de­vice sta­tus of the host

When media is being played on the host, the `/proc/asound/card*/pcm*p/sub*/status` files may con­tain in­for­ma­tion re­gard­ing the sta­tus of media play­back. Con­sider this com­mand for a demon­stra­tion:

```
docker run --rm \
    ubuntu:latest bash -c \
        "sleep 5; \
        cat /proc/asound/card*/pcm*p/sub*/status | \
        grep state | \
        cut -d ' ' -f2 | \
        grep RUNNING || echo 'not running'"
```

When play­ing a media file, watch­ing a video or ex­e­cut­ing sim­i­lar ac­tions in­volv­ing sound out­put on the host, the com­mand above demon­strates that a con­tainer­ized process is able to gather in­for­ma­tion on this sta­tus. There­fore a process in a con­tainer is able to check whether and what kind of user ac­tiv­ity is pre­sent on the host sys­tem. Also, this may in­di­cate whether a con­tainer runs on a desk­top sys­tem or a server as media play­back rarely hap­pens on server sys­tems.

The sce­nario de­scribed above is in re­gard to media play­back. More­over, when ex­am­in­ing the `/proc/asound/card*/pcm*c/sub*/status` files (`pcm*c` in­stead of `pcm*p`) this can also leak in­for­ma­tion re­gard­ing cap­tur­ing sound, as in record­ing audio or mak­ing calls on the host sys­tem.

#### Leak of Mon­i­tor Type

Mon­i­tors can also act as sound out­put de­vices when con­nected using a com­pat­i­ble in­ter­face like Dis­play­Port. The list­ing below il­lus­trates how the model type of the con­nected mon­i­tors can be read from within a con­tainer:

```
docker run --rm \
    ubuntu:latest bash -c \
        "cat /proc/asound/card*/eld* | \
        grep monitor_name"

monitor_name SMS24A650 # (A Samsung monitor)
monitor_name SMS24A650
```

This in­for­ma­tion should not be ac­ces­si­ble from within a con­tainer as it con­tains spe­cific in­for­ma­tion on the host and its en­vi­ron­ment.

These is­sues have been re­ported \[4\] to the Docker main­tain­ers and are now fixed in the up­stream ver­sion by adding `/proc/asound` to the list of paths that are being masked to make each con­tainer man­age its own ver­sions of the af­fected paths. This path list is part of the OCI spec­i­fi­ca­tion - there­fore the fix for these is­sues also prop­a­gates to `containerd` which also uses the OCI spec­i­fi­ca­tion.

## Next post in se­ries

*   Con­tinue read­ing the next ar­ti­cle in this se­ries [“The PID and Net­work Name­spaces”](https://www.schutzwerk.com/en/43/posts/namespaces_03_pid_net/)  
    Fol­low us on [Twit­ter](https://twitter.com/Schutzwerk), [LinkedIn](https://www.linkedin.com/company/schutzwerk/), [Xing](https://www.xing.com/companies/schutzwerkgmbh/updates) to stay up-to-date.

## Cred­its

_Cred­its: The elab­o­ra­tion and soft­ware pro­ject as­so­ci­ated to this sub­ject are re­sults of a Mas­ter’s the­sis cre­ated at SCHUTZW­ERK in col­lab­o­ra­tion with Aalen Uni­ver­sity by Philipp Schmied._

## Ref­er­ences

*   [1 - Mount name­spaces and shared sub­trees](https://lwn.net/Articles/689856/)
*   [2 - Shared sub­trees](https://lwn.net/Articles/159077/)
*   [3 - Moby Pull Re­quest #37404](https://github.com/moby/moby/pull/37404)
*   [4 - Minor In­for­ma­tion Leaks in /proc/asound](https://github.com/moby/moby/issues/38285)
*   [Title Image](https://www.pexels.com/de-de/foto/bunt-business-container-einfuhren-163726/)
