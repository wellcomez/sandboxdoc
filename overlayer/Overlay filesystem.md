---
date: 2023-02-07 13:26
title: Overlay filesystem
tags:
- overlay
---



# Overlay filesystem
https://wiki.archlinux.org/title/Overlay_filesystem

-   [Page](https://wiki.archlinux.org/title/Overlay_filesystem "View the content page [ctrl-option-c]")
-   [Discussion](https://wiki.archlinux.org/index.php?title=Talk:Overlay_filesystem&action=edit&redlink=1 "Discussion about the content page (page does not exist) [ctrl-option-t]")

-   [Read](https://wiki.archlinux.org/title/Overlay_filesystem)
-   [View source](https://wiki.archlinux.org/index.php?title=Overlay_filesystem&action=edit "This page is protected.
    You can view its source [ctrl-option-e]")
-   [View history](https://wiki.archlinux.org/index.php?title=Overlay_filesystem&action=history "Past revisions of this page [ctrl-option-h]")

From [the initial kernel commit](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=e9be9d5e76e34872f0c37d72e25bc27fe9e2c54c):

Overlayfs allows one, usually read-write, directory tree to be overlaid onto another, read-only directory tree. All modifications go to the upper, writable layer. This type of mechanism is most often used for live CDs but there is a wide variety of other uses.

The implementation differs from other "union filesystem" implementations in that after a file is opened all operations go directly to the underlying, lower or upper, filesystems. This simplifies the implementation and allows native performance in these cases.

Overlayfs has been in the Linux kernel since 3.18.

## Installation

Overlayfs is enabled in the default kernel and the `overlay` module is automatically loaded upon issuing a mount command.

## Usage

To mount an overlay use the following `mount` options:

~~~
# mount -t overlay overlay -o lowerdir=_/lower_,upperdir=_/upper_,workdir=_/work_ _/merged_

~~~
**Note:** The working directory (`workdir`) needs to be an empty directory on the same filesystem as the upper directory.

-   The lower directory can be read-only or could be an overlay itself.
-   The upper directory is normally writable.
-   The workdir is used to prepare files as they are switched between the layers.

The lower directory can actually be a list of directories separated by `:`, all changes in the `merged` directory are still reflected in `upper`.

Example:
~~~
# mount -t overlay overlay -o lowerdir=_/lower1:/lower2:/lower3_,upperdir=_/upper_,workdir=_/work_ _/merged_
~~~

**Note:** The order of lower directories is the rightmost is the lowest, thus the upper directory is on top of the first directory in the left-to-right list of lower directories; NOT on top of the last directory in the list, as the order might seem to suggest.

The above example will have the order:

~~~
/upper
/lower1 
/lower2
/lower3
~~~

To add an overlayfs entry to `/etc/fstab` use the following format:

~~~
/etc/fstab

overlay _/merged_ overlay noauto,x-systemd.automount,lowerdir=_/lower_,upperdir=_/upper_,workdir=_/work_ 0 0

The `noauto` and `x-systemd.automount` mount options are necessary to prevent systemd from hanging on boot because it failed to mount the overlay. The overlay is now mounted whenever it is first accessed and requests are buffered until it is ready. See [fstab#Automount with systemd](https://wiki.archlinux.org/title/Fstab#Automount_with_systemd "Fstab").

~~~
### Read-only overlay

Sometimes, it is only desired to create a read-only view of the combination of two or more directories. In that case, it can be created in an easier manner, as the directories `upper` and `work` are **not** required:

~~~
# mount -t overlay overlay -o lowerdir=_/lower1:/lower2_ _/merged_
~~~

When `upperdir` is not specified, the overlay is automatically mounted as read-only.

## See also

-   [Overlay Filesystem documentation](https://docs.kernel.org/filesystems/overlayfs.html)
-   [Explaining overlayfs - What it does and how it works](https://www.datalight.com/blog/2016/01/27/explaining-overlayfs-%E2%80%93-what-it-does-and-how-it-works)
-   [Wikipedia:OverlayFS](https://en.wikipedia.org/wiki/OverlayFS "wikipedia:OverlayFS")