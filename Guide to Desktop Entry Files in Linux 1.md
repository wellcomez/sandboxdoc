---
date: 2023-03-28 15:07
title: Guide to Desktop Entry Files in Linux
tags:
- desktop-icon
---



# Guide to Desktop Entry Files in Linux

Last modified: August 20, 2021

Written by: [Hiks Gerganov](https://www.baeldung.com/linux/author/hiksgerganov "Posts by Hiks Gerganov")

-   [Files](https://www.baeldung.com/linux/category/files)

If you have a few years of experience in the Linux ecosystem, and you’re interested in sharing that experience with the community, have a look at our [**Contribution Guidelines**](https://www.baeldung.com/linux/contribution-guidelines).

## 1. Overview

**Desktop entry files are a standard way for creating and ordering graphical user interface (GUI) shortcuts**. Most major Linux desktop environments support them. Furthermore, [GNOME](https://www.gnome.org/), [KDE](https://kde.org/), and [Xfce](https://www.xfce.org/) all follow the [XDG Desktop Entry Specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html), a de facto standard.

In this guide, we’ll discuss what desktop entry files are, why they are useful, and how they function. We’ll use the expressions desktop entry files, _.desktop_ files, and entry files interchangeably.

The code in the guide has been tested on Debian 10.10 (Buster) with GNU Bash 5.0.3 and Xfce 4.16. It is POSIX-compliant and should work in any such environment.

## 2. Description

Desktop entry files contain only text. Although commonly referred to as _.desktop_ files, they do not need an extension. For easy classification, however, they can also have the _.desktop_ or _.directory_ extensions. The standard naming convention for entry files is reverse DNS. For example, an acceptable name can be _org.example.AppName.desktop_.

**To function correctly, desktop entry files must be [executable](https://www.baeldung.com/linux/chown-chmod-permissions#executable)**. Users can set this permission via _[chmod](https://man7.org/linux/man-pages/man1/chmod.1.html)_ in Linux environments.

Entry files can describe **three element types**:

-   shortcut to application (_.desktop_ extension)
-   link to resource (_.desktop_ extension)
-   submenu (_.directory_ extension)

These elements function as quick visual access points. **Each one is usually manifested as a GUI entry on the desktop or system menu**. Where the entry is located depends on the location of the desktop entry file.

## 3. Location

Like normal files, the user can execute _.desktop_ files where they are created. One of their powers, however, lies in their connection with the GUI.

**A directory entry file is best placed on the desktop or in _$XDG_DATA_DIRS/applications_**. If the _XDG_DATA_DIRS_ [environment variable](https://www.baeldung.com/linux/environment-variables) is not set, defaults are:

-   _/usr/share_ (global)
-   _~/.local/share_ (user-specific)

**If we place a _.desktop_ file on the desktop, we are creating a shortcut icon**. Similarly, placing a _.desktop_ file in _$XDG_DATA_DIRS/applications_ adds a GUI menu icon for that shortcut. In addition, we use _.directory_ files to create system GUI submenus.

Each of these elements is customizable via attributes (such as name and icon). The attribute values exist in a specific format within the files themselves.

## 4. Format

**Desktop entry files consist of case-sensitive [UTF-8](https://man7.org/linux/man-pages/man7/utf-8.7.html) encoded lines** in three categories:

-   comments
-   group headers
-   key-value pairs

Comments are blank lines or lines starting with the _#_ character:

```shell
# This is a comment
```

**Group header lines denote the start of a group** and are formatted like:

```shell
[Group Name]
```

We enclose the group name in square brackets. It cannot contain control characters or other square brackets. **The only required group header is _[Desktop Entry]_**. This should be the first non-comment line in the desktop entry file. Group names must be unique within a desktop entry file.

**We format key-value pair lines as**:

```shell
Key=Value
```

Key names are unique per group and can consist of alphanumeric characters and dashes. Keys define attributes of the desktop entry file or its associated GUI element.

The full list of standard supported keys is in the specifications. For example, here are some commonly used ones:

-   _Version_ – Version of the used Desktop Entry Specification
-   _Comment_ – used as the tooltip for the GUI entry or icon
-   _TryExec_ – path to an executable that confirms the program in _Exec_ is present
-   _Path_ – current working directory
-   _Actions_ – application actions, different from the default, defined with special group headers

The complexity of desktop entry files can vary. Because of this, standardized tools such as _[desktop-file-utils](https://www.freedesktop.org/wiki/Software/desktop-file-utils/)_ exist. They can conveniently validate and install _.desktop_ files per the specification.

To illustrate the mechanism, we’ll go through a simple example.

## 5. Usage

Suppose we want a desktop shortcut for GNOME’s _[gedit](https://wiki.gnome.org/Apps/Gedit)_ application. Specifically, we want _gedit_ to start with a given file (_/home/user/file_) already open.

### 5.1. File Creation

Towards this end, let’s create a simple text file:

```shell
[Desktop Entry]
Type=Application
Name=Personal File
Exec=gedit /home/user/file
Icon=gedit
```

We name this file _com.baeldung.starter.GeditPersonal.desktop_ and place it on the desktop. **As a consequence, a new desktop icon with the name “Personal File” and the icon of _gedit_ appears**. Finally, we make _com.baeldung.starter.GeditPersonal.desktop_ executable:

```shell
$ chmod +x /home/user/Desktop/com.baeldung.starter.GeditPersonal.desktop
```

Now the user can click the desktop icon to open _/home/user/file_ with _gedit_. Let’s explore how we achieved this.

### 5.2. Explanation

**Initially, we start off with the group header _[Desktop Entry]_**. It allows desktop entry file ([_magic_](https://linux.die.net/man/5/magic)) detection even without an extension. Furthermore, _[Desktop Entry]_ normally contains the main portion of the desktop entry file. Under the group header, we list our key-value pairs.

First, we **define the category of desktop entry file via the required _Type_ key**. It can have the values (_1_) _Application_, (_2_) _Link_, or (_3_) _Directory_. For instance, we define shortcuts to URLs via the _Link_ type. On the other hand, the _Directory_ type defines a system GUI submenu.

Additionally, _Application_ desktop entry files like ours usually have an _Exec_ key. **The _Exec_ value contains the command line for execution via the shortcut**.

Since applications can take arguments, desktop entries should also have a mechanism to do so. In short, this is done via specifiers in the _Exec_ value such as:

-   _%f_ single filename
-   _%u_ single URL
-   _%k_ URI or local filename of the location of the desktop file

For example, the following format allows us to pass a file directly to the desktop entry file:

```shell
Exec=cat %f
```

We do this via drag-and-drop or the command line. The argument we supply will follow the [_cat_](https://man7.org/linux/man-pages/man1/cat.1.html) (concatenate) command. We can use the _%F_, _%u_, and _%U_ specifiers similarly.

In contrast, the _%k_ specifier provides a way for us to use the location of the desktop file itself in the _Exec_ command line.

Finally, an optional _Icon_ key defines the icon. The _Icon_ value can be a path to one of several supported image formats or an executable. More complex cases are discussed in the [Icon Theme Specification](https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html) standard.

**We supply a descriptive name via the required _Name_ key**. This name visually replaces the default filename of the desktop entry file.

In addition, an important feature of the _Name_ value is localization.

## 6. Localization

We can localize some values in desktop entry files. This allows us to display certain translations in certain linguistic environments.

The localization format follows the pattern:

```shell
Key[lang_COUNTRY.ENCODING@MODIFIER]=Value
```

**Here’s an example of a localized _Name_ alternative in its simplest form**:

```shell
Name=Personal File
Name[bg]=Личен Файл
```

Translations depend on the [_LC_MESSAGES_](https://www.baeldung.com/linux/locale-environment-variables) environment variable. In this example, if it includes _bg_ (Bulgarian), _Name_ will be “Личен файл”.

Using modifiers, we can achieve more precise control. Here’s another example that specifies the country, encoding, and modifier:

```java
Name=Financial Sheets
Name[bg_BG.UTF-8@Iztok]=Финансови Отчети
```

This locale will match _LC_MESSAGES_ in the following order:

-   _lang_COUNTRY@MODIFIER_
-   _lang_COUNTRY_
-   _lang@MODIFIER_
-   _lang_
-   default value

The _ENCODING_ does not take part in the matching.

## 7. Summary

In this guide, we discussed desktop entry files. First, we defined what they are and where they’re located. Next, we explained their format and usage. Lastly, we discussed an important feature of desktop entry files – localization.

If you have a few years of experience in the Linux ecosystem, and you’re interested in sharing that experience with the community, have a look at our [**Contribution Guidelines**](https://www.baeldung.com/linux/contribution-guidelines).

Comments are closed on this article!

![The Baeldung logo](https://www.baeldung.com/linux/wp-content/themes/baeldung/icon/logo.svg)

#### CATEGORIES

-   [ADMINISTRATION](https://www.baeldung.com/linux/category/administration)
-   [FILES](https://www.baeldung.com/linux/category/files)
-   [FILESYSTEMS](https://www.baeldung.com/linux/category/filesystems)
-   [INSTALLATION](https://www.baeldung.com/linux/category/installation)
-   [NETWORKING](https://www.baeldung.com/linux/category/networking)
-   [PROCESSES](https://www.baeldung.com/linux/category/processes)
-   [SCRIPTING](https://www.baeldung.com/linux/category/scripting)
-   [SEARCH](https://www.baeldung.com/linux/category/search)
-   [SECURITY](https://www.baeldung.com/linux/category/security)
-   [WEB](https://www.baeldung.com/linux/category/web)

#### SERIES

-   [LINUX ADMINISTRATION](https://www.baeldung.com/linux/linux-administration-series)
-   [LINUX FILES](https://www.baeldung.com/linux/linux-files-series)
-   [LINUX PROCESSES](https://www.baeldung.com/linux/linux-processes-guide)

#### ABOUT

-   [ABOUT BAELDUNG](https://www.baeldung.com/about)
-   [THE FULL ARCHIVE](https://www.baeldung.com/linux/full_archive)
-   [WRITE FOR BAELDUNG](https://www.baeldung.com/linux/contribution-guidelines)
-   [EDITORS](https://www.baeldung.com/editors)

-   [TERMS OF SERVICE](https://www.baeldung.com/terms-of-service)
 -   [PRIVACY POLICY](https://www.baeldung.com/privacy-policy)
 -   [COMPANY INFO](https://www.baeldung.com/baeldung-company-info)
 -   [CONTACT](https://www.baeldung.com/contact)