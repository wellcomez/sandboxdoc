---
date: 2023-09-25 11:42
title: xinitrc
tags:
  - xinit
---

#  xinitrc

[Xinitrc](https://wiki.debian.org/Xinitrc)

[Translation(s)](https://wiki.debian.org/DebianWiki/EditorGuide#translation): English - [Français](https://wiki.debian.org/fr/Xinitrc) - [Italiano](https://wiki.debian.org/it/Xinitrc)

---

- ![/!\](https://wiki.debian.org/htdocs/debwiki/img/alert.png "/!\") Note : Modern Desktop environment also have (easier|GUI) session management tools. (Under Gnome System Menu > Preferences > session). Except for very specific purpose, it shouldn't be necessary to touch xinitrc file.
    

The xinit program is used to start the [X Window System](https://wiki.debian.org/XWindowSystem) server and a first client program on systems that cannot start X directly from /etc/init or in environments that use multiple window systems. When this first client exits, xinit will kill the X server and then terminate.

If no specific client program is given on the command line, xinit will look for a hidden file in the user's [home_directory](https://wiki.debian.org/home_directory) called .xinitrc to run as a shell script to start up client programs

A .xinitrc sample is:
~~~sh
# ~/.xinitrc
# This file is sourced when running startx and 
#    other programs which call xinit
# As in all Bash/sh scripts lines starting with 
#    '#' are comments
# Set background to your favorite pic
xsetbg -fullscreen /morphix/background.png &
# Start a XTerm
/usr/bin/X11/xterm &
# Start the systems window manager.
# See WindowManagers for other choices.
exec /etc/alternatives/x-window-manager
# To run Gnome instead of system default, place a 
# '#' before the previous line and uncomment the 
# next line.
# exec gnome-session
~~~

Note that ~/.xinitrc is only for configuring the initialization of xinit. If you want the script to be called when ever an X Session is started, then you should instead use [~/.xsession](https://wiki.debian.org/Xsession). Most [WindowManager](https://wiki.debian.org/WindowManager)s also have a method of starting programs when they first startup.

---

[CategoryXWindowSystem](https://wiki.debian.org/CategoryXWindowSystem)

Xinitrc ([最后修改时间 2021-06-28 00:51:37](https://wiki.debian.org/Xinitrc?action=info))

- Debian [privacy policy](https://www.debian.org/legal/privacy), Wiki [team](https://wiki.debian.org/Teams/DebianWiki), [bugs](https://bugs.debian.org/wiki.debian.org) and [config](https://salsa.debian.org/debian/wiki.debian.org).
- Powered by [MoinMoin](https://moinmo.in/ "This site uses the MoinMoin Wiki software.") and [Python](https://moinmo.in/Python "MoinMoin is written in Python."), with hosting provided by [Metropolitan Area Network Darmstadt](https://www.man-da.de/).