---
title: "macOS: How to run your Applications in a Mac OS X sandbox to enhance security"
layout: post
---

# macOS: How to run your Applications in a Mac OS X sandbox to enhance security
<a style="text-decoration:underline" href="https://paolozaino.wordpress.com/2015/08/04/how-to-run-your-applications-in-a-mac-os-x-sandbox-to-enhance-security/">macOS: How to run your Applications in a Mac OS X sandbox to enhance security</a><br>
< center>@PaoloFabioZaino</center>
In this article we’ll see how to configure and execute your software applications in an Apple Mac OS X sandbox. This kind of feature helps a lot to protect your Mac OS X security by executing an arbitrary application in a complete isolated environment.

### Introduction

From Apple documentation:

> The sandbox facility allows applications to voluntarily restrict their access to operating system resources. This safety mechanism is intended to limit potential damage in the event that a vulnerability is exploited. It is not a replacement for other operating system access controls.
> 
> New processes inherit the sandbox of their parent.

This means that, by using sandboxing, you can restrict access an application can have to operating system resources like filesystem or network etc…

Apple offers two ways to use sandboxing:

1.  one is by using sandbox library straight in the source code of your application
2.  the other is by running an arbitrary application using external sandboxing commands.

In this article we analyse the second.

### When should I use Application Sandboxing?

Sandbox applications that:

-   You don’t trust (for example applications you have downloaded from the internet that require you to allow their execution without being able to verify the source signature nor to check their source code)
-   Need to access external websites (and so, may download malicious content that may attempt to exploit your browser or its plugins)

By reducing access an application can have over your filesystem and resources helps to limit or even prevent (in some cases) the damages that an exploitation can do on your system.

> Please note: Sandboxing does not eliminate the need for a good anti-virus system, it actually works in conjunction with your anti-virus software. This because by reducing the access level an application can have over your system you actually help the job of your anti-virus software.

### I have got an anti-virus/firewall software, do I need sandboxing then?

**YES** if you are running untrusted applications or applications that may be exposed to malicious content (like Internet browsers, image previewers, PDF readers and so on). This because anti-virus software can generically detect no more than 60% (this number can vary over the years) of the total number of virus and spyware out there, so by using sandboxing in conjunction with a good anti-virus software is a good security practice. It is a good practice also on Mac OS X, which is continuously advertised as secure, but, in multiple occasions, resulted not to be so secure.

### So, what exactly can I limit an application from accessing when sandboxing it?

On Mac OS X you can limit an application from performing the following type of operations:

-   File read and/or write
-   IPC (InterProcess Communication) via Posix and SysV
-   Mach
-   Network activity inbound, outbound (specifically general networking or internet access)
-   Process execution and/or fork
-   Signals handling
-   Sysctl changes
-   System features call

### Last but no least…

Sandboxing applications is not as simple as just running a software program. Many applications may crash when too heavily sandboxed or when specific restrictions are put into place. So, it’s more of a “try and fail until it works” art and it takes some time to proper master sandboxing.

Apple Store downloaded applications are strictly controlled, but this still does NOT make you immune from **IPC interception** for example, which allow a malicious application to sniff data from vulnerable application (at the bottom of this page you’ll find a real-world example of this). So, by putting extra care in those rare cases when you need to execute an arbitrary application (especially the ones you may download from the internet and not the Apple Store or even some of the ones from the Apple Store amazingly wrapped by some really skilled hacker) it’s a very good practice. Again, Sandboxing is not a solution for all problems and if you want to know more about it have a look at [http://www.trustedbsd.org](http://www.trustedbsd.org/).

### How to sandbox an application?

To sandbox an existing application all you have to do is _create a sandbox configuration file_ in order to tell to Mac OS X which resources you want the application to be able to access and use.

> **Please note:** To find out which resources are necessary for your application to run fine is the “_try and fail_” process I mentioned before. So you’ll need to be **patient** and keep modifying your sandbox configuration file until everything will work as you want and as your application needs.

#### Sandbox configuration file syntax

The sandbox configuration file is divided into multiple sections (one per resource macro category).

The “;” symbol indicates a comment line.

The commands/directives syntax is similar to LISP programming language syntax. In other words they are always between ( ) (parentheses) where the first element after the first “(” identify the subject and the subsequent ones either its parameters or its alterations.

You can import another configuration file using the “import” command and specifying where to find the file on your computer (file path and file name).  Apple provides pre-built configurations, but they tend to change their position on every new Mac OS update, so please try to find them on your system.

#### Sandbox configuration file example

Below an example of sandboxing configuration file that you can use as base for your own one (please replace _MyApp_ with the application name you’re trying to sandbox):

```
;; This is my first sandbox configuration file!
(version 1) 
(deny default)

;; Let's allow file read and write in specific locations and not 
;; all over my filesystem!
;; Please note you can add more (regex "^/Users/user_name/xxxxxxxxxxx") lines depending 
;; on what your MyApp needs to function properly.
(allow file-write* file-read-data file-read-metadata
  (regex "^/Users/user_name/[Directories it requires to write and read from]")
  (regex "^/Applications/MyApp.app")
  (regex "^(/private)?/tmp/"))

;; You can also add a separate section for reading and writing files outside your
;; user_name account directory.
(allow file-read-data file-read-metadata
  (regex "^/dev/autofs.*")
  (regex "^/System/Library")
  (regex "^/Applications/MyApp.app")
  (regex "^/usr/lib")
  (regex "^/var")
  (regex "^/Users/user_name"))

;; If your MyApp requires to access sysctl (in read)
(allow mach* sysctl-read)

;; If you want to import extra rules from 
;; an existing sandbox configuration file: 
(import "/usr/share/sandbox/bsd.sb")

;; If you want to decide in which filesystem paths 
;; MyApp is forbidden to write:
(deny file-write-data
   (regex #"^(/private)?/etc/localtime$"
     #"^/usr/share/nls/"
	 #"^/usr/share/zoneinfo/"))

;; If your MyApp wants to run extra processes it's be allowed to run only
;; child processes and nothing else
(allow process-exec 
  (regex "^/Applications/MyApp.app"))

;; If your MyApp requires network access you can grant it here:
(allow network*)
```

#### How to use a sandbox configuration file

Once we have done with our sandbox configuration file for your application, you can simply execute your application in the sandbox by using the following command from the command line:

```
sandbox-exec -f myapp-sandbox-conf /Applications/MyApp.app/Contents/MacOS/MyApp-bin
```

Where _myapp-sandbox-conf_ is the name of your sandbox configuration file and _MyApp_ is the name of the application your want to run in the sandbox.

#### For more info

If my generic sandbox file will be too generic for you and you want more practical examples (already implemented) then run your terminal application and have a look to all the examples already kindly provided by Apple:

In this directory you’ll find plenty of files like

To look into and have more insight/tutorial to write your own sandbox configuration file for your specific application.

That’s all folks, happy hacking!

[Quick Edit to add some security resources about sandboxing]

In case of any possible comment/thoughts about the native security offered by Mac OS X (included Yosemite), please look at **[this article](http://arstechnica.com/security/2015/06/serious-os-x-and-ios-flaws-let-hackers-steal-keychain-1password-contents/)** before posting any comment on that matter.

Also have a look at **[this one](https://drive.google.com/file/d/0BxxXk1d3yyuZOFlsdkNMSGswSGs/view?pli=1)** which is a useful source of information and how critical is securing IPC to help avoiding password stealing.

[/Quick Edit]

If you want to have a look at a practical example then see this other article of mine: [https://paolozaino.wordpress.com/2015/10/20/maximum-security-and-privacy-using-mac-os-sandbox-and-tor-browser-bundle/](https://paolozaino.wordpress.com/2015/10/20/maximum-security-and-privacy-using-mac-os-sandbox-and-tor-browser-bundle/)

