---
title: "macOS App sandboxing via sandbox-exec"
layout: post
---
[Site](https://www.karltarvas.com/2020/10/25/macos-app-sandboxing-via-sandbox-exec.html)

# macOS: App sandboxing via sandbox-exec

<time>25th October 2020</time>

It isn’t widely advertised, but macOS ships with a standalone sandboxing utility out of the box: `sandbox-exec`. While the very short manpage says the utility has been marked deprecated, and for quite a few major releases now, it’s used heavily by internal systems so it’s unlikely go away anytime soon.

Sandbox configurations are writen in a subset of Scheme. A minimal useful starter example for wrapping a modern application might look something like this:

```
(version 1)
;; Disallow everything by default
(deny default)

;;
;; This system profile grants access to a number of things, such as:
;;
;;  - locale info
;;  - system libraries (/System/Library, /usr/lib, etc)
;;  - access to to basic tools (/etc, /dev/urandom, etc)
;;  - Apple services (com.apple.system, com.apple.dyld, etc)
;;
;; and more, see bsd.sb and system.sb in the corresponding directory.
;;
(import "/System/Library/Sandbox/Profiles/bsd.sb")
```

Saving the above as `config.sb`, you can use it to sandbox an app as follows:

```
$ sandbox-exec -f config.sb /Applications/Foo.app/Contents/MacOS/Foo
```

To see all the operations that were denied, open Applications → Utilities → Console and search for `sandbox` and the application name. Historically, you could use the `(trace "output")` command, but this seems dysfunctional on the latest macOS.

Most modern applications will not function with such limited permissions, so expect some back and forth before your sandbox profile works.

Depending on your OS version, you can find some system sandbox examples in some of the following locations:

* `/Library/Sandbox/Profiles`
* `/System/Library/Sandbox/Profiles`
* `/usr/share/sandbox`

The tool has virtually no official documentation so some hacker insight can come very handy. There’s a number of useful examples here:

* [“Mac Sandbox V2 Design Doc” on chromium.googlesource.com](https://chromium.googlesource.com/chromium/src/+/master/sandbox/mac/seatbelt_sandbox_design.md)
* [“macOS: How to run your Applications in a Mac OS X sandbox to enhance security” on paolozaino.wordpress.com](https://paolozaino.wordpress.com/2015/08/04/how-to-run-your-applications-in-a-mac-os-x-sandbox-to-enhance-security/)
* [“A quick glance at macOS’ sandbox-exec” on jmmv.dev](https://jmmv.dev/2019/11/macos-sandbox-exec.html)

Further historical background and technical details can be found here:

* [“Sandbox/OS X Rule Set” on the Mozilla wiki](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set)
* [Ozymandias42/macOS-Sandbox-Profiles](https://github.com/Ozymandias42/macOS-Sandbox-Profiles/blob/master/telegram.sb)
* [“Test The MacOS Sandbox” on craftware.xyz](https://craftware.xyz/tips/MacOS-sandbox.html)
* [“OS X sandbox quickstart” on blog.squarelemon.com](https://blog.squarelemon.com/2015/02/os-x-sandbox-quickstart/)
* [This answer by Eskimo on the Apple Developer Forums](https://developer.apple.com/forums/thread/661176?answerId=635519022#635519022)

Setting up a Sandbox from scratch can often be largely trial and error — disallow everything, and then follow the trail of errors to see what you need to enable as a bare minimum to make the app work.

On the upside, it’s a great way to gain insight into what closed source binaries are trying to do on your system.