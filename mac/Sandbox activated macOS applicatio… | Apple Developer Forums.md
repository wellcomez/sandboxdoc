---
title: "Sandbox activated macOS applicatio… | Apple Developer Forums"
layout: post
---

# Sandbox activated macOS applicatio… | Apple Developer Forums
<a style="text-decoration:underline" href="https://developer.apple.com/forums/thread/661176">Sandbox activated macOS applicatio… | Apple Developer Forums</a><br>

> you can see my crash reports below:

That’s what I was looking for.

Etresoft has pointed you in the right direction here but I want to explain some of the backstory. All of the following is relative to Crash Report1.crash.

To start, consider:

```
Code Block  

<table><tbody><tr><td>Exception Type:        EXC_BAD_INSTRUCTION (SIGILL)</td></tr></tbody></table>


```

This indicates that your process died because it tried to execute an illegal instruction. In some cases this is caused by your code running off into the weeds but in this case it’s clear that this is a deliberate crash. Code running within your process has detected a problem and run an illegal instruction to cause the process to terminate.

You can see who did this by looking at the crashing thread backtrace:

```
Code Block  

<table><tbody><tr><td>Thread 0 Crashed:: Dispatch queue: com.apple.main-thread</td></tr><tr><td>0   libsystem_secinit.dylib … _libsecinit_appsandbox.cold.2 + 95</td></tr><tr><td>1   libsystem_secinit.dylib … _libsecinit_appsandbox + 1511</td></tr><tr><td>2   libsystem_secinit.dylib … _libsecinit_initializer + 35</td></tr><tr><td>3   libSystem.B.dylib       … libSystem_initializer + 268</td></tr><tr><td>4   dyld                    … ImageLoaderMachO::doModInitFunctions(ImageLoader::LinkContext const&amp;) + 535</td></tr><tr><td>5   dyld                    … ImageLoaderMachO::doInitialization(ImageLoader::LinkContext const&amp;) + 40</td></tr><tr><td>6   dyld                    … ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&amp;, unsigned in…</td></tr><tr><td>…</td></tr><tr><td>16  dyld                    … ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&amp;, unsigned in…</td></tr><tr><td>17  dyld                    … ImageLoader::processInitializers(ImageLoader::LinkContext const&amp;, unsigned int, I…</td></tr><tr><td>18  dyld                    … ImageLoader::runInitializers(ImageLoader::LinkContext const&amp;, ImageLoader::Initia…</td></tr><tr><td>19  dyld                    … dyld::initializeMainExecutable() + 199</td></tr><tr><td>20  dyld                    … dyld::_main(macho_header const*, unsigned long, int, char const<strong>, char const</strong>, …</td></tr><tr><td>21  dyld                    … dyldbootstrap::start(dyld3::MachOLoaded const*, int, char const, dyld3::MachOLo…</td></tr><tr><td>22  dyld                    … _dyld_start + 37</td></tr></tbody></table>


```

This is very early in the startup sequence for your process. The dynamic linker (frames 22 through 4) is in the middle of linking together your code. As part of this it runs the module initialiser for the System framework (frame 3). That calls the initialiser for the security subsystem (frame 2). That detects that your process is sandbox and attempts to set up the App Sandbox (frames 1 through 0). That setup operation has failed, at which point the code has no choice but to crash your process (it can’t let it continue without a sandbox).

Both dyld and the security subsystem have left breadcrumbs in the Application Specific Information section to explain the problem:

```
Code Block  

<table><tbody><tr><td>dyld: launch, running initializers</td></tr><tr><td>/usr/lib/libSystem.B.dylib</td></tr><tr><td>Could not set sandbox profile data: Operation not permitted (1)</td></tr><tr><td></td></tr><tr><td>Application Specific Signatures:</td></tr><tr><td>SYSCALL_SET_PROFILE</td></tr></tbody></table>


```

You can see that the system call to set up the sandbox has failed with EPERM. As to _why_ that’s happened, this is again something that Etresoft pointed to. Consider this:

```
Code Block  

<table><tbody><tr><td>Process:               test [8519]</td></tr></tbody></table>


```

and this:

```
Code Block  

<table><tbody><tr><td>Parent Process:        ??? [8518]</td></tr></tbody></table>


```

Normally an app’s parent process is launchd. In this case the process IDs are sequential, suggesting that the app has relaunched itself. Doing this is incompatible with the App Sandbox.

A process can set up the App Sandbox in one of two ways:

-   From scratch, by setting the com.apple.security.app-sandbox — In this case the process is expected to be launched by a non-sandboxed process, like launchd. This is how apps and app extensions work.
    
-   By inheriting it from its parent, by setting com.apple.security.app-sandbox and com.apple.security.inherit — If you embed a helper tool within your app, this is how you set it up to inherit the app’s sandbox [1].
    

Your process seems to be trying to combine these, that is, it’s signed like a normal app but it’s trying to inherit it sandbox from its parent. This won’t work because the system tries to re-apply the sandbox and that is not allowed (hence the EPERM).

It seems that you’ve inherited (so to speak) this approach from PyInstaller. If so, you’ll need to discuss this issue with them. The current approach they’re using is fundamentally incompatible with the App Sandbox.

Share and Enjoy  
—  
Quinn “The Eskimo!” @ Developer Technical Support @ Apple  
let myEmail = "eskimo" + "1" + "@apple.com"

