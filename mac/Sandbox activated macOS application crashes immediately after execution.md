https://developer.apple.com/forums/thread/661176

Developer Forums
Search by keywords or tags
Search by keywords or tags
Submit Search
Additional information about Search by keywords or tags
Post
Profile
Sandbox activated macOS application crashes immediately after execution
I want to submit my python based application to apple store, but whenever I activate sandboxing for my application(no matter the content of code, even just “hello world” printing crashes),

It crashes in a couple of sec. with following console error after execution, Although it works fine without sandbox activation :

“Sandbox: test(8409) deny(1) forbidden-sandbox-reinit”

1-Requirments:

macOS Catalina 10.15.4
Python 3.6
PyInstaller 4.0
Certification:Developer ID Application
Manual(without Xcode): build, signing, notarizing and sandboxing

2.Implementing Simple Code Ex(test.py):(No matter which code content is used, all crash)
~~~
test.py
from AppKit import NSOpenPanel
from objc import YES, NO
panel = NSOpenPanel.openPanel()
panel.setTitle_("open file")
panel.setAllowsMultipleSelection_(NO)
panel.setCanChooseDirectories_(YES)
panel.runModal()

from AppKit import NSOpenPanel
from objc import YES, NO
panel = NSOpenPanel.openPanel()
panel.setTitle("open file")
panel.setAllowsMultipleSelection(NO)
panel.setCanChooseDirectories_(YES)
panel.runModal()
~~~

3.Creating test.spec:

~~~
test.spec
block_cipher = None
a = Analysis(['test.py'],
             pathex=['/Users/Emre/Documents/Work/models/research/object_detection'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='test',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
app = BUNDLE(exe,
             name='test.app',
             icon=None,
             bundle_identifier=None)

blockcipher = None
a = Analysis(['test.py'],
             pathex=['/Users/Emre/Documents/Work/models/research/objectdetection'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtimehooks=[],
             excludes=[],
             winnopreferredirects=False,
             winprivateassemblies=False,
             cipher=blockcipher)
pyz = PYZ(a.pure, a.zippeddata,
             cipher=blockcipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='test',
          debug=False,
          strip=False,
          upx=True,
          runtimetmpdir=None,
          console=False )
app = BUNDLE(exe,
             name='test.app',
             icon=None,
             bundle_identifier=None)
~~~

4.Creating .App File:

~~~
python3.6 -m PyInstaller -F test.spec
~~~

5.Creating Entitlements(Entitlements.plist):

~~~

Entitlements.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-url(not allowed to be added in developer forum)">
<plist version="1.0">
<dict>
    <key>com.apple.security.app-sandbox</key>
    <true/>
</dict>
</plist>

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-url(not allowed to be added in developer forum)">
<plist version="1.0">
<dict>
    <key>com.apple.security.app-sandbox</key>
    <true/>
</dict>
</plist>
~~~

6.Codesiging Manually:

~~~
 codesign --force --options=runtime  --sign "Developer ID Application: Emre (xxxx)” test.app --entitlements Entitlements.plist

~~~
7.Generate dmg file via disk utility for Notarizing from test.py:

-

8.Notarizing the application:

~~~
xcrun altool --notarize-app --primary-bundle-id "com.emre" --username "emre@gmail.com" --password "@keychain:AppSpecPass" --file /Users/Emre/Documents/Work/models/research/objectdetection/dist/test.dmg
~~~

9.Verification Of Certificate and sandboxing:

~~~
spctl -a -t exec -vv test.app
test.app: accepted
source=Notarized Developer ID
origin=Developer ID Application: Emre Guenaydin (93DS7PC26P)

codesign --verify --deep --strict --verbose=2 test.app
test.app: valid on disk
test.app: satisfies its Designated Requirement

codesign -dvvv --entitlements :- test.app
Executable=/Users/Emre/Documents/Work/models/research/objectdetection/dist/untitled folder/test.app/Contents/MacOS/test
Identifier=test
Format=app bundle with Mach-O thin (x86_64)
CodeDirectory v=20500 size=73712 flags=0x10000(runtime) hashes=2295+5 location=embedded
Hash type=sha256 size=32
CandidateCDHash sha1=e3716fc5262107748b2658e4cbbcee97670268c1
CandidateCDHashFull sha1=e3716fc5262107748b2658e4cbbcee97670268c1
CandidateCDHash sha256=6fe01092e170231a9fdc855871ce3161b71b8af5
CandidateCDHashFull sha256=6fe01092e170231a9fdc855871ce3161b71b8af597cd78410dcea3b80092e5c0
Hash choices=sha1,sha256
CMSDigest=cf0a13051e77eba16609e67ca580b24d2698becad591a45fac911d44ddbef0b6
CMSDigestType=2
CDHash=6fe01092e170231a9fdc855871ce3161b71b8af5
Signature size=9058
Authority=Developer ID Application: Emre (***)
Authority=Developer ID Certification Authority
Authority=Apple Root CA
Timestamp=20. Sep 2020 at 23:40:39
Info.plist entries=8
TeamIdentifier=***
Runtime Version=10.11.0
Sealed Resources version=2 rules=13 files=1
Internal requirements count=1 size=164
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-url(not allowed to be commented in developer forum)">
<plist version="1.0">
<dict>
    <key>com.apple.security.app-sandbox</key>
    <true/>
</dict>
</plist>
~~~

10.Execute .app file:

~~~
Crashes with following console report follow:
default 23:45:08.671813+0200 secinitd test[8055]: AppSandbox request successful
default 23:45:09.275405+0200 secinitd test[8056]: AppSandbox request successful
error 23:45:09.504553+0200 sandboxd Failed to produce a full report for: test[8056].
error 23:45:09.504643+0200 sandboxd Sandbox: test(8056) deny(1) forbidden-sandbox-reinit
~~~

App Sandbox
 
App Submission
Up vote post of EmreGun
Down vote post of EmreGun
 2.2kviews
Posted 2 years ago by  EmreGun
Copy EmreGun question
Reply
Add a Comment
Replies
Does this generate a crash report? If so, please post it here. Use the text attachment feature to avoid clogging up the log.

ps Does the problem reproduce if you skip steps 7 through 9? I think it will and, if so, you can skip those steps while testing this. Those steps are important when you finally deploy your app, but you can improve you turnaround time by skipping them whilst debugging.

Share and Enjoy
—
Quinn “The Eskimo!” @ Developer Technical Support @ Apple
let myEmail = "eskimo" + "1" + "@apple.com"
Posted 2 years ago by  eskimo
Copy eskimo answer
Up vote reply of eskimo
Down vote reply of eskimo
Add a Comment
I want to submit my python based application to apple store

OK. Wait a second. Let's focus on that. Do you really want to do that? Apparently this is something that people have been trying for years and haven't gotten it working. See github.com/pyinstaller/pyinstaller/issues/2198

Also, you are trying to Notarize with a Developer ID too? Are you aware that has nothing to do with the Mac App Store? I don't know if trying to build a Notarized Developer ID version would be any easier. It may just give you a completely different set of problems. But you definitely don't want to try both. For one, it simply isn't possible. For another, you may just confuse yourself. Mac App Store and Developer ID are two separate tasks. Apple does try to get people to sandbox their Developer ID apps, but that is a different question and not something you should probably be attempting at this point.

That GitHub link contains a reference to this person getting something close working: medium.com/python-pandemonium/embedding-a-python-application-in-macos-d866adfcaf94

However, in this case, the developer has generic, Objective-C Mac app running and is then using Python as the back end. That is a much more manageable problem.
Posted 2 years ago by  Etresoft
Copy Etresoft answer
Up vote reply of Etresoft
Down vote reply of Etresoft
Add a Comment
@Etresoft:Thank you very much for your hints. Theys were really helpful to understand the problem in details.
Answers are below for your questions:

1-Do you really want to do that? 
Unfortunately, yes! Before I wasted several days to create my .app file via Pyinstaller, I was not aware of this sandboxing problem. 
Therefore I want to research a couple of days more to solve the problem.

2-you are trying to Notarize with a Developer ID too?
Actually It works fine somehow :) you can see in 9.Verification Of Certificate and sandboxing. But you can just ignore step9.
When I was debugging my sandboxing problem, I saw that “spctl -a -t exec -vv test.app” command was throwing “rejected” error, therefore I thought that my problem could be relevant with this rejection. But apparently it is not :)

3-That GitHub link contains a reference to this person getting something close working
I guess my problem is a similar one which you mentioned in your second link.
During my researches, I found following description in Pyinstaller tutorial:

PyInstaller Tutorial Bootloader Section
Bootloader
The bootloader prepares everything for running Python code. It begins the setup and then returns itself in
another process. This approach of using two processes allows a lot of flexibility and is used in all bundles 
except one-folder mode in Windows. So do not be surprised if you will see your bundled app as two
processes in your system task manager.


So I had a look into my activity monitor(test.app without sandboxing, since sandboxed application crashes immediately), and I saw that they are 2 processes indeed:

~~~
activity monitor view
 test	(null)	0,50	1	0	0,0	0,00	8049	emre	No	0,0	-	No	No	15,6 MB	14	0 bytes	0 bytes	0 bytes	0 bytes	0	0	20,5 MB	15,6 MB	752 KB	No	No	0 bytes	0 bytes	637,0	

 test	(null)	2,64	3	1	0,0	0,00	8048	emre	No	0,0	-	No	No	8,3 MB	96	0 bytes	0 bytes	0 bytes	0 bytes	0	0	98,6 MB	7,5 MB	6,2 MB	No	No	0 bytes	0 bytes	2.493,7	

~~~

Process id 8049 is parent of 8048. Could it be that I have to (somehow) inherit from my application itself for code signing?(<key>com.apple.security.inherit</key>) Maybe a similar situation as it is mentioned in your second link? Now "“Sandbox: test(8409) deny(1) forbidden-sandbox-reinit” error makes also sense.

I tried to codesign my test.py with following inheritance entitlements:
entitlements

~~~
<dict>
 	<key>com.apple.security.app-sandbox</key>
 	<true/>
 	<key>com.apple.security.inherit</key>
 	<true/>
</dict>
</plist>
~~~


But unfortunately I got following crash report:
~~~
crash report
Process:               test [8067]
Path:                  /Users/USER/Documents/*/test.app/Contents/MacOS/test
Identifier:            test
Version:               0.0.0 (???)
Code Type:             X86-64 (Native)
Parent Process:        ??? [1]
Responsible:           test [8067]
User ID:               501

Date/Time:             2020-09-22 23:22:33.704 +0200
OS Version:            Mac OS X 10.15.4 (19E287)
Report Version:        12
Bridge OS Version:     4.4 (17P4281)
Anonymous UUID:        D3EEF8BC-6116-B08B-E9F2-BBC7865AD660

Sleep/Wake UUID:       67A94588-B5B4-4DE9-825B-10716AC9B467

Time Awake Since Boot: 70000 seconds
Time Since Wake:       5400 seconds

System Integrity Protection: enabled

Crashed Thread:        0  Dispatch queue: com.apple.main-thread

Exception Type:        EXC_BAD_INSTRUCTION (SIGILL)
Exception Codes:       0x0000000000000001, 0x0000000000000000
Exception Note:        EXC_CORPSE_NOTIFY

Termination Signal:    Illegal instruction: 4
Termination Reason:    Namespace SIGNAL, Code 0x4
Terminating Process:   exc handler [8067]

Application Specific Information:
dyld: launch, running initializers
/usr/lib/libSystem.B.dylib
Process is not in an inherited sandbox.

Application Specific Signatures:
Process is not in an inherited sandbox.

Thread 0 Crashed:: Dispatch queue: com.apple.main-thread
0   libsystem_secinit.dylib       	0x00007fff6a02c17b _libsecinit_appsandbox.cold.5 + 59
1   libsystem_secinit.dylib       	0x00007fff6a02b98d _libsecinit_appsandbox + 2053
2   libsystem_secinit.dylib       	0x00007fff6a02b147 _libsecinit_initializer + 35
3   libSystem.B.dylib             	0x00007fff66e027c1 libSystem_initializer + 268
4   dyld                          	0x000000011cc9d1e3 ImageLoaderMachO::doModInitFunctions(ImageLoader::LinkContext const&) + 535
5   dyld                          	0x000000011cc9d5ee ImageLoaderMachO::doInitialization(ImageLoader::LinkContext const&) + 40
6   dyld                          	0x000000011cc9800b ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 493
7   dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
8   dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
9   dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
10  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
11  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
12  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
13  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
14  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
15  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
16  dyld                          	0x000000011cc97f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
17  dyld                          	0x000000011cc96014 ImageLoader::processInitializers(ImageLoader::LinkContext const&, unsigned int, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 188
18  dyld                          	0x000000011cc960b4 ImageLoader::runInitializers(ImageLoader::LinkContext const&, ImageLoader::InitializerTimingList&) + 82
19  dyld                          	0x000000011cc845e6 dyld::initializeMainExecutable() + 199
20  dyld                          	0x000000011cc89af8 dyld::_main(macho_header const*, unsigned long, int, char const**, char const**, char const**, unsigned long*) + 6667
21  dyld                          	0x000000011cc83227 dyldbootstrap::start(dyld3::MachOLoaded const*, int, char const**, dyld3::MachOLoaded const*, unsigned long*) + 453
22  dyld                          	0x000000011cc83025 _dyld_start + 37

Thread 0 crashed with X86 Thread State (64-bit):
  rax: 0x0000000000000027  rbx: 0x00007ffee04d6510  rcx: 0x05899ff724a00067  rdx: 0x0000000000000002
  rdi: 0x0000000000000000  rsi: 0x00007fb048d040fc  rbp: 0x00007ffee04d64c0  rsp: 0x00007ffee04d64b0
   r8: 0x00007ffee04d6200   r9: 0x00007ffee04d6480  r10: 0x00007fff6a02cd3f  r11: 0x00007fff6a02cd41
  r12: 0x00007fb048d040dc  r13: 0x000060000227c000  r14: 0x00007fb048d040dc  r15: 0x000000011523c5f0
  rip: 0x00007fff6a02c17b  rfl: 0x0000000000010206  cr2: 0x00007fff90949ac0

Logical CPU:     0
Error Code:      0x00000000
Trap Number:     6


Binary Images:
       0x10f725000 -        0x10f72bff7 +test (0.0.0 - ???) <A66A43DB-0BC7-32BE-B1E3-C67B5B02215F> /Users/USER/Documents/*/test.app/Contents/MacOS/test
       0x11cc82000 -        0x11cd13eff  dyld (750.5) <1F893B81-89A5-3502-8510-95B97B9F730D> /usr/lib/dyld
    0x7fff2bae5000 -     0x7fff2bae5fff  com.apple.Accelerate (1.11 - Accelerate 1.11) <8BE0965F-6A6A-35B0-89D0-F0A75835C2CA> /System/Library/Frameworks/Accelerate.framework/Versions/A/Accelerate
    0x7fff2bafd000 -     0x7fff2c153fef  com.apple.vImage (8.1 - 524.2) <DAE0E5C5-BA70-325D-8B4C-6B821F009CBF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vImage.framework/Versions/A/vImage
    0x7fff2c154000 -     0x7fff2c3bbff7  libBLAS.dylib (1303.60.1) <4E980D6B-4B3A-33D6-B52C-AFC7D120D11A> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
    0x7fff2c3bc000 -     0x7fff2c88ffef  libBNNS.dylib (144.100.2) <C05F9F9D-4498-37BD-9C1C-2F7B920B401D> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBNNS.dylib
    0x7fff2c890000 -     0x7fff2cc2bfff  libLAPACK.dylib (1303.60.1) <F8E9D081-7C60-32EC-A47D-2D30CAD73C5F> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLAPACK.dylib
    0x7fff2cc2c000 -     0x7fff2cc41fec  libLinearAlgebra.dylib (1303.60.1) <79CB28C5-F811-3EAF-AD8E-7D7D879FE662> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLinearAlgebra.dylib
    0x7fff2cc42000 -     0x7fff2cc47ff3  libQuadrature.dylib (7) <EB7C9E98-D1E7-314C-90B4-3EB04428CC7C> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libQuadrature.dylib
    0x7fff2cc48000 -     0x7fff2ccb8fff  libSparse.dylib (103) <8C55F5F2-6AE3-393C-B2FF-22B8CFCBD7FC> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparse.dylib
    0x7fff2ccb9000 -     0x7fff2cccbfef  libSparseBLAS.dylib (1303.60.1) <08F6D629-5DAC-3A99-B261-2B6095DD38B4> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparseBLAS.dylib
    0x7fff2cccc000 -     0x7fff2cea3fd7  libvDSP.dylib (735.100.4) <0744F29B-F822-3571-9B4A-B592146D4E03> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvDSP.dylib
    0x7fff2cea4000 -     0x7fff2cf66fef  libvMisc.dylib (735.100.4) <E6C94B52-931B-3858-AF4D-C2EA52ACB7F5> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvMisc.dylib
    0x7fff2cf67000 -     0x7fff2cf67fff  com.apple.Accelerate.vecLib (3.11 - vecLib 3.11) <66282197-81EE-316F-978E-EF1471551DEF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/vecLib
    0x7fff2df20000 -     0x7fff2df20fff  com.apple.ApplicationServices (48 - 50) <CABCF23C-55E5-35E1-AAF0-EE5DDE3FDB03> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices
    0x7fff2df21000 -     0x7fff2df8cfff  com.apple.ApplicationServices.ATS (377 - 493.0.4.1) <6AA4BBCC-43AF-3EBF-8EB5-7916A3B563AA> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/ATS
    0x7fff2e025000 -     0x7fff2e063ff0  libFontRegistry.dylib (274.0.4.2) <FBF6EC26-42C0-334E-B67C-871AD50DB0BC> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/Resources/libFontRegistry.dylib
    0x7fff2e0be000 -     0x7fff2e0edfff  com.apple.ATSUI (1.0 - 1) <D8C604E9-D854-3A32-B37B-819197537A63> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATSUI.framework/Versions/A/ATSUI
    0x7fff2e0ee000 -     0x7fff2e0f2ffb  com.apple.ColorSyncLegacy (4.13.0 - 1) <2359E2CD-8FCE-32D7-AF76-F4D9A3D9D9F8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ColorSyncLegacy.framework/Versions/A/ColorSyncLegacy
    0x7fff2e18c000 -     0x7fff2e1e3ffa  com.apple.HIServices (1.22 - 675.1) <B2DEE96F-ED7A-3924-A2E2-44BB7A950BD8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/HIServices.framework/Versions/A/HIServices
    0x7fff2e1e4000 -     0x7fff2e1f2fff  com.apple.LangAnalysis (1.7.0 - 1.7.0) <1603F2CC-DC51-3E15-B6B5-0A9F9AB0C045> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/LangAnalysis.framework/Versions/A/LangAnalysis
    0x7fff2e1f3000 -     0x7fff2e238ffa  com.apple.print.framework.PrintCore (15.4 - 516.2) <525E8A4B-297B-3CAC-8A4A-6C7E211D7A21> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/PrintCore.framework/Versions/A/PrintCore
    0x7fff2e239000 -     0x7fff2e243ff7  com.apple.QD (4.0 - 413) <1EAEF5BC-D649-3E42-87BC-43CCEE4D5274> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/QD.framework/Versions/A/QD
    0x7fff2e244000 -     0x7fff2e251ffc  com.apple.speech.synthesis.framework (9.0.24 - 9.0.24) <C2E5BBFC-2EF0-3FFE-A1CF-960631DC249C> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/SpeechSynthesis.framework/Versions/A/SpeechSynthesis
    0x7fff2e252000 -     0x7fff2e333ffa  com.apple.audio.toolbox.AudioToolbox (1.14 - 1.14) <4222CBDF-D637-30DB-BA45-C6E222BABB24> /System/Library/Frameworks/AudioToolbox.framework/Versions/A/AudioToolbox
    0x7fff2e335000 -     0x7fff2e335fff  com.apple.audio.units.AudioUnit (1.14 - 1.14) <73D89D5E-05D5-3F64-BE02-2B2ED6AD6C03> /System/Library/Frameworks/AudioUnit.framework/Versions/A/AudioUnit
    0x7fff2e6c8000 -     0x7fff2ea56ffd  com.apple.CFNetwork (1125.2 - 1125.2) <1D4D81F7-FC48-3588-87FC-481E2586E345> /System/Library/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
    0x7fff2ead2000 -     0x7fff2ead2fff  com.apple.Carbon (160 - 162) <CAA294BD-BC93-384A-8415-B254C89098FC> /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon
    0x7fff2ead3000 -     0x7fff2ead6ff3  com.apple.CommonPanels (1.2.6 - 101) <63261921-DD00-312E-AFD1-C099E1984725> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/CommonPanels.framework/Versions/A/CommonPanels
    0x7fff2ead7000 -     0x7fff2edcbff3  com.apple.HIToolbox (2.1.1 - 994.6) <C03A48FC-1A02-320D-9147-F4687A1BBC6F> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/HIToolbox
    0x7fff2edcc000 -     0x7fff2edcfff3  com.apple.help (1.3.8 - 71) <F5E9EA64-5D5E-346F-98D0-D04A2F805D8D> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Help.framework/Versions/A/Help
    0x7fff2edd0000 -     0x7fff2edd5ff7  com.apple.ImageCapture (9.0 - 1600.60.4.2) <63FE9A8C-A7C6-3ABD-AA1D-EA1BC6613C39> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/ImageCapture.framework/Versions/A/ImageCapture
    0x7fff2edd6000 -     0x7fff2edd6fff  com.apple.ink.framework (10.15 - 227) <90518F56-AD8A-3627-905A-16E2B8640F87> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Ink.framework/Versions/A/Ink
    0x7fff2edd7000 -     0x7fff2edf1ffa  com.apple.openscripting (1.7 - 185.1) <718C485A-4167-3A26-B2CD-6C42B5B36D01> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/OpenScripting.framework/Versions/A/OpenScripting
    0x7fff2ee12000 -     0x7fff2ee12fff  com.apple.print.framework.Print (15 - 271) <29384C24-6667-3BAA-992A-DAD809D6387E> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Print.framework/Versions/A/Print
    0x7fff2ee13000 -     0x7fff2ee15ff7  com.apple.securityhi (9.0 - 55008) <478C57A9-D5A5-3951-B289-DA5323E9044A> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SecurityHI.framework/Versions/A/SecurityHI
    0x7fff2ee16000 -     0x7fff2ee1cfff  com.apple.speech.recognition.framework (6.0.3 - 6.0.3) <E6BE4EC1-5C53-38BB-AAFD-E7BF7A8975BC> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SpeechRecognition.framework/Versions/A/SpeechRecognition
    0x7fff2efc4000 -     0x7fff2f0bafff  com.apple.ColorSync (4.13.0 - 3394.7) <FC6CFACE-CDD8-3811-BAB6-C9F82AC0A594> /System/Library/Frameworks/ColorSync.framework/Versions/A/ColorSync
    0x7fff2f3a5000 -     0x7fff2f8aeffb  com.apple.audio.CoreAudio (5.0 - 5.0) <CF50C6CC-6753-3D64-A76B-21CE211A98E8> /System/Library/Frameworks/CoreAudio.framework/Versions/A/CoreAudio
    0x7fff2f901000 -     0x7fff2f939fff  com.apple.CoreBluetooth (1.0 - 1) <D2943204-C3A0-3C09-A7A9-BF75822678B4> /System/Library/Frameworks/CoreBluetooth.framework/Versions/A/CoreBluetooth
    0x7fff2fd25000 -     0x7fff2fe50ffe  com.apple.CoreDisplay (1.0 - 186.5.25) <53F750C6-947A-39AE-984E-41939B858A68> /System/Library/Frameworks/CoreDisplay.framework/Versions/A/CoreDisplay
    0x7fff2fe51000 -     0x7fff302d0ffb  com.apple.CoreFoundation (6.9 - 1675.129) <9E632A1E-9622-33D6-BCCE-23AC16DAA6B7> /System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
    0x7fff302d2000 -     0x7fff30946fe0  com.apple.CoreGraphics (2.0 - 1355.13) <54528FE3-21A7-3F64-B7AA-F6B95394488D> /System/Library/Frameworks/CoreGraphics.framework/Versions/A/CoreGraphics
    0x7fff30954000 -     0x7fff30cafff0  com.apple.CoreImage (15.0.0 - 940.9) <CA78A35D-E15E-3D98-BDEF-9F3D9039DB78> /System/Library/Frameworks/CoreImage.framework/Versions/A/CoreImage
    0x7fff31239000 -     0x7fff31239fff  com.apple.CoreServices (1069.22 - 1069.22) <888FE7B9-CE6C-3C7C-BA33-63364462228A> /System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
    0x7fff3123a000 -     0x7fff312bffff  com.apple.AE (838.1 - 838.1) <2BAB1B88-C198-3D20-8DA3-056E66510E7A> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE.framework/Versions/A/AE
    0x7fff312c0000 -     0x7fff315a1ff7  com.apple.CoreServices.CarbonCore (1217 - 1217) <D0FECC17-7E16-308F-98EA-AF311CB77FE6> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
    0x7fff315a2000 -     0x7fff315efffd  com.apple.DictionaryServices (1.2 - 323.6) <11513ED9-8B4B-39BB-A6B2-AA6AA0A2DF72> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/DictionaryServices.framework/Versions/A/DictionaryServices
    0x7fff315f0000 -     0x7fff315f8ff7  com.apple.CoreServices.FSEvents (1268.100.1 - 1268.100.1) <CE3D8B13-2583-3527-8532-D5DDAAD7D56B> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEvents.framework/Versions/A/FSEvents
    0x7fff315f9000 -     0x7fff31832ffc  com.apple.LaunchServices (1069.22 - 1069.22) <E51EE658-608C-3034-9635-4FDF1E241E62> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/LaunchServices
    0x7fff31833000 -     0x7fff318cbff1  com.apple.Metadata (10.7.0 - 2076.3) <EE42CCA1-FEC2-3F1C-9B62-2E73EFB05FCC> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
    0x7fff318cc000 -     0x7fff318f9fff  com.apple.CoreServices.OSServices (1069.22 - 1069.22) <A0654B4E-3194-3066-911F-FF1FBEE1D2C2> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
    0x7fff318fa000 -     0x7fff31961fff  com.apple.SearchKit (1.4.1 - 1.4.1) <D4F82BC9-FD9B-3E04-B78E-D9E2A73B0BD7> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
    0x7fff31962000 -     0x7fff31986ff5  com.apple.coreservices.SharedFileList (131.4 - 131.4) <AEB4E42C-F5A2-3F63-80B0-4226483AD4F5> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SharedFileList.framework/Versions/A/SharedFileList
    0x7fff31cab000 -     0x7fff31e62ffc  com.apple.CoreText (643.1.4.4 - 643.1.4.4) <5D4EA236-DC1B-3772-95C5-7F4B6CFEAF84> /System/Library/Frameworks/CoreText.framework/Versions/A/CoreText
    0x7fff31e63000 -     0x7fff31ea7ffb  com.apple.CoreVideo (1.8 - 344.3) <8507ED54-43C3-3E5B-BC74-512FE510BF8D> /System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo
    0x7fff31ea8000 -     0x7fff31f35ffc  com.apple.framework.CoreWLAN (13.0 - 1601.2) <C1C2BBD4-EA97-3CC1-845D-1F1578E68380> /System/Library/Frameworks/CoreWLAN.framework/Versions/A/CoreWLAN
    0x7fff321cc000 -     0x7fff321d2fff  com.apple.DiskArbitration (2.7 - 2.7) <D7617B57-B01C-3848-8818-593FB12039E9> /System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
    0x7fff32507000 -     0x7fff328ccff8  com.apple.Foundation (6.9 - 1675.129) <9A74FA97-7F7B-3929-B381-D9514B1E4754> /System/Library/Frameworks/Foundation.framework/Versions/C/Foundation
    0x7fff32939000 -     0x7fff32989ff7  com.apple.GSS (4.0 - 2.0) <16DE732E-4A48-3C8A-BD61-8AF810F3A48C> /System/Library/Frameworks/GSS.framework/Versions/A/GSS
    0x7fff32ac6000 -     0x7fff32bdaff3  com.apple.Bluetooth (7.0.4 - 7.0.4f6) <9003721F-8543-3A21-BF11-2A981614F481> /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth
    0x7fff32c40000 -     0x7fff32ce4ff3  com.apple.framework.IOKit (2.0.2 - 1726.100.16) <3D8BA34A-AAF7-3AF2-9B5B-189AC4755404> /System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
    0x7fff32ce6000 -     0x7fff32cf7ffb  com.apple.IOSurface (269.11 - 269.11) <887CD3FD-1BB8-3BB7-B7F8-6A0BA4B3AEAE> /System/Library/Frameworks/IOSurface.framework/Versions/A/IOSurface
    0x7fff32d76000 -     0x7fff32ed2fee  com.apple.ImageIO.framework (3.3.0 - 1976.3.4.4) <EDAA3E6B-6D65-3807-86C2-91736BC0AF88> /System/Library/Frameworks/ImageIO.framework/Versions/A/ImageIO
    0x7fff32ed3000 -     0x7fff32ed6fff  libGIF.dylib (1976.3.4.4) <A4627958-EB22-3ADA-92BE-16229F9E9767> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libGIF.dylib
    0x7fff32ed7000 -     0x7fff32f90fff  libJP2.dylib (1976.3.4.4) <43672561-0E75-3A32-B428-697C6DA13BD8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJP2.dylib
    0x7fff32f91000 -     0x7fff32fb4fe3  libJPEG.dylib (1976.3.4.4) <52DC775B-CAB5-32B7-AC86-D9AAF7851BE9> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJPEG.dylib
    0x7fff33230000 -     0x7fff3324afef  libPng.dylib (1976.3.4.4) <0B79BE68-50CD-3C99-9CF4-2396CD203EF8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libPng.dylib
    0x7fff3324b000 -     0x7fff3324cfff  libRadiance.dylib (1976.3.4.4) <E506A652-A423-3170-8032-0B03FF367FE8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libRadiance.dylib
    0x7fff3324d000 -     0x7fff33296ffb  libTIFF.dylib (1976.3.4.4) <0419D70A-E156-3B5D-A8B0-33BA29B54A08> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libTIFF.dylib
    0x7fff347f8000 -     0x7fff3480aff3  com.apple.Kerberos (3.0 - 1) <DC673FF3-4DC9-3C23-9718-343AB36B2984> /System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
    0x7fff3480b000 -     0x7fff3480bfff  libHeimdalProxy.dylib (77) <A970C7A8-7CCD-3701-A459-078BD5E8FE4E> /System/Library/Frameworks/Kerberos.framework/Versions/A/Libraries/libHeimdalProxy.dylib
    0x7fff353c0000 -     0x7fff3548afff  com.apple.Metal (212.5.15 - 212.5.15) <2CBB178E-434E-31D3-BAE2-ED3EA801D4BC> /System/Library/Frameworks/Metal.framework/Versions/A/Metal
    0x7fff354a7000 -     0x7fff354e4ff7  com.apple.MetalPerformanceShaders.MPSCore (1.0 - 1) <5DF84B7A-9DD0-36DB-8686-D669CDA93D59> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSCore.framework/Versions/A/MPSCore
    0x7fff354e5000 -     0x7fff3556ffe2  com.apple.MetalPerformanceShaders.MPSImage (1.0 - 1) <CDC36001-66DA-3BBD-A9AA-2470B634B9C9> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSImage.framework/Versions/A/MPSImage
    0x7fff35570000 -     0x7fff35595ff4  com.apple.MetalPerformanceShaders.MPSMatrix (1.0 - 1) <1E4FE6EF-6D42-3439-835C-F4F20B05E0F5> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSMatrix.framework/Versions/A/MPSMatrix
    0x7fff35596000 -     0x7fff355abffb  com.apple.MetalPerformanceShaders.MPSNDArray (1.0 - 1) <8F8F0C2E-C4EC-3418-A06A-42B8280DDC9D> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNDArray.framework/Versions/A/MPSNDArray
    0x7fff355ac000 -     0x7fff3570affc  com.apple.MetalPerformanceShaders.MPSNeuralNetwork (1.0 - 1) <6BEFB262-2538-3A12-9E9F-A7CF94D2B68A> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNeuralNetwork.framework/Versions/A/MPSNeuralNetwork
    0x7fff3570b000 -     0x7fff3575aff4  com.apple.MetalPerformanceShaders.MPSRayIntersector (1.0 - 1) <4D352B8E-97D8-34FA-B2AF-3AB4E3149E2E> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSRayIntersector.framework/Versions/A/MPSRayIntersector
    0x7fff3575b000 -     0x7fff3575cff5  com.apple.MetalPerformanceShaders.MetalPerformanceShaders (1.0 - 1) <07F3B58C-F362-35F2-9A79-F38015A78DDA> /System/Library/Frameworks/MetalPerformanceShaders.framework/Versions/A/MetalPerformanceShaders
    0x7fff367e3000 -     0x7fff367efffe  com.apple.NetFS (6.0 - 4.0) <7A96A8FE-17F3-3850-8E81-9DDDC5A48DDB> /System/Library/Frameworks/NetFS.framework/Versions/A/NetFS
    0x7fff367f0000 -     0x7fff36947ff3  com.apple.Network (1.0 - 1) <D1C8FDDE-C822-3C40-BB26-18F24CFC8AE2> /System/Library/Frameworks/Network.framework/Versions/A/Network
    0x7fff39378000 -     0x7fff393d0fff  com.apple.opencl (3.5 - 3.5) <3F0E363C-9380-3226-A4D1-67E740079AAD> /System/Library/Frameworks/OpenCL.framework/Versions/A/OpenCL
    0x7fff393d1000 -     0x7fff393edfff  com.apple.CFOpenDirectory (10.15 - 220.40.1) <58835104-9E7A-32E8-862B-530CE899C9B4> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/Frameworks/CFOpenDirectory.framework/Versions/A/CFOpenDirectory
    0x7fff393ee000 -     0x7fff393f9ffd  com.apple.OpenDirectory (10.15 - 220.40.1) <D846BA35-59A1-3B78-B1C8-7E0EDE972AD2> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/OpenDirectory
    0x7fff39d5f000 -     0x7fff39d61fff  libCVMSPluginSupport.dylib (17.10.22) <65052150-BEFD-38D8-A789-560C2FB1644A> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCVMSPluginSupport.dylib
    0x7fff39d62000 -     0x7fff39d67fff  libCoreFSCache.dylib (176.11) <AEAEE894-BA4B-334F-90E1-7374DFB41979> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreFSCache.dylib
    0x7fff39d68000 -     0x7fff39d6cfff  libCoreVMClient.dylib (176.11) <29D2B5C2-CBFF-308A-ADD8-A559B760C494> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreVMClient.dylib
    0x7fff39d6d000 -     0x7fff39d75ff7  libGFXShared.dylib (17.10.22) <7FF5455A-3D5D-33D2-9C41-A51ABE53CE66> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGFXShared.dylib
    0x7fff39d76000 -     0x7fff39d80fff  libGL.dylib (17.10.22) <08450555-3BC8-3457-8F5E-E2BBE895C0C7> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGL.dylib
    0x7fff39d81000 -     0x7fff39db5ff7  libGLImage.dylib (17.10.22) <5182EE22-2914-30E0-A87D-C38F345F695B> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLImage.dylib
    0x7fff39f4b000 -     0x7fff39f87fff  libGLU.dylib (17.10.22) <2FE69FE7-B60D-3D05-824B-CD4958E2C7B8> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLU.dylib
    0x7fff3a9c3000 -     0x7fff3a9d2ff7  com.apple.opengl (17.10.22 - 17.10.22) <4E9C4B23-6D44-3804-AFF8-84C3B060E8F5> /System/Library/Frameworks/OpenGL.framework/Versions/A/OpenGL
    0x7fff3b990000 -     0x7fff3bc12ff2  com.apple.QuartzCore (1.11 - 840.18) <16502545-A0F3-3367-929B-DD80A6440226> /System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore
    0x7fff3c793000 -     0x7fff3cadcff1  com.apple.security (7.0 - 59306.101.1) <430E04FE-F068-3476-9CA2-72CB5F040D1F> /System/Library/Frameworks/Security.framework/Versions/A/Security
    0x7fff3cadd000 -     0x7fff3cb65ffb  com.apple.securityfoundation (6.0 - 55236.60.1) <BC15B825-955D-33CF-B416-A64D69A1D008> /System/Library/Frameworks/SecurityFoundation.framework/Versions/A/SecurityFoundation
    0x7fff3cb94000 -     0x7fff3cb98ff8  com.apple.xpc.ServiceManagement (1.0 - 1) <C66FC9CF-224B-348C-94A5-ABAC579F5C0A> /System/Library/Frameworks/ServiceManagement.framework/Versions/A/ServiceManagement
    0x7fff3d843000 -     0x7fff3d8b1ff7  com.apple.SystemConfiguration (1.19 - 1.19) <71AC15DE-7018-3D2B-B599-F2972F0288AE> /System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
    0x7fff4180e000 -     0x7fff418d3ff7  com.apple.APFS (1412.101.1 - 1412.101.1) <2F5A48FB-9788-3A24-87FE-C1B7DDBC8A07> /System/Library/PrivateFrameworks/APFS.framework/Versions/A/APFS
    0x7fff429e0000 -     0x7fff429e1ff1  com.apple.AggregateDictionary (1.0 - 1) <FE9B8728-9C37-367E-91A6-2D1321D485A0> /System/Library/PrivateFrameworks/AggregateDictionary.framework/Versions/A/AggregateDictionary
    0x7fff43476000 -     0x7fff4349affb  com.apple.framework.Apple80211 (13.0 - 1602.3) <7D1A08A0-27B0-3F53-BFC4-A2A482B055A0> /System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Apple80211
    0x7fff43758000 -     0x7fff43767fd7  com.apple.AppleFSCompression (119.100.1 - 1.0) <E1B024EB-DAB1-30A1-A43D-01D9E9357F2B> /System/Library/PrivateFrameworks/AppleFSCompression.framework/Versions/A/AppleFSCompression
    0x7fff43866000 -     0x7fff43871ff7  com.apple.AppleIDAuthSupport (1.0 - 1) <BE6A7C6D-060E-38E9-A010-61975ECE5E43> /System/Library/PrivateFrameworks/AppleIDAuthSupport.framework/Versions/A/AppleIDAuthSupport
    0x7fff438b3000 -     0x7fff438fbff7  com.apple.AppleJPEG (1.0 - 1) <C163D80A-6818-3C36-B9A9-7CC8777FE593> /System/Library/PrivateFrameworks/AppleJPEG.framework/Versions/A/AppleJPEG
    0x7fff43ce9000 -     0x7fff43d0bfff  com.apple.applesauce (1.0 - 16.25) <A6C6D37B-9AA5-3137-A02E-F61798A908B0> /System/Library/PrivateFrameworks/AppleSauce.framework/Versions/A/AppleSauce
    0x7fff43dca000 -     0x7fff43dcdffb  com.apple.AppleSystemInfo (3.1.5 - 3.1.5) <52444963-7A5E-36C8-BAAA-FFF8A0D14612> /System/Library/PrivateFrameworks/AppleSystemInfo.framework/Versions/A/AppleSystemInfo
    0x7fff43e67000 -     0x7fff43e76ff9  com.apple.AssertionServices (1.0 - 223.100.31) <2DA45CD2-C755-397C-977C-F4C6435A1272> /System/Library/PrivateFrameworks/AssertionServices.framework/Versions/A/AssertionServices
    0x7fff44a09000 -     0x7fff44c49ff0  com.apple.audio.AudioToolboxCore (1.0 - 1104.80) <EE6A2BD9-843C-3CC3-AEFC-6D7855DBB331> /System/Library/PrivateFrameworks/AudioToolboxCore.framework/Versions/A/AudioToolboxCore
    0x7fff44c4d000 -     0x7fff44d69ff3  com.apple.AuthKit (1.0 - 1) <0A3A05D4-0795-35B8-8729-4BF252D52E60> /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/AuthKit
    0x7fff44f26000 -     0x7fff44f2fff7  com.apple.coreservices.BackgroundTaskManagement (1.0 - 104) <2088BC70-5329-3390-A851-C4ECF654047C> /System/Library/PrivateFrameworks/BackgroundTaskManagement.framework/Versions/A/BackgroundTaskManagement
    0x7fff44fd2000 -     0x7fff4505eff6  com.apple.BaseBoard (466.3 - 466.3) <1EFE4339-9393-3B15-8DC9-2BE9B03F3062> /System/Library/PrivateFrameworks/BaseBoard.framework/Versions/A/BaseBoard
    0x7fff45160000 -     0x7fff4519cff7  com.apple.bom (14.0 - 219.2) <4B7C18B2-1E51-376E-9D6A-CE3F58D2AE53> /System/Library/PrivateFrameworks/Bom.framework/Versions/A/Bom
    0x7fff46bd8000 -     0x7fff46be8ffb  com.apple.CommonAuth (4.0 - 2.0) <E09BBBBE-ECDD-3442-8D4E-27A12F5E2347> /System/Library/PrivateFrameworks/CommonAuth.framework/Versions/A/CommonAuth
    0x7fff46bfc000 -     0x7fff46c13fff  com.apple.commonutilities (8.0 - 900) <1E6CE910-4B06-3704-A47D-06337A6F0992> /System/Library/PrivateFrameworks/CommonUtilities.framework/Versions/A/CommonUtilities
    0x7fff4773f000 -     0x7fff4775effc  com.apple.analyticsd (1.0 - 1) <F33987F5-A14A-3A55-8D26-FDE9A57B9269> /System/Library/PrivateFrameworks/CoreAnalytics.framework/Versions/A/CoreAnalytics
    0x7fff47ce5000 -     0x7fff47cf5ff3  com.apple.CoreEmoji (1.0 - 107) <AC83B860-61BD-384E-81BF-CA3CBE655968> /System/Library/PrivateFrameworks/CoreEmoji.framework/Versions/A/CoreEmoji
    0x7fff48335000 -     0x7fff4839fff0  com.apple.CoreNLP (1.0 - 213) <687A4C31-A307-3255-83BE-9B123971FF62> /System/Library/PrivateFrameworks/CoreNLP.framework/Versions/A/CoreNLP
    0x7fff487cd000 -     0x7fff487d5ff8  com.apple.CorePhoneNumbers (1.0 - 1) <17E6A3B0-A181-3295-8B19-E139EDF12E4B> /System/Library/PrivateFrameworks/CorePhoneNumbers.framework/Versions/A/CorePhoneNumbers
    0x7fff491c2000 -     0x7fff491e5fff  com.apple.CoreSVG (1.0 - 129) <53213F48-F888-3EBE-AE30-E9303E9B712C> /System/Library/PrivateFrameworks/CoreSVG.framework/Versions/A/CoreSVG
    0x7fff4921a000 -     0x7fff49248ffd  com.apple.CSStore (1069.22 - 1069.22) <39E431F9-3584-34DF-A64D-C5895AA72068> /System/Library/PrivateFrameworks/CoreServicesStore.framework/Versions/A/CoreServicesStore
    0x7fff4989a000 -     0x7fff499c6ff6  com.apple.coreui (2.1 - 609.4) <55EACF17-86EA-3F6E-A2CF-AF2F08C5F295> /System/Library/PrivateFrameworks/CoreUI.framework/Versions/A/CoreUI
    0x7fff499c7000 -     0x7fff49b7dff5  com.apple.CoreUtils (6.2 - 620.34) <172FC306-619F-3451-9BCA-F0B0D0B58EFD> /System/Library/PrivateFrameworks/CoreUtils.framework/Versions/A/CoreUtils
    0x7fff49cb7000 -     0x7fff49ccaff1  com.apple.CrashReporterSupport (10.13 - 15016) <8AB4A416-A174-386B-8A96-5F16EAA3FCDE> /System/Library/PrivateFrameworks/CrashReporterSupport.framework/Versions/A/CrashReporterSupport
    0x7fff49d96000 -     0x7fff49d9bfff  com.apple.DSExternalDisplay (3.1 - 380) <61597AB3-7E66-339D-A709-50D4F9B3D8E9> /System/Library/PrivateFrameworks/DSExternalDisplay.framework/Versions/A/DSExternalDisplay
    0x7fff49f29000 -     0x7fff4a0b0ff2  com.apple.desktopservices (1.14.4 - 1281.4.19) <82777143-A900-33D0-BCFA-2511C89C9EAD> /System/Library/PrivateFrameworks/DesktopServicesPriv.framework/Versions/A/DesktopServicesPriv
    0x7fff4ba49000 -     0x7fff4be64ff1  com.apple.vision.FaceCore (4.3.0 - 4.3.0) <E081D201-B82C-3AE3-8B58-1E909CE053B3> /System/Library/PrivateFrameworks/FaceCore.framework/Versions/A/FaceCore
    0x7fff4c4f5000 -     0x7fff4c62cffc  libFontParser.dylib (277.2.4.2) <B59E080A-9FC3-3511-9024-E6D5461E60D1> /System/Library/PrivateFrameworks/FontServices.framework/libFontParser.dylib
    0x7fff4c6c6000 -     0x7fff4c6d6ff6  libhvf.dylib (1.0 - $[CURRENT_PROJECT_VERSION]) <5A0F87CA-81C0-3444-B958-AAC7BD4319BC> /System/Library/PrivateFrameworks/FontServices.framework/libhvf.dylib
    0x7fff51268000 -     0x7fff5126efff  com.apple.GPUWrangler (5.1.16 - 5.1.16) <F91AD6D6-8242-348C-8296-AF1DD8DBA2EF> /System/Library/PrivateFrameworks/GPUWrangler.framework/Versions/A/GPUWrangler
    0x7fff526e2000 -     0x7fff526f0ffb  com.apple.GraphVisualizer (1.0 - 100.1) <7289AEE6-C577-3D89-A99E-98551218EB7D> /System/Library/PrivateFrameworks/GraphVisualizer.framework/Versions/A/GraphVisualizer
    0x7fff52890000 -     0x7fff5294eff4  com.apple.Heimdal (4.0 - 2.0) <C4838DCE-48FB-3828-9FB2-097BA2848C99> /System/Library/PrivateFrameworks/Heimdal.framework/Versions/A/Heimdal
    0x7fff54acf000 -     0x7fff54ad7ff5  com.apple.IOAccelerator (438.4.5 - 438.4.5) <4B2F1D11-C36B-3C48-9934-8A973348A966> /System/Library/PrivateFrameworks/IOAccelerator.framework/Versions/A/IOAccelerator
    0x7fff54ae4000 -     0x7fff54afafff  com.apple.IOPresentment (1.0 - 37) <2FE66352-4CF9-3F79-944D-053E2AD451D6> /System/Library/PrivateFrameworks/IOPresentment.framework/Versions/A/IOPresentment
    0x7fff54e82000 -     0x7fff54ecdff1  com.apple.IconServices (438.3 - 438.3) <2431AD46-37B8-367F-A1DC-119C781B1453> /System/Library/PrivateFrameworks/IconServices.framework/Versions/A/IconServices
    0x7fff5508b000 -     0x7fff55092ffa  com.apple.InternationalSupport (1.0 - 45.2) <296B6979-342E-35B8-A58B-B0797DFBA789> /System/Library/PrivateFrameworks/InternationalSupport.framework/Versions/A/InternationalSupport
    0x7fff5531f000 -     0x7fff5533effd  com.apple.security.KeychainCircle.KeychainCircle (1.0 - 1) <D0F59C6D-F069-3F0D-81C2-CBFC2E6B7101> /System/Library/PrivateFrameworks/KeychainCircle.framework/Versions/A/KeychainCircle
    0x7fff55473000 -     0x7fff55541ffd  com.apple.LanguageModeling (1.0 - 215.1) <3FAF1700-F7D4-3F92-88AA-A3920702B8BB> /System/Library/PrivateFrameworks/LanguageModeling.framework/Versions/A/LanguageModeling
    0x7fff55542000 -     0x7fff5558afff  com.apple.Lexicon-framework (1.0 - 72) <212D02CE-11BC-3C7F-BDFD-DF1A0C4017EE> /System/Library/PrivateFrameworks/Lexicon.framework/Versions/A/Lexicon
    0x7fff55591000 -     0x7fff55596ff3  com.apple.LinguisticData (1.0 - 353.18) <BA3869B7-9C39-32DA-A4BA-12F1BC4B04CF> /System/Library/PrivateFrameworks/LinguisticData.framework/Versions/A/LinguisticData
    0x7fff568fd000 -     0x7fff56949fff  com.apple.spotlight.metadata.utilities (1.0 - 2076.3) <EF8AC054-B15F-375F-AACB-018DC73CD16E> /System/Library/PrivateFrameworks/MetadataUtilities.framework/Versions/A/MetadataUtilities
    0x7fff5694a000 -     0x7fff56a1bffa  com.apple.gpusw.MetalTools (1.0 - 1) <BA343D96-58EA-374A-818C-E42968101EA8> /System/Library/PrivateFrameworks/MetalTools.framework/Versions/A/MetalTools
    0x7fff56c4e000 -     0x7fff56c6cfff  com.apple.MobileKeyBag (2.0 - 1.0) <0837C5C4-A860-387C-8F31-9A4627A3132F> /System/Library/PrivateFrameworks/MobileKeyBag.framework/Versions/A/MobileKeyBag
    0x7fff56ecf000 -     0x7fff56effff7  com.apple.MultitouchSupport.framework (3440.1 - 3440.1) <0AA68A0D-23F6-3628-A93F-8F8018B84920> /System/Library/PrivateFrameworks/MultitouchSupport.framework/Versions/A/MultitouchSupport
    0x7fff573fe000 -     0x7fff57408fff  com.apple.NetAuth (6.2 - 6.2) <D324C7CC-E614-35F6-8619-DECBE90ECAEB> /System/Library/PrivateFrameworks/NetAuth.framework/Versions/A/NetAuth
    0x7fff57e13000 -     0x7fff57e5effb  com.apple.OTSVG (1.0 - 643.1.4.4) <DCCAD72C-ED3E-3FB9-80C8-4DB36362C28A> /System/Library/PrivateFrameworks/OTSVG.framework/Versions/A/OTSVG
    0x7fff59066000 -     0x7fff59071ff2  com.apple.PerformanceAnalysis (1.243.2 - 243.2) <FFE831BE-C133-38BE-A6B4-BEEB9FD6BF37> /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/PerformanceAnalysis
    0x7fff59072000 -     0x7fff5909affb  com.apple.persistentconnection (1.0 - 1.0) <F16D4768-61F2-3298-8E37-0EAF612A55C0> /System/Library/PrivateFrameworks/PersistentConnection.framework/Versions/A/PersistentConnection
    0x7fff5ba58000 -     0x7fff5ba71ffb  com.apple.ProtocolBuffer (1 - 274.24.9.16.3) <05BE7640-A9FD-3963-8199-E60DE3C37A7E> /System/Library/PrivateFrameworks/ProtocolBuffer.framework/Versions/A/ProtocolBuffer
    0x7fff5c05e000 -     0x7fff5c099ff0  com.apple.RunningBoardServices (1.0 - 223.100.31) <28C26D68-F1F5-3ADC-832B-AF63336F35FB> /System/Library/PrivateFrameworks/RunningBoardServices.framework/Versions/A/RunningBoardServices
    0x7fff5daec000 -     0x7fff5dc13ff1  com.apple.Sharing (1526.14 - 1526.14) <8D0C1BC4-5133-399B-9EFC-74CAEF4FA389> /System/Library/PrivateFrameworks/Sharing.framework/Versions/A/Sharing
    0x7fff5f026000 -     0x7fff5f31cfff  com.apple.SkyLight (1.600.0 - 450.9) <C6AF6A79-C673-3B9E-95E0-993F43AE7EED> /System/Library/PrivateFrameworks/SkyLight.framework/Versions/A/SkyLight
    0x7fff5fb69000 -     0x7fff5fb77ffb  com.apple.SpeechRecognitionCore (6.0.91 - 6.0.91) <4678A6DB-D56E-393F-90BD-5AF4F3664440> /System/Library/PrivateFrameworks/SpeechRecognitionCore.framework/Versions/A/SpeechRecognitionCore
    0x7fff60668000 -     0x7fff60678ff3  com.apple.TCC (1.0 - 1) <AEE98D6E-03FD-3C80-90AC-5B45B4AE7A2E> /System/Library/PrivateFrameworks/TCC.framework/Versions/A/TCC
    0x7fff60b9b000 -     0x7fff60c61ff0  com.apple.TextureIO (3.10.9 - 3.10.9) <362C5815-6A2B-3CA8-B577-C5D4978EF981> /System/Library/PrivateFrameworks/TextureIO.framework/Versions/A/TextureIO
    0x7fff62d49000 -     0x7fff62d4afff  com.apple.WatchdogClient.framework (1.0 - 67.101.1) <1D6C2858-0A09-380E-8718-14131D9A0FE1> /System/Library/PrivateFrameworks/WatchdogClient.framework/Versions/A/WatchdogClient
    0x7fff639f3000 -     0x7fff63a01ff5  com.apple.audio.caulk (1.0 - 32.3) <DFE1EBB6-9A42-3227-8601-5CFCB1F665CD> /System/Library/PrivateFrameworks/caulk.framework/Versions/A/caulk
    0x7fff63d43000 -     0x7fff63d45ff3  com.apple.loginsupport (1.0 - 1) <B84ABC31-431B-3F99-****-44ED0A7DB3C0> /System/Library/PrivateFrameworks/login.framework/Versions/A/Frameworks/loginsupport.framework/Versions/A/loginsupport
    0x7fff66824000 -     0x7fff66857ffa  libAudioToolboxUtility.dylib (1104.80) <C34C8FCE-54DE-3884-8074-057B06807D22> /usr/lib/libAudioToolboxUtility.dylib
    0x7fff6685e000 -     0x7fff66892fff  libCRFSuite.dylib (48) <E52BECF7-1819-3998-ACC4-8D1A332CE4EB> /usr/lib/libCRFSuite.dylib
    0x7fff66895000 -     0x7fff6689ffff  libChineseTokenizer.dylib (34) <EE842A48-3D30-34B0-B9D2-F045DE582650> /usr/lib/libChineseTokenizer.dylib
    0x7fff6692b000 -     0x7fff6692dff7  libDiagnosticMessagesClient.dylib (112) <BE749883-9400-334A-8FBF-F3321CF205F5> /usr/lib/libDiagnosticMessagesClient.dylib
    0x7fff66973000 -     0x7fff66b2affb  libFosl_dynamic.dylib (100.4) <68038226-8CAA-36B5-B5D6-510F900B318D> /usr/lib/libFosl_dynamic.dylib
    0x7fff66b51000 -     0x7fff66b57ff3  libIOReport.dylib (54) <FA47D01E-E02C-3178-9C10-DF4E7F6351B0> /usr/lib/libIOReport.dylib
    0x7fff66c6f000 -     0x7fff66c8ffff  libMobileGestalt.dylib (826.100.27) <4B771C86-0CB7-3B06-8F41-5A40DDF66D72> /usr/lib/libMobileGestalt.dylib
    0x7fff66e01000 -     0x7fff66e02fff  libSystem.B.dylib (1281.100.1) <DB8310F1-272D-3533-A840-3B390AF55C26> /usr/lib/libSystem.B.dylib
    0x7fff66e8f000 -     0x7fff66e90fff  libThaiTokenizer.dylib (3) <DC582222-7C1F-3C27-8C3A-BAF696A2197D> /usr/lib/libThaiTokenizer.dylib
    0x7fff66ea8000 -     0x7fff66ebefff  libapple_nghttp2.dylib (1.39.2) <268F4E3E-95DC-35FB-82DC-5B0D1855A676> /usr/lib/libapple_nghttp2.dylib
    0x7fff66ef3000 -     0x7fff66f65ff7  libarchive.2.dylib (72.100.1) <65E0870E-02AB-365D-84F9-5800B5BB69FC> /usr/lib/libarchive.2.dylib
    0x7fff66f66000 -     0x7fff66ffffe5  libate.dylib (3.0.1) <4477640F-CC1B-3825-B877-69508F367E3D> /usr/lib/libate.dylib
    0x7fff67003000 -     0x7fff67003ff3  libauto.dylib (187) <FD0E5750-7004-36A7-B9C2-D6B6B4EF559B> /usr/lib/libauto.dylib
    0x7fff670c9000 -     0x7fff670d9ffb  libbsm.0.dylib (60.100.1) <B0373A39-DBC6-3A84-879B-BA46E30D04BF> /usr/lib/libbsm.0.dylib
    0x7fff670da000 -     0x7fff670e6fff  libbz2.1.0.dylib (44) <FFCD4427-AF87-36D2-8097-8870FDC75A1B> /usr/lib/libbz2.1.0.dylib
    0x7fff670e7000 -     0x7fff67139fff  libc++.1.dylib (902.1) <08199809-33CA-321E-9B9D-FD5B2BC64580> /usr/lib/libc++.1.dylib
    0x7fff6713a000 -     0x7fff6714fffb  libc++abi.dylib (902) <1C880020-396D-3F91-BE27-5A09A9239F68> /usr/lib/libc++abi.dylib
    0x7fff67150000 -     0x7fff67150fff  libcharset.1.dylib (59) <4E63BA25-04A3-329A-923D-251155C03F30> /usr/lib/libcharset.1.dylib
    0x7fff67151000 -     0x7fff67162fff  libcmph.dylib (8) <D4C5E0A8-92D9-33D5-9F83-6F4742FFBE29> /usr/lib/libcmph.dylib
    0x7fff67163000 -     0x7fff6717afd7  libcompression.dylib (87) <7F258A06-E01D-32D2-9CD2-6B2931DA5DA7> /usr/lib/libcompression.dylib
    0x7fff67454000 -     0x7fff6746aff7  libcoretls.dylib (167) <EFC237BB-78F7-33C6-BFF9-53860062DD99> /usr/lib/libcoretls.dylib
    0x7fff6746b000 -     0x7fff6746cfff  libcoretls_cfhelpers.dylib (167) <2E542A2B-7730-33EE-9B3B-154B08608AA6> /usr/lib/libcoretls_cfhelpers.dylib
    0x7fff67a29000 -     0x7fff67a88ff7  libcups.2.dylib (483.6) <F446DEF0-66C0-31AD-88E1-919B05F06C90> /usr/lib/libcups.2.dylib
    0x7fff67b94000 -     0x7fff67b94fff  libenergytrace.dylib (21) <FFB9FB70-8DBD-3025-BC92-51F02481A489> /usr/lib/libenergytrace.dylib
    0x7fff67b95000 -     0x7fff67badfff  libexpat.1.dylib (19.60.2) <1ED53818-578C-3D17-8761-68792CCAD685> /usr/lib/libexpat.1.dylib
    0x7fff67bbb000 -     0x7fff67bbdfff  libfakelink.dylib (149.1) <B04F9A05-7E52-3382-9186-F603BE4BFBB2> /usr/lib/libfakelink.dylib
    0x7fff67bcc000 -     0x7fff67bd1fff  libgermantok.dylib (24) <8091F952-B592-38E3-982B-7DEA0A44E211> /usr/lib/libgermantok.dylib
    0x7fff67bd2000 -     0x7fff67bdbff7  libheimdal-asn1.dylib (564.100.1) <2D639331-43CF-331F-98F4-CDF41990A468> /usr/lib/libheimdal-asn1.dylib
    0x7fff67bdc000 -     0x7fff67cccfff  libiconv.2.dylib (59) <9458704B-A702-37CB-9707-66ABBB5DB71E> /usr/lib/libiconv.2.dylib
    0x7fff67ccd000 -     0x7fff67f24fff  libicucore.A.dylib (64260.0.1) <DCC4A4EE-32FD-350F-84D8-E857F2F29855> /usr/lib/libicucore.A.dylib
    0x7fff67f3e000 -     0x7fff67f3ffff  liblangid.dylib (133) <E9595222-602B-38F0-8572-0F1872A00527> /usr/lib/liblangid.dylib
    0x7fff67f40000 -     0x7fff67f58ff3  liblzma.5.dylib (16) <0AA1EB11-A433-327E-B8DB-7395CFF06554> /usr/lib/liblzma.5.dylib
    0x7fff67f70000 -     0x7fff68017ff7  libmecab.dylib (883.10) <13136C11-8763-37BA-AEB2-676092798DAA> /usr/lib/libmecab.dylib
    0x7fff68018000 -     0x7fff6827afe1  libmecabra.dylib (883.10) <6AC22857-F528-35CE-94A9-D70F6F766C15> /usr/lib/libmecabra.dylib
    0x7fff685e7000 -     0x7fff68616fff  libncurses.5.4.dylib (57) <6BD6F430-C8B3-39D8-87B5-2C16E6578FD5> /usr/lib/libncurses.5.4.dylib
    0x7fff68746000 -     0x7fff68bc1ff5  libnetwork.dylib (1880.100.30) <9519B6F8-44E2-3F53-B995-1527C5333240> /usr/lib/libnetwork.dylib
    0x7fff68c61000 -     0x7fff68c94fde  libobjc.A.dylib (787.1) <20AC082F-2DB7-3974-A2D4-8C5E01787584> /usr/lib/libobjc.A.dylib
    0x7fff68ca7000 -     0x7fff68cabfff  libpam.2.dylib (25.100.1) <D5CEC1AD-A2EC-362C-B71A-22FD521917F1> /usr/lib/libpam.2.dylib
    0x7fff68cae000 -     0x7fff68ce4ff7  libpcap.A.dylib (89.100.1) <171BAAB0-A5C8-32C5-878E-83D46073BF8C> /usr/lib/libpcap.A.dylib
    0x7fff68d68000 -     0x7fff68d80fff  libresolv.9.dylib (67.40.1) <92A522F9-95E2-35EE-A8AD-FC8DEE6B2C1F> /usr/lib/libresolv.9.dylib
    0x7fff68ddc000 -     0x7fff68fc6ff7  libsqlite3.dylib (308.4) <BBC375B7-AF20-3D2C-8826-78D3BDC8A004> /usr/lib/libsqlite3.dylib
    0x7fff69217000 -     0x7fff6921affb  libutil.dylib (57) <07ED7CF0-1744-3386-B8B2-0DDBD446999E> /usr/lib/libutil.dylib
    0x7fff6921b000 -     0x7fff69228ff7  libxar.1.dylib (425.2) <625F24E1-1A0F-3301-9F99-F0F3DADE0287> /usr/lib/libxar.1.dylib
    0x7fff6922e000 -     0x7fff69310ff7  libxml2.2.dylib (33.3) <24147A90-E3EB-3926-BFB0-5F0FC9F706E2> /usr/lib/libxml2.2.dylib
    0x7fff69314000 -     0x7fff6933cfff  libxslt.1.dylib (16.9) <8C8648B1-F2CA-38EA-A409-D6F19715C6E6> /usr/lib/libxslt.1.dylib
    0x7fff6933d000 -     0x7fff6934fff3  libz.1.dylib (76) <6A449C6A-DF88-36C1-8F2D-DB9A808263B5> /usr/lib/libz.1.dylib
    0x7fff69bfd000 -     0x7fff69c02ff3  libcache.dylib (83) <5F90FFCE-403B-3724-991D-BA32401D99C5> /usr/lib/system/libcache.dylib
    0x7fff69c03000 -     0x7fff69c0efff  libcommonCrypto.dylib (60165) <C7A5E3F7-1E5A-3785-875A-B6647082B614> /usr/lib/system/libcommonCrypto.dylib
    0x7fff69c0f000 -     0x7fff69c16fff  libcompiler_rt.dylib (101.2) <A517E149-2D25-3C04-BCEF-F69149C85B18> /usr/lib/system/libcompiler_rt.dylib
    0x7fff69c17000 -     0x7fff69c20ff7  libcopyfile.dylib (166.40.1) <1A5270B5-0D97-35DA-9296-4F4A428BC6A2> /usr/lib/system/libcopyfile.dylib
    0x7fff69c21000 -     0x7fff69cb3fe3  libcorecrypto.dylib (866.100.30) <FCDEC0D1-8C30-3989-BDD1-996BBC715C29> /usr/lib/system/libcorecrypto.dylib
    0x7fff69dc0000 -     0x7fff69e00ff0  libdispatch.dylib (1173.100.2) <EB592997-B11C-3AB3-85B1-F725F3D0B412> /usr/lib/system/libdispatch.dylib
    0x7fff69e01000 -     0x7fff69e37fff  libdyld.dylib (750.5) <D2A07EF5-A64B-3692-BE13-89DAA2EC5E80> /usr/lib/system/libdyld.dylib
    0x7fff69e38000 -     0x7fff69e38ffb  libkeymgr.dylib (30) <CC5A2B43-770B-3C6C-BA10-AA3A6B4A142D> /usr/lib/system/libkeymgr.dylib
    0x7fff69e39000 -     0x7fff69e45ff3  libkxld.dylib (6153.101.6) <77282DCB-83D6-3199-874E-9A4A0FD7D4F3> /usr/lib/system/libkxld.dylib
    0x7fff69e46000 -     0x7fff69e46ff7  liblaunch.dylib (1738.100.39) <A7FF7357-600F-3014-8C28-A4F367717E8D> /usr/lib/system/liblaunch.dylib
    0x7fff69e47000 -     0x7fff69e4cff7  libmacho.dylib (959.0.1) <D8FED478-25A2-3844-AE4B-A5C9F9827615> /usr/lib/system/libmacho.dylib
    0x7fff69e4d000 -     0x7fff69e4fff3  libquarantine.dylib (110.40.3) <51E0304F-AB11-3BF7-99DC-BB916CC9088B> /usr/lib/system/libquarantine.dylib
    0x7fff69e50000 -     0x7fff69e51ff7  libremovefile.dylib (48) <078F29AB-26BA-3493-BCAA-E1E75A187521> /usr/lib/system/libremovefile.dylib
    0x7fff69e52000 -     0x7fff69e69ff3  libsystem_asl.dylib (377.60.2) <0F1BAC19-2AE0-3F8E-9B90-AACF819B2BF7> /usr/lib/system/libsystem_asl.dylib
    0x7fff69e6a000 -     0x7fff69e6aff7  libsystem_blocks.dylib (74) <32224AFF-C06F-3279-B753-097194EDEF49> /usr/lib/system/libsystem_blocks.dylib
    0x7fff69e6b000 -     0x7fff69ef2fff  libsystem_c.dylib (1353.100.2) <4F5EED22-4D46-3F04-8C64-C492CDAD70EB> /usr/lib/system/libsystem_c.dylib
    0x7fff69ef3000 -     0x7fff69ef6ffb  libsystem_configuration.dylib (1061.101.1) <2A2C778D-07EB-35C7-A954-8BF8FD74BD75> /usr/lib/system/libsystem_configuration.dylib
    0x7fff69ef7000 -     0x7fff69efafff  libsystem_coreservices.dylib (114) <FDA41CC4-170A-3D93-85BD-838A563B03C4> /usr/lib/system/libsystem_coreservices.dylib
    0x7fff69efb000 -     0x7fff69f03fff  libsystem_darwin.dylib (1353.100.2) <B567B86D-8818-38A4-A861-03EB83B55867> /usr/lib/system/libsystem_darwin.dylib
    0x7fff69f04000 -     0x7fff69f0bfff  libsystem_dnssd.dylib (1096.100.3) <7C690DF5-E119-33FB-85CD-9EFC67A36E40> /usr/lib/system/libsystem_dnssd.dylib
    0x7fff69f0c000 -     0x7fff69f0dffb  libsystem_featureflags.dylib (17) <415D83EF-084C-3485-B757-53001870EA94> /usr/lib/system/libsystem_featureflags.dylib
    0x7fff69f0e000 -     0x7fff69f5bff7  libsystem_info.dylib (538) <17049D3F-C798-3651-B391-1551FC699D3E> /usr/lib/system/libsystem_info.dylib
    0x7fff69f5c000 -     0x7fff69f88ff7  libsystem_kernel.dylib (6153.101.6) <E76440E1-D1E8-3D9A-8B47-D01F554FF1C4> /usr/lib/system/libsystem_kernel.dylib
    0x7fff69f89000 -     0x7fff69fd0fff  libsystem_m.dylib (3178) <74741FA8-5C29-3241-9046-4FC91C6A6D4A> /usr/lib/system/libsystem_m.dylib
    0x7fff69fd1000 -     0x7fff69ff8fff  libsystem_malloc.dylib (283.100.5) <97833239-2F83-3AEB-A426-0593997C8A54> /usr/lib/system/libsystem_malloc.dylib
    0x7fff69ff9000 -     0x7fff6a006ffb  libsystem_networkextension.dylib (1095.100.29) <C9E988B2-6A18-35C0-9577-63201E9D6018> /usr/lib/system/libsystem_networkextension.dylib
    0x7fff6a007000 -     0x7fff6a010ff7  libsystem_notify.dylib (241.100.2) <E405F84B-BD4F-3874-9755-CB3EC86E18D5> /usr/lib/system/libsystem_notify.dylib
    0x7fff6a011000 -     0x7fff6a019fef  libsystem_platform.dylib (220.100.1) <6EF12F34-C33F-36BF-9A9A-2A35EA19EFE0> /usr/lib/system/libsystem_platform.dylib
    0x7fff6a01a000 -     0x7fff6a024fff  libsystem_pthread.dylib (416.100.3) <A8514582-E000-3854-911A-0A73D2C79600> /usr/lib/system/libsystem_pthread.dylib
    0x7fff6a025000 -     0x7fff6a029ff3  libsystem_sandbox.dylib (1217.101.2) <E9D78CDE-FB67-32E7-BABC-9EFC23AA0DC6> /usr/lib/system/libsystem_sandbox.dylib
    0x7fff6a02a000 -     0x7fff6a02cfff  libsystem_secinit.dylib (62.100.2) <AAC639E5-7103-3366-A602-8FC6944E2C13> /usr/lib/system/libsystem_secinit.dylib
    0x7fff6a02d000 -     0x7fff6a034ffb  libsystem_symptoms.dylib (1238.100.26) <487B92DE-45F9-39F9-A478-89BBD478157D> /usr/lib/system/libsystem_symptoms.dylib
    0x7fff6a035000 -     0x7fff6a04bff2  libsystem_trace.dylib (1147.100.8) <BB90B1FD-8C09-3DF4-BD8B-9E4AEADFEA2B> /usr/lib/system/libsystem_trace.dylib
    0x7fff6a04d000 -     0x7fff6a052ff7  libunwind.dylib (35.4) <CC87C836-BE9D-334E-A0E6-0297D52E9D73> /usr/lib/system/libunwind.dylib
    0x7fff6a053000 -     0x7fff6a088ffe  libxpc.dylib (1738.100.39) <32B0E31E-9DA3-328B-A962-BC9591B93537> /usr/lib/system/libxpc.dylib

External Modification Summary:
  Calls made by other processes targeting this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by all processes on this machine:
    task_for_pid: 5257999
    thread_create: 0
    thread_set_state: 0
~~~

VM Region Summary:
ReadOnly portion of Libraries: Total=612.2M resident=0K(0%) swapped_out_or_unallocated=612.2M(100%)
Writable regions: Total=538.6M written=0K(0%) resident=0K(0%) swapped_out=0K(0%) unallocated=538.6M(100%)

~~~
                                VIRTUAL   REGION
REGION TYPE                        SIZE    COUNT (non-coalesced)
===========                     =======  =======
Activity Tracing                   256K        1
Kernel Alloc Once                    8K        1
MALLOC                           146.2M       15
MALLOC guard page                   16K        4
MALLOC_NANO (reserved)           384.0M        1         reserved VM address space (unallocated)
STACK GUARD                       56.0M        1
Stack                             8192K        1
__DATA                            16.7M      236
__DATA_CONST                        20K        1
__FONT_DATA                          4K        1
__LINKEDIT                       479.0M        3
__OBJC_RO                         32.2M        1
__OBJC_RW                         1892K        2
__TEXT                           133.1M      238
__UNICODE                          564K        1
shared memory                       12K        3
===========                     =======  =======
TOTAL                              1.2G      510
TOTAL, minus reserved VM space   874.0M      510
~~~


Do you have any idea?
Thank you very much in advance
Posted 2 years ago by  EmreGun
Copy EmreGun answer
Up vote reply of EmreGun
Down vote reply of EmreGun
Add a Comment
@ eskimo:Thank you very much for your response.
Your answers are below:

1-Does this generate a crash report?
you can see my crash reports below:

~~~
Crash Report1
Process:               test [8519]
Path:                  /Users/USER/Documents/*/test.app/Contents/MacOS/test
Identifier:            test
Version:               0.0.0 (???)
Code Type:             X86-64 (Native)
Parent Process:        ??? [8518]
User ID:               501

Date/Time:             2020-09-22 23:56:45.031 +0200
OS Version:            Mac OS X 10.15.4 (19E287)
Report Version:        12
Bridge OS Version:     4.4 (17P4281)
Anonymous UUID:        D3EEF8BC-6116-B08B-E9F2-BBC7865AD660

Sleep/Wake UUID:       67A94588-B5B4-4DE9-825B-10716AC9B467

Time Awake Since Boot: 72000 seconds
Time Since Wake:       7500 seconds

System Integrity Protection: enabled

Crashed Thread:        0  Dispatch queue: com.apple.main-thread

Exception Type:        EXC_BAD_INSTRUCTION (SIGILL)
Exception Codes:       0x0000000000000001, 0x0000000000000000
Exception Note:        EXC_CORPSE_NOTIFY

Termination Signal:    Illegal instruction: 4
Termination Reason:    Namespace SIGNAL, Code 0x4
Terminating Process:   exc handler [8519]
~~~

Application Specific Information:
~~~
dyld: launch, running initializers
/usr/lib/libSystem.B.dylib
Could not set sandbox profile data: Operation not permitted (1)

Application Specific Signatures:
SYSCALL_SET_PROFILE

Thread 0 Crashed:: Dispatch queue: com.apple.main-thread
0   libsystem_secinit.dylib       	0x00007fff6a02c11e _libsecinit_appsandbox.cold.2 + 95
1   libsystem_secinit.dylib       	0x00007fff6a02b76f _libsecinit_appsandbox + 1511
2   libsystem_secinit.dylib       	0x00007fff6a02b147 _libsecinit_initializer + 35
3   libSystem.B.dylib             	0x00007fff66e027c1 libSystem_initializer + 268
4   dyld                          	0x000000011c6e71e3 ImageLoaderMachO::doModInitFunctions(ImageLoader::LinkContext const&) + 535
5   dyld                          	0x000000011c6e75ee ImageLoaderMachO::doInitialization(ImageLoader::LinkContext const&) + 40
6   dyld                          	0x000000011c6e200b ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 493
7   dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
8   dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
9   dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
10  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
11  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
12  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
13  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
14  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
15  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
16  dyld                          	0x000000011c6e1f76 ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned int, char const*, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 344
17  dyld                          	0x000000011c6e0014 ImageLoader::processInitializers(ImageLoader::LinkContext const&, unsigned int, ImageLoader::InitializerTimingList&, ImageLoader::UninitedUpwards&) + 188
18  dyld                          	0x000000011c6e00b4 ImageLoader::runInitializers(ImageLoader::LinkContext const&, ImageLoader::InitializerTimingList&) + 82
19  dyld                          	0x000000011c6ce5e6 dyld::initializeMainExecutable() + 199
20  dyld                          	0x000000011c6d3af8 dyld::_main(macho_header const*, unsigned long, int, char const**, char const**, char const**, unsigned long*) + 6667
21  dyld                          	0x000000011c6cd227 dyldbootstrap::start(dyld3::MachOLoaded const*, int, char const**, dyld3::MachOLoaded const*, unsigned long*) + 453
22  dyld                          	0x000000011c6cd025 _dyld_start + 37

Thread 0 crashed with X86 Thread State (64-bit):
  rax: 0x00007fff6a02c91d  rbx: 0x00007ffee0230440  rcx: 0x29bcf379540e0081  rdx: 0x0000000000000000
  rdi: 0x0000000000000000  rsi: 0x00007fff6a02c91b  rbp: 0x00007ffee02303f0  rsp: 0x00007ffee02303d0
   r8: 0x00007ffee0230120   r9: 0x00007ffee02303a0  r10: 0x00007fff6a02c8f1  r11: 0xffffffff76203b63
  r12: 0x00007fe2090082d4  r13: 0x00007fe207704590  r14: 0x00007fff69eee615  r15: 0x00007fe20900825c
  rip: 0x00007fff6a02c11e  rfl: 0x0000000000010206  cr2: 0x00007fff90949ac0
  
Logical CPU:     2
Error Code:      0x00000000
Trap Number:     6


Binary Images:
       0x10f9cb000 -        0x10f9d1ff7 +test (0.0.0 - ???) <A66A43DB-0BC7-32BE-B1E3-C67B5B02215F> /Users/USER/Documents/*/test.app/Contents/MacOS/test
       0x11c6cc000 -        0x11c75deff  dyld (750.5) <1F893B81-89A5-3502-8510-95B97B9F730D> /usr/lib/dyld
    0x7fff2bae5000 -     0x7fff2bae5fff  com.apple.Accelerate (1.11 - Accelerate 1.11) <8BE0965F-6A6A-35B0-89D0-F0A75835C2CA> /System/Library/Frameworks/Accelerate.framework/Versions/A/Accelerate
    0x7fff2bafd000 -     0x7fff2c153fef  com.apple.vImage (8.1 - 524.2) <DAE0E5C5-BA70-325D-8B4C-6B821F009CBF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vImage.framework/Versions/A/vImage
    0x7fff2c154000 -     0x7fff2c3bbff7  libBLAS.dylib (1303.60.1) <4E980D6B-4B3A-33D6-B52C-AFC7D120D11A> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
    0x7fff2c3bc000 -     0x7fff2c88ffef  libBNNS.dylib (144.100.2) <C05F9F9D-4498-37BD-9C1C-2F7B920B401D> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBNNS.dylib
    0x7fff2c890000 -     0x7fff2cc2bfff  libLAPACK.dylib (1303.60.1) <F8E9D081-7C60-32EC-A47D-2D30CAD73C5F> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLAPACK.dylib
    0x7fff2cc2c000 -     0x7fff2cc41fec  libLinearAlgebra.dylib (1303.60.1) <79CB28C5-F811-3EAF-AD8E-7D7D879FE662> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLinearAlgebra.dylib
    0x7fff2cc42000 -     0x7fff2cc47ff3  libQuadrature.dylib (7) <EB7C9E98-D1E7-314C-90B4-3EB04428CC7C> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libQuadrature.dylib
    0x7fff2cc48000 -     0x7fff2ccb8fff  libSparse.dylib (103) <8C55F5F2-6AE3-393C-B2FF-22B8CFCBD7FC> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparse.dylib
    0x7fff2ccb9000 -     0x7fff2cccbfef  libSparseBLAS.dylib (1303.60.1) <08F6D629-5DAC-3A99-B261-2B6095DD38B4> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparseBLAS.dylib
    0x7fff2cccc000 -     0x7fff2cea3fd7  libvDSP.dylib (735.100.4) <0744F29B-F822-3571-9B4A-B592146D4E03> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvDSP.dylib
    0x7fff2cea4000 -     0x7fff2cf66fef  libvMisc.dylib (735.100.4) <E6C94B52-931B-3858-AF4D-C2EA52ACB7F5> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvMisc.dylib
    0x7fff2cf67000 -     0x7fff2cf67fff  com.apple.Accelerate.vecLib (3.11 - vecLib 3.11) <66282197-81EE-316F-978E-EF1471551DEF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/vecLib
    0x7fff2df20000 -     0x7fff2df20fff  com.apple.ApplicationServices (48 - 50) <CABCF23C-55E5-35E1-AAF0-EE5DDE3FDB03> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices
    0x7fff2df21000 -     0x7fff2df8cfff  com.apple.ApplicationServices.ATS (377 - 493.0.4.1) <6AA4BBCC-43AF-3EBF-8EB5-7916A3B563AA> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/ATS
    0x7fff2e025000 -     0x7fff2e063ff0  libFontRegistry.dylib (274.0.4.2) <FBF6EC26-42C0-334E-B67C-871AD50DB0BC> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/Resources/libFontRegistry.dylib
    0x7fff2e0be000 -     0x7fff2e0edfff  com.apple.ATSUI (1.0 - 1) <D8C604E9-D854-3A32-B37B-819197537A63> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATSUI.framework/Versions/A/ATSUI
    0x7fff2e0ee000 -     0x7fff2e0f2ffb  com.apple.ColorSyncLegacy (4.13.0 - 1) <2359E2CD-8FCE-32D7-AF76-F4D9A3D9D9F8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ColorSyncLegacy.framework/Versions/A/ColorSyncLegacy
    0x7fff2e18c000 -     0x7fff2e1e3ffa  com.apple.HIServices (1.22 - 675.1) <B2DEE96F-ED7A-3924-A2E2-44BB7A950BD8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/HIServices.framework/Versions/A/HIServices
    0x7fff2e1e4000 -     0x7fff2e1f2fff  com.apple.LangAnalysis (1.7.0 - 1.7.0) <1603F2CC-DC51-3E15-B6B5-0A9F9AB0C045> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/LangAnalysis.framework/Versions/A/LangAnalysis
    0x7fff2e1f3000 -     0x7fff2e238ffa  com.apple.print.framework.PrintCore (15.4 - 516.2) <525E8A4B-297B-3CAC-8A4A-6C7E211D7A21> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/PrintCore.framework/Versions/A/PrintCore
    0x7fff2e239000 -     0x7fff2e243ff7  com.apple.QD (4.0 - 413) <1EAEF5BC-D649-3E42-87BC-43CCEE4D5274> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/QD.framework/Versions/A/QD
    0x7fff2e244000 -     0x7fff2e251ffc  com.apple.speech.synthesis.framework (9.0.24 - 9.0.24) <C2E5BBFC-2EF0-3FFE-A1CF-960631DC249C> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/SpeechSynthesis.framework/Versions/A/SpeechSynthesis
    0x7fff2e252000 -     0x7fff2e333ffa  com.apple.audio.toolbox.AudioToolbox (1.14 - 1.14) <4222CBDF-D637-30DB-BA45-C6E222BABB24> /System/Library/Frameworks/AudioToolbox.framework/Versions/A/AudioToolbox
    0x7fff2e335000 -     0x7fff2e335fff  com.apple.audio.units.AudioUnit (1.14 - 1.14) <73D89D5E-05D5-3F64-BE02-2B2ED6AD6C03> /System/Library/Frameworks/AudioUnit.framework/Versions/A/AudioUnit
    0x7fff2e6c8000 -     0x7fff2ea56ffd  com.apple.CFNetwork (1125.2 - 1125.2) <1D4D81F7-FC48-3588-87FC-481E2586E345> /System/Library/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
    0x7fff2ead2000 -     0x7fff2ead2fff  com.apple.Carbon (160 - 162) <CAA294BD-BC93-384A-8415-B254C89098FC> /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon
    0x7fff2ead3000 -     0x7fff2ead6ff3  com.apple.CommonPanels (1.2.6 - 101) <63261921-DD00-312E-AFD1-C099E1984725> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/CommonPanels.framework/Versions/A/CommonPanels
    0x7fff2ead7000 -     0x7fff2edcbff3  com.apple.HIToolbox (2.1.1 - 994.6) <C03A48FC-1A02-320D-9147-F4687A1BBC6F> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/HIToolbox
    0x7fff2edcc000 -     0x7fff2edcfff3  com.apple.help (1.3.8 - 71) <F5E9EA64-5D5E-346F-98D0-D04A2F805D8D> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Help.framework/Versions/A/Help
    0x7fff2edd0000 -     0x7fff2edd5ff7  com.apple.ImageCapture (9.0 - 1600.60.4.2) <63FE9A8C-A7C6-3ABD-AA1D-EA1BC6613C39> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/ImageCapture.framework/Versions/A/ImageCapture
    0x7fff2edd6000 -     0x7fff2edd6fff  com.apple.ink.framework (10.15 - 227) <90518F56-AD8A-3627-905A-16E2B8640F87> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Ink.framework/Versions/A/Ink
    0x7fff2edd7000 -     0x7fff2edf1ffa  com.apple.openscripting (1.7 - 185.1) <718C485A-4167-3A26-B2CD-6C42B5B36D01> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/OpenScripting.framework/Versions/A/OpenScripting
    0x7fff2ee12000 -     0x7fff2ee12fff  com.apple.print.framework.Print (15 - 271) <29384C24-6667-3BAA-992A-DAD809D6387E> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Print.framework/Versions/A/Print
    0x7fff2ee13000 -     0x7fff2ee15ff7  com.apple.securityhi (9.0 - 55008) <478C57A9-D5A5-3951-B289-DA5323E9044A> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SecurityHI.framework/Versions/A/SecurityHI
    0x7fff2ee16000 -     0x7fff2ee1cfff  com.apple.speech.recognition.framework (6.0.3 - 6.0.3) <E6BE4EC1-5C53-38BB-AAFD-E7BF7A8975BC> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SpeechRecognition.framework/Versions/A/SpeechRecognition
    0x7fff2efc4000 -     0x7fff2f0bafff  com.apple.ColorSync (4.13.0 - 3394.7) <FC6CFACE-CDD8-3811-BAB6-C9F82AC0A594> /System/Library/Frameworks/ColorSync.framework/Versions/A/ColorSync
    0x7fff2f3a5000 -     0x7fff2f8aeffb  com.apple.audio.CoreAudio (5.0 - 5.0) <CF50C6CC-6753-3D64-A76B-21CE211A98E8> /System/Library/Frameworks/CoreAudio.framework/Versions/A/CoreAudio
    0x7fff2f901000 -     0x7fff2f939fff  com.apple.CoreBluetooth (1.0 - 1) <D2943204-C3A0-3C09-A7A9-BF75822678B4> /System/Library/Frameworks/CoreBluetooth.framework/Versions/A/CoreBluetooth
    0x7fff2fd25000 -     0x7fff2fe50ffe  com.apple.CoreDisplay (1.0 - 186.5.25) <53F750C6-947A-39AE-984E-41939B858A68> /System/Library/Frameworks/CoreDisplay.framework/Versions/A/CoreDisplay
    0x7fff2fe51000 -     0x7fff302d0ffb  com.apple.CoreFoundation (6.9 - 1675.129) <9E632A1E-9622-33D6-BCCE-23AC16DAA6B7> /System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
    0x7fff302d2000 -     0x7fff30946fe0  com.apple.CoreGraphics (2.0 - 1355.13) <54528FE3-21A7-3F64-B7AA-F6B95394488D> /System/Library/Frameworks/CoreGraphics.framework/Versions/A/CoreGraphics
    0x7fff30954000 -     0x7fff30cafff0  com.apple.CoreImage (15.0.0 - 940.9) <CA78A35D-E15E-3D98-BDEF-9F3D9039DB78> /System/Library/Frameworks/CoreImage.framework/Versions/A/CoreImage
    0x7fff31239000 -     0x7fff31239fff  com.apple.CoreServices (1069.22 - 1069.22) <888FE7B9-CE6C-3C7C-BA33-63364462228A> /System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
    0x7fff3123a000 -     0x7fff312bffff  com.apple.AE (838.1 - 838.1) <2BAB1B88-C198-3D20-8DA3-056E66510E7A> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE.framework/Versions/A/AE
    0x7fff312c0000 -     0x7fff315a1ff7  com.apple.CoreServices.CarbonCore (1217 - 1217) <D0FECC17-7E16-308F-98EA-AF311CB77FE6> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
    0x7fff315a2000 -     0x7fff315efffd  com.apple.DictionaryServices (1.2 - 323.6) <11513ED9-8B4B-39BB-A6B2-AA6AA0A2DF72> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/DictionaryServices.framework/Versions/A/DictionaryServices
    0x7fff315f0000 -     0x7fff315f8ff7  com.apple.CoreServices.FSEvents (1268.100.1 - 1268.100.1) <CE3D8B13-2583-3527-8532-D5DDAAD7D56B> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEvents.framework/Versions/A/FSEvents
    0x7fff315f9000 -     0x7fff31832ffc  com.apple.LaunchServices (1069.22 - 1069.22) <E51EE658-608C-3034-9635-4FDF1E241E62> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/LaunchServices
    0x7fff31833000 -     0x7fff318cbff1  com.apple.Metadata (10.7.0 - 2076.3) <EE42CCA1-FEC2-3F1C-9B62-2E73EFB05FCC> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
    0x7fff318cc000 -     0x7fff318f9fff  com.apple.CoreServices.OSServices (1069.22 - 1069.22) <A0654B4E-3194-3066-911F-FF1FBEE1D2C2> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
    0x7fff318fa000 -     0x7fff31961fff  com.apple.SearchKit (1.4.1 - 1.4.1) <D4F82BC9-FD9B-3E04-B78E-D9E2A73B0BD7> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
    0x7fff31962000 -     0x7fff31986ff5  com.apple.coreservices.SharedFileList (131.4 - 131.4) <AEB4E42C-F5A2-3F63-80B0-4226483AD4F5> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SharedFileList.framework/Versions/A/SharedFileList
    0x7fff31cab000 -     0x7fff31e62ffc  com.apple.CoreText (643.1.4.4 - 643.1.4.4) <5D4EA236-DC1B-3772-95C5-7F4B6CFEAF84> /System/Library/Frameworks/CoreText.framework/Versions/A/CoreText
    0x7fff31e63000 -     0x7fff31ea7ffb  com.apple.CoreVideo (1.8 - 344.3) <8507ED54-43C3-3E5B-BC74-512FE510BF8D> /System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo
    0x7fff31ea8000 -     0x7fff31f35ffc  com.apple.framework.CoreWLAN (13.0 - 1601.2) <C1C2BBD4-EA97-3CC1-845D-1F1578E68380> /System/Library/Frameworks/CoreWLAN.framework/Versions/A/CoreWLAN
    0x7fff321cc000 -     0x7fff321d2fff  com.apple.DiskArbitration (2.7 - 2.7) <D7617B57-B01C-3848-8818-593FB12039E9> /System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
    0x7fff32507000 -     0x7fff328ccff8  com.apple.Foundation (6.9 - 1675.129) <9A74FA97-7F7B-3929-B381-D9514B1E4754> /System/Library/Frameworks/Foundation.framework/Versions/C/Foundation
    0x7fff32939000 -     0x7fff32989ff7  com.apple.GSS (4.0 - 2.0) <16DE732E-4A48-3C8A-BD61-8AF810F3A48C> /System/Library/Frameworks/GSS.framework/Versions/A/GSS
    0x7fff32ac6000 -     0x7fff32bdaff3  com.apple.Bluetooth (7.0.4 - 7.0.4f6) <9003721F-8543-3A21-BF11-2A981614F481> /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth
    0x7fff32c40000 -     0x7fff32ce4ff3  com.apple.framework.IOKit (2.0.2 - 1726.100.16) <3D8BA34A-AAF7-3AF2-9B5B-189AC4755404> /System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
    0x7fff32ce6000 -     0x7fff32cf7ffb  com.apple.IOSurface (269.11 - 269.11) <887CD3FD-1BB8-3BB7-B7F8-6A0BA4B3AEAE> /System/Library/Frameworks/IOSurface.framework/Versions/A/IOSurface
    0x7fff32d76000 -     0x7fff32ed2fee  com.apple.ImageIO.framework (3.3.0 - 1976.3.4.4) <EDAA3E6B-6D65-3807-86C2-91736BC0AF88> /System/Library/Frameworks/ImageIO.framework/Versions/A/ImageIO
    0x7fff32ed3000 -     0x7fff32ed6fff  libGIF.dylib (1976.3.4.4) <A4627958-EB22-3ADA-92BE-16229F9E9767> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libGIF.dylib
    0x7fff32ed7000 -     0x7fff32f90fff  libJP2.dylib (1976.3.4.4) <43672561-0E75-3A32-B428-697C6DA13BD8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJP2.dylib
    0x7fff32f91000 -     0x7fff32fb4fe3  libJPEG.dylib (1976.3.4.4) <52DC775B-CAB5-32B7-AC86-D9AAF7851BE9> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJPEG.dylib
    0x7fff33230000 -     0x7fff3324afef  libPng.dylib (1976.3.4.4) <0B79BE68-50CD-3C99-9CF4-2396CD203EF8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libPng.dylib
    0x7fff3324b000 -     0x7fff3324cfff  libRadiance.dylib (1976.3.4.4) <E506A652-A423-3170-8032-0B03FF367FE8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libRadiance.dylib
    0x7fff3324d000 -     0x7fff33296ffb  libTIFF.dylib (1976.3.4.4) <0419D70A-E156-3B5D-A8B0-33BA29B54A08> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libTIFF.dylib
    0x7fff347f8000 -     0x7fff3480aff3  com.apple.Kerberos (3.0 - 1) <DC673FF3-4DC9-3C23-9718-343AB36B2984> /System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
    0x7fff3480b000 -     0x7fff3480bfff  libHeimdalProxy.dylib (77) <A970C7A8-7CCD-3701-A459-078BD5E8FE4E> /System/Library/Frameworks/Kerberos.framework/Versions/A/Libraries/libHeimdalProxy.dylib
    0x7fff353c0000 -     0x7fff3548afff  com.apple.Metal (212.5.15 - 212.5.15) <2CBB178E-434E-31D3-BAE2-ED3EA801D4BC> /System/Library/Frameworks/Metal.framework/Versions/A/Metal
    0x7fff354a7000 -     0x7fff354e4ff7  com.apple.MetalPerformanceShaders.MPSCore (1.0 - 1) <5DF84B7A-9DD0-36DB-8686-D669CDA93D59> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSCore.framework/Versions/A/MPSCore
    0x7fff354e5000 -     0x7fff3556ffe2  com.apple.MetalPerformanceShaders.MPSImage (1.0 - 1) <CDC36001-66DA-3BBD-A9AA-2470B634B9C9> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSImage.framework/Versions/A/MPSImage
    0x7fff35570000 -     0x7fff35595ff4  com.apple.MetalPerformanceShaders.MPSMatrix (1.0 - 1) <1E4FE6EF-6D42-3439-835C-F4F20B05E0F5> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSMatrix.framework/Versions/A/MPSMatrix
    0x7fff35596000 -     0x7fff355abffb  com.apple.MetalPerformanceShaders.MPSNDArray (1.0 - 1) <8F8F0C2E-C4EC-3418-A06A-42B8280DDC9D> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNDArray.framework/Versions/A/MPSNDArray
    0x7fff355ac000 -     0x7fff3570affc  com.apple.MetalPerformanceShaders.MPSNeuralNetwork (1.0 - 1) <6BEFB262-2538-3A12-9E9F-A7CF94D2B68A> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNeuralNetwork.framework/Versions/A/MPSNeuralNetwork
    0x7fff3570b000 -     0x7fff3575aff4  com.apple.MetalPerformanceShaders.MPSRayIntersector (1.0 - 1) <4D352B8E-97D8-34FA-B2AF-3AB4E3149E2E> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSRayIntersector.framework/Versions/A/MPSRayIntersector
    0x7fff3575b000 -     0x7fff3575cff5  com.apple.MetalPerformanceShaders.MetalPerformanceShaders (1.0 - 1) <07F3B58C-F362-35F2-9A79-F38015A78DDA> /System/Library/Frameworks/MetalPerformanceShaders.framework/Versions/A/MetalPerformanceShaders
    0x7fff367e3000 -     0x7fff367efffe  com.apple.NetFS (6.0 - 4.0) <7A96A8FE-17F3-3850-8E81-9DDDC5A48DDB> /System/Library/Frameworks/NetFS.framework/Versions/A/NetFS
    0x7fff367f0000 -     0x7fff36947ff3  com.apple.Network (1.0 - 1) <D1C8FDDE-C822-3C40-BB26-18F24CFC8AE2> /System/Library/Frameworks/Network.framework/Versions/A/Network
    0x7fff39378000 -     0x7fff393d0fff  com.apple.opencl (3.5 - 3.5) <3F0E363C-9380-3226-A4D1-67E740079AAD> /System/Library/Frameworks/OpenCL.framework/Versions/A/OpenCL
    0x7fff393d1000 -     0x7fff393edfff  com.apple.CFOpenDirectory (10.15 - 220.40.1) <58835104-9E7A-32E8-862B-530CE899C9B4> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/Frameworks/CFOpenDirectory.framework/Versions/A/CFOpenDirectory
    0x7fff393ee000 -     0x7fff393f9ffd  com.apple.OpenDirectory (10.15 - 220.40.1) <D846BA35-59A1-3B78-B1C8-7E0EDE972AD2> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/OpenDirectory
    0x7fff39d5f000 -     0x7fff39d61fff  libCVMSPluginSupport.dylib (17.10.22) <65052150-BEFD-38D8-A789-560C2FB1644A> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCVMSPluginSupport.dylib
    0x7fff39d62000 -     0x7fff39d67fff  libCoreFSCache.dylib (176.11) <AEAEE894-BA4B-334F-90E1-7374DFB41979> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreFSCache.dylib
    0x7fff39d68000 -     0x7fff39d6cfff  libCoreVMClient.dylib (176.11) <29D2B5C2-CBFF-308A-ADD8-A559B760C494> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreVMClient.dylib
    0x7fff39d6d000 -     0x7fff39d75ff7  libGFXShared.dylib (17.10.22) <7FF5455A-3D5D-33D2-9C41-A51ABE53CE66> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGFXShared.dylib
    0x7fff39d76000 -     0x7fff39d80fff  libGL.dylib (17.10.22) <08450555-3BC8-3457-8F5E-E2BBE895C0C7> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGL.dylib
    0x7fff39d81000 -     0x7fff39db5ff7  libGLImage.dylib (17.10.22) <5182EE22-2914-30E0-A87D-C38F345F695B> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLImage.dylib
    0x7fff39f4b000 -     0x7fff39f87fff  libGLU.dylib (17.10.22) <2FE69FE7-B60D-3D05-824B-CD4958E2C7B8> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLU.dylib
    0x7fff3a9c3000 -     0x7fff3a9d2ff7  com.apple.opengl (17.10.22 - 17.10.22) <4E9C4B23-6D44-3804-AFF8-84C3B060E8F5> /System/Library/Frameworks/OpenGL.framework/Versions/A/OpenGL
    0x7fff3b990000 -     0x7fff3bc12ff2  com.apple.QuartzCore (1.11 - 840.18) <16502545-A0F3-3367-929B-DD80A6440226> /System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore
    0x7fff3c793000 -     0x7fff3cadcff1  com.apple.security (7.0 - 59306.101.1) <430E04FE-F068-3476-9CA2-72CB5F040D1F> /System/Library/Frameworks/Security.framework/Versions/A/Security
    0x7fff3cadd000 -     0x7fff3cb65ffb  com.apple.securityfoundation (6.0 - 55236.60.1) <BC15B825-955D-33CF-B416-A64D69A1D008> /System/Library/Frameworks/SecurityFoundation.framework/Versions/A/SecurityFoundation
    0x7fff3cb94000 -     0x7fff3cb98ff8  com.apple.xpc.ServiceManagement (1.0 - 1) <C66FC9CF-224B-348C-94A5-ABAC579F5C0A> /System/Library/Frameworks/ServiceManagement.framework/Versions/A/ServiceManagement
    0x7fff3d843000 -     0x7fff3d8b1ff7  com.apple.SystemConfiguration (1.19 - 1.19) <71AC15DE-7018-3D2B-B599-F2972F0288AE> /System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
    0x7fff4180e000 -     0x7fff418d3ff7  com.apple.APFS (1412.101.1 - 1412.101.1) <2F5A48FB-9788-3A24-87FE-C1B7DDBC8A07> /System/Library/PrivateFrameworks/APFS.framework/Versions/A/APFS
    0x7fff429e0000 -     0x7fff429e1ff1  com.apple.AggregateDictionary (1.0 - 1) <FE9B8728-9C37-367E-91A6-2D1321D485A0> /System/Library/PrivateFrameworks/AggregateDictionary.framework/Versions/A/AggregateDictionary
    0x7fff43476000 -     0x7fff4349affb  com.apple.framework.Apple80211 (13.0 - 1602.3) <7D1A08A0-27B0-3F53-BFC4-A2A482B055A0> /System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Apple80211
    0x7fff43758000 -     0x7fff43767fd7  com.apple.AppleFSCompression (119.100.1 - 1.0) <E1B024EB-DAB1-30A1-A43D-01D9E9357F2B> /System/Library/PrivateFrameworks/AppleFSCompression.framework/Versions/A/AppleFSCompression
    0x7fff43866000 -     0x7fff43871ff7  com.apple.AppleIDAuthSupport (1.0 - 1) <BE6A7C6D-060E-38E9-A010-61975ECE5E43> /System/Library/PrivateFrameworks/AppleIDAuthSupport.framework/Versions/A/AppleIDAuthSupport
    0x7fff438b3000 -     0x7fff438fbff7  com.apple.AppleJPEG (1.0 - 1) <C163D80A-6818-3C36-B9A9-7CC8777FE593> /System/Library/PrivateFrameworks/AppleJPEG.framework/Versions/A/AppleJPEG
    0x7fff43ce9000 -     0x7fff43d0bfff  com.apple.applesauce (1.0 - 16.25) <A6C6D37B-9AA5-3137-A02E-F61798A908B0> /System/Library/PrivateFrameworks/AppleSauce.framework/Versions/A/AppleSauce
    0x7fff43dca000 -     0x7fff43dcdffb  com.apple.AppleSystemInfo (3.1.5 - 3.1.5) <52444963-7A5E-36C8-BAAA-FFF8A0D14612> /System/Library/PrivateFrameworks/AppleSystemInfo.framework/Versions/A/AppleSystemInfo
    0x7fff43e67000 -     0x7fff43e76ff9  com.apple.AssertionServices (1.0 - 223.100.31) <2DA45CD2-C755-397C-977C-F4C6435A1272> /System/Library/PrivateFrameworks/AssertionServices.framework/Versions/A/AssertionServices
    0x7fff44a09000 -     0x7fff44c49ff0  com.apple.audio.AudioToolboxCore (1.0 - 1104.80) <EE6A2BD9-843C-3CC3-AEFC-6D7855DBB331> /System/Library/PrivateFrameworks/AudioToolboxCore.framework/Versions/A/AudioToolboxCore
    0x7fff44c4d000 -     0x7fff44d69ff3  com.apple.AuthKit (1.0 - 1) <0A3A05D4-0795-35B8-8729-4BF252D52E60> /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/AuthKit
    0x7fff44f26000 -     0x7fff44f2fff7  com.apple.coreservices.BackgroundTaskManagement (1.0 - 104) <2088BC70-5329-3390-A851-C4ECF654047C> /System/Library/PrivateFrameworks/BackgroundTaskManagement.framework/Versions/A/BackgroundTaskManagement
    0x7fff44fd2000 -     0x7fff4505eff6  com.apple.BaseBoard (466.3 - 466.3) <1EFE4339-9393-3B15-8DC9-2BE9B03F3062> /System/Library/PrivateFrameworks/BaseBoard.framework/Versions/A/BaseBoard
    0x7fff45160000 -     0x7fff4519cff7  com.apple.bom (14.0 - 219.2) <4B7C18B2-1E51-376E-9D6A-CE3F58D2AE53> /System/Library/PrivateFrameworks/Bom.framework/Versions/A/Bom
    0x7fff46bd8000 -     0x7fff46be8ffb  com.apple.CommonAuth (4.0 - 2.0) <E09BBBBE-ECDD-3442-8D4E-27A12F5E2347> /System/Library/PrivateFrameworks/CommonAuth.framework/Versions/A/CommonAuth
    0x7fff46bfc000 -     0x7fff46c13fff  com.apple.commonutilities (8.0 - 900) <1E6CE910-4B06-3704-A47D-06337A6F0992> /System/Library/PrivateFrameworks/CommonUtilities.framework/Versions/A/CommonUtilities
    0x7fff4773f000 -     0x7fff4775effc  com.apple.analyticsd (1.0 - 1) <F33987F5-A14A-3A55-8D26-FDE9A57B9269> /System/Library/PrivateFrameworks/CoreAnalytics.framework/Versions/A/CoreAnalytics
    0x7fff47ce5000 -     0x7fff47cf5ff3  com.apple.CoreEmoji (1.0 - 107) <AC83B860-61BD-384E-81BF-CA3CBE655968> /System/Library/PrivateFrameworks/CoreEmoji.framework/Versions/A/CoreEmoji
    0x7fff48335000 -     0x7fff4839fff0  com.apple.CoreNLP (1.0 - 213) <687A4C31-A307-3255-83BE-9B123971FF62> /System/Library/PrivateFrameworks/CoreNLP.framework/Versions/A/CoreNLP
    0x7fff487cd000 -     0x7fff487d5ff8  com.apple.CorePhoneNumbers (1.0 - 1) <17E6A3B0-A181-3295-8B19-E139EDF12E4B> /System/Library/PrivateFrameworks/CorePhoneNumbers.framework/Versions/A/CorePhoneNumbers
    0x7fff491c2000 -     0x7fff491e5fff  com.apple.CoreSVG (1.0 - 129) <53213F48-F888-3EBE-AE30-E9303E9B712C> /System/Library/PrivateFrameworks/CoreSVG.framework/Versions/A/CoreSVG
    0x7fff4921a000 -     0x7fff49248ffd  com.apple.CSStore (1069.22 - 1069.22) <39E431F9-3584-34DF-A64D-C5895AA72068> /System/Library/PrivateFrameworks/CoreServicesStore.framework/Versions/A/CoreServicesStore
    0x7fff4989a000 -     0x7fff499c6ff6  com.apple.coreui (2.1 - 609.4) <55EACF17-86EA-3F6E-A2CF-AF2F08C5F295> /System/Library/PrivateFrameworks/CoreUI.framework/Versions/A/CoreUI
    0x7fff499c7000 -     0x7fff49b7dff5  com.apple.CoreUtils (6.2 - 620.34) <172FC306-619F-3451-9BCA-F0B0D0B58EFD> /System/Library/PrivateFrameworks/CoreUtils.framework/Versions/A/CoreUtils
    0x7fff49cb7000 -     0x7fff49ccaff1  com.apple.CrashReporterSupport (10.13 - 15016) <8AB4A416-A174-386B-8A96-5F16EAA3FCDE> /System/Library/PrivateFrameworks/CrashReporterSupport.framework/Versions/A/CrashReporterSupport
    0x7fff49d96000 -     0x7fff49d9bfff  com.apple.DSExternalDisplay (3.1 - 380) <61597AB3-7E66-339D-A709-50D4F9B3D8E9> /System/Library/PrivateFrameworks/DSExternalDisplay.framework/Versions/A/DSExternalDisplay
    0x7fff49f29000 -     0x7fff4a0b0ff2  com.apple.desktopservices (1.14.4 - 1281.4.19) <82777143-A900-33D0-BCFA-2511C89C9EAD> /System/Library/PrivateFrameworks/DesktopServicesPriv.framework/Versions/A/DesktopServicesPriv
    0x7fff4ba49000 -     0x7fff4be64ff1  com.apple.vision.FaceCore (4.3.0 - 4.3.0) <E081D201-B82C-3AE3-8B58-1E909CE053B3> /System/Library/PrivateFrameworks/FaceCore.framework/Versions/A/FaceCore
    0x7fff4c4f5000 -     0x7fff4c62cffc  libFontParser.dylib (277.2.4.2) <B59E080A-9FC3-3511-9024-E6D5461E60D1> /System/Library/PrivateFrameworks/FontServices.framework/libFontParser.dylib
    0x7fff4c6c6000 -     0x7fff4c6d6ff6  libhvf.dylib (1.0 - $[CURRENT_PROJECT_VERSION]) <5A0F87CA-81C0-3444-B958-AAC7BD4319BC> /System/Library/PrivateFrameworks/FontServices.framework/libhvf.dylib
    0x7fff51268000 -     0x7fff5126efff  com.apple.GPUWrangler (5.1.16 - 5.1.16) <F91AD6D6-8242-348C-8296-AF1DD8DBA2EF> /System/Library/PrivateFrameworks/GPUWrangler.framework/Versions/A/GPUWrangler
    0x7fff526e2000 -     0x7fff526f0ffb  com.apple.GraphVisualizer (1.0 - 100.1) <7289AEE6-C577-3D89-A99E-98551218EB7D> /System/Library/PrivateFrameworks/GraphVisualizer.framework/Versions/A/GraphVisualizer
    0x7fff52890000 -     0x7fff5294eff4  com.apple.Heimdal (4.0 - 2.0) <C4838DCE-48FB-3828-9FB2-097BA2848C99> /System/Library/PrivateFrameworks/Heimdal.framework/Versions/A/Heimdal
    0x7fff54acf000 -     0x7fff54ad7ff5  com.apple.IOAccelerator (438.4.5 - 438.4.5) <4B2F1D11-C36B-3C48-9934-8A973348A966> /System/Library/PrivateFrameworks/IOAccelerator.framework/Versions/A/IOAccelerator
    0x7fff54ae4000 -     0x7fff54afafff  com.apple.IOPresentment (1.0 - 37) <2FE66352-4CF9-3F79-944D-053E2AD451D6> /System/Library/PrivateFrameworks/IOPresentment.framework/Versions/A/IOPresentment
    0x7fff54e82000 -     0x7fff54ecdff1  com.apple.IconServices (438.3 - 438.3) <2431AD46-37B8-367F-A1DC-119C781B1453> /System/Library/PrivateFrameworks/IconServices.framework/Versions/A/IconServices
    0x7fff5508b000 -     0x7fff55092ffa  com.apple.InternationalSupport (1.0 - 45.2) <296B6979-342E-35B8-A58B-B0797DFBA789> /System/Library/PrivateFrameworks/InternationalSupport.framework/Versions/A/InternationalSupport
    0x7fff5531f000 -     0x7fff5533effd  com.apple.security.KeychainCircle.KeychainCircle (1.0 - 1) <D0F59C6D-F069-3F0D-81C2-CBFC2E6B7101> /System/Library/PrivateFrameworks/KeychainCircle.framework/Versions/A/KeychainCircle
    0x7fff55473000 -     0x7fff55541ffd  com.apple.LanguageModeling (1.0 - 215.1) <3FAF1700-F7D4-3F92-88AA-A3920702B8BB> /System/Library/PrivateFrameworks/LanguageModeling.framework/Versions/A/LanguageModeling
    0x7fff55542000 -     0x7fff5558afff  com.apple.Lexicon-framework (1.0 - 72) <212D02CE-11BC-3C7F-BDFD-DF1A0C4017EE> /System/Library/PrivateFrameworks/Lexicon.framework/Versions/A/Lexicon
    0x7fff55591000 -     0x7fff55596ff3  com.apple.LinguisticData (1.0 - 353.18) <BA3869B7-9C39-32DA-A4BA-12F1BC4B04CF> /System/Library/PrivateFrameworks/LinguisticData.framework/Versions/A/LinguisticData
    0x7fff568fd000 -     0x7fff56949fff  com.apple.spotlight.metadata.utilities (1.0 - 2076.3) <EF8AC054-B15F-375F-AACB-018DC73CD16E> /System/Library/PrivateFrameworks/MetadataUtilities.framework/Versions/A/MetadataUtilities
    0x7fff5694a000 -     0x7fff56a1bffa  com.apple.gpusw.MetalTools (1.0 - 1) <BA343D96-58EA-374A-818C-E42968101EA8> /System/Library/PrivateFrameworks/MetalTools.framework/Versions/A/MetalTools
    0x7fff56c4e000 -     0x7fff56c6cfff  com.apple.MobileKeyBag (2.0 - 1.0) <0837C5C4-A860-387C-8F31-9A4627A3132F> /System/Library/PrivateFrameworks/MobileKeyBag.framework/Versions/A/MobileKeyBag
    0x7fff56ecf000 -     0x7fff56effff7  com.apple.MultitouchSupport.framework (3440.1 - 3440.1) <0AA68A0D-23F6-3628-A93F-8F8018B84920> /System/Library/PrivateFrameworks/MultitouchSupport.framework/Versions/A/MultitouchSupport
    0x7fff573fe000 -     0x7fff57408fff  com.apple.NetAuth (6.2 - 6.2) <D324C7CC-E614-35F6-8619-DECBE90ECAEB> /System/Library/PrivateFrameworks/NetAuth.framework/Versions/A/NetAuth
    0x7fff57e13000 -     0x7fff57e5effb  com.apple.OTSVG (1.0 - 643.1.4.4) <DCCAD72C-ED3E-3FB9-80C8-4DB36362C28A> /System/Library/PrivateFrameworks/OTSVG.framework/Versions/A/OTSVG
    0x7fff59066000 -     0x7fff59071ff2  com.apple.PerformanceAnalysis (1.243.2 - 243.2) <FFE831BE-C133-38BE-A6B4-BEEB9FD6BF37> /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/PerformanceAnalysis
    0x7fff59072000 -     0x7fff5909affb  com.apple.persistentconnection (1.0 - 1.0) <F16D4768-61F2-3298-8E37-0EAF612A55C0> /System/Library/PrivateFrameworks/PersistentConnection.framework/Versions/A/PersistentConnection
    0x7fff5ba58000 -     0x7fff5ba71ffb  com.apple.ProtocolBuffer (1 - 274.24.9.16.3) <05BE7640-A9FD-3963-8199-E60DE3C37A7E> /System/Library/PrivateFrameworks/ProtocolBuffer.framework/Versions/A/ProtocolBuffer
    0x7fff5c05e000 -     0x7fff5c099ff0  com.apple.RunningBoardServices (1.0 - 223.100.31) <28C26D68-F1F5-3ADC-832B-AF63336F35FB> /System/Library/PrivateFrameworks/RunningBoardServices.framework/Versions/A/RunningBoardServices
    0x7fff5daec000 -     0x7fff5dc13ff1  com.apple.Sharing (1526.14 - 1526.14) <8D0C1BC4-5133-399B-9EFC-74CAEF4FA389> /System/Library/PrivateFrameworks/Sharing.framework/Versions/A/Sharing
    0x7fff5f026000 -     0x7fff5f31cfff  com.apple.SkyLight (1.600.0 - 450.9) <C6AF6A79-C673-3B9E-95E0-993F43AE7EED> /System/Library/PrivateFrameworks/SkyLight.framework/Versions/A/SkyLight
    0x7fff5fb69000 -     0x7fff5fb77ffb  com.apple.SpeechRecognitionCore (6.0.91 - 6.0.91) <4678A6DB-D56E-393F-90BD-5AF4F3664440> /System/Library/PrivateFrameworks/SpeechRecognitionCore.framework/Versions/A/SpeechRecognitionCore
    0x7fff60668000 -     0x7fff60678ff3  com.apple.TCC (1.0 - 1) <AEE98D6E-03FD-3C80-90AC-5B45B4AE7A2E> /System/Library/PrivateFrameworks/TCC.framework/Versions/A/TCC
    0x7fff60b9b000 -     0x7fff60c61ff0  com.apple.TextureIO (3.10.9 - 3.10.9) <362C5815-6A2B-3CA8-B577-C5D4978EF981> /System/Library/PrivateFrameworks/TextureIO.framework/Versions/A/TextureIO
    0x7fff62d49000 -     0x7fff62d4afff  com.apple.WatchdogClient.framework (1.0 - 67.101.1) <1D6C2858-0A09-380E-8718-14131D9A0FE1> /System/Library/PrivateFrameworks/WatchdogClient.framework/Versions/A/WatchdogClient
    0x7fff639f3000 -     0x7fff63a01ff5  com.apple.audio.caulk (1.0 - 32.3) <DFE1EBB6-9A42-3227-8601-5CFCB1F665CD> /System/Library/PrivateFrameworks/caulk.framework/Versions/A/caulk
    0x7fff63d43000 -     0x7fff63d45ff3  com.apple.loginsupport (1.0 - 1) <B84ABC31-431B-3F99-****-44ED0A7DB3C0> /System/Library/PrivateFrameworks/login.framework/Versions/A/Frameworks/loginsupport.framework/Versions/A/loginsupport
    0x7fff66824000 -     0x7fff66857ffa  libAudioToolboxUtility.dylib (1104.80) <C34C8FCE-54DE-3884-8074-057B06807D22> /usr/lib/libAudioToolboxUtility.dylib
    0x7fff6685e000 -     0x7fff66892fff  libCRFSuite.dylib (48) <E52BECF7-1819-3998-ACC4-8D1A332CE4EB> /usr/lib/libCRFSuite.dylib
    0x7fff66895000 -     0x7fff6689ffff  libChineseTokenizer.dylib (34) <EE842A48-3D30-34B0-B9D2-F045DE582650> /usr/lib/libChineseTokenizer.dylib
    0x7fff6692b000 -     0x7fff6692dff7  libDiagnosticMessagesClient.dylib (112) <BE749883-9400-334A-8FBF-F3321CF205F5> /usr/lib/libDiagnosticMessagesClient.dylib
    0x7fff66973000 -     0x7fff66b2affb  libFosl_dynamic.dylib (100.4) <68038226-8CAA-36B5-B5D6-510F900B318D> /usr/lib/libFosl_dynamic.dylib
    0x7fff66b51000 -     0x7fff66b57ff3  libIOReport.dylib (54) <FA47D01E-E02C-3178-9C10-DF4E7F6351B0> /usr/lib/libIOReport.dylib
    0x7fff66c6f000 -     0x7fff66c8ffff  libMobileGestalt.dylib (826.100.27) <4B771C86-0CB7-3B06-8F41-5A40DDF66D72> /usr/lib/libMobileGestalt.dylib
    0x7fff66e01000 -     0x7fff66e02fff  libSystem.B.dylib (1281.100.1) <DB8310F1-272D-3533-A840-3B390AF55C26> /usr/lib/libSystem.B.dylib
    0x7fff66e8f000 -     0x7fff66e90fff  libThaiTokenizer.dylib (3) <DC582222-7C1F-3C27-8C3A-BAF696A2197D> /usr/lib/libThaiTokenizer.dylib
    0x7fff66ea8000 -     0x7fff66ebefff  libapple_nghttp2.dylib (1.39.2) <268F4E3E-95DC-35FB-82DC-5B0D1855A676> /usr/lib/libapple_nghttp2.dylib
    0x7fff66ef3000 -     0x7fff66f65ff7  libarchive.2.dylib (72.100.1) <65E0870E-02AB-365D-84F9-5800B5BB69FC> /usr/lib/libarchive.2.dylib
    0x7fff66f66000 -     0x7fff66ffffe5  libate.dylib (3.0.1) <4477640F-CC1B-3825-B877-69508F367E3D> /usr/lib/libate.dylib
    0x7fff67003000 -     0x7fff67003ff3  libauto.dylib (187) <FD0E5750-7004-36A7-B9C2-D6B6B4EF559B> /usr/lib/libauto.dylib
    0x7fff670c9000 -     0x7fff670d9ffb  libbsm.0.dylib (60.100.1) <B0373A39-DBC6-3A84-879B-BA46E30D04BF> /usr/lib/libbsm.0.dylib
    0x7fff670da000 -     0x7fff670e6fff  libbz2.1.0.dylib (44) <FFCD4427-AF87-36D2-8097-8870FDC75A1B> /usr/lib/libbz2.1.0.dylib
    0x7fff670e7000 -     0x7fff67139fff  libc++.1.dylib (902.1) <08199809-33CA-321E-9B9D-FD5B2BC64580> /usr/lib/libc++.1.dylib
    0x7fff6713a000 -     0x7fff6714fffb  libc++abi.dylib (902) <1C880020-396D-3F91-BE27-5A09A9239F68> /usr/lib/libc++abi.dylib
    0x7fff67150000 -     0x7fff67150fff  libcharset.1.dylib (59) <4E63BA25-04A3-329A-923D-251155C03F30> /usr/lib/libcharset.1.dylib
    0x7fff67151000 -     0x7fff67162fff  libcmph.dylib (8) <D4C5E0A8-92D9-33D5-9F83-6F4742FFBE29> /usr/lib/libcmph.dylib
    0x7fff67163000 -     0x7fff6717afd7  libcompression.dylib (87) <7F258A06-E01D-32D2-9CD2-6B2931DA5DA7> /usr/lib/libcompression.dylib
    0x7fff67454000 -     0x7fff6746aff7  libcoretls.dylib (167) <EFC237BB-78F7-33C6-BFF9-53860062DD99> /usr/lib/libcoretls.dylib
    0x7fff6746b000 -     0x7fff6746cfff  libcoretls_cfhelpers.dylib (167) <2E542A2B-7730-33EE-9B3B-154B08608AA6> /usr/lib/libcoretls_cfhelpers.dylib
    0x7fff67a29000 -     0x7fff67a88ff7  libcups.2.dylib (483.6) <F446DEF0-66C0-31AD-88E1-919B05F06C90> /usr/lib/libcups.2.dylib
    0x7fff67b94000 -     0x7fff67b94fff  libenergytrace.dylib (21) <FFB9FB70-8DBD-3025-BC92-51F02481A489> /usr/lib/libenergytrace.dylib
    0x7fff67b95000 -     0x7fff67badfff  libexpat.1.dylib (19.60.2) <1ED53818-578C-3D17-8761-68792CCAD685> /usr/lib/libexpat.1.dylib
    0x7fff67bbb000 -     0x7fff67bbdfff  libfakelink.dylib (149.1) <B04F9A05-7E52-3382-9186-F603BE4BFBB2> /usr/lib/libfakelink.dylib
    0x7fff67bcc000 -     0x7fff67bd1fff  libgermantok.dylib (24) <8091F952-B592-38E3-982B-7DEA0A44E211> /usr/lib/libgermantok.dylib
    0x7fff67bd2000 -     0x7fff67bdbff7  libheimdal-asn1.dylib (564.100.1) <2D639331-43CF-331F-98F4-CDF41990A468> /usr/lib/libheimdal-asn1.dylib
    0x7fff67bdc000 -     0x7fff67cccfff  libiconv.2.dylib (59) <9458704B-A702-37CB-9707-66ABBB5DB71E> /usr/lib/libiconv.2.dylib
    0x7fff67ccd000 -     0x7fff67f24fff  libicucore.A.dylib (64260.0.1) <DCC4A4EE-32FD-350F-84D8-E857F2F29855> /usr/lib/libicucore.A.dylib
    0x7fff67f3e000 -     0x7fff67f3ffff  liblangid.dylib (133) <E9595222-602B-38F0-8572-0F1872A00527> /usr/lib/liblangid.dylib
    0x7fff67f40000 -     0x7fff67f58ff3  liblzma.5.dylib (16) <0AA1EB11-A433-327E-B8DB-7395CFF06554> /usr/lib/liblzma.5.dylib
    0x7fff67f70000 -     0x7fff68017ff7  libmecab.dylib (883.10) <13136C11-8763-37BA-AEB2-676092798DAA> /usr/lib/libmecab.dylib
    0x7fff68018000 -     0x7fff6827afe1  libmecabra.dylib (883.10) <6AC22857-F528-35CE-94A9-D70F6F766C15> /usr/lib/libmecabra.dylib
    0x7fff685e7000 -     0x7fff68616fff  libncurses.5.4.dylib (57) <6BD6F430-C8B3-39D8-87B5-2C16E6578FD5> /usr/lib/libncurses.5.4.dylib
    0x7fff68746000 -     0x7fff68bc1ff5  libnetwork.dylib (1880.100.30) <9519B6F8-44E2-3F53-B995-1527C5333240> /usr/lib/libnetwork.dylib
    0x7fff68c61000 -     0x7fff68c94fde  libobjc.A.dylib (787.1) <20AC082F-2DB7-3974-A2D4-8C5E01787584> /usr/lib/libobjc.A.dylib
    0x7fff68ca7000 -     0x7fff68cabfff  libpam.2.dylib (25.100.1) <D5CEC1AD-A2EC-362C-B71A-22FD521917F1> /usr/lib/libpam.2.dylib
    0x7fff68cae000 -     0x7fff68ce4ff7  libpcap.A.dylib (89.100.1) <171BAAB0-A5C8-32C5-878E-83D46073BF8C> /usr/lib/libpcap.A.dylib
    0x7fff68d68000 -     0x7fff68d80fff  libresolv.9.dylib (67.40.1) <92A522F9-95E2-35EE-A8AD-FC8DEE6B2C1F> /usr/lib/libresolv.9.dylib
    0x7fff68ddc000 -     0x7fff68fc6ff7  libsqlite3.dylib (308.4) <BBC375B7-AF20-3D2C-8826-78D3BDC8A004> /usr/lib/libsqlite3.dylib
    0x7fff69217000 -     0x7fff6921affb  libutil.dylib (57) <07ED7CF0-1744-3386-B8B2-0DDBD446999E> /usr/lib/libutil.dylib
    0x7fff6921b000 -     0x7fff69228ff7  libxar.1.dylib (425.2) <625F24E1-1A0F-3301-9F99-F0F3DADE0287> /usr/lib/libxar.1.dylib
    0x7fff6922e000 -     0x7fff69310ff7  libxml2.2.dylib (33.3) <24147A90-E3EB-3926-BFB0-5F0FC9F706E2> /usr/lib/libxml2.2.dylib
    0x7fff69314000 -     0x7fff6933cfff  libxslt.1.dylib (16.9) <8C8648B1-F2CA-38EA-A409-D6F19715C6E6> /usr/lib/libxslt.1.dylib
    0x7fff6933d000 -     0x7fff6934fff3  libz.1.dylib (76) <6A449C6A-DF88-36C1-8F2D-DB9A808263B5> /usr/lib/libz.1.dylib
    0x7fff69bfd000 -     0x7fff69c02ff3  libcache.dylib (83) <5F90FFCE-403B-3724-991D-BA32401D99C5> /usr/lib/system/libcache.dylib
    0x7fff69c03000 -     0x7fff69c0efff  libcommonCrypto.dylib (60165) <C7A5E3F7-1E5A-3785-875A-B6647082B614> /usr/lib/system/libcommonCrypto.dylib
    0x7fff69c0f000 -     0x7fff69c16fff  libcompiler_rt.dylib (101.2) <A517E149-2D25-3C04-BCEF-F69149C85B18> /usr/lib/system/libcompiler_rt.dylib
    0x7fff69c17000 -     0x7fff69c20ff7  libcopyfile.dylib (166.40.1) <1A5270B5-0D97-35DA-9296-4F4A428BC6A2> /usr/lib/system/libcopyfile.dylib
    0x7fff69c21000 -     0x7fff69cb3fe3  libcorecrypto.dylib (866.100.30) <FCDEC0D1-8C30-3989-BDD1-996BBC715C29> /usr/lib/system/libcorecrypto.dylib
    0x7fff69dc0000 -     0x7fff69e00ff0  libdispatch.dylib (1173.100.2) <EB592997-B11C-3AB3-85B1-F725F3D0B412> /usr/lib/system/libdispatch.dylib
    0x7fff69e01000 -     0x7fff69e37fff  libdyld.dylib (750.5) <D2A07EF5-A64B-3692-BE13-89DAA2EC5E80> /usr/lib/system/libdyld.dylib
    0x7fff69e38000 -     0x7fff69e38ffb  libkeymgr.dylib (30) <CC5A2B43-770B-3C6C-BA10-AA3A6B4A142D> /usr/lib/system/libkeymgr.dylib
    0x7fff69e39000 -     0x7fff69e45ff3  libkxld.dylib (6153.101.6) <77282DCB-83D6-3199-874E-9A4A0FD7D4F3> /usr/lib/system/libkxld.dylib
    0x7fff69e46000 -     0x7fff69e46ff7  liblaunch.dylib (1738.100.39) <A7FF7357-600F-3014-8C28-A4F367717E8D> /usr/lib/system/liblaunch.dylib
    0x7fff69e47000 -     0x7fff69e4cff7  libmacho.dylib (959.0.1) <D8FED478-25A2-3844-AE4B-A5C9F9827615> /usr/lib/system/libmacho.dylib
    0x7fff69e4d000 -     0x7fff69e4fff3  libquarantine.dylib (110.40.3) <51E0304F-AB11-3BF7-99DC-BB916CC9088B> /usr/lib/system/libquarantine.dylib
    0x7fff69e50000 -     0x7fff69e51ff7  libremovefile.dylib (48) <078F29AB-26BA-3493-BCAA-E1E75A187521> /usr/lib/system/libremovefile.dylib
    0x7fff69e52000 -     0x7fff69e69ff3  libsystem_asl.dylib (377.60.2) <0F1BAC19-2AE0-3F8E-9B90-AACF819B2BF7> /usr/lib/system/libsystem_asl.dylib
    0x7fff69e6a000 -     0x7fff69e6aff7  libsystem_blocks.dylib (74) <32224AFF-C06F-3279-B753-097194EDEF49> /usr/lib/system/libsystem_blocks.dylib
    0x7fff69e6b000 -     0x7fff69ef2fff  libsystem_c.dylib (1353.100.2) <4F5EED22-4D46-3F04-8C64-C492CDAD70EB> /usr/lib/system/libsystem_c.dylib
    0x7fff69ef3000 -     0x7fff69ef6ffb  libsystem_configuration.dylib (1061.101.1) <2A2C778D-07EB-35C7-A954-8BF8FD74BD75> /usr/lib/system/libsystem_configuration.dylib
    0x7fff69ef7000 -     0x7fff69efafff  libsystem_coreservices.dylib (114) <FDA41CC4-170A-3D93-85BD-838A563B03C4> /usr/lib/system/libsystem_coreservices.dylib
    0x7fff69efb000 -     0x7fff69f03fff  libsystem_darwin.dylib (1353.100.2) <B567B86D-8818-38A4-A861-03EB83B55867> /usr/lib/system/libsystem_darwin.dylib
    0x7fff69f04000 -     0x7fff69f0bfff  libsystem_dnssd.dylib (1096.100.3) <7C690DF5-E119-33FB-85CD-9EFC67A36E40> /usr/lib/system/libsystem_dnssd.dylib
    0x7fff69f0c000 -     0x7fff69f0dffb  libsystem_featureflags.dylib (17) <415D83EF-084C-3485-B757-53001870EA94> /usr/lib/system/libsystem_featureflags.dylib
    0x7fff69f0e000 -     0x7fff69f5bff7  libsystem_info.dylib (538) <17049D3F-C798-3651-B391-1551FC699D3E> /usr/lib/system/libsystem_info.dylib
    0x7fff69f5c000 -     0x7fff69f88ff7  libsystem_kernel.dylib (6153.101.6) <E76440E1-D1E8-3D9A-8B47-D01F554FF1C4> /usr/lib/system/libsystem_kernel.dylib
    0x7fff69f89000 -     0x7fff69fd0fff  libsystem_m.dylib (3178) <74741FA8-5C29-3241-9046-4FC91C6A6D4A> /usr/lib/system/libsystem_m.dylib
    0x7fff69fd1000 -     0x7fff69ff8fff  libsystem_malloc.dylib (283.100.5) <97833239-2F83-3AEB-A426-0593997C8A54> /usr/lib/system/libsystem_malloc.dylib
    0x7fff69ff9000 -     0x7fff6a006ffb  libsystem_networkextension.dylib (1095.100.29) <C9E988B2-6A18-35C0-9577-63201E9D6018> /usr/lib/system/libsystem_networkextension.dylib
    0x7fff6a007000 -     0x7fff6a010ff7  libsystem_notify.dylib (241.100.2) <E405F84B-BD4F-3874-9755-CB3EC86E18D5> /usr/lib/system/libsystem_notify.dylib
    0x7fff6a011000 -     0x7fff6a019fef  libsystem_platform.dylib (220.100.1) <6EF12F34-C33F-36BF-9A9A-2A35EA19EFE0> /usr/lib/system/libsystem_platform.dylib
    0x7fff6a01a000 -     0x7fff6a024fff  libsystem_pthread.dylib (416.100.3) <A8514582-E000-3854-911A-0A73D2C79600> /usr/lib/system/libsystem_pthread.dylib
    0x7fff6a025000 -     0x7fff6a029ff3  libsystem_sandbox.dylib (1217.101.2) <E9D78CDE-FB67-32E7-BABC-9EFC23AA0DC6> /usr/lib/system/libsystem_sandbox.dylib
    0x7fff6a02a000 -     0x7fff6a02cfff  libsystem_secinit.dylib (62.100.2) <AAC639E5-7103-3366-A602-8FC6944E2C13> /usr/lib/system/libsystem_secinit.dylib
    0x7fff6a02d000 -     0x7fff6a034ffb  libsystem_symptoms.dylib (1238.100.26) <487B92DE-45F9-39F9-A478-89BBD478157D> /usr/lib/system/libsystem_symptoms.dylib
    0x7fff6a035000 -     0x7fff6a04bff2  libsystem_trace.dylib (1147.100.8) <BB90B1FD-8C09-3DF4-BD8B-9E4AEADFEA2B> /usr/lib/system/libsystem_trace.dylib
    0x7fff6a04d000 -     0x7fff6a052ff7  libunwind.dylib (35.4) <CC87C836-BE9D-334E-A0E6-0297D52E9D73> /usr/lib/system/libunwind.dylib
    0x7fff6a053000 -     0x7fff6a088ffe  libxpc.dylib (1738.100.39) <32B0E31E-9DA3-328B-A962-BC9591B93537> /usr/lib/system/libxpc.dylib
~~~

External Modification Summary:
  Calls made by other processes targeting this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by all processes on this machine:
    task_for_pid: 5398027
    thread_create: 0
    thread_set_state: 0

VM Region Summary:
ReadOnly portion of Libraries: Total=529.2M resident=0K(0%) swapped_out_or_unallocated=529.2M(100%)
Writable regions: Total=44.6M written=0K(0%) resident=0K(0%) swapped_out=0K(0%) unallocated=44.6M(100%)
 
~~~
                                VIRTUAL   REGION 
REGION TYPE                        SIZE    COUNT (non-coalesced) 
===========                     =======  ======= 
Activity Tracing                   256K        1 
Kernel Alloc Once                    8K        1 
MALLOC                            36.1M       14 
MALLOC guard page                   16K        4 
STACK GUARD                       56.0M        1 
Stack                             8192K        1 
__DATA                            16.7M      236 
__DATA_CONST                        20K        1 
__FONT_DATA                          4K        1 
__LINKEDIT                       396.1M        3 
__OBJC_RO                         32.2M        1 
__OBJC_RW                         1892K        2 
__TEXT                           133.1M      238 
__UNICODE                          564K        1 
shared memory                       12K        3 
===========                     =======  ======= 
TOTAL                            681.0M      508 

Crash Report2
Process:               test [8518]
Path:                  /Users/USER/Documents/*/test.app/Contents/MacOS/test
Identifier:            test
Version:               0.0.0 (???)
Code Type:             X86-64 (Native)
Parent Process:        ??? [1]
Responsible:           test [8518]
User ID:               501

Date/Time:             2020-09-22 23:56:45.078 +0200
OS Version:            Mac OS X 10.15.4 (19E287)
Report Version:        12
Bridge OS Version:     4.4 (17P4281)
Anonymous UUID:        D3EEF8BC-6116-B08B-E9F2-BBC7865AD660

Sleep/Wake UUID:       67A94588-B5B4-4DE9-825B-10716AC9B467

Time Awake Since Boot: 72000 seconds
Time Since Wake:       7500 seconds

System Integrity Protection: enabled

Crashed Thread:        0  Dispatch queue: com.apple.main-thread

Exception Type:        EXC_CRASH (SIGILL)
Exception Codes:       0x0000000000000000, 0x0000000000000000
Exception Note:        EXC_CORPSE_NOTIFY

Thread 0 Crashed:: Dispatch queue: com.apple.main-thread
0   libsystem_kernel.dylib        	0x00007fff69f6333a __pthread_kill + 10
1   libsystem_pthread.dylib       	0x00007fff6a01fe60 pthread_kill + 430
2   libsystem_c.dylib             	0x00007fff69e7a93e raise + 26
3   test                          	0x000000010c1e485c 0x10c1e0000 + 18524
4   test                          	0x000000010c1e267d 0x10c1e0000 + 9853
5   test                          	0x000000010c1e0d44 0x10c1e0000 + 3396

Thread 1:: Dispatch queue: com.apple.root.utility-qos.overcommit
0   libsystem_kernel.dylib        	0x00007fff69f5cdfa mach_msg_trap + 10
1   libsystem_kernel.dylib        	0x00007fff69f5d170 mach_msg + 60
2   libxpc.dylib                  	0x00007fff6a067012 _xpc_send_serializer + 96
3   libxpc.dylib                  	0x00007fff6a06bf58 _xpc_pipe_simpleroutine + 79
4   libxpc.dylib                  	0x00007fff6a066f9a xpc_pipe_simpleroutine + 42
5   libsystem_trace.dylib         	0x00007fff6a043a2b ___os_activity_stream_reflect_block_invoke + 29
6   libdispatch.dylib             	0x00007fff69dc2658 _dispatch_client_callout + 8
7   libdispatch.dylib             	0x00007fff69dc4d79 _dispatch_block_invoke_direct + 245
8   libdispatch.dylib             	0x00007fff69dc4c79 dispatch_block_perform + 124
9   libsystem_trace.dylib         	0x00007fff6a0434d7 _os_activity_stream_reflect + 530
10  libsystem_trace.dylib         	0x00007fff6a04704e _os_log_impl_stream + 535
11  libsystem_trace.dylib         	0x00007fff6a03b84b _os_log_impl_flatten_and_send + 8502
12  libsystem_trace.dylib         	0x00007fff6a039706 _os_log + 162
13  libsystem_trace.dylib         	0x00007fff6a03c282 _os_log_debug_impl + 9
14  com.apple.LaunchServices      	0x00007fff317ab985 LaunchServices::BindingEvaluation::logComparison(LaunchServices::BindingEvaluation::State&, LaunchServices::BindingEvaluation::ExtendedBinding const&, LaunchServices::BindingEvaluation::ExtendedBinding const&, char const*, LSRelativePriority) (.cold.1) + 100
15  com.apple.LaunchServices      	0x00007fff31615f1e LaunchServices::BindingEvaluation::logComparison(LaunchServices::BindingEvaluation::State&, LaunchServices::BindingEvaluation::ExtendedBinding const&, LaunchServices::BindingEvaluation::ExtendedBinding const&, char const*, LSRelativePriority) + 565
16  com.apple.LaunchServices      	0x00007fff316157ce LaunchServices::BindingEvaluation::compareBindings(LaunchServices::BindingEvaluation::State&, LaunchServices::BindingEvaluation::ExtendedBinding const&, LaunchServices::BindingEvaluation::ExtendedBinding const&) + 363
17  com.apple.LaunchServices      	0x00007fff316134cb LaunchServices::BindingEvaluation::addAndEvaluate(LaunchServices::BindingEvaluation::State&, void (*)(LaunchServices::BindingEvaluation::State&), std::__1::vector<LaunchServices::BindingEvaluation::ExtendedBinding, std::__1::allocator<LaunchServices::BindingEvaluation::ExtendedBinding> >&) + 410
18  com.apple.LaunchServices      	0x00007fff31612d10 LaunchServices::BindingEvaluation::runEvaluator(LaunchServices::BindingEvaluation::State&, NSError* __autoreleasing*) + 360
19  com.apple.LaunchServices      	0x00007fff31612273 LaunchServices::BindingEvaluator::getBestBinding(LSContext*, NSError* __autoreleasing*) const + 105
20  com.apple.LaunchServices      	0x00007fff31611547 _LSBundleFindWithNode + 578
21  com.apple.LaunchServices      	0x00007fff31610bf4 _LSFindOrRegisterBundleNode + 202
22  com.apple.LaunchServices      	0x00007fff316190df LaunchServices::URLPropertyProvider::getAppBundleIdentifierVersionAndCapabilityFlags(LaunchServices::Database::Context&, FSNode*, LaunchServices::PrefsCapabilityInfo const*, NSString* __autoreleasing*, LSVersionNumber*, unsigned char*, NSError* __autoreleasing*) + 117
23  com.apple.LaunchServices      	0x00007fff31618b21 LaunchServices::URLPropertyProvider::prepareApplicationCapabilityValue(LaunchServices::Database::Context&, FSNode*, __FileCache*, __CFString const*, NSError* __autoreleasing*) + 281
24  com.apple.LaunchServices      	0x00007fff3160a1a9 LaunchServices::URLPropertyProvider::prepareValues(__CFURL const*, __FileCache*, __CFString const* const*, void const**, long, void const*, __CFError**) + 573
25  com.apple.CoreServicesInternal	0x00007fff491ea4c4 prepareValuesForBitmap(__CFURL const*, __FileCache*, _FilePropertyBitmap*, __CFError**) + 363
26  com.apple.CoreServicesInternal	0x00007fff491e6b9e _FSURLCopyResourcePropertyForKeyInternal(__CFURL const*, __CFString const*, void*, void*, __CFError**, unsigned char) + 221
27  com.apple.CoreFoundation      	0x00007fff2fea86b0 CFURLCopyResourcePropertyForKey + 119
28  com.apple.CoreFoundation      	0x00007fff2fea7e47 ____CFRunLoopSetOptionsReason_block_invoke_5 + 168
29  libdispatch.dylib             	0x00007fff69dc16c4 _dispatch_call_block_and_release + 12
30  libdispatch.dylib             	0x00007fff69dc2658 _dispatch_client_callout + 8
31  libdispatch.dylib             	0x00007fff69dd0bfc _dispatch_root_queue_drain + 1003
32  libdispatch.dylib             	0x00007fff69dd1097 _dispatch_worker_thread2 + 92
33  libsystem_pthread.dylib       	0x00007fff6a01c9f7 _pthread_wqthread + 220
34  libsystem_pthread.dylib       	0x00007fff6a01bb77 start_wqthread + 15

Thread 2:: Dispatch queue: Recommended browser check queue
0   libsystem_kernel.dylib        	0x00007fff69f5cdfa mach_msg_trap + 10
1   libsystem_kernel.dylib        	0x00007fff69f5d170 mach_msg + 60
2   libxpc.dylib                  	0x00007fff6a067012 _xpc_send_serializer + 96
3   libxpc.dylib                  	0x00007fff6a06bf58 _xpc_pipe_simpleroutine + 79
4   libxpc.dylib                  	0x00007fff6a066f9a xpc_pipe_simpleroutine + 42
5   libsystem_trace.dylib         	0x00007fff6a043a2b ___os_activity_stream_reflect_block_invoke + 29
6   libdispatch.dylib             	0x00007fff69dc2658 _dispatch_client_callout + 8
7   libdispatch.dylib             	0x00007fff69dc4d79 _dispatch_block_invoke_direct + 245
8   libdispatch.dylib             	0x00007fff69dc4c79 dispatch_block_perform + 124
9   libsystem_trace.dylib         	0x00007fff6a0434d7 _os_activity_stream_reflect + 530
10  libsystem_trace.dylib         	0x00007fff6a04704e _os_log_impl_stream + 535
11  libsystem_trace.dylib         	0x00007fff6a03b84b _os_log_impl_flatten_and_send + 8502
12  libsystem_trace.dylib         	0x00007fff6a039706 _os_log + 162
13  libsystem_trace.dylib         	0x00007fff6a03c282 _os_log_debug_impl + 9
14  com.apple.LaunchServices      	0x00007fff317aaf78 LaunchServices::BindingEvaluation::State::shouldBindToTagClaims() const (.cold.1) + 158
15  com.apple.LaunchServices      	0x00007fff3161a67b LaunchServices::BindingEvaluation::State::shouldBindToTagClaims() const + 747
16  com.apple.LaunchServices      	0x00007fff3161989d LaunchServices::BindingEvaluation::addClaims(LaunchServices::BindingEvaluation::State&) + 72
17  com.apple.LaunchServices      	0x00007fff316133f0 LaunchServices::BindingEvaluation::addAndEvaluate(LaunchServices::BindingEvaluation::State&, void (*)(LaunchServices::BindingEvaluation::State&), std::__1::vector<LaunchServices::BindingEvaluation::ExtendedBinding, std::__1::allocator<LaunchServices::BindingEvaluation::ExtendedBinding> >&) + 191
18  com.apple.LaunchServices      	0x00007fff31612df7 LaunchServices::BindingEvaluation::runEvaluator(LaunchServices::BindingEvaluation::State&, NSError* __autoreleasing*) + 591
19  com.apple.LaunchServices      	0x00007fff31612273 LaunchServices::BindingEvaluator::getBestBinding(LSContext*, NSError* __autoreleasing*) const + 105
20  com.apple.LaunchServices      	0x00007fff316194c5 _LSGetBindingForNodeOrSchemeOrUTI + 395
21  com.apple.LaunchServices      	0x00007fff316192af _LSCanBundleHandleNodeOrSchemeOrUTI + 123
22  com.apple.LaunchServices      	0x00007fff31618f1a _LSCanBundleHandleURL + 177
23  com.apple.LaunchServices      	0x00007fff31605eb1 LSCanURLAcceptURL + 383
24  com.apple.LaunchServices      	0x00007fff31759a72 invocation function for block in _LSRecommendSafariIfProcessIsOtherBrowser(__CFDictionary const*) + 290
25  libdispatch.dylib             	0x00007fff69dc16c4 _dispatch_call_block_and_release + 12
26  libdispatch.dylib             	0x00007fff69dc2658 _dispatch_client_callout + 8
27  libdispatch.dylib             	0x00007fff69dc7c44 _dispatch_lane_serial_drain + 597
28  libdispatch.dylib             	0x00007fff69dc8609 _dispatch_lane_invoke + 414
29  libdispatch.dylib             	0x00007fff69dd1c09 _dispatch_workloop_worker_thread + 596
30  libsystem_pthread.dylib       	0x00007fff6a01ca3d _pthread_wqthread + 290
31  libsystem_pthread.dylib       	0x00007fff6a01bb77 start_wqthread + 15

Thread 3:
0   libsystem_pthread.dylib       	0x00007fff6a01bb68 start_wqthread + 0

Thread 4:
0   libsystem_pthread.dylib       	0x00007fff6a01bb68 start_wqthread + 0

Thread 5:
0   libsystem_pthread.dylib       	0x00007fff6a01bb68 start_wqthread + 0

Thread 6:
0   libsystem_pthread.dylib       	0x00007fff6a01bb68 start_wqthread + 0

Thread 0 crashed with X86 Thread State (64-bit):
  rax: 0x0000000000000000  rbx: 0x000000010f946dc0  rcx: 0x00007ffee3a1ee58  rdx: 0x0000000000000000
  rdi: 0x0000000000000307  rsi: 0x0000000000000004  rbp: 0x00007ffee3a1ee80  rsp: 0x00007ffee3a1ee58
   r8: 0x00000000000003ff   r9: 0x00000000000007fb  r10: 0x000000010f946dc0  r11: 0x0000000000000246
  r12: 0x0000000000000307  r13: 0x000000010c1e62b3  r14: 0x0000000000000004  r15: 0x0000000000000016
  rip: 0x00007fff69f6333a  rfl: 0x0000000000000246  cr2: 0x00007fe20880b000
  
Logical CPU:     0
Error Code:      0x02000148
Trap Number:     133


Binary Images:
       0x10c1e0000 -        0x10c1e6ff7 +test (0.0.0 - ???) <A66A43DB-0BC7-32BE-B1E3-C67B5B02215F> /Users/USER/Documents/*/test.app/Contents/MacOS/test
       0x10f87c000 -        0x10f90deff  dyld (750.5) <1F893B81-89A5-3502-8510-95B97B9F730D> /usr/lib/dyld
    0x7fff2bae5000 -     0x7fff2bae5fff  com.apple.Accelerate (1.11 - Accelerate 1.11) <8BE0965F-6A6A-35B0-89D0-F0A75835C2CA> /System/Library/Frameworks/Accelerate.framework/Versions/A/Accelerate
    0x7fff2bafd000 -     0x7fff2c153fef  com.apple.vImage (8.1 - 524.2) <DAE0E5C5-BA70-325D-8B4C-6B821F009CBF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vImage.framework/Versions/A/vImage
    0x7fff2c154000 -     0x7fff2c3bbff7  libBLAS.dylib (1303.60.1) <4E980D6B-4B3A-33D6-B52C-AFC7D120D11A> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
    0x7fff2c3bc000 -     0x7fff2c88ffef  libBNNS.dylib (144.100.2) <C05F9F9D-4498-37BD-9C1C-2F7B920B401D> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBNNS.dylib
    0x7fff2c890000 -     0x7fff2cc2bfff  libLAPACK.dylib (1303.60.1) <F8E9D081-7C60-32EC-A47D-2D30CAD73C5F> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLAPACK.dylib
    0x7fff2cc2c000 -     0x7fff2cc41fec  libLinearAlgebra.dylib (1303.60.1) <79CB28C5-F811-3EAF-AD8E-7D7D879FE662> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLinearAlgebra.dylib
    0x7fff2cc42000 -     0x7fff2cc47ff3  libQuadrature.dylib (7) <EB7C9E98-D1E7-314C-90B4-3EB04428CC7C> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libQuadrature.dylib
    0x7fff2cc48000 -     0x7fff2ccb8fff  libSparse.dylib (103) <8C55F5F2-6AE3-393C-B2FF-22B8CFCBD7FC> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparse.dylib
    0x7fff2ccb9000 -     0x7fff2cccbfef  libSparseBLAS.dylib (1303.60.1) <08F6D629-5DAC-3A99-B261-2B6095DD38B4> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libSparseBLAS.dylib
    0x7fff2cccc000 -     0x7fff2cea3fd7  libvDSP.dylib (735.100.4) <0744F29B-F822-3571-9B4A-B592146D4E03> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvDSP.dylib
    0x7fff2cea4000 -     0x7fff2cf66fef  libvMisc.dylib (735.100.4) <E6C94B52-931B-3858-AF4D-C2EA52ACB7F5> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libvMisc.dylib
    0x7fff2cf67000 -     0x7fff2cf67fff  com.apple.Accelerate.vecLib (3.11 - vecLib 3.11) <66282197-81EE-316F-978E-EF1471551DEF> /System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/vecLib
    0x7fff2d111000 -     0x7fff2ded0ff5  com.apple.AppKit (6.9 - 1894.40.150) <FDDF35FF-4007-3F0B-B59C-03CFF3A0A73B> /System/Library/Frameworks/AppKit.framework/Versions/C/AppKit
    0x7fff2df20000 -     0x7fff2df20fff  com.apple.ApplicationServices (48 - 50) <CABCF23C-55E5-35E1-AAF0-EE5DDE3FDB03> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/ApplicationServices
    0x7fff2df21000 -     0x7fff2df8cfff  com.apple.ApplicationServices.ATS (377 - 493.0.4.1) <6AA4BBCC-43AF-3EBF-8EB5-7916A3B563AA> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/ATS
    0x7fff2e025000 -     0x7fff2e063ff0  libFontRegistry.dylib (274.0.4.2) <FBF6EC26-42C0-334E-B67C-871AD50DB0BC> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATS.framework/Versions/A/Resources/libFontRegistry.dylib
    0x7fff2e0be000 -     0x7fff2e0edfff  com.apple.ATSUI (1.0 - 1) <D8C604E9-D854-3A32-B37B-819197537A63> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ATSUI.framework/Versions/A/ATSUI
    0x7fff2e0ee000 -     0x7fff2e0f2ffb  com.apple.ColorSyncLegacy (4.13.0 - 1) <2359E2CD-8FCE-32D7-AF76-F4D9A3D9D9F8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/ColorSyncLegacy.framework/Versions/A/ColorSyncLegacy
    0x7fff2e18c000 -     0x7fff2e1e3ffa  com.apple.HIServices (1.22 - 675.1) <B2DEE96F-ED7A-3924-A2E2-44BB7A950BD8> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/HIServices.framework/Versions/A/HIServices
    0x7fff2e1e4000 -     0x7fff2e1f2fff  com.apple.LangAnalysis (1.7.0 - 1.7.0) <1603F2CC-DC51-3E15-B6B5-0A9F9AB0C045> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/LangAnalysis.framework/Versions/A/LangAnalysis
    0x7fff2e1f3000 -     0x7fff2e238ffa  com.apple.print.framework.PrintCore (15.4 - 516.2) <525E8A4B-297B-3CAC-8A4A-6C7E211D7A21> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/PrintCore.framework/Versions/A/PrintCore
    0x7fff2e239000 -     0x7fff2e243ff7  com.apple.QD (4.0 - 413) <1EAEF5BC-D649-3E42-87BC-43CCEE4D5274> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/QD.framework/Versions/A/QD
    0x7fff2e244000 -     0x7fff2e251ffc  com.apple.speech.synthesis.framework (9.0.24 - 9.0.24) <C2E5BBFC-2EF0-3FFE-A1CF-960631DC249C> /System/Library/Frameworks/ApplicationServices.framework/Versions/A/Frameworks/SpeechSynthesis.framework/Versions/A/SpeechSynthesis
    0x7fff2e252000 -     0x7fff2e333ffa  com.apple.audio.toolbox.AudioToolbox (1.14 - 1.14) <4222CBDF-D637-30DB-BA45-C6E222BABB24> /System/Library/Frameworks/AudioToolbox.framework/Versions/A/AudioToolbox
    0x7fff2e335000 -     0x7fff2e335fff  com.apple.audio.units.AudioUnit (1.14 - 1.14) <73D89D5E-05D5-3F64-BE02-2B2ED6AD6C03> /System/Library/Frameworks/AudioUnit.framework/Versions/A/AudioUnit
    0x7fff2e6c8000 -     0x7fff2ea56ffd  com.apple.CFNetwork (1125.2 - 1125.2) <1D4D81F7-FC48-3588-87FC-481E2586E345> /System/Library/Frameworks/CFNetwork.framework/Versions/A/CFNetwork
    0x7fff2ead2000 -     0x7fff2ead2fff  com.apple.Carbon (160 - 162) <CAA294BD-BC93-384A-8415-B254C89098FC> /System/Library/Frameworks/Carbon.framework/Versions/A/Carbon
    0x7fff2ead3000 -     0x7fff2ead6ff3  com.apple.CommonPanels (1.2.6 - 101) <63261921-DD00-312E-AFD1-C099E1984725> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/CommonPanels.framework/Versions/A/CommonPanels
    0x7fff2ead7000 -     0x7fff2edcbff3  com.apple.HIToolbox (2.1.1 - 994.6) <C03A48FC-1A02-320D-9147-F4687A1BBC6F> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/HIToolbox
    0x7fff2edcc000 -     0x7fff2edcfff3  com.apple.help (1.3.8 - 71) <F5E9EA64-5D5E-346F-98D0-D04A2F805D8D> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Help.framework/Versions/A/Help
    0x7fff2edd0000 -     0x7fff2edd5ff7  com.apple.ImageCapture (9.0 - 1600.60.4.2) <63FE9A8C-A7C6-3ABD-AA1D-EA1BC6613C39> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/ImageCapture.framework/Versions/A/ImageCapture
    0x7fff2edd6000 -     0x7fff2edd6fff  com.apple.ink.framework (10.15 - 227) <90518F56-AD8A-3627-905A-16E2B8640F87> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Ink.framework/Versions/A/Ink
    0x7fff2edd7000 -     0x7fff2edf1ffa  com.apple.openscripting (1.7 - 185.1) <718C485A-4167-3A26-B2CD-6C42B5B36D01> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/OpenScripting.framework/Versions/A/OpenScripting
    0x7fff2ee12000 -     0x7fff2ee12fff  com.apple.print.framework.Print (15 - 271) <29384C24-6667-3BAA-992A-DAD809D6387E> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/Print.framework/Versions/A/Print
    0x7fff2ee13000 -     0x7fff2ee15ff7  com.apple.securityhi (9.0 - 55008) <478C57A9-D5A5-3951-B289-DA5323E9044A> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SecurityHI.framework/Versions/A/SecurityHI
    0x7fff2ee16000 -     0x7fff2ee1cfff  com.apple.speech.recognition.framework (6.0.3 - 6.0.3) <E6BE4EC1-5C53-38BB-AAFD-E7BF7A8975BC> /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/SpeechRecognition.framework/Versions/A/SpeechRecognition
    0x7fff2efc4000 -     0x7fff2f0bafff  com.apple.ColorSync (4.13.0 - 3394.7) <FC6CFACE-CDD8-3811-BAB6-C9F82AC0A594> /System/Library/Frameworks/ColorSync.framework/Versions/A/ColorSync
    0x7fff2f3a5000 -     0x7fff2f8aeffb  com.apple.audio.CoreAudio (5.0 - 5.0) <CF50C6CC-6753-3D64-A76B-21CE211A98E8> /System/Library/Frameworks/CoreAudio.framework/Versions/A/CoreAudio
    0x7fff2f901000 -     0x7fff2f939fff  com.apple.CoreBluetooth (1.0 - 1) <D2943204-C3A0-3C09-A7A9-BF75822678B4> /System/Library/Frameworks/CoreBluetooth.framework/Versions/A/CoreBluetooth
    0x7fff2f93a000 -     0x7fff2fd24fe8  com.apple.CoreData (120 - 977.3) <9A33F390-687F-3EE2-8293-4E564A164469> /System/Library/Frameworks/CoreData.framework/Versions/A/CoreData
    0x7fff2fd25000 -     0x7fff2fe50ffe  com.apple.CoreDisplay (1.0 - 186.5.25) <53F750C6-947A-39AE-984E-41939B858A68> /System/Library/Frameworks/CoreDisplay.framework/Versions/A/CoreDisplay
    0x7fff2fe51000 -     0x7fff302d0ffb  com.apple.CoreFoundation (6.9 - 1675.129) <9E632A1E-9622-33D6-BCCE-23AC16DAA6B7> /System/Library/Frameworks/CoreFoundation.framework/Versions/A/CoreFoundation
    0x7fff302d2000 -     0x7fff30946fe0  com.apple.CoreGraphics (2.0 - 1355.13) <54528FE3-21A7-3F64-B7AA-F6B95394488D> /System/Library/Frameworks/CoreGraphics.framework/Versions/A/CoreGraphics
    0x7fff30954000 -     0x7fff30cafff0  com.apple.CoreImage (15.0.0 - 940.9) <CA78A35D-E15E-3D98-BDEF-9F3D9039DB78> /System/Library/Frameworks/CoreImage.framework/Versions/A/CoreImage
    0x7fff31239000 -     0x7fff31239fff  com.apple.CoreServices (1069.22 - 1069.22) <888FE7B9-CE6C-3C7C-BA33-63364462228A> /System/Library/Frameworks/CoreServices.framework/Versions/A/CoreServices
    0x7fff3123a000 -     0x7fff312bffff  com.apple.AE (838.1 - 838.1) <2BAB1B88-C198-3D20-8DA3-056E66510E7A> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/AE.framework/Versions/A/AE
    0x7fff312c0000 -     0x7fff315a1ff7  com.apple.CoreServices.CarbonCore (1217 - 1217) <D0FECC17-7E16-308F-98EA-AF311CB77FE6> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/CarbonCore.framework/Versions/A/CarbonCore
    0x7fff315a2000 -     0x7fff315efffd  com.apple.DictionaryServices (1.2 - 323.6) <11513ED9-8B4B-39BB-A6B2-AA6AA0A2DF72> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/DictionaryServices.framework/Versions/A/DictionaryServices
    0x7fff315f0000 -     0x7fff315f8ff7  com.apple.CoreServices.FSEvents (1268.100.1 - 1268.100.1) <CE3D8B13-2583-3527-8532-D5DDAAD7D56B> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/FSEvents.framework/Versions/A/FSEvents
    0x7fff315f9000 -     0x7fff31832ffc  com.apple.LaunchServices (1069.22 - 1069.22) <E51EE658-608C-3034-9635-4FDF1E241E62> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/LaunchServices
    0x7fff31833000 -     0x7fff318cbff1  com.apple.Metadata (10.7.0 - 2076.3) <EE42CCA1-FEC2-3F1C-9B62-2E73EFB05FCC> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/Metadata.framework/Versions/A/Metadata
    0x7fff318cc000 -     0x7fff318f9fff  com.apple.CoreServices.OSServices (1069.22 - 1069.22) <A0654B4E-3194-3066-911F-FF1FBEE1D2C2> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/OSServices.framework/Versions/A/OSServices
    0x7fff318fa000 -     0x7fff31961fff  com.apple.SearchKit (1.4.1 - 1.4.1) <D4F82BC9-FD9B-3E04-B78E-D9E2A73B0BD7> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SearchKit.framework/Versions/A/SearchKit
    0x7fff31962000 -     0x7fff31986ff5  com.apple.coreservices.SharedFileList (131.4 - 131.4) <AEB4E42C-F5A2-3F63-80B0-4226483AD4F5> /System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/SharedFileList.framework/Versions/A/SharedFileList
    0x7fff31cab000 -     0x7fff31e62ffc  com.apple.CoreText (643.1.4.4 - 643.1.4.4) <5D4EA236-DC1B-3772-95C5-7F4B6CFEAF84> /System/Library/Frameworks/CoreText.framework/Versions/A/CoreText
    0x7fff31e63000 -     0x7fff31ea7ffb  com.apple.CoreVideo (1.8 - 344.3) <8507ED54-43C3-3E5B-BC74-512FE510BF8D> /System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo
    0x7fff31ea8000 -     0x7fff31f35ffc  com.apple.framework.CoreWLAN (13.0 - 1601.2) <C1C2BBD4-EA97-3CC1-845D-1F1578E68380> /System/Library/Frameworks/CoreWLAN.framework/Versions/A/CoreWLAN
    0x7fff321cc000 -     0x7fff321d2fff  com.apple.DiskArbitration (2.7 - 2.7) <D7617B57-B01C-3848-8818-593FB12039E9> /System/Library/Frameworks/DiskArbitration.framework/Versions/A/DiskArbitration
    0x7fff32507000 -     0x7fff328ccff8  com.apple.Foundation (6.9 - 1675.129) <9A74FA97-7F7B-3929-B381-D9514B1E4754> /System/Library/Frameworks/Foundation.framework/Versions/C/Foundation
    0x7fff32939000 -     0x7fff32989ff7  com.apple.GSS (4.0 - 2.0) <16DE732E-4A48-3C8A-BD61-8AF810F3A48C> /System/Library/Frameworks/GSS.framework/Versions/A/GSS
    0x7fff32ac6000 -     0x7fff32bdaff3  com.apple.Bluetooth (7.0.4 - 7.0.4f6) <9003721F-8543-3A21-BF11-2A981614F481> /System/Library/Frameworks/IOBluetooth.framework/Versions/A/IOBluetooth
    0x7fff32c40000 -     0x7fff32ce4ff3  com.apple.framework.IOKit (2.0.2 - 1726.100.16) <3D8BA34A-AAF7-3AF2-9B5B-189AC4755404> /System/Library/Frameworks/IOKit.framework/Versions/A/IOKit
    0x7fff32ce6000 -     0x7fff32cf7ffb  com.apple.IOSurface (269.11 - 269.11) <887CD3FD-1BB8-3BB7-B7F8-6A0BA4B3AEAE> /System/Library/Frameworks/IOSurface.framework/Versions/A/IOSurface
    0x7fff32d76000 -     0x7fff32ed2fee  com.apple.ImageIO.framework (3.3.0 - 1976.3.4.4) <EDAA3E6B-6D65-3807-86C2-91736BC0AF88> /System/Library/Frameworks/ImageIO.framework/Versions/A/ImageIO
    0x7fff32ed3000 -     0x7fff32ed6fff  libGIF.dylib (1976.3.4.4) <A4627958-EB22-3ADA-92BE-16229F9E9767> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libGIF.dylib
    0x7fff32ed7000 -     0x7fff32f90fff  libJP2.dylib (1976.3.4.4) <43672561-0E75-3A32-B428-697C6DA13BD8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJP2.dylib
    0x7fff32f91000 -     0x7fff32fb4fe3  libJPEG.dylib (1976.3.4.4) <52DC775B-CAB5-32B7-AC86-D9AAF7851BE9> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libJPEG.dylib
    0x7fff33230000 -     0x7fff3324afef  libPng.dylib (1976.3.4.4) <0B79BE68-50CD-3C99-9CF4-2396CD203EF8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libPng.dylib
    0x7fff3324b000 -     0x7fff3324cfff  libRadiance.dylib (1976.3.4.4) <E506A652-A423-3170-8032-0B03FF367FE8> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libRadiance.dylib
    0x7fff3324d000 -     0x7fff33296ffb  libTIFF.dylib (1976.3.4.4) <0419D70A-E156-3B5D-A8B0-33BA29B54A08> /System/Library/Frameworks/ImageIO.framework/Versions/A/Resources/libTIFF.dylib
    0x7fff347f8000 -     0x7fff3480aff3  com.apple.Kerberos (3.0 - 1) <DC673FF3-4DC9-3C23-9718-343AB36B2984> /System/Library/Frameworks/Kerberos.framework/Versions/A/Kerberos
    0x7fff3480b000 -     0x7fff3480bfff  libHeimdalProxy.dylib (77) <A970C7A8-7CCD-3701-A459-078BD5E8FE4E> /System/Library/Frameworks/Kerberos.framework/Versions/A/Libraries/libHeimdalProxy.dylib
    0x7fff353c0000 -     0x7fff3548afff  com.apple.Metal (212.5.15 - 212.5.15) <2CBB178E-434E-31D3-BAE2-ED3EA801D4BC> /System/Library/Frameworks/Metal.framework/Versions/A/Metal
    0x7fff354a7000 -     0x7fff354e4ff7  com.apple.MetalPerformanceShaders.MPSCore (1.0 - 1) <5DF84B7A-9DD0-36DB-8686-D669CDA93D59> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSCore.framework/Versions/A/MPSCore
    0x7fff354e5000 -     0x7fff3556ffe2  com.apple.MetalPerformanceShaders.MPSImage (1.0 - 1) <CDC36001-66DA-3BBD-A9AA-2470B634B9C9> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSImage.framework/Versions/A/MPSImage
    0x7fff35570000 -     0x7fff35595ff4  com.apple.MetalPerformanceShaders.MPSMatrix (1.0 - 1) <1E4FE6EF-6D42-3439-835C-F4F20B05E0F5> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSMatrix.framework/Versions/A/MPSMatrix
    0x7fff35596000 -     0x7fff355abffb  com.apple.MetalPerformanceShaders.MPSNDArray (1.0 - 1) <8F8F0C2E-C4EC-3418-A06A-42B8280DDC9D> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNDArray.framework/Versions/A/MPSNDArray
    0x7fff355ac000 -     0x7fff3570affc  com.apple.MetalPerformanceShaders.MPSNeuralNetwork (1.0 - 1) <6BEFB262-2538-3A12-9E9F-A7CF94D2B68A> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSNeuralNetwork.framework/Versions/A/MPSNeuralNetwork
    0x7fff3570b000 -     0x7fff3575aff4  com.apple.MetalPerformanceShaders.MPSRayIntersector (1.0 - 1) <4D352B8E-97D8-34FA-B2AF-3AB4E3149E2E> /System/Library/Frameworks/MetalPerformanceShaders.framework/Frameworks/MPSRayIntersector.framework/Versions/A/MPSRayIntersector
    0x7fff3575b000 -     0x7fff3575cff5  com.apple.MetalPerformanceShaders.MetalPerformanceShaders (1.0 - 1) <07F3B58C-F362-35F2-9A79-F38015A78DDA> /System/Library/Frameworks/MetalPerformanceShaders.framework/Versions/A/MetalPerformanceShaders
    0x7fff367e3000 -     0x7fff367efffe  com.apple.NetFS (6.0 - 4.0) <7A96A8FE-17F3-3850-8E81-9DDDC5A48DDB> /System/Library/Frameworks/NetFS.framework/Versions/A/NetFS
    0x7fff367f0000 -     0x7fff36947ff3  com.apple.Network (1.0 - 1) <D1C8FDDE-C822-3C40-BB26-18F24CFC8AE2> /System/Library/Frameworks/Network.framework/Versions/A/Network
    0x7fff39378000 -     0x7fff393d0fff  com.apple.opencl (3.5 - 3.5) <3F0E363C-9380-3226-A4D1-67E740079AAD> /System/Library/Frameworks/OpenCL.framework/Versions/A/OpenCL
    0x7fff393d1000 -     0x7fff393edfff  com.apple.CFOpenDirectory (10.15 - 220.40.1) <58835104-9E7A-32E8-862B-530CE899C9B4> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/Frameworks/CFOpenDirectory.framework/Versions/A/CFOpenDirectory
    0x7fff393ee000 -     0x7fff393f9ffd  com.apple.OpenDirectory (10.15 - 220.40.1) <D846BA35-59A1-3B78-B1C8-7E0EDE972AD2> /System/Library/Frameworks/OpenDirectory.framework/Versions/A/OpenDirectory
    0x7fff39d5f000 -     0x7fff39d61fff  libCVMSPluginSupport.dylib (17.10.22) <65052150-BEFD-38D8-A789-560C2FB1644A> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCVMSPluginSupport.dylib
    0x7fff39d62000 -     0x7fff39d67fff  libCoreFSCache.dylib (176.11) <AEAEE894-BA4B-334F-90E1-7374DFB41979> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreFSCache.dylib
    0x7fff39d68000 -     0x7fff39d6cfff  libCoreVMClient.dylib (176.11) <29D2B5C2-CBFF-308A-ADD8-A559B760C494> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libCoreVMClient.dylib
    0x7fff39d6d000 -     0x7fff39d75ff7  libGFXShared.dylib (17.10.22) <7FF5455A-3D5D-33D2-9C41-A51ABE53CE66> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGFXShared.dylib
    0x7fff39d76000 -     0x7fff39d80fff  libGL.dylib (17.10.22) <08450555-3BC8-3457-8F5E-E2BBE895C0C7> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGL.dylib
    0x7fff39d81000 -     0x7fff39db5ff7  libGLImage.dylib (17.10.22) <5182EE22-2914-30E0-A87D-C38F345F695B> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLImage.dylib
    0x7fff39f4b000 -     0x7fff39f87fff  libGLU.dylib (17.10.22) <2FE69FE7-B60D-3D05-824B-CD4958E2C7B8> /System/Library/Frameworks/OpenGL.framework/Versions/A/Libraries/libGLU.dylib
    0x7fff3a9c3000 -     0x7fff3a9d2ff7  com.apple.opengl (17.10.22 - 17.10.22) <4E9C4B23-6D44-3804-AFF8-84C3B060E8F5> /System/Library/Frameworks/OpenGL.framework/Versions/A/OpenGL
    0x7fff3b990000 -     0x7fff3bc12ff2  com.apple.QuartzCore (1.11 - 840.18) <16502545-A0F3-3367-929B-DD80A6440226> /System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore
    0x7fff3c793000 -     0x7fff3cadcff1  com.apple.security (7.0 - 59306.101.1) <430E04FE-F068-3476-9CA2-72CB5F040D1F> /System/Library/Frameworks/Security.framework/Versions/A/Security
    0x7fff3cadd000 -     0x7fff3cb65ffb  com.apple.securityfoundation (6.0 - 55236.60.1) <BC15B825-955D-33CF-B416-A64D69A1D008> /System/Library/Frameworks/SecurityFoundation.framework/Versions/A/SecurityFoundation
    0x7fff3cb94000 -     0x7fff3cb98ff8  com.apple.xpc.ServiceManagement (1.0 - 1) <C66FC9CF-224B-348C-94A5-ABAC579F5C0A> /System/Library/Frameworks/ServiceManagement.framework/Versions/A/ServiceManagement
    0x7fff3d843000 -     0x7fff3d8b1ff7  com.apple.SystemConfiguration (1.19 - 1.19) <71AC15DE-7018-3D2B-B599-F2972F0288AE> /System/Library/Frameworks/SystemConfiguration.framework/Versions/A/SystemConfiguration
    0x7fff4180e000 -     0x7fff418d3ff7  com.apple.APFS (1412.101.1 - 1412.101.1) <2F5A48FB-9788-3A24-87FE-C1B7DDBC8A07> /System/Library/PrivateFrameworks/APFS.framework/Versions/A/APFS
    0x7fff429e0000 -     0x7fff429e1ff1  com.apple.AggregateDictionary (1.0 - 1) <FE9B8728-9C37-367E-91A6-2D1321D485A0> /System/Library/PrivateFrameworks/AggregateDictionary.framework/Versions/A/AggregateDictionary
    0x7fff43476000 -     0x7fff4349affb  com.apple.framework.Apple80211 (13.0 - 1602.3) <7D1A08A0-27B0-3F53-BFC4-A2A482B055A0> /System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Apple80211
    0x7fff43758000 -     0x7fff43767fd7  com.apple.AppleFSCompression (119.100.1 - 1.0) <E1B024EB-DAB1-30A1-A43D-01D9E9357F2B> /System/Library/PrivateFrameworks/AppleFSCompression.framework/Versions/A/AppleFSCompression
    0x7fff43866000 -     0x7fff43871ff7  com.apple.AppleIDAuthSupport (1.0 - 1) <BE6A7C6D-060E-38E9-A010-61975ECE5E43> /System/Library/PrivateFrameworks/AppleIDAuthSupport.framework/Versions/A/AppleIDAuthSupport
    0x7fff438b3000 -     0x7fff438fbff7  com.apple.AppleJPEG (1.0 - 1) <C163D80A-6818-3C36-B9A9-7CC8777FE593> /System/Library/PrivateFrameworks/AppleJPEG.framework/Versions/A/AppleJPEG
    0x7fff43ce9000 -     0x7fff43d0bfff  com.apple.applesauce (1.0 - 16.25) <A6C6D37B-9AA5-3137-A02E-F61798A908B0> /System/Library/PrivateFrameworks/AppleSauce.framework/Versions/A/AppleSauce
    0x7fff43dca000 -     0x7fff43dcdffb  com.apple.AppleSystemInfo (3.1.5 - 3.1.5) <52444963-7A5E-36C8-BAAA-FFF8A0D14612> /System/Library/PrivateFrameworks/AppleSystemInfo.framework/Versions/A/AppleSystemInfo
    0x7fff43e67000 -     0x7fff43e76ff9  com.apple.AssertionServices (1.0 - 223.100.31) <2DA45CD2-C755-397C-977C-F4C6435A1272> /System/Library/PrivateFrameworks/AssertionServices.framework/Versions/A/AssertionServices
    0x7fff44a09000 -     0x7fff44c49ff0  com.apple.audio.AudioToolboxCore (1.0 - 1104.80) <EE6A2BD9-843C-3CC3-AEFC-6D7855DBB331> /System/Library/PrivateFrameworks/AudioToolboxCore.framework/Versions/A/AudioToolboxCore
    0x7fff44c4d000 -     0x7fff44d69ff3  com.apple.AuthKit (1.0 - 1) <0A3A05D4-0795-35B8-8729-4BF252D52E60> /System/Library/PrivateFrameworks/AuthKit.framework/Versions/A/AuthKit
    0x7fff44f26000 -     0x7fff44f2fff7  com.apple.coreservices.BackgroundTaskManagement (1.0 - 104) <2088BC70-5329-3390-A851-C4ECF654047C> /System/Library/PrivateFrameworks/BackgroundTaskManagement.framework/Versions/A/BackgroundTaskManagement
    0x7fff44f30000 -     0x7fff44fd1ff5  com.apple.backup.framework (1.11.4 - 1298.4.19) <1F848C06-11E3-3D85-8358-7E37AD2BE9D7> /System/Library/PrivateFrameworks/Backup.framework/Versions/A/Backup
    0x7fff44fd2000 -     0x7fff4505eff6  com.apple.BaseBoard (466.3 - 466.3) <1EFE4339-9393-3B15-8DC9-2BE9B03F3062> /System/Library/PrivateFrameworks/BaseBoard.framework/Versions/A/BaseBoard
    0x7fff45160000 -     0x7fff4519cff7  com.apple.bom (14.0 - 219.2) <4B7C18B2-1E51-376E-9D6A-CE3F58D2AE53> /System/Library/PrivateFrameworks/Bom.framework/Versions/A/Bom
    0x7fff46bd8000 -     0x7fff46be8ffb  com.apple.CommonAuth (4.0 - 2.0) <E09BBBBE-ECDD-3442-8D4E-27A12F5E2347> /System/Library/PrivateFrameworks/CommonAuth.framework/Versions/A/CommonAuth
    0x7fff46bfc000 -     0x7fff46c13fff  com.apple.commonutilities (8.0 - 900) <1E6CE910-4B06-3704-A47D-06337A6F0992> /System/Library/PrivateFrameworks/CommonUtilities.framework/Versions/A/CommonUtilities
    0x7fff4773f000 -     0x7fff4775effc  com.apple.analyticsd (1.0 - 1) <F33987F5-A14A-3A55-8D26-FDE9A57B9269> /System/Library/PrivateFrameworks/CoreAnalytics.framework/Versions/A/CoreAnalytics
    0x7fff47ce5000 -     0x7fff47cf5ff3  com.apple.CoreEmoji (1.0 - 107) <AC83B860-61BD-384E-81BF-CA3CBE655968> /System/Library/PrivateFrameworks/CoreEmoji.framework/Versions/A/CoreEmoji
    0x7fff48335000 -     0x7fff4839fff0  com.apple.CoreNLP (1.0 - 213) <687A4C31-A307-3255-83BE-9B123971FF62> /System/Library/PrivateFrameworks/CoreNLP.framework/Versions/A/CoreNLP
    0x7fff487cd000 -     0x7fff487d5ff8  com.apple.CorePhoneNumbers (1.0 - 1) <17E6A3B0-A181-3295-8B19-E139EDF12E4B> /System/Library/PrivateFrameworks/CorePhoneNumbers.framework/Versions/A/CorePhoneNumbers
    0x7fff491c2000 -     0x7fff491e5fff  com.apple.CoreSVG (1.0 - 129) <53213F48-F888-3EBE-AE30-E9303E9B712C> /System/Library/PrivateFrameworks/CoreSVG.framework/Versions/A/CoreSVG
    0x7fff491e6000 -     0x7fff49219fff  com.apple.CoreServicesInternal (446.7 - 446.7) <4C4834E0-EA46-36DE-BA11-16B8826DD754> /System/Library/PrivateFrameworks/CoreServicesInternal.framework/Versions/A/CoreServicesInternal
    0x7fff4921a000 -     0x7fff49248ffd  com.apple.CSStore (1069.22 - 1069.22) <39E431F9-3584-34DF-A64D-C5895AA72068> /System/Library/PrivateFrameworks/CoreServicesStore.framework/Versions/A/CoreServicesStore
    0x7fff4989a000 -     0x7fff499c6ff6  com.apple.coreui (2.1 - 609.4) <55EACF17-86EA-3F6E-A2CF-AF2F08C5F295> /System/Library/PrivateFrameworks/CoreUI.framework/Versions/A/CoreUI
    0x7fff499c7000 -     0x7fff49b7dff5  com.apple.CoreUtils (6.2 - 620.34) <172FC306-619F-3451-9BCA-F0B0D0B58EFD> /System/Library/PrivateFrameworks/CoreUtils.framework/Versions/A/CoreUtils
    0x7fff49cb7000 -     0x7fff49ccaff1  com.apple.CrashReporterSupport (10.13 - 15016) <8AB4A416-A174-386B-8A96-5F16EAA3FCDE> /System/Library/PrivateFrameworks/CrashReporterSupport.framework/Versions/A/CrashReporterSupport
    0x7fff49d83000 -     0x7fff49d95ff8  com.apple.framework.DFRFoundation (1.0 - 252.40.1) <852E7EE8-EC39-3CFF-9605-9F971F7BCED5> /System/Library/PrivateFrameworks/DFRFoundation.framework/Versions/A/DFRFoundation
    0x7fff49d96000 -     0x7fff49d9bfff  com.apple.DSExternalDisplay (3.1 - 380) <61597AB3-7E66-339D-A709-50D4F9B3D8E9> /System/Library/PrivateFrameworks/DSExternalDisplay.framework/Versions/A/DSExternalDisplay
    0x7fff49e25000 -     0x7fff49e9fff0  com.apple.datadetectorscore (8.0 - 659) <F7BA8B28-FD51-34A7-A423-63878638D00E> /System/Library/PrivateFrameworks/DataDetectorsCore.framework/Versions/A/DataDetectorsCore
    0x7fff49f29000 -     0x7fff4a0b0ff2  com.apple.desktopservices (1.14.4 - 1281.4.19) <82777143-A900-33D0-BCFA-2511C89C9EAD> /System/Library/PrivateFrameworks/DesktopServicesPriv.framework/Versions/A/DesktopServicesPriv
    0x7fff4ba49000 -     0x7fff4be64ff1  com.apple.vision.FaceCore (4.3.0 - 4.3.0) <E081D201-B82C-3AE3-8B58-1E909CE053B3> /System/Library/PrivateFrameworks/FaceCore.framework/Versions/A/FaceCore
    0x7fff4c4f5000 -     0x7fff4c62cffc  libFontParser.dylib (277.2.4.2) <B59E080A-9FC3-3511-9024-E6D5461E60D1> /System/Library/PrivateFrameworks/FontServices.framework/libFontParser.dylib
    0x7fff4c6c6000 -     0x7fff4c6d6ff6  libhvf.dylib (1.0 - $[CURRENT_PROJECT_VERSION]) <5A0F87CA-81C0-3444-B958-AAC7BD4319BC> /System/Library/PrivateFrameworks/FontServices.framework/libhvf.dylib
    0x7fff51268000 -     0x7fff5126efff  com.apple.GPUWrangler (5.1.16 - 5.1.16) <F91AD6D6-8242-348C-8296-AF1DD8DBA2EF> /System/Library/PrivateFrameworks/GPUWrangler.framework/Versions/A/GPUWrangler
    0x7fff526e2000 -     0x7fff526f0ffb  com.apple.GraphVisualizer (1.0 - 100.1) <7289AEE6-C577-3D89-A99E-98551218EB7D> /System/Library/PrivateFrameworks/GraphVisualizer.framework/Versions/A/GraphVisualizer
    0x7fff52890000 -     0x7fff5294eff4  com.apple.Heimdal (4.0 - 2.0) <C4838DCE-48FB-3828-9FB2-097BA2848C99> /System/Library/PrivateFrameworks/Heimdal.framework/Versions/A/Heimdal
    0x7fff54acf000 -     0x7fff54ad7ff5  com.apple.IOAccelerator (438.4.5 - 438.4.5) <4B2F1D11-C36B-3C48-9934-8A973348A966> /System/Library/PrivateFrameworks/IOAccelerator.framework/Versions/A/IOAccelerator
    0x7fff54ae4000 -     0x7fff54afafff  com.apple.IOPresentment (1.0 - 37) <2FE66352-4CF9-3F79-944D-053E2AD451D6> /System/Library/PrivateFrameworks/IOPresentment.framework/Versions/A/IOPresentment
    0x7fff54e82000 -     0x7fff54ecdff1  com.apple.IconServices (438.3 - 438.3) <2431AD46-37B8-367F-A1DC-119C781B1453> /System/Library/PrivateFrameworks/IconServices.framework/Versions/A/IconServices
    0x7fff5508b000 -     0x7fff55092ffa  com.apple.InternationalSupport (1.0 - 45.2) <296B6979-342E-35B8-A58B-B0797DFBA789> /System/Library/PrivateFrameworks/InternationalSupport.framework/Versions/A/InternationalSupport
    0x7fff5531f000 -     0x7fff5533effd  com.apple.security.KeychainCircle.KeychainCircle (1.0 - 1) <D0F59C6D-F069-3F0D-81C2-CBFC2E6B7101> /System/Library/PrivateFrameworks/KeychainCircle.framework/Versions/A/KeychainCircle
    0x7fff55473000 -     0x7fff55541ffd  com.apple.LanguageModeling (1.0 - 215.1) <3FAF1700-F7D4-3F92-88AA-A3920702B8BB> /System/Library/PrivateFrameworks/LanguageModeling.framework/Versions/A/LanguageModeling
    0x7fff55542000 -     0x7fff5558afff  com.apple.Lexicon-framework (1.0 - 72) <212D02CE-11BC-3C7F-BDFD-DF1A0C4017EE> /System/Library/PrivateFrameworks/Lexicon.framework/Versions/A/Lexicon
    0x7fff55591000 -     0x7fff55596ff3  com.apple.LinguisticData (1.0 - 353.18) <BA3869B7-9C39-32DA-A4BA-12F1BC4B04CF> /System/Library/PrivateFrameworks/LinguisticData.framework/Versions/A/LinguisticData
    0x7fff568fd000 -     0x7fff56949fff  com.apple.spotlight.metadata.utilities (1.0 - 2076.3) <EF8AC054-B15F-375F-AACB-018DC73CD16E> /System/Library/PrivateFrameworks/MetadataUtilities.framework/Versions/A/MetadataUtilities
    0x7fff5694a000 -     0x7fff56a1bffa  com.apple.gpusw.MetalTools (1.0 - 1) <BA343D96-58EA-374A-818C-E42968101EA8> /System/Library/PrivateFrameworks/MetalTools.framework/Versions/A/MetalTools
    0x7fff56c4e000 -     0x7fff56c6cfff  com.apple.MobileKeyBag (2.0 - 1.0) <0837C5C4-A860-387C-8F31-9A4627A3132F> /System/Library/PrivateFrameworks/MobileKeyBag.framework/Versions/A/MobileKeyBag
    0x7fff56ecf000 -     0x7fff56effff7  com.apple.MultitouchSupport.framework (3440.1 - 3440.1) <0AA68A0D-23F6-3628-A93F-8F8018B84920> /System/Library/PrivateFrameworks/MultitouchSupport.framework/Versions/A/MultitouchSupport
    0x7fff573fe000 -     0x7fff57408fff  com.apple.NetAuth (6.2 - 6.2) <D324C7CC-E614-35F6-8619-DECBE90ECAEB> /System/Library/PrivateFrameworks/NetAuth.framework/Versions/A/NetAuth
    0x7fff57e13000 -     0x7fff57e5effb  com.apple.OTSVG (1.0 - 643.1.4.4) <DCCAD72C-ED3E-3FB9-80C8-4DB36362C28A> /System/Library/PrivateFrameworks/OTSVG.framework/Versions/A/OTSVG
    0x7fff59066000 -     0x7fff59071ff2  com.apple.PerformanceAnalysis (1.243.2 - 243.2) <FFE831BE-C133-38BE-A6B4-BEEB9FD6BF37> /System/Library/PrivateFrameworks/PerformanceAnalysis.framework/Versions/A/PerformanceAnalysis
    0x7fff59072000 -     0x7fff5909affb  com.apple.persistentconnection (1.0 - 1.0) <F16D4768-61F2-3298-8E37-0EAF612A55C0> /System/Library/PrivateFrameworks/PersistentConnection.framework/Versions/A/PersistentConnection
    0x7fff5ba58000 -     0x7fff5ba71ffb  com.apple.ProtocolBuffer (1 - 274.24.9.16.3) <05BE7640-A9FD-3963-8199-E60DE3C37A7E> /System/Library/PrivateFrameworks/ProtocolBuffer.framework/Versions/A/ProtocolBuffer
    0x7fff5bed0000 -     0x7fff5bef9ff1  com.apple.RemoteViewServices (2.0 - 148) <680F9F89-C44B-3AB3-B9EA-155B41B7295A> /System/Library/PrivateFrameworks/RemoteViewServices.framework/Versions/A/RemoteViewServices
    0x7fff5c05e000 -     0x7fff5c099ff0  com.apple.RunningBoardServices (1.0 - 223.100.31) <28C26D68-F1F5-3ADC-832B-AF63336F35FB> /System/Library/PrivateFrameworks/RunningBoardServices.framework/Versions/A/RunningBoardServices
    0x7fff5daec000 -     0x7fff5dc13ff1  com.apple.Sharing (1526.14 - 1526.14) <8D0C1BC4-5133-399B-9EFC-74CAEF4FA389> /System/Library/PrivateFrameworks/Sharing.framework/Versions/A/Sharing
    0x7fff5f026000 -     0x7fff5f31cfff  com.apple.SkyLight (1.600.0 - 450.9) <C6AF6A79-C673-3B9E-95E0-993F43AE7EED> /System/Library/PrivateFrameworks/SkyLight.framework/Versions/A/SkyLight
    0x7fff5fb69000 -     0x7fff5fb77ffb  com.apple.SpeechRecognitionCore (6.0.91 - 6.0.91) <4678A6DB-D56E-393F-90BD-5AF4F3664440> /System/Library/PrivateFrameworks/SpeechRecognitionCore.framework/Versions/A/SpeechRecognitionCore
    0x7fff60668000 -     0x7fff60678ff3  com.apple.TCC (1.0 - 1) <AEE98D6E-03FD-3C80-90AC-5B45B4AE7A2E> /System/Library/PrivateFrameworks/TCC.framework/Versions/A/TCC
    0x7fff60b9b000 -     0x7fff60c61ff0  com.apple.TextureIO (3.10.9 - 3.10.9) <362C5815-6A2B-3CA8-B577-C5D4978EF981> /System/Library/PrivateFrameworks/TextureIO.framework/Versions/A/TextureIO
    0x7fff60e22000 -     0x7fff6107aff0  com.apple.UIFoundation (1.0 - 661.2) <27837A1C-A833-3F99-B8D8-84A583EEA523> /System/Library/PrivateFrameworks/UIFoundation.framework/Versions/A/UIFoundation
    0x7fff62d49000 -     0x7fff62d4afff  com.apple.WatchdogClient.framework (1.0 - 67.101.1) <1D6C2858-0A09-380E-8718-14131D9A0FE1> /System/Library/PrivateFrameworks/WatchdogClient.framework/Versions/A/WatchdogClient
    0x7fff63976000 -     0x7fff63979ffa  com.apple.dt.XCTTargetBootstrap (1.0 - 16091) <B40E4B60-2DB5-30ED-A210-3ED708862162> /System/Library/PrivateFrameworks/XCTTargetBootstrap.framework/Versions/A/XCTTargetBootstrap
    0x7fff639f3000 -     0x7fff63a01ff5  com.apple.audio.caulk (1.0 - 32.3) <DFE1EBB6-9A42-3227-8601-5CFCB1F665CD> /System/Library/PrivateFrameworks/caulk.framework/Versions/A/caulk
    0x7fff63d43000 -     0x7fff63d45ff3  com.apple.loginsupport (1.0 - 1) <B84ABC31-431B-3F99-****-44ED0A7DB3C0> /System/Library/PrivateFrameworks/login.framework/Versions/A/Frameworks/loginsupport.framework/Versions/A/loginsupport
    0x7fff66824000 -     0x7fff66857ffa  libAudioToolboxUtility.dylib (1104.80) <C34C8FCE-54DE-3884-8074-057B06807D22> /usr/lib/libAudioToolboxUtility.dylib
    0x7fff6685e000 -     0x7fff66892fff  libCRFSuite.dylib (48) <E52BECF7-1819-3998-ACC4-8D1A332CE4EB> /usr/lib/libCRFSuite.dylib
    0x7fff66895000 -     0x7fff6689ffff  libChineseTokenizer.dylib (34) <EE842A48-3D30-34B0-B9D2-F045DE582650> /usr/lib/libChineseTokenizer.dylib
    0x7fff6692b000 -     0x7fff6692dff7  libDiagnosticMessagesClient.dylib (112) <BE749883-9400-334A-8FBF-F3321CF205F5> /usr/lib/libDiagnosticMessagesClient.dylib
    0x7fff66973000 -     0x7fff66b2affb  libFosl_dynamic.dylib (100.4) <68038226-8CAA-36B5-B5D6-510F900B318D> /usr/lib/libFosl_dynamic.dylib
    0x7fff66b51000 -     0x7fff66b57ff3  libIOReport.dylib (54) <FA47D01E-E02C-3178-9C10-DF4E7F6351B0> /usr/lib/libIOReport.dylib
    0x7fff66c6f000 -     0x7fff66c8ffff  libMobileGestalt.dylib (826.100.27) <4B771C86-0CB7-3B06-8F41-5A40DDF66D72> /usr/lib/libMobileGestalt.dylib
    0x7fff66e01000 -     0x7fff66e02fff  libSystem.B.dylib (1281.100.1) <DB8310F1-272D-3533-A840-3B390AF55C26> /usr/lib/libSystem.B.dylib
    0x7fff66e8f000 -     0x7fff66e90fff  libThaiTokenizer.dylib (3) <DC582222-7C1F-3C27-8C3A-BAF696A2197D> /usr/lib/libThaiTokenizer.dylib
    0x7fff66ea8000 -     0x7fff66ebefff  libapple_nghttp2.dylib (1.39.2) <268F4E3E-95DC-35FB-82DC-5B0D1855A676> /usr/lib/libapple_nghttp2.dylib
    0x7fff66ef3000 -     0x7fff66f65ff7  libarchive.2.dylib (72.100.1) <65E0870E-02AB-365D-84F9-5800B5BB69FC> /usr/lib/libarchive.2.dylib
    0x7fff66f66000 -     0x7fff66ffffe5  libate.dylib (3.0.1) <4477640F-CC1B-3825-B877-69508F367E3D> /usr/lib/libate.dylib
    0x7fff67003000 -     0x7fff67003ff3  libauto.dylib (187) <FD0E5750-7004-36A7-B9C2-D6B6B4EF559B> /usr/lib/libauto.dylib
    0x7fff670c9000 -     0x7fff670d9ffb  libbsm.0.dylib (60.100.1) <B0373A39-DBC6-3A84-879B-BA46E30D04BF> /usr/lib/libbsm.0.dylib
    0x7fff670da000 -     0x7fff670e6fff  libbz2.1.0.dylib (44) <FFCD4427-AF87-36D2-8097-8870FDC75A1B> /usr/lib/libbz2.1.0.dylib
    0x7fff670e7000 -     0x7fff67139fff  libc++.1.dylib (902.1) <08199809-33CA-321E-9B9D-FD5B2BC64580> /usr/lib/libc++.1.dylib
    0x7fff6713a000 -     0x7fff6714fffb  libc++abi.dylib (902) <1C880020-396D-3F91-BE27-5A09A9239F68> /usr/lib/libc++abi.dylib
    0x7fff67150000 -     0x7fff67150fff  libcharset.1.dylib (59) <4E63BA25-04A3-329A-923D-251155C03F30> /usr/lib/libcharset.1.dylib
    0x7fff67151000 -     0x7fff67162fff  libcmph.dylib (8) <D4C5E0A8-92D9-33D5-9F83-6F4742FFBE29> /usr/lib/libcmph.dylib
    0x7fff67163000 -     0x7fff6717afd7  libcompression.dylib (87) <7F258A06-E01D-32D2-9CD2-6B2931DA5DA7> /usr/lib/libcompression.dylib
    0x7fff67454000 -     0x7fff6746aff7  libcoretls.dylib (167) <EFC237BB-78F7-33C6-BFF9-53860062DD99> /usr/lib/libcoretls.dylib
    0x7fff6746b000 -     0x7fff6746cfff  libcoretls_cfhelpers.dylib (167) <2E542A2B-7730-33EE-9B3B-154B08608AA6> /usr/lib/libcoretls_cfhelpers.dylib
    0x7fff67a29000 -     0x7fff67a88ff7  libcups.2.dylib (483.6) <F446DEF0-66C0-31AD-88E1-919B05F06C90> /usr/lib/libcups.2.dylib
    0x7fff67b94000 -     0x7fff67b94fff  libenergytrace.dylib (21) <FFB9FB70-8DBD-3025-BC92-51F02481A489> /usr/lib/libenergytrace.dylib
    0x7fff67b95000 -     0x7fff67badfff  libexpat.1.dylib (19.60.2) <1ED53818-578C-3D17-8761-68792CCAD685> /usr/lib/libexpat.1.dylib
    0x7fff67bbb000 -     0x7fff67bbdfff  libfakelink.dylib (149.1) <B04F9A05-7E52-3382-9186-F603BE4BFBB2> /usr/lib/libfakelink.dylib
    0x7fff67bcc000 -     0x7fff67bd1fff  libgermantok.dylib (24) <8091F952-B592-38E3-982B-7DEA0A44E211> /usr/lib/libgermantok.dylib
    0x7fff67bd2000 -     0x7fff67bdbff7  libheimdal-asn1.dylib (564.100.1) <2D639331-43CF-331F-98F4-CDF41990A468> /usr/lib/libheimdal-asn1.dylib
    0x7fff67bdc000 -     0x7fff67cccfff  libiconv.2.dylib (59) <9458704B-A702-37CB-9707-66ABBB5DB71E> /usr/lib/libiconv.2.dylib
    0x7fff67ccd000 -     0x7fff67f24fff  libicucore.A.dylib (64260.0.1) <DCC4A4EE-32FD-350F-84D8-E857F2F29855> /usr/lib/libicucore.A.dylib
    0x7fff67f3e000 -     0x7fff67f3ffff  liblangid.dylib (133) <E9595222-602B-38F0-8572-0F1872A00527> /usr/lib/liblangid.dylib
    0x7fff67f40000 -     0x7fff67f58ff3  liblzma.5.dylib (16) <0AA1EB11-A433-327E-B8DB-7395CFF06554> /usr/lib/liblzma.5.dylib
    0x7fff67f70000 -     0x7fff68017ff7  libmecab.dylib (883.10) <13136C11-8763-37BA-AEB2-676092798DAA> /usr/lib/libmecab.dylib
    0x7fff68018000 -     0x7fff6827afe1  libmecabra.dylib (883.10) <6AC22857-F528-35CE-94A9-D70F6F766C15> /usr/lib/libmecabra.dylib
    0x7fff685e7000 -     0x7fff68616fff  libncurses.5.4.dylib (57) <6BD6F430-C8B3-39D8-87B5-2C16E6578FD5> /usr/lib/libncurses.5.4.dylib
    0x7fff68746000 -     0x7fff68bc1ff5  libnetwork.dylib (1880.100.30) <9519B6F8-44E2-3F53-B995-1527C5333240> /usr/lib/libnetwork.dylib
    0x7fff68c61000 -     0x7fff68c94fde  libobjc.A.dylib (787.1) <20AC082F-2DB7-3974-A2D4-8C5E01787584> /usr/lib/libobjc.A.dylib
    0x7fff68ca7000 -     0x7fff68cabfff  libpam.2.dylib (25.100.1) <D5CEC1AD-A2EC-362C-B71A-22FD521917F1> /usr/lib/libpam.2.dylib
    0x7fff68cae000 -     0x7fff68ce4ff7  libpcap.A.dylib (89.100.1) <171BAAB0-A5C8-32C5-878E-83D46073BF8C> /usr/lib/libpcap.A.dylib
    0x7fff68d68000 -     0x7fff68d80fff  libresolv.9.dylib (67.40.1) <92A522F9-95E2-35EE-A8AD-FC8DEE6B2C1F> /usr/lib/libresolv.9.dylib
    0x7fff68ddc000 -     0x7fff68fc6ff7  libsqlite3.dylib (308.4) <BBC375B7-AF20-3D2C-8826-78D3BDC8A004> /usr/lib/libsqlite3.dylib
    0x7fff69217000 -     0x7fff6921affb  libutil.dylib (57) <07ED7CF0-1744-3386-B8B2-0DDBD446999E> /usr/lib/libutil.dylib
    0x7fff6921b000 -     0x7fff69228ff7  libxar.1.dylib (425.2) <625F24E1-1A0F-3301-9F99-F0F3DADE0287> /usr/lib/libxar.1.dylib
    0x7fff6922e000 -     0x7fff69310ff7  libxml2.2.dylib (33.3) <24147A90-E3EB-3926-BFB0-5F0FC9F706E2> /usr/lib/libxml2.2.dylib
    0x7fff69314000 -     0x7fff6933cfff  libxslt.1.dylib (16.9) <8C8648B1-F2CA-38EA-A409-D6F19715C6E6> /usr/lib/libxslt.1.dylib
    0x7fff6933d000 -     0x7fff6934fff3  libz.1.dylib (76) <6A449C6A-DF88-36C1-8F2D-DB9A808263B5> /usr/lib/libz.1.dylib
    0x7fff69bfd000 -     0x7fff69c02ff3  libcache.dylib (83) <5F90FFCE-403B-3724-991D-BA32401D99C5> /usr/lib/system/libcache.dylib
    0x7fff69c03000 -     0x7fff69c0efff  libcommonCrypto.dylib (60165) <C7A5E3F7-1E5A-3785-875A-B6647082B614> /usr/lib/system/libcommonCrypto.dylib
    0x7fff69c0f000 -     0x7fff69c16fff  libcompiler_rt.dylib (101.2) <A517E149-2D25-3C04-BCEF-F69149C85B18> /usr/lib/system/libcompiler_rt.dylib
    0x7fff69c17000 -     0x7fff69c20ff7  libcopyfile.dylib (166.40.1) <1A5270B5-0D97-35DA-9296-4F4A428BC6A2> /usr/lib/system/libcopyfile.dylib
    0x7fff69c21000 -     0x7fff69cb3fe3  libcorecrypto.dylib (866.100.30) <FCDEC0D1-8C30-3989-BDD1-996BBC715C29> /usr/lib/system/libcorecrypto.dylib
    0x7fff69dc0000 -     0x7fff69e00ff0  libdispatch.dylib (1173.100.2) <EB592997-B11C-3AB3-85B1-F725F3D0B412> /usr/lib/system/libdispatch.dylib
    0x7fff69e01000 -     0x7fff69e37fff  libdyld.dylib (750.5) <D2A07EF5-A64B-3692-BE13-89DAA2EC5E80> /usr/lib/system/libdyld.dylib
    0x7fff69e38000 -     0x7fff69e38ffb  libkeymgr.dylib (30) <CC5A2B43-770B-3C6C-BA10-AA3A6B4A142D> /usr/lib/system/libkeymgr.dylib
    0x7fff69e39000 -     0x7fff69e45ff3  libkxld.dylib (6153.101.6) <77282DCB-83D6-3199-874E-9A4A0FD7D4F3> /usr/lib/system/libkxld.dylib
    0x7fff69e46000 -     0x7fff69e46ff7  liblaunch.dylib (1738.100.39) <A7FF7357-600F-3014-8C28-A4F367717E8D> /usr/lib/system/liblaunch.dylib
    0x7fff69e47000 -     0x7fff69e4cff7  libmacho.dylib (959.0.1) <D8FED478-25A2-3844-AE4B-A5C9F9827615> /usr/lib/system/libmacho.dylib
    0x7fff69e4d000 -     0x7fff69e4fff3  libquarantine.dylib (110.40.3) <51E0304F-AB11-3BF7-99DC-BB916CC9088B> /usr/lib/system/libquarantine.dylib
    0x7fff69e50000 -     0x7fff69e51ff7  libremovefile.dylib (48) <078F29AB-26BA-3493-BCAA-E1E75A187521> /usr/lib/system/libremovefile.dylib
    0x7fff69e52000 -     0x7fff69e69ff3  libsystem_asl.dylib (377.60.2) <0F1BAC19-2AE0-3F8E-9B90-AACF819B2BF7> /usr/lib/system/libsystem_asl.dylib
    0x7fff69e6a000 -     0x7fff69e6aff7  libsystem_blocks.dylib (74) <32224AFF-C06F-3279-B753-097194EDEF49> /usr/lib/system/libsystem_blocks.dylib
    0x7fff69e6b000 -     0x7fff69ef2fff  libsystem_c.dylib (1353.100.2) <4F5EED22-4D46-3F04-8C64-C492CDAD70EB> /usr/lib/system/libsystem_c.dylib
    0x7fff69ef3000 -     0x7fff69ef6ffb  libsystem_configuration.dylib (1061.101.1) <2A2C778D-07EB-35C7-A954-8BF8FD74BD75> /usr/lib/system/libsystem_configuration.dylib
    0x7fff69ef7000 -     0x7fff69efafff  libsystem_coreservices.dylib (114) <FDA41CC4-170A-3D93-85BD-838A563B03C4> /usr/lib/system/libsystem_coreservices.dylib
    0x7fff69efb000 -     0x7fff69f03fff  libsystem_darwin.dylib (1353.100.2) <B567B86D-8818-38A4-A861-03EB83B55867> /usr/lib/system/libsystem_darwin.dylib
    0x7fff69f04000 -     0x7fff69f0bfff  libsystem_dnssd.dylib (1096.100.3) <7C690DF5-E119-33FB-85CD-9EFC67A36E40> /usr/lib/system/libsystem_dnssd.dylib
    0x7fff69f0c000 -     0x7fff69f0dffb  libsystem_featureflags.dylib (17) <415D83EF-084C-3485-B757-53001870EA94> /usr/lib/system/libsystem_featureflags.dylib
    0x7fff69f0e000 -     0x7fff69f5bff7  libsystem_info.dylib (538) <17049D3F-C798-3651-B391-1551FC699D3E> /usr/lib/system/libsystem_info.dylib
    0x7fff69f5c000 -     0x7fff69f88ff7  libsystem_kernel.dylib (6153.101.6) <E76440E1-D1E8-3D9A-8B47-D01F554FF1C4> /usr/lib/system/libsystem_kernel.dylib
    0x7fff69f89000 -     0x7fff69fd0fff  libsystem_m.dylib (3178) <74741FA8-5C29-3241-9046-4FC91C6A6D4A> /usr/lib/system/libsystem_m.dylib
    0x7fff69fd1000 -     0x7fff69ff8fff  libsystem_malloc.dylib (283.100.5) <97833239-2F83-3AEB-A426-0593997C8A54> /usr/lib/system/libsystem_malloc.dylib
    0x7fff69ff9000 -     0x7fff6a006ffb  libsystem_networkextension.dylib (1095.100.29) <C9E988B2-6A18-35C0-9577-63201E9D6018> /usr/lib/system/libsystem_networkextension.dylib
    0x7fff6a007000 -     0x7fff6a010ff7  libsystem_notify.dylib (241.100.2) <E405F84B-BD4F-3874-9755-CB3EC86E18D5> /usr/lib/system/libsystem_notify.dylib
    0x7fff6a011000 -     0x7fff6a019fef  libsystem_platform.dylib (220.100.1) <6EF12F34-C33F-36BF-9A9A-2A35EA19EFE0> /usr/lib/system/libsystem_platform.dylib
    0x7fff6a01a000 -     0x7fff6a024fff  libsystem_pthread.dylib (416.100.3) <A8514582-E000-3854-911A-0A73D2C79600> /usr/lib/system/libsystem_pthread.dylib
    0x7fff6a025000 -     0x7fff6a029ff3  libsystem_sandbox.dylib (1217.101.2) <E9D78CDE-FB67-32E7-BABC-9EFC23AA0DC6> /usr/lib/system/libsystem_sandbox.dylib
    0x7fff6a02a000 -     0x7fff6a02cfff  libsystem_secinit.dylib (62.100.2) <AAC639E5-7103-3366-A602-8FC6944E2C13> /usr/lib/system/libsystem_secinit.dylib
    0x7fff6a02d000 -     0x7fff6a034ffb  libsystem_symptoms.dylib (1238.100.26) <487B92DE-45F9-39F9-A478-89BBD478157D> /usr/lib/system/libsystem_symptoms.dylib
    0x7fff6a035000 -     0x7fff6a04bff2  libsystem_trace.dylib (1147.100.8) <BB90B1FD-8C09-3DF4-BD8B-9E4AEADFEA2B> /usr/lib/system/libsystem_trace.dylib
    0x7fff6a04d000 -     0x7fff6a052ff7  libunwind.dylib (35.4) <CC87C836-BE9D-334E-A0E6-0297D52E9D73> /usr/lib/system/libunwind.dylib
    0x7fff6a053000 -     0x7fff6a088ffe  libxpc.dylib (1738.100.39) <32B0E31E-9DA3-328B-A962-BC9591B93537> /usr/lib/system/libxpc.dylib
~~~

External Modification Summary:
  Calls made by other processes targeting this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by this process:
    task_for_pid: 0
    thread_create: 0
    thread_set_state: 0
  Calls made by all processes on this machine:
    task_for_pid: 5398055
    thread_create: 0
    thread_set_state: 0

VM Region Summary:
ReadOnly portion of Libraries: Total=550.8M resident=0K(0%) swapped_out_or_unallocated=550.8M(100%)
Writable regions: Total=571.0M written=0K(0%) resident=0K(0%) swapped_out=0K(0%) unallocated=571.0M(100%)
 
~~~
                                VIRTUAL   REGION 
REGION TYPE                        SIZE    COUNT (non-coalesced) 
===========                     =======  ======= 
Activity Tracing                   256K        1 
Kernel Alloc Once                    8K        1 
MALLOC                           175.5M       28 
MALLOC guard page                   16K        4 
MALLOC_NANO (reserved)           384.0M        1         reserved VM address space (unallocated)
Memory Tag 242                      12K        1 
STACK GUARD                       56.0M        7 
Stack                             11.0M        7 
VM_ALLOCATE                         44K        2 
__DATA                            21.7M      249 
__DATA_CONST                        20K        1 
__FONT_DATA                          4K        1 
__LINKEDIT                       396.1M        3 
__OBJC_RO                         32.2M        1 
__OBJC_RW                         1892K        2 
__TEXT                           154.7M      247 
__UNICODE                          564K        1 
mapped file                       33.3M        6 
shared memory                      632K       14 
===========                     =======  ======= 
TOTAL                              1.2G      577 
TOTAL, minus reserved VM space   883.9M      577 
~~~


2-Does the problem reproduce if you skip steps 7 through 9?
Etresoft has also mentioned it. I guess it was a mistake to add these steps to my question :) you can just ignore step9.
When I was debugging my sandboxing problem, I saw that “spctl -a -t exec -vv test.app” command was throwing “rejected” error, therefore I thought that my problem could be relevant with this rejection. But apparently it is not.

Thanks a lot in advance.


Posted 2 years ago by  EmreGun
Copy EmreGun answer
Up vote reply of EmreGun
Down vote reply of EmreGun
Add a Comment
you can see my crash reports below:

That’s what I was looking for.

Etresoft has pointed you in the right direction here but I want to explain some of the backstory. All of the following is relative to Crash Report1.crash.

To start, consider:

Code Block  
Exception Type:        EXC_BAD_INSTRUCTION (SIGILL)


This indicates that your process died because it tried to execute an illegal instruction. In some cases this is caused by your code running off into the weeds but in this case it’s clear that this is a deliberate crash. Code running within your process has detected a problem and run an illegal instruction to cause the process to terminate.

You can see who did this by looking at the crashing thread backtrace:

Code Block  
~~~
Thread 0 Crashed:: Dispatch queue: com.apple.main-thread
0   libsystem_secinit.dylib … _libsecinit_appsandbox.cold.2 + 95
1   libsystem_secinit.dylib … _libsecinit_appsandbox + 1511
2   libsystem_secinit.dylib … _libsecinit_initializer + 35
3   libSystem.B.dylib       … libSystem_initializer + 268
4   dyld                    … ImageLoaderMachO::doModInitFunctions(ImageLoader::LinkContext const&) + 535
5   dyld                    … ImageLoaderMachO::doInitialization(ImageLoader::LinkContext const&) + 40
6   dyld                    … ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned in…
…
16  dyld                    … ImageLoader::recursiveInitialization(ImageLoader::LinkContext const&, unsigned in…
17  dyld                    … ImageLoader::processInitializers(ImageLoader::LinkContext const&, unsigned int, I…
18  dyld                    … ImageLoader::runInitializers(ImageLoader::LinkContext const&, ImageLoader::Initia…
19  dyld                    … dyld::initializeMainExecutable() + 199
20  dyld                    … dyld::_main(macho_header const*, unsigned long, int, char const, char const, …
21  dyld                    … dyldbootstrap::start(dyld3::MachOLoaded const*, int, char const**, dyld3::MachOLo…
22  dyld                    … _dyld_start + 37

~~~

This is very early in the startup sequence for your process. The dynamic linker (frames 22 through 4) is in the middle of linking together your code. As part of this it runs the module initialiser for the System framework (frame 3). That calls the initialiser for the security subsystem (frame 2). That detects that your process is sandbox and attempts to set up the App Sandbox (frames 1 through 0). That setup operation has failed, at which point the code has no choice but to crash your process (it can’t let it continue without a sandbox).

Both dyld and the security subsystem have left breadcrumbs in the Application Specific Information section to explain the problem: 

~~~
Code Block  
dyld: launch, running initializers
/usr/lib/libSystem.B.dylib
Could not set sandbox profile data: Operation not permitted (1)
Application Specific Signatures:
SYSCALL_SET_PROFILE
~~~


You can see that the system call to set up the sandbox has failed with EPERM. As to why that’s happened, this is again something that Etresoft pointed to. Consider this:

Code Block  
Process:               test [8519]


and this:

Code Block  
Parent Process:        ??? [8518]


Normally an app’s parent process is launchd. In this case the process IDs are sequential, suggesting that the app has relaunched itself. Doing this is incompatible with the App Sandbox.

A process can set up the App Sandbox in one of two ways:
From scratch, by setting the com.apple.security.app-sandbox — In this case the process is expected to be launched by a non-sandboxed process, like launchd. This is how apps and app extensions work.

By inheriting it from its parent, by setting com.apple.security.app-sandbox and com.apple.security.inherit — If you embed a helper tool within your app, this is how you set it up to inherit the app’s sandbox [1].

Your process seems to be trying to combine these, that is, it’s signed like a normal app but it’s trying to inherit it sandbox from its parent. This won’t work because the system tries to re-apply the sandbox and that is not allowed (hence the EPERM).

It seems that you’ve inherited (so to speak) this approach from PyInstaller. If so, you’ll need to discuss this issue with them. The current approach they’re using is fundamentally incompatible with the App Sandbox.

Share and Enjoy
—
Quinn “The Eskimo!” @ Developer Technical Support @ Apple
let myEmail = "eskimo" + "1" + "@apple.com"

[1] A child process will inherit its parent sandbox even if it has no entitlements. These entitlements make that inheritance explicit, primarily for the benefit of the App Store ingestion process.
Posted 2 years ago by  eskimo
Copy eskimo answer
Up vote reply of eskimo
Down vote reply of eskimo
Add a Comment
@eskimo 

Thank you very much for your fast and detailed response. Now it is clear to me, what is happening. 
I will try a couple of hours to find a way to prevent that pyinstaller app calls (inherits) itself, 
If I cannot find a solution then I will try further with py2app tool instead of pyinstaller to bundle my application.

Thank you very much for your help.
Posted 2 years ago by  EmreGun