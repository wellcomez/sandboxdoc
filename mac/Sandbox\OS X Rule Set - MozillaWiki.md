---
title: "SandboxOS X Rule Set - MozillaWiki"
layout: post
---

# Sandbox\OS X Rule Set - MozillaWiki
<a style="text-decoration:underline" href="https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set">Sandbox\OS X Rule Set - MozillaWiki</a><br>

References ~

Refers to the full path to the home directory of the user. On OS X this is /Users/<username>.

[^/]+

This is used in some of the regular expressions. [^/] matches a single character that is not a '/'. [^/]+ Matches 1 or more non-slash characters.

Rule Description

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_def)

```
static const char contentSandboxRules[] =
```[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_version)```
(version 1)
```References[1] states only version 1 is supported. I tried with version=2 and sandbox_init failed due to "unsupported version".

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_macros)

```
(define sandbox-level %d)
(define macosMinorVersion %d)
(define appPath \\"%s\\")
(define appBinaryPath \\"%s\\")
(define appDir \\"%s\\")
(define appTempDir \\"%s\\")
(define home-path \\"%s\\")
```

-   sandbox-level: this is the value of security.sandbox.content.level when it is >=1. If the value is zero, the sandbox policy isn't used and content processes aren't sandboxed.
-   macosMinorVersion: this is used to detect OS X 10.9 which rejects the "lsopen" rule.
-   appTempDir: this is the temp directory for content processes that we create and cleanup in the parent, see bug 1252207 and bug 1237847.
-   home-path: this is used so the ruleset can generate home-relative rules for ~/Library, ~/.cups (printing) and ~/.CFUserTextEncoding (stores the default text encoding and preferred language for the user.)

These setup some macros to be used later in the policy. See the next row for examples of what they evaulate to on a Nightly build.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_example)

Example output of the above macros after running a Nightly build. Paths abbreviated with "...".

```
(define sandbox-level 1)
(define macosMinorVersion 11)
(define appPath "/.../NightlyDebug.app/Contents/MacOS/plugin-container.app")
(define appBinaryPath "/.../NightlyDebug.app/Contents/MacOS/plugin-container.app/Contents/MacOS/plugin-container")
(define appDir "/.../NightlyDebug.app/Contents/Resources/browser")
(define appTempDir "/Users/<USERNAME>/Library/Caches/TemporaryItems/Temp-{62ac76fa-73fd-8f46-bd2b-12c4d53aa1cc}")
(define home-path "/Users/<USERNAME>")

```

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_syspaths1)  

```
; Allow read access to standard system paths.
(allow file-read*
       (require-all (file-mode #o0004)
                    (require-any (subpath "/Library/Filesystems/NetFSPlugins")
                                 (subpath "/System")
                                 (subpath "/private/var/db/dyld")
                                 (subpath "/usr/lib")
                                 (subpath "/usr/share"))))

```

Allow these directories and any contained directories and files to be read if the file's permission permits any user to read them.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_syspaths2)

```
(allow file-read-metadata
       (literal "/etc")
       (literal "/tmp")
       (literal "/var")
       (literal "/private/etc/localtime"))

```

Allow reading of metadata of these directories.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_syspaths3)

```
; Allow access to standard special files.
(allow file-read*
       (literal "/dev/autofs_nowait")
       (literal "/dev/random")
       (literal "/dev/urandom")

```

/dev/random, /dev/urandom Used for randomization code. autofs_nowait TBD, probably allows non-blocking I/O to autofs paths (used for network mounts and other pseudo mount points.)

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_syspaths4)

```
(allow file-read*
       file-write-data
       (literal "/dev/null")
       (literal "/dev/zero"))

```

Wondering if we need write access to these.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_dtrace)

```
(allow file-read*
       file-write-data
       file-ioctl
       (literal "/dev/dtracehelper"))

```

Aids debugging the plugin-container using dtrace. Could be removed, but since root privileges are required to read /dev/dtracehelper this wouldn't be exploitable unless Firefox was run as root or with sudo.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_apple1)

```
(allow mach-lookup
       (global-name "com.apple.appsleep")
       (global-name "com.apple.bsd.dirhelper")
       (global-name "com.apple.cfprefsd.agent")
       (global-name "com.apple.cfprefsd.daemon")
       (global-name "com.apple.diagnosticd")
       (global-name "com.apple.espd")
       (global-name "com.apple.secinitd")
       (global-name "com.apple.system.DirectoryService.libinfo_v1")
       (global-name "com.apple.system.logger")
       (global-name "com.apple.system.notification_center")
       (global-name "com.apple.system.opendirectoryd.libinfo")
       (global-name "com.apple.system.opendirectoryd.membership")
       (global-name "com.apple.trustd")
       (global-name "com.apple.trustd.agent")
       (global-name "com.apple.xpc.activity.unmanaged")
       (global-name "com.apple.xpcd")
       (local-name "com.apple.cfprefsd.agent"))

```

Miscellaneous undocumented services.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_sysctl1)

```
; Used to read hw.ncpu, hw.physicalcpu_max, kern.ostype, and others
(allow sysctl-read)

```

A subset of the rules originally from /System/Library/Sandbox/Profiles/system.sb which ships with OS X.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_defaultdeny)

```
  "  (begin\\n"
  "    (deny default)\\n"

```

By default, we deny. i.e., for any capability not explicitly allowed here, do not allow it to be used.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_debugdeny)

```
  "    (debug deny)\\n"
  "\\n"

```

For any rule that causes an action to be denied, log something in system.log. These log entries are easily viewed using the OS X "Console" application and filtering on "sandbox".

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_literal)

```
  "    (define resolving-literal literal)\\n"
  "    (define resolving-subpath subpath)\\n"
  "    (define resolving-regex regex)\\n"

```

Shortcut macros.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_textmacros)

```
  "    (define container-path appPath)\\n"
  "    (define appdir-path appDir)\\n"
  "    (define var-folders-re \\"^/private/var/folders/[^/][^/]\\")\\n"
  "    (define var-folders2-re (string-append var-folders-re \\"/[^/]+/[^/]\\"))\\n"
  "\\n"
  "    (define (home-regex home-relative-regex)\\n"
  "      (resolving-regex (string-append \\"^\\" (regex-quote home-path) home-relative-regex)))\\n"
  "    (define (home-subpath home-relative-subpath)\\n"
  "      (resolving-subpath (string-append home-path home-relative-subpath)))\\n"
  "    (define (home-literal home-relative-literal)\\n"
  "      (resolving-literal (string-append home-path home-relative-literal)))\\n"
  "\\n"
  "    (define (var-folders-regex var-folders-relative-regex)\\n"
  "      (resolving-regex (string-append var-folders-re var-folders-relative-regex)))\\n"
  "    (define (var-folders2-regex var-folders2-relative-regex)\\n"
  "      (resolving-regex (string-append var-folders2-re var-folders2-relative-regex)))\\n"
  "\\n"

```Text substitution macros for dealing with paths.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_sharedprefs)

```
  "    (define (allow-shared-preferences-read domain)\\n"
  "          (begin\\n"
  "            (if (defined? \`user-preference-read)\\n"
  "              (allow user-preference-read (preference-domain domain)))\\n"
  "            (allow file-read*\\n"
  "                   (home-literal (string-append \\"/Library/Preferences/\\" domain \\".plist\\"))\\n"
  "                   (home-regex (string-append \\"/Library/Preferences/ByHost/\\"
                              (regex-quote domain) \\"\\\\..*\\\\.plist$\\")))\\n"
  "            ))\\n"
  "\\n"
  "    (define (allow-shared-list domain)\\n"
  "      (allow file-read*\\n"
  "             (home-regex (string-append \\"/Library/Preferences/\\" (regex-quote domain)))))\\n"
  "\\n"

```Macros for dealing with some form of OS X preferences. TBD.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_posixshm)

```
  "\\n"
  "    (allow ipc-posix-shm\\n"
  "        (ipc-posix-name-regex \\"^/tmp/com.apple.csseed:\\")\\n"
  "        (ipc-posix-name-regex \\"^CFPBS:\\")\\n"
  "        (ipc-posix-name-regex \\"^AudioIO\\"))\\n"

```Access via IPC shared memory to services with names matching these regexes? TBD

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_paths4)

```
  "\\n"
  "    (allow file-read-metadata\\n"
  "        (literal \\"/home\\")\\n"
  "        (literal \\"/net\\")\\n"
  "        (regex \\"^/private/tmp/KSInstallAction\\\\.\\")\\n"
  "        (var-folders-regex \\"/\\")\\n"
  "        (home-subpath \\"/Library\\"))\\n"

```Allow reading of file metadata for these directories and files. Appears to be redundant given the above "(allow file-read-metadata)" rule?

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_signalself)

```
  "    (allow signal (target self))\\n"

```

Allow the content process to send a signal to itself. Searched for callers of kill(2) in mozilla-central and didn't find any.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_jobcreationdal)

```
  "    (allow job-creation (literal \\"/Library/CoreMediaIO/Plug-Ins/DAL\\"))\\n"

```

This might be related to using the camera.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_audioctrl)

```
  "    (allow iokit-set-properties (iokit-property \\"IOAudioControlValue\\"))\\n"

```

Setting sound volume?

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_appleglobalcom2)

```
  "    (allow mach-lookup\\n"
  "        (global-name \\"com.apple.coreservices.launchservicesd\\")\\n"
  "        (global-name \\"com.apple.coreservices.appleevents\\")\\n"
  "        (global-name \\"com.apple.pasteboard.1\\")\\n"
  "        (global-name \\"com.apple.window_proxies\\")\\n"
  "        (global-name \\"com.apple.windowserver.active\\")\\n"
  "        (global-name \\"com.apple.audio.coreaudiod\\")\\n"
  "        (global-name \\"com.apple.audio.audiohald\\")\\n"
  "        (global-name \\"com.apple.PowerManagement.control\\")\\n"
  "        (global-name \\"com.apple.cmio.VDCAssistant\\")\\n"
  "        (global-name \\"com.apple.SystemConfiguration.configd\\")\\n"
  "        (global-name \\"com.apple.iconservices\\")\\n"
  "        (global-name \\"com.apple.cookied\\")\\n"
  "        (global-name \\"com.apple.printuitool.agent\\")\\n"
  "        (global-name \\"com.apple.printtool.agent\\")\\n"
  "        (global-name \\"com.apple.cache_delete\\")\\n"
  "        (global-name \\"com.apple.pluginkit.pkd\\")\\n"
  "        (global-name \\"com.apple.bird\\")\\n"
  "        (global-name \\"com.apple.ocspd\\")\\n"
  "        (global-name \\"com.apple.cmio.AppleCameraAssistant\\")\\n"
  "        (global-name \\"com.apple.DesktopServicesHelper\\"))\\n"

```

Access to more undocumented OS X facilities.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_iokituserclient)

```
  "    (allow iokit-open\\n"
  "        (iokit-user-client-class \\"IOHIDParamUserClient\\")\\n"
  "        (iokit-user-client-class \\"IOAudioControlUserClient\\")\\n"
  "        (iokit-user-client-class \\"IOAudioEngineUserClient\\")\\n"
  "        (iokit-user-client-class \\"IGAccelDevice\\")\\n"
  "        (iokit-user-client-class \\"nvDevice\\")\\n"
  "        (iokit-user-client-class \\"nvSharedUserClient\\")\\n"
  "        (iokit-user-client-class \\"nvFermiGLContext\\")\\n"
  "        (iokit-user-client-class \\"IGAccelGLContext\\")\\n"
  "        (iokit-user-client-class \\"IGAccelSharedUserClient\\")\\n"
  "        (iokit-user-client-class \\"IGAccelVideoContextMain\\")\\n"
  "        (iokit-user-client-class \\"IGAccelVideoContextMedia\\")\\n"
  "        (iokit-user-client-class \\"IGAccelVideoContextVEBox\\")\\n"
  "        (iokit-user-client-class \\"RootDomainUserClient\\")\\n"
  "        (iokit-user-client-class \\"IOUSBDeviceUserClientV2\\")\\n"
  "        (iokit-user-client-class \\"IOUSBInterfaceUserClientV2\\"))\\n"

```

Sound like this is for accessing various kernel driver provided functionality.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_hitoolbox)

```
  "; depending on systems, the 1st, 2nd or both rules are necessary\\n"
  "    (allow-shared-preferences-read \\"com.apple.HIToolbox\\")\\n"
  "    (allow file-read-data (literal \\"/Library/Preferences/com.apple.HIToolbox.plist\\"))\\n"

```

HIToolbox is Human Interface Toolbox. Sounds related to OS X UI controls.  
Allows reading from /Library/Preferences/com.apple.HIToolbox.plist which contains information about the input device such as keyboard layout.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_comappleats)

```
  "    (allow-shared-preferences-read \\"com.apple.ATS\\")\\n"

```

Possibly font-related.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_globalprefs)

```
  "    (allow file-read-data (literal \\"/Library/Preferences/.GlobalPreferences.plist\\"))\\n"

```

Contains some details about time zone, city, language, display devices.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_syspaths5)

```
  "    (allow file-read*\\n"
  "        (subpath \\"/Library/Fonts\\")\\n"
  "        (subpath \\"/Library/Audio/Plug-Ins\\")\\n"
  "        (subpath \\"/Library/CoreMediaIO/Plug-Ins/DAL\\")\\n"
  "        (subpath \\"/Library/Spelling\\")\\n"
  "        (literal \\"/\\")\\n"
  "        (literal \\"/private/tmp\\")\\n"
  "        (literal \\"/private/var/tmp\\")\\n"

```

Filesystem read access to some system directories.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_cfuser)

```
  "        (home-literal \\"/.CFUserTextEncoding\\")\\n"

```

Filesystem read access to file ~/.CFUserTextEncoding (stores the user's default text encoding.)

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_homepaths1)

```
  "        (home-literal \\"/Library/Preferences/com.apple.DownloadAssessment.plist\\")\\n"
  "        (home-subpath \\"/Library/Colors\\")\\n"
  "        (home-subpath \\"/Library/Fonts\\")\\n"
  "        (home-subpath \\"/Library/FontCollections\\")\\n"
  "        (home-subpath \\"/Library/Keyboard Layouts\\")\\n"
  "        (home-subpath \\"/Library/Input Methods\\")\\n"
  "        (home-subpath \\"/Library/Spelling\\")\\n"

```Filesystem read access to these ~/Library subdirectories.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_appdir)

```
  "        (subpath appdir-path)\\n"

```

Read access to part of the application bundle:  
/.../NightlyDebug.app/Contents/Resources/browser

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_apppath)

```
  "        (literal appPath)\\n"
  "        (literal appBinaryPath))\\n"

```

Read access to plugin-container .app:  
/.../NightlyDebug.app/Contents/MacOS/plugin-container.app  
and the executable contained in it  
/.../NightlyDebug.app/Contents/MacOS/plugin-container.app/Contents/MacOS/plugin-container

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_plugincontainer_pref)

```
  "    (allow-shared-list \\"org.mozilla.plugincontainer\\")\\n"

```Might not be needed. On OS X 10.11, no matches for files with this name found under ~/Library/Preferences/. Matches found in ~/Library/Caches though.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_miccam)

```
  "; the following 2 rules should be removed when microphone and camera access\\n"
  "; are brokered through the content process\\n"
  "    (allow device-microphone)\\n"
  "    (allow device-camera)\\n"

```Camera and mic access.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_intldata)

```
  "\\n"
  "    (allow file* (var-folders2-regex \\"/com\\\\.apple\\\\.IntlDataCache\\\\.le$\\"))\\n"

```Read and write access to  
/private/var/folders/[^/][^/][^/]+/[^/]com.apple.IntlDataCache.le  
This file not prevent on my 10.11 system.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_icon)

```
  "    (allow file-read*\\n"
  "        (var-folders2-regex \\"/com\\\\.apple\\\\.IconServices/\\")\\n"
  "        (var-folders2-regex \\"/[^/]+\\\\.mozrunner/extensions/[^/]+/chrome/[^/]+/content/[^/]+\\\\.j(s|ar)$\\"))\\n"


```

Read access to  
/private/var/folders/[^/][^/][^/]+/[^/]com.apple.IconServices  
and  
/private/var/folders/[^/][^/][^/]+/[^/][^/]+.mozrunner/extensions/[^/]/chrome/[^/]+/content/[^/]+.j(s|ar)  
Not found on my 10.11 system.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_chromregex)

```
  "    (allow file-write* (var-folders2-regex \\"/org\\\\.chromium\\\\.[a-zA-Z0-9]*$\\"))\\n"

```

Write access to  
/private/var/folders/[^/][^/][^/]+/[^/]org.chromium.[a-Z0-9]\*  
Not found on my 10.11 system.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_lib_ext)

```
  "; Per-user and system-wide Extensions dir\\n"
  "  (allow file-read*\\n"
  "      (home-regex \\"/Library/Application Support/[^/]+/Extensions/[^/]/\\")\\n"
  "      (resolving-regex \\"/Library/Application Support/[^/]+/Extensions/[^/]/\\"))\\n"

```

See the docs on extensions.autoDisableScopes for more information on these paths. The first path allow access to an Extensions directory that applies to all of a user's profiles. The second is for a system-wide Extensions directory that applies to all users. The setting of extensions.autoDisableScopes controls whether or not these locations are used.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#level1_fs_rules)

```
  "; The following rules impose file access restrictions which get\\n"
  "; more restrictive in higher levels. When file-origin-specific\\n"
  "; content processes are used for file:// origin browsing, the\\n"
  "; global file-read* permission should be removed from each level.\\n"
  "\\n"
  "; level 1: global read access permitted, no home write access\\n"
  "  (if (= sandbox-level 1)\\n"
  "    (begin\\n"
  "      (allow file-read*)\\n"
  "      (allow file-write* (require-not (subpath home-path)))))\\n"

```Level 1-specifc filesystem rules: allow reading to anywhere the OS permits. Do not allow writing anywhere in the home directory.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#level2_fs_rules)

```
  "; level 2: global read access permitted, no home write access,\\n"
  ";          no read/write access to ~/Library,\\n"
  ";          no read/write access to $PROFILE,\\n"
  ";          read access permitted to $PROFILE/{extensions,weave}\\n"
  "  (if (= sandbox-level 2)\\n"
  "    (if (not (zero? hasProfileDir))\\n"
  "      ; we have a profile dir\\n"
  "      (begin\\n"
  "        (allow file-read* (require-all\\n"
  "              (require-not (home-subpath \\"/Library\\"))\\n"
  "              (require-not (subpath profileDir))))\\n"
  "        (allow file-write* (require-all\\n"
  "              (require-not (subpath home-path))\\n"
  "              (require-not (subpath profileDir))))\\n"
  "        (allow file-read*\\n"
  "              (profile-subpath \\"/extensions\\")\\n"
  "              (profile-subpath \\"/weave\\")))\\n"
  "      ; we don't have a profile dir\\n"
  "      (begin\\n"
  "        (allow file-read*\\n"
  "              (require-not (home-subpath \\"/Library\\")))\\n"
  "        (allow file-write* (require-all\\n"
  "              (require-not (subpath home-path)))))))\\n"

```Level 2-specifc filesystem rules: do not allow writing anywhere in the home directory. Allow reading to anywhere the OS permits except ~/Library or the PROFILE dir. Allow reading from PROFILE/extensions and PROFILE/weave.

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_printpreview)

```
  "\\n"
  "; accelerated graphics\\n"
  "    (allow-shared-preferences-read \\"com.apple.opengl\\")\\n"
  "    (allow-shared-preferences-read \\"com.nvidia.OpenGL\\")\\n"
  "    (allow mach-lookup\\n"
  "        (global-name \\"com.apple.cvmsServ\\"))\\n"
  "    (allow iokit-open\\n"
  "        (iokit-connection \\"IOAccelerator\\")\\n"
  "        (iokit-user-client-class \\"IOAccelerationUserClient\\")\\n"
  "        (iokit-user-client-class \\"IOSurfaceRootUserClient\\")\\n"
  "        (iokit-user-client-class \\"IOSurfaceSendRight\\")\\n"
  "        (iokit-user-client-class \\"IOFramebufferSharedUserClient\\")\\n"
  "        (iokit-user-client-class \\"AppleSNBFBUserClient\\")\\n"
  "        (iokit-user-client-class \\"AGPMClient\\")\\n"
  "        (iokit-user-client-class \\"AppleGraphicsControlClient\\")\\n"
  "        (iokit-user-client-class \\"AppleGraphicsPolicyClient\\"))\\n"
  "\\n"
  "; bug 1153809\\n"
  "    (allow iokit-open\\n"
  "        (iokit-user-client-class \\"NVDVDContextTesla\\")\\n"
  "        (iokit-user-client-class \\"Gen6DVDContext\\"))\\n"

```Graphics

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_tempitems)

```
  "\\n"
  "; bug 1201935\\n"
  "    (allow file-read*\\n"
  "        (home-subpath \\"/Library/Caches/TemporaryItems\\"))\\n"
  "\\n"

```Read-only access to anything in ~/Library/Caches/TemporaryItems

[link](https://wiki.mozilla.org/Sandbox/OS_X_Rule_Set#aar_apptempdir)

```
  "; bug 1237847\\n"
  "    (allow file-read*\\n"
  "        (home-subpath appTempDir))\\n"
  "    (allow file-write*\\n"
  "        (home-subpath appTempDir))\\n"
  "  )\\n"
  ")\\n";

```

