#!/usr/bin/env python3

import functools
import os
from os.path import isdir
from typing import is_typeddict
root = os.path.dirname(__file__)
root = os.path.abspath(root)


def get_relative_path(path):
    path = os.path.abspath(path)
    s = path.replace(root, "")
    return s
    if len(s) == 0:
        return s
    if s[0] == '/':
        return s[1:]
    return s


current = os.getcwd()
currentpath = os.path.abspath(".")
parent = "" if root == currentpath else get_relative_path(
    os.path.abspath(".."))
parent2 = get_relative_path(os.path.abspath("."))
# print(11111,parent,nn,2222222222222)
# print(root,pz arent)

# print(222222,parent)
aa = os.listdir(current)
aa = list(filter(lambda a: (os.path.isdir(a) and len(
    a) > 1 and a[0] != '.') or a.find('.md') > 0, aa))


def cmp(a, b):
    if os.path.isdir(a) and os.path.isdir(b) == False:
        return -1
    if os.path.isdir(a) == False and os.path.isdir(b):
        return 1
    # return a > b
    # return -1
    if a > b:
        return 1
    return 0 if a == b else -1


aa = sorted(aa, key=functools.cmp_to_key(cmp))
# aa=sorted(aa)


def escapePath(path):
    path = path.replace('\\', "%5C")
    path = path.replace(" ", "%20")
    path = path.replace(":", "%3A")
    path = path.replace(",", "%2C")
    path = path.replace('"', "%22")
    path = path.replace('#', "%23")
    return path


ret = ["- [[%s]](%s/)" % (".." if len(parent) else ".", parent)]
for a in aa:
    if a == "_sidebar.md" or a.upper() == "README.MD":
        continue
    if os.path.isdir(a):
        if a.find("_files") > 0:
            continue
        ret.append("- [[%s]](%s/)" % (a, escapePath(os.path.join(parent2, a))))
    else:
        url = escapePath(os.path.join(parent2, a))
        ret.append('- [%s](%s)' % (a.replace('.md', ''), url))
open("_sidebar.md", "w").write("\n".join(ret))
open("README.md", "w").write("\n".join(ret))
# open("index.md","w").write("\n".join(ret))

if __name__ == "__main__":
    pass
