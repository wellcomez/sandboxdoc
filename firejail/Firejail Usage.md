---
title: "Firejail Usage"
layout: post
---
[firejail.wordpress.com /documentation-2/basic-usage/](https://firejail.wordpress.com/documentation-2/basic-usage/)

# Firejail Usage

23-30 minutes 2015/12/15

---

Welcome to Firejail, a SUID security sandbox based on Linux namespaces and seccomp-bpf. We are a volunteer weekend project and our target is the desktop. Linux beginner or accomplished programmer, you are welcome to join us at http://github.com/netblue30/firejail.

In cybersecurity there are two distinct cases: server and desktop. For servers, using various SUID and kernel exploits, the attacker tries to access information stored on the system as root. The security industry is mainly dealing with this case. With tools and procedures developed by big government and/or big business, the desktop and the home user are never a priority.

Our focus is the desktop. You can find a text-book attack example on [Mozilla’s webpage](https://blog.mozilla.org/security/2015/08/06/firefox-exploit-found-in-the-wild/). It’s obvious the attacker is targeting system administrators and other software professionals. The info he is after is stored in the home directory: passwords, encryption keys, financial documents, user identity (Tor browser). As a side note, the attacker is not likely to attempt to exploit SUID binaries or kernel bugs, since this might alert the system owner.

This document is an effort to centralize project information currently spread across several howtos, blogs, and discussion threads. I’ll start with a short description of the kernel technologies involved, move to sandbox configuration and management, and explore some of the most common usage scenarios. We assume the user has a clean, updated Linux system without any malicious software already installed.

## Contents

## 1. Technology

There is nothing magic about the internal workings of a sandbox, just some kernel security technologies stack one on top of the other. As a user you don’t deal with them directly, Firejail takes care of it. We offer preconfigured security profiles for more than 1000 Linux applications, and if your application is not among them, no problem – the default configuration should just work!

## 1.1. The Linux Kernel

All Firejail security features are implemented inside Linux kernel. The sandbox program configures the kernel and goes to sleep. The setup is very fast, usually tens of milliseconds. In very complicated setups it can go as high as 1 second. The memory requirements are low, all it needs are a few MB of RAM. As for slowing down the application, I don’t think you’ll notice any.

We divide the kernel technologies used for sandboxing in three categories:

![img](firejail-usage2.png)

Front-end technologies are simple and very effective. They are designed to withstand a massive attack.

Back-end technologies are smart and sophisticated. They play a support role, i.e to keep the front-end in place. We use them to lock the attacker inside the sandbox, and prevent him from becoming root.

In the third category, we place the technologies we use to configure the kernel. Currently there are exactly two technologies available: SUID and user namespaces. Both of them are insecure. User namespace has the advantage when things go wrong you can blame it on kernel developers. For Firejail we use SUID.

## 1.2 What is SUID, and how does it affect me?

SUID (**S**et owner **U**ser **ID** upon execution) is a special type of file permission. Most programs running on your computer inherit access permissions from the user logged in. SUID allows the program to run as root, rather than the user that started the program.

We use this Linux feature to start the sandbox, since most kernel technologies involved in sandboxing require root privileges. Once the sandbox is started, the privileges are dropped, and the real program starts as regular user. For example, in the case of a Firefox browser we build the sandbox as root, drop privileges, then we start the browser as a regular user.

SUID programs are considered dangerous on multiuser systems. It is not a great idea to install Firejail on such systems. If you have a server full of people logging in over SSH, forget about it!

Here are some of the ways to mitigate the problems introduced by SUID:

**1. Configure /etc/firejail/firejail.users**

The file contains a simple list with all the users allowed to run Firejail. If the file is not configured, all users are allowed to run Firejail.

**2. Use `firecfg`**

Integrate your desktop software with Firejail, by running `firecfg` utility described in [Desktop Integration](https://firejail.wordpress.com/documentation-2/basic-usage/#integration) section. As a result, most programs will be sandboxed automatically, and a `/etc/firejail/firejail.users` file is created.

**3. Set `force-nonewprivs` flag**

Turn on force-nonewprivs flag in /etc/firejail/firejail.config file. As root, open the file in a text editor and add this line:

```
force-nonewprivs yes
```

The flag prevents rising privileges after the sandbox was started. It is believed to clean most SUID problems that will ever be attributed to Firejail.

Note: you should avoid doing this if you use a Chromium-based browser and have set kernel.unprivileged_userns_clone=0 (or are on one of the few distros which do this by default.) Unfortunately, Chromium-based browsers need to rise privileges in order to install their own SUID sandbox.

**4. Create a special `firejail` group**

This is equivalent to `/etc/firejail/firejail.users` trick above, but build using file system primitives. Create a `firejail` group, set `/usr/bin/firejail` executable as part of this group, change the file mode to 4750, and add only the users allowed to use Firejail to the group. Sample set of instructions on Debian:

```
$ su# addgroup firejail# chown root:firejail /usr/bin/firejail# chmod 4750 /usr/bin/firejail# ls -l /usr/bin/firejail-rwsr-x--- 1 root firejail 1584496 Apr 5 21:53 /usr/bin/firejail
```

To add the user to the group, type:

```
# usermod -a -G firejail username
```

A logout and login back is necessary after adding the user to the group.

**5. Consider running the long term support release**

The current LTS release was branched out from version 0.9.56 in October 2018. It includes only bugfixes and additional SUID hardening. The code base is much smaller, and easier to audit.

## 2. Usage and Configuration

## 2.1 Installation

Try installing Firejail using your package manager first. Firejail is included in a large number of distributions. Among them Arch, Debian, Gentoo, Mint, Slackware, Ubuntu.

You can find newer versions of the software on our [download](https://firejail.wordpress.com/download-2/) page. We keep there up to date .deb packages for Debian/Ubuntu/Mint. You can also download the source archive and compile it yourself. There are no external dependencies, all you need is a C compiler (`sudo apt-get install build-essential`) and a regular compile/install (`./configure && make && make install`).

## 2.2 Basic Usage

Start the sandbox by prefixing your application with “firejail”:

```shell
$ firejail firefox
Reading profile /etc/firejail/firefox.profile
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-programs.inc
Reading profile /etc/firejail/disable-devel.inc
Reading profile /etc/firejail/whitelist-common.inc
Blacklist violations are logged to syslogChild process initialized
```

Any type of GUI programs should work with sound, video and hardware acceleration support if available on the platform. This makes Firejail ideal for running desktop applications such as web browsers, media players, and games.

## 2.3 Desktop Integration

To integrate Firejail with your desktop environment run:

```
$ sudo firecfg
```

As a result:

- Clicking on desktop manager icons and menus will sandbox the application automatically. We support Cinnamon, KDE, LXDE/LXQT, MATE and XFCE desktop managers, and partially Gnome 3 and Unity. This part works well across all Linux distributions.
- Clicking on files in your file manager will open the file in a sandboxed application. It works fine in newer Linux distributions like Debian “stretch”, Ubuntu 17.04, Arch, Gentoo.

You can always check if your application was sandboxed by running `"firejail --list"` in a terminal. Or you can keep a terminal running `"firejail --top"` to track your sandboxes.

![img](top1.png)

Monitoring sandboxes with “firejail –top”

Some users prefer desktop launchers for stating applications. A launcher is a regular text file with .desktop extension placed in `~/Desktop` directory. This is an example for Mozilla Firefox browser:

```
$ cat ~/Desktop/firefox.desktop[Desktop Entry]Type=ApplicationName=FirefoxIcon=firefox.pngExec=firejail firefoxTerminal=false
```

## 2.4 Security profiles

We distribute Firejail with over 1000 security profiles, covering most common Linux applications. Profile files have a friendly syntax, and are stored in `/etc/firejail` directory.

Profiles build by users should be placed in `~/.config/firejail` directory. Use `"include"` command to bring in the original profile file, then add your commands. For example, this is a profile for a [VLC](https://www.videolan.org/vlc/index.html) media player without network access:

```
$ cat ~/.config/firejail/vlc.profileinclude /etc/firejail/vlc.profilenet none
```

For more information see [Building Custom Profiles](https://firejail.wordpress.com/documentation-2/building-custom-profiles/).

## 2.5 Managing Sandboxes

The relevant command line options are as follow:

- `firejail --list` – list all running sandboxes
- `firejail --tree` – list all running sandboxes and the processes running in each sandbox
- `firejail --top` – similar to Linux `top` command

In case a sandbox is not responding and you need to shut it down, use `--shutdown` option. First, list the sandboxes,

```
$ firejail --list
3787:netblue:firejail --private
3860:netblue:firejail firefox
3963:root:firejail /etc/init.d/nginx start
```

and then shutdown the sandbox using the PID number from the list. In this example I shut down Firefox browser from the example above:

```
$ firejail --shutdown=3860
```

Use `--join` option if you need to join an already running sandbox. It works like a regular terminal login in the sandbox. The new shell session inherits all the sandbox restrictions:

```
$ firejail --join=3860
Switching to pid 3861, the first child process inside the sandbox
[netblue@debian ~]$ ps aux
USER PID %CPU %MEM VSZ RSS TTY STAT START TIME COMMAND
netblue 1 12.1 4.5 996168 320576 ? Sl 07:33 1:59 firefox
netblue 77 2.5 0.0 20916 3716 pts/2 S 07:49 0:00 /bin/bash
netblue 120 0.0 0.0 16840 1256 pts/2 R+ 07:49 0:00 ps aux
[netblue@debian ~]$
```

## 3. Filesystem

## 3.1 Private Mode

Private mode is a quick way to hide all the files in your home directory from sandboxed programs. Enable it using `--private` command line option:

```
$ firejail --private firefox
```

Firejail mounts a temporary `tmpfs` filesystem on top of `/home/user` directory. Any files created in this directory will be deleted when you close the sandbox. You can also use an existing directory as home for your sandbox, allowing you to have a persistent home:

```
$ firejail --private=~/my_private_dir firefox
```

## 3.2 Chroot

Most of the time I’m happy with the applications distributed by Debian “stable”, but occasionally I need a much newer version of a program or another. In this case, I build a Debian “unstable” chroot on my “stable” system, and run my application (youtube-dl) using Firejail’s chroot feature. These are the steps:

**Step 1: Build a basic Debian sid filesystem:**

```
$ sudo mkdir /chroot-sid$ sudo debootstrap --arch=amd64 sid /chroot-sid
```

**Step 2: Add a regular user account and install the target application** ([youtube-dl](https://rg3.github.io/youtube-dl/) in this example):

```
$ sudo firejail --noprofile --chroot=/chroot-sid# adduser netblue# apt-get install youtube-dl# exit
```

**Step 3: Run the application:**

```
$ firejail --chroot=/chroot-sid$ youtube-dl https://www.youtube.com/watch?v=Yk1HVPOeoTc
```

The setup also works for GUI programs such as [mpv](https://mpv.io/) and [HandBrake](https://handbrake.fr/), you just have to bring the programs in:

```
$ sudo firejail --noprofile --chroot=/chroot-sid# apt-get update# apt-get upgrade# apt-get install handbrake mpv
```

## 3.3 OverlayFS

One use case for Firejail’s [OverlayFS](https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt) front-end is testing new software packages. All filesystem modifications performed while installing and running the software are stored in overlay layer. The host filesystem is not touched.

This is an example of testing [Gnome AisleRiot](https://help.gnome.org/users/aisleriot/stable/) game from the regular Debian repository. AisleRiot is a collection of over eighty different solitaire card games, including popular variants such as spider, freecell, klondike, thirteen (pyramid), yukon, canfield and many more.

The steps are as follow:

**Step 1: Start a root sandbox with a temporary OverlayFS filesystem**

```
$ sudo firejail --noprofile --overlay-tmpfs#
```

This is a very relaxed sandbox. All directories are visible, with an overlay on top of them. The only filter installed is seccomp. This means you package manager will not be able to install and load new kernel modules. Also, if you are thinking about installing server programs, it will not work – systemd lives in a different namespace, and it will fail to find your new server.

**Step 2: Install the program**

```
# apt-get install aisleriot
```

**Step 3: Switch to your regular user and run the program**

```
# su netblue$ sol
```

![Playing Klondike (AisleRiot) in a Firejail sandbox](aisleriot.png%3Fw%3D625.png)

Playing Klondike (AisleRiot) in a Firejail sandbox

## 3.4 AppImage

The previous chroot and OverlayFS tricks will only get you so far. As more and more complex applications are built by thousands of Linux users, new ways of distributing software emerged. My favourite is [AppImage](https://appimage.org/).

We introduced AppImage support in 2016, and since then we added more features, bug fixes etc. On their side, AppImage team kept on bringing in new cool stuff, such as a new filesystem layout and a croud-sourced repository of [appimages](https://appimage.github.io/) for most Linux applications.

Here is a simple usage example: the latest and greatest [Kdenlive](https://kdenlive.org/) video editor built and distributed by the developer.

![Kdenlive AppImage running in Firejail](kdenlive.png%3Fw%3D625.png)

Kdenlive AppImage running in Firejail

I create a private home directory for this application and start the appimage in this directory:

```
$ mkdir ~/mykdenlive
$ firejail --private=~/mykdenlive --appimage ~/Downloads/Kdenlive-17.12.0d-x86_64.AppImage
Mounting appimage type 2
Reading profile /etc/firejail/default.profile
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-passwdmgr.inc
Reading profile /etc/firejail/disable-programs.inc
** Note: you can use --noprofile to disable default.profile **
Parent pid 17670, child pid 17673Dropping all Linux capabilities and enforcing default seccomp filterChild process initialized in 60.82 ms...
```

All the files I am editing are in `~/mykdenlive` directory, no other files in my home are visible in the sandbox. You can find more examples in our [AppImage Support](https://firejail.wordpress.com/documentation-2/appimage-support/) document.

## 3.5 AppArmor

Currently, AppArmor is enabled by default in Ubuntu and Debian kernels. On other distributions you’ll have to enable it yourself. These are the instructions for [Arch Linux](https://wiki.archlinux.org/index.php/AppArmor) and [Gentoo](https://wiki.gentoo.org/wiki/AppArmor).

Like Firejail, AppArmor restricts programs’ capabilities with per-program profiles. If you have an AppArmor profile for your application, use it! There is some overlap between the two technologies: both of them restrict the same filesystem. In case one of them misses something important, hopefully the other one picks it up.

If you don’t have an AppArmor profile for your specific application, we give you one. The profile is installed in `/etc/apparmor.d/firejail-default` when you install Firejail. You would need to load it into the kernel by running the following command:

```
$ sudo aa-enforce firejail-default
```

or reboot the computer and Firejail AppArmor profile will be loaded automatically into the kernel.

When you start your application, use `--apparmor` command line option to enable AppArmor confinement inside your sandbox:

```
$ firejail --apparmor warzon2100
```

In your Firejail profile files, use `apparmor` command. This is the previous VLC profile with AppArmor support:

```
$ cat ~/.config/firejail/vlc.profileinclude /etc/firejail/vlc.profilenet noneapparmor
```

## 3.6 EncFS and SSHFS

[EncFS](https://github.com/vgough/encfs) is an encrypted filesystem built on top of [FUSE](https://github.com/libfuse/libfuse) library. It is available on most Linux distributions, and it runs in user space. Integrating EncFS with Firejail brings up an interesting problem. Take a look at this paragraph in *man encfs*:

> By default, all FUSE based filesystems are visible only to the user who mounted them. No other users (including root) can view the filesystem contents.

For various reasons, during sandbox setup Firejail handles EncFS filesystems as root user. FUSE will prevent the root access to user’s files and the sandbox will fail to start.

This problem affects all filesystems based on FUSE library. Quite popular among them is [sshfs](https://github.com/libfuse/sshfs). The solution is to allow root user to access the filesystem using `allow_root` FUSE mount flag. On some distributions (Debian & friends) you might have to change FUSE config file in `/etc/fuse.conf` and uncomment `user_allow_other` line:

```
$ cat /etc/fuse.conf# /etc/fuse.conf - Configuration file for Filesystem in Userspace (FUSE)
# Set the maximum number of FUSE mounts allowed to non-root users.# The default is 1000.#mount_max = 1000
# Allow non-root users to specify the allow_other or allow_root mount options.user_allow_other
```

This is how to start a Firejail-friendly EncFS:

```
$ encfs -o allow_root ~/.crypt ~/crypt
```

And this is a SSHFS:

```
$ sshfs -o reconnect,allow_root netblue@192.168.1.25:/home/netblue/work work
```

After mounting your FUSE filesystem, start your sandboxes the regular way.

## 4. Networking

A network namespace is a new, independent TCP/IP stack attached to the sandbox. The stack has its own routing table, firewall and set of interfaces. Apart from “net none” and an optional “netfilter”, we never configure networking features in the security profiles distributed with the sandbox software.

You can create a network namespace with –net command. There are three setups to choose from:

- –net=none creates a network namespace unconnected to the real network. The sandbox looks like a computer without any network interfaces.
- –net=macvlan-device creates a direct network setup. The namespace is connected on the same network as your Ethernet interface using a macvlan kernel device. This is the easiest setup for home users. Unfortunately, the macvlan Linux kernel device works only for wired Ethernet interfaces.
- –net=bridge-device connects the sandbox to a bridge kernel device. The regular network stack routes the sandbox traffic to your main wired/wireless interface.

## 4.1 Direct Network Setup

Run “ip addr show” to find the name of your wired Ethernet interface (eth0 in my case):

```shell
$ ip addr show  
1: lo: mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1  
link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00  
inet 127.0.0.1/8 scope host lo  
valid_lft forever preferred_lft forever  
inet6 ::1/128 scope host  
valid_lft forever preferred_lft forever  
2: eth0: mtu 1500 qdisc pfifo_fast state UP group default qlen 1000  
link/ether e0:3f:4f:72:14:a9 brd ff:ff:ff:ff:ff:ff  
inet 192.168.1.50/24 brd 192.168.1.255 scope global eth0  
valid_lft forever preferred_lft forever  
inet6 fe80::e23f:49ff:fe7a:1409/64 scope link  
valid_lft forever preferred_lft forever```

and start the sandbox

```
$ firejail --net=eth0 firefox
```

You can specify an IP address (–ip=192.168.1.207), a range of IP addresses (–iprange=192.168.1.100,192.168.1.240) to choose from, or you can let the sandbox find an unused IP address on your network.

Because of the way macvlan kernel drivers are wired to the real Ethernet interface, it is not possible for the sandboxed application to access TCP/IP services running on the host, and the other way around. The sandbox and the host are totally disconnected, even if both of them are on the same network.

![Direct network](directnet.png)Direct network

This is a Firefox profile adding network namespace support to the sandbox:

```shell
$ cat ~/.config/firejail/firefox-exr.profile  
include /etc/firejail/firefox-esr.profile  
net eth0  
iprange 192.168.1.100,192.168.1.240
```

Similar, a profile for [Transmission](https://transmissionbt.com/):

```shell
$ cat ~/.config/firejail/transmission-qt.profile  
include /etc/firejail/transmission-qt.profile  
net eth0  
iprange 192.168.1.100,192.168.1.240
```

In the examples above, I let Firefox and Transmission fight for address in 192.168.1.100 – 192.168.1.240 range. Actually, all network clients on my home network are fighting for addresses in this range. To monitor the traffic use “firejail –netstats”.

![](https://firejail.files.wordpress.com/2017/07/netstats1.png)
Monitoring network traffic with “firejail –netstats”

## 4.2 Routed Network Setup

In a routed setup sandboxes are connected to a Linux bridge, and the bridge traffic is routed by the host. This setup works for both wired and wireless interfaces. Address translation needs to be enabled on the host in order for the sandbox traffic to go out on Internet:

![routednet](routednet.png)

Script for setting this up – I assume a wired eth0 interface for the system:
~~~shell
#!/bin/bash
 

#
# Routed network configuration script
#

# bridge setup
brctl addbr br0
ifconfig br0 10.10.20.1/24 up

# enable ipv4 forwarding
echo "1" > /proc/sys/net/ipv4/ip_forward

# netfilter cleanup
iptables --flush
iptables -t nat -F
iptables -X
iptables -Z
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

# netfilter network address translation
iptables -t nat -A POSTROUTING -o eth0 -s 10.10.20.0/24 -j MASQUERADE
~~~



Starting the sandbox:

```
$ firejail --net=br0 firefox
```

For running servers I replace network address translation with port forwarding in the script above:
~~~shell
# host port 80 forwarded to sandbox port 80
iptables -t nat -A PREROUTING -p tcp --dport 80 -j DNAT --to 10.10.20.10:80
~~~


## 4.3 Traffic shaping

Network bandwidth is an expensive resource shared among all sandboxes running on a system. Traffic shaping allows the user to increase network performance by controlling the amount of data that flows into and out of sandboxes. Firejail implements a simple rate-limiting shaper based on Linux *tc* command. The shaper works at sandbox level:

```
$ firejail --name=browser --net=eth0 firefox &$ firejail --bandwidth=browser set eth0 80 20
```

In this example I set a bandwidth of 80 kilobytes per second on receive side and a bandwidth of 20 kilobytes per second on transmit side. As the sandbox is running, I can change the values or even reset them:

```
$ firejail --bandwidth=browser set eth0 40 10$ firejail --bandwidth=browser clear eth0
```

## 5. X11 Sandboxing

If you don’t have Wayland running, the most reliable way to sandbox X11 with Firejail is [Xephyr](https://en.wikipedia.org/wiki/Xephyr). Xephyr is a light X11 server you can run in parallel with the main xorg server on your machine. The software is part of [X.Org](https://www.x.org/wiki/).

In this example I use Firejail to sandbox two applications, Inkscape and Firefox, in a the same Xephyr window.

**Step 1. Sandbox Xephyr**

In order to be able to rearrange and resize windows, I start OpenBox window manager on top of Xephyr. Notice `--net=none` command option.

```
$ firejail --x11=xephyr --net=none openbox&
```

*Note: You can replace openbox with any other supported window manager. Currently we support openbox, fluxbox, blackbox, awesome and i3.*

As a rule, whenever we are dealing with X11 we also need to install a new network namespace. This is the only way to block access to the abstract Unix socket opened by the main X11 server already running on your box. Every application sandboxed on this display server is required to install a network namespace, either `--net=none` or `--net=eth0`.

**Step 2. Find the display number for the new server**

Each X11 server server running on your box is identified by a unique display number. This number is used to connect X11 applications to a specific X11 server. Run `firemon -x11` to find Xephyr’s display number:

```
$ firemon --x112377:netblue::/usr/bin/firejail /usr/bin/Xephyr -ac -br -noreset -screen 1024x2394:netblue::firejail --net=none openboxDISPLAY :265
```

The display number is 265. Notice how Xephyr and OpenBox are running in independent Firejal sandboxes. Let’s start some more sandboxes:

**Step 3. Start your applications**

```
$ DISPLAY=:265 firejail --net=eth0 firefox -no-remote &$ DISPALY=:265 firejail --net=none inkscape &
```

![Independent Inkscape and Firefox sandboxes running in a Xephyr X11 window](hb-x11-xephyr.png%3Fw%3D625.png)

Independent Inkscape and Firefox sandboxes running in a Xephyr X11 window

In this moment I have 4 independent sandboxes, one for each program involved: Xephyr, OpenBox, Inkscape and Firefox.

![X11 sandboxing using Xephyr](hb-x11-list.png)

X11 sandboxing using Xephyr

## 6. Servers

As a rule, always use a new network namespace for server sandboxes in order to isolate services such as SSH, X11, DBus running on your workstation. This is an Apache server example:

```
# firejail --net=eth0 --ip=192.168.1.244 /etc/init.d/apache2 start
```

The default server profile is /etc/firejail/server.profile. To further restrict your servers, here are some ideas:

```
# capabilities list for Apache servercaps.keep chown,sys_resource,net_bind_service,setuid,setgid
# capabilities list for nginx servercaps.keep chown,net_bind_service,setgid,setuid
# use a netfilter configurationnetfilter /etc/firejail/webserver.net# instead of /var/www/html for webpages, use a different directorybind /server/web1,/var/www/html
```

You can run thousands of webservers on a regular system, each one with its own IP address, webpages, and applications.

And this is a Bind 9 DNS server setup – you can find the profile used in the video on our blog [here](https://firejail.wordpress.com/2021/10/26/securing-bind-9-with-apparmor-and-firejail/).
