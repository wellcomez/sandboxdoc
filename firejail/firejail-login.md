---
title: "firejail-login"
layout: post
---



# firejail-login(5) - Login file syntax for Firejail

VERSION, MONTH YEAR


<a name="description"></a>

# Description

/etc/firejail/login.users file describes additional arguments passed to the firejail executable
upon user logging into a Firejail restricted shell. Each user entry in the file consists of
a user name followed by the arguments passed to firejail. The format is as follows:

	user_name: arguments

Example:

	netblue: --net=none --protocol=unix

Wildcard patterns are accepted in the user name field:

	user*: --private


<a name="restricted-shell"></a>

# Restricted Shell

To configure a restricted shell, replace /bin/bash with /usr/bin/firejail in
the /etc/passwd file for each user that needs to be restricted. Alternatively,
you can specify /usr/bin/firejail using the \`adduser\` or \`usermod\` commands:

adduser --shell /usr/bin/firejail username  
usermod --shell /usr/bin/firejail username


<a name="files"></a>

# Files

/etc/firejail/login.users


<a name="license"></a>

# License

Firejail is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

Homepage: https://firejail.wordpress.com

<a name="see-also"></a>

# See Also

**firejail**(1),
**firemon**(1),
**firecfg**(1),
**firejail-profile**(5),
**firejail-users**(5),
**jailcheck**(1)

