---
title: "Building Custom Profiles"
layout: post
---

# Building Custom Profiles
<a style="text-decoration:underline" href="https://firejail.wordpress.com/documentation-2/building-custom-profiles/">Building Custom Profiles</a><br>

**Contents**

-   [Introduction](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#intro)
-   [The Access Control Layer](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#access)
-   [Building Security Profiles](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#building)
-   [Testing Security Profiles](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#testing)
-   [Customizing Security Profiles](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#customizing)
-   [A More Complex Example](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#more)
-   [jailcheck](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#jailcheck)
-   [Conclusion](https://firejail.wordpress.com/documentation-2/building-custom-profiles/#conclusion)

Introduction
------------

Firejail is a security system implemented using modern kernel technologies such as Linux namespaces and seccomp-bpf. On top of namespaces we place a thin software layer to control file access. By enforcing specific rules for each application, the access control layer proactively protects the system from external threats.

Security rules are stored in `/etc/firejail` directory in regular text files, one file for each application. We call these files security profiles. We recently passed the 1000 profile mark, covering various desktop programs people use every day. That’s about 10x more applications than SELinux and AppArmor combined!

This article is all about the access layer: how is built, how to create security profiles, how to test and how to customize them. I also describe jailcheck, a tool we introduced in version 0.9.66. The tool provides a high level view of the access system, warning the user in case of possible problems.

The Access Control Layer
------------------------

C code, complicated at times, it already went trough several iterations. This is the code that provides security and privacy to our users. Contrary to popular opinion, seccomp plays a secondary role in the grand scheme of things.

For Firefox we start with a list of directories the browser needs to work correctly:

-   `.mozilla, .pki, .config/pulse, .cache/mozilla, .local/share, Downloads`

Next, the list of directories we definitely don’t want the browser to access:

-   `.ssh, .gnupg` …

This list is much longer. It is spread over several files such as [this one](https://github.com/netblue30/firejail/blob/master/etc/inc/disable-common.inc) in our git repository. There are literally hundreds of entries, everything from desktop configuration, to X11, compilers, and system tools.

Lastly, we build a list with directories we don’t want to expose for privacy reasons:

-   `Documents, Desktop, Pictures` … and any other user directory

As we put all the lists together and load them in our file access engine, we end up with something like this:

![](https://firejail.files.wordpress.com/2021/02/firefox1-tandt.png?w=625)

User home directory in a sandboxed browser

The home directory is **virtual**. `Downloads` and the config files are brought in from the real file system, everything else is temporary and will be removed when the sandbox is closed.

We use this type of setup for networked applications, games, desktop tools such as calculators etc. For media players we bring in `Music` and `Videos`, for emails we bring in the email storage directory, and so on. Programs such as editors and console tools end up without the privacy list, and have access to most user directories (the security list is still included).

Firejail has no impact on the application speed, and it has a neglijable impact on user convenience. For example, the user won’t be able to download and upload files from random places in the file system, and web-based SSH tools won’t work in the browser. In our view, that’s a small price to pay for privacy and security.

Building Security Profiles
--------------------------

Security profiles are usually built starting from the template installed in `/usr/share/doc/firejail/profile.template`. Make a copy of the file and modify it. The details are in the template file, the sandbox commands are described in [man firejail-profile](https://firejail.wordpress.com/features-3/man-firejail-profile/).

You can also generate profiles automatically by running your application under `firejail --build` as described in the video above.

Testing Security Profiles
-------------------------

Here is a simple, practical method for testing file system access. It applies to any other mandatory access control system. Let’s start with this user question:

> “I have the key I use to connect to GitHub in `~/.ssh` directory, and another key I use to sign the release in `~/.gnupg`. That’s my threat exposure. How do I test Firejail?”

Copy a PNG file in `.ssh` directory and try to open it in [GIMP](https://www.gimp.org/):

~~~sh
$ cp test.png ~/.ssh/.   $ firejail gimp ~/.ssh/test.png
~~~

[![](https://firejail.files.wordpress.com/2021/03/gimp-tandt.png?w=625)](https://firejail.files.wordpress.com/2021/03/gimp-tandt.png)

Access test for .ssh directory

Copy the same file in `.gnupg` or any other directory you want to test. Do the same for other programs – for a media player use a `test.mp4` file and so on.

Even better, go in `File/Open` menu in your application and soon you’ll get an idea what’s covered and what’s not. This is LibreOffice:

![](https://firejail.files.wordpress.com/2021/02/libreoffice-tandt.png?w=896)

LibreOffice ~/.ssh test

For browsers, use the address bar to access your home directory:

[![](https://firejail.files.wordpress.com/2021/02/firefox2-tandt.png?w=625)](https://firejail.files.wordpress.com/2021/02/firefox2-tandt.png)

Access denied!

Customizing Security Profiles
-----------------------------

One funny complaint I got was _“I want to put my pictures on Facebook and `~/Pictures` is not there!”_. Go in `~/.config/firejail` directory and create a text file `firefox.profile` with the following content:

```
$ cat ~/.config/firejail/firefox.profile   
whitelist ~/Pictures   
include /etc/firejail/firefox.profile
```

Or you can just copy your pictures temporarily in `~/Downloads`.

One popular customization is removing network access for media players such as VLC:

```
$ cat ~/.config/firejail/vlc.profile   
net none   
include /etc/firejail/vlc.profile
```

Another interesting one is shutting down the sound in Warzone2100:

```
$ cat ~/.config/firejail/warzone2100.profile   
nosound   
include /etc/firejail/warzone2100.profile
```

A More Complex Example
----------------------

[Audacity](https://audacityteam.org/), the open source audio editor, has been in the news recently. The program was acquired by Muse Group, a multi-national company involved in data harvesting. Among other changes, starting with version 3.0.3 they introduced **telemetry**, a mechanism for phoning home with various pieces of data taken from user computers. Here is the problem:

Every telemetry program stores on hard disk an universally unique identifier ([UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier)). This is a huge random number used to track program install instances. This way they can differentiate users on a local network behind a network address translation (NAT) firewall.

Now, let’s say you start your computer, telemetry phones home, your IP address is recorded. Later, you start your VPN software and connect to Internet with a different IP. Telemetry phones home again, same UUID but a different IP – instant de-anonymization!

We’ve had `"net none"` in the default Audacity security profile distributed with Firejail software for a very long time. This disables network access. But since it’s the intention that counts, segregate Audacity in its own private home directory:

Create a new directory in your home, `~/myaudio`, and a new profile in `~/.config/firejail`:

~~~sh
$ cd ~   $ mkdir myaudio
$ cat ~/.config/firejail/audacity.profile   private ~/myaudio   net none   include /etc/firejail/audacity.profile
~~~

I added `"net none"` just in case you have an old version of Firejail. Move the files you want to edit in `~/myaudio`, and start the program (`firejail audacity`):

![](https://firejail.files.wordpress.com/2021/07/audacity.png?w=625)

Segregated home directory for Audacity

jailcheck
---------

**jailcheck** is a small program we use to simplify the testing. The program will attach itself to all running sandboxes and try to access `.ssh` and `.gnupg` directories. You can add more directories on the command line, see [man jailcheck](https://firejail.wordpress.com/man-jailcheck/).

```
$ sudo jailcheck   
**2014:netblue::firejail /usr/bin/gimp**      Virtual dirs: /tmp, /var/tmp, /dev, /usr/share,      Warning: I can run programs in /home/netblue      Networking: disabled

**2055:netblue::firejail /usr/bin/ssh -X netblue@x.y.z.net**      
Virtual dirs: /var/tmp, /dev, /usr/share, /run/user/1000,      Warning: I can read ~/.ssh      Networking: enabled

**2186:netblue:libreoffice:firejail --appimage /opt/LibreOffice-fresh.appimage**      Virtual dirs: /tmp, /var/tmp, /dev,      Networking: enabled`

**26090:netblue::/usr/bin/firejail /opt/firefox/firefox**      Virtual dirs: /home/netblue, /tmp, /var/tmp, /dev, /etc, /usr/share,   /run/user/1000,      Networking: enabled`
```

As I write this article I have Firefox, LibreOffice and GIMP open. I also have an SSH connection going out. As expected, `.ssh` directory is visible in SSH session. Nobody has access to `.gnupg`.

jailcheck also flags the presence of several **system tools** such as sudo, mount and strace, **compilers** such as gcc and clang, and **networking tools** such as dig, ncat, nmap and tcpdump, since these provide the perpetrators with a huge opportunity for mischief.

For each sandbox we list the most important **virtual directories** in use. These directories are build in temporary file systems by bringing in either real files and directories, or by copying files from the real file system. We deploy this type of mandatory access control in all sandboxes, with some differences here and there, depending on the application.

Another test is **noexec** test. In GIMP example above, the home directory allows GIMP to start programs stored locally. Usually these are application plugins the user installed himself, but they can also be aggressive malware programs inserted in the sandbox using a zero-day app exploit. If you don’t have external plugins installed, it is a good idea to make the home directory noexec:

```
$ cat ~/.config/firejail/gimp.profile   
noexec ${HOME}   include /etc/firejail/gimp.profile
```

Conclusion
----------

