---
date: 2023-04-13 10:50
title: X11 Guide
tags:
- x11
- firejail
- Xpra
- xephyr
---

# X11 Guide

<a style="text-decoration:underline" href="https://firejail.wordpress.com/documentation-2/x11-guide/">X11 Guide</a><br>
**Contents**

-   [Introduction](https://firejail.wordpress.com/documentation-2/x11-guide/#intro)
-   [Configuring Xpra](https://firejail.wordpress.com/documentation-2/x11-guide/#configurexpra)
-   [Configuring Xephyr](https://firejail.wordpress.com/documentation-2/x11-guide/#configurexephyr)
-   [How to test](https://firejail.wordpress.com/documentation-2/x11-guide/#howtotest)
-   [Attaching new sandboxes to an existing X11 server](https://firejail.wordpress.com/documentation-2/x11-guide/#listing)
-   [Resizing Xephyr window](https://firejail.wordpress.com/documentation-2/x11-guide/#resize)
-   [More…](https://firejail.wordpress.com/documentation-2/x11-guide/#more)

Introduction
------------

Firejail X11 sandboxing support is built around an external X11 server software package. Both [Xpra](http://xpra.org/) and [Xephyr](https://en.wikipedia.org/wiki/Xephyr) are supported (_apt-get install xpra xserver-xephyr_ on Debian/Ubuntu). To allow people to use the sandbox on headless systems, Firejail compile and install is not be dependent on Xpra or Xephyr packages.

The sandbox replaces the regular X11 server with Xpra or Xephyr server. This prevents X11 keyboard loggers and screenshot utilities from accessing the main X11 server.

![Mozilla
Firefox running in a X11 sandbox. The regular X11 server (X0 Unix socket) is not visible, and it was replaced by another X11 server (X723 Unix socket).](https://firejail.files.wordpress.com/2016/04/firefox-x11.png)

Mozilla Firefox running in a X11 sandbox. The regular X11 server (X0 Unix socket) is not visible, and it was replaced by another X11 server (X723 Unix socket).

The commands are as follows:

~~~sh
$ firejail --x11=xpra --net=eth0 program-and-arguments   $ firejail --x11=xephyr --net=eth0 program-and-arguments
~~~

A shorter form is also available:

~~~sh
$ firejail --x11 --net=eth0 program-and-arguments
~~~

In this case, Firejail will try first Xpra, and if Xpra is not installed on the system, it will try to find Xephyr. For various reasons, X11 sandboxing features are not supported if the sandbox is started as root user.

The main X11 server running on a Linux computer has two sockets active:

![Regular X11 server sockets](https://firejail.files.wordpress.com/2016/02/x11-x0.png)

Regular X11 server sockets

  
_/tmp/.X11-unix/X0_ is disabled using a temporary filesystem mounted on _/tmp/.X11-unix_ directory. The only way to disable the abstract socket _@/tmp/.X11-unix/X0_ is by using a network namespace. If for any reasons you cannot use a network namespace, the abstract socket will still be visible inside the sandbox. Hackers can attach keylogger and screenshot programs to this socket.

Configuring Xpra
----------------

![xpra-clipboard](https://firejail.files.wordpress.com/2016/02/xpra-clipboard.png)

Xpra is a persistent remote display server and client for forwarding X11 applications and desktop screens. The default configuration of the software package works fine in most cases, text configuration files are located in _/etc/xpra_ directory.

The current running session adds a small icon in the system tray. This can be used to enable or disable at run time the clipboard and a number of other parameters. It also provides statistics for the current session. Each active sandbox can be configured independently in such a window.

Configuring Xephyr
------------------

Xephyr runs in its own window just like any other X application, but it is an X server itself. Firejail sandboxed applications are started in this window. The default Xephyr window size is 800×600. This can be modified in /etc/firejail/firejail.config file, see man 5 firejail-config for more details. Or you can use _xrandr_, see [below](https://firejail.wordpress.com/documentation-2/x11-guide/#resize).

The recommended way to use this feature is to run a window manager inside the sandbox. This is the only way to resize or minimize the windows running on Xephyr server. Lots of light window managers are available on Linux platform, Firejail software provides a security profile for [Openbox](http://www.openbox.org/) (_apt-get install openbox_) as a reference in _/etc/firejail/openbox.profile_.

~~~sh
$ firejail --x11=xephyr --net=eth0 openbox
~~~

![Openbox running in a Firejail sandbox - right-click to access the application menu. ](https://firejail.files.wordpress.com/2016/04/openbox.png)

Openbox running in a Firejail sandbox – right-click to access the application menu.

Applications can be started using the window manager – right-click on an empty area of the window to open the application menu in Openbox. All apps started this way share the same sandbox with the window manager.

How to test
-----------

Start a sandboxed terminal (_firejail –x11 –net=eth0 xterm_) and run _netstat_ command. _/tmp/.X11-unix/X0_ sockets should not be visible:

![X11 server sockets visible in a Firejail sandbox.](https://firejail.files.wordpress.com/2016/02/x11-x305.png)

X11 server sockets visible in a Firejail sandbox.

To test the keyboard, I start an xterm sandbox (_firejail –x11 –net=eth0 –noprofile xterm_) and use xinput (_sudo apt-get install xinput_).

![Testing keyboard events in a Firejail sandbox](https://firejail.files.wordpress.com/2016/02/x11-x642.png)

Testing keyboard events in a Firejail sandbox

Testing screenshots is even easier: _firejail –x11 –net=none gimp_.

Attaching new sandboxes to an existing X11 server
-------------------------------------------------

[Drag and drop](https://en.wikipedia.org/wiki/Drag_and_drop) between windows running on different X11 servers is not possible. The way to get around this limitation is to run multiple sandboxed applications on the same server.

_firemon_ command was enhanced to print X11 display information:

~~~sh
$ firemon --x11   2142:netblue:firejail --x11 --net=eth0 firefox   DISPLAY :470
~~~

We have Firefox already running in a sandbox, on X11 display server :470. We start a new sandbox and place it on the same server:

~~~sh
$ DISPLAY=:470 firejail --net=eth0 transmission-gtk
~~~

Please note, a _–x11_ option is not necessary when the second sandbox is started, DISPLAY environment variable does all the magic.

_–net_ command line option is still required for both sandboxes in order to disable access to the main X11 server. Each sandbox has a different IP addresses and different container filesystems, but they share the X11 socket in _/tmp/.X11-unix_ directory. User Downloads directory is also shared between the sandboxes. Drag and drop between the two sandboxes is possible.

Resizing Xephyr window
----------------------

You can use _xrandr_ utility to resize a Xephyr window. I sandbox an xterm, find out the display number used by the sandbox, and resize the window:

~~~sh
$ firejail --x11=xephyr xterm &   
$ firemon --x11   16148:netblue:firejail xterm   DISPLAY :139   
$ xrandr --display :139   
default connected 640x400+0+0 (normal left inverted right x axis y axis) 0mm x 0mm   
1600x1200 0.00   1400x1050 0.00   1280x960 0.00   1280x1024 0.00   1152x864 0.00   1024x768 0.00   832x624 0.00   800x600 0.00   720x400 0.00   480x640 0.00   640x480 0.00   640x400 0.00*   320x240 0.00   240x320 0.00   160x160 0.00   
$ xrandr --display :139 --output default --mode 1280x1024
~~~

More…
-----

