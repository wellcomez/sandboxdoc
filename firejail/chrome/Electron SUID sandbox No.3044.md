---
title: "Electron SUID sandbox No.3044"
layout: post
---

# Electron SUID sandbox No.3044

[flatpak](https://github.com/flatpak)/[flatpak](https://github.com/flatpak/flatpak)Public

[fxha](https://github.com/fxha) opened this issue on Aug 5, 2019 · 21 comments Closed

[Electron SUID sandbox](https://github.com/flatpak/flatpak/issues/3044#top)#3044

[fxha](https://github.com/fxha) opened this issue on Aug 5, 2019 · 21 comments




### fxha on Aug 5, 2019

## Linux distribution and version

Fedora 30

## Flatpak version

`1.4.2`

## Description of the problem

Electron `>=v5` will fail to execute due no kernel access and invalid sandbox [binary permissions](https://github.com/electron/electron/issues/17972). Chromiums sandbox need user namespaces or the sandbox binary (`chrome-sandbox`) must be installed with SUID flag and root owner. Is there a way to configure the package that we can execute Electron with working sandbox? Currently `--no-sandbox` flag is needed to get a Electron v4 no renderer process sandbox behavior. Is it possible to own a file as root in the flatpak sandbox?

```
[2:0805/160109.913391:FATAL:setuid_sandbox_host.cc(157)] The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that /app/<name>/chrome-sandbox is owned by root and has mode 4755.
```

## Steps to reproduce

Build Electron `>=v5` as flatpak and try to execute it.





###  refi64 on Aug 6, 2019

Not possible, I guess the urgency of my previous Chromium-related experiments just shot up...

 [SISheogorath](https://github.com/SISheogorath) mentioned this issue [on Aug 6, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-issue-477219483)

[Remove --no-sandbox as soon as possible flathub/im.riot.Riot#59](https://github.com/flathub/im.riot.Riot/issues/59)

 Closed



### Contributor gasinvein on Aug 6, 2019

Isn't it more like a bubblewrap issue than flatpak's?



### Contributor Erick555 on Aug 6, 2019

No, `chrome-sandbox` needs suid bit (or unpriv user namespaces) and flatpak doesn't allow installing suid binaries (also it mounts fs with `nosuid`). This is the reason chrome isn't on flathub. `--no-sandbox` seems the only option.



### SISheogorath on Aug 6, 2019

[electron/electron#17972 (comment)](https://github.com/electron/electron/issues/17972#issuecomment-516957971) <-- at least according to this comment we might should raise and issue in the electron repository



### Author fxha on Aug 6, 2019

[@SISheogorath](https://github.com/SISheogorath) I think this is not a Chromium/Electron issue because Flatpak (or the sandbox Flatpak is using) don't provide a way to use Chromium's security features. From Chromium's perspective, it's the correct way to hard fail because the needed requirements are not meet. The issue with Chromium is that Flatpak's sandbox isolate the application and we as package maintainers cannot set the necessary permissions on the "fallback" sandbox binary. E.g. when installing `deb` or `rpm` packages, you can set the necessary permissions and the binary is automatically owned by `root`. So you have a fallback if user namspaces are not supported by the distro.





### andrunko on Aug 30, 2019

Related: [https://discourse.gnome.org/t/sandboxing-portal/1651](https://discourse.gnome.org/t/sandboxing-portal/1651).


 [andrunko](https://github.com/andrunko) mentioned this issue [on Aug 30, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-issue-486790101)

[Latest version of Skype won't run flathub/com.skype.Client#70](https://github.com/flathub/com.skype.Client/issues/70)Open

 [mrckndt](https://github.com/mrckndt) mentioned this issue [on Sep 7, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-issue-490319569)

[Signal 1.27.1 will not run flathub/org.signal.Signal#92](https://github.com/flathub/org.signal.Signal/issues/92)

 Closed



### enricotagliavini on Sep 7, 2019

SUID is just a fallback in case clone() with NEWUSER flag fails. Running a modern unpatched kernel requires no privileges to do that. However it seems tricky to do from inside an already made sandbox.

Would it be possible to (securely) allow something similar to [https://stackoverflow.com/questions/46140088/why-is-unprivileged-recursive-unshareclone-newuser-not-permitted](https://stackoverflow.com/questions/46140088/why-is-unprivileged-recursive-unshareclone-newuser-not-permitted)

However uid and gid maps seems to be set correctly in the sandbox as far as I understand, but then I don't get why the clone() syscall fails with EPERM

```shell
19719 readlink("/proc/self/exe", "/app/Signal/signal-desktop", 4096) = 26
19719 access("/app/Signal/chrome-sandbox", F_OK) = 0
19719 getresuid([1000], [1000], [1000]) = 0
19719 getresgid([1000], [1000], [1000]) = 0
19719 clone(child_stack=0x7fffbcdc6410, flags=CLONE_NEWUSER|SIGCHLD) = -1 EPERM (Operation not permitted)
19719 lstat("/app", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
19719 lstat("/app/Signal", {st_mode=S_IFDIR|0755, st_size=4096, ...}) = 0
19719 lstat("/app/Signal/signal-desktop", {st_mode=S_IFREG|0755, st_size=112456472, ...}) = 0
19719 socketpair(AF_UNIX, SOCK_SEQPACKET, 0, [11, 12]) = 0
19719 setsockopt(11, SOL_SOCKET, SO_PASSCRED, [1], 4) = 0
19719 access("/app/Signal/chrome-sandbox", F_OK) = 0
19719 stat("/app/Signal/chrome-sandbox", {st_mode=S_IFREG|S_ISUID|0755, st_size=235840, ...}) = 0
19719 access("/app/Signal/chrome-sandbox", X_OK) = 0
19719 getpid()                          = 2
19719 write(2, "[2:0907/113237.256171:FATAL:setuid_sandbox_host.cc(157)] The SUID sandbox helper binary was found, but is not configured correctly. Rather than run without sandboxing I'm aborting now. You need to make sure that /app/Signal/chrome-sandbox is owned by root and has mode 4755.\n", 275) = 275
```

The only other documented EPERM related to NEWUSER is

> CLONE_NEWUSER was specified in flags and the caller is in a chroot environment (i.e., the caller's root directory does not match the root directory of the mount namespace in which it resides).

but I haven't got a clue how to check that.



### Contributor Erick555 on Sep 7, 2019

> Running a modern unpatched kernel requires no privileges to do that.

Every kernel in Debian requires privileges for that. Maybe some other distros too



### enricotagliavini on Sep 7, 2019

> > Running a modern unpatched kernel requires no privileges to do that.
> 
> Every kernel in Debian requires privileges for that. Maybe some other distros too

Very true, but that's a deliberate Debian decision of not following upstream. I understand their reasons, and they have all the rights to do so.

However when running upstream-like kernels where unprivileged user namespace is available it should be used. A generic check for application requiring such a feature can be implemented and issue a warning they are being started with --no-sandbox when running on Debian or kernel without said feature.

If the above problem has a solution at all :)



### Contributor Erick555 on Sep 8, 2019

> A generic check for application requiring such a feature can be implemented and issue a warning they are being started with --no-sandbox when running on Debian or kernel without said feature.

For debian like kernels they have setuid sandbox. `--no-sandbox` may be acceptable for flatpak which has its own sandboxing but for distros like Debian it will be highly questionable. Also there were reports that `--no-sandbox` didn't work for skype at all.

[SISheogorath](https://github.com/SISheogorath) added a commit to SISheogorath/im.riot.Riot that referenced this issue [on Sep 17, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-commit-31a77ca)



[Upgrade to version 1.3.5](https://github.com/SISheogorath/im.riot.Riot/commit/31a77ca6ee71c0fa547aac7f90d2d0d76667377c )Upgrade to version 1.3.5 This upgrade also contains a temporary disabling of the electron sandbox. Since we don't ship SUID binaries in flatpaks and neasted user namespaces are still not a thing, this is the only way to have electron 5 applications running within flatpak. As soon as the flatpak upstream found a better solution, we'll switch to it and remove the `--no-sandbox` option again. Reference: https://github.com/flatpak/flatpak/issues/3044 …Verified

[31a77ca](https://github.com/SISheogorath/im.riot.Riot/commit/31a77ca6ee71c0fa547aac7f90d2d0d76667377c)`

 [SISheogorath](https://github.com/SISheogorath) mentioned this issue [on Sep 17, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-pullrequest-494206652) [Upgrade to version 1.3.5 flathub/im.riot.Riot#62](https://github.com/flathub/im.riot.Riot/pull/62) Merged



### Esokrates on Sep 30, 2019

IMO you should look for a way to make the chromium sandbox work, compromising upstream security is not what any downstream / distributor should do. You do way more harm disabling the chromium sandbox than allowing suid binaries in Flatpak.



### Member TingPing on Oct 1, 2019

I believe we'd all love that and it is the end goal. However nobody here is a Chromium contributor and probably doesn't even have a machine that can compile that project. Somebody will have to help out.



###   refi64 on Oct 1, 2019

Some background: I've made patches to implement this before, but from what I can tell, upstream has no interest in them. They basically said they'd be interested if Flatpak became the main Linux packaging standard, but as of right now, it's not. Afaik this was part the background for [@ramcq](https://github.com/ramcq)'s interest in making a cross-packaging sandbox API: it would be a much easier sell if we could say "any packaging / sandbox tool can implement this, and one (or two) major players already do. *. I've also used LD_PRELOAD to fake having an suid sandbox before, which works-ish, but right now I've got to basically make a custom Zygote of sorts for it that...isn't actually a Zygote, so that's going to take a bit of time to be ready for actual usage. (It would be great if --no-zygote could just be used, but using that without --no-sandbox causes assertion failures that can't be worked around.) The TLDR would be, "it's probably not getting upstreamed soon, and a workaround exists, but it's not ready yet".

[…](https://github.com/flatpak/flatpak/issues/3044#)

 [dvreed](https://github.com/dvreed) mentioned this issue [on Oct 24, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-pullrequest-511541753)[Update exodus to 19.10.18 flathub/io.exodus.Exodus#40](https://github.com/flathub/io.exodus.Exodus/pull/40) Closed



### mat8913 on Nov 17, 2019

From what I can tell, namespaces in flatpak are explicitly disallowed by

[flatpak/common/flatpak-run.c](https://github.com/flatpak/flatpak/blob/1f35dda1b50cceb24c471b19ac886dc91bf8a0b4/common/flatpak-run.c#L2481-L2485)

Lines 2481 to 2485 in [1f35dda](https://github.com/flatpak/flatpak/commit/1f35dda1b50cceb24c471b19ac886dc91bf8a0b4)
~~~c
/* Don't allow subnamespace setups: */

{SCMP_SYS (unshare)},

{SCMP_SYS (mount)},

{SCMP_SYS (pivot_root)},

{SCMP_SYS (clone), &SCMP_A0 (SCMP_CMP_MASKED_EQ, CLONE_NEWUSER, CLONE_NEWUSER)},

~~~
Would it be possible to add an option (`--allow=namespaces` perhaps) to allow it? Or would an app be able to use that to break free of flatpak's sandbox?



### Member TingPing on Nov 17, 2019

It is blacklisted because it is considred a security risk.



### Contributor ramcq on Nov 18, 2019

Alex is planning to work on a patch for Chromium/Electron which will ask Flatpak to set up the sandbox using the existing (or similar, if new capabilities are needed) API. The hope is that we can identify an API which can be provided as a portal so that both Snap and Flatpak can implement it. [https://discourse.gnome.org/t/sandboxing-portal/1651](https://discourse.gnome.org/t/sandboxing-portal/1651)

 [t-nelis](https://github.com/t-nelis) mentioned this issue [on Nov 20, 2019](https://github.com/flatpak/flatpak/issues/3044#ref-issue-357926998)
[Make brave available through Flatpak and Flathub brave/brave-browser#1000](https://github.com/brave/brave-browser/issues/1000) Closed



###  refi64 on Dec 13, 2019

While everything else is taking place, there's currently [zypak](https://github.com/refi64/zypak) which can be used for Electron apps to make them use the native Flatpak sandboxing primitives.



### SISheogorath on Dec 13, 2019

[@refi64](https://github.com/refi64) are you currently implementing it in the electron base flatpak? Would look forward to get this running for Riot where the PR was closed a while ago.



### malept on Dec 13, 2019

Not [@refi64](https://github.com/refi64), but I'm trying to get it working in [`@malept/electron-installer-flatpak`](https://github.com/malept/electron-installer-flatpak), which is obviously on a different level from the base flatpak.

_ETA: I filed a pull request with code that works, I've successfully installed/run an Electron 7 test app (created with Electron Forge) via Flatpak: [malept/electron-installer-flatpak#16](https://github.com/malept/electron-installer-flatpak/pull/16) ._



[kepalas](https://github.com/kepalas) added a commit to kepalas/com.meetfranz.Franz that referenced this issue [on Jan 2, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-commit-5275e7d)
[Fix electron sandboxing issue](https://github.com/kepalas/com.meetfranz.Franz/commit/5275e7dded228431b6f6c0089eeed2f1da9c2403 "Fix electron sandboxing issue Related to https://github.com/electron/electron/issues/17972, https://github.com/flatpak/flatpak/issues/3044")… [5275e7d](https://github.com/kepalas/com.meetfranz.Franz/commit/5275e7dded228431b6f6c0089eeed2f1da9c2403)

[TingPing](https://github.com/TingPing) pushed a commit to flathub/com.discordapp.Discord that referenced this issue [on Feb 26, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-commit-ceedf21)
[Fix electron sandboxing issue](https://github.com/flathub/com.discordapp.Discord/commit/ceedf212a713937b3191b99fda3f6a4dfe83a6e3 "Fix electron sandboxing issue Related to electron/electron#17972, flatpak/flatpak#3044") … [ceedf21](https://github.com/flathub/com.discordapp.Discord/commit/ceedf212a713937b3191b99fda3f6a4dfe83a6e3)

 [r-ml](https://github.com/r-ml) mentioned this issue [on Feb 26, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-issue-570793067)[0.0.10 fatal: SUID sandbox helper binary was found, but is not configured correctly flathub/com.discordapp.Discord#87](https://github.com/flathub/com.discordapp.Discord/issues/87)Closed

[G-Ray](https://github.com/G-Ray) added a commit to flathub/com.beakerbrowser.Beaker that referenced this issue [on Mar 14, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-commit-9b9e2b7)



[Fix electron sanboxing issue](https://github.com/flathub/com.beakerbrowser.Beaker/commit/9b9e2b70f24c2df6cc2d738a90939dbbb94b7513 "Fix electron sanboxing issue Related to https://github.com/electron/electron/issues/17972, https://github.com/flatpak/flatpak/issues/3044") … [9b9e2b7](https://github.com/flathub/com.beakerbrowser.Beaker/commit/9b9e2b70f24c2df6cc2d738a90939dbbb94b7513)

[klausenbusk](https://github.com/klausenbusk) added a commit to flathub/com.airtame.Client that referenced this issue [on Jun 6, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-commit-94a1e44)



[Update to 4.0.1](https://github.com/flathub/com.airtame.Client/commit/94a1e44714a45a574a40844a76fe19f7213c9bdc "Update to 4.0.1 Sadly --no-sandbox is required, see: https://github.com/flatpak/flatpak/issues/3044") … [](https://github.com/flathub/com.airtame.Client/commit/94a1e44714a45a574a40844a76fe19f7213c9bdc#comments)[94a1e44](https://github.com/flathub/com.airtame.Client/commit/94a1e44714a45a574a40844a76fe19f7213c9bdc)



### Contributor Erick555 on Sep 5, 2020

The zypak solution exist and works well so I think this issue should be closed as app maintainers are being misled that this is still unsolved problem and choose the wrong workaround.

cc [@TingPing](https://github.com/TingPing) [@barthalion](https://github.com/barthalion)

 [temrix](https://github.com/temrix) mentioned this issue [on Oct 16, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-pullrequest-722704881)

[Update base and add zypak-wrapper. flathub/io.lbry.lbry-app#82](https://github.com/flathub/io.lbry.lbry-app/pull/82) Merged

 [Chemrat](https://github.com/Chemrat) mentioned this issue [on Dec 11, 2020](https://github.com/flatpak/flatpak/issues/3044#ref-issue-761244453)

[Missing PWA shortcuts fucntionality flathub/com.brave.Browser#7](https://github.com/flathub/com.brave.Browser/issues/7) Closed

 [refi64](https://github.com/refi64) closed this as [completed](https://github.com/flatpak/flatpak/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on Aug 24, 2021](https://github.com/flatpak/flatpak/issues/3044#event-5198473563)

 [atomicptr](https://github.com/atomicptr) mentioned this issue [on May 5](https://github.com/flatpak/flatpak/issues/3044#ref-issue-1221393821)

[Vampire Survivors doesn't launch. flathub/com.valvesoftware.Steam#897](https://github.com/flathub/com.valvesoftware.Steam/issues/897) Open

 [ferroson](https://github.com/ferroson) mentioned this issue [on Jul 5](https://github.com/flatpak/flatpak/issues/3044#ref-issue-1292897609)

[[Bug]: Can't run JASP in Arch-based Linux Distro jasp-stats/jasp-issues#1769](https://github.com/jasp-stats/jasp-issues/issues/1769) Open

3 tasks

 [soredake](https://github.com/soredake) mentioned this issue [on Sep 28](https://github.com/flatpak/flatpak/issues/3044#ref-issue-1210111528) [Install Steam as a Flatpak LukeShortCloud/winesapOS#328](https://github.com/LukeShortCloud/winesapOS/issues/328) Open



### soredake on Sep 28

There is problem with electron games in flatpak, they will not work out of the box, requiring you to set `--no-sandbox` manually, or try to convince maintainer to use zypak/set --no-sandbox when flatpak is detected, but you can't expect maintainer of every game based on electron to do this.

 [dngray](https://github.com/dngray) mentioned this issue [18 days ago](https://github.com/flatpak/flatpak/issues/3044#ref-issue-1419059398)
	[Zypak, and Brave privacyguides/privacyguides.org#1854](https://github.com/privacyguides/privacyguides.org/issues/1854) Open