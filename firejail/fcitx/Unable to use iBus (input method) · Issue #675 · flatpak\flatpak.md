---
title: "Unable to use iBus (input method) · Issue #675 · flatpakflatpak"
layout: post
---

# Unable to use iBus (input method) · Issue #675 · flatpak\flatpak
<a style="text-decoration:underline" href="https://github.com/flatpak/flatpak/issues/675">Unable to use iBus (input method) · Issue #675 · flatpak\flatpak</a><br>

Comments
--------

[![@genggoro](https://avatars.githubusercontent.com/u/7993844?s=80&v=4)](https://github.com/genggoro)

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue

[Aug 30, 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-203a3df)

[![@alexlarsson](https://avatars.githubusercontent.com/u/990736?s=40&v=4)](https://github.com/alexlarsson) [![@fujiwarat](https://avatars.githubusercontent.com/u/105929?s=40&v=4)](https://github.com/fujiwarat)

```
This adds a dbus service called org.freedesktop.portal.IBus on the
session bus. It is a very limited service that only implements
CreateInputContext and the InputContext interface (and Service.Destroy
for lifetime access).

It uses gdbus code generation for demarshalling the method calls which
means it will verify that all arguments have the right type.

Additionally all method calls to the input context object have to be
from the client that created it, so each client is isolated.

BUG=[flatpak/flatpak#675](https://github.com/flatpak/flatpak/issues/675)
R=Shawn.P.Huang@gmail.com

Review URL: [https://codereview.appspot.com/326350043](https://codereview.appspot.com/326350043)

Patch from Alexander Larsson <alexl@redhat.com>.
```

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue

[Aug 31, 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-35ce624)

[![@alexlarsson](https://avatars.githubusercontent.com/u/990736?s=40&v=4)](https://github.com/alexlarsson) [![@fujiwarat](https://avatars.githubusercontent.com/u/105929?s=40&v=4)](https://github.com/fujiwarat)

```
This adds a new way to create an IbusBus, ibus_bus_new_async_client().
This returns an object that is not guarantee to handle any calls
that are not needed by a client, meaning CreateInputContext and
handling the input context.

If you are running in a flatpak, or if IBUS_USE_PORTAL is set, then
instead of talking to the regular ibus bus we connect to
org.freedesktop.portal.IBus on the session bus and use the
limited org.freedesktop.IBus.Portal interface instead of the
org.freedesktop.IBus interface.

This allows flatpaks (or other sandbox systems) to safely use
dbus clients (apps).

BUG=[flatpak/flatpak#675](https://github.com/flatpak/flatpak/issues/675)

Review URL: [https://codereview.appspot.com/328410043](https://codereview.appspot.com/328410043)

Patch from Alexander Larsson <alexl@redhat.com>.
```

[fujiwarat](https://github.com/fujiwarat) added a commit to ibus/ibus that referenced this issue

[Sep 4, 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-9937a0e)

[![@fujiwarat](https://avatars.githubusercontent.com/u/105929?s=40&v=4)](https://github.com/fujiwarat)

```
When ibus-daemon restarts, ibus-portal exits with on_name_lost() and
the clients wait for portal_name_appeared() until ibus-poral restarts.
Now the clients can connect to ibus-daemon with this way and also
they don't have to activate ibus-portal.

BUG=[flatpak/flatpak#675](https://github.com/flatpak/flatpak/issues/675)
R=Shawn.P.Huang@gmail.com

Review URL: [https://codereview.appspot.com/321530043](https://codereview.appspot.com/321530043)
