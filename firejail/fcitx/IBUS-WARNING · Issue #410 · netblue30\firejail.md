---
title: "IBUS-WARNING · Issue #410 · netblue30firejail"
layout: post
---
<style>
img {
  width:32px;!important;
}
</style>

# IBUS-WARNING · Issue #410 · netblue30\firejail

Ref: Ubuntu 15.10 - Unity desktop  
Firejail Versions: 0.9.38 and 0.9.40-rc1

Following today's updates, my keyboard stops working within Firefox, and I receive the following errors when starting Firefox sandboxed:

(firefox:2): IBUS-WARNING \*\*: Unable to connect to ibus: Could not connect: Connection refused

(firefox:2): IBUS-WARNING \*\*: Events queue growing too big, will start to drop.

My 15.10 Lubuntu, UbuntuMATE, and Xubuntu virtual machines appear to be working normally.
