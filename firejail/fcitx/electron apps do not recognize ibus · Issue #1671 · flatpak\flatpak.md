---
title: "electron apps do not recognize ibus · Issue #1671 · flatpakflatpak"
layout: post
---

# electron apps do not recognize ibus · Issue #1671 · flatpak\flatpak
<a style="text-decoration:underline" href="https://github.com/flatpak/flatpak/issues/1671">electron apps do not recognize ibus · Issue #1671 · flatpak\flatpak</a><br>

Linux distribution and version
------------------------------

Ubuntu 18.04

Flatpak version
---------------

0.11.3 to .6

Description of the problem
--------------------------

Electron apps do not recognize ibus

Steps to reproduce
------------------

use ibus in an electron-using flatpak like Signal or Skype. Not recognized. Just uses bog standard US English layout.

others experienced this, too: [flathub/org.signal.Signal#17](https://github.com/flathub/org.signal.Signal/issues/17) but this is due to how electron is used. Rather than fixing every packaging of a flatpak or every implementation of electron, is there some way for flatpak the platform to ease the issue for electron apps?

