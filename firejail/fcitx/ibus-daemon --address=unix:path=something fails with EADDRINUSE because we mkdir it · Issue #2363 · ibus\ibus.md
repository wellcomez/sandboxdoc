---
title: "ibus-daemon --address=unix:path=something fails with EADDRINUSE because we mkdir it · Issue #2363 · ibusibus"
layout: post
---

# ibus-daemon --address=unix:path=something fails with EADDRINUSE because we mkdir it · Issue #2363 · ibus\ibus
<a style="text-decoration:underline" href="https://github.com/ibus/ibus/issues/2363">ibus-daemon --address=unix:path=something fails with EADDRINUSE because we mkdir it · Issue #2363 · ibus\ibus</a><br>

Issue description:  
trying to run ibus-daemon with --address=unix:path=/some/path fails because we create /some/path before trying to connect the socket.  
incidentally dbus-daemon also segfaults when trying to print a log at that point, I didn't check why as I'm in the middle of debugging something else...

Steps to reproduce:  
1.

```
$ ibus-daemon --xim  --replace --address=unix:path=/run/user/1000/bus --verbose

(ibus-daemon:28444): IBUS-ERROR : 21:37:28.530: g_dbus_server_new_sync() is failed with address unix:path=/run/user/1000/bus and guid c6fb8acb152b7e9d29a1831661af5588: Error binding to address (GUnixSocketAddress): Address already in use
Trace/breakpoint trap (core dumped)

```

Reason:  
The problem is in `bus_server_init()` the code has the same logic for unix:tmpdir/dir and path:

```
#define IF_GET_UNIX_DIR(prefix)                                         \\
    if (g_str_has_prefix (socket_address, (prefix))) {                  \\
        unix_dir = g_strdup (socket_address + strlen (prefix));         \\
    }

    IF_GET_UNIX_DIR (IBUS_UNIX_TMPDIR)
    else
    IF_GET_UNIX_DIR (IBUS_UNIX_PATH)
    else
    IF_GET_UNIX_DIR (IBUS_UNIX_ABSTRACT)
    else
    IF_GET_UNIX_DIR (IBUS_UNIX_DIR)
    else {
        g_error ("Your socket address \\"%s\\" does not correspond with "
                 "one of the following formats; "
                 IBUS_UNIX_TMPDIR "DIR, " IBUS_UNIX_PATH "FILE, "
                 IBUS_UNIX_ABSTRACT "FILE, " IBUS_UNIX_DIR "DIR.",
                 socket_address);
    }
    if (!g_file_test (unix_dir, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_DIR)) {
        /* Require mkdir for BSD system.
         * The mode 0700 can eliminate malicious users change the mode.
         * \`chmod\` runs for the last directory only not to change the modes
         * of the parent directories. E.g. "/tmp/ibus".
         */
        errno = 0;
        if (g_mkdir_with_parents (unix_dir, 0700) != 0) {
            g_error ("mkdir is failed in: %s: %s",
                     unix_dir, g_strerror (errno));
        }
    }

```

the IF_GET_UNIX_DIR for IBUS_UNIX_PATH should strip the last component of the path before creating only parent directories.

