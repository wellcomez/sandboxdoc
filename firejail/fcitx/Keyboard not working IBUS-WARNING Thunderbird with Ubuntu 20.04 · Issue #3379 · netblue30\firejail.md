---
title: "Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 · Issue #3379 · netblue30firejail"
layout: post
---
<style>
img {
  width:32px;!important;
}
</style>

# Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 · Issue #3379 · netblue30\firejail

Hey,  
after upgrading my installation to Ubuntu 20.04 I ran into the following issues:

**Describe the bug**  
When running Thunderbird on Ubuntu 20.04 with firejail the keyboard input is broken.

**Behavior change on disabling firejail**  
The keyboard input works as intended

**To Reproduce**  
Steps to reproduce the behavior:

1.  Start Thunderbird: `firejail --private thunderbird`
2.  Try to enter something into the input fields
3.  See error in terminal:

```
(thunderbird:17): IBUS-WARNING : 02:56:35.752: Unable to connect to ibus: Could not connect: Connection refused
(thunderbird:17): IBUS-WARNING : 02:56:39.831: Events queue growing too big, will start to drop.
(thunderbird:17): IBUS-WARNING : 02:56:39.877: Events queue growing too big, will start to drop.
(thunderbird:17): IBUS-WARNING : 02:56:39.905: Events queue growing too big, will start to drop.

```

**Expected behavior**  
Being able to input information with the keyboard.

**Desktop (please complete the following information):**

-   Ubuntu 20.04 LTS (focal)

```
firejail version 0.9.62

Compile time support:
	- AppArmor support is enabled
	- AppImage support is enabled
	- chroot support is enabled
	- file and directory whitelisting support is enabled
	- file transfer support is enabled
	- firetunnel support is enabled
	- networking support is enabled
	- overlayfs support is enabled
	- private-home support is enabled
	- seccomp-bpf support is enabled
	- user namespace support is enabled
	- X11 sandboxing support is enabled

```

**Additional context**  
This issues also appears in gedit.
