---
title: "Fcitx and Firefox · Issue #3732 · netblue30firejail"
layout: post
---

<style>img {width:30px;height:30px}</style>
# Fcitx and Firefox · Issue #3732 · netblue30\firejail

New issue

**Have a question about this project?** Sign up for a free GitHub account to open an issue and contact its maintainers and the community.

By clicking “Sign up for GitHub”, you agree to our [terms of service](https://docs.github.com/terms) and [privacy statement](https://docs.github.com/privacy). We’ll occasionally send you account related emails.

Already on GitHub? [Sign in](https://github.com/login?return_to=%2Fnetblue30%2Ffirejail%2Fissues%2Fnew) to your account

Comments
--------

[![@hugthecactus](https://avatars.githubusercontent.com/u/68545444?s=80&v=4)](https://github.com/hugthecactus)

I was going to ask for help with this issue but I managed to fix it myself so I'm leaving the solution here in the hope that it will be easier for others to find.

In previous versions of firejail, the way to enable Fcitx input method (for typing in Japanese, for example) was to use `ignore nodbus` but this has recently been deprecated in favour of various `dbus-user` and `dbus-system` options. The one that seems to conflict with Fcitx is `dbus-user filter` in `firefox.profile` so what fixed the issue for me was to add the line `ignore dbus-user filter` to `~/.config/firejail/firefox.local` (create if necessary).

Hope that helps someone.

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

Does it work if you add `dbus-user.talk org.fcitx.Fcitx` instead of `ignore dbus-user filter`? This would be mores secure since it only allows a additional name than all session-bus.

[![@hugthecactus](https://avatars.githubusercontent.com/u/68545444?s=80&v=4)](https://github.com/hugthecactus)

Just tested `dbus-user.talk org.fcitx.Fcitx` and unfortunately it's not enough to get it working for me. Happy to test some more configurations though, if you have any other ideas.

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

No, unfortunately I have no further ideas. But you can check `grep -i fcitx /usr/share/dbus-1/services/*` and `busctl --user list | grep -iE "(fcitx|firefox)"`.

[![@hugthecactus](https://avatars.githubusercontent.com/u/68545444?s=80&v=4)](https://github.com/hugthecactus)

Aha! That second command listed a few things, among which is `org.freedesktop.portal.Fcitx`.

Now my `firefox.local` looks like:

`dbus-user.talk org.freedesktop.portal.Fcitx`

and it seems to be working just fine. Thanks very much for pointing me in the right direction!

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

Summary: Depending on the dbus-policy of the profile you need to add different command to its local.

If `dbus-user none`:

```
dbus-user filter
dbus-user.talk org.freedesktop.portal.Fcitx
ignore dbus-user none

```

If `dbus-user filter`:

```
dbus-user.talk org.freedesktop.portal.Fcitx

```

I'll add it to the FAQ.

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

2 participants

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=52&v=4)](https://github.com/rusty-snake) [![@hugthecactus](https://avatars.githubusercontent.com/u/68545444?s=52&v=4)](https://github.com/hugthecactus)