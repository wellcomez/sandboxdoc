---
title: "Unable to use ibus-daemon in firejail · Issue #116 · netblue30firejail"
layout: post
---


<style>img {width:30px;height:30px}</style>
# Unable to use ibus-daemon in firejail · Issue #116 · netblue30\firejail

New issue

**Have a question about this project?** Sign up for a free GitHub account to open an issue and contact its maintainers and the community.

By clicking “Sign up for GitHub”, you agree to our [terms of service](https://docs.github.com/terms) and [privacy statement](https://docs.github.com/privacy). We’ll occasionally send you account related emails.

Already on GitHub? [Sign in](https://github.com/login?return_to=%2Fnetblue30%2Ffirejail%2Fissues%2Fnew) to your account

Closed

[pyamsoft](https://github.com/pyamsoft) opened this issue

Nov 3, 2015

· 49 comments

Comments
--------

[![@pyamsoft](https://avatars.githubusercontent.com/u/7267458?s=80&u=10ea40d69ccb2b003a1b29444da1ccab418902fe&v=4)](https://github.com/pyamsoft)

The ibus-daemon is used to change the languages on the system. For example, on my personal machine running Arch Linux 64bit using firejail 0.9.34-rc1, I use ibus to switch between English (US) input and Japanese inputs.

When not running in firejail, programs such as MousePad and rxvt-unicode for example will respect the current ibus language and type in either Japanese or English.

However, when running an application in firejail, because the ibus-daemon is not also running in the jail, and the jailed program has no way of talking to the rest of the processes running on the machine, programs such as Mousepad and Chromium do not properly switch inputs using the ibus-daemon.

Steps to reproduce:

1.  Install ibus-daemon, and in my case, ibus-anthy for Japanese input and noto-fonts-cjk for East Asian fonts.
2.  Start ibus daemon with `ibus-daemon -drx`
3.  Launch mousepad, switch the language in ibus from English to Japanese.
4.  Upon typing, Japanese characters will be displayed as expected.
5.  Launch a firejail instance of mousepad
6.  Switch language from English to Japanese
7.  Upon typing, English is still output, even though Japanese was requested.

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

I'll look into it, thanks.

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

[![@pyamsoft](https://avatars.githubusercontent.com/u/7267458?s=80&u=10ea40d69ccb2b003a1b29444da1ccab418902fe&v=4)](https://github.com/pyamsoft)

This patch introduces a regression when using network namespaces with a bridge interface. Because of the new network ns, the iBus daemon will reject all forms on input.

This can be observed via the following:

1.  Create a new bridge network device `br0`
2.  Pass the bridge device to firejail using the `--net=br0` option
3.  Launch any graphical program like chromium / mousepad (anything that can talk with ibus)
4.  Attempt to type

Excepted: Output of some kind  
Result: iBus daemon rejects all input from the program, making it effectively impossible to run a network bridged namespace while using the ibus daemon.

A new issue can be opened in regards to this, or the current one can be reopened.

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

I've disabled the previous fix if a network ns is created by the sandbox.

[![@pirate486743186](https://avatars.githubusercontent.com/u/429925?s=80&v=4)](https://github.com/pirate486743186)

beh, it doesn't work at all in Firefox.  
You have to kill ibus to type anything.

"(firefox:1): IBUS-WARNING \*\*: Events queue growing too big, will start to drop."

This seams to be a more general problem, with applications unable to communicate with the outside...

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

Yes, I get the same thing if I have a network namespace configured. Without the net namespace it should work fine. I'll bring in a fix for network namespace.

The apps will not be able to communicate outside when a net namespace is configured. I'll have to proxy that traffic somehow.

[![@ioparaskev](https://avatars.githubusercontent.com/u/8558388?s=80&u=703859448f4a81c1e506ab4cf9a58d5392175007&v=4)](https://github.com/ioparaskev)

Same issue exists in fedora too..  
After that ibus is probably misbehaving since amixer cannot see any devices after this and gives an "Mixer attach default error: Connection refused" error.. This makes firejail unusable in my case unfortunately

Version: Fedora 23

How to reproduce:

1.  `firejail firefox`
2.  open new tab and type url
3.  `amixer` shows error `"Mixer attach default error: Connection refused"`

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

This looks more like a sound problem. Do you have PulseAudio installed, or only ALSA?

[![@ioparaskev](https://avatars.githubusercontent.com/u/8558388?s=80&u=703859448f4a81c1e506ab4cf9a58d5392175007&v=4)](https://github.com/ioparaskev)

I have also pulseaudio and same happens with pulseaudio.. After the reproduce I'm losing control over the sound device.  
`sh -c "pactl set-sink-mute 0 false ; pactl set-sink-volume 1 -100%"`

> shm_open() failed: No such file or directory  
> Connection failure: Protocol error  
> shm_open() failed: No such file or directory  
> Connection failure: Protocol error

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

So, it is a sound problem, it has nothing to do with ibus. PulseAudio has a problem when running in a PID namespace. There is a workaround until they fix the problem. Look at "Known Problems" section here:

[https://firejail.wordpress.com/support/](https://firejail.wordpress.com/support/)

[![@ioparaskev](https://avatars.githubusercontent.com/u/8558388?s=80&u=703859448f4a81c1e506ab4cf9a58d5392175007&v=4)](https://github.com/ioparaskev)

oh I see.. thanks and sorry for hijacking this

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

[![@pirate486743186](https://avatars.githubusercontent.com/u/429925?s=80&v=4)](https://github.com/pirate486743186)

[![@dfaerch](https://avatars.githubusercontent.com/u/7899484?s=80&v=4)](https://github.com/dfaerch)

I wanted to try out firejail (0.9.40 on ubuntu 14.04), but I seem to be having the same issue. I did

but keyboard doesn't respond and i get errors in the console:

```
(google-chrome:2): IBUS-WARNING : Unable to connect to ibus: Could not connect: Connection refused
(google-chrome:2): IBUS-WARNING : Events queue growing too big, will start to drop.
(google-chrome:2): IBUS-WARNING : Events queue growing too big, will start to drop.

```

[![@biergaizi](https://avatars.githubusercontent.com/u/1310693?s=80&u=24ecf6c008764dbb1a64e5313733fde2b33bed32&v=4)](https://github.com/biergaizi)

Still no possible solutions? It basically disables any keyboard inputs on a iBus-based desktop, since English is also managed by iBus, and effectively made the great Firejail useless...

[![@folti](https://avatars.githubusercontent.com/u/1019243?s=80&v=4)](https://github.com/folti)

temporary workaround is to tell Gtk to use a different input module, by setting the GTK_IM_MODULE accordingly. For example:

don't have keyboard, while

```
GTK_IM_MODULE=xim firejail opera

```

does.

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

Thanks for the info, I'll document it on the web site.

[![@anatoli26](https://avatars.githubusercontent.com/u/22968420?s=80&u=6fe8450189d77e775192c8c6ca881f2d43dee37b&v=4)](https://github.com/anatoli26)

Hi, I just wanted to try Firejail on Ubuntu 16.04 stock with the latest Firefox 52.0.1 from xenial channel. The FJ version from the official channel (firejail/xenial 0.9.38.10-0ubuntu0.16.04.1 amd64) had a number of issues (especially with sound and broke it for other apps that were started before the FJ install), then I learned that this version is quite outdated and has a known issue with sound, so I purged it and installed a new one from this repo (0.9.45), but I get the same issue as other users here:

`(firefox:7): IBUS-WARNING **: Unable to connect to ibus: Could not connect: Connection refused`

Then, when trying to type anything, I get:

`(firefox:7): IBUS-WARNING **: Events queue growing too big, will start to drop.`

Is there any solution to this problem?

_UPDATE_: tried more apps (rhythmbox, wire messenger, etc.), they all seem to have the same problem under FJ: IBUS-WARNING and no keyboard.

[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=80&v=4)](https://github.com/netblue30)

I'll give it a try, thanks.

[![@pdo-smith](https://avatars.githubusercontent.com/u/12514185?s=80&v=4)](https://github.com/pdo-smith)

I have the same problem in Ubuntu 17.04  
The fix given by folti (GTK_IM_MODULE=xim firejail opera) works.

[![@cwmke](https://avatars.githubusercontent.com/u/1923967?s=80&v=4)](https://github.com/cwmke)

I've been using Fedora lately without this issue happening. Tonight I installed the nvidia drivers and the problem started again. I don't have the issue with intel video drivers. It only seems to happen when I'm using nvidia.

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

Is this still an issue and do the workarounds above not work?

[![@dfaerch](https://avatars.githubusercontent.com/u/7899484?s=80&v=4)](https://github.com/dfaerch)

[![@graywolf](https://avatars.githubusercontent.com/u/790463?s=80&v=4)](https://github.com/graywolf)

I still have the issue, `GTK_IM_MODULE=ibus firefox` does not work. I don't think this should be closed.

[![@graywolf](https://avatars.githubusercontent.com/u/790463?s=80&v=4)](https://github.com/graywolf)

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

[@graywolf](https://github.com/graywolf) The way I look at it is: If it works, don't worry about it 😂 Seriously, though...unless there are security implications, this is probably okay. If it stops working, we'll figure something out then 😂

[![@graywolf](https://avatars.githubusercontent.com/u/790463?s=80&v=4)](https://github.com/graywolf)

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

So if the workarounds here _are_ working (ibus developers' opinions notwithstanding), I'm going to go ahead and close the issue.

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

On second thought, re-opening, since this is still an issue that should be debugged.

[![@xelxebar](https://avatars.githubusercontent.com/u/787430?s=80&v=4)](https://github.com/xelxebar)

For whatever it's worth the proposed `*_IM_MODULE` workaround doesn't fix the issue for me, though I'm running _qutebrowser_ instead of _firefox_.

```
$ firejail --version
firejail version 0.9.56

Compile time support:
        - AppArmor support is enabled
        - AppImage support is enabled
        - chroot support is enabled
        - file and directory whitelisting support is enabled
        - file transfer support is enabled
        - networking support is enabled
        - overlayfs support is enabled
        - private-home support is enabled
        - seccomp-bpf support is enabled
        - user namespace support is enabled
        - X11 sandboxing support is enabled

$ ibus version
1.5.1.19

$ qutebrowser --version
qutebrowser v1.5.2
Git commit:
Backend: QtWebEngine (Chromium 61.0.3163.140)

CPython: 3.6.7
Qt: 5.10.1
PyQt: 5.10.1

sip: 4.19.8
colorama: no
pypeg2: 2.15
jinja2: 2.10
pygments: 2.2.0
yaml: 3.13
cssutils: no
attr: 18.2.0
PyQt5.QtWebEngineWidgets: yes
PyQt5.QtWebKitWidgets: no
pdf.js: no
sqlite: 3.25.2
QtNetwork SSL: LibreSSL 2.7.4
...

```

[![@mhva](https://avatars.githubusercontent.com/u/12730677?s=80&v=4)](https://github.com/mhva)

Disclaimer: I don't use firejail, but I'm almost positive that this issue is related to the fact that ibus-daemon uses an abstract unix socket for IPC by default. This socket is invisible to firejail sandbox if it resides in a separate network namespace.

It should be possible to circumvent this issue in the current master branch of ibus by requesting it to use named socket for IPC. This can be done by passing `--address=unix:path=<somedir>/<somefile>` to ibus-daemon and making sure that `<somedir>` and `$HOME/.config/ibus` are both visible in sandbox. The latter dir contains ibus-daemon socket address, which is needed for ibus clients to be able to find where they should connect.

[![@biergaizi](https://avatars.githubusercontent.com/u/1310693?s=80&u=24ecf6c008764dbb1a64e5313733fde2b33bed32&v=4)](https://github.com/biergaizi)

Awesome! Having the capability to use an alternative IPC socket should definitely solve the problem.

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

Given the workaround [@mhva](https://github.com/mhva) suggested, do people still have this issue?

[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=80&u=e93652b62cbbcfca9a3045ceda7e5fb783aa6f83&v=4)](https://github.com/chiraag-nataraj)

I'm going to go ahead and close this for now. If anyone affected is still running into this, please feel free to re-open!

[![@SailReal](https://avatars.githubusercontent.com/u/1786772?s=80&u=c54929d3bd941d2f050a88618556745b2f4a1ab1&v=4)](https://github.com/SailReal)

I just started using firejail, what an awesome piece of software, thank you all :)

Nearly everything works out of the box but I just stumbled over this issue using Ubuntu 20.04 and the Signal-Desktop client. On my device it can only be started with `GTK_IM_MODULE=xim`, otherwise you can't enter anything using my keyboard.

[![@glitsj16](https://avatars.githubusercontent.com/u/959378?s=80&v=4)](https://github.com/glitsj16)

[@SailReal](https://github.com/SailReal) Glad to hear you're enjoying firejail. As for the environment variable workaround, you can put that into a `signal-desktop.local` file. If you don't have one yet, you will have to create one manually. Either use /etc/firejail/signal-desktop.local (affects all users on your machine) or ${HOME}/.config/firejail/signal-desktop.local (affects your user only). Add the below to that file:

[![@overchu](https://avatars.githubusercontent.com/u/76196884?s=80&v=4)](https://github.com/overchu)

> Disclaimer: I don't use firejail, but I'm almost positive that this issue is related to the fact that ibus-daemon uses an abstract unix socket for IPC by default. This socket is invisible to firejail sandbox if it resides in a separate network namespace.
> 
> It should be possible to circumvent this issue in the current master branch of ibus by requesting it to use named socket for IPC. This can be done by passing `--address=unix:path=<somedir>/<somefile>` to ibus-daemon and making sure that `<somedir>` and `$HOME/.config/ibus` are both visible in sandbox. The latter dir contains ibus-daemon socket address, which is needed for ibus clients to be able to find where they should connect.

Where can you pass these argruments to ibus? I couldn't figure out who is starting ibus in the first place.

1.  `systemd` seems not to start it
2.  neither `systemd --user`
3.  it's also not started by gnome3

But anyway, if I stop ibus-daemon and start it manually with the given options, than I got an error that the address is already in use.

```
$ ps aux | grep ibus-
alex       33084  2.2  0.0 815460 13488 pts/4    Sl   13:44   0:00 ibus-daemon --replace --panel disable --xim
alex       33105  0.0  0.0 449324  7396 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-dconf
alex       33106  4.0  0.1 483180 28636 pts/4    Sl   13:44   0:01 /usr/libexec/ibus-extension-gtk3
alex       33108  0.1  0.1 404168 22388 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-x11 --kill-daemon
alex       33116  0.0  0.0 449092  7292 ?        Ssl  13:44   0:00 /usr/libexec/ibus-portal
alex       33128  0.5  0.0 375336  7148 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-engine-simple
alex       33207  0.0  0.0 221588   852 pts/4    S+   13:45   0:00 grep --color=auto ibus-
```

```
$ kill 33084 # kill ibus-daemon
```

```
$ ibus-daemon --replace --panel disable --xim  --address=unix:path=/home/alex/.cache/alex-ibus/ibus-address/test --verbose
(ibus-daemon:35008): IBUS-ERROR : 13:57:37.755: g_dbus_server_new_sync() is failed with address unix:path=/home/alex/.cache/alex-ibus/ibus-address/test and guid 30d04cbb95ff4a29d0b02d835fdc44d1: Error binding to address (GUnixSocketAddress): Address already in use
```

So it seems that even if I find out where ibus is started in will not work anyway.

I am running Fedora 33 with Gnome 3, X, and ibus 1.5.23

[![@glitsj16](https://avatars.githubusercontent.com/u/959378?s=80&v=4)](https://github.com/glitsj16)

> Where can you pass these argruments to ibus? I couldn't figure out who is starting ibus in the first place.

On Arch Linux the ibus package installs `/usr/share/dbus-1/services/org.freedesktop.IBus.service`:

```
$ cat /usr/share/dbus-1/services/org.freedesktop.IBus.service
[D-BUS Service]
Name=org.freedesktop.IBus
Exec=/usr/bin/ibus-daemon --replace --panel disable --xim

```

If memory serves you can place an override in /etc/dbus-1/services/org.freedesktop.IBus.service with your desired Exec= command.

Note: As of Jul 2020, Firejail introduced DBus filtering. It is now possible to use the --dbus-user.own=org.freedesktop.IBus parameter to let Firefox access IBus.

[![@overchu](https://avatars.githubusercontent.com/u/76196884?s=80&v=4)](https://github.com/overchu)

> On Arch Linux the ibus package installs /usr/share/dbus-1/services/org.freedesktop.IBus.service:

Thank you for the idea of just checking what hte package installs. You are right.

```
$ rpm -ql ibus | grep -e '\\.service'                                                                         │
/usr/share/dbus-1/services/org.freedesktop.IBus.service                                                                │
/usr/share/dbus-1/services/org.freedesktop.portal.IBus.service 
```

But I don't see this `.service`\-file linked anywhere, so I don't think this is what actually gets used:

```
$ sudo find -L /etc/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
$ find -L /home/$USER/.config/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
```

Both yield empty results.

And no mentioning of ibus in /etc/dbus-1/ either, no results for:

```
$ tree /etc/dbus-1/ | grep Ibus
```

[![@overchu](https://avatars.githubusercontent.com/u/76196884?s=80&v=4)](https://github.com/overchu)

> Note: As of Jul 2020, Firejail introduced DBus filtering. It is now possible to use the --dbus-user.own=org.freedesktop.IBus parameter to let Firefox access IBus.

Sorry, I forgot to mention that I tried this solution unsuccessfully:

```
$ firejail --dbus-user.own=org.freedesktop.IBus firefox
Reading profile /etc/firejail/firefox.profile
Reading profile /etc/firejail/whitelist-usr-share-common.inc
Reading profile /etc/firejail/firefox-common.profile
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-devel.inc
Reading profile /etc/firejail/disable-exec.inc
Reading profile /etc/firejail/disable-interpreters.inc
Reading profile /etc/firejail/disable-programs.inc
Reading profile /etc/firejail/whitelist-common.inc
Reading profile /etc/firejail/whitelist-var-common.inc
Warning: Warning: NVIDIA card detected, nogroups command disabled
Parent pid 105219, child pid 105223
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Warning: An abstract unix socket for session D-BUS might still be available. Use --net or remove unix from --protocol set.
Warning: cleaning all supplementary groups
Warning: not remounting /run/user/1000/gvfs
Warning: cleaning all supplementary groups
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Warning: cleaning all supplementary groups
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Child process initialized in 216.69 ms

Parent is shutting down, bye...
```

I even consider a security bug because it spits out a working firefox (with ibus) that is _not_ sandboxed which is not what you expect if you run `firejail`.  
Above, this might be the most interesting line:

```
Warning: An abstract unix socket for session D-BUS might still be available. Use --net or remove unix from --protocol set.
```

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

> But I don't see this .service-file linked anywhere, so I don't think this is what actually gets used:
> 
> ```
> $ sudo find -L /etc/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
> $ find -L /home/$USER/.config/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
> 
> ```
> 
> Both yield empty results.
> 
> And no mentioning of ibus in /etc/dbus-1/ either, no results for:
> 
> ```
> $ tree /etc/dbus-1/ | grep Ibus
> 
> ```

It's **not** a systemd unit. It's a D-Bus service (look at the `[D-BUS Service]` line in it).

D-Bus configuration is located at `/usr/share/dbus-1` (distributions defaults) and `/etc/dbus-1` (user/admin overrides).

> \--dbus-user.own=org.freedesktop.IBus

`--dbus-user.talk=org.freedesktop.IBus` should be enough.

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

FWIW: Fcitx works we allowing the portal, see [#3732](https://github.com/netblue30/firejail/issues/3732).

[![@graywolf](https://avatars.githubusercontent.com/u/790463?s=80&v=4)](https://github.com/graywolf)

The `xim` workaround stopped working for me on firefox 93.0. Should this be re-opened?

[![@martinetd](https://avatars.githubusercontent.com/u/1729331?s=80&v=4)](https://github.com/martinetd)

I don't know about the xim workaround not working (I've never had input stuck because of ibus in default configuration), but the 'solution' that had been accepted years ago is nothing but a workaround that disables ibus, it's not a way of using ibus.

I've spent quite a bit of time on this, here's my take:

-   by default, ibus will try to open `$HOME/.config/ibus/bus/<machineid>-<hostname>-<display>` (`ibus_get_socket_path()`) to guess the ibus socket. While adding `whitelist ${HOME}/.config/ibus` to the profile lets ibus load this file, ibus tries to check if the socket is valid by sending a signal (kill(0) to the pid defined in that file, and that doesn't work with our PID namespace, so giving access to that file is useless unless we could share the pid namespace, which is not possible [Ability to turn off pid namespacing? #892](https://github.com/netblue30/firejail/issues/892)
-   instead, one can set `IBUS_ADDRESS` directly in the environment (preload that file before starting firejail), which will skip the PID check.
    -   The default IBUS_ADDRESS uses an abstract path, so unless firejail creates a different network namespace firefox will be able to connect to it
    -   For cases with a separate namespace, it's possible to start ibus-daemon with `--address=unix:dir=/path/to/somedir` and whitelist that directory to allow firefox to connect to it. Note ibus allows unix:path=/path/to/sock, but that doesn't work right now, see [ibus-daemon --address=unix:path=something fails with EADDRINUSE because we mkdir it ibus/ibus#2363](https://github.com/ibus/ibus/issues/2363)
    -   Note that if IBUS_ADDRESS is set to something invalid, input will not work (like what others describe here, ibus-warning "Events queue growing too big, will start to drop.")
-   Allowing `dbus-user.talk org.freedesktop.IBus` is also needed as suggested. Ther'e's also a `org.freedesktop.portal.IBus` but I didn't find this to be required.

Ultimately my conf is as follow (using unix socket in ~/.cache/ibus)

```
$ cat ~/.config/firejail/firefox.local
dbus-user.talk org.freedesktop.IBus
whitelist ${HOME}/.cache/ibus
$ grep ibus ~/.xsession
ibus-daemon --xim -d --address=unix:dir=$HOME/.cache/ibus/dbus
$ cat ~/.bin/ff
#!/bin/sh

. "$HOME/.config/ibus/bus/$(cat /var/lib/dbus/machine-id)-unix-${WAYLAND_DISPLAY}"

export IBUS_ADDRESS

exec systemd-run --user --scope --unit=ff-$$.scope \
	-p MemoryMax=3G -p MemoryHigh=2G \
	firejail firefox "$@"

```

[![@rusty-snake](https://avatars.githubusercontent.com/u/41237666?s=80&u=25be73331809504fc2a75a418a613cdfcb506071&v=4)](https://github.com/rusty-snake)

```
env GTK_IM_MODULE=ibus
env IBUS_USE_PORTAL=1
dbus-user.talk org.freedesktop.portal.IBus

```

seems to work too

[![@scottslowe](https://avatars.githubusercontent.com/u/1977326?s=80&u=210c7ec1f0dcc0597fe241e83bdb6ce95d6eb3a2&v=4)](https://github.com/scottslowe)

Confirming that [@rusty-snake](https://github.com/rusty-snake)'s workaround from Jan 8 2022 works for me on a newly-upgraded Fedora 36 system (to fix keyboard input for Thunderbird).

[![@martinetd](https://avatars.githubusercontent.com/u/1729331?s=80&v=4)](https://github.com/martinetd)

Follow up half a year later: turns out my system just doesn't start ibus-portal, running it manually and asking gtk to use it seems to be easier to handle than my old workaround so switched to that as well.

Thanks [@rusty-snake](https://github.com/rusty-snake) !