---
title: "Ability to turn off pid namespacing? · Issue #892 · netblue30firejail"
layout: post
---
style>
img {
  width:32px;!important;
}
</style>

# Ability to turn off pid namespacing? · Issue #892 · netblue30\firejail

Related to [#862](https://github.com/netblue30/firejail/issues/862) and my quest to manipulate cgroup assignment for subprocesses of a firejailed process, accessing the cgroup vfs is one thing, but in order to add a subprocess to a cgroup, one needs to know its true pid. But firejail namespaces the pids, which makes knowing the real pid impossible.

I couldn't find a way to turn off pid namespacing, did I miss something? Out of curiosity, is it a deliberate feature or a side-effect of some system folder/file being hidden by firejail?

Anyway, if you have any idea on how pid namespacing could be turned off, it would be greatly appreciated.
