---
title: "Keyboard no longer working in gedit · Issue #5100 · netblue30firejail"
layout: post
---

# Keyboard no longer working in gedit · Issue #5100 · netblue30\firejail
<a style="text-decoration:underline" href="https://github.com/netblue30/firejail/issues/5100">Keyboard no longer working in gedit · Issue #5100 · netblue30\firejail</a><br>

With `GTK_IM_MODULE=xim` the input works, if set to `ibus` it does not.

```
LANG=C GTK_IM_MODULE=ibus gedit
Reading profile /etc/firejail/gedit.profile
Reading profile /etc/firejail/allow-common-devel.inc
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-exec.inc
Reading profile /etc/firejail/disable-programs.inc
Reading profile /etc/firejail/whitelist-runuser-common.inc
Reading profile /etc/firejail/whitelist-var-common.inc
Parent pid 73627, child pid 73628
Warning: /sbin directory link was not blacklisted
Warning: /usr/sbin directory link was not blacklisted
Warning: not remounting /home/jose/.ssh/authorized_keys2
Warning: not remounting /home/jose/.ssh/config
Blacklist violations are logged to syslog
Warning: cleaning all supplementary groups
Child process initialized in 92.12 ms

(gedit:3): dbind-WARNING : 12:20:24.984: Couldn't connect to accessibility bus: Failed to connect to socket /run/user/1000/at-spi/bus_0: No such file or directory

(gedit:3): IBUS-WARNING : 12:20:25.071: Unable to connect to ibus: Could not connect: Connection refused

Parent is shutting down, bye...

