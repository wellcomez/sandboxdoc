

[Capabilities in 2.6](https://lwn.net/Articles/79185/) (April 6, 2004)

[Magic groups in 2.6](https://lwn.net/Articles/84566/) (May 11, 2004)

[Trustees Linux](https://lwn.net/Articles/111247/) (November 16, 2004)

[A bid to resurrect Linux capabilities](https://lwn.net/Articles/199004/) (September 10, 2006)

[File-based capabilities](https://lwn.net/Articles/211883/) (November 29, 2006)

[Fixing CAP_SETPCAP](https://lwn.net/Articles/256519/) (October 31, 2007)

[Restricting root with per-process securebits](https://lwn.net/Articles/280279/) (April 30, 2008)

[Capabilities for loading network modules](https://lwn.net/Articles/430462/) (March 2, 2011)

[CAP_SYS_ADMIN: the new root](https://lwn.net/Articles/486306/) (March 14, 2012)

[The trouble with CAP_SYS_RAWIO](https://lwn.net/Articles/542327/) (March 13, 2013)

[BSD-style securelevel comes to Linux — again](https://lwn.net/Articles/566169/) (September 11, 2013)

[Inheriting capabilities](https://lwn.net/Articles/632520/) (February 11, 2015)

[The kdbuswreck](https://lwn.net/Articles/641275/) (April 22, 2015)

[Tracking resources and capabilities used](https://lwn.net/Articles/694209/) (July 13, 2016)

[Namespaced file capabilities](https://lwn.net/Articles/726816/) (June 30, 2017)

[CAP_PERFMON — and new capabilities in general](https://lwn.net/Articles/812719/) (February 21, 2020)

[A crop of new capabilities](https://lwn.net/Articles/822362/) (June 8, 2020)