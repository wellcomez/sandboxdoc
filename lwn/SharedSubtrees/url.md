[Shared subtrees](https://lwn.net/Kernel/Index/#Shared_subtrees)

[Shared subtrees](https://lwn.net/Articles/159077/) (November 8, 2005)

[Mount namespaces and shared subtrees](https://lwn.net/Articles/689856/) (June 8, 2016)

[Mount namespaces, mount propagation, and unbindable mounts](https://lwn.net/Articles/690679/) (June 15, 2016)