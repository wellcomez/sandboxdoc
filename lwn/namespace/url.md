- Namespaces

  - [A system call for unsharing](https://lwn.net/Articles/135321/) (May 10, 2005)

  - [Sysfs and namespaces](https://lwn.net/Articles/295587/) (August 26, 2008)

  - [Divorcing namespaces from processes](https://lwn.net/Articles/377109/) (March 3, 2010)

  - [Namespace file descriptors](https://lwn.net/Articles/407495/) (September 29, 2010)

  - [Namespaces in operation, part 1: namespaces overview](https://lwn.net/Articles/531114/) (January 4, 2013)

  - [Namespaces in operation, part 2: the namespaces API](https://lwn.net/Articles/531381/) (January 8, 2013)

  - [Namespaces in operation, part 7: Network namespaces](https://lwn.net/Articles/580893/) (January 22, 2014)

  - [Control group namespaces](https://lwn.net/Articles/621006/) (November 19, 2014)

  - [Audit, namespaces, and containers](https://lwn.net/Articles/699819/) (September 8, 2016)

  - [Time namespaces](https://lwn.net/Articles/766089/) (September 21, 2018)

  - [A filesystem for namespaces](https://lwn.net/Articles/877308/) (December 3, 2021)

- Device namespaces

  - [Device namespaces](https://lwn.net/Articles/564854/) (August 28, 2013)

- Mount namespaces

  - [FUSE and private namespaces](https://lwn.net/Articles/133848/) (April 27, 2005)

  - [Shared subtrees](https://lwn.net/Articles/159077/) (November 8, 2005)

  - [Mount namespaces and shared subtrees](https://lwn.net/Articles/689856/) (June 8, 2016)

  - [Mount namespaces, mount propagation, and unbindable mounts](https://lwn.net/Articles/690679/) (June 15, 2016)

- PID namespaces

  - [Namespaces in operation, part 3: PID namespaces](https://lwn.net/Articles/531419/) (January 16, 2013)

  - [Namespaces in operation, part 4: more on PID namespaces](https://lwn.net/Articles/532748/) (January 23, 2013)

- Syslog namespaces

  - [Stepping closer to practical containers: "syslog" namespaces](https://lwn.net/Articles/527342/) (December 5, 2012)

- User namespaces

  - [A new approach to user namespaces](https://lwn.net/Articles/491310/) (April 10, 2012)

  - [User namespaces progress](https://lwn.net/Articles/528078/) (December 13, 2012)

  - [Namespaces in operation, part 5: User namespaces](https://lwn.net/Articles/532593/) (February 27, 2013)

  - [Namespaces in operation, part 6: more on user namespaces](https://lwn.net/Articles/540087/) (March 6, 2013)

  - [Anatomy of a user namespaces vulnerability](https://lwn.net/Articles/543273/) (March 20, 2013)

  - [Mount point removal and renaming](https://lwn.net/Articles/570338/) (October 16, 2013)

  - [The trouble with dropping groups](https://lwn.net/Articles/621612/) (November 19, 2014)

  - [User namespaces and setgroups()](https://lwn.net/Articles/626665/) (December 17, 2014)

  - [Filesystem mounts in user namespaces](https://lwn.net/Articles/652468/) (July 29, 2015)

  - [Controlling access to user namespaces](https://lwn.net/Articles/673597/) (January 27, 2016)

  - [Filesystem mounts in user namespaces — one year later](https://lwn.net/Articles/697278/) (August 17, 2016)

  - [Namespaced file capabilities](https://lwn.net/Articles/726816/) (June 30, 2017)

  - [Unprivileged filesystem mounts, 2018 edition](https://lwn.net/Articles/755593/) (May 30, 2018)

  - [Filesystem UID mapping for user namespaces: yet another shiftfs](https://lwn.net/Articles/812504/) (February 17, 2020)

  - [ID mapping for mounted filesystems](https://lwn.net/Articles/837566/) (November 19, 2020)

  - [Resource limits in user namespaces](https://lwn.net/Articles/842842/) (January 18, 2021)

  - [Sticky groups in the shadows](https://lwn.net/Articles/855943/) (May 14, 2021)

  - [ID-mapped mounts](https://lwn.net/Articles/896255/) (May 30, 2022)

  - [A security-module hook for user-namespace creation](https://lwn.net/Articles/903580/) (August 4, 2022)

- UTS namespaces
  - [Namespaces in operation, part 2: the namespaces API](https://lwn.net/Articles/531381/) (January 8, 2013)