- [Containers and PID virtualization](https://lwn.net/Articles/168093/) (January 17, 2006)

- [PID virtualization: a wealth of choices](https://lwn.net/Articles/171017/) (February 8, 2006)

- [Containers and lightweight virtualization](https://lwn.net/Articles/179361/) (April 10, 2006)

- [Kernel Summit 2006: Paravirtualization and containers](https://lwn.net/Articles/191923/) (July 19, 2006)

- [Another container implementation](https://lwn.net/Articles/200073/) (September 19, 2006)

- [Process containers](https://lwn.net/Articles/236038/) (May 29, 2007)

- [Controlling memory use in containers](https://lwn.net/Articles/243795/) (July 31, 2007)

- [KS2007: Containers](https://lwn.net/Articles/249080/) (September 10, 2007)

- [Process IDs in a multi-namespace world](https://lwn.net/Articles/257297/) (November 6, 2007)

- [System call updates: indirect(), timerfd(), and hijack()](https://lwn.net/Articles/260172/) (November 28, 2007)

- [Kernel-based checkpoint and restart](https://lwn.net/Articles/293575/) (August 11, 2008)

- [Checkpoint/restart tries to head towards the mainline](https://lwn.net/Articles/320508/) (February 25, 2009)

- [Which I/O controller is the fairest of them all?](https://lwn.net/Articles/332839/) (May 12, 2009)

- [clone_with_pids()](https://lwn.net/Articles/346584/) (August 12, 2009)

- [A Checkpoint/restart update](https://lwn.net/Articles/375855/) (February 24, 2010)

- [Divorcing namespaces from processes](https://lwn.net/Articles/377109/) (March 3, 2010)

- [Namespace file descriptors](https://lwn.net/Articles/407495/) (September 29, 2010)

- [Mob rule for dentries](https://lwn.net/Articles/441438/) (May 4, 2011)

- [Checkpoint/restart (mostly) in user space](https://lwn.net/Articles/452184/) (July 19, 2011)

- [Running distributions in containers](https://lwn.net/Articles/462669/) (October 12, 2011)

- [A new approach to user namespaces](https://lwn.net/Articles/491310/) (April 10, 2012)

- [TCP connection repair](https://lwn.net/Articles/495304/) (May 1, 2012)

- [LCE: The failure of operating systems and how we can fix it](https://lwn.net/Articles/524952/) (November 14, 2012)

- [Namespaces in operation, part 1: namespaces overview](https://lwn.net/Articles/531114/) (January 4, 2013)

- [SO_PEERCGROUP: which container is calling?](https://lwn.net/Articles/590928/) (March 18, 2014)

- [Architecture emulation containers with binfmt_misc](https://lwn.net/Articles/679308/) (March 9, 2016)

- [Virtual machines as containers](https://lwn.net/Articles/684614/) (April 23, 2016)

- [Quickly: Filesystems and containers / Self-encrypting drives](https://lwn.net/Articles/684832/) (April 27, 2016)

- [Containers, pseudo TTYs, and backward compatibility](https://lwn.net/Articles/688809/) (June 1, 2016)

- [Container-aware filesystems](https://lwn.net/Articles/718639/) (April 3, 2017)

- [Containers as kernel objects](https://lwn.net/Articles/723561/) (May 23, 2017)

- [Process tagging with ptags](https://lwn.net/Articles/741261/) (December 13, 2017)

- [An audit container ID proposal](https://lwn.net/Articles/750313/) (March 29, 2018)

- [Containers as kernel objects — again](https://lwn.net/Articles/780364/) (February 22, 2019)

- [A filesystem for namespaces](https://lwn.net/Articles/877308/) (December 3, 2021)

- [System call interception for unprivileged containers](https://lwn.net/Articles/899281/) (June 29, 2022)