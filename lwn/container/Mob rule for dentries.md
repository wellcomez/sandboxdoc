---
title: "Mob rule for dentries"
layout: post
---

# Mob rule for dentries
<a style="text-decoration:underline" href="https://lwn.net/Articles/441438/">Mob rule for dentries</a><br>

>Did you know...?
LWN.net is a subscriber-supported publication; we rely on subscribers to keep the entire operation going. Please help out by [buying a subscription](https://lwn.net/subscribe/) and keeping LWN on the net.

As was discussed at the [2011 filesystem, storage, and memory management summit](https://lwn.net/Articles/436871/), there is an increasing level of interest in restricting the amount of kernel memory which can be used by groups of processes. One area of special interest is the directory entry (dentry) cache; a malicious program can, by creating a deep enough directory hierarchy, run the kernel out of memory with an explosion of the size of the dentry cache. So limiting dentry use has some real appeal, especially for those working to ensure that containers running on a Linux system cannot interfere with each other.

Pavel Emelyanov's [per-container dcache management patches](https://lwn.net/Articles/441164/) are a first attempt at limiting dentry use. This patch works by organizing dentries into "mobs," being groups of dentries all of which represent names in a specific subtree of the filesystem. If the root of a mob were the root of a container's filesystem namespace, all dentries created by that container would be contained within that mob. At that point, a simple sort of resource control can be applied: adding a dentry to a mob which has hit its maximum size would require the removal of another dentry to compensate. If no dentries an be removed, attempts to add others will fail.

The patch set adds three new ioctl() calls: FIMOBROOT to create a new mob at a given point in the filesystem, FIMOBSIZE to set the maximum size of a mob, and FIMOBSTAT to query the current usage of a mob. Pavel is somewhat apologetic about this interface; he seems to think it will have to change before the work could be considered upstream. But the first step is get some discussion of the concept; so far, there have been no responses to Pavel's patches.  

| Index entries for this article |
| --- |
| [Kernel](https://lwn.net/Kernel/Index) | [Containers](https://lwn.net/Kernel/Index#Containers) |
| [Kernel](https://lwn.net/Kernel/Index) | [Dentry cache](https://lwn.net/Kernel/Index#Dentry_cache) |

  

* * *

