---
title: "containers"
layout: post
---
[containers](https://lwn.net/Kernel/Index/#Containers)

# containers

[Containers and PID virtualization](/lwn/container/Containers%20and%20PID%20virtualization.md) (January 17, 2006)

[PID virtualization: a wealth of choices](/lwn/container/PID%20virtualization%3A%20a%20wealth%20of%20choices.md) (February 8, 2006)

[Containers and lightweight virtualization](/lwn/container/Containers%20and%20lightweight%20virtualization.md) (April 10, 2006)

[Kernel Summit 2006: Paravirtualization and containers](Kernel%20Summit%20%20Paravirtualization%20and%20containers.md) (July 19, 2006)

[Another container implementation](/lwn/container/Another%20container%20implementation.md) (September 19, 2006)

[Process containers](/lwn/container/Process%20containers.md) (May 29, 2007)

[Controlling memory use in containers](/lwn/container/Controlling%20memory%20use%20in%20containers.md) (July 31, 2007)

[KS2007: Containers](KS%20Containers.md) (September 10, 2007)

[Process IDs in a multi-namespace world](/lwn/container/Process%20IDs%20in%20a%20multi-namespace%20world.md) (November 6, 2007)

[System call updates: indirect(), timerfd(), and hijack()](/lwn/container/System%20call%20updates%3A%20indirect()%2C%20timerfd()%2C%20and%20hijack().md) (November 28, 2007)

[Kernel-based checkpoint and restart](/lwn/container/Kernel-based%20checkpoint%20and%20restart.md) (August 11, 2008)

[Checkpoint/restart tries to head towards the mainline](/lwn/container/Checkpoint%5Crestart%20tries%20to%20head%20towards%20the%20mainline.md) (February 25, 2009)

[Which I/O controller is the fairest of them all?](/lwn/container/Which%20I%5CO%20controller%20is%20the%20fairest%20of%20them%20all?.md) (May 12, 2009)

[clone_with_pids()](/lwn/container/clone_with_pids().md) (August 12, 2009)

[A Checkpoint/restart update](/lwn/container/A%20Checkpoint%5Crestart%20update.md) (February 24, 2010)

[Divorcing namespaces from processes](/lwn/container/Divorcing%20namespaces%20from%20processes.md) (March 3, 2010)

[Namespace file descriptors](/lwn/container/Namespace%20file%20descriptors.md) (September 29, 2010)

[Mob rule for dentries](/lwn/container/Mob%20rule%20for%20dentries.md) (May 4, 2011)

[Checkpoint/restart (mostly) in user space](/lwn/container/Checkpoint%5Crestart%20(mostly)%20in%20user%20space.md) (July 19, 2011)

[Running distributions in containers](/lwn/container/Running%20distributions%20in%20containers.md) (October 12, 2011)

[A new approach to user namespaces](/lwn/container/A%20new%20approach%20to%20user%20namespaces.md) (April 10, 2012)

[TCP connection repair](/lwn/container/TCP%20connection%20repair.md) (May 1, 2012)

[LCE: The failure of operating systems and how we can fix it](/lwn/container/LCE%3A%20The%20failure%20of%20operating%20systems%20and%20how%20we%20can%20fix%20it.md) (November 14, 2012)

[Namespaces in operation, part 1: namespaces overview](/lwn/container/Namespaces%20in%20operation%2C%20part%201%3A%20namespaces%20overview.md) (January 4, 2013)

[SO_PEERCGROUP: which container is calling?](/lwn/container/SO_PEERCGROUP%3A%20which%20container%20is%20calling?.md) (March 18, 2014)

[Architecture emulation containers with binfmt_misc](/lwn/container/Architecture%20emulation%20containers%20with%20binfmt_misc.md) (March 9, 2016)

[Virtual machines as containers](/lwn/container/Virtual%20machines%20as%20containers.md) (April 23, 2016)

[Quickly: Filesystems and containers / Self-encrypting drives](/lwn/container/Quickly%3A%20Filesystems%20and%20containers%20%5C%20Self-encrypting%20drives.md) (April 27, 2016)

[Containers, pseudo TTYs, and backward compatibility](/lwn/container/Containers%2C%20pseudo%20TTYs%2C%20and%20backward%20compatibility.md) (June 1, 2016)

[Container-aware filesystems](/lwn/container/Container-aware%20filesystems.md) (April 3, 2017)

[Containers as kernel objects](/lwn/container/Containers%20as%20kernel%20objects.md) (May 23, 2017)

[Process tagging with ptags](/lwn/container/Process%20tagging%20with%20ptags.md) (December 13, 2017)

[An audit container ID proposal](/lwn/container/An%20audit%20container%20ID%20proposal.md) (March 29, 2018)

[Containers as kernel objects — again](/lwn/container/Containers%20as%20kernel%20objects%20—%20again.md) (February 22, 2019)

[A filesystem for namespaces](/lwn/container/A%20filesystem%20for%20namespaces.md) (December 3, 2021)

[System call interception for unprivileged containers](/lwn/container/System%20call%20interception%20for%20unprivileged%20containers.md) (June 29, 2022)