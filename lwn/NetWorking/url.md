[Networking](https://lwn.net/Kernel/Index/#Networking)

[The end of init_etherdev() and friends](https://lwn.net/Articles/73897/) (March 2, 2004)

[Netpoll is merged](https://lwn.net/Articles/75944/) (March 16, 2004)

[TCP vulnerability: cancel red alert](https://lwn.net/Articles/81560/) (April 21, 2004)

[On the alignment of IP packets](https://lwn.net/Articles/89597/) (June 15, 2004)

[Configuration of pluggable network adaptors](https://lwn.net/Articles/103203/) (September 22, 2004)

[alloc_skb_from_cache()](https://lwn.net/Articles/117736/) (January 4, 2005)

[Network block devices and OOM safety](https://lwn.net/Articles/129703/) (March 30, 2005)

[Toward more robust network-based block I/O](https://lwn.net/Articles/146689/) (August 8, 2005)

[Linux and TCP offload engines](https://lwn.net/Articles/148697/) (August 22, 2005)

[On the safety of Linux random numbers](https://lwn.net/Articles/182874/) (May 9, 2006)

[OLS: A proposal for a new networking API](https://lwn.net/Articles/192410/) (July 22, 2006)

[The return of network block device deadlock prevention](https://lwn.net/Articles/195416/) (August 14, 2006)

[Network namespaces](https://lwn.net/Articles/219794/) (January 30, 2007)

[Large receive offload](https://lwn.net/Articles/243949/) (August 1, 2007)

[Network transmit batching](https://lwn.net/Articles/246337/) (August 22, 2007)

[Multiqueue networking](https://lwn.net/Articles/289137/) (July 8, 2008)

[Char devices for network interfaces](https://lwn.net/Articles/356899/) (October 14, 2009)

[JLS2009: Generic receive offload](https://lwn.net/Articles/358910/) (October 27, 2009)

[Receive packet steering](https://lwn.net/Articles/362339/) (November 17, 2009)

[Receive flow steering](https://lwn.net/Articles/382428/) (April 7, 2010)

[ICMP sockets](https://lwn.net/Articles/420799/) (December 22, 2010)

[SKB fragment lifetime tracking](https://lwn.net/Articles/452866/) (July 25, 2011)

[Network transmit queue limits](https://lwn.net/Articles/454390/) (August 9, 2011)

[TCP connection hijacking and parasites - as a good thing](https://lwn.net/Articles/454427/) (August 9, 2011)

[Routing Open vSwitch into the mainline](https://lwn.net/Articles/469775/) (November 30, 2011)

[TCP connection repair](TCP%20connection%20repair.md) (May 1, 2012)

[TCP Fast Open: expediting web services](https://lwn.net/Articles/508865/) (August 1, 2012)

[TCP friends](https://lwn.net/Articles/511254/) (August 15, 2012)

[Low-latency Ethernet device polling](https://lwn.net/Articles/551284/) (May 21, 2013)

[Ethernet polling and patch-pulling latency](https://lwn.net/Articles/558305/) (July 10, 2013)

[TSO sizing and the FQ scheduler](https://lwn.net/Articles/564978/) (August 28, 2013)

[A proposal for "silent" port knocking](https://lwn.net/Articles/577164/) (December 18, 2013)

[Networking on tiny machines](https://lwn.net/Articles/597529/) (May 7, 2014)

[Foo over UDP](https://lwn.net/Articles/614348/) (October 1, 2014)

[Kernel support for SYN packet fingerprinting](https://lwn.net/Articles/644906/) (May 20, 2015)

[The kernel connection multiplexer](https://lwn.net/Articles/657999/) (September 21, 2015)

[SOCK_DESTROY: an old Android patch aims upstream](https://lwn.net/Articles/666220/) (December 2, 2015)

[Checksum offloads and protocol ossification](https://lwn.net/Articles/667059/) (December 8, 2015)

[A BoF on kernel network performance](https://lwn.net/Articles/676806/) (February 24, 2016)

[Transport-level protocols in user space](https://lwn.net/Articles/691887/) (June 20, 2016)

[The NET policy mechanism](https://lwn.net/Articles/696856/) (August 10, 2016)

[The rise of Linux-based networking hardware](https://lwn.net/Articles/720313/) (April 19, 2017)

[QUIC as a solution to protocol ossification](https://lwn.net/Articles/745590/) (January 29, 2018)

[Time-based packet transmission](https://lwn.net/Articles/748879/) (March 8, 2018)

[Let them run CAKE](https://lwn.net/Articles/758353/) (June 27, 2018)

[Writing network flow dissectors in BPF](Writing%20network%20flow%20dissectors%20in%20BPF.md) (September 6, 2018)

[The TCP SACK panic](https://lwn.net/Articles/791409/) (June 19, 2019)

[End-to-end network programmability](https://lwn.net/Articles/828056/) (August 10, 2020)

[Better visibility into packet-dropping decisions](https://lwn.net/Articles/885729/) (February 25, 2022)

[Bufferbloat](https://lwn.net/Kernel/Index/#Networking-Bufferbloat)

[LPC: An update on bufferbloat](https://lwn.net/Articles/458625/) (September 13, 2011)

[The CoDel queue management algorithm](https://lwn.net/Articles/496509/) (May 9, 2012)

[TCP small queues](https://lwn.net/Articles/507065/) (July 17, 2012)

[A damp discussion of network queuing](https://lwn.net/Articles/616241/) (October 15, 2014)

[Channels](https://lwn.net/Kernel/Index/#Networking-Channels)

[Van Jacobson's network channels](https://lwn.net/Articles/169961/) (January 31, 2006)

[Implementing network channels](https://lwn.net/Articles/182060/) (May 1, 2006)

[Reconsidering network channels](https://lwn.net/Articles/192767/) (July 25, 2006)

[The Grand Unified Flow Cache](https://lwn.net/Articles/194443/) (August 7, 2006)

[Patch summary: regulatory domains, network channels, and virtualization](Patch%20summary%3A%20regulatory%20domains%2C%20network%20channels%2C%20and%20virtualization.md) (October 25, 2006)

[The return of network channels](https://lwn.net/Articles/260880/) (December 4, 2007)

[Congestion control](https://lwn.net/Kernel/Index/#Networking-Congestion_control)

[Pluggable congestion avoidance modules](https://lwn.net/Articles/128681/) (March 22, 2005)

[The CHOKe packet scheduler](https://lwn.net/Articles/422477/) (January 11, 2011)

[Increasing the TCP initial congestion window](https://lwn.net/Articles/427104/) (February 9, 2011)

[LPC: Making the net go faster](https://lwn.net/Articles/458610/) (September 13, 2011)

[Delay-gradient congestion control](https://lwn.net/Articles/645115/) (May 20, 2015)

[BBR congestion control](https://lwn.net/Articles/701165/) (September 21, 2016)

[The congestion-notification conflict](https://lwn.net/Articles/783673/) (March 22, 2019)

[D-Bus](https://lwn.net/Kernel/Index/#Networking-D-Bus)

[Fast interprocess messaging](Fast%20interprocess%20messaging.md) (September 15, 2010)

[Speeding up D-Bus](Speeding%20up%20D-Bus.md) (February 29, 2012)

[Missing the AF_BUS](Missing%20the%20AF_BUS.md) (July 3, 2012)

[eXpress Data Path (XDP)](https://lwn.net/Kernel/Index/#Networking-eXpress_Data_Path_XDP)

[Early packet drop — and more — with BPF](Early%20packet%20drop%20—%20and%20more%20—%20with%20BPF.md) (April 6, 2016)

[Debating the value of XDP](https://lwn.net/Articles/708087/) (December 6, 2016)

[Accelerating networking with AF_XDP](https://lwn.net/Articles/750845/) (April 9, 2018)

[Lua in the kernel?](https://lwn.net/Articles/830154/) (September 9, 2020)

[hard_start_xmit() locking](https://lwn.net/Kernel/Index/#Networking-hard_start_xmit_locking)

[NETIF_F_LLTX](https://lwn.net/Articles/101215/) (September 8, 2004)

[NETIF_F_LLTX and race conditions](https://lwn.net/Articles/121566/) (February 1, 2005)

[IPv6](https://lwn.net/Kernel/Index/#Networking-IPv6)

[LCA: IP address exhaustion and the end of the open net](https://lwn.net/Articles/424696/) (January 26, 2011)

[IPv6 NAT](https://lwn.net/Articles/452293/) (July 20, 2011)

[Identifier locator addressing](https://lwn.net/Articles/657012/) (September 10, 2015)

[IPv6 segment routing](https://lwn.net/Articles/722804/) (May 17, 2017)

[The trouble with IPv6 extension headers](https://lwn.net/Articles/808896/) (January 7, 2020)

[Going big with TCP packets](https://lwn.net/Articles/884104/) (February 14, 2022)

[NAPI](https://lwn.net/Kernel/Index/#Networking-NAPI)

[NAPI performance - a weighty matter](https://lwn.net/Articles/139884/) (June 15, 2005)

[Reworking NAPI](https://lwn.net/Articles/214457/) (December 18, 2006)

[Newer, newer NAPI](https://lwn.net/Articles/244640/) (August 7, 2007)

[NAPI polling in kernel threads](https://lwn.net/Articles/833840/) (October 9, 2020)

[Netlink](https://lwn.net/Kernel/Index/#Networking-Netlink)

[Extending netlink](https://lwn.net/Articles/131802/) (April 12, 2005)

[Networking summits](https://lwn.net/Kernel/Index/#Networking-Networking_summits)

[Kernel Summit: Network summit summary](https://lwn.net/Articles/94565/) (July 21, 2004)

[Kernel Summit 2005: Report from the networking summit](https://lwn.net/Articles/144272/) (July 19, 2005)

[KS2010: Minisummit reports](KS2010%3A%20Minisummit%20reports.md) (November 3, 2010)

[A report from the networking miniconference](A%20report%20from%20the%20networking%20miniconference.md) (August 27, 2014)

[Netconf discussions, part 1](https://lwn.net/Articles/674943/) (February 10, 2016)

[Nonlinear packets v. large allocations](https://lwn.net/Kernel/Index/#Networking-Nonlinear_packets_v._large_allocations)

[Kswapd and high-order allocations](https://lwn.net/Articles/101230/) (September 8, 2004)

[Packet filtering](https://lwn.net/Kernel/Index/#Networking-Packet_filtering)

[Nftables: a new packet filtering engine](Nftables%3A%20a%20new%20packet%20filtering%20engine.md) (March 24, 2009)

[A JIT for packet filters](A%20JIT%20for%20packet%20filters.md) (April 12, 2011)

[Xtables2 vs. nftables](Xtables2%20vs.%20nftables.md) (January 9, 2013)

[The return of nftables](The%20return%20of%20nftables.md) (August 20, 2013)

[Network filtering for control groups](Network%20filtering%20for%20control%20groups.md) (August 24, 2016)

[BPF comes to firewalls](BPF%20comes%20to%20firewalls.md) (February 19, 2018)

[Bpfilter (and user-mode blobs) for 4.18](Bpfilter%20(and%20user-mode%20blobs)%20for%204.18.md) (May 30, 2018)

[Accelerating netfilter with hardware offload, part 1](https://lwn.net/Articles/809333/) (January 14, 2020)

[Accelerating netfilter with hardware offload, part 2](https://lwn.net/Articles/810663/) (January 31, 2020)

[Rethinking bpfilter and user-mode helpers](Rethinking%20bpfilter%20and%20user-mode%20helpers.md) (June 12, 2020)

[Nftables reaches 1.0](Nftables%20reaches%201.0.md) (August 27, 2021)

[Performance](https://lwn.net/Kernel/Index/#Networking-Performance)

[Bulk network packet transmission](https://lwn.net/Articles/615238/) (October 8, 2014)

[Improving Linux networking performance](https://lwn.net/Articles/629155/) (January 13, 2015)

[Measuring packet classifier performance](https://lwn.net/Articles/675056/) (February 10, 2016)

[Bulk memory-allocation APIs](https://lwn.net/Articles/684616/) (April 23, 2016)

[Zero-copy networking](https://lwn.net/Articles/726917/) (July 3, 2017)

[Zero-copy TCP receive](https://lwn.net/Articles/752188/) (April 19, 2018)

[A reworked TCP zero-copy receive API](https://lwn.net/Articles/754681/) (May 18, 2018)

[Batch processing of network packets](https://lwn.net/Articles/763056/) (August 21, 2018)

[Memory management for 400Gb/s interfaces](https://lwn.net/Articles/787754/) (May 8, 2019)

[Zero-copy network transmission with io_uring](https://lwn.net/Articles/879724/) (December 30, 2021)

[Protocols](https://lwn.net/Kernel/Index/#Networking-Protocols)

[Linux gets DCCP](Linux%20gets%20DCCP.md) (August 30, 2005)

[PF_CAN](PF_CAN.md) (October 8, 2007)

[How not to get a protocol implementation merged](How%20not%20to%20get%20a%20protocol%20implementation%20merged.md) (January 12, 2011)

[Mesh networking with batman-adv](Mesh%20networking%20with%20batman-adv.md) (February 8, 2011)

[Multipath TCP: an overview](Multipath%20TCP%3A%20an%20overview.md) (March 26, 2013)

[TLS in the kernel](TLS%20in%20the%20kernel.md) (December 2, 2015)

[The trouble with SMC-R](The%20trouble%20with%20SMC-R.md) (May 18, 2017)

[Upstreaming multipath TCP](Upstreaming%20multipath%20TCP.md) (September 26, 2019)

[Extending in-kernel TLS support](Extending%20in-kernel%20TLS%20support.md) (April 25, 2022)

[Adding an in-kernel TLS handshake](Adding%20an%20in-kernel%20TLS%20handshake.md) (June 1, 2022)

[SO_REUSEPORT](Kernel%20index%20[LWN.net].md)

[The SO_REUSEPORT socket option](The%20SO_REUSEPORT%20socket%20option.md) (March 13, 2013)

[Avoiding unintended connection failures with SO_REUSEPORT](Avoiding%20unintended%20connection%20failures%20with%20SO_REUSEPORT.md) (April 23, 2021)

[Virtual private networks](Kernel%20index%20[LWN.net].md)

[Virtual private networks with WireGuard](Virtual%20private%20networks%20with%20WireGuard.md) (March 6, 2018)

[WireGuarding the mainline](WireGuarding%20the%20mainline.md) (August 6, 2018)

[Whither WireGuard?](Whither%20WireGuard?.md) (March 25, 2019)

[WireGuard and the crypto API](WireGuard%20and%20the%20crypto%20API.md) (October 16, 2019)

[Window scaling](Kernel%20index%20[LWN.net].md)

[TCP window scaling and broken routers](TCP%20window%20scaling%20and%20broken%20routers.md) (July 7, 2004)

[Wireless](https://lwn.net/Kernel/Index/#Networking-Wireless)

[Toward a generic wireless access point stack](Toward%20a%20generic%20wireless%20access%20point%20stack.md) (June 9, 2004)

[Reworking the wireless extensions](Reworking%20the%20wireless%20extensions.md) (June 23, 2004)

[bcm43xx and the 802.11 stack](bcm43xx%20and%20the%20802.11%20stack.md) (December 6, 2005)

[Linux and wireless networking](Linux%20and%20wireless%20networking.md) (January 11, 2006)

[The 2006 Wireless Networking Summit](The%202006%20Wireless%20Networking%20Summit.md) (April 10, 2006)

[Kernel Summit 2006: Mini-summit summaries](Kernel%20Summit%202006%3A%20Mini-summit%20summaries.md) (July 18, 2006)

[The final wireless extension?](The%20final%20wireless%20extension?.md) (October 4, 2006)

[Patch summary: regulatory domains, network channels, and virtualization](Patch%20summary%3A%20regulatory%20domains%2C%20network%20channels%2C%20and%20virtualization.md) (October 25, 2006)

[Toward a free Atheros driver](Toward%20a%20free%20Atheros%20driver.md) (November 15, 2006)

[Wireless regulatory compliance](Wireless%20regulatory%20compliance.md) (June 6, 2007)

[Regulating wireless devices](Regulating%20wireless%20devices.md) (August 19, 2008)

[Broadcom firmware and regulatory compliance](Broadcom%20firmware%20and%20regulatory%20compliance.md) (September 22, 2010)

[Radar detection with Linux](Radar%20detection%20with%20Linux.md) (December 21, 2010)

[Airplane mode and rfkill](Airplane%20mode%20and%20rfkill.md) (March 2, 2016)

[Making WiFi fast](Making%20WiFi%20fast.md) (November 8, 2016)

[TCP small queues and WiFi aggregation — a war story](TCP%20small%20queues%20and%20WiFi%20aggregation%20—%20a%20war%20story.md) (June 18, 2018)