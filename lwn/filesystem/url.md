t [pramfs - a new filesystem](https://lwn.net/Articles/74138/) (March 3, 2004)

[Trapfs - an automounter on the cheap](https://lwn.net/Articles/109400/) (November 3, 2004)

[Debugfs](https://lwn.net/Articles/115405/) (December 13, 2004)

[The mini_fo filesystem](https://lwn.net/Articles/135283/) (May 10, 2005)

[A filesystem from Plan 9 space](https://lwn.net/Articles/137439/) (May 25, 2005)

[Files with negative offsets](https://lwn.net/Articles/138063/) (May 31, 2005)

[Coming soon: eCryptfs](https://lwn.net/Articles/156921/) (October 25, 2005)

[Shared subtrees](https://lwn.net/Articles/159077/) (November 8, 2005)

[KHB: A Filesystems reading list](https://lwn.net/Articles/196292/) (August 21, 2006)

[SEEK_HOLE or FIEMAP?](https://lwn.net/Articles/260795/) (December 3, 2007)

[What's AdvFS good for?](https://lwn.net/Articles/287289/) (June 25, 2008)

[Freezing filesystems and containers](https://lwn.net/Articles/287435/) (June 25, 2008)

[KS2008: Filesystem and block layer interaction](https://lwn.net/Articles/298589/) (September 16, 2008)

[A superficial introduction to fsblock](https://lwn.net/Articles/322668/) (March 11, 2009)

[That massive filesystem thread](https://lwn.net/Articles/326471/) (March 31, 2009)

[The two sides of reflink()](https://lwn.net/Articles/331808/) (May 5, 2009)

[This week's reflink() API](https://lwn.net/Articles/333783/) (May 19, 2009)

[Avoiding a read-only filesystem on errors](https://lwn.net/Articles/337765/) (June 17, 2009)

[Protected RAMFS](https://lwn.net/Articles/338435/) (June 24, 2009)

[Soft updates, hard problems](https://lwn.net/Articles/339337/) (July 1, 2009)

[O_*SYNC](https://lwn.net/Articles/350219/) (September 1, 2009)

[POSIX v. reality: A position on O_PONIES](https://lwn.net/Articles/351422/) (September 9, 2009)

[Featherstitch: Killing fsync() softly](https://lwn.net/Articles/354861/) (September 30, 2009)

[File holes, races, and mmap()](https://lwn.net/Articles/357767/) (October 21, 2009)

[LCA: Why filesystems are hard](https://lwn.net/Articles/370419/) (January 20, 2010)

[One billion files on Linux](https://lwn.net/Articles/400629/) (August 18, 2010)

[The return of SEEK_HOLE](https://lwn.net/Articles/440255/) (April 26, 2011)

[Runtime filesystem consistency checking](https://lwn.net/Articles/490291/) (April 3, 2012)

[O_HOT and O_COLD](https://lwn.net/Articles/494028/) (April 24, 2012)

[LSFMM: Range locking](https://lwn.net/Articles/548939/) (May 1, 2013)

[Two paths to a better readdir()](https://lwn.net/Articles/606995/) (July 30, 2014)

[Filesystem defragmentation](https://lwn.net/Articles/637428/) (March 23, 2015)

[UID/GID identity and filesystems](https://lwn.net/Articles/637431/) (March 23, 2015)

[Trading off safety and performance in the kernel](https://lwn.net/Articles/643892/) (May 12, 2015)

[Heading toward 2038-safe filesystems](https://lwn.net/Articles/672576/) (January 21, 2016)

[A case for variant symlinks](https://lwn.net/Articles/680705/) (March 23, 2016)

[Exposing extent information to user space](https://lwn.net/Articles/685978/) (May 4, 2016)

[Online filesystem scrubbing and repair](https://lwn.net/Articles/718800/) (April 5, 2017)

[Filesystem management interfaces](https://lwn.net/Articles/718803/) (April 5, 2017)

[Filesystem metadata memory management](https://lwn.net/Articles/752552/) (April 24, 2018)

[A filesystem "change journal" and other topics](https://lwn.net/Articles/755277/) (June 4, 2018)

[Implementing fully immutable files](https://lwn.net/Articles/786258/) (April 19, 2019)

[Filesystems for zoned block devices](https://lwn.net/Articles/788851/) (May 21, 2019)

[Filesystems and crash resistance](https://lwn.net/Articles/788938/) (May 21, 2019)

[Lazy file reflink](https://lwn.net/Articles/789038/) (May 22, 2019)

[Accessing zoned block devices with zonefs](https://lwn.net/Articles/794364/) (July 23, 2019)

[On-disk format robustness requirements for new filesystems](https://lwn.net/Articles/796687/) (August 19, 2019)

[Filesystem sandboxing with eBPF](https://lwn.net/Articles/803890/) (November 6, 2019)

[When and why to deprecate filesystems](https://lwn.net/Articles/886708/) (March 7, 2022)

[Zoned storage](https://lwn.net/Articles/897263/) (June 14, 2022)

[Toward a better definition for i_version](https://lwn.net/Articles/905931/) (August 26, 2022)