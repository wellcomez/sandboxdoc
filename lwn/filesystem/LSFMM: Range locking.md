---
title: "LSFMM: Range locking"
layout: post
---

# LSFMM: Range locking
<a style="text-decoration:underline" href="https://lwn.net/Articles/548939/">LSFMM: Range locking</a><br>

>Benefits for LWN subscribers.The primary benefit from [subscribing to LWN](https://lwn.net/subscribe/) is helping to keep us publishing, but, beyond that, subscribers get immediate access to all site content and access to a number of extra site features. Please sign up today!

At the 2013 LSFMM Summit, Jan Kara led a discussion of his [proposal for range locking](https://lwn.net/Articles/535843/). It is an in-kernel mechanism that would allow locking ranges of pages within a file, rather than locking the entire file. Currently, there are several synchronization primitives used to serialize page cache access, but some of the operations are not serialized, which can lead to races.

Range locking has a number of advantages, including fixing a race condition when punching holes in files that can result in filesystem corruption. Nothing prevents a page fault from happening when punching a hole, so there is a race with mmap(). There is also a need for a synchronizing mechanism when truncating the page cache (e.g. for direct I/O). A range lock will prevent the creation of pages within the range that is covered by the lock.

For filesystems that are changed to take advantage of it (or changed to do so for certain cases), range locks can replace the i_mutex (inode mutex). The patches to implement range locking are around 200 lines of code, Kara said. It is not a complex piece of code, either, just using a pair of mutexes, he said. The good news is that the way it is implemented, the overhead is minimal.

The reaction was generally favorable, though more testing is probably required.

  

| Index entries for this article |
| --- |
| [Kernel](https://lwn.net/Kernel/Index) | [Filesystems](https://lwn.net/Kernel/Index#Filesystems) |
| [Conference](https://lwn.net/Archives/ConferenceIndex/) | [Storage Filesystem & Memory Management/2013](https://lwn.net/Archives/ConferenceIndex/#Storage_Filesystem__Memory_Management-2013) |

  

* * *

