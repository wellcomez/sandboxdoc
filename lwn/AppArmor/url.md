[The AppArmor debate begins](https://lwn.net/Articles/181508/) (April 26, 2006)

[Kernel Summit 2006: Security](https://lwn.net/Articles/191737/) (July 19, 2006)

[Linux security non-modules and AppArmor](https://lwn.net/Articles/239962/) (June 27, 2007)

[TOMOYO Linux and pathname-based security](https://lwn.net/Articles/277833/) (April 14, 2008)