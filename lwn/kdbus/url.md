- [ALS: Linux interprocess communication and kdbus](https://lwn.net/Articles/551969/) (May 30, 2013)

- [The LPC Android microconference](https://lwn.net/Articles/570406/) (October 17, 2013)

- [The unveiling of kdbus](https://lwn.net/Articles/580194/) (January 13, 2014)

- [Kdbus meets linux-kernel](https://lwn.net/Articles/619068/) (November 4, 2014)

- [Version 2 of the kdbus patch set](https://lwn.net/Articles/624045/) (December 3, 2014)

- [Obstacles for kdbus](https://lwn.net/Articles/640357/) (April 15, 2015)

- [The kdbuswreck](https://lwn.net/Articles/641275/) (April 22, 2015)

- [Trying to head off kdbus](https://lwn.net/Articles/649111/) (June 24, 2015)

- [The LPC Android microconference, part 1](https://lwn.net/Articles/656324/) (September 8, 2015)

- [A mismatch of development styles](https://lwn.net/Articles/663956/) (November 11, 2015)

- [Bus1: a new Linux interprocess communication proposal](https://lwn.net/Articles/697191/) (August 17, 2016)