---
date: 2023-03-20 22:47
title: 深入分析Linux Kernel安全特性 System Trusted Keyring
tags:
- kernel keyring
---

# 深入分析Linux Kernel安全特性: System Trusted Keyring





## 概述

System Trusted Keyring为内核模块签名、IMA证书等安全机制提供了背书和基石，其功能包括：

-   认证内核模块的签名
-   认证IMA证书
-   认证从用户空间导入的system trusted key

System Trusted Keyring可以被认为是Linux kernel运行时的信任根的一部分，而且在这个keyring中的所有key都是被无条件信任的，因此这也是被称作“可信”的原因之一。

本文以Linux Kernel 4.x为例，深度分析System Trusted Keyring特性的方方面面，主要涉及以下方面：

-   与System Trusted Keyring相关的内核配置选项的作用的解读
-   在具体实现细节方面，按照从构建开始，到系统引导过程再到运行时的自然顺序，解读System Trusted Keyring在初始化阶段和运行时的实现细节
-   最后简要总结了System Trusted Keyring自身所依赖的其他安全特性

## 最佳实践

这份最佳实践适合于那些已经熟悉System Trusted Keyring特性并希望快速使用该特性的读者，以及想尽快动手实践的读者。对于那些对该特性还不太了解的读者，请按照行文顺序正常阅读。

-   用openssl命令生成PEM格式的证书文件system_key.crt：

```sh
openssl req -new -nodes -utf8 -sha256 -days 36500 \
        -batch -x509 -config openssl.cnf \
        -outform PEM -out system_key.crt \
        -keyout system_key.pem
```

其中openssl.cnf的内容如下：

```
[ req ]
default_bits = 2048
distinguished_name = req_distinguished_name
prompt = no
string_mask = utf8only
x509_extensions = v3_req

[ req_distinguished_name ]
O = Test
OU = Test
CN = Test modsign key
emailAddress = test@foo.com

[ v3_req ]
basicConstraints=critical,CA:FALSE
keyUsage=digitalSignature
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
```

-   用openssl命令生成额外的PEM格式的证书文件extra_system_key.crt，并且确保该证书是用system_key.crt签名过的：

```sh
openssl req -new -newkey rsa:2048 \
        -sha256 -nodes \
        -config openssl.cnf \
        -keyout extra_system_key.pem \
        -out extra_system_key.csr

    openssl x509 -req -in extra_system_key.csr \
        -CA system_key.crt \
        -CAkey system_key.pem \
        -set_serial 1 -days 3650 \
        -extfile openssl.cnf -extensions v3_req \
        -out extra_system_key.crt
```

-   确保私钥文件system_key.pem和extra_system_key.pem的安全存储。  
    
-   配置内核  
    

-   开启内核模块签名或IMA相关的kernel config
-   CONFIG_SYSTEM_TRUSTED_KEYS=\<前一个步骤生成的system_key.crt文件的路径>
-   CONFIG_SYSTEM_EXTRA_CERTIFICATE=y
-   CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE=32768
-   CONFIG_SECONDARY_TRUSTED_KEYRING=y  
    

-   编译并安装内核和模块  
    
-   运行时加载额外的system trusted key  
    

```sh
openssl x509 -in extra_system_key.crt -out extra_system_key.der -outform DER
sudo keyctl padd asymmetric "Extra System Trust Key" %:.secondary_trusted_keys < extra_system_key.der
```

## 内核配置选项

### CONFIG_SYSTEM_TRUSTED_KEYRING: Provide system-wide ring of trusted keys

内核会在初始化阶段创建出一个或多个system trusted keyring。凡是添加到这些keyring里的key都将被无条件信任，因此这些key被称为system trusted key。

在没有开启CONFIG_SECONDARY_TRUSTED_KEYRING的情况下，内核仅会创建一个名为.builtin_trusted_keys的system trusted keyring。为了与随后介绍的secondary trusted keying进行区分，我们把.builtin_trusted_keys叫做builtin trusted keyring；而system trusted keyring则是builtin trusted keyring和secondary trusted keying的统称。

向system trusted keyring中添加system trusted key的方法共有4种： 
- 通过内核配置CONFIG_SYSTEM_TRUSTED_KEYS=和CONFIG_MODULE_SIG_KEY=指定的、之后被编译到内核中的key 
- 使用工具insert-sys-cert向vmlinux中的保留区域写入的key 
- 从硬件key store（比如TPM设备）加载的key 
- 从用户空间导入、且经过system trusted keyring里的key签过名的key（该功能仅限于内核开启了CONFIG_SECONDARY_TRUSTED_KEYRING的情况下）

### CONFIG_SYSTEM_TRUSTED_KEYS: Additional X.509 keys for default system keyring

该选项指定了一个PEM格式的X.509证书文件的路径。该文件可以包含多个证书，构造方法是将多个X.509证书级联为单个文件。

kbuild会将这个X.509证书文件转换为system certificate list并编译进内核中，然后在system trusted keyring初始化阶段将这个list中的每一个X.509证书都添加到builtin trusted keyring中。此时，每一个X.509证书就是一把system trusted key。

注意该选项名称中的“Additional”字样，它的含义是：相对于CONFIG_MODULE_SIG_KEY指定的、用于对内核模块签名的system trusted key而言，该选项还可以定义额外的system trusted key。

Builtin trusted keyring中的key的用途之一是可以用来验证内核模块签名，在功能上与CONFIG_MODULE_SIG_KEY指定的key完全一致。

### CONFIG_SYSTEM_EXTRA_CERTIFICATE：Reserve area for inserting a certificate without recompiling

如果system trusted key只能通过CONFIG_SYSTEM_TRUSTED_KEYS选项静态指定的话，那么每次需要添加system trusted key的时候都需要重编内核，这会让签名策略和签名模型变得非常死板。

于是内核开发者想出了一种动态添加额外的system trusted key的方法：在vmlinux中事先预留一段空间，每当想使用新的system trusted key的时候，只要通过工具将新的key写入到vmlinux中的这个预留区域即可。然后在重启内核vmlinux之后，内核会从这个预留区域中将这些额外添加的system trusted key也加载到builtin trusted keyring中。因此，这种被事后写入到vmlinux中的key被称为extra system trusted key。

为了实现该功能，设计者巧妙地定义了这样一个layout：

```c
__INITRODATA

        .align 8
        .globl VMLINUX_SYMBOL(system_certificate_list)
VMLINUX_SYMBOL(system_certificate_list):
__cert_list_start:
#ifdef CONFIG_MODULE_SIG
        .incbin "certs/signing_key.x509"
#endif
        .incbin "certs/x509_certificate_list"
__cert_list_end:

#ifdef CONFIG_SYSTEM_EXTRA_CERTIFICATE
        .globl VMLINUX_SYMBOL(system_extra_cert)
        .size system_extra_cert, CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE
VMLINUX_SYMBOL(system_extra_cert):
        .fill CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE, 1, 0

        .align 4
        .globl VMLINUX_SYMBOL(system_extra_cert_used)
VMLINUX_SYMBOL(system_extra_cert_used):
        .int 0

#endif /* CONFIG_SYSTEM_EXTRA_CERTIFICATE */

        .align 8
        .globl VMLINUX_SYMBOL(system_certificate_list_size)
VMLINUX_SYMBOL(system_certificate_list_size):
#ifdef CONFIG_64BIT
        .quad __cert_list_end - __cert_list_start
#else
        .long __cert_list_end - __cert_list_start
#endif
```

在构造内核时，保留给extra system trusted key的区域由符号system_extra_cert定义，该区域的大小由CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE确定，并且该区域紧邻预先编译进内核image中的其他system trusted key；system_extra_cert_used符号定义了system_extra_cert区域中已经写入了的extra system trusted key的实际大小；system_certificate_list_size则定义了预先编译进内核image中的其他system trusted key以及extra system trusted key的大小总和。

预先编译进vmlinux中的system trusted key以及extra system trusted key被统称为system certificate list，它们的共同点都是在内核启动前就已经写入到了vmlinux中，只不过写入的时机不同，前者永远是在build内核的时候写入的，因此前者本质上同时存在于bzImage和vmlinux中；而后者是在运行或离线时借助外部工具insert-sys-cert写入的，鉴于重新构造bzImage的复杂性，通常只能写到vmlinux而不是bzImage中。

### CONFIG_SYSTEM_EXTRA_CERTIFICATE_SIZE：Number of bytes to reserve for the extra certificate

该选项指定在vmlinux中为将来需要额外添加的system trusted key所预留的区域的大小。默认值为4096字节。

### CONFIG_SECONDARY_TRUSTED_KEYRING：Provide a keyring to which extra trustable keys may be added

使用内核image中的预留区域动态添加system trusted key的方式仍旧不够灵活，甚至在某些情况下不适用。

为了凸显CONFIG_SYSTEM_EXTRA_CERTIFICATE特性带来的不便，这里使用了两个稍微极端一点的例子来说明使用它到底有多麻烦：为了能够动态添加额外的system trusted key，我们只能修改vmlinux文件，而不能直接修改bzImage文件；接下来，为了能够使额外添加的system trusted key生效，需要重启kernel，并且使用vmlinux文件来启动系统；在重启之前，还需要增加新的grub启动选项，再配合grub2-set-default指定下次启动要从新的grub启动选项引导系统。上述种种做法中的每一个环节都可能会给系统升级和部署带来额外的麻烦。再比如，对于使用了Secure Boot的系统来说，添加额外的system trusted key的过程就是在修改内核image，如果没有办法对修改后的内核进行重签名，那么在下次启动新内核的时候，就会因为内核image签名认证失败导致系统无法启动。解决方法只有对修改过的内核image重新签名，但在某些业务场景下这么做可能非常不方便（比如网络环境不支持远程签名等），甚至无法实现。

因此，内核开发者设计了一种更为灵活的方案：secondary trusted keyring，以方便用户从用户空间加入新的system trusted key。严格来讲上，从用户空间加载的这些system trusted key不会加入到builtin trusted keyring中，而是会被加入到名为.secondary_trusted_keys的system trusted keyring中，即所谓的secondary trusted keyring。不过，加入到这个keyring中的key与加入到builtin trusted keyring中的key同样都被认为是可信的system trusted key。

当然，能加入到secondary trusted keyring中的key必须满足以下条件：

-   key是从用户空间加载的
-   key必须是由system trusted keyring（即builtin trusted keyring或secondary trusted keyring）中的任意一把key签过的

只有满足上述条件，才能确保从用户空间加载的key是真正可信的，才能有资格成为system trusted key。

但需要注意的是，secondary trusted keyring中的key与builtin trusted keyring中的key在用途上存在微小的差异，搞错的话会非常致命：

-   内核模块只能用builtin trusted keyring中的key来验证，而绝不能用secondary trusted keyring中的key来验证。

## 实现细节

## 构建内核阶段

### X.509证书格式转换

kbuild会调用scripts/extract-cert程序将CONFIG_SYSTEM_TRUSTED_KEYS指定的PEM格式的X.509证书文件（可以将多个X.509证书级联为单个文件）转换为DER格式的certs/x509_certificate_list文件：

```sh
scripts/extract-cert $(CONFIG_SYSTEM_TRUSTED_KEYS) certs/x509_certificate_list
```

-   用法

```sh
extract-cert <source> <dest>
```

-   选项
-   \<source>  
    指定待转换的PEM格式的X.509证书文件；另外该选项也支持PKCS#11标准，即支持从HSM或智能卡中读取X.509证书。使用PKCS#11的方法是将\参数指定为"pkcs11:<参数>"。  
    
-   \<dest>  
    将\<source>转换为DER格式的X.509证书文件的保存路径。  
    
-   环境变量  
    
-   KBUILD_VERBOSE  
    kbuild在编译内核过程中是否开启verbose输出模式。如果开启了verbose输出模式，那么在运行extract-cert的时候也会输出额外的详细信息。  
    
-   KBUILD_SIGN_PIN  
    该选项仅当\<source>为PKCS#11时有用。该选项用来指定PKCS#11的PIN码。  
    
-   实现细节  
    

-   打开并读取\<source>指定的PEM格式的X.509证书文件、或PKCS#11指定的X.509证书。
-   将读取到的证书内容转换为DER格式，并写入\<dest>参数指定的文件中。
-   如果<source>指定的参数内含多个证书，重复上述步骤。
-   每次成功提取出一个X.509证书就会显示“Extracted cert: <证书的标题>”。

### 构建system certificate list

接下来，上一个步骤生成的certs/x509_certificate_list文件会连同system_certificates.S一起被编译为certs/system_certificates.o。该object文件的内容会被包含到内核image的.init.rodata节中。

```c
__INITRODATA

        .align 8
        .globl VMLINUX_SYMBOL(system_certificate_list)
VMLINUX_SYMBOL(system_certificate_list):
__cert_list_start:
#ifdef CONFIG_MODULE_SIG
        .incbin "certs/signing_key.x509"
#endif
        .incbin "certs/x509_certificate_list"
__cert_list_end:
```

该system certificate list中的所有X.509证书最终都会被加载到builtin trusted keyring中，并作为system trusted key来使用。

顺便一提，由内核选项CONFIG_MODULE_SIG_KEY指定的对内核模块进行签名的key文件（即certs/signing_key.x509）也会成为system certificate list的一部分。准确来说，它会在内核初始化system trusted keyring时加入到builtin trusted keyring中。

## 内核引导阶段

### 初始化system trusted keyrings

内核在启动阶段，会调用system_trusted_keyring_init函数对system trusted keyrings进行初始化。

```sh
device_initcall(system_trusted_keyring_init);
```

-   打印提示信息"Initialise system trusted keyrings"  
    
-   创建builtin trusted keyring：  
    

```c
builtin_trusted_keys =
                keyring_alloc(".builtin_trusted_keys",
                              KUIDT_INIT(0), KGIDT_INIT(0), current_cred(),
                              ((KEY_POS_ALL & ~KEY_POS_SETATTR) |
                              KEY_USR_VIEW | KEY_USR_READ | KEY_USR_SEARCH),
                              KEY_ALLOC_NOT_IN_QUOTA,
                              NULL, NULL);
```

-   创建secondary trusted keyring：

```c
secondary_trusted_keys =
                keyring_alloc(".secondary_trusted_keys",
                              KUIDT_INIT(0), KGIDT_INIT(0), current_cred(),
                              ((KEY_POS_ALL & ~KEY_POS_SETATTR) |
                               KEY_USR_VIEW | KEY_USR_READ | KEY_USR_SEARCH |
                               KEY_USR_WRITE),
                              KEY_ALLOC_NOT_IN_QUOTA,
                              restrict_link_by_builtin_and_secondary_trusted,
                              NULL);
```

注意：与builtin trusted keyring不同，secondary trusted keyring的属性包括了KEY_USR_WRITE，这就是为什么允许从用户空间向secondary trusted keyring加载system trusted key的根本原因。

-   在secondary trusted keyring中创建一个指向builtin trusted keyring的link，这样在访问secondary trusted keyring的时候，就可以直接引用builtin trusted keyring里的key：

```c
key_link(secondary_trusted_keys, builtin_trusted_keys)
```

因此，只要遍历secondary trusted keyring，就能遍历到连同builtin trusted keyring一起在内所有的system trusted key。

### 加载builtin system trusted keys

内核在启动阶段，会调用load_system_certificate_list函数尝试从system certificate list中加载所有的X.509证书到builtin system trusted keyring中。

```c
late_initcall(load_system_certificate_list);
```

-   打印信息"Loading compiled-in X.509 certificates"。  
    
-   遍历system certificate list中的每一个X.509证书，并尝试加载到builtin system trusted keyring中：

```c
key = key_create_or_update(make_key_ref(builtin_trusted_keys, 1),
                                           "asymmetric",
                                           NULL,
                                           p,
                                           plen,
                                           ((KEY_POS_ALL & ~KEY_POS_SETATTR) |
                                           KEY_USR_VIEW | KEY_USR_READ),
                                           KEY_ALLOC_NOT_IN_QUOTA |
                                           KEY_ALLOC_BUILT_IN |
                                           KEY_ALLOC_BYPASS_RESTRICTION);
```

-   如果加载成功，显示“Loaded X.509 cert '<证书的标题>'”；如果加载失败，显示“Problem loading in-kernel X.509 certificate (<错误码>)”。

根据之前的解说可知，builtin system trusted keys的成分包括：

-   通过内核选项CONFIG_SYSTEM_TRUSTED_KEYS预先编译进内核中的system trusted key。
-   通过内核选项CONFIG_MODULE_SIG_KEY预先编译进内核中的system trusted key。
-   通过外部工具insert-sys-cert写入到vmlinux中保留区域内的extra system trusted key。

### 初始化IMA trusted keyring

```c
late_initcall(init_ima);

static int __init init_ima(void)
....
        error = ima_init();
                ....
                rc = integrity_init_keyring(INTEGRITY_KEYRING_IMA);  // ".ima"
                ....
....
```

-   创建ima trusted keyring：

```c
keyring[id] = keyring_alloc(keyring_name[id], KUIDT_INIT(0),
                                    KGIDT_INIT(0), cred,
                                    ((KEY_POS_ALL & ~KEY_POS_SETATTR) |
                                     KEY_USR_VIEW | KEY_USR_READ |
                                     KEY_USR_WRITE | KEY_USR_SEARCH),
                                    KEY_ALLOC_NOT_IN_QUOTA,
                                    restrict_link_to_ima, NULL);
```

其中restrict_link_to_ima宏会被扩展为restrict_link_by_builtin_and_secondary_trusted。

与secondary trusted keyring类似，ima trusted keyring的属性也包括了KEY_USR_WRITE，因此允许从用户空间向ima trusted keyring加载ima trusted key。

### 将默认的IMA trusted key加载到IMA trusted keyring中

如果CONFIG_IMA_X509_PATH（默认值为/etc/keys/x509_ima.der）设置了值，则该参数指定的DER格式的X.509证书文件会被加载到IMA trusted keyring中。

注意：该参数指定的文件位于运行时系统中，而不是在build内核时指定的、从build主机上加载并编译到内核里。因此，运行时系统中必须事先存放好该文件。

```c
#ifdef CONFIG_IMA_LOAD_X509
void __init ima_load_x509(void)
{
        int unset_flags = ima_policy_flag & IMA_APPRAISE;

        ima_policy_flag &= ~unset_flags;
        integrity_load_x509(INTEGRITY_KEYRING_IMA, CONFIG_IMA_X509_PATH);
        ima_policy_flag |= unset_flags;
}
#endif
```

-   能够导入到IMA trusted keyring中的key必须是经过builtin trusted keyring或secondary trusted keyring中的key签名的。当内核尝试将CONFIG_IMA_X509_PATH指定的X.509证书导入到IMA trusted keyring的时候，会回调restrict_link_by_builtin_and_secondary_trusted()。该函数的作用就是确保上述安全准则：

```c
#ifdef CONFIG_SECONDARY_TRUSTED_KEYRING
/**
 * restrict_link_by_builtin_and_secondary_trusted - Restrict keyring
 *   addition by both builtin and secondary keyrings
 *
 * Restrict the addition of keys into a keyring based on the key-to-be-added
 * being vouched for by a key in either the built-in or the secondary system
 * keyrings.
 */
int restrict_link_by_builtin_and_secondary_trusted(
        struct key *keyring,
        const struct key_type *type,
        const union key_payload *payload)
{
        /* If we have a secondary trusted keyring, then that contains a link
         * through to the builtin keyring and the search will follow that link.
         */
        if (type == &key_type_keyring &&
            keyring == secondary_trusted_keys &&
            payload == &builtin_trusted_keys->payload)
                /* Allow the builtin keyring to be added to the secondary */
                return 0;

        return restrict_link_by_signature(secondary_trusted_keys, type, payload);
}
```

这里很有意思，如果尝试把builtin trusted keyring中的key加入到secondary trusted keyring中，则被无条件放行。我们已经知道builtin trusted keyring已经实现link到了secondary trusted keyring中，因此没有必要再把builtin trusted keyring中的key添加到secondary trusted keyring中了。不过出于逻辑上的严谨，这里要把这种情况过滤出去；对于剩下的情况，导入的key必须先通过restrict_link_by_signature()的检查。

-   restrict_link_by_signature()会去确保导入的key必须是经过trust_keyring参数即secondary trusted keyring中的key签过名，其证书内容没有经过篡改。

```c
/**
 * restrict_link_by_signature - Restrict additions to a ring of public keys
 * @trust_keyring: A ring of keys that can be used to vouch for the new cert.
 * @type: The type of key being added.
 * @payload: The payload of the new key.
 *
 * Check the new certificate against the ones in the trust keyring.  If one of
 * those is the signing key and validates the new certificate, then mark the
 * new certificate as being trusted.
 *
 * Returns 0 if the new certificate was accepted, -ENOKEY if we couldn't find a
 * matching parent certificate in the trusted list, -EKEYREJECTED if the
 * signature check fails or the key is blacklisted and some other error if
 * there is a matching certificate but the signature check cannot be performed.
 */
int restrict_link_by_signature(struct key *trust_keyring,
                               const struct key_type *type,
                               const union key_payload *payload)
{
        const struct public_key_signature *sig;
        struct key *key;
        int ret;

        pr_devel("==>%s()\n", __func__);

        if (!trust_keyring)
                return -ENOKEY;

        if (type != &key_type_asymmetric)
                return -EOPNOTSUPP;

        sig = payload->data[asym_auth];
        if (!sig->auth_ids[0] && !sig->auth_ids[1])
                return -ENOKEY;

        if (ca_keyid && !asymmetric_key_id_partial(sig->auth_ids[1], ca_keyid))
                return -EPERM;

        /* See if we have a key that signed this one. */
        key = find_asymmetric_key(trust_keyring,
                                  sig->auth_ids[0], sig->auth_ids[1],
                                  false);
        if (IS_ERR(key))
                return -ENOKEY;

        if (use_builtin_keys && !test_bit(KEY_FLAG_BUILTIN, &key->flags))
                ret = -ENOKEY;
        else
                ret = verify_signature(key, sig);
        key_put(key);
        return ret;
}
```

## 用户空间运行时阶段

### 加载内核模块

在加载内核模块时，内核会使用builtin trusted keyring中的key对内核模块中的模块签名进行检查。

### 查看builtin trusted keyring

```text
$ sudo keyctl list %:.builtin_trusted_keys
6 keys in keyring:
...
```

可以看到，正如前面分析的那样，builtin trusted keyring里的key的数量与内核配置以及写入到内核image的保留区域内的extra system trusted key的数量有关。

### 查看secondary trusted keyring

```text
$ sudo keyctl list %:.secondary_trusted_keys
1 key in keyring:
 94515123: --alswrv     0     0 keyring: .builtin_trusted_keys
```

可以看到，正如前面分析的那样，builtin trusted keyring整个全都link到了secondary trusted keyring，因此当前secondary trusted keyring里只有一个keyring对象。

### 从用户空间向secondary trusted keyring导入system trusted key

执行以下命令可将extra_system_key.der导入到secondary trusted keyring中：

```text
openssl x509 -in extra_system_key.pem -out extra_system_key.der -outform DER
sudo keyctl padd asymmetric "Extra System Trust Key" %:.secondary_trusted_keys < extra_system_key.der
```

与想IMA trusted keyring中导入key的情况类似，不是任意一个DER格式的X.509证书都可以无条件的导入到secondary trusted keyring中。只有经过builtin trusted keyring和secondary trusted keyring中的key签名过的X.509证书才能导入到secondary trusted keyring中，才能成为真正意义上的system trusted key。

导入后，再次检查secondary trusted keyring的内容：

```text
$ sudo keyctl list %:.secondary_trusted_keys
2 keys in keyring:
 94515123: --alswrv     0     0 keyring: .builtin_trusted_keys
589270152: --als--v     0     0 asymmetric: Extra System Trust Keys
```

可以看到，刚刚导入的extra_system_key.der已经出现在了secondary trusted keyring中，证明导入成功。

另外，如果extra_system_key.der是多个X.509级联组成的，就需要运行另一下命令才能查看到“Extra System Trust Keys”内部的所有子key：

```text
sudo keyctl show %:.secondary_trusted_keys
```

### 向内核image的保留区域写入额外的system trusted key

内核自带的工具scripts/inert-sys-cert提供了向vmlinux文件写入额外的system trusted key的功能。需要注意的是，每次调用该工具都会覆盖vmlinux中已经存在的、之前已经写入的额外的system trusted key。如果想实现类似“append”的写入方式，只能把原先的system trusted key与新的system trusted key级联为一个文件，然后用-c参数指定它。

-   用法

```sh
insert-sys-cert [-s <System.map> ] -b <vmlinux> -c <certfile>
```

-   选项
-   -s \<System.map> 包含内核符号的文件。可选参数，仅在vmlinux文件中不包含symbol表的情况下才需要指定。  
    
-   -b \<vmlinux> 待写入额外的system trusted key的vmlinux文件（ELF格式）。  
    
-   -c \<certfile> 待写入到vmlinux文件的额外的system trusted key文件。  
    
-   实现细节  
    
-   打开vmlinux文件，按照ELF格式尝试找到symbol节。  
    

-   如果没找到symbol节，显示“Could not find the symbol table.”信息；然后逐行解析System.map文件，并尝试找到以下三个符号：system_extra_cert，system_extra_cert_used以及ystem_certificate_list_size。下面是一个包含这3个符号的System.map文件的示例：

`ffffffff81f3fb64 T system_extra_cert ffffffff81f47b64 T system_extra_cert_used ffffffff81f47b68 T system_certificate_list_size`  
在解析过程中，每个符号的信息会记录在struct sym中：  
`C struct sym { char *name; // 符号的名称 unsigned long address; // 符号的运行时地址 unsigned long offset; // 符号在vmlinux文件中的偏移 void *content; // 指向符号的值在内存中的位置 int size; // 表示该符号所代表的数据占用的字节大小 };`  

-   如果找到了symbol节，显示“Symbol table found.”信息；如果同时还指定了-s选项，则忽略System.map文件，同时还会显示“System.map is ignored.”。
-   分别打印这3个符号的名称，运行时地址，大小以及偏移。
-   如果\<certfile>文件的大小超过了整个保留区域的大小，显示错误信息“Certificate is larger than the reserved area!”。
-   如果\<certfile>文件的大小等于system_extra_cert_used记录的大小，且内容也与已经写入到内核image中的system_extra_cert完全一致，则显示“Certificate was already inserted.”，且不再向内核image重复写入\<certfile>文件。
-   如果system_extra_cert_used的值不等于0，证明vmlinux文件之前已经被写入过system trusted key，因此需要显示“Replacing previously inserted certificate.”，以提醒用户之前写入的system trusted key将会被覆盖。
-   将\<certfile>文件的内容写入system_extra_cert区域，并覆盖原先写入过的system trusted key。
-   如果新写入的system trusted key的长度小于之前写入的system trusted key，则将不足的部分清0，防止泄露之前的system trusted key的信息。
-   更新system_certificate_list_size，以便内核启动时能够识别新写入的system trusted key。
-   更新system_extra_cert_used，以便反映出真实的system_extra_cert的大小。
-   显示信息"Inserted the contents of \<certfile文件名> into \<system_extra_cert的运行时内核地址>."。
-   显示信息"Used \<system_extra_cert_used的值> bytes out of \<system_extra_cert区域的总长度> bytes reserved."。

## 保护System Trusted Keyring

-   Secure Boot System Trusted Keyring需要Secure Boot机制或KEXEC image验证作为背书才能算是真正的安全。因为builtin trusted keyring中的key是从内核image中读取并加载到builtin trusted keyring中去的。如果攻击者篡改或向内核image添加了自己的key，而内核image本身又没有做任何任何完整性检查，那么系统的安全性就没有任何保证，内核模块签名、IMA等安全机制都将形同虚设。  
    
-   运行时防护 System Trusted Keyring的内容很容易受到内核漏洞或DMA攻击而被篡改内容，因此可以利用一些运行时防护手段组织攻击者对其内容进行攻击。

## BUG

加载builtin system trusted keys和初始化IMA trusted keyring的实际都发生在late_initcall():

```c
late_initcall(load_system_certificate_list);
        late_initcall(init_ima);
```

从关系上来看，最严谨的做法应该是让load_system_certificate_list总是先执行，然后才是init_ima，否则如果后者先执行，builtin system trusted keyring中的key为空，IMA trusted key就无法被加载到IMA trusted keyring中，最严重的情况下可能导致系统启动失败。

## FAQ

## 如何添加额外的system trusted key？

-   在可重编内核的前提下，修改CONFIG_SYSTEM_TRUSTED_KEYS指定的key文件的内容，将更多的system trusted key级联到该key文件中即可。
-   在不可重编内核的前提下，借用CONFIG_SYSTEM_EXTRA_CERTIFICATE或CONFIG_SECONDARY_TRUSTED_KEYRING可以添加额外的system trusted key。第一种方法要求内核在build时要事先保留足够大的空间。如果该空间的总容量本身就很小，那只能精简要导入的X.509证书的内容，删除掉不再使用的system trusted key；第二种方法是从用户空间直接向secondary trusted keyring中导入额外的system trusted key，不过这个key必须事先由已经存在于builtin trusted keyring或secondary trusted keyring中的某一把key签过的才可以导入。

## 如何一次性编译或写入多个system trusted key？

将多个system trusted key先级联为单个文件，再按照规定编译（通过内核配置选项CONFIG_SYSTEM_TRUSTED_KEYS）或写入（利用工具insert-sys-cert）到内核image中。内核在解析时，能够自动识别多个system trusted key。

编辑于 2021-11-27 20:38