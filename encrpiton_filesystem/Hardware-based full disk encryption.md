---
title: "Hardware-based full disk encryption"
layout: post
---
Reader View [en.wikipedia.org /wiki/Hardware-based\_full\_disk\_encryption](https://en.wikipedia.org/wiki/Hardware-based_full_disk_encryption)

# Hardware-based full disk encryption

Contributors to Wikimedia projects 11-14 minutes 2008/4/6

* * *

**Hardware-based full disk encryption** (**FDE**) is available from many [hard disk drive](https://en.wikipedia.org/wiki/Hard_disk_drive "Hard disk drive") (HDD/[SSD](https://en.wikipedia.org/wiki/Solid-state_drive "Solid-state drive")) vendors, including: ClevX, [Hitachi](https://en.wikipedia.org/wiki/Hitachi "Hitachi"), Integral Memory, iStorage Limited, [Micron](https://en.wikipedia.org/wiki/Micron_Technology "Micron Technology"), [Seagate Technology](https://en.wikipedia.org/wiki/Seagate_Technology "Seagate Technology"), [Samsung](https://en.wikipedia.org/wiki/Samsung "Samsung"), [Toshiba](https://en.wikipedia.org/wiki/Toshiba "Toshiba"), [Viasat UK](https://en.wikipedia.org/wiki/ViaSat "ViaSat"), [Western Digital](https://en.wikipedia.org/wiki/Western_Digital "Western Digital"). The [symmetric encryption key](https://en.wikipedia.org/wiki/Symmetric-key_algorithm "Symmetric-key algorithm") is maintained independently from the computer's [CPU](https://en.wikipedia.org/wiki/Central_processing_unit "Central processing unit"), thus allowing the complete data store to be encrypted and removing computer memory as a potential attack vector.

Hardware-FDE has two major components: the hardware encryptor and the data store. There are currently four varieties of hardware-FDE in common use:

1.  Hard disk drive (HDD) FDE (self-encrypting drive)
2.  Enclosed hard disk drive FDE
3.  Removable hard disk drive FDE
4.  Bridge and [Chipset](https://en.wikipedia.org/wiki/Chipset "Chipset") (BC) FDE

Hardware designed for a particular purpose can often achieve better performance than [disk encryption software](https://en.wikipedia.org/wiki/Disk_encryption_software "Disk encryption software"), and disk encryption hardware can be made more transparent to software than encryption done in software. As soon as the key has been initialised, the hardware should in principle be completely transparent to the OS and thus work with any OS. If the disk encryption hardware is integrated with the media itself the media may be designed for better integration. One example of such design would be through the use of physical sectors slightly larger than the logical sectors.

## Hardware-based full disk encryption Types\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=1 "Edit section: Hardware-based full disk encryption Types")\]

### Hard disk drive FDE\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=2 "Edit section: Hard disk drive FDE")\]

Usually referred to as **self-encrypting drive** (**SED**). HDD FDE is made by HDD vendors using the [OPAL](https://en.wikipedia.org/wiki/Opal_Storage_Specification "Opal Storage Specification") and Enterprise standards developed by the [Trusted Computing Group](https://en.wikipedia.org/wiki/Trusted_Computing_Group "Trusted Computing Group").[\[1\]](#cite_note-1) [Key management](https://en.wikipedia.org/wiki/Key_management "Key management") takes place within the hard disk controller and encryption keys are 128 or 256 [bit](https://en.wikipedia.org/wiki/Bit "Bit") [Advanced Encryption Standard](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Advanced Encryption Standard") (AES) keys. [Authentication](https://en.wikipedia.org/wiki/Authentication "Authentication") on power up of the drive must still take place within the [CPU](https://en.wikipedia.org/wiki/Central_processing_unit "Central processing unit") via either a [software](https://en.wikipedia.org/wiki/Software "Software") [pre-boot authentication](https://en.wikipedia.org/wiki/Pre-boot_authentication "Pre-boot authentication") environment (i.e., with a [software-based full disk encryption](https://en.wikipedia.org/wiki/Disk_encryption_software "Disk encryption software") component - hybrid full disk encryption) or with a [BIOS](https://en.wikipedia.org/wiki/BIOS "BIOS") password.

[Hitachi](https://en.wikipedia.org/wiki/Hitachi "Hitachi"), [Micron](https://en.wikipedia.org/wiki/Micron_Technology "Micron Technology"), [Seagate](https://en.wikipedia.org/wiki/Seagate_Technology "Seagate Technology"), [Samsung](https://en.wikipedia.org/wiki/Samsung "Samsung"), and [Toshiba](https://en.wikipedia.org/wiki/Toshiba "Toshiba") are the disk drive manufacturers offering [TCG](https://en.wikipedia.org/wiki/Trusted_Computing_Group "Trusted Computing Group") [OPAL](https://en.wikipedia.org/wiki/Opal_Storage_Specification "Opal Storage Specification") [SATA](https://en.wikipedia.org/wiki/Serial_ATA "Serial ATA") drives. HDDs have become a commodity so SED allow drive manufacturers to maintain revenue.[\[2\]](#cite_note-2) Older technologies include the proprietary Seagate DriveTrust, and the older, and less secure, [PATA](https://en.wikipedia.org/wiki/Parallel_ATA "Parallel ATA") Security command standard shipped by all drive makers including [Western Digital](https://en.wikipedia.org/wiki/Western_Digital "Western Digital"). Enterprise SAS versions of the TCG standard are called "TCG Enterprise" drives.

### Enclosed hard disk drive FDE\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=3 "Edit section: Enclosed hard disk drive FDE")\]

Within a standard [hard drive form factor](https://en.wikipedia.org/wiki/Harddrive#Form_factors "Harddrive") case the encryptor (BC), [key](https://en.wikipedia.org/wiki/Cryptographic_key "Cryptographic key") store and a smaller form factor, commercially available, hard disk drive is enclosed.

*   The enclosed hard disk drive's case can be [tamper-evident](https://en.wikipedia.org/wiki/Tamper-evident "Tamper-evident"), so when retrieved the user can be assured that the [data has not been compromised](https://en.wikipedia.org/wiki/Data_breach "Data breach").
*   The encryptors electronics including the [key](https://en.wikipedia.org/wiki/Cryptographic_key "Cryptographic key") store and integral hard drive (if it is [solid-state](https://en.wikipedia.org/wiki/Solid-state_drive "Solid-state drive")) can be protected by other [tamper respondent](https://en.wikipedia.org/wiki/Tamper_resistance "Tamper resistance") measures.
*   The key can be [purged](https://en.wikipedia.org/wiki/Crypto-shredding "Crypto-shredding"), allowing a user to prevent his [authentication parameters](https://en.wikipedia.org/wiki/Authentication_factors#Authentication_factors "Authentication factors") being used without destroying the encrypted data. Later the same [key](https://en.wikipedia.org/wiki/Cryptographic_key "Cryptographic key") can be re-loaded into the Enclosed hard disk drive FDE, to retrieve this data.
*   Tampering is not an issue for SEDs as they cannot be read without the decryption key, regardless of access to the internal electronics\[_[clarification needed](https://en.wikipedia.org/wiki/Wikipedia:Please_clarify "Wikipedia:Please clarify")_\].

For example: [Viasat UK (formerly Stonewood Electronics)](https://en.wikipedia.org/wiki/ViaSat "ViaSat") with their FlagStone and Eclypt[\[3\]](#cite_note-softpedia-3) drives or GuardDisk [\[4\]](#cite_note-4) with an [RFID](https://en.wikipedia.org/wiki/Radio-frequency_identification "Radio-frequency identification") token.

### Removable Hard Drive FDE\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=4 "Edit section: Removable Hard Drive FDE")\]

The Inserted [Hard Drive](https://en.wikipedia.org/wiki/Hard_disk_drive "Hard disk drive") FDE allows a standard [form factor](https://en.wikipedia.org/wiki/List_of_disk_drive_form_factors "List of disk drive form factors") [hard disk drive](https://en.wikipedia.org/wiki/Hard_disk_drive "Hard disk drive") to be inserted into it. The concept can be seen on [\[5\]](#cite_note-5)

*   This is an improvement on removing \[unencrypted\] [hard drives](https://en.wikipedia.org/wiki/Hard_disk_drive "Hard disk drive") from a [computer](https://en.wikipedia.org/wiki/Computer "Computer") and storing them in a [safe](https://en.wikipedia.org/wiki/Safe "Safe") when not in use.
*   This design can be used to encrypt multiple [drives](https://en.wikipedia.org/wiki/Hard_disk_drive "Hard disk drive") using the same [key](https://en.wikipedia.org/wiki/Cryptographic_key "Cryptographic key").
*   Generally they are not securely locked[\[6\]](#cite_note-6) so the drive's interface is open to attack.

### Chipset FDE\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=5 "Edit section: Chipset FDE")\]

The encryptor bridge and chipset (BC) is placed between the computer and the standard hard disk drive, encrypting every sector written to it.

[Intel](https://en.wikipedia.org/wiki/Intel "Intel") announced the release of the Danbury chipset[\[7\]](#cite_note-7) but has since abandoned this approach.\[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_\]

## Characteristics\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=6 "Edit section: Characteristics")\]

Hardware-based encryption when built into the drive or within the drive enclosure is notably transparent to the user. The drive, except for bootup authentication, operates just like any drive, with no degradation in performance. There is no complication or performance overhead, unlike [disk encryption software](https://en.wikipedia.org/wiki/Disk_encryption_software "Disk encryption software"), since all the encryption is invisible to the [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system") and the host [computer's processor](https://en.wikipedia.org/wiki/Central_processing_unit "Central processing unit").

The two main use cases are [Data at Rest](https://en.wikipedia.org/wiki/Data_at_Rest "Data at Rest") protection, and Cryptographic Disk Erasure.

For Data at Rest protection a computer or laptop is simply powered off. The disk now self-protects all the data on it. The data is safe because all of it, even the OS, is now encrypted, with a secure mode of [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Advanced Encryption Standard"), and locked from reading and writing. The drive requires an authentication code which can be as strong as 32 bytes (2^256) to unlock.

### Disk sanitisation\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=7 "Edit section: Disk sanitisation")\]

[Crypto-shredding](https://en.wikipedia.org/wiki/Crypto-shredding "Crypto-shredding") is the practice of 'deleting' data by (only) deleting or overwriting the encryption keys. When a cryptographic disk erasure (or crypto erase) command is given (with proper authentication credentials), the drive self-generates a new media encryption key and goes into a 'new drive' state.[\[8\]](#cite_note-8) Without the old key, the old data becomes irretrievable and therefore an efficient means of providing [disk sanitisation](https://en.wikipedia.org/wiki/Data_erasure "Data erasure") which can be a lengthy (and costly) process. For example, an unencrypted and unclassified computer hard drive that requires sanitising to conform with [Department of Defense](https://en.wikipedia.org/wiki/United_States_Department_of_Defense "United States Department of Defense") Standards must be overwritten 3+ times;[\[9\]](#cite_note-9) a one Terabyte Enterprise SATA3 disk would take many hours to complete this process. Although the use of faster [solid-state drives](https://en.wikipedia.org/wiki/Solid-state_drive "Solid-state drive") (SSD) technologies improves this situation, the take up by enterprise has so far been slow.[\[10\]](#cite_note-10) The problem will worsen as disk sizes increase every year. With encrypted drives a complete and secure data erasure action takes just a few milliseconds with a simple key change, so a drive can be safely repurposed very quickly. This sanitisation activity is protected in SEDs by the drive's own key management system built into the firmware in order to prevent accidental data erasure with confirmation passwords and secure authentications related to the original key required.

When [keys](https://en.wikipedia.org/wiki/Cryptographic_key "Cryptographic key") are self-generated randomly, generally there is no method to store a copy to allow [data recovery](https://en.wikipedia.org/wiki/Data_recovery "Data recovery"). In this case protecting this data from accidental loss or theft is achieved through a consistent and comprehensive data backup policy. The other method is for user-defined keys, for some Enclosed hard disk drive FDE,[\[11\]](#cite_note-11) to be generated externally and then loaded into the FDE.

### Protection from alternative boot methods\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=8 "Edit section: Protection from alternative boot methods")\]

Recent hardware models circumvents [booting](https://en.wikipedia.org/wiki/Booting "Booting") from other devices and allowing access by using a dual [Master Boot Record](https://en.wikipedia.org/wiki/Master_Boot_Record "Master Boot Record") (MBR) system whereby the MBR for the operating system and data files is all encrypted along with a special MBR which is required to boot the [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system"). In SEDs, all data requests are intercepted by their [firmware](https://en.wikipedia.org/wiki/Firmware "Firmware"), that does not allow decryption to take place unless the system has been [booted](https://en.wikipedia.org/wiki/Booting "Booting") from the special SED [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system") which then loads the [MBR](https://en.wikipedia.org/wiki/Master_boot_record "Master boot record") of the encrypted part of the drive. This works by having a separate [partition](https://en.wikipedia.org/wiki/Disk_partitioning "Disk partitioning"), hidden from view, which contains the proprietary [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system") for the encryption management system. This means no other boot methods will allow access to the drive.

### Vulnerabilities\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=9 "Edit section: Vulnerabilities")\]

Typically FDE, once unlocked, will remain unlocked as long as power is provided.[\[12\]](#cite_note-sed-attacks-12) Researchers at [Universität Erlangen-Nürnberg](https://en.wikipedia.org/wiki/University_of_Erlangen-Nuremberg "University of Erlangen-Nuremberg") have demonstrated a number of attacks based on moving the drive to another computer without cutting power.[\[12\]](#cite_note-sed-attacks-12) Additionally, it may be possible to reboot the computer into an attacker-controlled operating system without cutting power to the drive.

When a computer with a self-encrypting drive is put into [sleep mode](https://en.wikipedia.org/wiki/Sleep_mode "Sleep mode"), the drive is powered down, but the encryption password is retained in memory so that the drive can be quickly resumed without requesting the password. An attacker can take advantage of this to gain easier physical access to the drive, for instance, by inserting extension cables.[\[12\]](#cite_note-sed-attacks-12)

The firmware of the drive may be compromised[\[13\]](#cite_note-13)[\[14\]](#cite_note-14) and so any data that is sent to it may be at risk. Even if the data is encrypted on the physical medium of the drive, the fact that the firmware is controlled by a malicious third-party means that it can be decrypted by that third-party. If data is encrypted by the operating system, and it is sent in a scrambled form to the drive, then it would not matter if the firmware is malicious or not.

### Criticism\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=10 "Edit section: Criticism")\]

Hardware solutions have also been criticised for being poorly documented\[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_\]. Many aspects of how the encryption is done are not published by the vendor. This leaves the user with little possibility to judge the security of the product and potential attack methods. It also increases the risk of a [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in "Vendor lock-in").

In addition, implementing system wide hardware-based full disk encryption is prohibitive for many companies due to the high cost of replacing existing hardware. This makes migrating to hardware encryption technologies more difficult and would generally require a clear migration and central management solution for both hardware- and software-based [full disk encryption](https://en.wikipedia.org/wiki/Disk_encryption#Full_disk_encryption "Disk encryption") solutions.[\[15\]](#cite_note-15) however Enclosed hard disk drive FDE and Removable Hard Drive FDE are often installed on a single drive basis.

## See also\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=11 "Edit section: See also")\]

*   [Disk encryption hardware](https://en.wikipedia.org/wiki/Disk_encryption_hardware "Disk encryption hardware")
*   [Disk encryption software](https://en.wikipedia.org/wiki/Disk_encryption_software "Disk encryption software")
*   [Crypto-shredding](https://en.wikipedia.org/wiki/Crypto-shredding "Crypto-shredding")
*   [Opal Storage Specification](https://en.wikipedia.org/wiki/Opal_Storage_Specification "Opal Storage Specification")
*   [Yubikey](https://en.wikipedia.org/wiki/Yubikey "Yubikey")
*   [Full disk encryption](https://en.wikipedia.org/wiki/Full_disk_encryption "Full disk encryption")
*   [IBM Secure Blue](https://en.wikipedia.org/wiki/IBM_Secure_Blue "IBM Secure Blue")

## References\[[edit](https://en.wikipedia.org/w/index.php?title=Hardware-based_full_disk_encryption&action=edit&section=12 "Edit section: References")\]

1.  **[^](#cite_ref-1 "Jump up")** ["Trusted Computing Group Data Protection page"](https://www.webcitation.org/65fUDqdql?url=http://www.trustedcomputinggroup.org/solutions/data_protection). Trustedcomputinggroup.org. Archived from [the original](http://www.trustedcomputinggroup.org/solutions/data_protection) on 2012-02-23. Retrieved 2013-08-06.
2.  **[^](#cite_ref-2 "Jump up")** Skamarock, Anne (2020-02-21). ["Is Storage a commodity"](https://www.itworld.com/article/2799690/is-storage-a-commodity-.html). _ITWorld.com_. Network World. Retrieved 2020-05-22.
3.  **[^](#cite_ref-softpedia_3-0 "Jump up")** ["Softpedia on Eclypt Drive AES-256"](http://news.softpedia.com/news/Stonewood-039-s-Eclypt-Drive-the-AES-256-Data-Fortress-84632.shtml). News.softpedia.com. 2008-04-30. Retrieved 2013-08-06.
4.  **[^](#cite_ref-4 "Jump up")** ["Hardware Disk Encryption for the Masses, Finally!"](https://www.turbotas.co.uk/2003/07/30/hardware-disk-encryption-for-the-masses-finally/). _turbotas.co.uk_. Turbotas. 2003-05-30. Retrieved 2020-05-22.
5.  **[^](#cite_ref-5 "Jump up")** ["Removable Drives"](https://www.cru-inc.com/industries/removables/). _www.Cru-inc.com_. CRU. Retrieved 2020-05-15.
6.  **[^](#cite_ref-6 "Jump up")** ["Sapphire Cipher Snap-In"](https://www.addonics.com/products/ssna256eu.php). _Addonics.com_. Addonics. Retrieved 2020-05-15.
7.  **[^](#cite_ref-7 "Jump up")** Smith, Tony (2007-09-21). ["Next-gen Intel vPro platform to get hardware encryption"](https://www.theregister.co.uk/2007/09/21/intel_vpro_danbury/). _The Register_. Retrieved 2013-08-06.
8.  **[^](#cite_ref-8 "Jump up")** Trusted Computing Group (2010). ["10 Reasons to Buy Self-Encrypting Drives"](https://www.trustedcomputinggroup.org/wp-content/uploads/10-Reasons-to-Buy-SEDs_Sept.2010.pdf) (PDF). Trusted Computing Group. Retrieved 2018-06-06.
9.  **[^](#cite_ref-9 "Jump up")** [http://www-03.ibm.com/systems/resources/IBM\_Certified\_Secure\_Data\_Overwrite\_Service\_SB.pdf](http://www-03.ibm.com/systems/resources/IBM_Certified_Secure_Data_Overwrite_Service_SB.pdf)\[_[bare URL PDF](https://en.wikipedia.org/wiki/Wikipedia:Bare_URLs "Wikipedia:Bare URLs")_\]
10.  **[^](#cite_ref-10 "Jump up")** ["Slow on the Uptake"](https://docplayer.net/30164112-Ssd-story-slow-on-the-uptake.html). Retrieved 18 February 2021.
11.  **[^](#cite_ref-11 "Jump up")** ["Eclypt Core Encrypted Internal Hard Drive"](https://www.viasat.com/products/cybersecurity/data-at-rest-encryption/). _Viasat.com_. Viasat. 2020. Retrieved 2021-02-17.
12.  ^ [Jump up to: _**a**_](#cite_ref-sed-attacks_12-0) [_**b**_](#cite_ref-sed-attacks_12-1) [_**c**_](#cite_ref-sed-attacks_12-2) ["Hardware-based Full Disk Encryption (In)Security | IT-Sicherheitsinfrastrukturen (Informatik 1)"](https://www1.cs.fau.de/sed). .cs.fau.de. Retrieved 2013-08-06.
13.  **[^](#cite_ref-13 "Jump up")** Zetter, Kim (2015-02-22). ["How the NSA's Firmware Hacking Works and Why It's So Unsettling"](https://www.wired.com/2015/02/nsa-firmware-hacking/). _Wired_.
14.  **[^](#cite_ref-14 "Jump up")** Pauli, Darren (2015-02-17). ["Your hard drives were RIDDLED with NSA SPYWARE for YEARS"](https://www.theregister.co.uk/2015/02/17/kaspersky_labs_equation_group/). _The Register_.
15.  **[^](#cite_ref-15 "Jump up")** ["Closing the Legacy Gap"](https://archive.today/20120909075410/http://www.secude.com/html/?id=1375). Secude. February 21, 2008. Archived from [the original](http://www.secude.com/html/?id=1375) on September 9, 2012. Retrieved 2008-02-22.

