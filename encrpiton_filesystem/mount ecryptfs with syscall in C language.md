---
date: 2023-04-04 16:46
title: mount ecryptfs with syscall in C language
tags:
- ecryptfs
---

# mount ecryptfs with syscall in C language

[](https://stackoverflow.com/posts/28580033/timeline)

In fact, the key must be provide to the kernel before using mount syscall with the Key management facility (see man keyctl)

see : [https://bazaar.launchpad.net/~ecryptfs/ecryptfs/trunk/view/head:/src/utils/ecryptfs_add_passphrase.c](https://bazaar.launchpad.net/~ecryptfs/ecryptfs/trunk/view/head:/src/utils/ecryptfs_add_passphrase.c)

and

[https://bazaar.launchpad.net/~ecryptfs/ecryptfs/trunk/view/head:/src/libecryptfs/key_management.c](https://bazaar.launchpad.net/~ecryptfs/ecryptfs/trunk/view/head:/src/libecryptfs/key_management.c)

code sample :



```c
from_hex(salt, ECRYPTFS_DEFAULT_SALT_HEX, ECRYPTFS_SALT_SIZE);
r = ecryptfs_generate_passphrase_auth_tok(&auth_tok, auth_tok_sig_hex,
                    fekek, salt, passphrase);

r = ecryptfs_add_passphrase_key_to_keyring(auth_tok_sig_hex,
                     passphrase,
                     salt);
auth_tok_sig_hex[ECRYPTFS_SIG_SIZE_HEX] = '\0';

if ((rc_long = keyctl(KEYCTL_LINK, KEY_SPEC_USER_KEYRING,
        KEY_SPEC_SESSION_KEYRING))) 
{
  syslog(LOG_ERR, "Error attempting to link the user session "
     "keyring into the session keyring\n");
}
```



```c
r = mount(source, source, "ecryptfs", MS_MGC_VAL, opt);
```

[Share](https://stackoverflow.com/a/28580033 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/28580033/edit)

Follow



## How to use ecryptfs with a non-home directory

Asked 8 years, 2 months ago



Viewed 16k times

15

[](https://askubuntu.com/posts/574110/timeline)

I would like to use ecryptfs to encrypt a random directory (i.e., not my home directory or a subdirectory thereof, principally due to disk space limitations on my home partition) and mount that directory when I log into my account. I can't see how to do this or even if it is really possible with the existing software. I have seen posts that provide vague suggestions (e.g., to use `mount.ecryptfs_private` with the ALIAS option), but I have yet to find simple, step-by-step instructions on how to do this. Would someone be able to provide these instructions or direct me to where to find them?


asked Jan 15, 2015 at 15:43


**34111 gold badge33 silver badges55 bronze badges**

-   2
    
    This is the simplest method you will find: [wiki.archlinux.org/index.php/…](https://wiki.archlinux.org/index.php/ECryptfs#Encrypting_a_data_directory) 
    
    – [Rinzwind](https://askubuntu.com/users/15811/rinzwind "289,391 reputation")
    
     [Jan 15, 2015 at 15:48](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792041_574110)
    
-   1
    
    My problem with this solution is that I already have an encrypted home directory. Looking at the code for `ecryptsfs-setup-private`, I am not certain what will happen if you already have an encrypted home and don't really want to try for fear of it doing bad things. 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 15, 2015 at 20:02](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792196_574110)
    
-   1
    
    Do you understand why a double seems so hard? It would seem to me that the logical thing would be to build a generic system for creating encrypted storage and folders and automounting them, and then build upon that a system to do the home directory, but this software seems to have been written with a lot of things hardwired. Storage has to be .Private, directory must be Private, etc. 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 15, 2015 at 20:16](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792203_574110)
    
-   2
    
    Using a password-protected zip file is not cryptographically secure. An similar approach is to use `gpg`. Say you want to securely store the folder `mydata` then you could use `tar -c mydata | gpg --symmetric > mydata.tar.gpg && rm -rf mydata` to store your data and `gpg --decrypt mydata.tar.gpg | tar -x` to restore your data. You can easily use your private/public key to protected your data which seems advisable. Only small amounts of data should be stored with this method. @Rinzwind 
    
    – [Arne L.](https://askubuntu.com/users/191359/arne-l "101 reputation")
    
     [Aug 6, 2015 at 9:20](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment945781_574110)
    
-   1
    
    Kind of defeats the purpose of encrypting the data if you `rm -fR mydata`. That data could be recovered from the disk long after you "delete" it. I'm not sure how secure it is, but I'd lean towards a "recursive" shred instead: `find mydata -type f -exec shred -uz -- {} \;`. Note, shredding is only effective on non-journaled file systems and certain device types.. Where it matters one should research the best way. I don't think this is a safe practice: encrypting and decrypting the archive like this. Odds are it will be ineffective. 
    
    – [bambams](https://askubuntu.com/users/504562/bambams "101 reputation")
    
     [Oct 18, 2017 at 14:29](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment1546609_574110)
    

[Show **1** more comment](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory# "Expand to show all comments on this post")

11
[](https://askubuntu.com/posts/574425/timeline)

You're only looking at the the super-easy scripts like `ecryptsfs-setup-private` and `ecryptsfs-mount-private`, they use the more "generic" tools you seem to be looking for: `mount.ecryptfs` and `ecryptfs-add-passphrase`. See their `man` pages for more info.

And the link Rinzwind posted has all the info you need, further down the page under [Manual Setup](https://wiki.archlinux.org/index.php/ECryptfs#Manual_setup). They're quite long, but the _very very short version_ would be:

---

#### The "Manual Setup" way (archlinux wiki)

First choose an ALIAS as you like. Through this section, ALIAS will be secret. Create the required directories/files:

```au
$ mkdir ~/.secret ~/secret ~/.ecryptfs
$ touch ~/.ecryptfs/secret.conf ~/.ecryptfs/secret.sig
```

The `~/.secret` directory will hold the encrypted data. The `~/secret` directory is the mount point where `~/.secret` will be mounted as an ecryptfs filesystem.

[Now create the **actual mount passphrase** (the easy scripts will pick a pseudo-random 32 characters from `/dev/urandom`), make it a good one]

```
$ echo "$HOME/.secret $HOME/secret ecryptfs" > ~/.ecryptfs/secret.conf
$ ecryptfs-add-passphrase
Passphrase: 
Inserted auth tok with sig [78c6f0645fe62da0] into the user session keyring
```

Write the output signature (ecryptfs_sig) from the previous command to ~/.ecryptfs/secret.sig:

```
$ echo 78c6f0645fe62da0 > ~/.ecryptfs/secret.sig
```

-   A second passphrase for filename encryption may be used. If you choose so, add it to the keyring:
    
    ```
    $ ecryptfs-add-passphrase
    Passphrase: 
    Inserted auth tok with sig [326a6d3e2a5d444a] into the user session keyring
    ```
    
    If you run the command above, append its output signature (ecryptfs_fnek_sig) to ~/.ecryptfs/secret.sig:
    
    ```
    $ echo 326a6d3e2a5d444a >> ~/.ecryptfs/secret.sig
    ```
    

Finally, to mount ~/.secret on ~/secret:

```
$ mount.ecryptfs_private secret
```

To unmount ~/.secret:

```
$ umount.ecryptfs_private secret
```

---

-   **Or** you could really get your hands dirty yourself and follow the [Without ecryptfs-utils](https://wiki.archlinux.org/index.php/ECryptfs#Without_ecryptfs-utils) directions.
    
-   **Or** if you already looked at the easy scripts `ecryptsfs-setup-private` & `ecryptsfs-mount-private`, you might be able to copy those and edit them to point to your preferred directories, with a little bit of skill & patience.
    
-   **Or** just store the passphrase(s) yourself somehow (securely preferably) and do like the `man ecryptfs` page's example (must read the man pages):
    
    ```
    The following command will layover mount eCryptfs on /secret with a passphrase
    contained in a file stored on secure media mounted at /mnt/usb/.
    
    mount  -t  ecryptfs -o key=passphrase:passphrase_passwd_file=/mnt/usb/file.txt /secret /secret
    
    Where file.txt contains the contents "passphrase_passwd=[passphrase]".
    ```
    

---

### Aside about encrypted home folders and an encrypted folder inside home - nested eCryptfs folders

And, an encrypted home folder normally stores files in `/home/.ecryptfs/user/`, while an encrypted Private folder has files inside your own home folder. You can **not** use both at the same time, eCryptfs will not do nested encrypted folders. But having an encrypted home, and encrypted folders outside of your home is ok.

-   I just tried creating a new user with an encrypted home `sudo adduser --encrypt-home jack`
    
    It created a `/home/.ecryptfs/` folder, with:
    
    -   `/home/.ecryptfs/jack/.ecryptfs/` - wrapped passphrase & config files to automount jack's home on login
    -   `/home/.ecryptfs/jack/.Private/` - actual encrypted home files, mounted to `/home/jack/` when logged in.
        
        And also the `/home/jack/` folder, but it contained a link that _stays_ there whether logged in or not:
        
        `/home/jack/.ecryptfs/ -> /home/.ecryptfs/jack/.ecryptfs`
        
    -   Next I logged in as jack, but the link was still there, so trying to run `ecryptfs-setup-private` caused it to look in `/home/jack/.ecryptfs/` but really see the existing files in `/home/.ecryptfs/jack/.ecryptfs` so it failed to create another password file & fail with `ERROR: wrapped-passphrase file already exists, use --force to overwrite.`
        
        Trying the "ALIAS" steps above, using a .secret folder **_inside the encrypted home_** failed, with these errors:  
        `Mount on filesystem of type eCryptfs explicitly disallowed due to known incompatibilities`  
        `Reading sb failed; rc = [-22]`
        
        [_"Nesting encrypted directories inside of encrypted directories is not supported with eCryptfs. Sorry."_](https://askubuntu.com/a/279932) - eCryptfs author & maintainer
        
    -   Changing the ALIAS folder outside of jack's home, trying `/tmp/.secret/` & `/tmp/secret/` **works**. BUT if jack log's out the new encrypted folder will **stay mounted**, so you have to unmount it (`umount.ecryptfs_private secret`).
        


8,52844 gold badges3232 silver badges5151 bronze badges

-   1
    
    Thanks for the simplified response. One question though: does this take care of the auto-mount problem? Sorry for being dense about this, but the webpage you indicated and man pages really aren't particularly easy to understand regarding setting up auto-mount for a non-standard situation. As I already have an encrypted home directory, I already have $HOME/.ecryptfs/auto-mount and wrapped-passphrase, but the first is empty and the second already has something in it. It isn't entirely clear to me how to add the new passphrase and instruct it to auto-mount the directory. 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 16, 2015 at 16:32](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792691_574425) 
    
-   Updated with a little testing, and this guy [ragingpenguin.com/2012/12/…](http://www.ragingpenguin.com/2012/12/17-setting-up-an-encrypted-directory-with-ecryptfs/index.html) apparently had some luck following the arch Wiki guide on auto-mounting with PAM, but he's pretty tight-lipped about it 
    
    – [Xen2050](https://askubuntu.com/users/129271/xen2050 "8,528 reputation")
    
     [Jan 16, 2015 at 18:34](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792776_574425)
    
-   Thanks for your efforts. I am not interested in nesting the ecryptfs directories, but auto-mounting a second directory in a second place. I will try getting it to work when I have a bit of time, but it definitely isn't very clear... 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 21, 2015 at 19:51](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment795467_574425)
    
-   You can trim it down to one `mount` line, don't need a `.conf` file or adding keys, just read the `man ecryptfs` pages for the options available. Then throw a "run at login" file in `/home/user/.config/autostart/`. But the security of the passphrase could be at risk if stored improperly 
    
    – [Xen2050](https://askubuntu.com/users/129271/xen2050 "8,528 reputation")
    
     [Jan 22, 2015 at 0:38](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment795601_574425)
    
-   The thing is that ecryptfs has a nice system of wrapping encryption passwords inside a wrapper that is opened with a login password. This provides added protection for the ecryptfs password, which would be nice to have. I don't think putting something in .config/autostart will allow this unless I am misunderstanding what I am supposed to put in the script. 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 23, 2015 at 14:36](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment796525_574425)
    

[Show **1** more comment](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory# "Expand to show all comments on this post")

0

[](https://askubuntu.com/posts/773262/timeline)

If you want to use it like encfs you can do it with the following entry in `/etc/fstab`
~~~
/tmp/.geheim /tmp/geheim ecryptfs rw,no_sig_cache,ecryptfs_fnek_sig=1f7aefb9e239099f,ecryptfs_cipher=aes,ecryptfs_key_bytes=32,ecryptfs_enable_filename_crypto=y,ecryptfs_passthrough=n,passphrase_passwd=geheimpw,user,noauto 0 0

~~~

geheim is the german-word for secret, but ensures that it's not a keyword. You must create the directories first. The first-time you should leave `ecryptfs_fnek_sig=1f7aefb9e239099f` away. Then `mount /tmp/geheim` will show you the correct value.

You can store the password at other place and set more sophisticated options. You will find all options in `man ecryptfs`.


Follow

answered May 16, 2016 at 14:57


24811 silver badge77 bronze badges


```
ecryptfs /destination/to/encrypted/storage /destination/to/seeing/unencrypted/data
```

e.g:

```
ecryptfs /home/$USER/EFILES /home/$USER/Downloads/RANDOMDIRECTORY
```

use the commmand above to create and mount encrypted system where files saved in RANDOMDIRECTORY are encrypted and saved to EFILES.

additional notes. make sure RANDOMDIRECTORY is empty when you begin. Once you run the command above, and the system is mounted and ready to go, any files that you save in RANDOMDIRECTORY will be encrypted in to EFILES if the system is mounted. For a quick moount/unmount you can either create a bash script, and run it through an app shortcut, or create an alias command for quick mounting.

I've been using this for more than a year now.

EDIT: went home to confirm, thy command is not ecryptfs. its encfs i.e.

```
encfs /destination/encrypted /destination/unencrypted
```

sorry about that. With this you will have to install a new program tho (probably)

111 bronze badge

-   1
    
    I typed in the command `ecryptfs` and it responds command not found. `man ecryptfs` brings up the man page for `mount -t ecryptfs`, but doesn't really explain how to create such an encrypted filesystem. 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 15, 2015 at 19:52](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792190_574134)
    
-   Note that I am using Ubuntu 14.04 LTS, so my installation is relatively recent. Is the `ecryptfs` command a recent addition? 
    
    – [user3004015](https://askubuntu.com/users/369037/user3004015 "341 reputation")
    
     [Jan 15, 2015 at 19:59](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment792194_574134)
    
-   encfs is not the same as ecryptfs. These are two different. 
    
    – [Diagon](https://askubuntu.com/users/242522/diagon "420 reputation")
    
     [Mar 5, 2017 at 23:16](https://askubuntu.com/questions/574110/how-to-use-ecryptfs-with-a-non-home-directory#comment1391249_574134)
    