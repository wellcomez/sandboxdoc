---
date: 2023-05-23 16:59
title: Full-Disk Encryption
tags:
- Encryption
- fdeA
- full-disk-encrption 
---



# Full-Disk Encryption

bookmark_border

**Caution:** Only devices that launched with Android 9 or lower can use full-disk encryption. Devices that launched with Android 10 or higher must use [file-based encryption](https://source.android.com/docs/security/features/encryption/file-based) instead. Android 10-12 support full-disk encryption only for devices that upgraded from a lower Android version. Android 13 removes support for full-disk encryption entirely.

Full-disk encryption is the process of encoding all user data on an Android device using an encrypted key. Once a device is encrypted, all user-created data is automatically encrypted before committing it to disk and all reads automatically decrypt data before returning it to the calling process.

Full-disk encryption was introduced to Android in 4.4, but Android 5.0 introduced these new features:

-   Created fast encryption, which only encrypts used blocks on the data partition to avoid first boot taking a long time. Only ext4 and f2fs filesystems currently support fast encryption.
-   Added the [`forceencrypt` fstab flag](https://source.android.com/docs/core/storage/config) to encrypt on first boot.
-   Added support for patterns and encryption without a password.
-   Added hardware-backed storage of the encryption key using Trusted Execution Environment’s (TEE) signing capability (such as in a TrustZone). See [Storing the encrypted key](https://source.android.com/docs/security/features/encryption/full-disk#storing_the_encrypted_key) for more details.

**Caution:** Devices upgraded to Android 5.0 and then encrypted may be returned to an unencrypted state by factory data reset. New Android 5.0 devices encrypted at first boot cannot be returned to an unencrypted state.

## How Android full-disk encryption works

Android full-disk encryption is based on `dm-crypt`, which is a kernel feature that works at the block device layer. Because of this, encryption works with Embedded MultiMediaCard **(**eMMC) and similar flash devices that present themselves to the kernel as block devices. Encryption is not possible with YAFFS, which talks directly to a raw NAND flash chip.

The encryption algorithm is 128 Advanced Encryption Standard (AES) with cipher-block chaining (CBC) and ESSIV:SHA256. The master key is encrypted with 128-bit AES via calls to the OpenSSL library. You must use 128 bits or more for the key (with 256 being optional).

**Note:** OEMs can use 128-bit or higher to encrypt the master key.

In the Android 5.0 release, there are four kinds of encryption states:

-   default
-   PIN
-   password
-   pattern

Upon first boot, the device creates a randomly generated 128-bit master key and then hashes it with a default password and stored salt. The default password is: "default_password" However, the resultant hash is also signed through a TEE (such as TrustZone), which uses a hash of the signature to encrypt the master key.

You can find the default password defined in the Android Open Source Project [cryptfs.cpp](https://android.googlesource.com/platform/system/vold/+/master/cryptfs.cpp) file.

When the user sets the PIN/pass or password on the device, only the 128-bit key is re-encrypted and stored. (ie. user PIN/pass/pattern changes do NOT cause re-encryption of userdata.) Note that [managed device](http://developer.android.com/guide/topics/admin/device-admin.html) may be subject to PIN, pattern, or password restrictions.

Encryption is managed by `init` and `vold`. `init` calls `vold`, and vold sets properties to trigger events in init. Other parts of the system also look at the properties to conduct tasks such as report status, ask for a password, or prompt to factory reset in the case of a fatal error. To invoke encryption features in `vold`, the system uses the command line tool `vdc`’s `cryptfs` commands: `checkpw`, `restart`, `enablecrypto`, `changepw`, `cryptocomplete`, `verifypw`, `setfield`, `getfield`, `mountdefaultencrypted`, `getpwtype`, `getpw`, and `clearpw`.

In order to encrypt, decrypt or wipe `/data`, `/data` must not be mounted. However, in order to show any user interface (UI), the framework must start and the framework requires `/data` to run. To resolve this conundrum, a temporary filesystem is mounted on `/data`. This allows Android to prompt for passwords, show progress, or suggest a data wipe as needed. It does impose the limitation that in order to switch from the temporary filesystem to the true `/data` filesystem, the system must stop every process with open files on the temporary filesystem and restart those processes on the real `/data` filesystem. To do this, all services must be in one of three groups: `core`, `main`, and `late_start`.

-   `core`: Never shut down after starting.
-   `main`: Shut down and then restart after the disk password is entered.
-   `late_start`: Does not start until after `/data` has been decrypted and mounted.

To trigger these actions, the `vold.decrypt` property is set to [various strings](https://android.googlesource.com/platform/system/vold/+/master/cryptfs.c). To kill and restart services, the `init` commands are:

-   `class_reset`: Stops a service but allows it to be restarted with class_start.
-   `class_start`: Restarts a service.
-   `class_stop`: Stops a service and adds a `SVC_DISABLED` flag. Stopped services do not respond to `class_start`.

## Flows

There are four flows for an encrypted device. A device is encrypted just once and then follows a normal boot flow.

-   Encrypt a previously unencrypted device:
    -   Encrypt a new device with `forceencrypt`: Mandatory encryption at first boot (starting in Android L).
    -   Encrypt an existing device: User-initiated encryption (Android K and earlier).
-   Boot an encrypted device:
    -   Starting an encrypted device with no password: Booting an encrypted device that has no set password (relevant for devices running Android 5.0 and later).
    -   Starting an encrypted device with a password: Booting an encrypted device that has a set password.

In addition to these flows, the device can also fail to encrypt `/data`. Each of the flows are explained in detail below.

### Encrypt a new device with forceencrypt

This is the normal first boot for an Android 5.0 device.

1.  **Detect unencrypted filesystem with `forceencrypt` flag**
    
    `/data` is not encrypted but needs to be because `forceencrypt` mandates it. Unmount `/data`.
    
2.  **Start encrypting `/data`**
    
    `vold.decrypt = "trigger_encryption"` triggers `init.rc`, which will cause `vold` to encrypt `/data` with no password. (None is set because this should be a new device.)
    
3.  **Mount tmpfs**
    
    `vold` mounts a tmpfs `/data` (using the tmpfs options from `ro.crypto.tmpfs_options`) and sets the property `vold.encrypt_progress` to 0. `vold` prepares the tmpfs `/data` for booting an encrypted system and sets the property `vold.decrypt` to: `trigger_restart_min_framework`
    
4.  **Bring up framework to show progress**
    
    Because the device has virtually no data to encrypt, the progress bar will often not actually appear because encryption happens so quickly. See [Encrypt an existing device](https://source.android.com/docs/security/features/encryption/full-disk#encrypt_an_existing_device) for more details about the progress UI.
    
5.  **When `/data` is encrypted, take down the framework**
    
    `vold` sets `vold.decrypt` to `trigger_default_encryption` which starts the `defaultcrypto` service. (This starts the flow below for mounting a default encrypted userdata.) `trigger_default_encryption` checks the encryption type to see if `/data` is encrypted with or without a password. Because Android 5.0 devices are encrypted on first boot, there should be no password set; therefore we decrypt and mount `/data`.
    
6.  **Mount `/data`**
    
    `init` then mounts `/data` on a tmpfs RAMDisk using parameters it picks up from `ro.crypto.tmpfs_options`, which is set in `init.rc`.
    
7.  **Start framework**
    
    `vold` sets `vold.decrypt` to `trigger_restart_framework`, which continues the usual boot process.
    

### Encrypt an existing device

This is what happens when you encrypt an unencrypted Android K or earlier device that has been migrated to L.

This process is user-initiated and is referred to as “inplace encryption” in the code. When a user selects to encrypt a device, the UI makes sure the battery is fully charged and the AC adapter is plugged in so there is enough power to finish the encryption process.

**Warning:** If the device runs out of power and shuts down before it has finished encrypting, file data is left in a partially encrypted state. The device must be factory reset and all data is lost.

To enable inplace encryption, `vold` starts a loop to read each sector of the real block device and then write it to the crypto block device. `vold` checks to see if a sector is in use before reading and writing it, which makes encryption much faster on a new device that has little to no data.

**State of device**: Set `ro.crypto.state = "unencrypted"` and execute the `on nonencrypted` `init` trigger to continue booting.

1.  **Check password**
    
    The UI calls `vold` with the command `cryptfs enablecrypto inplace` where `passwd` is the user's lock screen password.
    
2.  **Take down the framework**
    
    `vold` checks for errors, returns -1 if it can't encrypt, and prints a reason in the log. If it can encrypt, it sets the property `vold.decrypt` to `trigger_shutdown_framework`. This causes `init.rc` to stop services in the classes `late_start` and `main`.
    
3.  **Create a crypto footer**
4.  **Create a breadcrumb file**
5.  **Reboot**
6.  **Detect breadcrumb file**
7.  **Start encrypting `/data`**
    
    `vold` then sets up the crypto mapping, which creates a virtual crypto block device that maps onto the real block device but encrypts each sector as it is written, and decrypts each sector as it is read. `vold` then creates and writes out the crypto metadata.
    
8.  **While it’s encrypting, mount tmpfs**
    
    `vold` mounts a tmpfs `/data` (using the tmpfs options from `ro.crypto.tmpfs_options`) and sets the property `vold.encrypt_progress` to 0. `vold` prepares the tmpfs `/data` for booting an encrypted system and sets the property `vold.decrypt` to: `trigger_restart_min_framework`
    
9.  **Bring up framework to show progress**
    
    `trigger_restart_min_framework` causes `init.rc` to start the `main` class of services. When the framework sees that `vold.encrypt_progress` is set to 0, it brings up the progress bar UI, which queries that property every five seconds and updates a progress bar. The encryption loop updates `vold.encrypt_progress` every time it encrypts another percent of the partition.
    
10.  **When `/data` is encrypted, update the crypto footer**
    
    When `/data` is successfully encrypted, `vold` clears the flag `ENCRYPTION_IN_PROGRESS` in the metadata.
    
    When the device is successfully unlocked, the password is then used to encrypt the master key and the crypto footer is updated.
    
    If the reboot fails for some reason, `vold` sets the property `vold.encrypt_progress` to `error_reboot_failed` and the UI should display a message asking the user to press a button to reboot. This is not expected to ever occur.
    

### Starting an encrypted device with default encryption

This is what happens when you boot up an encrypted device with no password. Because Android 5.0 devices are encrypted on first boot, there should be no set password and therefore this is the _default encryption_ state.

1.  **Detect encrypted `/data` with no password**
    
    Detect that the Android device is encrypted because `/data` cannot be mounted and one of the flags `encryptable` or `forceencrypt` is set.
    
    `vold` sets `vold.decrypt` to `trigger_default_encryption`, which starts the `defaultcrypto` service. `trigger_default_encryption` checks the encryption type to see if `/data` is encrypted with or without a password.
    
2.  **Decrypt /data**
    
    Creates the `dm-crypt` device over the block device so the device is ready for use.
    
3.  **Mount /data**
    
    `vold` then mounts the decrypted real `/data` partition and then prepares the new partition. It sets the property `vold.post_fs_data_done` to 0 and then sets `vold.decrypt` to `trigger_post_fs_data`. This causes `init.rc` to run its `post-fs-data` commands. They will create any necessary directories or links and then set `vold.post_fs_data_done` to 1.
    
    Once `vold` sees the 1 in that property, it sets the property `vold.decrypt` to: `trigger_restart_framework.` This causes `init.rc` to start services in class `main` again and also start services in class `late_start` for the first time since boot.
    
4.  **Start framework**
    
    Now the framework boots all its services using the decrypted `/data`, and the system is ready for use.
    

### Starting an encrypted device without default encryption

This is what happens when you boot up an encrypted device that has a set password. The device’s password can be a pin, pattern, or password.

1.  **Detect encrypted device with a password**
    
    Detect that the Android device is encrypted because the flag `ro.crypto.state = "encrypted"`
    
    `vold` sets `vold.decrypt` to `trigger_restart_min_framework` because `/data` is encrypted with a password.
    
2.  **Mount tmpfs**
    
    `init` sets five properties to save the initial mount options given for `/data` with parameters passed from `init.rc`. `vold` uses these properties to set up the crypto mapping:
    
    1.  `ro.crypto.fs_type`
    2.  `ro.crypto.fs_real_blkdev`
    3.  `ro.crypto.fs_mnt_point`
    4.  `ro.crypto.fs_options`
    5.  `ro.crypto.fs_flags` (ASCII 8-digit hex number preceded by 0x)
3.  **Start framework to prompt for password**
    
    The framework starts up and sees that `vold.decrypt` is set to `trigger_restart_min_framework`. This tells the framework that it is booting on a tmpfs `/data` disk and it needs to get the user password.
    
    First, however, it needs to make sure that the disk was properly encrypted. It sends the command `cryptfs cryptocomplete` to `vold`. `vold` returns 0 if encryption was completed successfully, -1 on internal error, or -2 if encryption was not completed successfully. `vold` determines this by looking in the crypto metadata for the `CRYPTO_ENCRYPTION_IN_PROGRESS` flag. If it's set, the encryption process was interrupted, and there is no usable data on the device. If `vold` returns an error, the UI should display a message to the user to reboot and factory reset the device, and give the user a button to press to do so.
    
4.  **Decrypt data with password**
    
    Once `cryptfs cryptocomplete` is successful, the framework displays a UI asking for the disk password. The UI checks the password by sending the command `cryptfs checkpw` to `vold`. If the password is correct (which is determined by successfully mounting the decrypted `/data` at a temporary location, then unmounting it), `vold` saves the name of the decrypted block device in the property `ro.crypto.fs_crypto_blkdev` and returns status 0 to the UI. If the password is incorrect, it returns -1 to the UI.
    
5.  **Stop framework**
    
    The UI puts up a crypto boot graphic and then calls `vold` with the command `cryptfs restart`. `vold` sets the property `vold.decrypt` to `trigger_reset_main`, which causes `init.rc` to do `class_reset main`. This stops all services in the main class, which allows the tmpfs `/data` to be unmounted.
    
6.  **Mount `/data`**
    
    `vold` then mounts the decrypted real `/data` partition and prepares the new partition (which may never have been prepared if it was encrypted with the wipe option, which is not supported on first release). It sets the property `vold.post_fs_data_done` to 0 and then sets `vold.decrypt` to `trigger_post_fs_data`. This causes `init.rc` to run its `post-fs-data` commands. They will create any necessary directories or links and then set `vold.post_fs_data_done` to 1. Once `vold` sees the 1 in that property, it sets the property `vold.decrypt` to `trigger_restart_framework`. This causes `init.rc` to start services in class `main` again and also start services in class `late_start` for the first time since boot.
    
7.  **Start full framework**
    
    Now the framework boots all its services using the decrypted `/data` filesystem, and the system is ready for use.
    

### Failure

A device that fails to decrypt might be awry for a few reasons. The device starts with the normal series of steps to boot:

1.  Detect encrypted device with a password
2.  Mount tmpfs
3.  Start framework to prompt for password

But after the framework opens, the device can encounter some errors:

-   Password matches but cannot decrypt data
-   User enters wrong password 30 times

If these errors are not resolved, **prompt user to factory wipe**:

If `vold` detects an error during the encryption process, and if no data has been destroyed yet and the framework is up, `vold` sets the property `vold.encrypt_progress` to `error_not_encrypted`. The UI prompts the user to reboot and alerts them the encryption process never started. If the error occurs after the framework has been torn down, but before the progress bar UI is up, `vold` will reboot the system. If the reboot fails, it sets `vold.encrypt_progress` to `error_shutting_down` and returns -1; but there will not be anything to catch the error. This is not expected to happen.

If `vold` detects an error during the encryption process, it sets `vold.encrypt_progress` to `error_partially_encrypted` and returns -1. The UI should then display a message saying the encryption failed and provide a button for the user to factory reset the device.

## Storing the encrypted key

The encrypted key is stored in the crypto metadata. Hardware backing is implemented by using Trusted Execution Environment’s (TEE) signing capability. Previously, we encrypted the master key with a key generated by applying scrypt to the user's password and the stored salt. In order to make the key resilient against off-box attacks, we extend this algorithm by signing the resultant key with a stored TEE key. The resultant signature is then turned into an appropriate length key by one more application of scrypt. This key is then used to encrypt and decrypt the master key. To store this key:

1.  Generate random 16-byte disk encryption key (DEK) and 16-byte salt.
2.  Apply scrypt to the user password and the salt to produce 32-byte intermediate key 1 (IK1).
3.  Pad IK1 with zero bytes to the size of the hardware-bound private key (HBK). Specifically, we pad as: 00 || IK1 || 00..00; one zero byte, 32 IK1 bytes, 223 zero bytes.
4.  Sign padded IK1 with HBK to produce 256-byte IK2.
5.  Apply scrypt to IK2 and salt (same salt as step 2) to produce 32-byte IK3.
6.  Use the first 16 bytes of IK3 as KEK and the last 16 bytes as IV.
7.  Encrypt DEK with AES_CBC, with key KEK, and initialization vector IV.

## Changing the password

When a user elects to change or remove their password in settings, the UI sends the command `cryptfs changepw` to `vold`, and `vold` re-encrypts the disk master key with the new password.

## Encryption properties

`vold` and `init` communicate with each other by setting properties. Here is a list of available properties for encryption.

### Vold properties

Property

Description

`vold.decrypt trigger_encryption`

Encrypt the drive with no password.

`vold.decrypt trigger_default_encryption`

Check the drive to see if it is encrypted with no password. If it is, decrypt and mount it, else set `vold.decrypt` to trigger_restart_min_framework.

`vold.decrypt trigger_reset_main`

Set by vold to shutdown the UI asking for the disk password.

`vold.decrypt trigger_post_fs_data`

Set by vold to prep `/data` with necessary directories, et al.

`vold.decrypt trigger_restart_framework`

Set by vold to start the real framework and all services.

`vold.decrypt trigger_shutdown_framework`

Set by vold to shutdown the full framework to start encryption.

`vold.decrypt trigger_restart_min_framework`

Set by vold to start the progress bar UI for encryption or prompt for password, depending on the value of `ro.crypto.state`.

`vold.encrypt_progress`

When the framework starts up, if this property is set, enter the progress bar UI mode.

`vold.encrypt_progress 0 to 100`

The progress bar UI should display the percentage value set.

`vold.encrypt_progress error_partially_encrypted`

The progress bar UI should display a message that the encryption failed, and give the user an option to factory reset the device.

`vold.encrypt_progress error_reboot_failed`

The progress bar UI should display a message saying encryption completed, and give the user a button to reboot the device. This error is not expected to happen.

`vold.encrypt_progress error_not_encrypted`

The progress bar UI should display a message saying an error occurred, no data was encrypted or lost, and give the user a button to reboot the system.

`vold.encrypt_progress error_shutting_down`

The progress bar UI is not running, so it is unclear who will respond to this error. And it should never happen anyway.

`vold.post_fs_data_done 0`

Set by `vold` just before setting `vold.decrypt` to `trigger_post_fs_data`.

`vold.post_fs_data_done 1`

Set by `init.rc` or `init.rc` just after finishing the task `post-fs-data`.

### init properties

Property

Description

`ro.crypto.fs_crypto_blkdev`

Set by the `vold` command `checkpw` for later use by the `vold` command `restart`.

`ro.crypto.state unencrypted`

Set by `init` to say this system is running with an unencrypted `/data ro.crypto.state encrypted`. Set by `init` to say this system is running with an encrypted `/data`.

`ro.crypto.fs_type   ro.crypto.fs_real_blkdev   ro.crypto.fs_mnt_point   ro.crypto.fs_options   ro.crypto.fs_flags   `

These five properties are set by `init` when it tries to mount `/data` with parameters passed in from `init.rc`. `vold` uses these to setup the crypto mapping.

`ro.crypto.tmpfs_options`

Set by `init.rc` with the options init should use when mounting the tmpfs `/data` filesystem.

## Init actions

on post-fs-data
on nonencrypted
on property:vold.decrypt=trigger_reset_main
on property:vold.decrypt=trigger_post_fs_data
on property:vold.decrypt=trigger_restart_min_framework
on property:vold.decrypt=trigger_restart_framework
on property:vold.decrypt=trigger_shutdown_framework
on property:vold.decrypt=trigger_encryption
on property:vold.decrypt=trigger_default_encryption

Was this helpful?

Content and code samples on this page are subject to the licenses described in the [Content License](https://source.android.com/license). Java and OpenJDK are trademarks or registered trademarks of Oracle and/or its affiliates.

Last updated 2023-01-23 UTC.

-   ### BUILD
    
    -   [Android repository](https://android.googlesource.com/)
    -   [Requirements](https://source.android.com/source/requirements)
    -   [Downloading](https://source.android.com/source/downloading)
    -   [Preview binaries](https://developers.google.com/android/blobs-preview/)
    -   [Factory images](https://developers.google.com/android/images/)
    -   [Driver binaries](https://developers.google.com/android/drivers/)
-