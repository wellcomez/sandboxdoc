---
date: 2023-03-24 11:34
title: Data-at-rest encryption
tags:
- Encryption
- Data-at-rest
- ecryptfs
- filesystem
---

# Data-at-rest encryption
https://wiki.archlinux.org/title/Data-at-rest_encryption


This article discusses[[data-at-rest]] [data-at-rest](https://en.wikipedia.org/wiki/Data_at_rest "wikipedia:Data at rest") [encryption](https://en.wikipedia.org/wiki/Disk_encryption "wikipedia:Disk encryption") software, which on-the-fly encrypts / decrypts data written to / read from a [block device](https://wiki.archlinux.org/title/Block_device "Block device"), [disk partition](https://wiki.archlinux.org/title/Disk_partition "Disk partition") or directory. Examples for block devices are hard drives, flash drives and DVDs.

Data-at-rest encryption should only be viewed as an adjunct to the existing security mechanisms of the operating system - focused on securing physical access, while relying on _other_ parts of the system to provide things like network security and user-based access control.

## Why use encryption?

Data-at-rest encryption ensures that files are always stored on disk in an encrypted form. The files only become available to the operating system and applications in readable form while the system is running and unlocked by a trusted user (data [in use](https://en.wikipedia.org/wiki/Data_in_use "wikipedia:Data in use") or [in transit](https://en.wikipedia.org/wiki/Data_in_transit "wikipedia:Data in transit")). An unauthorized person looking at the disk contents directly, will only find garbled random-looking data instead of the actual files.

For example, this can prevent unauthorized viewing of the data when the computer or hard-disk is:

-   located in a place to which **No**n-trusted people might gain access while you are away
-   lost or stolen, as with laptops, netbooks or external storage devices
-   in the repair shop
-   discarded after its end-of-life

In addition, data-at-rest encryption can also be used to add some security against unauthorized attempts to tamper with your operating system – for example, the installation of keyloggers or Trojan horses by attackers who can gain physical access to the system while you are away.

**Warning:** Data-at-rest encryption does **No**t protect your data from all threats.

You will still be vulnerable to:

-   Attackers who can break into your system (e.g. over the Internet) while it is running and after you have already unlocked and mounted the encrypted parts of the disk.
-   Attackers who are able to gain physical access to the computer while it is running (even if you use a screenlocker), or very shortly _after_ it was running, if they have the resources to perform a [cold boot attack](https://en.wikipedia.org/wiki/Cold_boot_attack "wikipedia:Cold boot attack").
-   A government entity, which **No**t only has the resources to easily pull off the above attacks, but also may simply force you to give up your keys/passphrases using various techniques of [coercion](https://en.wikipedia.org/wiki/Coercion "wikipedia:Coercion"). In most **No**n-democratic countries around the world, as well as in the USA and UK, it may be legal for law enforcement agencies to do so if they have suspicions that you might be hiding something of interest.
-   [Rubber-hose cryptanalysis](https://en.wikipedia.org/wiki/Rubber-hose_cryptanalysis "wikipedia:Rubber-hose cryptanalysis"). Also see [XKCD #538](https://xkcd.com/538/)

Data-at-rest encryption also will **No**t protect you against someone simply [wiping your disk](https://wiki.archlinux.org/title/Securely_wipe_disk "Securely wipe disk"). [Regular backups](https://wiki.archlinux.org/title/Backup_programs "Backup programs") are recommended to keep your data safe.

A very strong disk encryption setup (e.g. full system encryption with authenticity checking and **No** plaintext boot partition) is required to stand a chance against professional attackers who are able to tamper with your system _before_ you use it. And even then it can**No**t prevent all types of tampering (e.g. hardware keyloggers). The best remedy might be [hardware-based full-disk encryption](https://wiki.archlinux.org/title/Hardware-based_full-disk_encryption "Hardware-based full-disk encryption") and [Trusted Computing](https://en.wikipedia.org/wiki/Trusted_Computing "wikipedia:Trusted Computing").

### System data encryption

While encrypting only the user data itself (often located within the home directory, or on removable media like a data DVD), is the simplest and least intrusive method, it has some significant drawbacks. In modern computer systems, there are many background processes that may cache and store information about user data or parts of the data itself in **No**n-encrypted areas of the hard drive, like:

-   swap partitions
    -   (potential remedies: disable swapping, or use [encrypted swap](https://wiki.archlinux.org/title/Encrypted_swap "Encrypted swap") as well)
-   `/tmp` (temporary files created by user applications)
    -   (potential remedies: avoid such applications; mount `/tmp` inside a [ramdisk](https://wiki.archlinux.org/title/Ramdisk "Ramdisk"))
-   `/var` (log files and databases and such; for example, [mlocate](https://wiki.archlinux.org/title/Mlocate "Mlocate") stores an index of all file names in `/var/lib/mlocate/mlocate.db`)

The solution is to encrypt both system and user data, preventing unauthorized physical access to private data that may be cached by the system. This however comes with the disadvantage that unlocking of the encrypted parts of the disk has to happen at boot time. A**No**ther benefit of system data encryption is that it complicates the installation of malware like [keyloggers](https://en.wikipedia.org/wiki/Keystroke_logging "wikipedia:Keystroke logging") or rootkits for someone with physical access.

## Preparation

### Choosing a setup

Which encryption setup is appropriate for you will depend on your goals (please read [\#Why use encryption?](https://wiki.archlinux.org/title/Data-at-rest_encryption#Why_use_encryption?) above) and system parameters.

Among other things, you will need to answer the following questions:

What kind of "attacker" do you want to protect against?

-   Casual computer user s**No**oping around your disk when your system is turned off, stolen, etc.
-   Professional cryptanalyst who can get repeated read/write access to your system before and after you use it
-   Anyone in between

What do you want to encrypt?

-   Only user data
-   User data and system data
-   Only confidential data, i.e. a subset of your data

How should swap, `/tmp`, etc. be taken care of?

-   Disable or mount as ramdisk
-   Encrypted swap
    -   Swapfile as part of full disk encryption
    -   Encrypt swap partition separately

How should encrypted parts of the disk be unlocked?

-   Passphrase
    -   Same as login password
    -   Different to login password
-   Keyfile (e.g. on a USB stick, that you keep in a safe place or carry around with yourself)
-   Both

_When_ should encrypted parts of the disk be unlocked?

-   Before boot
-   During boot
-   At login
-   Manually on demand _(after login)_

How should multiple users be accommodated?

-   **No**t at all
-   Using a shared passphrase (or keyfile) k**No**wn to every user
-   Independently issued and revocable passphrases (or keyfiles) for the same encrypted part of the disk
-   Separate encrypted parts of the disk for different users

Then you can go on to make the required technical choices (see [\#Available methods](https://wiki.archlinux.org/title/Data-at-rest_encryption#Available_methods) and [\#How the encryption works](https://wiki.archlinux.org/title/Data-at-rest_encryption#How_the_encryption_works) below), regarding:

-   stacked filesystem encryption vs. blockdevice encryption
-   key management
-   cipher and mode of operation
-   metadata storage
-   location of the "lower directory" (in case of stacked filesystem encryption)

### Preparing the disk

Before setting up encryption on a (part of a) disk, consider securely wiping it first. This consists of overwriting the entire drive or partition with a stream of zero bytes or random bytes, and is done for one or both of the following reasons:

Prevent recovery of previously stored data

Disk encryption does **No**t change the fact that individual sectors are only overwritten on demand, when the file system creates or modifies the data those particular sectors hold (see [\#How the encryption works](https://wiki.archlinux.org/title/Data-at-rest_encryption#How_the_encryption_works) below). Sectors which the filesystem considers "**No**t currently used" are **No**t touched, and may still contain remnants of data from previous filesystems. The only way to make sure that all data which you previously stored on the drive can **No**t be [recovered](https://en.wikipedia.org/wiki/Data_recovery "wikipedia:Data recovery"), is to manually erase it. For this purpose it does **No**t matter whether zero bytes or random bytes are used (although wiping with zero bytes will be much faster).

Prevent disclosure of usage patterns on the encrypted drive

Ideally, the whole encrypted part of the disk should be indistinguishable from uniformly random data. This way, **No** unauthorized person can k**No**w which and how many sectors actually contain encrypted data - which may be a desirable goal in itself (as part of true confidentiality), and also serves as an additional barrier against attackers trying to break the encryption. In order to satisfy this goal, wiping the disk using high-quality random bytes is crucial.

The second goal only makes sense in combination with block device encryption, because in the case of stacked filesystem encryption the encrypted data can easily be located anyways (in the form of distinct encrypted files in the host filesystem). Also **No**te that even if you only intend to encrypt a particular folder, you will have to erase the whole partition if you want to get rid of files that were previously stored in that folder in unencrypted form (due to [disk fragmentation](https://en.wikipedia.org/wiki/File_system_fragmentation "wikipedia:File system fragmentation")). If there are other folders on the same partition, you will have to back them up and move them back afterwards.

Once you have decided which kind of disk erasure you want to perform, refer to the [Securely wipe disk](https://wiki.archlinux.org/title/Securely_wipe_disk "Securely wipe disk") article for technical instructions.

**Tip:** In deciding which method to use for secure erasure of a hard disk drive, remember that this will **No**t need to be performed more than once for as long as the drive is used as an encrypted drive.

### Available methods

**This article or section needs expansion.**

**Reason:** [Ext4](https://wiki.archlinux.org/title/Ext4 "Ext4"), [ZFS](https://wiki.archlinux.org/title/ZFS "ZFS") and possible other filesystems offer (native) encryption. (Discuss in [Talk:Data-at-rest encryption](https://wiki.archlinux.org/title/Talk:Data-at-rest_encryption))

All data-at-rest encryption methods operate in such a way that even though the disk actually holds encrypted data, the operating system and applications "see" it as the corresponding **No**rmal readable data as long as the cryptographic container (i.e. the logical part of the disk that holds the encrypted data) has been "unlocked" and mounted.

For this to happen, some "secret information" (usually in the form of a keyfile and/or passphrase) needs to be supplied by the user, from which the actual encryption key can be derived (and stored in the kernel keyring for the duration of the session).

If you are completely unfamiliar with this sort of operation, please also read the [\#How the encryption works](https://wiki.archlinux.org/title/Data-at-rest_encryption#How_the_encryption_works) section below.

The available data-at-rest encryption methods can be separated into two types by their layer of operation:

#### Stacked filesystem encryption

Stacked filesystem encryption solutions are implemented as a layer that stacks on top of an existing filesystem, causing all files written to an encryption-enabled folder to be encrypted on-the-fly before the underlying filesystem writes them to disk, and decrypted whenever the filesystem reads them from disk. This way, the files are stored in the host filesystem in encrypted form (meaning that their contents, and usually also their file/folder names, are replaced by random-looking data of roughly the same length), but other than that they still exist in that filesystem as they would without encryption, as **No**rmal files / symlinks / hardlinks / etc.

The way it is implemented, is that to unlock the folder storing the raw encrypted files in the host filesystem ("lower directory"), it is mounted (using a special stacked pseudo-filesystem) onto itself or optionally a different location ("upper directory"), where the same files then appear in readable form - until it is unmounted again, or the system is turned off.

Available solutions in this category are [eCryptfs](https://wiki.archlinux.org/title/ECryptfs "ECryptfs") and [EncFS](https://wiki.archlinux.org/title/EncFS "EncFS").

##### Cloud-storage optimized

If you are deploying stacked filesystem encryption to achieve zero-k**No**wledge synchronization with third-party-controlled locations such as cloud-storage services, you may want to consider alternatives to eCryptfs and EncFS, since these are **No**t optimized for transmission of files over the Internet. There are some solutions designed for this purpose instead:

-   [gocryptfs](https://wiki.archlinux.org/title/Gocryptfs "Gocryptfs")
-   [cryptomator](https://aur.archlinux.org/packages/cryptomator/)AUR or [cryptomator-bin](https://aur.archlinux.org/packages/cryptomator-bin/)AUR (multi-platform)
-   [cryfs](https://archlinux.org/packages/?name=cryfs)

**No**te that some cloud-storage services offer zero-k**No**wledge encryption directly through their own [client applications](https://wiki.archlinux.org/title/List_of_applications/Internet#Cloud_synchronization_clients "List of applications/Internet").

#### Block device encryption

Block device encryption methods, on the other hand, operate _below_ the filesystem layer and make sure that everything written to a certain block device (i.e. a whole disk, or a partition, or a file acting as a [loop device](https://en.wikipedia.org/wiki/loop_device "wikipedia:loop device")) is encrypted. This means that while the block device is offline, its whole content looks like a large blob of random data, with **No** way of determining what kind of filesystem and data it contains. Accessing the data happens, again, by mounting the protected container (in this case the block device) to an arbitrary location in a special way.

The following "block device encryption" solutions are available in Arch Linux:

loop-AES

loop-AES is a descendant of cryptoloop and is a secure and fast solution to system encryption. However, loop-AES is considered less user-friendly than other options as it requires **No**n-standard kernel support.

dm-crypt

[dm-crypt](https://wiki.archlinux.org/title/Dm-crypt "Dm-crypt") is the standard device-mapper encryption functionality provided by the Linux kernel. It can be used directly by those who like to have full control over all aspects of partition and key management. The management of dm-crypt is done with the [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) userspace utility. It can be used for the following types of block-device encryption: _LUKS_ (default), _plain_, and has limited features for _loopAES_ and _Truecrypt_ devices.

-   LUKS, used by default, is an additional convenience layer which stores all of the needed setup information for dm-crypt on the disk itself and abstracts partition and key management in an attempt to improve ease of use and cryptographic security.
-   plain dm-crypt mode, being the original kernel functionality, does **No**t employ the convenience layer. It is more difficult to apply the same cryptographic strength with it. When doing so, longer keys (passphrases or keyfiles) are the result. It has, however, other advantages for very specific cases. For example, a single block device can be segmented and encrypted accordingly.

TrueCrypt/VeraCrypt

A portable format, supporting encryption of whole disks/partitions or file containers, with compatibility across all major operating systems. TrueCrypt was discontinued by its developers in May 2014. The [VeraCrypt](https://wiki.archlinux.org/title/VeraCrypt "VeraCrypt") fork was audited in 2016.

For practical implications of the chosen layer of operation, see the [#Block device vs stacked filesystem encryption](https://wiki.archlinux.org/title/Data-at-rest_encryption#Block_device_vs_stacked_filesystem_encryption) below, as well as the general write up for [eCryptfs](https://www.systutorials.com/docs/linux/packages/ecryptfs-utils-111/ecryptfs-faq.html#compare). See [Category:Encryption](https://wiki.archlinux.org/title/Category:Encryption "Category:Encryption") for the available content of the methods compared below, as well as other tools **No**t included in the table.

### Block device vs stacked filesystem encryption

| | Block device encryption | Stacked filesystem encryption | 
| --- | --- | --- |
| Encrypts | block devices | files | 
| Container for encrypted data may be... | a disk or disk partition / a file as loop device | a directory in an existing file system |
| Relation to filesystem | operates below filesystem layer: does **No**t care whether the content of the encrypted block device is a filesystem, a partition table, a LVM setup, or anything else | adds an additional layer to an existing filesystem, to automatically encrypt/decrypt files whenever they are written/read |
| Encrypts file metadata (number of files, directory structure, file sizes, permissions, modification times, etc.) | **yes**(Using 'discard' may reveal file sizes) | Partial(Only names are encrypted, all other metadata is visible)) |
| Can be used to custom-encrypt whole hard drives (including partition tables) | **yes** | **No** | 
| Can be used to encrypt swap space | **yes** | **No** | 
| Can be used without pre-allocating a fixed amount of space for the encrypted data container | **No**(Using 'discard' may allow sparsely-allocated containers, at the cost of revealing file sizes) | **yes** |
| Can be used to protect existing filesystems without block device access, e.g. NFS or Samba shares, cloud storage, etc. 1 | **No** | **yes** | 
| Allows offline file-based backups of encrypted files | **No** | **yes** |

1.  well, a single file in those filesystems could be used as a container (virtual loop-back device!) but then one would **No**t actually be using the filesystem (and the features it provides) anymore

### Comparison table

**This article or section needs expansion.**

**Reason:** Fill in blanks. Add sources to checkmarks / crosses. What is _salt_, _key-slot diffusion_ or _key scrubbing_? (Discuss in [Talk:Data-at-rest encryption](https://wiki.archlinux.org/title/Talk:Data-at-rest_encryption))

The column "dm-crypt +/- LUKS" de**No**tes features of dm-crypt for both LUKS ("+") and plain ("-") encryption modes. If a specific feature requires using LUKS, this is indicated by "(with LUKS)". Likewise "(without LUKS)" indicates usage of LUKS is counter-productive to achieve the feature and plain mode should be used.

| Summary | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | eCryptfs | EncFS | gocryptfs | fscrypt | 
| --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| Encryption type | block device | block device | block device | native filesystem or block device | stacked filesystem | stacked filesystem | stacked filesystem | native filesystem |
| **No**te | longest-existing one; possibly the fastest; works on legacy systems | de-facto standard for block device encryption on Linux; very flexible | maintained fork of TrueCrypt, supporting TrueCrypt and VeraCrypt volumes | encryption feature relatively new (2019); encrypted block devices provided by ZVOL | slightly faster than EncFS; individual encrypted files portable between systems | easiest one to use; supports **No**n-root administration | aspiring successor of EncFS | default for Chrome OS and Android encryption |
| Availability in Arch Linux | requires manually compiled, custom kernel | kernel modules: already shipped with default kernel; tools: device-mapper, cryptsetup | veracrypt | ZFS#Installation | kernel module: already shipped with default kernel; tools: ecryptfs-utils | encfs | gocryptfs | kernel module: already shipped with default kernel; tool: fscrypt | 
| License | GPL | GPL | Apache License 2.0, parts subject to TrueCrypt License v3.0 | CDDL | GPL | GPL | MIT | GPL (kernel), Apache 2.0 (userspace tool) | 
| Encryption implemented in... | kernelspace | kernelspace | kernelspace | kernelspace | kernelspace | userspace (FUSE) | userspace (FUSE) | kernelspace |
| Cryptographic metadata stored in... | ? | with LUKS: LUKS Header | begin/end of (decrypted) device (format spec) | DSL (dataset & snapshot layer; talk/slides) | header of each encrypted file | control file at the top level of each EncFS container | ? | .fscrypt directory at root of each filesystem |
| Wrapped encryption key stored in... | ? | with LUKS: LUKS header | begin/end of (decrypted) device (format spec) | DSL (dataset & snapshot layer; talk/slides) | key file that can be stored anywhere | key file that can be stored anywhere [1][2] | ? | .fscrypt directory at root of each filesystem |
| Usability features | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | eCryptfs | EncFS | gocryptfs | fscrypt | 
| **No**n-root users can create/destroy containers for encrypted data | **No** | **No** | **No** | **yes** | limited | **yes** | **yes** | **yes** | 
| Provides a GUI | **No** | **No** | **yes** | **No** | **No** | **yes** optional | **No** | **No** | 
| Support for automounting on login | ? | **yes** | **yes** systemd and /etc/crypttab | **yes** with systemd | **yes** | **yes** | **yes** | **yes** | 
| Support for automatic unmounting in case of inactivity | ? | ? | ? | **No** | ? | **yes** | **yes**[3] | **No** |

| **Security features** | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | eCryptfs | EncFS | gocryptfs | fscrypt | 
| --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| Supported ciphers | AES | AES, Anubis, CAST5/6, Twofish, Serpent, Camellia, Blowfish,… (every cipher the kernel Crypto API offers) | AES, Twofish, Serpent, Camellia, Kuznyechik | AES | AES, Blowfish, Twofish... | AES, Blowfish, Twofish, and any other ciphers available on the system | AES | AES, ChaCha12 | 
| Integrity | **No**ne | optional in LUKS2 | **No**ne | CCM, GCM | **No**ne | **No**ne (default mode)HMAC (para**No**ia mode) | GCM | **No**ne | 
| Support for salting | ? | **yes**(with LUKS) | **yes** | **yes** | **yes** | ? | **yes** | **yes** | 
| Support for cascading multiple ciphers | ? | **No**t in one device, but blockdevices can be cascaded | **yes** AES-Twofish, AES-Twofish-Serpent, Serpent-AES, Serpent-Twofish-AES, Twofish-Serpent | **No** | ? | **No** | **No** | **No** |
| Support for key-slot diffusion | ? | **yes**(with LUKS) | ? | ? | ? | ? | ? | **No** | 
| Protection against key scrubbing | **yes** | **yes**(without LUKS) | ? | ? | ? | ? | ? | **No** | 
| Support for multiple (independently revocable) keys for the same encrypted data | ? | **yes**(with LUKS) | ? | **No** | ? | **No** | ? | **yes** | 

| **Performance features** | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | eCryptfs | EncFS | gocryptfs | fscrypt |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| Multithreading support | ? | **yes**[4] | **yes** | **yes** | ? | ? | **yes** | **yes** | 
| Hardware-accelerated encryption support | **yes** | **yes** | **yes** | **yes** | **yes** | **yes**[5] | **yes** | **yes** | 
| Block device encryption specific | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | 
| Support for (manually) resizing the encrypted block device in-place | ? | **yes** | **No** | **yes** | ||||
| Stacked filesystem encryption specific |||| ZFS | eCryptfs | EncFS | gocryptfs | fscrypt |
| Supported file systems |||| ZFS | ext3, ext4, xfs (with caveats), jfs, nfs... | ext3, ext4, xfs (with caveats), jfs, nfs, cifs... [6] | any | ext4, F2FS, UBIFS | 
| Ability to encrypt filenames |||| **yes**zfs(8) | **yes** | **yes** | **yes** | **yes** |
| Ability to **No**t encrypt filenames |||| **No** | **yes** | **yes** | **yes**[7] | **No** | 
| Optimized handling of sparse files |||| **yes** | **No** | **yes** | **yes** | **yes** |

| **Compatibility & prevalence** | Loop-AES | dm-crypt +/- LUKS | VeraCrypt | ZFS | eCryptfs | EncFS | gocryptfs | fscrypt |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | 
| Supported Linux kernel versions | 2.0 or newer | CBC-mode since 2.6.4, ESSIV 2.6.10, LRW 2.6.20, XTS 2.6.24 | ? | 2.6.32 or newer (as of 0.8.3) | ? | 2.4 or newer | ? | 4.1 or newer |
| Encrypted data can also be accessed from Windows | ? | ? | **yes** | **yes**OpenZFS on Windows (repo) | ? | **No** (requires admin rights) | **yes** (cppcryptfs port) | **No** |
| Encrypted data can also be accessed from Mac OS X | ? | ? | **yes** | **yes**OpenZFS on OS X (repo) | ? | **yes**[8] | **yes** (beta quality) | **No** |
| Encrypted data can also be accessed from FreeBSD | ? | ? | **yes** | **yes**ZFS on FreeBSD (native; repo) | ? | **yes**[9] | ? | **No** | 
| Used by | ? | Debian/Ubuntu installer (system encryption)Fedora installer | ? | ? | ? | ? | ? | Android, Chrome OS |





### Examples

In practice, it could turn out something like:

Example 1

Simple user data encryption (internal hard drive) using a virtual folder called `~/Private` in the user's home directory encrypted with [EncFS](https://wiki.archlinux.org/title/EncFS "EncFS")

-   encrypted versions of the files stored on-disk in `~/.Private`
-   unlocked on demand with dedicated passphrase

Example 2

Partial system encryption with each user's home directory encrypted with [ECryptfs](https://wiki.archlinux.org/title/ECryptfs "ECryptfs")

-   unlocked on respective user login, using login passphrase
-   `swap` and `/tmp` partitions encrypted with [Dm-crypt with LUKS](https://wiki.archlinux.org/title/Dm-crypt_with_LUKS "Dm-crypt with LUKS"), using an automatically generated per-session throwaway key
-   indexing/caching of contents of `/home` by _slocate_ (and similar applications) disabled.

Example 3

System encryption - whole hard drive except `/boot` partition (however, `/boot` can be encrypted with [GRUB](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Encrypted_boot_partition_(GRUB) "Dm-crypt/Encrypting an entire system")) encrypted with [Dm-crypt with LUKS](https://wiki.archlinux.org/title/Dm-crypt_with_LUKS "Dm-crypt with LUKS")

-   unlocked during boot, using passphrases or USB stick with keyfiles
-   Maybe different passphrases/keys per user - independently revocable
-   Maybe encryption spanning multiple drives or partition layout flexibility with [LUKS on LVM](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_LVM "Dm-crypt/Encrypting an entire system")

Example 4

Hidden/plain system encryption - whole hard drive encrypted with [plain dm-crypt](https://wiki.archlinux.org/title/Dm-crypt "Dm-crypt")

-   USB-boot, using dedicated passphrase plus USB stick with keyfile
-   data integrity checked before mounting
-   `/boot` partition located on aforementioned USB stick

Example 5

File container encryption - a pre-allocated file serves as encrypted container for user data

-   A file such as `~/.volume.img` is encrypted with [Dm-crypt/Encrypting a **No**n-root file system#File container](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_a_**No**n-root_file_system#File_container "Dm-crypt/Encrypting a **No**n-root file system")
-   Occasional backup by copying the whole container to a remote host

Many other combinations are of course possible. You should carefully plan what kind of setup will be appropriate for your system.

  

## How the encryption works

This section is intended as a high-level introduction to the concepts and processes which are at the heart of usual disk encryption setups.

It does **No**t go into technical or mathematical details (consult the appropriate literature for that), but should provide a system administrator with a rough understanding of how different setup choices (especially regarding key management) can affect usability and security.

### Basic principle

For the purposes of disk encryption, each blockdevice (or individual file in the case of stacked filesystem encryption) is divided into **sectors** of equal length, for example 512 bytes (4,096 bits). The encryption/decryption then happens on a per-sector basis, so the n'th sector of the blockdevice/file on disk will store the encrypted version of the n'th sector of the original data.

Whenever the operating system or an application requests a certain fragment of data from the blockdevice/file, the whole sector (or sectors) that contains the data will be read from disk, decrypted on-the-fly, and temporarily stored in memory:
~~~
           ╔═══════╗
  sector 1 ║"???.."║
           ╠═══════╣         ╭┈┈┈┈┈╮
  sector 2 ║"???.."║         ┊ key ┊
           ╠═══════╣         ╰┈┈┬┈┈╯
           :       :            │
           ╠═══════╣            ▼             ┣┉┉┉┉┉┉┉┫
  sector n ║"???.."║━━━━━━(decryption)━━━━━━━▶┋"abc.."┋ sector n
           ╠═══════╣                          ┣┉┉┉┉┉┉┉┫
           :       :
           ╚═══════╝

           encrypted                          unencrypted
      blockdevice or                          data in RAM
        file on disk

~~~
Similarly, on each write operation, all sectors that are affected must be re-encrypted completely (while the rest of the sectors remain untouched).

In order to be able to de/encrypt data, the disk encryption system needs to k**No**w the unique secret "key" associated with it. Whenever the encrypted block device or folder in question is to be mounted, its corresponding key (called henceforth its "master key") must be supplied.

The entropy of the key is of utmost importance for the security of the encryption. A randomly generated byte string of a certain length, for example 32 bytes (256 bits), has desired properties but is **No**t feasible to remember and apply manually during the mount.

For that reason two techniques are used as aides. The first is the application of cryptography to increase the entropic property of the master key, usually involving a separate human-friendly passphrase. For the different types of encryption the [\#Comparison table](https://wiki.archlinux.org/title/Data-at-rest_encryption#Comparison_table) lists respective features. The second method is to create a keyfile with high entropy and store it on a medium separate from the data drive to be encrypted.

See also [Wikipedia:Authenticated encryption](https://en.wikipedia.org/wiki/Authenticated_encryption "wikipedia:Authenticated encryption").

### Keys, keyfiles and passphrases

The following are examples how to store and cryptographically secure a master key with a keyfile:

Stored in a plaintext keyfile

Simply storing the master key in a file (in readable form) is the simplest option. The file - called a "keyfile" - can be placed on a USB stick that you keep in a secure location and only connect to the computer when you want to mount the encrypted parts of the disk (e.g. during boot or login).

Stored in passphrase-protected form in a keyfile or on the disk itself

The master key (and thus the encrypted data) can be protected with a secret passphrase, which you will have to remember and enter each time you want to mount the encrypted block device or folder. See [\#Cryptographic metadata](https://wiki.archlinux.org/title/Data-at-rest_encryption#Cryptographic_metadata) below for details.

Randomly generated on-the-fly for each session

In some cases, e.g. when encrypting swap space or a `/tmp` partition, it is **No**t necessary to keep a persistent master key at all. A new throwaway key can be randomly generated for each session, without requiring any user interaction. This means that once unmounted, all files written to the partition in question can never be decrypted again by _anyone_ - which in those particular use-cases is perfectly fine.

### Cryptographic metadata

Frequently the encryption techniques use cryptographic functions to enhance the security of the master key itself. On mount of the encrypted device the passphrase or keyfile is passed through these and only the result can unlock the master key to decrypt the data.

A common setup is to apply so-called "key stretching" to the passphrase (via a "key derivation function"), and use the resulting enhanced passphrase as the mount key for decrypting the actual master key (which has been previously stored in encrypted form):
~~~
 ╭┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╮                         ╭┈┈┈┈┈┈┈┈┈┈┈╮
 ┊ mount passphrase ┊━━━━⎛key derivation⎞━━━━▶┊ mount key ┊
 ╰┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈╯ ,──⎝   function   ⎠     ╰┈┈┈┈┈┬┈┈┈┈┈╯
 ╭──────╮            ╱                              │
 │ salt │───────────´                               │
 ╰──────╯                                           │
 ╭──────────────────────╮                           ▼         ╭┈┈┈┈┈┈┈┈┈┈┈┈╮
 │ encrypted master key │━━━━━━━━━━━━━━━━━━━━(decryption)━━━━▶┊ master key ┊
 ╰──────────────────────╯                                     ╰┈┈┈┈┈┈┈┈┈┈┈┈╯
~~~

The **key derivation function** (e.g. PBKDF2 or scrypt) is deliberately slow (it applies many iterations of a hash function, e.g. 1000 iterations of HMAC-SHA-512), so that brute-force attacks to find the passphrase are rendered infeasible. For the **No**rmal use-case of an authorized user, it will only need to be calculated once per session, so the small slowdown is **No**t a problem. It also takes an additional blob of data, the so-called "**salt**", as an argument - this is randomly generated once during set-up of the disk encryption and stored unprotected as part of the cryptographic metadata. Because it will be a different value for each setup, this makes it infeasible for attackers to speed up brute-force attacks using precomputed tables for the key derivation function.

The **encrypted master key** can be stored on disk together with the encrypted data. This way, the confidentiality of the encrypted data depends completely on the secret passphrase.

Additional security can be attained by instead storing the encrypted master key in a keyfile on e.g. a USB stick. This provides **two-factor authentication**: Accessing the encrypted data **No**w requires something only you _k**No**w_ (the passphrase), and additionally something only you _have_ (the keyfile).

A**No**ther way of achieving two-factor authentication is to augment the above key retrieval scheme to mathematically "combine" the passphrase with byte data read from one or more external files (located on a USB stick or similar), before passing it to the key derivation function.The files in question can be anything, e.g. **No**rmal JPEG images, which can be beneficial for [\#Plausible deniability](https://wiki.archlinux.org/title/Data-at-rest_encryption#Plausible_deniability). They are still called "keyfiles" in this context, though.

After it has been derived, the master key is securely stored in memory (e.g. in a kernel keyring), for as long as the encrypted block device or folder is mounted.

It is usually **No**t used for de/encrypting the disk data directly, though. For example, in the case of stacked filesystem encryption, each file can be automatically assigned its own encryption key. Whenever the file is to be read/modified, this file key first needs to be decrypted using the main key, before it can itself be used to de/encrypt the file contents:

~~~
                           ╭┈┈┈┈┈┈┈┈┈┈┈┈╮
                           ┊ master key ┊
   file on disk:           ╰┈┈┈┈┈┬┈┈┈┈┈┈╯
  ┌ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┐        │
  ╎╭───────────────────╮╎        ▼          ╭┈┈┈┈┈┈┈┈┈┈╮
  ╎│ encrypted file key│━━━(decryption)━━━━▶┊ file key ┊
  ╎╰───────────────────╯╎                   ╰┈┈┈┈┬┈┈┈┈┈╯
  ╎┌───────────────────┐╎                        ▼           ┌┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┐
  ╎│ encrypted file    │◀━━━━━━━━━━━━━━━(de/encryption)━━━━━▶┊ readable file ┊
  ╎│ contents          │╎                                    ┊ contents      ┊
  ╎└───────────────────┘╎                                    └┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┘
  └ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ┘
~~~

In a similar manner, a separate key (e.g. one per folder) may be used for the encryption of file names in the case of stacked filesystem encryption.

In the case of block device encryption one master key is used per device and, hence, all data. Some methods offer features to assign multiple passphrases/keyfiles for the same device and others **No**t. Some use above mentioned functions to secure the master key and others give the control over the key security fully to the user. Two examples are explained by the cryptographic parameters used by [dm-crypt](https://wiki.archlinux.org/title/Dm-crypt "Dm-crypt") in plain or LUKS modes.

When comparing the parameters used by both modes one **No**tes that dm-crypt plain mode has parameters relating to how to locate the keyfile (e.g. `--keyfile-size`, `--keyfile-offset`). The dm-crypt LUKS mode does **No**t need these, because each blockdevice contains a header with the cryptographic metadata at the beginning. The header includes the used cipher, the encrypted master-key itself and parameters required for its derivation for decryption. The latter parameters in turn result from options used during initial encryption of the master-key (e.g. `--iter-time`, `--use-random`).

For the dis-/advantages of the different techniques, please refer back to [\#Comparison table](https://wiki.archlinux.org/title/Data-at-rest_encryption#Comparison_table) or browse the specific pages.

See also:

-   [Security#Passwords](https://wiki.archlinux.org/title/Security#Passwords "Security")
-   [Wikipedia:Passphrase](https://en.wikipedia.org/wiki/Passphrase "wikipedia:Passphrase")
-   [Wikipedia:Key (cryptography)](https://en.wikipedia.org/wiki/Key_(cryptography) "wikipedia:Key (cryptography)")
-   [Wikipedia:Key management](https://en.wikipedia.org/wiki/Key_management "wikipedia:Key management")
-   [Wikipedia:Key derivation function](https://en.wikipedia.org/wiki/Key_derivation_function "wikipedia:Key derivation function")

### Ciphers and modes of operation

The actual algorithm used for translating between pieces of unencrypted and encrypted data (so-called "plaintext" and "ciphertext") which correspond to each other with respect to a given encryption key, is called a "**cipher**".

Disk encryption employs "block ciphers", which operate on fixed-length blocks of data, e.g. 16 bytes (128 bits). At the time of this writing, the predominantly used ones are:
| | block size | key size | comment |
| --- | --- | --- | --- |
| AES | 128 bits | 128, 192 or 256 bits | approved by the NSA for protecting "SECRET" and "TOP SECRET" classified US-government information (when used with a key size of 192 or 256 bits) | 
| Blowfish | 64 bits | 32–448 bits | one of the first patent-free secure ciphers that became publicly available, hence very well established on Linux | 
| Twofish | 128 bits | 128, 192 or 256 bits | developed as successor of Blowfish, but has **No**t attained as much widespread usage |
| Serpent | 128 bits | 128, 192 or 256 bits | Considered the most secure of the five AES-competition finalists[10][11][12]. |

Considered the most secure of the five AES-competition finalists[[10]](https://csrc.nist.gov/archive/aes/round2/r2report.pdf)[[11]](https://www.cl.cam.ac.uk/~rja14/Papers/serpentcase.pdf)[[12]](https://www.cl.cam.ac.uk/~rja14/Papers/serpent.pdf).

Encrypting/decrypting a sector ([see above](https://wiki.archlinux.org/title/Data-at-rest_encryption#Basic_principle)) is achieved by dividing it into small blocks matching the cipher's block-size, and following a certain rule-set (a so-called "**mode of operation**") for how to consecutively apply the cipher to the individual blocks.

Simply applying it to each block separately without modification (dubbed the "_electronic codebook (ECB)_" mode) would **No**t be secure, because if the same 16 bytes of plaintext always produce the same 16 bytes of ciphertext, an attacker could easily recognize patterns in the ciphertext that is stored on disk.

The most basic (and common) mode of operation used in practice is "_cipher-block chaining (CBC)_". When encrypting a sector with this mode, each block of plaintext data is combined in a mathematical way with the ciphertext of the previous block, before encrypting it using the cipher. For the first block, since it has **No** previous ciphertext to use, a special pre-generated data block stored with the sector's cryptographic metadata and called an "**initialization vector (IV)**" is used:
~~~

                                   ╭──────────────╮
                                   │initialization│
                                   │vector        │
                                   ╰────────┬─────╯
           ╭  ╠══════════╣        ╭─key     │      ┣┉┉┉┉┉┉┉┉┉┉┫
           │  ║          ║        ▼         ▼      ┋          ┋         . START
           ┴  ║"????????"║◀━━━(cipher)━━━━━(+)━━━━━┋"Hello, W"┋ block  ╱╰────┐
     sector n ║          ║                         ┋          ┋ 1      ╲╭────┘
   of file or ║          ║──────────────────╮      ┋          ┋         '
  blockdevice ╟──────────╢        ╭─key     │      ┠┈┈┈┈┈┈┈┈┈┈┨
           ┬  ║          ║        ▼         ▼      ┋          ┋
           │  ║"????????"║◀━━━(cipher)━━━━━(+)━━━━━┋"orld !!!"┋ block
           │  ║          ║                         ┋          ┋ 2
           │  ║          ║──────────────────╮      ┋          ┋
           │  ╟──────────╢                  │      ┠┈┈┈┈┈┈┈┈┈┈┨
           │  ║          ║                  ▼      ┋          ┋
           :  :   ...    :        ...      ...     :   ...    : ...

                ciphertext                         plaintext
                   on disk                         in RAM

~~~
When decrypting, the procedure is reversed analogously.

One thing worth **No**ting is the generation of the unique initialization vector for each sector. The simplest choice is to calculate it in a predictable fashion from a readily available value such as the sector number. However, this might allow an attacker with repeated access to the system to perform a so-called [watermarking attack](https://en.wikipedia.org/wiki/Watermarking_attack "wikipedia:Watermarking attack"). To prevent that, a method called "Encrypted salt-sector initialization vector (**ESSIV**)" can be used to generate the initialization vectors in a way that makes them look completely random to a potential attacker.

There are also a number of other, more complicated modes of operation available for disk encryption, which already provide built-in security against such attacks (and hence do **No**t require ESSIV). Some can also additionally guarantee authenticity of the encrypted data (i.e. confirm that it has **No**t been modified/corrupted by someone who does **No**t have access to the key).

See also:

-   [Wikipedia:Disk encryption theory](https://en.wikipedia.org/wiki/Disk_encryption_theory "wikipedia:Disk encryption theory")
-   [Wikipedia:Block cipher](https://en.wikipedia.org/wiki/Block_cipher "wikipedia:Block cipher")
-   [Wikipedia:Block cipher modes of operation](https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation "wikipedia:Block cipher modes of operation")

### Plausible deniability

See [Wikipedia:Plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "wikipedia:Plausible deniability").

## Backup for disk encryption scenarios

Make a [backup](https://wiki.archlinux.org/title/Backup "Backup") of the user data to protect against data loss. In general the backup of your encrypted data should be encrypted as well.

### Block device encryption

There are multiple options; you can back up the disk block device where the encryption container resides as an image, e.g. `/dev/sd_x_`, or you can back up the filesystem inside of the encrypted container, e.g. `/dev/mapper/_dm_name_`, or you can back up the files, e.g. with [rsync](https://wiki.archlinux.org/title/Rsync "Rsync"). The following sections list the advantages and disadvantages of each option.

#### Backup of disk block device

A backup of the disk block device is:

-   encrypted as is with the same level of security as the working copy
-   contains your LUKS header
-   always as large as the disk block device
-   does **No**t easily allow advanced backup strategies such as incremental backup, compression, or deduplication
-   easily restored to a new disk as this also restores the encryption container

#### Backup of the filesystem or files

A backup of the filesystem or files is:

-   _**No**t_ encrypted as is
-   should be encrypted before transport over the network, or when saving to disk, requiring additional effort
-   **No**t necessarily encrypted with the same level of security as the working copy
-   does **No**t contain your LUKS header
-   only as large as the used space on the filesystem, see e.g. [partclone](https://wiki.archlinux.org/title/Partclone "Partclone")
-   allows advanced backup strategies such as incremental backup, compression, or deduplication
-   requires manual restoration of the encryption container to a new disk, for example by restoring the LUKS header backup

#### LUKS header backup

If using LUKS it is possible to make a [backup of the LUKS headers](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Backup_and_restore "Dm-crypt/Device encryption"): it can make sense to periodically check and synchronize those backups, especially if passphrases have been revoked.

If however you have a backup of the data, and want to restore it, you can recreate the LUKS encrypted partition from scratch with cryptsetup and then restore the data, therefore backing up the LUKS header is less important than backing up the data.