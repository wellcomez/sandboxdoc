---
date: 2023-03-20 22:42
title: ecryptfs-add-passphrase
tags:
- encryptfs 
---

# ecryptfs-add-passphrase(1) - Linux man page

## Name

ecryptfs-add-passphrase - add an eCryptfs mount passphrase to the kernel keyring.

## Synopsis

**ecryptfs-add-passphrase** [--fnek]

printf "%s" "passphrase" | **ecryptfs-add-passphrase** [--fnek] -

## Description

**ecryptfs-add-passphrase** is a utility to manually add a passphrase to the kernel keyring.

If the --fnek option is specified, the filename encryption key associated with the input passphrase will also be added to the keyring.

## See Also

**[ecryptfs](https://linux.die.net/man/7/ecryptfs)**(7), **[keyctl](https://linux.die.net/man/1/keyctl)**(1)

_/usr/share/doc/ecryptfs-utils/ecryptfs-faq.html_

[http://launchpad.net/ecryptfs/](http://launchpad.net/ecryptfs/)

## Author

This manpage was written by Dustin Kirkland <[kirkland@canonical.com](mailto:kirkland@canonical.com)> for Ubuntu systems (but may be used by others). Permission is granted to copy, distribute and/or modify this document under the terms of the GNU General Public License, Version 2 or any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License can be found in /usr/share/common-licenses/GPL.