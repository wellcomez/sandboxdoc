---
date: 2023-05-23 17:01
title: Top 10 Full Disk Encryption Software Products
tags:
-  full-disk-encrption
---



  
# Top 10 Full Disk Encryption Software Products

[Chad Kime](https://www.esecurityplanet.com/author/chad-kime/)

February 21, 2023


Whenever a hacker breaches a network or an executive loses a laptop, data is exposed to theft. Full disk encryption of hard drives, external drives, and other storage systems provides a baseline of defense against this risk and can easily be implemented as a first step toward better security.

We will start with a list of 10 top full disk encryption (FDE) software solutions and discuss their features, pros, cons, and cover any available pricing. Next, we will cover the pros and cons of full disk encryption as a technology before we conclude with an overview of what criteria enabled vendors to make this list.

**For other encryption needs consider:**

-   **Learn about encryption: [Encryption: How It Works, Types and the Quantum Future](https://www.esecurityplanet.com/trends/encryption-guide/)**
-   **Other encryptions needs (file sharing, cloud, application, etc.): [15 Best Encryption Software & Tools](https://www.esecurityplanet.com/products/best-encryption-software/)**

**Jump to:**

-   **[Top Free and Built-in Encryption Software](https://www.esecurityplanet.com/products/top-full-disk-software-products/#free_and_built-in)**
    -   **[Apple File Vault 2 – Best Free Tool for macOS](https://www.esecurityplanet.com/products/top-full-disk-software-products/#apple)**
    -   **[Microsoft BitLocker – Best Free Tool for Windows](https://www.esecurityplanet.com/products/top-full-disk-software-products/#microsoft)**
-   **[Top Enterprise Full Disk Encryption Software](https://www.esecurityplanet.com/products/top-full-disk-software-products/#enterprise)**
    -   **[Micro Focus ZENworks FDE](https://www.esecurityplanet.com/products/top-full-disk-software-products/#micro_focus)**
    -   **[R&S Trusted Disk](https://www.esecurityplanet.com/products/top-full-disk-software-products/#r&s)**
    -   **[Sophos Central Device Encryption](https://www.esecurityplanet.com/products/top-full-disk-software-products/#sophos)**
    -   **[Symantec Endpoint Encryption](https://www.esecurityplanet.com/products/top-full-disk-software-products/#symantec)**
    -   **[Trellix Complete Data Protection](https://www.esecurityplanet.com/products/top-full-disk-software-products/#trellix)**
    -   **[Trend Micro Endpoint Encryption](https://www.esecurityplanet.com/products/top-full-disk-software-products/#trend_micro)**
-   **[Top Integrated Full Disk Encryption Software](https://www.esecurityplanet.com/products/top-full-disk-software-products/#integrated)**
    -   **[Check Point Harmony Endpoint](https://www.esecurityplanet.com/products/top-full-disk-software-products/#check_point)**
    -   **[ESET PROTECT](https://www.esecurityplanet.com/products/top-full-disk-software-products/#eset)**
-   **[Full Disk Encryption Pros & Cons](https://www.esecurityplanet.com/products/top-full-disk-software-products/#pros_and_cons)**
-   **[How the List of Full-Disk Encryption Solutions Was Selected](https://www.esecurityplanet.com/products/top-full-disk-software-products/#methodology)**
-   **[Bottom Line: Use Full Drive Encryption Wisely](https://www.esecurityplanet.com/products/top-full-disk-software-products/#bottom_line)**

## Top Free and Built-in Encryption Software

Smaller security teams, home offices, and sole proprietors have simple needs.  They only own a few computers so manual management of encryption isn’t a significant burden. These organizations want something inexpensive and easy to use. Fortunately, most operating systems have built-in solutions.

Linux and iOS (phone or ipad) users will need to locate their [OS options](https://ubuntu.com/core/docs/uc20/full-disk-encryption) or their [data protection settings](https://support.apple.com/guide/security/encryption-and-data-protection-overview-sece3bee0835/web). Android users can do the same, although [newer Android phones](https://support.google.com/nexus/answer/2844831?hl=en) are encrypted by default.

Most PC and server operating systems have built-in encryption technology that can be easily enabled for local file and full-drive encryption. The top two technologies are covered in detail below: FileVault 2 and BitLocker.

![Apple logo](https://www.esecurityplanet.com/wp-content/uploads/2023/02/apple-logo-150x150.png)

### Apple FileVault 2 – Best Free Tool For MacOS

Apple macOS includes FileVault 2 directly integrated into the operating system and [easily accessible](https://support.apple.com/en-us/HT204837) from Apple’s System Preferences and Security & Privacy configurations. FileVault 2 uses XTS-AES-128 encryption with a 256-bit key.

#### Features

-   **Encryption options:** can use user directory and startup volume encryption
-   **Convenient password option:** can use the user’s iCloud or Apple macOS user ID login as the passphrase for decryption
-   **Lost password recovery:** Users can set up their iCloud account or use a recovery key to recover lost encryption passwords

#### Pros

-   Option to use a separate and unique password from the login ID
-   Simple and easy to use
-   Built-in, no additional software to download or install

#### Cons

-   Implementing FileVault incorrectly might damage some files
-   Does not handle multiple users on a single machine very well
-   A compromised user’s password can allow access to the encrypted data
-   A lost password and lost recovery key makes data inaccessible

#### Price

-   Free, Included with OS X/macOS since version 10.7
-   Free legacy FileVault tool included with earlier versions of OS X

![Microsoft logo](https://www.esecurityplanet.com/wp-content/uploads/2023/01/microsoft-logo-300x100.png)

### Microsoft BitLocker – Best Free Tool For Windows

Microsoft’s BitLocker full disk encryption software delivers built-in, full disk encryption for modern versions of Windows. BitLocker can be enabled with AES 128-bit (weaker, not recommended) or AES 256-bit (recommended) encryption algorithms.

#### Features

-   **Network unlock capability:** enables a Windows PC to start automatically when connected to the internal [network](https://www.esecurityplanet.com/networks/network-security/)
-   **Encryption options:** Users can encrypt the entire drive, specific folders, or specific files (or a combination for extra security)
-   **Multi-factor authentication (MFA):** Encryption can be configured to be unlocked by keys stored on USB flash drives, smart cards, etc.

#### Pros

-   Optional Microsoft centralized control using
    -   Legacy Microsoft BitLocker Administration and Monitoring (MBAM)
    -   Microsoft Endpoint Manager Config Manager (MEMCM)
    -   Built-in features of Group Policy, Azure AD, and Microsoft Endpoint Manager Microsoft Intune (MEMMI)
-   Easy and convenient for a variety of encryption methods
-   Natural integration with the Windows OS

#### Cons

-   A lost key can make data unrecoverable and the PC unusable
-   Not available on Home versions of Windows
-   Can use Intel Trusted Platform Module (TPM) chips to generate keys, which prevents decryption when unauthorized changes are made the file system
-   No integration with OS passwords
-   Does not support other operating systems (Linux, macOS, etc.)

#### Price

-   Free encryption software
    -   Included with Windows OS since Ultimate and Enterprise editions of Microsoft Vista and Windows 7
    -   Included with Pro, Enterprise, and Education versions of Windows 8+
    -   Included with Microsoft Server 2008 or later
-   No additional fee management software: Legacy Microsoft BitLocker Administration and Monitoring (MBAM) software receives extended support until 2026 and is licensed for no additional fee, but requires an Enterprise license for all managed Windows machines
-   Starting at $5 / user / month for management software: Microsoft Intune (Endpoint Manager) offers a free trial and several on-premises pricing editions

**Read more: [Windows 11 Security Features & Requirements](https://www.esecurityplanet.com/endpoint/windows-11-security/)**

## Top Enterprise Full Disk Encryption Software

Built-in free encryption tools provide strong protection, but larger businesses need centralized controls to enforce compliance with security standards and to manage encryption keys. Organizations willingly pay licensing fees for more sophisticated encryption software to reduce IT support costs associated with encryption setup, management, and user support.

![Micro Focus (OpenText) logo](https://www.esecurityplanet.com/wp-content/uploads/2023/02/micro_focus-logo-300x150.png)

### Micro Focus ZENworks FDE

Micro Focus, acquired by OpenText in August 2022, delivers the [ZENworks Full Disk Encryption](https://www.microfocus.com/en-us/products/zenworks-full-disk-encryption/overview) (FDE) solution for management and enforcement of endpoint full-disk encryption. Other tools in the ZENworks product family include solutions for tracking, configuration, security, and endpoint management through a single web-based console.

#### Features

-   **Full control capabilities:** IT admins can provide emergency recovery, remotely unlock devices and even decommission a drive or device
-   **Authentication options** for booting an encrypted drive includes support for smartcards combined with a PIN or just login credentials
-   **Preboot authentication** prevents device startup without the proper password

#### Pros

-   Can integrate with other ZENworks tools to provide a unified dashboard for endpoint security and control
-   Supports a wide range of operating systems for Windows Servers (2008-2022), thin client sessions, Windows Workstations (Windows 7-11), Linux servers and workstations, and macOS (10.8.3 – 12.x)
-   Can recover encrypted data even with damaged encryption keys
-   Satisfies federal information processing standards (FIPS) 140-2
    -   Level 2 for encryption in conjunction with OPAL-compliant hardware
    -   Level 1 for software-only encryption
-   Can test OPAL self-encrypting drives to detect if they are ZENworks FDE compatible
-   Firewall friendly agent uses HTTP web connections
-   Centralized encryption key management
-   Users won’t see a difference because encryption happens in the background during boot-up

#### Cons

-   Does not manage mobile devices
-   Requires an endpoint agent
-   Uses unencrypted HTTP web communication instead of HTTPS

#### Price

Microfocus does not publish pricing for the ZENworks FDE product on their website. Zenworks FDE annual licenses can be purchased on a per user/device basis and customers likely can obtain volume discounts or reseller discounts through sales partners.

![Rohde & Schwarz logo](https://www.esecurityplanet.com/wp-content/uploads/2023/02/rohde__schwarz-logo-300x100.png)

### R&S Trusted Disk

Rohde and Schwarz’s [R&S Trusted Disk](https://www.rohde-schwarz.com/us/products/cybersecurity/endpoint-security/rs-trusted-disk_63493-543620.html) provides full-disk encryption for both individual systems as well as [enterprise networks](https://www.esecurityplanet.com/networks/network-security/) running Microsoft Windows. The German-based technology aims to meet the VS-NfD (RESTRICTED) approval level for the BSI (German Federal Office for Information Security) standards.

#### Features

-   **Temporary file protection**
-   **Pre-boot authentication** procedure is robust and includes both a PIN and a hardware token
-   **Customizable Graphics** to match customer colors, corporate images, and button positions
-   **BSI compliant:** meets current random number generation and flexible re-keying based upon time and data quantity requirements
-   **AES-XTS-512** encryption algorithm used with SHA-2 512 HASH algorithm

#### Pros

-   Real-time encryption of local drives and peripheral drives
-   Popular for international organizations seeking to satisfy EU standards
-   Windows PE-based recovery tool
-   Self-service pin code reset
-   Supports UEFI Secure Boot

#### Cons

-   Only supports Windows endpoints and servers
-   Not a well-known brand in North America

#### Price

Rohde & Schwarz does not publish prices for their R&S Trusted Disk product but customers can obtain quotes directly through their website.

![Sophos logo](https://www.esecurityplanet.com/wp-content/uploads/2023/01/sophos-logo-300x100.png)

### Sophos Central Device Encryption

[Sophos Central Device Encryption](https://www.sophos.com/en-us/products/central-device-encryption) replaces [a number of SafeGuard](https://support.sophos.com/support/s/article/KB-000034767?language=en_US) encryption tools to extend the native capabilities of Windows BitLocker and macOS FileVault with additional management features. Sophos is deployable on endpoints centrally without any end user involvement and encryption can be accelerated using Intel’s AES-NI instruction set.

#### Features

-   **Central management** provides administrator management of full-disk encryption across a fleet of devices
-   **Beyond full disk encryption** enables file level-encryption for removable storage devices and the cloud
-   **Strong reporting** to show enforcement of encryption policies for regulatory compliance
-   **Self-service portal** for reduced burden on the IT helpdesk
-   **Encryption key storage** within the Sophos Central Device Encryption platform to prevent lost data from key corruption or password loss

#### Pros

-   Easy-to-understand dashboard view
-   Windows endpoint recovery
-   Integrates well with Sophos security solutions also managed through the central administrator
-   Manager detects and tracks devices out of compliance
-   Cloud-based management
-   Required endpoint agent also manages endpoint security

#### Cons

-   Does not support server encryption or linux endpoints
-   Does not support mobile devices
-   No longer supports on-prem management
-   Requires an endpoint agent

#### Price

Sophos does not publish pricing information but offers free trials and quotations through their website. Sophos Central Device Encryption can also be purchased through resellers and annual per endpoint client licenses are priced around $15. Bulk discounts and dealer incentives may reduce the prices further.

![Symantec logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20300%20100'%3E%3C/svg%3E)

### Symantec Endpoint Encryption

Broadcom’s [Symantec Endpoint Encryption](https://www.broadcom.com/products/advanced-threat-protection/encryption) provides an array of integration options through a centralized management platform, starting with full disk encryption for Windows and macOS devices.

#### Features

-   **Proprietary encryption algorithm** based on PGP
-   **Third-party encryption compatibility** for Microsoft BitLocker, Apple FileVault 2, and OPAL-compliant self-encrypting storage drives
-   **Centralized policy** management and enforcement
-   **Encryption options** for removable media, external hard drives, folders, files, emails
-   **PGP command line encryption** protects data exchange between internal and external parties
-   **Strong recovery options** to enable IT help desk staff to recover lost encryption keys
-   **Centralized management** for encryption keys

#### Pros

-   Command line encryption enables programmable and granular encryption control
-   Supports fast and flexible integration and deployment of encryption options
-   Centralized audit trails and reporting for tracking and compliance
-   Can support encryption options for Apple iOS and Android devices

#### Cons

-   Older devices will perform slowly on startup
-   Requires an endpoint agent

#### Price

Broadcom does not publish pricing for the Symantec Endpoint Encryption product, but it does provide website resources to obtain an enterprise solution quote or to locate a reseller.

A license for the Symantec Endpoint Encryption with one year of support is estimated to cost around $65 per Windows device. There may be variance in prices for licenses for different operating systems and volume discounts.

![Trellix logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20300%20125'%3E%3C/svg%3E)

### Trellix Complete Data Protection

Previously known as McAfee’s [Complete Data Protection](https://www.trellix.com/en-us/products/complete-data-protection.html) before [merging with FireEye](https://www.esecurityplanet.com/products/mcafee-fireeye-merger-makes-stg-plans-clearer/), the product has now switched to the new Trellix branding. Trellix offers both a standard and an advanced version of Complete Data Protection, but full disk drive encryption comes standard with the baseline product.

#### Features

-   **Manages encryption natively** through Microsoft’s BitLocker and Apple’s FileVault 2
-   **Encryption can be managed centrally** via McAfee’s ePolicy Orchestrator (ePO) management suite, which also manages other McAfee endpoint products
-   **Synchronize security policies** with Microsoft Active Directory, Novell NDS, PKI, and other systems
-   **Goes beyond FDE** by providing management and capabilities for files and folders as well as removable media
-   **Advanced version options** for data loss protection (DLP) and device control
-   **Drive encryption MFA** support for Stored Value and smartcards
-   **Automatic network unlock** for devices when on a corporate network

#### Pros

-   Centralized key management
-   Advanced reporting and auditing through a centralized console
-   Integrates with Active Directory and other Trellix technologies
-   Supports removable media and cloud data encryption

#### Cons

-   Automatic network unlock assumes networks are not compromised
-   Support only applies to the management console, not the underlying Microsoft BitLocker or FileVault 2 technology
-   Not all features are supported on the macOS by FileVault 2

#### Price

Trellix does not publish pricing but encourages interested organizations to contact sales through their website. Trellix offers annual subscriptions and perpetual licenses with one year of support, and organizations can likely obtain volume-based discounts and partner promotions.

As an example of the estimated price ranges:

-   Annual subscription license
    -   $28 / user / year for 10-20 users
    -   $22 / user / year for 2500 – 4500 users
-   Perpetual license with one year of support
    -   $136 / user for 10 – 20 users
    -   $78 / user for 2500 – 4500 users

![Trend Micro logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20300%20125'%3E%3C/svg%3E)

### Trend Micro Endpoint Encryption

[Trend Micro Endpoint Encryption](https://www.trendmicro.com/en_us/business/products/user-protection/sps/endpoint/endpoint-encryption.html) deploys a platform to manage full disk encryption as well data protection for removable media. Integrates with the full suite of Trend Micro security solutions.

#### Features

-   **Central management system** for Microsoft BitLocker and Apple FileVault
-   **Transparent key management** to enable easy encryption management for for both users and administrators
-   **Remote lock and kill capability** for lost or stolen devices
-   **Self-encrypting drive support** for TCG OPAL and OPAL 2 SED drives

#### Pros

-   Encrypts full volumes, files and file folders
-   Protects laptops, desktops, and removable media
-   Enforcement of compliance requirements through real-time policy enforcement and detailed audits
-   Reporting by individual, organizational unit, or device
-   Recovery console for Windows devices
-   Pre-boot authentication
-   Extends to protect user-owned devices

#### Cons

-   Does not protect servers
-   Does not protect linux OS
-   Uses a separate agent from endpoint protection

#### Price

Trend Micro does not publish pricing information on their website for the Endpoint Encryption tool; however, a license for a single user and up to 500 endpoints is estimated to be between $75 and $85 per year. Trend Micro offers free quotes and free trials of the Smart Protection Suite that includes the Endpoint Encryption tool. Trend Micro also helps organizations to find a resale partner that can likely provide bulk pricing or other incentives.

## Top Integrated Full Disk Encryption Software

The widespread need for full-disk encryption security has led many vendors to incorporate the option for full-disk encryption as a feature of other tools — primarily endpoint protection software. These vendors do not deliver a separate full disk encryption tool, but organizations may welcome an opportunity to solve multiple security problems through a single software solution.

![Check Point logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20300%20100'%3E%3C/svg%3E)

### Check Point Harmony Endpoint

Formerly known as SandBlast Agent, Check Point’s full disk encryption features come bundled within its endpoint security solution, [Harmony Endpoint](https://www.checkpoint.com/harmony/advanced-endpoint-protection/). Harmony Endpoint also includes solutions for endpoint security, virtual private network (VPN) remote access, clientless zero-trust access to applications, email security, secure internet browsing, and mobile security.

#### Features

-   **Pre-boot protection** ensures that any unauthorized startup of the computer cannot access or tamper with the data
-   **Authentication options** ensure that only validated users get access to encrypted data
-   **Endpoint security screen saver** option can encrypt idle machines

#### Pros

-   Can use the login credentials as the encryption password
-   Can encrypt all drives on the device, specific drives, or used disk space only
-   Uses built-in encryption of existing BitLocker (Windows devices) and FileVault 2 (macOS) software
-   Takes over management of previously encrypted endpoint devices
-   Managed services are available for Managed IT Service (MSP) and Managed IT Security Service (MSSP) providers

#### Cons

-   Default reports tend to be too simple and insufficient for some needs
-   Full disk encryption capabilities cannot be purchased separately from the full package of tools
-   Not all features are equally well developed and capable
-   Although Harmony Endpoint is sold for Linux endpoints, full disk encryption is not supported
-   Does not support servers
-   Requires an endpoint agent

#### Price

Check Point offers demos and free trials for Harmony Endpoint. Check Point offers annual licenses for three versions of Harmony Endpoint (Basic, Advanced, Complete). However, host encryption is only available with Harmony Endpoint Complete, which costs about $64 for per device (Windows, macOS, Linux).

**_See our picks for the [Top Endpoint Detection & Response (EDR) Solutions](https://www.esecurityplanet.com/products/edr-solutions/)_**

![ESET logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20300%20150'%3E%3C/svg%3E)

### ESET PROTECT

[ESET PROTECT](https://www.eset.com/int/business/complete-protection-bundle/) delivers full disk encryption as part of their ESET PROTECT bundle that also includes a unified management console, endpoint protection, file server security, advanced threat defense, email security, and cloud application protection.

#### Features

-   **Multi-factor authentication** options for more robust user validation
-   **Encryption options** to protect removable media, files and folders as well as email
-   **High encryption standards** with FIPS 140-2 validated and 256-bit AES encryption
-   **Easy to change** licenses to adjust for organization scale

#### Pros

-   Native macOS encryption through managed FileVault 2
-   Support for a wide range of Windows OS versions (8.1 to 11)
-   Transparent and publicized pricing
-   Easy to install

#### Cons

-   Does not support encryption management for linux or Windows servers
-   Requires the purchase of unnecessary solutions included in the full ESET Protect Complete bundle to obtain full-disk encryption (file, folder, virtual disk, or archive encryption is available as a separately licensed Endpoint Encryption software)
-   Some users complain of resource-heavy operations that slow endpoints
-   Other users complain of non-automated software updates

#### Price

ESET offers an interactive demo and a 30-day free trial of PROTECT. Pricing is listed on their website, but does not reflect potential discounts available through resellers and MSP / MSSP partners. For businesses, ESET offers three versions of ESET Protect (Entry, Advanced, Complete), but only Advanced and Complete support full disk encryption.

ESET Protect licenses are for a minimum of one year and five devices. Discounts are available for longer time commitments, more endpoints, and through occasional new customer promotions. Prices start at:

-   $248.40 / year Advanced
    -   Includes management console, endpoint protection, file server security, full disk encryption, and advanced threat defense
    -   Detection and Response as well as Security Services are available on demand for additional fees.
-   $382.50 / year Complete
    -   Adds cloud app protection and email security

**Also read our full review of [ESET PROTECT Advanced](https://www.esecurityplanet.com/products/eset-review/).**

## Full Disk Encryption Pros & Cons

[Encrypted](https://www.esecurityplanet.com/networks/encryption/) data provides an obstacle and a layer of [risk mitigation](https://www.esecurityplanet.com/products/risk-management-software/) against data loss by rendering data unreadable at rest. Full disk encryption (FDE) uses encryption algorithms to encase the operating system, all data, and all installed applications residing within a storage device within the encrypted environment.

When a device is turned on, the user is prompted for the encryption key that descrambles the data and allows the system to decrypt enough to boot and run normally. However, while encryption provides a strong benefit in specific use cases, it cannot solve all problems, and organizations need to be aware of both the advantages as well as the limitations of FDE.

### Full Disk Encryption Pros

Full Data Encryption plays a critical role in reducing the risk of data loss. Understanding its capabilities helps a security team to understand where it fits into the security stack.

**Locks Down Lost Data:** FDE’s primary advantage is that it protects data at rest. If an encrypted drive (laptop, external hard drive, etc.) is lost or stolen, the thief would also need the encryption key to unlock the data.

**Enables User-Data-Device Matching:** FDE can also work with multi-factor authentication methods (biometrics, USB keys, one-time passwords, etc.) to ensure that the data is only accessed by the correct person on the correct device.

**Also read: [Exfiltration Can Be Stopped With Data-in-Use Encryption, Company Says](https://www.esecurityplanet.com/trends/exfiltration-data-in-use-encryption/)**

### Full Disk Encryption Cons

Organizations should not assume that their data is fully secured by only using FDE. The limitations of the technology should be countered by applying other technologies or techniques.

**Decrypted data remains vulnerable:** Full disk encryption only works when the data is at rest. Once the user turns on the device and the drive decrypts, information read from the disk is decrypted on the fly and stored in memory. This decrypted data can then become vulnerable to a variety of attacks:

-   Users can also attach files to emails and upload data to unencrypted USB drives or cloud data repositories
-   Hackers using compromised or falsified credentials can decrypt data
-   Attackers can steal the device while it is operating
-   Advanced adversaries with device access can potentially use chilling to preserve the data residing in DRAM memory chips to extract data

**Brute force attack vulnerability:** The only practical way to decrypt encrypted drives without access to the key is to use software to make repeated attempts to guess the password. Organizations prevent these brute force attacks through settings in the FDE software to limit failed login attempts or disable the system (permanently or for a fixed period) after a certain number of failed login attempts. Weak user passwords undermine brute force protections so password complexity should be enforced.

**Slowed performance:** Encryption adds a layer of calculations and one more application for computer memory and processors to juggle in addition to other workloads. For newer personal computers, the slowness may be imperceptible, especially for users that begin working with already-encrypted drives. However, for computationally heavy applications and older devices, performance will certainly be affected.

**Lost or stolen keys:** Encryption solves one access problem by introducing a new one — tracking and maintaining the keys that enable decryption. Centralized management can aid in key replacement or recovery to offset this issue. Users attempting self-management need to safely store their keys to prevent stolen or lost keys. Lost keys can render the data permanently inaccessible.

**Disabled software:** Full disk encryption products may overwrite parts of the disk (such as the boot sector) already in use by other installed software. FDE installed later may make this software unusable. In most cases, this type of conflict can only be detected after installation.

## How This List of Full-Disk Encryption Solutions Was Selected

To gather candidates for this list, market research was performed on the encryption category to determine popular solutions for full disk encryption. Based upon product reviews, industry discussions, and industry rankings, the list was narrowed to top candidates. An analysis of capabilities was then performed to determine how the product fit into the Full Disk Encryption category relative to peers.

Of course, the first criteria needed to be the capability to fully encrypt the hard drive. This eliminates [many popular encryption tools](https://www.esecurityplanet.com/products/best-encryption-software/) that provide other forms of encryption but not FDE.

We then considered tool features, with the most weight on the critical centralization of encryption control and key management. Finally, price, prominence, and extra features helped us make our final list.

[A large number of tools](https://www.esecurityplanet.com/networks/types-of-network-security/) enable full disk encryption, and the market continues to evolve. This list of top tools will likely evolve with the market over time to reflect added capabilities and rising competitors.

## The Bottom Line: Use Full Drive Encryption Wisely

Full drive encryption may be a limited tool, but it plays a crucial role in a business environment. The low effort to implement FDE and the decrease in risk for lost or stolen data more than offsets potential limitations. FDE provides a high-value solution that all organizations should strongly consider adding to their security stack.

**Further reading:**

-   **[Disk vs File Encryption: Which Is Best for You?](https://www.esecurityplanet.com/threats/disk-vs-file-encryption-which-is-best-for-you/)**
-   **[New Quantum-safe Cryptography Standards Arrive None Too Soon](https://www.esecurityplanet.com/trends/quantum-safe-cryptography-standards/)**

