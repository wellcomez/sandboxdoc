---
layout: post
tags:
- Encryption
- filesystem
title: "dm-crypt"
---
# dm-crypt

7 languages

-   [Article](https://en.wikipedia.org/wiki/Dm-crypt "View the content page [alt-shift-c]")
-   [Talk](https://en.wikipedia.org/wiki/Talk:Dm-crypt "Discuss improvements to the content page [alt-shift-t]")

-   [Read](https://en.wikipedia.org/wiki/Dm-crypt)
-   [Edit](https://en.wikipedia.org/w/index.php?title=Dm-crypt&action=edit "Edit this page [alt-shift-e]")
-   [View history](https://en.wikipedia.org/w/index.php?title=Dm-crypt&action=history "Past revisions of this page [alt-shift-h]")

From Wikipedia, the free encyclopedia

**dm-crypt** is a transparent [block device encryption](https://en.wikipedia.org/wiki/Disk_encryption "Disk encryption") subsystem in [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel "Linux kernel") versions 2.6 and later and in [DragonFly BSD](https://en.wikipedia.org/wiki/DragonFly_BSD "DragonFly BSD"). It is part of the [device mapper](https://en.wikipedia.org/wiki/Device_mapper "Device mapper") (dm) infrastructure, and uses cryptographic routines from the kernel's [Crypto API](https://en.wikipedia.org/wiki/Crypto_API_(Linux) "Crypto API (Linux)"). Unlike its predecessor [cryptoloop](https://en.wikipedia.org/wiki/Cryptoloop "Cryptoloop"), dm-crypt was designed to support advanced modes of operation, such as [XTS](https://en.wikipedia.org/wiki/Disk_encryption_theory#XTS "Disk encryption theory"), [LRW](https://en.wikipedia.org/wiki/Disk_encryption_theory#Liskov,_Rivest,_and_Wagner_(LRW) "Disk encryption theory") and [ESSIV](https://en.wikipedia.org/wiki/Disk_encryption_theory#Encrypted_salt-sector_initialization_vector_(ESSIV) "Disk encryption theory") (see [disk encryption theory](https://en.wikipedia.org/wiki/Disk_encryption_theory "Disk encryption theory") for further information), in order to avoid [watermarking attacks](https://en.wikipedia.org/wiki/Watermarking_attack "Watermarking attack").[[1]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-new-methods-1) In addition to that, dm-crypt addresses some reliability problems of cryptoloop.[[2]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-2)

dm-crypt is implemented as a device mapper target and may be stacked on top of other device mapper transformations. It can thus encrypt whole disks (including [removable media](https://en.wikipedia.org/wiki/Removable_media "Removable media")), [partitions](https://en.wikipedia.org/wiki/Disk_partitioning "Disk partitioning"), [software RAID](https://en.wikipedia.org/wiki/Software_RAID "Software RAID") volumes, [logical volumes](https://en.wikipedia.org/wiki/Logical_volume "Logical volume"), as well as [files](https://en.wikipedia.org/wiki/Computer_file "Computer file"). It appears as a block device, which can be used to back [file systems](https://en.wikipedia.org/wiki/File_systems "File systems"), [swap](https://en.wikipedia.org/wiki/Swap_partition "Swap partition") or as an [LVM](https://en.wikipedia.org/wiki/Logical_Volume_Manager_(Linux) "Logical Volume Manager (Linux)") [physical volume](https://en.wikipedia.org/wiki/Physical_volume "Physical volume").

Some [Linux distributions](https://en.wikipedia.org/wiki/Linux_distribution "Linux distribution") support the use of dm-crypt on the root file system. These distributions use [initrd](https://en.wikipedia.org/wiki/Initrd "Initrd") to prompt the user to enter a passphrase at the console, or insert a [smart card](https://en.wikipedia.org/wiki/Smart_card "Smart card") prior to the normal boot process.[[3]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-crypt-fedora-3)

## Frontends

The dm-crypt device mapper target resides entirely in kernel space, and is only concerned with encryption of the [block device](https://en.wikipedia.org/wiki/Block_device "Block device") – it does not interpret any data itself. It relies on [user space](https://en.wikipedia.org/wiki/User_space "User space") [front-ends](https://en.wikipedia.org/wiki/Front_and_back_ends "Front and back ends") to create and activate encrypted volumes, and manage authentication. At least two frontends are currently available: `cryptsetup` and `cryptmount`.


The `cryptsetup` command-line interface, by default, does not write any headers to the encrypted volume, and hence only provides the bare essentials: encryption settings have to be provided every time the disk is mounted (although usually employed with automated scripts), and only one [key](https://en.wikipedia.org/wiki/Key_(cryptography) "Key (cryptography)") can be used per volume; the [symmetric encryption](https://en.wikipedia.org/wiki/Symmetric-key_algorithm "Symmetric-key algorithm") key is directly derived from the supplied [passphrase](https://en.wikipedia.org/wiki/Passphrase "Passphrase").

Because it lacks a "[salt](https://en.wikipedia.org/wiki/Salt_(cryptography) "Salt (cryptography)")", using cryptsetup is less secure in this mode than is the case with [Linux Unified Key Setup](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup "Linux Unified Key Setup") (LUKS).[[9]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-9) However, the simplicity of cryptsetup makes it useful when combined with third-party software, for example, with [smart card](https://en.wikipedia.org/wiki/Smart_card "Smart card") authentication.

`cryptsetup` also provides commands to deal with the LUKS on-disk format. This format provides additional features such as [key management](https://en.wikipedia.org/wiki/Key_management "Key management") and [key stretching](https://en.wikipedia.org/wiki/Key_stretching "Key stretching") (using [PBKDF2](https://en.wikipedia.org/wiki/PBKDF2 "PBKDF2")), and remembers encrypted volume configuration across reboots.[[3]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-crypt-fedora-3)[[10]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-tks1-10)

### cryptmount
The `cryptmount` interface is an alternative to the "cryptsetup" tool that allows any user to [mount](https://en.wikipedia.org/wiki/Mount_(computing) "Mount (computing)") and unmount a dm-crypt file system when needed, without needing [superuser](https://en.wikipedia.org/wiki/Superuser "Superuser") privileges after the device has been configured by a superuser.

## Features

The fact that disk encryption (volume encryption) software like dm-crypt only deals with transparent encryption of abstract [block devices](https://en.wikipedia.org/wiki/Block_device "Block device") gives it a lot of flexibility. This means that it can be used for encrypting any disk-backed [file systems](https://en.wikipedia.org/wiki/File_system "File system") supported by the [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system"), as well as [swap space](https://en.wikipedia.org/wiki/Swap_space "Swap space"); [write barriers](https://en.wikipedia.org/wiki/Write_barrier "Write barrier") implemented by file systems are preserved.[[11]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-11)[[12]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-12) Encrypted volumes can be stored on [disk partitions](https://en.wikipedia.org/wiki/Disk_partition "Disk partition"), [logical volumes](https://en.wikipedia.org/wiki/Logical_volume "Logical volume"), whole disks as well as [file](https://en.wikipedia.org/wiki/Computer_file "Computer file")-backed [disk images](https://en.wikipedia.org/wiki/Disk_image "Disk image") (through the use of [loop devices](https://en.wikipedia.org/wiki/Loop_device "Loop device") with the losetup utility). dm-crypt can also be configured to encrypt [RAID](https://en.wikipedia.org/wiki/RAID "RAID") volumes and [LVM](https://en.wikipedia.org/wiki/Logical_volume_management "Logical volume management") physical volumes.

dm-crypt can also be configured to provide [pre-boot](https://en.wikipedia.org/wiki/Booting "Booting") authentication through an [initrd](https://en.wikipedia.org/wiki/Initrd "Initrd"), thus encrypting all the data on a computer – except the bootloader, the kernel and the initrd image itself.[[3]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-crypt-fedora-3)

When using the [cipher block chaining](https://en.wikipedia.org/wiki/Block_cipher_modes_of_operation#Cipher_Block_Chaining_(CBC) "Block cipher modes of operation") mode of operation with predictable [initialization vectors](https://en.wikipedia.org/wiki/Initialization_vector "Initialization vector") as other disk encryption software, the disk is vulnerable to [watermarking attacks](https://en.wikipedia.org/wiki/Watermarking_attack "Watermarking attack"). This means that an attacker is able to detect the presence of specially crafted data on the disk. To address this problem in its predecessors, dm-crypt included provisions for more elaborate, disk encryption-specific modes of operation.[[1]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-new-methods-1) Support for [ESSIV](https://en.wikipedia.org/wiki/ESSIV "ESSIV") (encrypted salt-sector initialization vector) was introduced in Linux kernel version 2.6.10, [LRW](https://en.wikipedia.org/wiki/Disk_encryption_theory#LRW "Disk encryption theory") in 2.6.20 and [XTS](https://en.wikipedia.org/wiki/Disk_encryption_theory#XTS "Disk encryption theory") in 2.6.24.

The Linux Crypto API includes support for most popular [block ciphers](https://en.wikipedia.org/wiki/Block_cipher "Block cipher") and [hash functions](https://en.wikipedia.org/wiki/Hash_function "Hash function"), which are all usable with dm-crypt.

Crypted FS support include LUKS volumes, [loop-AES](https://en.wikipedia.org/w/index.php?title=Loop-AES&action=edit&redlink=1 "Loop-AES (page does not exist)") and since Linux kernel 3.13, the [TrueCrypt](https://en.wikipedia.org/wiki/TrueCrypt "TrueCrypt") target called "tcw".[[13]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-13)[[14]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-14)[[15]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-15)

## Compatibility

dm-crypt and LUKS encrypted disks can be accessed and used under MS Windows using the now defunct [FreeOTFE](https://en.wikipedia.org/wiki/FreeOTFE "FreeOTFE") (formerly DoxBox, LibreCrypt), provided that the filesystem used is supported by Windows (e.g. [FAT](https://en.wikipedia.org/wiki/File_Allocation_Table "File Allocation Table")/FAT32/[NTFS](https://en.wikipedia.org/wiki/NTFS "NTFS")). Encrypted [ext2](https://en.wikipedia.org/wiki/Ext2 "Ext2") and [ext3](https://en.wikipedia.org/wiki/Ext3 "Ext3") filesystems are supported by using [Ext2Fsd](https://en.wikipedia.org/wiki/Ext2Fsd "Ext2Fsd") or so-called "Ext2 Installable File System for Windows";[[16]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-16) FreeOTFE also supports them.

Cryptsetup/LUKS and the required infrastructure have also been implemented on the DragonFly BSD operating system.[[17]](https://en.wikipedia.org/wiki/Dm-crypt#cite_note-17)