---
date: 2023-03-24 13:13
title: data-at-rest
tags:
-  Encryption
-  data-at-rest
---



# Data at rest

From Wikipedia, the free encyclopedia

**Data at rest** in [information technology](https://en.wikipedia.org/wiki/Information_technology "Information technology") means data that is housed physically on [computer data storage](https://en.wikipedia.org/wiki/Computer_data_storage "Computer data storage") in any digital form (e.g. [cloud storage](https://en.wikipedia.org/wiki/Cloud_storage "Cloud storage"), [file hosting services](https://en.wikipedia.org/wiki/File_hosting_service "File hosting service"), [databases](https://en.wikipedia.org/wiki/Database "Database"), [data warehouses](https://en.wikipedia.org/wiki/Data_warehouse "Data warehouse"), [spreadsheets](https://en.wikipedia.org/wiki/Spreadsheet "Spreadsheet"), archives, tapes, off-site or cloud backups, [mobile devices](https://en.wikipedia.org/wiki/Mobile_device "Mobile device") etc.). Data at rest includes both structured and unstructured data.[1](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-1) This type of data is subject to threats from hackers and other malicious threats to gain access to the data digitally or physical theft of the data storage media. To prevent this data from being accessed, modified or stolen, organizations will often employ security protection measures such as password protection, data encryption, or a combination of both. The security options used for this type of data are broadly referred to as _**d**ata **a**t **r**est **p**rotection_ (DARP).[2](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-2)



![[Pasted image 20230324131844.png]]

Figure 1: The 3 states of data.

_Data at rest_ is used as a complement to the terms _[data in use](https://en.wikipedia.org/wiki/Data_in_use "Data in use")_ and _[data in transit](https://en.wikipedia.org/wiki/Data_in_transit "Data in transit")_ which together define the three states of digital data (_see Figure 1_).[3](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-3)

## Alternative definition

There is some disagreement regarding the difference between data at rest and [data in use](https://en.wikipedia.org/wiki/Data_in_use "Data in use"). Data at rest generally refers to data stored in persistent storage (disk, tape) while data in use generally refers to data being processed by a computer central processing unit ([CPU](https://en.wikipedia.org/wiki/CPU "CPU")) or in random access memory ([RAM](https://en.wikipedia.org/wiki/RAM "RAM"), also referred to as main memory or simply memory). Definitions include:

> "...all data in computer storage while excluding data that is traversing a network or temporarily residing in computer memory to be read or updated."[4](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-techtarget1-4)


Figure 2: Data at Rest vs Data in Use.
![[Pasted image 20230324131907.png]]

> "...all data in storage but excludes any data that frequently traverses the network or that which resides in temporary memory. Data at rest includes but is not limited to archived data, data which is not accessed or changed frequently, files stored on hard drives, USB thumb drives, files stored on backup tape and disks, and also files stored off-site or on a [storage area network](https://en.wikipedia.org/wiki/Storage_area_network "Storage area network") (SAN)."[5](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-5)

**Data in use** has also been taken to mean “active data” in the context of being in a database or being manipulated by an application. For example, some [enterprise encryption gateway](https://en.wikipedia.org/wiki/Enterprise_encryption_gateway "Enterprise encryption gateway") solutions for the cloud claim to encrypt data at rest, [data in transit](https://en.wikipedia.org/wiki/Data_in_transit "Data in transit") and [data in use](https://en.wikipedia.org/wiki/Data_in_use "Data in use").[6](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-6)

While it is generally accepted that archive data (i.e. which never changes), regardless of its storage medium, is data at rest and active data subject to constant or frequent change is data in use. “Inactive data” could be taken to mean data which may change, but infrequently. The imprecise nature of terms such as “constant” and “frequent” means that some stored data cannot be comprehensively defined as either data at rest or in use. These definitions could be taken to assume that Data at Rest is a superset of data in use; however, data in use, subject to frequent change, has distinct processing requirements from data at rest, whether completely static or subject to occasional change.

The division of data at rest into the sub-categories "static" and "inconstant" addresses this distinction (_see Figure 2_)..

## Concerns about data at rest

Because of its nature data at rest is of increasing concern to businesses, government agencies and other institutions.[4](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-techtarget1-4) Mobile devices are often subject to specific security protocols to protect data at rest from unauthorized access when lost or stolen[7](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-7) and there is an increasing recognition that database management systems and file servers should also be considered as at risk;[8](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-8) the longer data is left unused in storage, the more likely it might be retrieved by unauthorized individuals outside the network.

### Encryption

[Data encryption](https://en.wikipedia.org/wiki/Data_encryption "Data encryption"), which prevents data visibility in the event of its unauthorized access or theft, is commonly used to protect data in motion and increasingly promoted for protecting data at rest.[9](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-9)

The encryption of data at rest should only include strong encryption methods such as [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Advanced Encryption Standard") or [RSA](https://en.wikipedia.org/wiki/RSA_(algorithm) "RSA (algorithm)"). Encrypted data should remain encrypted when access controls such as usernames and password fail. Increasing encryption on multiple levels is recommended. [Cryptography](https://en.wikipedia.org/wiki/Cryptography "Cryptography") can be implemented on the database housing the data and on the physical storage where the databases are stored. Data encryption keys should be updated on a regular basis. Encryption keys should be stored separately from the data. Encryption also enables [crypto-shredding](https://en.wikipedia.org/wiki/Crypto-shredding "Crypto-shredding") at the end of the data or hardware lifecycle. Periodic auditing of sensitive data should be part of policy and should occur on scheduled occurrences. Finally, only store the minimum possible amount of sensitive data.[10](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-10)

### Tokenization

[Tokenization](https://en.wikipedia.org/wiki/Tokenization_(data_security) "Tokenization (data security)") is a non-mathematical approach to protecting data at rest that replaces sensitive data with non-sensitive substitutes, referred to as tokens, which have no extrinsic or exploitable meaning or value. This process does not alter the type or length of data, which means it can be processed by legacy systems such as databases that may be sensitive to data length and type.

Tokens require significantly less computational resources to process and less storage space in databases than traditionally encrypted data. This is achieved by keeping specific data fully or partially visible for processing and analytics while sensitive information is kept hidden. Lower processing and storage requirements makes tokenization an ideal method of securing data at rest in systems that manage large volumes of data.

### Federation

A further method of preventing unwanted access to data at rest is the use of data federation[11](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-11) especially when data is distributed globally (e.g. in off-shore archives). An example of this would be a European organisation which stores its archived data off-site in the US. Under the terms of the [USA PATRIOT Act](https://en.wikipedia.org/wiki/USA_PATRIOT_Act "USA PATRIOT Act")[12](https://en.wikipedia.org/wiki/Data_at_rest#cite_note-12) the American authorities can demand access to all data physically stored within its boundaries, even if it includes personal information on European citizens with no connections to the US. Data encryption alone cannot be used to prevent this as the authorities have the right to demand decrypted information. A data federation policy which retains personal citizen information with no foreign connections within its country of origin (separate from information which is either not personal or is relevant to off-shore authorities) is one option to address this concern. However, data stored in foreign countries can be accessed using legislation in the CLOUD Act.