---
date: 2023-03-20 22:53
title: ecryptfs-simple
tags:
- ecryptfs
---

# ecryptfs-simple



## Warning

**This occurred in 2012. I have not checked ecryptfs since. The code may have improved since then.**

I have lost data with eCryptFS and can no longer recommend it. As far as I can tell, this is **not** due to `ecryptfs-simple`. All of the encryption and decryption is handled by eCryptFS, and data loss due to resulting input/output errors eith eCryptFS alone abound online.

This may be due to some common user error[1](https://xyne.dev/projects/ecryptfs-simple/#fn1), but it is nevertheless too great a risk for important data. I have switched back to using EncFS for my data.

## About

`ecryptfs-simple` is a utility for users that want to work with eCryptfs in the simplest way possible. It allows users to mount and unmount eCryptfs directories if they have full permissions on the source and target directories. `ecryptfs-simple` requires neither superuser privileges nor entries in fstab. Unlike the utilities in the ecryptfs-utils package, there are no hard-coded paths (e.g. ~/.Private).

The idea is to make eCryptfs as easy to use as EncFS.

See the ecryptfs-simple help message below for more information.

## Options

`ecryptfs-simple --help` will list command-line options. See `man ecryptfs` for mount options.

## Automounting

`ecryptfs-simple` provides an “automount” option. The first time it is invoked with this option, it will present the same mount option prompt as `mount.ecryptfs`. Subsequent mounts with this option will reload options selected during the first mount.

The mount options are stored in the configuration directory, which is located at `$XDG_CONFIG_HOME/ecryptfs-simple` if `$XDG_CONFIG_HOME` is set else at `$HOME/.config/ecryptfs-simple`. The canonicalized file path of the source directory is hashed with the SHA512 algorithm to generate a filename. If you need to get the name of the config path for a given encrypted directory, use the `--print-config-path` option.

Options passed in with the `-o` flag while automounting take precedence. To override flag options such as `no_sig_cache`, use the `-x` (exclude) option. This will also prevent them from being written to the configuration file. Options can also be removed by manually editing the configuration file for the given path.

## Technical Details

### Kernel Module

Make sure that you load the `ecryptfs` kernel module before using `ecryptfs-simple`. If you intend to use `ecryptfs-simple` regulary, add `ecryptfs` to the `MODULES` array in /etc/rc.conf. Otherwise, the module can be loaded during a session with

```
modprobe ecryptfs
```

### Security

`ecryptfs-simple` should be owned by root and have the setuid flag enabled. It will check that the real user has full permissions to both the directory and mountpoint for the requested operation. It will refuse to mount a directory on an existing mountpoint and it will only unmount ecryptfs directories.

The key ring and mounting is handled by code from `mount.ecryptfs.c` in the ecryptfs-utils package. This may change in the future but at the time of writing it made more sense to me to re-use the existing code. My reasoning was that the code has been in use for a while and _should_ therefore have been scrutinized and tested.

### Key Management

Mounting is handled by code in `mount.ecryptfs.c` from the ecryptfs-utils package, which manages keys as needed. Unmounting, however, does not. Use `keyctl` to manage keys if necessary after unmounting. For example, to list all keys in the user-defined keyring, run

```
$ keyctl list @u
```

If the keys are no longer needs, they can be removed with `keyctl unlink...` or `keyctl purge...`, for example.

### Troubleshooting

#### ecryptfs-simple segfaults

This may happen if the `ecryptfs` module is not loaded. Check the output of

```
lsmod | grep ecryptfs
```

If the `ecryptfs` is not listed, load it with

```
modprobe ecryptfs
```

You should probably add it to a file in /etc/modules-load.d/ to ensure that it is loaded automatically at startup.

If the module is listed then the kernel may have been updated without a reboot. In this case, try reloading the module:

```
modprobe -r ecryptfs
modprobe ecryptfs
```

It may be a good idea to unmount other ecryptfs mountpoints before reloading the module.

If that still does not solve the problem, try rebuilding the ecryptfs-simple package using the provided PKGBUILD (check the nav links or get it from the AUR).

## Related

[SiriKali](https://mhogomchungu.github.io/sirikali/) provides a GUI frontend to ecryptfs-simple and other encrypted directory managers. I have not used it.

## CHANGELOG

### 2016-11-14

-   Improved buffer handling.
-   Added option to unlink key from keyring when unmounting.

### 2016-11-13

-   Added option to set configuration file path, including relative paths to the encrypted directory.
-   Corrected inclusion of ecryptfs_enable_filename_crypto to configuration file.
-   Avoid parameter prompt when not running from a terminal.
-   Added support for CMake (prompted by mhogomchungu: https://github.com/mhogomchungu/ecryptfs-simple)

### 2012-06-25

Major backend update: the upstream ecryptfs-utils were changed in such a way that hooks were no longer possible, so the functionality was recreated directly in ecryptfs-utils.

### 2012-06-17

Major update: added code to handle parameters and hook directly into mount.ecryptfs.c.

#### Notable Changes

-   added proper command-line options
-   added option saving for automatic remounts without prompting
-   both mountpoints and source directories can now be given when unmounting

1.  The only thing that comes to mind right now is that it may be due to upgrading the kernel while eCryptFS files are exposed, but I have no idea if this is plausible and I have not tested it.[↩︎](https://xyne.dev/projects/ecryptfs-simple/#fnref1)