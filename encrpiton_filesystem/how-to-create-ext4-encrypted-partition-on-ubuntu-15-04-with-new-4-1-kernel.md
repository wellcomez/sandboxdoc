---
date: 2023-05-29 17:08
title: how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel
tags:
- ext4-encrption
---



# How to create ext4 encrypted partition on Ubuntu 15.04 with new 4.1 kernel?

Asked 7 years, 11 months ago

Modified [5 years, 5 months ago](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel?lastactivity "2017-12-03 09:13:15Z")

Viewed 23k times

11

[](https://askubuntu.com/posts/643577/timeline)

Can I create a new `ext4` encrypted partition with kernel 4.1 on Ubuntu 15.04?

- [15.04](https://askubuntu.com/questions/tagged/15.04 "show questions tagged '15.04'")
- [encryption](https://askubuntu.com/questions/tagged/encryption "show questions tagged 'encryption'")
- [ext4](https://askubuntu.com/questions/tagged/ext4 "show questions tagged 'ext4'")

[Share](https://askubuntu.com/q/643577 "Short permalink to this question")

[Improve this question](https://askubuntu.com/posts/643577/edit)

Follow

[edited Jul 2, 2015 at 19:27](https://askubuntu.com/posts/643577/revisions "show all edits to this post")

[![Tim's user avatar](https://www.gravatar.com/avatar/cad89a877102a31fd4642bfd88e3e9a6?s=64&d=identicon&r=PG)$@[Tim](https://askubuntu.com/users/186134/tim)

32k2525 gold badges116116 silver badges177177 bronze badges

asked Jul 2, 2015 at 13:11

[![Oto_Shan's user avatar](https://www.gravatar.com/avatar/95d9ad3ce01bc4dedb0b83fa3e83401a?s=64&d=identicon&r=PG&f=y&so-version=2)$@[Oto_Shan](https://askubuntu.com/users/313718/oto-shan)

11311 gold badge11 silver badge66 bronze badges

- 2
    
    possible duplicate of [How to create an encrypted partition?](http://askubuntu.com/questions/12576/how-to-create-an-encrypted-partition) 
    
    – [Ron](https://askubuntu.com/users/402004/ron "20,388 reputation")
    
     [Jul 3, 2015 at 11:09](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment922448_643577)
    
- This is not a duplicate as none of the existing answers are about the particular feature added in kernel 4.1, reference: [phoronix.com/…](https://www.phoronix.com/scan.php?page=news_item&px=EXT4-Changes-Linux-4.1) 
    
    – [LiveWireBT](https://askubuntu.com/users/40581/livewirebt "28,315 reputation")
    
     [Feb 28, 2016 at 13:24](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment1100363_643577)
    

[Add a comment](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

## 2 Answers

Sorted by:

                                              Highest score (default)                                                                   Date modified (newest first)                                                                   Date created (oldest first)                              

11

[](https://askubuntu.com/posts/689036/timeline)

First off a disclaimer: I've not done this with Ubuntu, but on a machine with Debian "Stretch" installed using a custom Linux 4.2.3 kernel that I enabled `EXT4_FS_ENCRYPTION` on.

The instructions given by **kmeaw** don't work for me exactly as posted. A few things were left out (command line parameters and steps).

- Update `e2fsprogs` as shown above
- Generate your random salt. I used the following to store it in a "safe place":
    
    ```
    head -c 16 /dev/urandom | xxd -p >~/tmp-salt.txt
    echo 0x`cat ~/tmp-salt.txt` >~/.cryptoSalt
    ```
    
- In order to use ext4 encryption on the file system, the "encrypt" flag must be set in the super-block. This is not the default when the ext4 file system is created. Using the "tune2fs" utility from e2fsprogs 1.43 or later, set the "encrypt" option:
    
    ```
    sudo tune2fs -O encrypt /dev/sda4
    ```
    
- Mount or remount the file system so the kernel knows about the change (maybe it's automatic, but I have only done this on a separate partition, so I'm not sure.)
    
- Create a directory on the file system that is mounted with encryption enabled:
    
    ```
    sudo mkdir -p /secret/home/$USER
    sudo chown $USER:$USER /secret/home/$USER
    ```
    
- Create the key in the keyring and use it to set the policy for the directory to be encrypted (the `sudo` command is not needed here):
    
    ```
    $ /usr/sbin/e4crypt add_key -S s:`cat ~/.cryptoSalt`
    Enter passphrase (echo disabled):
    Added key with descriptor [0132fed69f946c86]
    $ /usr/bin/e4crypt set_policy 0132fed69f946c86 /secret/home/$USER
    Key with descriptor [0132fed69f946c86] applies to /secret/home/theuser.
    ```
    
- After each reboot, the `add_key` command can be used set the key for decryption of the directory and its descendants:
    
    ```
    $ /usr/sbin/e4crypt add_key -S s:`cat ~/.cryptoSalt`
    Enter passphrase (echo disabled):
    Added key with descriptor [0132fed69f946c86]
    ```
    
    Enter the same password used in the previous step, and you don't have to remember the descriptor hex string.
    
- You can also use `add_key` directly. This will use a filesystem specific salt (So all folders under that partition will have the same salt)
    
    ```
    $ /usr/sbin/e4crypt add_key /secret/home/$USER
    Added key with descriptor [0132fed69f946c86]
    Key with descriptor [0132fed69f946c86] applies to /secret/home/theuser.
    ```
    

[Share](https://askubuntu.com/a/689036 "Short permalink to this answer")

[Improve this answer](https://askubuntu.com/posts/689036/edit)

Follow

[edited Dec 3, 2017 at 9:13](https://askubuntu.com/posts/689036/revisions "show all edits to this post")

[![J V's user avatar](https://www.gravatar.com/avatar/8bd5cf5f39b912777773d6a0110aeaec?s=64&d=identicon&r=PG)](https://askubuntu.com/users/143322/j-v)[J V](https://askubuntu.com/users/143322/j-v)

10366 bronze badges

answered Oct 23, 2015 at 18:49

[![Daniel Glasser's user avatar](https://i.stack.imgur.com/kBHgP.jpg?s=64&g=1)](https://askubuntu.com/users/195851/daniel-glasser)[Daniel Glasser](https://askubuntu.com/users/195851/daniel-glasser)

35811 gold badge33 silver badges1010 bronze badges

- The article in the Arch wiki suggests to read the following blog post, whose author wrote a program to simplify some of the steps: [blog.quarkslab.com/…](http://blog.quarkslab.com/a-glimpse-of-ext4-filesystem-level-encryption.html) At the time of writing 16.04 is still in development and has the two corresponding kernel configuration options set, e2fsprogs though is still at version 1.42. 
    
    – [LiveWireBT](https://askubuntu.com/users/40581/livewirebt "28,315 reputation")
    
     [Feb 28, 2016 at 13:29](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment1100365_689036)
    
- Well... I tried those instructions and my folder is blocked... on a `cp .. /secret/home/$USER/.` I get: _«cp: cannot create regular file '/secret/home/alexis/test-top-secret-image.svg': Required key not available»_. I just followed your instruction so I guess something changed. 
    
    – [Alexis Wilke](https://askubuntu.com/users/31366/alexis-wilke "2,567 reputation")
    
     [Oct 20, 2019 at 23:46](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment1975582_689036)
    
- The second instance of "`/usr/bin/e4crypt`" is missing the "s" in "/[s]bin". 
    
    – [Alexis Wilke](https://askubuntu.com/users/31366/alexis-wilke "2,567 reputation")
    
     [Oct 20, 2019 at 23:47](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment1975583_689036)
    

[Add a comment](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

3

[](https://askubuntu.com/posts/646032/timeline)

Linux 4.1 comes with a new Ext4 feature to encrypt directories of a filesystem. Encryption keys are stored in the keyring. To get started, make sure you have enabled `CONFIG_KEYS` and `CONFIG_EXT4_FS_ENCRYPTION` kernel options and you have kernel 4.1 or higher.

First of all, you need to update e2fsprogs to at least version 1.43, which is still WIP at the time of writing so we need to fetch it from the [git repository](https://git.kernel.org/cgit/fs/ext2/e2fsprogs.git/):

```
$ git clone git://git.kernel.org/pub/scm/fs/ext2/e2fsprogs.git
```

e4crypt source has disabled a relevant section in its source code, enable it by editing misc/e4crypt.c and removing these two lines near line 714:

```
    printf("arg %s\n", argv[optind]);
    exit(0);
```

Now build and install new e2fsprogs:

```
$ sudo apt-get install devscripts build-essential gettext texinfo pkg-config debhelper m4
$ debuild
$ sudo dpkg -i e2fsprogs_1.43-WIP_amd64.deb
```

Check your version now, it should be 1.43-WIP:

```
# e2fsck -V
e2fsck 1.43-WIP (18-May-2015)
        Using EXT2FS Library version 1.43-WIP, 18-May-2015
```

To work with keys, we need to install the `keyutils` package:

```
$ sudo apt-get install keyutils
```

Let's make a directory that we will encrypt. Encryption policy can be set only on empty directories:

```
$ sudo mkdir -p /encrypted/dir
```

First generate a random salt value and store it in a safe place:

```
$ head -c 16 /dev/random | xxd -p
877282f53bd0adbbef92142fc4cac459
```

Now generate and add a new key into your keyring: this step should be repeated every time you flush your keychain (reboot)

```
$ sudo e4crypt -S 0x877282f53bd0adbbef92142fc4cac459
Enter passphrase (echo disabled): 
Added key with descriptor [f88747555a6115f5]
```

Now you know a descriptor for your key. Make sure you have added a key into your keychain:

```
$ keyctl show
Session Keyring
1021618178 --alswrv   1000  1000  keyring: _ses
 176349519 --alsw-v   1000  1000   \_ logon: ext4:f88747555a6115f5
```

Almost done. Now set an encryption policy for a directory:

```
$ e4crypt set_policy f88747555a6115f5 /encrypted/dir
```

That's all. If you try accessing the disk without adding a key into keychain, filenames and their contents will be seen as encrypted gibberish. Be careful running old versions of e2fsck on your filesystem - it will treat encrypted filenames as invalid.

[Share](https://askubuntu.com/a/646032 "Short permalink to this answer")

[Improve this answer](https://askubuntu.com/posts/646032/edit)

Follow

[edited Feb 28, 2016 at 13:19](https://askubuntu.com/posts/646032/revisions "show all edits to this post")

[![LiveWireBT's user avatar](https://i.stack.imgur.com/ZE1zQ.png?s=64&g=1)](https://askubuntu.com/users/40581/livewirebt)[LiveWireBT](https://askubuntu.com/users/40581/livewirebt)

28.3k2626 gold badges107107 silver badges219219 bronze badges

answered Jul 8, 2015 at 17:14

[![kmeaw's user avatar](https://www.gravatar.com/avatar/4ac3576c1e7cb3b0f5ab96a633cea192?s=64&d=identicon&r=PG&f=y&so-version=2)](https://askubuntu.com/users/427699/kmeaw)[kmeaw](https://askubuntu.com/users/427699/kmeaw)

3911 bronze badge

- This answer had severe formatting issues. I tried to improve as far as I could, but it doesn't make sense to me why one would obviously attempt to use markdown formatting, handle complex commands and also do a similar edit in the Arch wiki at the same time, but then look at the garbage rendered in the preview and decide to leave it as it is. There is no satisfying explanation or further reference why the lines have to be removed. Further it's suggested to use debuild in a way it's not intended (with a non-debianized package, there is an experimental package in Debian) and doesn't work. -1 
    
    – [LiveWireBT](https://askubuntu.com/users/40581/livewirebt "28,315 reputation")
    
     [Feb 28, 2016 at 13:19](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel#comment1100360_646032)
    

[Add a comment](https://askubuntu.com/questions/643577/how-to-create-ext4-encrypted-partition-on-ubuntu-15-04-with-new-4-1-kernel# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

