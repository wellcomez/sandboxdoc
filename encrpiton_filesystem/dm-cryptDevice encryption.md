---
date: 2023-03-24 09:59
title: dm-cryptDevice encryption
tags:
- ecryptfs
- Encryption
- cryptsetup
- dm-crypt
---
# dm-crypt/Device encryption



This section covers how to manually utilize _dm-crypt_ from the command line to encrypt a system.

## Preparation

Before using [cryptsetup](https://archlinux.org/packages/?name=cryptsetup), always make sure the `dm_crypt` [kernel module](https://wiki.archlinux.org/title/Kernel_module "Kernel module") is loaded.

## Cryptsetup usage

_Cryptsetup_ is the command line tool to interface with _dm-crypt_ for creating, accessing and managing encrypted devices. The tool was later expanded to support different encryption types that rely on the Linux kernel **d**evice-**m**apper and the **crypt**ographic modules. The most notable expansion was for the Linux Unified Key Setup (LUKS) extension, which stores all of the needed setup information for dm-crypt on the disk itself and abstracts partition and key management in an attempt to improve ease of use. Devices accessed via the device-mapper are called block devices. For further information see [Data-at-rest encryption#Block device encryption](https://wiki.archlinux.org/title/Data-at-rest_encryption#Block_device_encryption "Data-at-rest encryption").

The tool is used as follows:

~~~
# cryptsetup _OPTIONS_ _action_ _action-specific-options_ _device_ _dmname_
~~~

It has compiled-in defaults for the options and the encryption mode, which will be used if no others are specified on the command line. Have a look at

~~~
$ cryptsetup --help 
~~~

which lists options, actions and the default parameters for the encryption modes in that order. A full list of options can be found on the man page. Since different parameters are required or optional, depending on encryption mode and action, the following sections point out differences further. Block device encryption is fast, but speed matters a lot too. Since changing an encryption cipher of a block device after setup is difficult, it is important to check _dm-crypt_ performance for the individual parameters in advance:

~~~
$ cryptsetup benchmark 
~~~

can give guidance on deciding for an algorithm and key-size prior to installation. If certain AES ciphers excel with a considerable higher throughput, these are probably the ones with hardware support in the CPU.

**Tip:** You may want to practise encrypting a virtual hard drive in a [virtual machine](https://wiki.archlinux.org/title/Virtual_machine "Virtual machine") when learning.

### Cryptsetup passphrases and keys

An encrypted block device is protected by a key. A key is either:

-   a passphrase: see [Security#Passwords](https://wiki.archlinux.org/title/Security#Passwords "Security").
-   a keyfile, see [\#Keyfiles](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Keyfiles).

Both key types have default maximum sizes: passphrases can be up to 512 characters and keyfiles up to 8192 KiB.

An important distinction of _LUKS_ to note at this point is that the key is used to unlock the master-key of a LUKS-encrypted device and can be changed with root access. Other encryption modes do not support changing the key after setup, because they do not employ a master-key for the encryption. See [Data-at-rest encryption#Block device encryption](https://wiki.archlinux.org/title/Data-at-rest_encryption#Block_device_encryption "Data-at-rest encryption") for details.

## Encryption options with dm-crypt

_Cryptsetup_ supports different encryption operating modes to use with _dm-crypt_:

-   `--type luks` for using the default LUKS format version (LUKS1 with [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) < 2.1.0, LUKS2 with [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) ≥ 2.1.0),
-   `--type luks1` for using LUKS1, the most common version of LUKS,
-   `--type luks2` for using LUKS2, the latest available version of LUKS that allows additional extensions,
-   `--type plain` for using dm-crypt plain mode,
-   `--type loopaes` for a loopaes legacy mode,
-   `--type tcrypt` for a [TrueCrypt](https://wiki.archlinux.org/title/TrueCrypt "TrueCrypt") compatibility mode.
-   `--type bitlk` for a [BitLocker](https://en.wikipedia.org/wiki/BitLocker "wikipedia:BitLocker") compatibility mode. See [cryptsetup(8) § BITLK (WINDOWS BITLOCKER COMPATIBLE) EXTENSION](https://man.archlinux.org/man/cryptsetup.8#BITLK_(WINDOWS_BITLOCKER_COMPATIBLE)_EXTENSION).

The basic cryptographic options for encryption cipher and hashes available can be used for all modes and rely on the kernel cryptographic backend features. All that are loaded and available to use as options at runtime can be viewed with:
~~~
$ less /proc/crypto 
~~~
 
**Tip:** If the list is short, execute `$ cryptsetup benchmark` which will trigger loading available modules.

The following introduces encryption options for the `luks`, `luks1`, `luks2` and `plain` modes. Note that the tables list options used in the respective examples in this article and not all available ones.

### Encryption options for LUKS mode

The _cryptsetup_ action to set up a new dm-crypt device in LUKS encryption mode is `luksFormat`. Unlike what the name implies, it does not format the device, but sets up the LUKS device header and encrypts the master-key with the desired cryptographic options.

In order to create a new LUKS container with the compiled-in defaults listed by `cryptsetup --help`, simply execute:

~~~
# cryptsetup luksFormat _device_
~~~

As of cryptsetup 2.4.0, this is equivalent to:
~~~
# cryptsetup --type luks2 --cipher aes-xts-plain64 --hash sha256 --iter-time 2000 --key-size 256 --pbkdf argon2id --use-urandom --verify-passphrase luksFormat _device_

~~~
Defaults are compared with a cryptographically higher specification example in the table below, with accompanying comments:
| Options | Cryptsetup 2.1.0 defaults | Example | Comment|
|---|---|---|---|
| --cipher-c| aes-xts-plain64| aes-xts-plain64 | Release 1.6.0 changed the defaults to an AES cipher in XTS mode (see item 5.16 of the FAQ). It is advised against using the previous default --cipher aes-cbc-essiv because of its known issues and practical attacks against them.|
| --key-size -s| 256 (512 for XTS | 512 | By default a 512 bit key-size is used for XTS ciphers. Note however that XTS splits the supplied key in half, so this results in AES-256 being used. |
| --hash -h  | sha256  | sha512 | Hash algorithm used for key derivation. Release 1.7.0 changed defaults from sha1 to sha256 "not for security reasons [but] mainly to prevent compatibility problems on hardened systems where SHA1 is already [being] phased out"[1]. The former default of sha1 can still be used for compatibility with older versions of cryptsetup since it is considered secure (see item 5.20).|
| --iter-time -i | 2000 | 5000 | Number of milliseconds to spend with PBKDF2 passphrase processing. Release 1.7.0 changed defaults from 1000 to 2000 to "try to keep PBKDF2 iteration count still high enough and also still acceptable for users."[2]. This option is only relevant for LUKS operations that set or change passphrases, such as luksFormat or luksAddKey. Specifying 0 as parameter selects the compiled-in default. |
| --use-urandom  | --use-urandom  | --use-random | Selects which random number generator to use. Note that /dev/random blocking pool has been removed. Therefore, --use-random flag is now equivalent to --use-urandom. |
| --verify-passphrase -y | Yes | - | Enabled by default in Arch Linux for luksFormat and luksAddKey.  |



The properties of LUKS features and options are described in the [LUKS1](https://gitlab.com/cryptsetup/cryptsetup/wikis/Specification) (pdf) and [LUKS2](https://gitlab.com/cryptsetup/cryptsetup/blob/master/docs/on-disk-format-luks2.pdf) (pdf) specifications.

**Tip:** The project developers' [devconfcz2016](https://mbroz.fedorapeople.org/talks/DevConf2016/devconf2016-luks2.pdf) (pdf) presentation summarizes the motivation for the major specification update to LUKS2.

#### Iteration time

From [cryptsetup FAQ§2.1](https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#2-setup) and [§3.4](https://gitlab.com/cryptsetup/cryptsetup/-/wikis/FrequentlyAskedQuestions#3-common-problems):

The unlock time for a key-slot [...] is calculated when setting a passphrase. By default it is 1 second (2 seconds for LUKS2). [...]

Passphrase iteration count is based on time and hence security level depends on CPU power of the system the LUKS container is created on. [...]

If you set a passphrase on a fast machine and then unlock it on a slow machine, the unlocking time can be much longer.

As such, it is better to always create a container on the machine where it will be most often accessed.

Read the rest of those sections for advice on how to correctly adjust the iteration count should the need arise.

#### Sector size

See [Advanced Format#dm-crypt](https://wiki.archlinux.org/title/Advanced_Format#dm-crypt "Advanced Format").

### Encryption options for plain mode

In dm-crypt _plain_ mode, there is no master-key on the device, hence, there is no need to set it up. Instead the encryption options to be employed are used directly to create the mapping between an encrypted disk and a named device. The mapping can be created against a partition or a full device. In the latter case not even a partition table is needed.

**This article or section needs expansion.**

**Reason:** Using the defaults is not safe as they can change in future cryptsetup releases. [cryptsetup 2.6.0 changelog](https://www.kernel.org/pub/linux/utils/cryptsetup/v2.6/v2.6.0-ReleaseNotes) says the the next release will have a backward incompatible change. (Discuss in [Talk:Dm-crypt/Device encryption](https://wiki.archlinux.org/title/Talk:Dm-crypt/Device_encryption))

To create a _plain_ mode mapping with cryptsetup's default parameters:

~~~
# cryptsetup _options_ open --type plain _device_ _dmname_
~~~

Executing it will prompt for a password, which should have very high entropy. Below a comparison of default parameters with the example in [dm-crypt/Encrypting an entire system#Plain dm-crypt](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Plain_dm-crypt "Dm-crypt/Encrypting an entire system").


| Option | Cryptsetup 2.1.0 defaults | Example | Comment |
| --- | --- | --- | --- | 
| --hash -h | ripemd160 | - | The hash is used to create the key from the passphrase; it is not used on a keyfile. | 
| --cipher -c | aes-cbc-essiv:sha256 | aes-xts-plain64 | The cipher consists of three parts: cipher-chainmode-IV generator. Please see Data-at-rest encryption#Ciphers and modes of operation for an explanation of these settings, and the DMCrypt documentation for some of the options available. | 
| --key-size -s | 256 | 512 | The key size (in bits). The size will depend on the cipher being used and also the chainmode in use. Xts mode requires twice the key size of cbc. | | --size -b | real size of target disk | 2048 (mapped device will be 512B×2048=1MiB) | Limit the maximum size of the device (in 512-byte sectors). | 
| --offset -o | 0 | 0 | The offset from the beginning of the target disk (in 512-byte sectors) from which to start the mapping. | | --skip -p | 0 | 2048 (512B×2048=1MiB will be skipped) | The number of 512-byte sectors of encrypted data to skip at the beginning. |
| --key-file -d | default uses a passphrase | /dev/sdZ (or e.g. /boot/keyfile.enc) | The device or file to be used as a key. See #Keyfiles for further details. | 
| --keyfile-offset | 0 | 0 | Offset from the beginning of the file where the key starts (in bytes). This option is supported from cryptsetup 1.6.7 onwards. | | --keyfile-size -l | 8192kB | - (default applies) | Limits the bytes read from the key file. This option is supported from cryptsetup 1.6.7 onwards. |


Using the device `/dev/sd_X_`, the above right column example results in:

~~~
# cryptsetup --cipher=aes-xts-plain64 --offset=0 --key-file=/dev/sd_Z_ --key-size=512 open --type=plain /dev/sdX enc
~~~

Unlike encrypting with LUKS, the above command must be executed _in full_ whenever the mapping needs to be re-established, so it is important to remember the cipher, hash and key file details. We can now check that the mapping has been made:

~~~
# fdisk -l
~~~

An entry should now exist for `/dev/mapper/enc`.

## Encrypting devices with cryptsetup

This section shows how to employ the options for creating new encrypted block devices and accessing them manually.

**Warning:** GRUB's support for LUKS2 is limited; see [GRUB#Encrypted /boot](https://wiki.archlinux.org/title/GRUB#Encrypted_/boot "GRUB") for details. Use LUKS1 (`cryptsetup luksFormat --type luks1`) for partitions that GRUB will need to unlock.

### Encrypting devices with LUKS mode

#### Formatting LUKS partitions

In order to setup a partition as an encrypted LUKS partition execute:

~~~
# cryptsetup luksFormat _device_
~~~

You will then be prompted to enter a password and verify it.

See [\#Encryption options for LUKS mode](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode) for command line options.

You can check the results with:

~~~
# cryptsetup luksDump _device_
~~~

You will note that the dump not only shows the cipher header information, but also the key-slots in use for the LUKS partition.

The following example will create an encrypted root partition on `/dev/sda1` using the default AES cipher in XTS mode with an effective 256-bit encryption

~~~
# cryptsetup -s 512 luksFormat /dev/sda1
~~~

##### Using LUKS to format partitions with a keyfile

When creating a new LUKS encrypted partition, a keyfile may be associated with the partition on its creation using:

~~~
# cryptsetup luksFormat _device_ _/path/to/mykeyfile_
~~~

See [\#Keyfiles](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Keyfiles) for instructions on how to generate and manage keyfiles.

#### Unlocking/Mapping LUKS partitions with the device mapper

Once the LUKS partitions have been created, they can then be unlocked.

The unlocking process will map the partitions to a new device name using the device mapper. This alerts the kernel that `_device_` is actually an encrypted device and should be addressed through LUKS using the `/dev/mapper/_dm_name_` so as not to overwrite the encrypted data. To guard against accidental overwriting, read about the possibilities to [backup the cryptheader](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Backup_and_restore) after finishing setup.

In order to open an encrypted LUKS partition execute:
~~~
# cryptsetup open _device_ _dm_name_
~~~

You will then be prompted for the password to unlock the partition. Usually the device mapped name is descriptive of the function of the partition that is mapped. For example the following unlocks a root luks partition `/dev/sda1` and maps it to device mapper named `root`:

~~~
# cryptsetup open /dev/sda1 root 
~~~

Once opened, the root partition device address would be `/dev/mapper/root` instead of the partition (e.g. `/dev/sda1`).

For setting up LVM ontop the encryption layer the device file for the decrypted volume group would be anything like `/dev/mapper/root` instead of `/dev/sda1`. LVM will then give additional names to all logical volumes created, e.g. `/dev/lvmpool/root` and `/dev/lvmpool/swap`.

In order to write encrypted data into the partition it must be accessed through the device mapped name. The first step of access will typically be to [create a file system](https://wiki.archlinux.org/title/Create_a_file_system "Create a file system"). For example:

~~~
# mkfs -t ext4 /dev/mapper/root
~~~

The device `/dev/mapper/root` can then be [mounted](https://wiki.archlinux.org/title/Mount "Mount") like any other partition.

To close the LUKS container, unmount the partition and do:

~~~
# cryptsetup close root
~~~

#### Using a TPM to store keys

See [Trusted Platform Module#Data-at-rest encryption with LUKS](https://wiki.archlinux.org/title/Trusted_Platform_Module#Data-at-rest_encryption_with_LUKS "Trusted Platform Module").

### Encrypting devices with plain mode

The creation and subsequent access of a _dm-crypt_ plain mode encryption both require not more than using the _cryptsetup_ `open` action with correct [parameters](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_plain_mode). The following shows that with two examples of non-root devices, but adds a quirk by stacking both (i.e. the second is created inside the first). Obviously, stacking the encryption doubles overhead. The usecase here is simply to illustrate another example of the cipher option usage.

A first mapper is created with _cryptsetup's_ plain-mode defaults, as described in the table's left column above

~~~
# cryptsetup --type plain -v open /dev/sd_xY_ plain1
~~~

Enter passphrase: 
Command successful.

Now we add the second block device inside it, using different encryption parameters and with an (optional) offset, create a file system and mount it

~~~
# cryptsetup --type plain --cipher=serpent-xts-plain64 --hash=sha256 --key-size=256 --offset=10  open /dev/mapper/plain1 plain2
~~~

Enter passphrase:

~~~
# lsblk -p

 NAME                                                     
 /dev/sda                                     
 ├─/dev/sd_xY_          
 │ └─/dev/mapper/plain1     
 │   └─/dev/mapper/plain2              
 ...

# mkfs -t ext2 /dev/mapper/plain2
# mount -t ext2 /dev/mapper/plain2 /mnt
# echo "This is stacked. one passphrase per foot to shoot." > /mnt/stacked.txt
~~~

We close the stack to check access works

~~~
# cryptsetup close plain2
# cryptsetup close plain1

First, let us try to open the file system directly:

# cryptsetup --type plain --cipher=serpent-xts-plain64 --hash=sha256 --key-size=256 --offset=10 open /dev/sd_xY_ plain2

# mount -t ext2 /dev/mapper/plain2 /mnt

mount: wrong fs type, bad option, bad superblock on /dev/mapper/plain2,
      missing codepage or helper program, or other error
~~~

Why that did not work? Because the "plain2" starting block (`10`) is still encrypted with the cipher from "plain1". It can only be accessed via the stacked mapper. The error is arbitrary though, trying a wrong passphrase or wrong options will yield the same. For _dm-crypt_ plain mode, the `open` action will not error out itself.

Trying again in correct order:
~~~

# cryptsetup close plain2    # dysfunctional mapper from previous try

# cryptsetup --type plain open /dev/sd_xY_ plain1

Enter passphrase:

# cryptsetup --type plain --cipher=serpent-xts-plain64 --hash=sha256 --key-size=256 --offset=10 open /dev/mapper/plain1 plain2

Enter passphrase:

# mount /dev/mapper/plain2 /mnt && cat /mnt/stacked.txt
~~~
This is stacked. one passphrase per foot to shoot.

_dm-crypt_ will handle stacked encryption with some mixed modes too. For example LUKS mode could be stacked on the "plain1" mapper. Its header would then be encrypted inside "plain1" when that is closed.

Available for plain mode only is the option `--shared`. With it a single device can be segmented into different non-overlapping mappers. We do that in the next example, using a _loopaes_ compatible cipher mode for "plain2" this time:

~~~
# cryptsetup --type plain --offset 0 --size 1000 open /dev/sd_xY_ plain1

Enter passphrase:

# cryptsetup --type plain --offset 1000 --size 1000 --shared --cipher=aes-cbc-lmk --hash=sha256 open /dev/sd_xY_ plain2

Enter passphrase:

# lsblk -p

NAME                    
dev/sd_xY_                    
├─/dev/sd_xY_               
│ ├─/dev/mapper/plain1     
│ └─/dev/mapper/plain2     
...

~~~
As the device tree shows both reside on the same level, i.e. are not stacked and "plain2" can be opened individually.

## Cryptsetup actions specific for LUKS

### Key management

It is possible to define additional keys for the LUKS partition. This enables the user to create access keys for safe backup storage In so-called key escrow, one key is used for daily usage, another kept in escrow to gain access to the partition in case the daily passphrase is forgotten or a keyfile is lost/damaged. A different key-slot could also be used to grant access to a partition to a user by issuing a second key and later revoking it again.

Once an encrypted partition has been created, the initial keyslot 0 is created (if no other was specified manually). Additional keyslots are numbered from 1 to 7. Which keyslots are used can be seen by issuing

~~~
# cryptsetup luksDump /dev/_device_
~~~

Where `_device_` is the block device containing the LUKS header. This and all the following commands in this section work on header backup files as well.

#### Adding LUKS keys

Adding new keyslots is accomplished with the `luksAddKey` action. For safety it will always, even for already unlocked devices, ask for a valid existing key (a passphrase for any existing slot) before a new one may be entered:

~~~
# cryptsetup luksAddKey /dev/_device_ [_/path/to/additionalkeyfile_]

Enter any passphrase:
Enter new passphrase for key slot:
Verify passphrase: 
~~~

If `_/path/to/additionalkeyfile_` is given, cryptsetup will add a new keyslot for `_additionalkeyfile_`. Otherwise it prompts for a new passphrase. To authorize the action with an existing _keyfile_, the `--key-file` or `-d` option followed by the "old" `_keyfile_` will try to unlock all available keyfile keyslots:

~~~
# cryptsetup luksAddKey /dev/_device_ [_/path/to/additionalkeyfile_] -d _/path/to/keyfile_
~~~

If it is intended to use multiple keys and change or revoke them, the `--key-slot` or `-S` option may be used to specify the slot:

~~~
# cryptsetup luksAddKey /dev/_device_ -S 6

Enter any passphrase: 
Enter new passphrase for key slot: 
Verify passphrase:

# cryptsetup luksDump /dev/sda8 | grep 'Slot 6'

Key Slot 6: ENABLED

To show an associated action in this example, we decide to change the key right away:

# cryptsetup luksChangeKey /dev/_device_ -S 6

Enter LUKS passphrase to be changed: 
Enter new LUKS passphrase:

before continuing to remove it.
~~~

#### Removing LUKS keys

There are three different actions to remove keys from the header:

-   `luksRemoveKey` removes a key by specifying its passphrase/key-file.
-   `luksKillSlot` removes a key by specifying its slot (needs another valid key). Obviously, this is extremely useful if you have forgotten a passphrase, lost a key-file, or have no access to it.
-   `luksErase` removes **all** active keys.

**Warning:**

-   All above actions can be used to irrevocably delete the last active key for an encrypted device!
-   The `luksErase` command was added in version 1.6.4 to quickly nuke access to the device. This action **will not** prompt for a valid passphrase! It will not [wipe the LUKS header](https://wiki.archlinux.org/title/Dm-crypt/Drive_preparation#Wipe_LUKS_header "Dm-crypt/Drive preparation"), but all keyslots at once and you will, therefore, not be able to regain access unless you have a valid backup of the LUKS header.

For above warning it is good to know the key we want to **keep** is valid. An easy check is to unlock the device with the `-v` option, which will specify which slot it occupies:

~~~
# cryptsetup --test-passphrase -v open /dev/_device_

Enter passphrase for /dev/_device_: 
Key slot 1 unlocked.
Command successful.
~~~

Now we can remove the key added in the previous subsection using its passphrase:

~~~
# cryptsetup luksRemoveKey /dev/_device_
~~~

Enter LUKS passphrase to be deleted:

If we had used the same passphrase for two keyslots, the first slot would be wiped now. Only executing it again would remove the second one.

Alternatively, we can specify the key slot:

~~~
# cryptsetup luksKillSlot /dev/_device_ 6
~~~

Enter any remaining LUKS passphrase:

Note that in both cases, no confirmation was required.

~~~
# cryptsetup luksDump /dev/sda8 | grep 'Slot 6'

Key Slot 6: DISABLED
~~~

To re-iterate the warning above: If the same passphrase had been used for key slots 1 and 6, both would be gone now.

### Backup and restore

If the header of a LUKS encrypted partition gets destroyed, you will not be able to decrypt your data. It is just as much of a dilemma as forgetting the passphrase or damaging a key-file used to unlock the partition. Damage may occur by your own fault while re-partitioning the disk later or by third-party programs misinterpreting the partition table. Therefore, having a backup of the header and storing it on another disk might be a good idea.

**Note:** If one of the LUKS-encrypted partitions' passphrases becomes compromised, you must revoke it on _every_ copy of the cryptheader, even those you have backed up. Otherwise, a copy of the backed-up cryptheader that uses the compromised passphrase can be used to determine the master key which in turn can be used to decrypt the associated partition (even your actual partition, not only the backed-up version). On the other hand, if the master key gets compromised, you have to reencrypt your whole partition. See [LUKS FAQ](https://gitlab.com/cryptsetup/cryptsetup/wikis/FrequentlyAskedQuestions#6-backup-and-data-recovery) for further details.

#### Backup using cryptsetup

Cryptsetup's `luksHeaderBackup` action stores a binary backup of the LUKS header and keyslot area:

~~~
# cryptsetup luksHeaderBackup /dev/_device_ --header-backup-file _/mnt/backup/file.img_

~~~
where `_device_` is the partition containing the LUKS volume.

You can also back up the plain text header into ramfs and encrypt it with e.g. [GPG](https://wiki.archlinux.org/title/GPG "GPG") before writing it to persistent storage:

~~~
# mount --mkdir -t ramfs ramfs /root/_tmp_
# cryptsetup luksHeaderBackup /dev/_device_ --header-backup-file /root/_tmp_/_file_.img
# gpg2 --recipient _User_ID_ --encrypt /root/_tmp_/_file_.img 
# cp /root/_tmp_/_file_.img.gpg /mnt/_backup_/
# umount /root/_tmp_
~~~

**Warning:** [tmpfs](https://wiki.archlinux.org/title/Tmpfs "Tmpfs") can swap to the disk in low memory situations, so it is not recommended here.

#### Restore using cryptsetup

**Warning:** Restoring the wrong header or restoring to an unencrypted partition will cause data loss! The action can not perform a check whether the header is actually the _correct_ one for that particular device.

In order to evade restoring a wrong header, you can ensure it does work by using it as a remote `--header` first:

~~~
# cryptsetup -v --header /mnt/_backup_/_file_.img open /dev/_device_ test

Key slot 0 unlocked.
Command successful.

# mount /dev/mapper/test /mnt/test && ls /mnt/test 
# umount /mnt/test 
# cryptsetup close test 

Now that the check succeeded, the restore may be performed:

# cryptsetup luksHeaderRestore /dev/_device_ --header-backup-file ./mnt/_backup_/_file_.img
~~~

Now that all the keyslot areas are overwritten; only active keyslots from the backup file are available after issuing the command.

#### Manual backup and restore

The header always resides at the beginning of the device and a backup can be performed without access to _cryptsetup_ as well. First you have to find out the payload offset of the crypted partition:

~~~
# cryptsetup luksDump /dev/_device_ | grep "Payload offset"

Payload offset:	4040
~~~

Second check the sector size of the drive
~~~

# fdisk -l /dev/_device_ | grep "Sector size"

Sector size (logical/physical): 512 bytes / 512 bytes
~~~

Now that you know the values, you can backup the header with a simple [dd](https://wiki.archlinux.org/title/Dd "Dd") command:
~~~

# dd if=/dev/_device_ of=/path/to/_file_.img bs=512 count=4040
~~~

and store it safely.

A restore can then be performed using the same values as when backing up:
~~~

# dd if=./_file_.img of=/dev/_device_ bs=512 count=4040
~~~

### Re-encrypting devices

**Reason:** cryptsetup 2.2 using LUKS2 (with a 16 MiB header) supports online encryption/decryption/reencryption.[[3]](https://mirrors.edge.kernel.org/pub/linux/utils/cryptsetup/v2.2/v2.2.0-ReleaseNotes) (Discuss in [Talk:Dm-crypt/Device encryption](https://wiki.archlinux.org/title/Talk:Dm-crypt/Device_encryption))

**This article or section is out of date.**

**Reason:** cryptsetup-reencrypt was removed in cryptsetup 2.5.0.[[4]](https://www.kernel.org/pub/linux/utils/cryptsetup/v2.5/v2.5.0-ReleaseNotes) (Discuss in [Talk:Dm-crypt/Device encryption](https://wiki.archlinux.org/title/Talk:Dm-crypt/Device_encryption))

The [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) package features two options for re-encryption.

- **cryptsetup reencrypt**
	Argument to `cryptsetup` itself: Preferred method. Currently LUKS2 devices only. Actions can be performed online. Supports multiple parallel re-encryption jobs. Resilient to system failures. See [cryptsetup(8)](https://man.archlinux.org/man/cryptsetup.8) for more information.

- **cryptsetup-reencrypt**
	Legacy tool, supports LUKS1 in addition to LUKS2. Actions can be performed on unmounted devices only. Single process at a time. Sensitive to system failures. See [cryptsetup-reencrypt(8)](https://man.archlinux.org/man/cryptsetup-reencrypt.8) for more information.

Both can be used to convert an existing unencrypted file system to a LUKS encrypted one or permanently remove LUKS encryption from a device (using `--decrypt`). As its name suggests it can also be used to re-encrypt an existing LUKS encrypted device, though, re-encryption is not possible for a detached LUKS header or other encryption modes (e.g. plain-mode). For re-encryption it is possible to change the [\#Encryption options for LUKS mode](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode).

One application of re-encryption may be to secure the data again after a passphrase or [keyfile](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Keyfiles) has been compromised _and_ one cannot be certain that no copy of the LUKS header has been obtained. For example, if only a passphrase has been shoulder-surfed but no physical/logical access to the device happened, it would be enough to change the respective passphrase/key only ([\#Key management](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Key_management)).

**Warning:** Always make sure a **reliable backup** is available and double-check options you specify before using the tool!

The following shows an example to encrypt an unencrypted file system partition and a re-encryption of an existing LUKS device.

#### Encrypt an existing unencrypted file system

**Tip:** If you are trying to encrypt an existing root partition, you might want to create a separate and unencrypted boot partition which will be mounted to `/boot` (see [Dm-crypt/Encrypting an entire system#Preparing the boot partition](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Preparing_the_boot_partition "Dm-crypt/Encrypting an entire system")). It is not strictly necessary but has a number of advantages:

-   If `/boot` is located inside an encrypted root partition, the system will ask for the passphrase twice when the machine is powered on. The first time will happen when the boot loader attempts to read the files located inside encrypted `/boot`, the second time will be when the kernel tries to mount the encrypted partition [[5]](https://opencraft.com/blog/tutorial-encrypting-an-existing-root-partition-in-ubuntu-with-dm-crypt-and-luks/). This might not be the desired behaviour and can be prevented by having a separate and unencryted boot partition.
-   Some system restore applications (e.g., [timeshift](https://aur.archlinux.org/packages/timeshift/)AUR) will not work if `/boot` is located inside an encryted partition [[6]](https://github.com/teejee2008/timeshift/issues/280).

In short, create a partition with the size of at least 260 MiB if needed. See [Partitioning#/boot](https://wiki.archlinux.org/title/Partitioning#/boot "Partitioning").

A LUKS encryption header is always stored at the beginning of the device. Since an existing file system will usually be allocated all partition sectors, the first step is to shrink it to make space for the LUKS header.

**This article or section needs expansion.**

**Reason:** cryptsetup man pages suggest using twice the LUKS2 header size. That implies 32 MiB and using `--reduce-device-size 32M` (Discuss in [Talk:Dm-crypt/Device encryption](https://wiki.archlinux.org/title/Talk:Dm-crypt/Device_encryption))

The [default](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode) LUKS2 header requires 16 MiB. If the current file system occupies all the available space, we will have to shrink it at least that much. To shrink an existing `ext4` file system on `/dev/sd_xY_` to its current possible minimum:

~~~
# umount /mnt

# e2fsck -f /dev/sd_xY_

e2fsck 1.46.5 (30-Dec-2021)
Pass 1: Checking inodes, blocks, and sizes
...
/dev/sda6: 12/166320 files (0.0% non-contiguous), 28783/665062 blocks

# resize2fs -p -M /dev/sd_xY_

e2fsck 1.46.5 (30-Dec-2021)
Resizing the filesystem on /dev/sd_xY_ to 26347 (4k) blocks.
The filesystem on /dev/sd_xY_ is now 26347 (4k) blocks long.
~~~

**Tip:** Shrinking to the minimum size with `-M` might take very long. You might want to calculate a size just 32 MiB smaller than the current size instead of using `-M`.

**Warning:** The file system should be shrunk while the underlying device (e.g., a partition) should be kept at its original size. Some graphical tools (e.g., [GParted](https://wiki.archlinux.org/title/GParted "GParted")) may resize both the file system and the partition, and data loss may occur after encryption.

Now we encrypt it, using the default cipher we do not have to specify it explicitly:

~~~
# cryptsetup reencrypt --encrypt --reduce-device-size 16M /dev/sd_xY_

WARNING!

========

This will overwrite data on LUKS2-temp-12345678-9012-3456-7890-123456789012.new irrevocably.

Are you sure? (Type 'yes' in capital letters): YES
Enter passphrase for LUKS2-temp-12345678-9012-3456-7890-123456789012.new: 
Verify passphrase: 

After it finished, the whole `/dev/sd_xY_` partition is encrypted, not only the space the file system was shrunk to. As a final step we extend the original `ext4` file system to occupy all available space again, on the now encrypted partition:

# cryptsetup open /dev/sd_xY_ recrypt

Enter passphrase for /dev/sd_xY_: 
...

# resize2fs /dev/mapper/recrypt

resize2fs 1.43-WIP (18-May-2015)
Resizing the filesystem on /dev/mapper/recrypt to 664807 (4k) blocks.
The filesystem on /dev/mapper/recrypt is now 664807 (4k) blocks long.

# mount /dev/mapper/recrypt /mnt

The file system is now ready to use. You may want to add it to your [crypttab](https://wiki.archlinux.org/title/Crypttab "Crypttab").

~~~
**Tip:** If you have just encrypted your root partition, you might need to perform a number of post-encryption adjustments. See [Dm-crypt/Encrypting an entire system#Configuring mkinitcpio](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_mkinitcpio "Dm-crypt/Encrypting an entire system"), [Dm-crypt/Encrypting an entire system#Configuring the boot loader](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_the_boot_loader "Dm-crypt/Encrypting an entire system"), and [Dm-crypt/Encrypting an entire system#Configuring GRUB](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Configuring_GRUB "Dm-crypt/Encrypting an entire system").

#### Re-encrypting an existing LUKS partition

In this example an existing LUKS device is re-encrypted.

**Warning:** Double-check you specify encryption options for correctly and _never_ re-encrypt without a **reliable backup**!

In order to re-encrypt a device with its existing encryption options, they do not need to be specified:

~~~
# cryptsetup reencrypt /dev/sd_xY_
~~~

**Note:** For LUKS1 we will need to use the legacy tool:
~~~

# cryptsetup-reencrypt /dev/sd_xY_

Existing keys are retained when re-encrypting a device with a different cipher and/or hash. Another use case is to re-encrypt LUKS devices which have non-current encryption options. Apart from above warning on specifying options correctly, the ability to change the LUKS header may also be limited by its size. For example, if the device was initially encrypted using a CBC mode cipher and 128 bit key-size, the LUKS header will be half the size of above mentioned `4096` sectors:

# cryptsetup luksDump /dev/sd_xY_ | grep -e "mode" -e "Payload" -e "MK bits"

Cipher mode:   	cbc-essiv:sha256
Payload offset:	**2048**
MK bits:       	128
~~~

While it is possible to upgrade the encryption of such a device, it is currently only feasible in two steps. First, re-encrypting with the same encryption options, but using the `--reduce-device-size` option to make further space for the larger LUKS header. Second, re-encypt the whole device again with the desired cipher. For this reason and the fact that a backup should be created in any case, creating a new, fresh encrypted device to restore into is always the faster option.

### Conversion from LUKS1 to LUKS2 and back

The [cryptsetup](https://archlinux.org/packages/?name=cryptsetup) package has `convert` option that needed for conversion between LUKS1 and LUKS2 container types. The argument `--type` is **required**.

Migration from LUKS1 to LUKS2:

~~~
# cryptsetup convert --type luks2 /dev/sd_xY_
~~~

**Note:** The LUKS header size will be 16 MiB instead of 2 MiB.

Rollback to LUKS1 (for example, to boot from [GRUB with encrypted /boot](https://wiki.archlinux.org/title/GRUB#Encrypted_/boot "GRUB")):

~~~
# cryptsetup convert --type luks1 /dev/sd_xY_
~~~

**Note:** Conversion from LUKS2 to LUKS1 is **not** always possible. You may get the following error:

Cannot convert to LUKS1 format - keyslot 0 is not LUKS1 compatible.

If the container is using Argon2, it needs to be converted to PBKDF2 to be LUKS1-compatible.

~~~
# cryptsetup luksConvertKey --pbkdf pbkdf2 /dev/sd_xY_
~~~

## Resizing encrypted devices

**This article or section needs expansion.**

**Reason:** This section should be rewritten to introduce resizing more generically. Perhaps work on it together with [Resizing LVM-on-LUKS](https://wiki.archlinux.org/title/Resizing_LVM-on-LUKS "Resizing LVM-on-LUKS"). (Discuss in [Talk:Dm-crypt/Device encryption](https://wiki.archlinux.org/title/Talk:Dm-crypt/Device_encryption))

If a storage device encrypted with dm-crypt is being cloned (with a tool like dd) to another larger device, the underlying dm-crypt device must be resized to use the whole space.

The destination device is /dev/sdX2 in this example, the whole available space adjacent to the partition will be used:

~~~
# cryptsetup luksOpen /dev/sdX2 sdX2
# cryptsetup resize sdX2
~~~

Then the underlying file system must be resized.

### Loopback file system

Assume that an encrypted loopback file system is stored in a file `/bigsecret`, looped to `/dev/loop0`, mapped to `secret` and mounted on `/mnt/secret`, as in the example at [dm-crypt/Encrypting a non-root file system#File container](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_a_non-root_file_system#File_container "Dm-crypt/Encrypting a non-root file system").

If the container file is currently mapped and/or mounted, unmount and/or close it:

~~~
# umount /mnt/secret
# cryptsetup close secret
# losetup -d /dev/loop0
~~~

Next, expand the container file with the size of the data you want to add. In this example, the file will be expanded with 1M * 1024, which is 1G.

**Warning:** Make absolutely sure to use **two** `>`, instead of just one, or else you will overwrite the file instead of appending to it. Making a backup before this step is strongly recommended.

~~~
# dd if=/dev/urandom bs=1M count=1024 | cat - >> /bigsecret
~~~

Now map the container to the loop device:

~~~
# losetup /dev/loop0 /bigsecret
# cryptsetup open /dev/loop0 secret
~~~

After this, resize the encrypted part of the container to the new maximum size of the container file:

~~~
# cryptsetup resize secret
~~~

Finally, perform a file system check and, if it is ok, resize it (example for ext2/3/4):

~~~
# e2fsck -f /dev/mapper/secret
# resize2fs /dev/mapper/secret
~~~

You can now mount the container again:

~~~
# mount /dev/mapper/secret /mnt/secret
~~~

### Integrity protected device

If the device was formatted with integrity support (e.g., `--integrity hmac-sha256`) and the backing block device is shrinked, it cannot be opened with this error: `device-mapper: reload ioctl on failed: Invalid argument`.

To fix this issue without wiping the device again, it can be formatted with the previous master key (keeping the per-sector tags valid).

~~~
# cryptsetup luksDump /dev/sdX2 --dump-master-key --master-key-file=/tmp/masterkey-in-tmpfs.key
# cryptsetup luksFormat /dev/sdX2 --type luks2 --integrity hmac-sha256 --master-key-file=/tmp/masterkey-in-tmpfs.key --integrity-no-wipe
# rm /tmp/masterkey-in-tmpfs.key
~~~

## Keyfiles

**Note:** This section describes using a plaintext keyfile. If you want to encrypt your keyfile giving you two factor authentication see [Using GPG or OpenSSL Encrypted Keyfiles](https://wiki.archlinux.org/title/Dm-crypt/Specialties#Using_GPG,_LUKS,_or_OpenSSL_Encrypted_Keyfiles "Dm-crypt/Specialties") for details, but please still read this section.

**What is a keyfile?**

A keyfile is a file whose data is used as the passphrase to unlock an encrypted volume. That means if such a file is lost or changed, decrypting the volume may no longer be possible.

**Tip:** Define a passphrase in addition to the keyfile for backup access to encrypted volumes in the event the defined keyfile is lost or changed.

**Why use a keyfile?**

There are many kinds of keyfiles. Each type of keyfile used has benefits and disadvantages summarized below:

### Types of keyfiles

#### passphrase

This is a keyfile containing a simple passphrase. The benefit of this type of keyfile is that if the file is lost the data it contained is known and hopefully easily remembered by the owner of the encrypted volume. However the disadvantage is that this does not add any security over entering a passphrase during the initial system start.

Example: `1234`

**Note:** The keyfile containing the passphrase must not have a newline in it. One option is to create it using

~~~
# echo -n 'your_passphrase' > _/path/to/keyfile_
# chown root:root _/path/to/keyfile_; chmod 400 _/path/to/keyfile_

~~~
If the file contains special characters such as a backslash, rather than escaping these, it is recommended to simply edit the key file directly entering or pasting the passphrase and then remove the trailing newline with a handy perl one-liner:

~~~
# perl -pi -e 'chomp if eof' _/path/to/keyfile_
~~~

#### randomtext

This is a keyfile containing a block of random characters. The benefit of this type of keyfile is that it is much more resistant to dictionary attacks than a simple passphrase. An additional strength of keyfiles can be utilized in this situation which is the length of data used. Since this is not a string meant to be memorized by a person for entry, it is trivial to create files containing thousands of random characters as the key. The disadvantage is that if this file is lost or changed, it will most likely not be possible to access the encrypted volume without a backup passphrase.

Example: `fjqweifj830149-57 819y4my1-38t1934yt8-91m 34co3;t8y;9p3y-`

#### binary

This is a binary file that has been defined as a keyfile. When identifying files as candidates for a keyfile, it is recommended to choose files that are relatively static such as photos, music, video clips. The benefit of these files is that they serve a dual function which can make them harder to identify as keyfiles. Instead of having a text file with a large amount of random text, the keyfile would look like a regular image file or music clip to the casual observer. The disadvantage is that if this file is lost or changed, it will most likely not be possible to access the encrypted volume without a backup passphrase. Additionally, there is a theoretical loss of randomness when compared to a randomly generated text file. This is due to the fact that images, videos and music have some intrinsic relationship between neighboring bits of data that does not exist for a random text file. However this is controversial and has never been exploited publicly.

Example: images, text, video, ...

### Creating a keyfile with random characters

#### Storing the keyfile on a file system

A keyfile can be of arbitrary content and size.

Here [dd](https://wiki.archlinux.org/title/Dd "Dd") is used to generate a keyfile of 2048 random bytes, storing it in the file `/etc/mykeyfile`:

~~~
# dd bs=512 count=4 if=/dev/random of=/etc/mykeyfile iflag=fullblock
~~~

If you are planning to store the keyfile on an external device, you can also simply change the outputfile to the corresponding directory:

~~~
# dd bs=512 count=4 if=/dev/random of=/media/usbstick/mykeyfile iflag=fullblock
~~~

To deny any access for other users than `root`:

~~~
# chmod 600 /etc/mykeyfile

~~~
##### Securely overwriting stored keyfiles

If you stored your temporary keyfile on a physical storage device, and want to delete it, remember to not just remove the keyfile later on, but use something like

~~~
# shred --remove --zero mykeyfile
~~~

to securely overwrite it. For overaged file systems like FAT or ext2 this will suffice while in the case of journaling file systems, flash memory hardware and other cases it is highly recommended to [wipe the entire device](https://wiki.archlinux.org/title/Securely_wipe_disk "Securely wipe disk").

#### Storing the keyfile in ramfs

Alternatively, you can mount a ramfs for storing the keyfile temporarily:

~~~
# mount --mkdir -t ramfs ramfs /root/myramfs
# cd /root/myramfs

~~~
The advantage is that it resides in RAM and not on a physical disk, therefore it can not be recovered after unmounting the ramfs. After copying the keyfile to another secure and persistent file system, unmount the ramfs again with

~~~
# umount /root/myramfs

~~~
### Configuring LUKS to make use of the keyfile

Add a keyslot for the keyfile to the LUKS header:

~~~
# cryptsetup luksAddKey /dev/sda2 /etc/mykeyfile

Enter any LUKS passphrase:
key slot 0 unlocked.
Command successful.

~~~
### Manually unlocking a partition using a keyfile

Use the `--key-file` option when opening the LUKS device:

~~~
# cryptsetup open /dev/sda2 _dm_name_ --key-file /etc/mykeyfile
~~~

### Unlocking the root partition at boot

This is simply a matter of configuring [mkinitcpio](https://wiki.archlinux.org/title/Mkinitcpio "Mkinitcpio") to include the necessary modules or files and configuring the [cryptkey](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#cryptkey "Dm-crypt/System configuration") [kernel parameter](https://wiki.archlinux.org/title/Kernel_parameter "Kernel parameter") to know where to find the keyfile.

Two cases are covered below:

1.  Using a keyfile stored on an external medium (e.g. a USB stick)
2.  Using a keyfile embedded in the initramfs

#### With a keyfile stored on an external media

##### Configuring mkinitcpio

You have to add the kernel module for the drive's [file system](https://wiki.archlinux.org/title/File_system "File system") to the [MODULES array](https://wiki.archlinux.org/title/Mkinitcpio#MODULES "Mkinitcpio") in `/etc/mkinitcpio.conf`. For example, add `ext4` if the file system is [Ext4](https://wiki.archlinux.org/title/Ext4 "Ext4") or `vfat` in case it is [FAT](https://wiki.archlinux.org/title/FAT "FAT"):

MODULES=(vfat)

If there are messages about bad superblock and bad codepage at boot, then you need an extra codepage module to be loaded. For instance, you may need `nls_iso8859-1` module for `iso8859-1` codepage.

[Regenerate the initramfs](https://wiki.archlinux.org/title/Regenerate_the_initramfs "Regenerate the initramfs").

##### Configuring the kernel parameters

-   For a busybox-based initramfs using the [encrypt](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#Using_encrypt_hook "Dm-crypt/System configuration") hook, see [dm-crypt/System configuration#cryptkey](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#cryptkey "Dm-crypt/System configuration").
-   For a systemd based initramfs using the [sd-encrypt](https://wiki.archlinux.org/title/Sd-encrypt "Sd-encrypt") hook, see [dm-crypt/System configuration#rd.luks.key](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#rd.luks.key "Dm-crypt/System configuration").

#### With a keyfile embedded in the initramfs

**Warning:** Use an embedded keyfile **only** if you protect the keyfile sufficiently by:

-   Using some form of authentication earlier in the boot process. Otherwise auto-decryption will occur, defeating completely the purpose of block device encryption.
-   `/boot` is encrypted. Otherwise root on a different installation (including the [live environment](https://wiki.archlinux.org/title/Installation_guide#Boot_the_live_environment "Installation guide")) can extract your key from the initramfs, and unlock the device without any other authentication.

This method allows to use a specially named keyfile that will be embedded in the [initramfs](https://wiki.archlinux.org/title/Initramfs "Initramfs") and picked up by the `encrypt` [hook](https://wiki.archlinux.org/title/Mkinitcpio#HOOKS "Mkinitcpio") to unlock the root file system (`cryptdevice`) automatically. It may be useful to apply when using the [GRUB early cryptodisk](https://wiki.archlinux.org/title/GRUB#Encrypted_/boot "GRUB") feature, in order to avoid entering two passphrases during boot.

The `encrypt` hook lets the user specify a keyfile with the `cryptkey` kernel parameter: in the case of initramfs, the syntax is `rootfs:_/path/to/keyfile_`. See [dm-crypt/System configuration#cryptkey](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#cryptkey "Dm-crypt/System configuration"). Besides, this kernel parameter defaults to use `/crypto_keyfile.bin`, and if the initramfs contains a valid key with this name, decryption will occur automatically without the need to configure the `cryptkey` parameter.

If using `sd-encrypt` instead of `encrypt`, specify the location of the keyfile with the `rd.luks.key` kernel parameter: in the case of initramfs, the syntax is `_/path/to/keyfile_`. See [dm-crypt/System configuration#rd.luks.key](https://wiki.archlinux.org/title/Dm-crypt/System_configuration#rd.luks.key "Dm-crypt/System configuration"). This kernel parameter defaults to using `/etc/cryptsetup-keys.d/_name_.key` (where `_name_` is the `_dm_name_` used for decryption in [\#Encrypting devices with cryptsetup](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encrypting_devices_with_cryptsetup)) and can be omitted if initramfs contains a valid key with this path.

[Generate the keyfile](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Creating_a_keyfile_with_random_characters), give it suitable permissions and [add it as a LUKS key](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Adding_LUKS_keys):
~~~
# dd bs=512 count=4 if=/dev/random of=/crypto_keyfile.bin iflag=fullblock
# chmod 600 /crypto_keyfile.bin
# cryptsetup luksAddKey /dev/sdX# /crypto_keyfile.bin
~~~

**Note:** The initramfs is generated by mkinitcpio with `600` [permissions](https://wiki.archlinux.org/title/Permissions "Permissions") by default, so regular users are not able to read the keyfile via the generated initramfs.

Include the key in [mkinitcpio's FILES array](https://wiki.archlinux.org/title/Mkinitcpio#BINARIES_and_FILES "Mkinitcpio"):
~~~
/etc/mkinitcpio.conf

FILES=(/crypto_keyfile.bin)
~~~

Finally [regenerate the initramfs](https://wiki.archlinux.org/title/Regenerate_the_initramfs "Regenerate the initramfs").

On the next reboot you should only have to enter your container decryption passphrase once.