
---
date: 2023-04-10 13:58
title: eCryptfs，文件系统级加密，在登出时自动为文件加密 通过挂载文件解密和卸载文件加密的方式保护文件
tags:
- ecryptfs
---



# eCryptfs，文件系统级加密，在登出时自动为文件加密。通过挂载文件解密和卸载文件加密的方式保护文件

**Table of Contents**

[一、文件目录加密与磁盘加密](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#%E4%B8%80%E3%80%81%E6%96%87%E4%BB%B6%E7%9B%AE%E5%BD%95%E5%8A%A0%E5%AF%86%E4%B8%8E%E7%A3%81%E7%9B%98%E5%8A%A0%E5%AF%86)

[1.文件目录加密](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#1.%E6%96%87%E4%BB%B6%E7%9B%AE%E5%BD%95%E5%8A%A0%E5%AF%86)

[2.磁盘加密](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#2.%E7%A3%81%E7%9B%98%E5%8A%A0%E5%AF%86)

[二、eCryptFS](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#%E4%BA%8C%E3%80%81eCryptFS)

[1、eCryptfs介绍](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#1%E3%80%81eCryptfs%E4%BB%8B%E7%BB%8D)

[2、eCrypFS架构](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#2%E3%80%81eCrypFS%E6%9E%B6%E6%9E%84)

[三、eCrypFS安装](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#%E4%B8%89%E3%80%81eCrypFS%E5%AE%89%E8%A3%85)

[四、eCryptFS的使用](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#%E5%9B%9B%E3%80%81eCryptFS%E7%9A%84%E4%BD%BF%E7%94%A8)

[1.通过挂载文件目录使用](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#1.%E9%80%9A%E8%BF%87%E6%8C%82%E8%BD%BD%E6%96%87%E4%BB%B6%E7%9B%AE%E5%BD%95%E4%BD%BF%E7%94%A8)

[2.解挂载](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#2.%E8%A7%A3%E6%8C%82%E8%BD%BD%C2%A0) 

[3.再挂载](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#3.%E5%86%8D%E6%8C%82%E8%BD%BD)

[4.问题一:输入错误的密码](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#4.%E9%97%AE%E9%A2%98%E4%B8%80%3A%E8%BE%93%E5%85%A5%E9%94%99%E8%AF%AF%E7%9A%84%E5%AF%86%E7%A0%81)

[5.问题二：未挂载前创建的文件是否可以查看](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#5.%E9%97%AE%E9%A2%98%E4%BA%8C%EF%BC%9A%E6%9C%AA%E6%8C%82%E8%BD%BD%E5%89%8D%E5%88%9B%E5%BB%BA%E7%9A%84%E6%96%87%E4%BB%B6%E6%98%AF%E5%90%A6%E5%8F%AF%E4%BB%A5%E6%9F%A5%E7%9C%8B)

[五、eCryptFS自动挂载](https://blog.csdn.net/ck784101777/article/details/104544731?spm=1001.2101.3001.6650.7&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7EBlogCommendFromBaidu%7ERate-7-104544731-blog-80317070.235%5Ev28%5Epc_relevant_t0_download&utm_relevant_index=8#%E4%BA%94%E3%80%81eCryptFS%E8%87%AA%E5%8A%A8%E6%8C%82%E8%BD%BD)

---

## 一、文件目录加密与磁盘加密

### 1.文件目录加密

我们主要有两种加密文件和目录的方法。一种是文件系统级别的加密，在这种加密中，你可以选择性地加密某些文件或者目录（如，/home/alice）。然而，文件系统级别的加密也有一些缺点。例如，许多现代应用程序会缓存（部分）文件你硬盘中未加密的部分中，比如交换分区、/tmp和/var文件夹，而这会导致隐私泄漏。

**加密方式：**

[EncFS](http://www.arg0.net/encfs)：尝试加密的最简单方式之一。EncFS工作在基于FUSE的伪文件系统上，所以你只需要创建一个加密文件夹并将它挂载到某个文件夹就可以工作了。

[eCryptFS](http://ecryptfs.org/)：一个POSIX兼容的加密文件系统，eCryptFS工作方式和EncFS相同，所以你必须挂载它。

### 2.磁盘加密

另外一种方式，就是所谓的全盘加密，这意味着整个磁盘都会被加密（可能除了主引导记录外）。全盘加密工作在物理磁盘级别，写入到磁盘的每个比特都会被加密，而从磁盘中读取的任何东西都会在运行中解密。这会阻止任何潜在的对未加密数据的未经授权的访问，并且确保整个文件系统中的所有东西都被加密，包括交换分区或任何临时缓存数据。

本篇介绍[eCryptFS](http://ecryptfs.org/)，一个基于文件系统的加密软件。

##  

## 二、[eCryptFS](http://ecryptfs.org/)

### 1、eCryptfs介绍

eCrypFS是一个基于FUSE（Filesystem in Userspace）的用户空间加密文件系统，在Linux内核2.6.19及更高版本中可用（作为encryptfs模块）。eCryptFS加密的伪文件系统是挂载到当前文件系统顶部的。它可以很好地工作在EXT文件系统家族和其它文件系统如JFS、XFS、ReiserFS、Btrfs，甚至是NFS/CIFS共享文件系统上。Ubuntu使用eCryptFS作为加密其家目录的默认方法，ChromeOS也是。在eCryptFS底层，默认使用的是AES算法，但是它也支持其它算法，如blowfish、des3、cast5、cast6。如果你是通过手工创建eCryptFS设置，你可以选择其中一种算法。

Ubuntu让我们在安装过程中选择是否加密/home目录。好吧，这是使用eCryptFS的最简单的一种方法。

### 2、eCrypFS架构

架构在在文件系统之上，VFS(virtual File System)之下。

解读以下架构，首先用户输出相关指令（也就是图上的Application），我们可以吧指令当做应用接口对底层的调用，指令穿过VFS,虚拟文件层，说明eCrypFS是在虚拟文件之下的，也就说明eCrypFS提供了对虚拟文件的支持。再往下走就是ext3，文件系统底层。我们看到eCrypFS层，这一层的核心是ＡＰＩ应用接口及程序，eCrypFS默认使用AES加密算法，加密算法将生成一段秘钥，称为Keystore，keystore回调到用户空间，eCrypFS Daemon指的是你使用eCrypFS加密的模型文件，比如你的家目录/home。无论是加密还是解密，这一流程都是一定的，不同的只是调用的接口和执行的指令不一样。在加密时会生成keystore，解密时根据你预留的密码去找keystore，在于底层的keystore比对，一致则解开文件枷锁。

![](https://img-blog.csdn.net/20180603231238328?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h4Z18yNg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

eCryptfs 采用 OpenPGP (优良保密协议,是一套用于消息加密、验证的应用程序)的文件格式存放加密文件， 对称密钥加密算法以块为单位进行加密/解密，因此 eCryptfs 将加密文件分成多个逻辑块，称为 extent。当读入一个 extent 中的任何部分的密文时，整个 extent 被读入 Page Cache，通过 Kernel Crypto API 被解密；当 extent 中的任何部分的明文数据被写回磁盘时，需要加密并写回整个 extent。

加密文件的头部存放元数据，包括元数据长度、标志位以及 EFEK 链，目前元数据的最小长度为 8192 字节。

![](https://img-blog.csdn.net/20180603231140120?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3h4Z18yNg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**加密写入**

eCryptfs Layer 创建一个新文件时，Keystore 利用内核提供的随机函数创建一个 FEK；新文件关闭时，Keystore 和 eCryptfs Daemon 合作为每个授权用户创建相应 EFEK，存放在加密文件的头部元数据中。

**解密读出**

eCryptfs Layer 首次打开一个文件时，通过下层文件系统读取该文件的头部元数据，交与 Keystore 模块进行 EFEK（加密后的 FEK）的解密。

### 三、eCrypFS安装

Ubuntu系统是默认装有eCryptFS的，当然你也可以通过下面方式安装。

**Debian，Ubuntu或其衍生版：**

> $ sudo apt-get install ecryptfs-utils

**CentOS, RHEL or Fedora：**

> # yum install ecryptfs-utils

**Arch Linux：**

> $ sudo pacman -S ecryptfs-utils

在安装完包后，加载eCryptFS内核模块（可选）

$ sudo modprobe ecryptfs

## 四、eCryptFS的使用

### 1.通过挂载文件目录使用

使用mount命令，我们通常认为的，只有磁盘块可以挂载使用，没有听说过挂载文件目录，但是我们却可以指定类型为ecryptfs来挂载文件目录使用，在挂载的时候设置密码，在取消挂载的时候，目录里的一切内容将被加密，再次挂载输入密码，如果与上一次输入的密码一致，那么挂载就成功，又可以查看目录下文件的内容了，下面通过实验演示：

> $ mkdir myfile   #创建一个目录
> 
> $ mount -t ecryptfs myfile myfile  #我标红的是需要输入的，一定要以 -t ecryptfs形式挂载  
> Passphrase:   
> Select cipher:   
>  1) aes: blocksize = 16; min keysize = 16; max keysize = 32  
>  2) blowfish: blocksize = 8; min keysize = 16; max keysize = 56  
>  3) des3_ede: blocksize = 8; min keysize = 24; max keysize = 24  
>  4) twofish: blocksize = 16; min keysize = 16; max keysize = 32  
>  5) cast6: blocksize = 16; min keysize = 16; max keysize = 32  
>  6) cast5: blocksize = 8; min keysize = 5; max keysize = 16  
> Selection [aes]:   
> Select key bytes:   
>  1) 16  
>  2) 32  
>  3) 24  
> Selection [16]:   
> Enable plaintext passthrough (y/n) [n]:   
> Enable filename encryption (y/n) [n]:   
> Attempting to mount with the following options:  
>   ecryptfs_unlink_sigs  
>   ecryptfs_key_bytes=16  
>   ecryptfs_cipher=aes  
>   ecryptfs_sig=360d0573e701851e  
> Mounted eCryptfs 

> passphrase:
> 
> （这是要你输入密码，自己编一个。一定要记住。另外密码是不会有任何显示的，输完回车就行）
> 
> select cipher: 
> 
> （选择加密方式，不选默认是[aes]。最好记住自己的选择）
> 
> select key bytes:
> 
> （选择加密位数，不选默认是[16]。最好记住自己的选择）
> 
> enable plaintext passthrough(y/n) [n]:
> 
> （是否允许使用明文，默认是 n）
> 
> enable filename encryption (y/n) [n]:
> 
> （是否把文件名也进行加密，默认是 n。如果选择y，那么在没有解密 的情况下是无法看见文件夹内部的文件的） 如果设置的密码是第一次使用，它会提示你密码被标识为[799d2f922c0f1f26] 。当然，你的密码标识肯定不会是这个。并且告诉你挂载错误，因为/root/.ecryptfs/sig-cache.txt中没有相关记录。开始让你选择：
> 
> Would you like to proceed with the mount (yes/no)? :
> 
> （你是否希望继续进行挂载。我们输入yes，来完成加密）
> 
> Would you like to append sig [799d2f922c0f1f26] to [/root/.ecryptfs/sig-cache.txt] in order to avoid this warning in the future (yes/no)?:
> 
> （你是否把密码标识加到/root/.ecryptfs/sig-cache.txt中，免得下次再报警。我们输入yes）

挂载后显示

> $ df -h   
> Filesystem      Size  Used Avail Use% Mounted on  
> udev            3.9G     0  3.9G   0% /dev  
> tmpfs           788M  9.3M  779M   2% /run  
> /dev/sda4        37G   18G   18G  50% /  
> tmpfs           3.9G  317M  3.6G   9% /dev/shm  
> tmpfs           5.0M  4.0K  5.0M   1% /run/lock  
> tmpfs           3.9G     0  3.9G   0% /sys/fs/cgroup  
> cgmfs           100K     0  100K   0% /run/cgmanager/fs  
> tmpfs           788M   56K  788M   1% /run/user/1000  
> /root/myfile     37G   18G   18G  50% /root/myfile  
>  

挂载之后，在目录下创建一些文件并写入内容

> $ echo 123 > 123.txt

### 2.解挂载 

以 -t  ecryptfs解挂载

> $ umount -t ecryptfs myfile 

再次查看文件,为乱码文件

> $ cat 123.txt
> 
> s����>����"3DUfw`�Cp��I�����{��~v�_CONSOLE6  
>             �8n�ſi�,��;����װOW����2ֳVi8�7�eԚJ�YGY�oGBP�3�zLK�O��bS�n �T���    wZí��۾t9�3�����n%t�H��H�`�y��P���Y|�Yl�n�}on��T��6a���l#Ͼ�^��4��

### 3.再挂载

如果想恢复文件内容，再次挂载，又要输入一堆东西，一定要把密码与上一次密码统一，稍后我们会讲不统一的结果。

> $ mount -t ecryptfs myfile myfile  
> Passphrase:   
> Select cipher:   
>  1) aes: blocksize = 16; min keysize = 16; max keysize = 32  
>  2) blowfish: blocksize = 8; min keysize = 16; max keysize = 56  
>  3) des3_ede: blocksize = 8; min keysize = 24; max keysize = 24  
>  4) twofish: blocksize = 16; min keysize = 16; max keysize = 32  
>  5) cast6: blocksize = 16; min keysize = 16; max keysize = 32  
>  6) cast5: blocksize = 8; min keysize = 5; max keysize = 16  
> Selection [aes]:   
> Select key bytes:   
>  1) 16  
>  2) 32  
>  3) 24  
> Selection [16]:   
> Enable plaintext passthrough (y/n) [n]:   
> Enable filename encryption (y/n) [n]:   
> Attempting to mount with the following options:  
>   ecryptfs_unlink_sigs  
>   ecryptfs_key_bytes=16  
>   ecryptfs_cipher=aes  
>   ecryptfs_sig=360d0573e701851e  
> Mounted eCryptfs

再次查看文件内容

> $ cat 123.txt
> 
> 123

### 4.问题一:输入错误的密码

我们将密码故意输错，居然发现可以挂载。但是却打不开文件，这说明：每个人都可以使用同一个文件进行挂载使用(通过ecryptfs的方式），但是只有输入自己填写的密码，才能查看自己创建的文件。

> $ mount -t ecryptfs myfile myfile 　
> 
> Passphrase:
> 
> $ cat 123.txt   
> cat: 123.txt: Input/output error

**总结两句话：**

１．错误密码可以挂载

２．挂载之后不能查看由其他人通过不同密码创建的文件

### 5.问题二：未挂载前创建的文件是否可以查看

不可以，只有在解挂载的时候才能查看。如果你挂载了目录再打开，则会把Input/output error的错误

## 五、eCryptFS自动挂载

现在，让我们开始加密一些目录，运行eCryptFS配置工具：

这条命令有相当多的选项，下面讲解

> $ ecryptfs-setup-private

![](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9pbWcxLnN5Y2RuLmltb29jLmNvbS81YmI1YWI5NTAwMDEwNWY5MDY0MDAzNDcuanBn?x-oss-process=image/format,png)

它会要求你输入登录密码和挂载密码。登录密码和你常规登录的密码一样，而挂载密码用于派生一个文件加密主密钥。这里留空可以生成一个（复杂的），这样会更安全。登出然后重新登录。

你会注意到，eCryptFS默认在你的家目录中创建了两个目录：Private和.Private。~/.Private目录包含有加密的数据，而你可以在~/Private目录中访问到相应的解密后的数据。在你登录时，~/.Private目录会自动解密并映射到~/Private目录，因此你可以访问它。当你登出时，~/Private目录会自动卸载，而~/Private目录中的内容会加密回到~/.Private目录。

eCryptFS怎么会知道你拥有~/.Private目录，并自动将其解密到~/Private目录而不需要我们输入密码呢？这就是eCryptFS的PAM模块捣的鬼，它为我们提供了这项便利服务。

如果你不想让~/Private目录在登录时自动挂载，只需要在运行ecryptfs-setup-private工具时添加“--noautomount”选项。同样，如果你不想要~/Private目录在登出后自动卸载，也可以自动“--noautoumount”选项。但是，那样后，你需要自己手工挂载或卸载~/Private目录：
~~~
$ ecryptfs-mount-private ~/.Private ~/Private
$ ecryptfs-umount-private ~/Private
~~~

你可以来验证一下.Private文件夹是否被挂载，运行：

~~~
$ mount　-l 
~~~

![](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9pbWc0LnN5Y2RuLmltb29jLmNvbS81YmI1YWI5NjAwMDFlNWM4MDY0MDAzNDcuanBn?x-oss-process=image/format,png)

现在，我们可以开始把任何敏感文件放进~/Private文件夹里头了，它们会在我们登出时自动被加密并锁在~/.Private文件内。

所有这一切看起来是那么得神奇。这主要是ecryptfs-setup-private工具让一切设置变得简单。如果你想要深究一点，对eCryptFS指定的方面进行设置，那么请转到[官方文档](http://ecryptfs.org/documentation.html)。

  