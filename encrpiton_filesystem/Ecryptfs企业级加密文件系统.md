---
date: 2023-04-10 13:57
title: Ecryptfs企业级加密文件系统
tags:
- ecryptfs
---




# Ecryptfs企业级加密文件系统

联系我：一般还是使用站内短信通知比较快。 [http://forum.ubuntu.org.cn/ucp.php?i=pm&mode=compose&u=171468](http://forum.ubuntu.org.cn/ucp.php?i=pm&mode=compose&u=171468)

cat650＠163.com

安装系统时提示是否加密主文件夹，使用的就是这个加密系统：）

快速指导：[Ecryptfs](https://wiki.ubuntu.org.cn/Ecryptfs "Ecryptfs")

## 目录

 [[隐藏](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#)] 

-   [1 什么是加密文件系统](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E4.BB.80.E4.B9.88.E6.98.AF.E5.8A.A0.E5.AF.86.E6.96.87.E4.BB.B6.E7.B3.BB.E7.BB.9F)
-   [2 安装](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E5.AE.89.E8.A3.85)
-   [3 使用举例](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E4.BD.BF.E7.94.A8.E4.B8.BE.E4.BE.8B)
-   [4 通用环境下脚本](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E9.80.9A.E7.94.A8.E7.8E.AF.E5.A2.83.E4.B8.8B.E8.84.9A.E6.9C.AC)
-   [5 kde环境下，文件管理器右键集成脚本](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#kde.E7.8E.AF.E5.A2.83.E4.B8.8B.EF.BC.8C.E6.96.87.E4.BB.B6.E7.AE.A1.E7.90.86.E5.99.A8.E5.8F.B3.E9.94.AE.E9.9B.86.E6.88.90.E8.84.9A.E6.9C.AC)
-   [6 深入使用的讨论](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E6.B7.B1.E5.85.A5.E4.BD.BF.E7.94.A8.E7.9A.84.E8.AE.A8.E8.AE.BA)
    -   [6.1 1，重复加密\混和加密](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#1.EF.BC.8C.E9.87.8D.E5.A4.8D.E5.8A.A0.E5.AF.86.5C.E6.B7.B7.E5.92.8C.E5.8A.A0.E5.AF.86)
    -   [6.2 2，把普通软件升级为 带有可加密数据库的安全软件](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#2.EF.BC.8C.E6.8A.8A.E6.99.AE.E9.80.9A.E8.BD.AF.E4.BB.B6.E5.8D.87.E7.BA.A7.E4.B8.BA_.E5.B8.A6.E6.9C.89.E5.8F.AF.E5.8A.A0.E5.AF.86.E6.95.B0.E6.8D.AE.E5.BA.93.E7.9A.84.E5.AE.89.E5.85.A8.E8.BD.AF.E4.BB.B6)
    -   [6.3 3，为网络文件夹加密](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#3.EF.BC.8C.E4.B8.BA.E7.BD.91.E7.BB.9C.E6.96.87.E4.BB.B6.E5.A4.B9.E5.8A.A0.E5.AF.86)
-   [7 延伸阅读](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E5.BB.B6.E4.BC.B8.E9.98.85.E8.AF.BB)
    -   [7.1 eCryptfs 设计目标](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#eCryptfs_.E8.AE.BE.E8.AE.A1.E7.9B.AE.E6.A0.87)
    -   [7.2 eCryptfs 的架构](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#eCryptfs_.E7.9A.84.E6.9E.B6.E6.9E.84)
    -   [7.3 eCryptfs 不足之处在于](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#eCryptfs_.E4.B8.8D.E8.B6.B3.E4.B9.8B.E5.A4.84.E5.9C.A8.E4.BA.8E)
-   [8 问与答](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#.E9.97.AE.E4.B8.8E.E7.AD.94)
    -   [8.1 No.1](https://wiki.ubuntu.org.cn/Ecryptfs%E4%BC%81%E4%B8%9A%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%96%87%E4%BB%B6%E7%B3%BB%E7%BB%9F#No.1)

## 什么是加密文件系统

保护敏感数据不被泄漏成为人们关注的热点问题。入侵者除了直接盗取物理存储设备，还可以通过网络攻击来窃夺文件数据；而且，由于共享的需求，敏感数据会由多人访问，这也增大了泄漏的可能性。对数据或文件进行加密已经成为一种公认的比较成功的保护方法。事实上，人们早已开发了许多优秀的加密算法，如 DES、AES、RSA 等，并且有一些应用程序如 crypt 使用这些加密算法，用户通过这些工具手工地完成加密、解密的工作。由于这些应用程序操作麻烦、没有和整个系统紧密地结合而且容易受到攻击，因此一般用户并不愿意使用。

加密文件系统（比如eCryptfs）通过将加密服务集成到文件系统这一层面来解决上面的问题。加密文件的内容一般经过对称密钥算法加密后以密文的形式存放在物理介质上，即使文件丢失或被窃取，在加密密钥未泄漏的情况下，非授权用户几乎无法通过密文逆向获得文件的明文，从而保证了高安全性。与此同时，授权用户对加密文件的访问则非常方便。用户通过初始身份认证后，对加密文件的访问和普通文件没有什么区别，就好像该文件并没有被加密过，这是因为加密文件系统自动地在后台做了相关的加密和解密的工作。由于加密文件系统一般工作在内核态，普通的攻击比较难于奏效。还有一类系统级加密方案是基于块设备，与它们相比，加密文件系统具有更多的优势，例如：

-   支持文件粒度的加密，也就是说，用户可以选择对哪些文件或目录加密（这些文件夹可相互包含）。而且，应用程序不用关心文件是否被加密，可以完全透明地访问加密文件。
-   无需预先保留足够的空间，用户可以随时加密或恢复文件。
-   对单个加密文件更改密钥和加密算法比较容易。
-   不同的文件可以使用不同的加密算法和密钥，增大了破解的难度。
-   只有加密文件才需要特殊的加密/解密处理，普通文件的存取没有额外开销。
-   加密文件转移到别的物理介质上时，没有额外的加密/解密开销。
-   附加一点，ecryptfs 的解密层存在于内存(内存不足时可能使用 到交换分区)。硬盘上面保存的只有加密数据。这样即使硬盘被偷了，使用数据还原软件也不可能得到 解密文件。不是像普通的加密软件、压缩软件把 解密文件缓存于硬盘上。你即使删除了，使用数据还原软件依然有可能得到 解密文件。

## 安装

sudo apt-get install ecryptfs-utils

然后就可以开始使用了。因为很简单使用命令界面就能解决问题，所以没有图形界面。但是不用担心，我会把每一步讲解清楚。 没有图形界面还有一个好处：隐蔽性高

## 使用举例

这里举个单独加密文件夹的例子，而不是按网上流行的方式：把主文件夹加密。我认为这种操作简单、独立涉及的东西少,效率高。

注：ubuntu安装时有个选项——加密用户文件夹，使用的就是该系统。

＊1，新建一个测试文件夹：show 使用这个文件夹存放加密文件 命令：

sudo mount -t ecryptfs source show

mount 是挂载命令。-t 是指定文件类型。ecryptfs 就是我们使用的加密文件类型。 ！这里换一个命令举例说明一下

sudo mount -t ecryptfs real_path ecryptfs_mounted_path

real_path 是真实存放数据的地方；ecryptfs_mounted_path 是指你要把文件夹挂载于哪里（具体位置可以随意）

推荐：ecryptfs_mounted_path 和 真实目录 real_path 一致，这样非授权用户不能通过原路径访问加密文件。

这里必须说一下挂载：很多人不理解
所谓挂载，可以理解为超级链接。比如说有两个文件夹     
a，b。a中有文件isa，b中有文件isb。
现在我们要把a挂载于b上，命令：mount a b
此时b已经变成了a 的链接。所有对b的访问实际上都指向了a。
比如我们打开b文件夹，看到的文件只有isa。而打开a文件夹，  文件还是isa
我们在b中存放一个新文件 isnew。
好了我们卸载这个挂载（取消这个超链接），命令：umount b
现在查看a文件夹，里面有文件isa isnew
查看b文件夹，里面只有isb

为使么说是超级链接呢，因为有个选项（其它选项我还不会） -t
它的意思是指定 文件夹类型（文件系统类型）。文件系统类型有很多，比如我们使用的ext4、ext3、fat32、ntfs……还有特殊的文件系统，比如 tmpfs（用内存 存文件）、ecryptfs（文件加密格）……
使用-t 就可以以指定文件格式进行“链接”，能查看、操作其它文件格式的文件了。这是普通连接做不到的。

  
＊2，mount 需要root权限，我们使用 命令sudo ，来以root身份运行命令 结果如下：![image001.jpg](http://www.ibm.com/developerworks/cn/linux/l-cn-ecryptfs/image001.jpg)

passphrase:
（这是要你输入密码，自己编一个。一定要记住。另外密码是不会有任何显示的，输完回车就行）
select cipher:
（选择加密方式，不选默认是[aes]。最好记住自己的选择）
select key bytes:
（选择加密位数，不选默认是[16]。最好记住自己的选择）
enable plaintext passthrough(y/n) [n]:
（是否正常显示未经加密文件，默认是 n。以确保你不会误将秘密写入未经加密文件）
enable filename encryption (y/n) [n]:
（是否把文件名也进行加密，默认是 n。如果选择y，那么在没有解密 的情况下是无法列出文件夹内部的文件名。）

如果设置的密码是第一次使用，它会提示你密码被标识为[799d2f922c0f1f26] 。当然，你的密码标识肯定不会是这个。并且告诉你输入可能有误，因为/root/.ecryptfs/sig-cache.txt中没有相关记录。密码标识只存在于你现在的系统中，是怕你忘记密码，它会告诉你这个密码是否用过。因为ecryptfs在密码错误的情况下一样可以进行错误解密，实际上是另外一种形式的加密。要是不保存密码标识，我肯定盗资料的人会很头痛，记心不好的用户也一样。

 出现输入有误的警告后，会让你选择：

 Would you like to proceed with the mount (yes/no)? :
 （你是否希望继续进行挂载。）我们输入yes，来完成加密。
 Would you like to append sig [799d2f922c0f1f26] to
[/root/.ecryptfs/sig-cache.txt]
 in order to avoid this warning in the future (yes/no)?:
 （你是否把密码标识保存到/root/.ecryptfs/sig-cache.txt中？）我们输入yes，也可以输no，看你的记心好坏了。

-   3，好了，现在正常操作ecryptfs_test文件夹。放一些文件来测试。

-   4，关闭解密状态

命令：

 sudo umount -t ecryptfs ecryptfs_test

 （如果使用的是sudo mount -t ecryptfs real_path ecryptfs_mounted_path

那么对应的命令是sudo umount -t ecryptfs ecryptfs_mounted_path） 这命令意思是把挂载取消了。这里的作用相当于把解密取消了，毕竟把重要文件长时间解密放着不好。 好了，再看看ecryptfs_test文件夹，里面的文件是不是都无法正确打开了？

＊＊＊＊＊＊＊＊连概念加内容 加操作都说了，就这么点。简单吧

-   5.需要解密文件夹时还是用命令：

 sudo mount -t ecryptfs ecryptfs_test ecryptfs_test

 （格式还是不变：sudo mount -t ecryptfs real_path ecryptfs_mounted_path）
 passphrase:
 select cipher:
 select key bytes:
 enable plaintext passthrough(y/n) [n]:
 enable filename encryption (y/n) [n]:

上面这几相当初怎么设置的就怎么填，之后文件夹就解密了。

不需要用了，还是用命令解除解密状态

sudo umount -t ecryptfs ecryptfs ecryptfs_test

  

这里说一点，mount命令重启就失效了。如果你要关机了，不必使用sudo umount -t ecryptfs ecryptfs_test 命令了。因为再次开机，解密是失效的。必须重新运行命令sudo mount -t ecryptfs ecryptfs_test ecryptfs_test

## 通用环境下脚本

使用“在此打开终端”，使用该脚本，就能免去输入文件地址的麻烦

#!/bin/bash
    # 本脚本运行由cat650制作，有问题联系cat650@163.com
    # 脚本适用于xubuntu环境，请提前安装ecryptfs-utils   
    # 脚本另存为topsc.sh 存放于/bin
    count=0 ##初始值
  dir="/tmp/mnt/ecryptfs$count" ##初始挂载点
  echo "已挂载加密目录有："
  while df|grep -q $dir 2>/dev/null  ##挂载点 是否使用了
  do
echo $dir
let count=$count+1 ##一定要是/bin/bash如是/bin/sh这里就会出错
dir="/tmp/mnt/ecryptfs$count"  ###新挂载点
done
echo "新增挂载目录："$dir
echo "等待超级用授权"
if [ ! -d $dir ];then ##判断目录是否存在
sudo mkdir -p $dir ###建立新挂载点目录,选项p，可以创建连续文件夹
fi
    echo "授权完成，开始加密……"
    echo "请输入加密密码，选择加密方式："
    sudo mount -t ecryptfs $(pwd) $dir  ### $( )为引用命令结果
    echo "显示加密文件夹"
    thunar $dir   ## 请改为你当前环境下文件编辑器，当前为xubuntu
    echo $dir"已处于加密状态"
    read -p "是否自动卸载加密文件夹(yes?)" yn
    if [ "$yn" != "yes" ]; then
    echo "请记得手动解除加密"
    echo "命令为：sudo umount /tmp/mnt/ecryptfs*"
    read -p "回车退出" no
    exit 0
    fi
    echo "去加密……"
    sudo umount $dir
    sudo rmdir $dir
    ## 下面删除缓存图片，这个大家自己设置。我把整个文件夹链到了/tmp.普通用户位置是：~/.thumbnails/
    ## rm -r /tmp/large/
    ## rm -r /tmp/normal/
    echo "处于加密状态的文件夹有："
    df|grep "tmp/mnt"
    read -p "请验证是否已解除加密,回车退出" no

  

## kde环境下，文件管理器右键集成脚本

注意以下命令运行于kde环境，gnome（ubuntu默认环境）请作相应更改

但命令行很不方便，特别是文件夹地址比较复杂时，来回输入命令太痛苦了。

本文的目的是调用一个简单的脚本，在文件夹右键添加按钮，处理加密、解密问题。

  

2.创建一个脚本，我们放在/bin中。其实放在哪里都可以，只是考虑到安全性还是放在系统文件夹中，以root用户创建。

sudo touch /bin/mountecryptfs.sh

  

3.编辑脚本

sudo kate /bin/mountecryptfs.sh

拷贝以下内容：

#!/bin/bash
     # 本脚本运行由cat650制作，有问题联系cat650@163.com
     # 脚本适用于kde环境，请提前安装ecryptfs-utils     
     count=0 ##初始值
   dir="/tmp/mnt/ecryptfs$count" ##初始挂载点
   echo "已挂载加密目录有："
   while df|grep -q $dir 2>/dev/null  ##挂载点 是否使用了
   do
 echo $dir
 let count=$count+1 ##一定要是/bin/bash如是/bin/sh这里就会出错
 dir="/tmp/mnt/ecryptfs$count"  ###新挂载点
 done
 echo "新增挂载目录："$dir
echo "等待超级用授权"
if [ ! -d $dir ];then ##判断目录是否存在
sudo mkdir -p $dir ###建立新挂载点目录,选项p，可以创建连续文件夹
fi
     echo "授权完成，开始加密……"
     echo "请输入加密密码，选择加密方式："
     sudo mount -t ecryptfs "$1" $dir
     echo "显示加密文件夹"
     dolphin $dir
     echo $dir"已处于加密状态"
     read -p "是否自动卸载加密文件夹(yes?)" yn
     if [ "$yn" != "yes" ]; then
     echo "请记得手动解除加密"
     echo "命令为：sudo umount /tmp/mnt/ecryptfs*"
     read -p "回车退出" no
     exit 0
     fi
     echo "去加密……"
     sudo umount $dir
     sudo rmdir $dir
     ## 下面删除缓存图片，这个大家自己设置。我把整个文件夹链到了/tmp.普通用户位置是：~/.thumbnails/
     rm -r /tmp/large/
     rm -r /tmp/normal/
     echo "处于加密状态的文件夹有："
     df|grep "tmp/mnt"
     read -p "请验证是否已解除加密,回车退出" no

4.把这个脚本变为可执行

sudo dolphin /bin

找到脚本，属性改为可执行。

  

5.用dolphin打开任意文件夹。文件夹右键，属性，类型旁有个小扳手。

添加。选择程序，找到/bin/mountecryptfs.sh

选择并移动到最底。

选择，编辑。应用程序，高级选项：在终端中运行。这步必须有，要不无法进行加密操作。

  

6.测试

在/tmp文件夹中新建一个文件夹，右键，动作，选择mountecryptfs.sh（或者你自己命名的名字）。

好了，在新跳出的文件管理器中，添加一些文件、写点字。

在终端输入yes,让脚本自动卸载加密。

此时，文件管理器刷新后应该为空且不可编辑。

回到我们新建的文件夹中，那些新添的东西应该无法识别。

## 深入使用的讨论

我们知道 ecryptfs 是文件虚拟层，其功能就是 加密/解密。没有密码验证一说

也就是说，它根据密码进行加密/解密。即使密码不正确依旧进行 加密/解密，与数据无关。

那么：

### 1，重复加密\混和加密

实验证明的确可以。因为 mount 是可以重复挂载的。更别说多次挂载在不同地方。

试验：假设有这么几个文件夹 test a b

mount -t ecryptfs test test

密码设置为 1234

里面放文本文件 one。

好了再次挂载

mount -t ecryptfs test a

密码为4321

放文本 two

再次挂载

mount -t ecryptfs a b

密码0987

放文本 three

好了在文件夹 test a b中均可看见 one two three文件，可是 test中只有 one可以打开，a 中只有two可以打开，b中只有three可以打开。

因为one被密码1234加密，two被密码1234和密码4321加密两次，three呢1234、4321、0987加密三次。

test中文件被1234解密一次，a中文件被密码1234和密码4321解密两次，b中1234、4321、0987解密3次。

所以test中除了one被正确解密，其它的都还在加密状态。 a中，one被1234正确解密却又被4321再次解密，就成了乱码（相当于被加了密）

这样一来，假设test中的文件被加密了2次。

那么别人破解密码就很有一思了（假设他不知道文件被加密了几次）。

如果尝试一个密码，显示的文件夹内容是乱码。那么就有2个可能

要么是密码错了，要么是密码对了还要再解密。

当然，对于密码学而言（直接从密文还原为明文）。使用同一方法重复加密丝毫不能增加安全性，不管重复多少次其效果只等同于加密一次。但是不要忘记，ecryptfs有：

1) aes: blocksize = 16; min keysize = 16; max keysize = 32 (not loaded)
2) blowfish: blocksize = 16; min keysize = 16; max keysize = 32 (not loaded)
3) des3_ede: blocksize = 8; min keysize = 24; max keysize = 24 (not loaded)
4) twofish: blocksize = 16; min keysize = 16; max keysize = 32 (not loaded)
5) cast6: blocksize = 16; min keysize = 16; max keysize = 32 (not loaded)
6) cast5: blocksize = 8; min keysize = 5; max keysize = 16 (not loaded)

种加密方式。如果正确使用，破解难度很大

由此可以得知 ecryptfs 有效重复加密次数只有6次，但是对于非情报人员而言，已经够用了。

### 2，把普通软件升级为 带有可加密数据库的安全软件

这点应用原理很简单。软件在 home/~ 中 大多都有配置，软件数据、用户数据的保存文件夹。

比 如：某某通讯录软件 在 ~/xxx/date 中保存用户的数据。而这软件比较普通，使用的明文存储 通讯录。

这意味着，资料外泄。比如里面是 大量客户 ，被竞争对手拿到…… 别觉得不可能，触及钱的东西，被人顾黑客黑都是很有可能的。

此时，使用 ecryptfs 把 date 文件夹 mount 。保证了处于加密状态。正常使用软件就行了。使用完了，umount。 ok，软件有了一个数据库加密的功能。而且是不在影响 软件使用的基础上实现的。

想想，rar这类的压缩加密的软件能做到吗？win 下的各类文件夹加密软件可以吗（可是不能任意 mount的）？

微 软基于用户的（ntfs文件系统），更是不方便。只要用户登录，加密文件夹就自动解密 直到用户注销。虽然说微软的加密真的很强，可是对于在线入侵，或者软件搞鬼就没办法了。只要用户处于登录状态，加密文件夹就像分开双腿的处女，谁想都行。 对于 ecryptfs 可不行，就算你是root，也要输入密码、加密方法、密钥长度。就算你拿到用户的登录密码，也无能为力（比微软全部鸡蛋放一个笼子好的多）。

这里 明白我不喜欢 整体加密主文件夹了吧。

### 3，为网络文件夹加密

这里使用 ubuntu one 或者类似工具的朋友，为了你的数据安全。 请先使用 ecryptfs mount 存入资料，再umount,之后再 同步文件夹。即使黑客 攻破了 ubuntu one 的服务器，也不会获得你的资料。而且使用上，没有太大差异。

欢迎讨论：forcat650@gmail.com --[Cat650](https://wiki.ubuntu.org.cn/%E7%94%A8%E6%88%B7:Cat650 "用户:Cat650") 2010年9月7日 (二) 19:16 (CST)

再说一 点。大家也许都想到了：同一文件夹里的文件，可以是不同密码加密的。 推荐一本书《经典密码学与现代密码学》，网络上可以下载pdf 的版本。对于保守机密很有帮助

## 延伸阅读

引用自：[http://www.ibm.com/developerworks/cn/linux/l-cn-ecryptfs/](http://www.ibm.com/developerworks/cn/linux/l-cn-ecryptfs/)

### eCryptfs 设计目标

为了评估一个加密解决方案的可行性，企业往往要考虑诸多因素，例如员工的学习曲线、增量备份是否受到影响、密钥丢失的话如何防止信息泄漏或如何恢复信息、转换及使用成本、潜在的风险等等。eCryptfs 在设计之初，充分考虑企业用户的如下需求：

-   易于部署。eCryptfs 完全不需要对 Linux Kernel 的其它组件做任何修改，可以作为一个独立的内核模块进行部署。同时，eCryptfs 也不需要额外的前期准备和转换过程。
-   用户能够自由选择下层文件系统来存放加密文件。由于不修改 VFS 层，eCryptfs 通过挂载（mount）到一个已存在的目录之上的方式实现堆叠的功能。对 eCryptfs 挂载点中文件的访问首先被重定向到 eCryptfs 内核文件系统模块中。
-   易于使用。每次使用 eCryptfs 前，用户只需执行 mount 命令，随后 eCryptfs自动完成相关的密钥产生/读取、文件的动态加密/解密和元数据保存等工作。
-   充分利用已有的成熟安全技术。例如，eCryptfs 对于加密文件采用 OpenPGP 的文件格式、通过 Kernel Crypto API使用内核实现的对称密钥加密算法和散列算法等。
-   增强安全性。eCryptfs 的安全性最终完全依赖于解密 FEK 时所需的口令或私钥。通过利用 TPM 硬件（TPM可以产生公钥/私钥对，硬件直接执行加密/解密操作，而且私钥无法从硬件芯片中获得），eCryptfs 最大程度上保证私钥不被泄漏。
-   支持增量备份。eCryptfs 将元数据和密文保存在同一个文件中，从而完美的支持增量备份及文件迁移。
-   密钥托管。用户可以预先指定恢复帐号，万一遗失加密 FEK 的口令/私钥，也可以通过恢复帐号重新获得文件的明文；但是如果未指定恢复帐号，即使系统管理员也无法恢复文件内容。
-   丰富的配置策略。当应用程序在 eCryptfs 的挂载点目录中创建新文件的时候，eCryptfs 必须做出许多决定，比如新文件是否加密、使用何种算法、FEK 长度、是否使用 TPM 等等。eCryptfs 支持与 Apache 类似的策略文件，用户可以根据具体的应用程序、目录进行详细的配置。

### eCryptfs 的架构

图 2. eCryptfs 加密文件系统的架构 ![image002.jpg](http://www.ibm.com/developerworks/cn/linux/l-cn-ecryptfs/image002.jpg)

eCryptfs Layer 是一个比较完备的内核文件系统模块，但是没有实现在物理介质上存取数据的功能。在 eCryptfs Layer自己的数据结构中，加入了指向下层文件系统数据结构的指针，通过这些指针，eCryptfs就可以存取加密文件。清单 2 列出几种主要的数据结构：

清单 2. eCryptfs Layer 主要数据结构

static struct file_system_type ecryptfs_fs_type = {
	.owner = THIS_MODULE,
	.name = "ecryptfs",
	.get_sb = ecryptfs_get_sb,
	.kill_sb = ecryptfs_kill_block_super,
 	.fs_flags = 0
} ;

struct ecryptfs_sb_info {
	struct super_block *wsi_sb;
	struct ecryptfs_mount_crypt_stat mount_crypt_stat;
};

struct ecryptfs_inode_info {
	struct inode vfs_inode;
	struct inode *wii_inode;
	struct file *lower_file; /* wii_inode, lower_file 指向下层文件系统对应的数据结构 */
	struct mutex lower_file_mutex;
	struct ecryptfs_crypt_stat crypt_stat;
};

struct ecryptfs_dentry_info {
	struct path lower_path; /* 下层文件系统的 dentry */
	struct ecryptfs_crypt_stat *crypt_stat;
};

struct ecryptfs_file_info {
	struct file *wfi_file;
	struct ecryptfs_crypt_stat *crypt_stat;
};

Keystore 和用户态的 eCryptfs Daemon 进程一起负责密钥管理的工作。eCryptfs Layer 首次打开一个文件时，通过下层文件系统读取该文件的头部元数据，交与 Keystore 模块进行 EFEK（加密后的 FEK）的解密。前面已经提及，因为允许多人共享加密文件，头部元数据中可以有一串 EFEK。EFEK 和相应的公钥算法/口令的描述构成一个鉴别标识符，由 ecryptfs_auth_tok 结构表示。Keystore 依次解析加密文件的每一个 ecryptfs_auth_tok 结构：首先在所有进程的密钥链（key ring）中查看是否有相对应的私钥/口令，如果没有找到，Keystore 则发一个消息给 eCryptfs Daemon，由它提示用户输入口令或导入私钥。第一个被解析成功的 ecryptfs_auth_tok 结构用于解密 FEK。如果 EFEK 是用公钥加密算法加密的，因为目前 Kernel Crypto API 并不支持公钥加密算法，Keystore 必须把 ecryptfs_auth_tok 结构发给 eCryptfs Daemon，由它调用 Key Module API 来使用 TPM 或 OpenSSL库解密 FEK。解密后的 FEK 以及加密文件内容所用的对称密钥算法的描述信息存放在 ecryptfs_inode_info 结构的 crypt_stat 成员中。eCryptfs Layer 创建一个新文件时，Keystore 利用内核提供的随机函数创建一个 FEK；新文件关闭时，Keystore 和 eCryptfs Daemon 合作为每个授权用户创建相应 EFEK，一齐存放在加密文件的头部元数据中。

eCryptfs 采用 OpenPGP 的文件格式存放加密文件，详情参阅 [RFC 2440](https://tools.ietf.org/html/rfc2440) 规范[4]。我们知道，对称密钥加密算法以块为单位进行加密/解密，例如 AES 算法中的块大小为 128 位。因此 eCryptfs 将加密文件分成多个逻辑块，称为 extent。当读入一个 extent 中的任何部分的密文时，整个 extent 被读入 Page Cache，通过 Kernel Crypto API 被解密；当 extent 中的任何部分的明文数据被写回磁盘时，需要加密并写回整个 extent（参见图 5[5]）。extent 的大小是可调的，但是不会大于物理页的尺寸。当前的版本中的 extent 默认值等于物理页大小，因此在 IA32 体系结构下就是 4096 字节。加密文件的头部存放元数据，包括元数据长度、标志位以及 EFEK 链，目前元数据的最小长度为 8192 字节。

图 3. eCryptfs 加密/解密操作流程图 ![image003.jpg](http://www.ibm.com/developerworks/cn/linux/l-cn-ecryptfs/image003.jpg)

### eCryptfs 不足之处在于

写操作性能比较差。笔者用 iozone 测试了 eCryptfs 的性能，发现读操作的开销不算太大，最多降低 29%，有些小文件测试项目反而性能更好；对于写操作，所有测试项目的结果都很差，普遍下降 16 倍左右。这是因为 Page Cache 里面只存放明文，因此首次数据的读取需要解密操作，后续的读操作没有开销；而每一次写 x 字节的数据，就会涉及 ((x – 1) / extent_size + 1) * extent_size 字节的加密操作，因此开销比较大。

有两种情况可能造成信息泄漏：a. 当系统内存不足时，Page Cache 中的加密文件的明文页可能会被交换到 swap 区，目前的解决方法是用 dm-crypt 加密 swap 区。b. 应用程序也有可能在读取加密文件后，将其中某些内容以临时文件的方式写入未挂载 eCryptfs 的目录中（比如直接写到 /tmp 中），解决方案是配置应用程序或修改其实现。 eCryptfs 实现的安全性完全依赖于操作系统自身的安全。如果 Linux Kernel 被攻陷，那么黑客可以轻而易举地获得文件的明文，FEK 等重要信息。

## 问与答

联系我：一般还是使用站内短信通知比较快。 [http://forum.ubuntu.org.cn/ucp.php?i=pm&mode=compose&u=171468](http://forum.ubuntu.org.cn/ucp.php?i=pm&mode=compose&u=171468)

### No.1

HI,cat650:

我看了你关于ecryptfs使用详解一文，对于文件加密粒度方面有所疑问。

支持文件粒度的加密，也就是说，用户可以选择对哪些文件或目录加密。而且，应用程序不用关心文件是否被加密，可以完全透明地访问加密文件。
对单个加密文件更改密钥和加密算法比较容易。
不同的文件可以使用不同的加密算法和密钥，增大了破解的难度。
只有加密文件才需要特殊的加密/解密处理，普通文件的存取没有额外开销。

答：

在linux下 mount 命令只能挂载于目录上。但不影响单个文件的加密工作。

在例子中你应该看到，假设有一个文件夹text被加密（使用加密方法a，加密长度a，密码a；挂载于新建文件夹text－a 下），我们放入ta、tb文件。

我们对text文件夹进行再次加密挂载（使用加密方法b，加密长度b，密码b；挂载于新建文件夹text－b下），仅操作其中一个文件（新建文件tx，并存入新记录）。

此时我们再看text文件夹，ta、tb使用的是加密方法a，加密长度a，密码a；tx使用加密方法b，加密长度b，密码b。也就是说——选择对哪些文件或目录（目录操作类似）加密。做到文件粒度支持。

如果要更改tb文件的加密方式也很简单（上述条件不变的情况下）：新建加密挂载（使用加密方法c，加密长度c，密码c；挂载于新建文件夹text－c下）。把text－a下的tb文件移动到text-c下。密码更改完成。——对单个加密文件更改密钥和加密算法比较容易。

此时ta、tb、tc使用的是完全不同的 加密方法，加密长度，密码——不同的文件可以使用不同的加密算法和密钥，增大了破解的难度。

 支持文件粒度的加密，也就是说，用户可以选择对哪些文件或目录加密。而且，应用程序不用关心文件是否被加密，可以完全透明地访问加密文件。——这个问题解答了吧。
对单个加密文件更改密钥和加密算法比较容易。 
不同的文件可以使用不同的加密算法和密钥，增大了破解的难度。 
只有加密文件才需要特殊的加密/解密处理，普通文件的存取没有额外开销。——不进行加密挂载，就是它的实现。

问题明白了吗？其它特性是因为这个加密层是存在于内存上（或虚拟内存内）。