---
date: 2023-03-24 11:13
title: Filesystem-level encryption
tags:
-  filesystem
- filesystem-level
-  encryption
- stackable
- stacked
- cryptographic filesystem
---



# Filesystem-level encryption


This article **does not [cite](https://en.wikipedia.org/wiki/Wikipedia:Citing_sources "Wikipedia:Citing sources") any [sources](https://en.wikipedia.org/wiki/Wikipedia:Verifiability "Wikipedia:Verifiability")**. Please help [improve this article](https://en.wikipedia.org/w/index.php?title=Filesystem-level_encryption&action=edit) by [adding citations to reliable sources](https://en.wikipedia.org/wiki/Help:Referencing_for_beginners "Help:Referencing for beginners"). Unsourced material may be challenged and [removed](https://en.wikipedia.org/wiki/Wikipedia:Verifiability#Burden_of_evidence "Wikipedia:Verifiability").  
_Find sources:_ ["Filesystem-level encryption"](https://www.google.com/search?as_eq=wikipedia&q=%22Filesystem-level+encryption%22) – [news](https://www.google.com/search?tbm=nws&q=%22Filesystem-level+encryption%22+-wikipedia&tbs=ar:1) **·** [newspapers](https://www.google.com/search?&q=%22Filesystem-level+encryption%22&tbs=bkt:s&tbm=bks) **·** [books](https://www.google.com/search?tbs=bks:1&q=%22Filesystem-level+encryption%22+-wikipedia) **·** [scholar](https://scholar.google.com/scholar?q=%22Filesystem-level+encryption%22) **·** [JSTOR](https://www.jstor.org/action/doBasicSearch?Query=%22Filesystem-level+encryption%22&acc=on&wc=on) _(July 2009)_ _([Learn how and when to remove this template message](https://en.wikipedia.org/wiki/Help:Maintenance_template_removal "Help:Maintenance template removal"))_

**Filesystem-level encryption**, often called **file-based encryption**, **FBE**, or **file/folder encryption**, is a form of [disk encryption](https://en.wikipedia.org/wiki/Disk_encryption "Disk encryption") where individual files or directories are [encrypted](https://en.wikipedia.org/wiki/Encryption "Encryption") by the [file system](https://en.wikipedia.org/wiki/File_system "File system") itself.

This is in contrast to the [full disk encryption](https://en.wikipedia.org/wiki/Full_disk_encryption "Full disk encryption") where the entire partition or disk, in which the file system resides, is encrypted.

Types of filesystem-level encryption include:

-  <mark style="background: #ABF7F7A6;"> the use of a 'stackable' **cryptographic filesystem** layered on top of the main file system</mark>
-   a single _general-purpose_ file system with encryption

The advantages of filesystem-level encryption include:

-   flexible file-based [key management](https://en.wikipedia.org/wiki/Key_management "Key management"), so that each file can be and usually is encrypted with a separate encryption key[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]
-   individual management of encrypted files e.g. incremental backups of the individual changed files even in encrypted form, rather than backup of the entire encrypted volume[_[clarification needed](https://en.wikipedia.org/wiki/Wikipedia:Please_clarify "Wikipedia:Please clarify")_]
-   [access control](https://en.wikipedia.org/wiki/Access_control "Access control") can be enforced through the use of [public-key cryptography](https://en.wikipedia.org/wiki/Public-key_cryptography "Public-key cryptography"), and
-   the fact that [cryptographic keys](https://en.wikipedia.org/wiki/Key_(cryptography) "Key (cryptography)") are only held in memory while the file that is decrypted by them is held open.

## General-purpose file systems with encryption

Unlike cryptographic file systems or [full disk encryption](https://en.wikipedia.org/wiki/Full_disk_encryption "Full disk encryption"), general-purpose file systems that include<mark style="background: #35E708E3;"> filesystem-level encryption do not typically encrypt file system [metadata](https://en.wikipedia.org/wiki/Metadata "Metadata"), such as the directory structure, file names, sizes or modification timestamps.</mark> <mark style="background: #E73308E3;">This can be problematic if the metadata itself needs to be kept confidential.</mark> In other words, if files are stored with identifying file names, anyone who has access to the physical disk can know which documents are stored on the disk, although not the contents of the documents.

<mark style="background: #ABF7F7A6;">One exception to this is the encryption support being added to the [ZFS](https://en.wikipedia.org/wiki/ZFS "ZFS") filesystem. </mark>Filesystem metadata such as filenames, ownership, ACLs, extended attributes are all stored encrypted on disk. The ZFS metadata relating to the storage pool is stored in [plaintext](https://en.wikipedia.org/wiki/Plaintext "Plaintext"), so it is possible to determine how many filesystems (datasets) are available in the pool, including which ones are encrypted. The content of the stored files and directories remain encrypted.

Another exception is [CryFS](https://en.wikipedia.org/w/index.php?title=CryFS&action=edit&redlink=1 "CryFS (page does not exist)") replacement for [EncFS](https://en.wikipedia.org/wiki/EncFS "EncFS").

## Cryptographic file systems

Cryptographic file systems are specialized (not general-purpose) file systems that are specifically designed with encryption and security in mind.<mark style="background: #ADCCFFA6;"> They usually encrypt all the data they contain – including metadata. </mark> <mark>Instead of implementing an on-disk format and their own [block allocation]</mark>, these file systems are <mark style="background: #E77C08E3;">often layered on top of existing file systems e.g. residing in a directory on a host file system</mark>. Many such file systems also offer advanced features, such as [deniable encryption](https://en.wikipedia.org/wiki/Deniable_encryption "Deniable encryption"), cryptographically secure read-only [file system permissions](https://en.wikipedia.org/wiki/File_system_permissions "File system permissions") and different views of the directory structure depending on the key or user ...

One use for a cryptographic file system is when part of an existing file system is [synchronized] with '[cloud storage]'.<mark style="background: #FF5582A6;"> In such cases the cryptographic file system could be 'stacked' on top, to help protect data confidentiality.</mark>