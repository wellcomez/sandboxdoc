---
date: 2023-03-24 00:35
title: running cryptsetup in docker containers
tags:
- ecryptfs
- cryptsetup
- dm-crypt
---
https://gitlab.com/cryptsetup/cryptsetup/-/issues/353


# running cryptsetup in docker containers

Closed

running cryptsetup in docker containers

Dear Cryptsetupers,

I would like to use multiple disk partitions, one per docker container. Each docker container will then encrypt its partition with LUKS. The passphrase for each partition are known only by the container owning the partition. It is ok for the docker host to have access to the luksOpened devices, but one container should not have access to the others' luksOpened devices.

My thought is for the docker host to map each partition to the container using `--device=/dev/sdc1:/dev/sdc:rwm` option of the docker run command, and for the docker container to run cryptsetup `luksFormat`, `luksOpen` and `luksClose` on the mapped partition (e.g., /dev/sdc). However, `luksFormat` and `luksOpen` fail when run within the docker container. This is because these commands create a temporary dm device, `/dev/mapper/temporary-cryptsetup-<PID>`, whose minor number is dynamically assigned by devicemapper. This temporary device is not in the docker container's device cgroup, so read/write access to it is denied. Unfortunately there is no way for the docker container device cgroup to be dynamically updated to include this device. I can allow all devicemapper `253:*` devices in the cgroup, but this makes the device accessible from the other docker containers. Running the docker container in the `--privileged` mode also makes this possible, but this is equivalent to allow all `253:*` devices which is not desired.

To solve my problem, and to make cryptsetup be more friendly to docker containers in general, I would like to propose changing cryptsetup as follows. The idea is to pre-allocate a dm device for the container to use, one device for each LUKS device. Then change cryptsetup to use this pre-allocated device for its temporary device and the eventual LUKS container.

At docker host:

-   pre-create a dm device to be used as its `temporary-cryptsetup-<PID>` device, one device per docker container

~~~
     # dmsetup create cryptsetup-tmp-device-<1..n>
     0 1 error
     # dmsetup mknodes cryptsetup-tmp-device-<1..n>
~~~

-   Run the container, mapping the above device as follows:

~~~
    # docker run blah \
        --device /dev/sdc1:/dev/sdc:rwm                                                      \ \# the partition to be encrypted
        --device /dev/mapper/cryptsetup-tmp-device-1:/dev/mapper/cryptsetup-tmp-device-1:rwm \ \# the dm temporary device
        --device /dev/mapper/control:/dev/mapper/control:rwm                                 \ \# so ioctl to devmapper will work
~~~

Cryptsetup changes:

-   Add a new option, e.g., `--temporary-devname=cryptsetup-tmp-device-1`, for cryptsetup to use this device rather than creating a new device. 
    
-   Add the string `tmpdevname` into `struct crypt_device`, and the setter and getter functions for it. 
    
-   From `action_luksFormat()`, `action_open_luks()` and `action_close()`, set the `tmpdevname` in the created crypt_device if the command line option is specified 
    
-   In `LUKS_endec_template()`, use `cd->tmpdevname` if specified instead of the `temporary-cryptsetup-<PID>` for the device name. Also call `dm_create_device()` with the reload option true, so the existing tmpdevname device will be reloaded instead of created. 
    
-   Add the `dm_rename_device()` function to rename a dm device to another name. This is used in `LUKS_activate()` to rename the temporary device to the LUKS container name instead of creating a new device. Also in `dm_remove_device()` to rename the device from the LUKS container name back to the temporary device name. 
    
-   In `dm_remove_device()`, instead of removing the device, if `tmpdevname` is set, reload it as a device with the “error” target. This is so subsequent cryptsetup operations in this docker container will retain the mapped major and minor number.
    

With the above changes, I can successfully run cryptsetup within a docker container:

~~~
    # cryptsetup --temporary-devname=cryptsetup-tmp-device-1 luksFormat /dev/sdc
    # cryptsetup --temporary-devname=cryptsetup-tmp-device-1 luksOpen /dev/sdc crpyt_sdc
    # cryptsetup --temporary-devname=cryptsetup-tmp-device-1 luksClose crypt_sdc
~~~

with /dev/sdc and /dev/mapper/crypt_sdc only accessible from this docker container (and of course the docker host itself) and no other docker containers running in the same host.

I have implemented the above changes in my local sandbox.

👍0👎0

1.  Drag your designs here or [click to upload](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#).
    

##### Tasks

0

No tasks are currently assigned. Use tasks to break down this issue into smaller parts.

### [](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#related-issues)Linked items

0

Link issues together to show that they're related or that one is blocking others. [Learn more.](https://gitlab.com/help/user/project/issues/related_issues)

## Activity

Sort or filter

-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_50684272)
    
    Author
    
    [cryptsetup.diff](https://gitlab.com/cryptsetup/cryptsetup/uploads/e0c6ef4eed54840e20a86eb545e81692/cryptsetup.diff)
    
    
    [Milan Broz](https://gitlab.com/mbroz)[@mbroz](https://gitlab.com/mbroz)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_50728929)
    
    Owner
    
    Sorry, but this is completely wrong fix that just hides the real problem.
    
    If you allow any device-mapper IOCTL inside container, I think container will have access to any host device anyway (it can just remap the device mapper table for existing device to point to any device that kernel has access to).
    
    Anyway, if you want to go this way, then note that temporary devices are just fallback to very old kernels.
    
    If you have recent cryptsetup (1.7.x), recent kernel (3.x or any later) and userspace kernel crypto API available (af_alg, af_skciphers kernel modules loaded) then these device are no longer used and it should work without any patch.
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_50967083)
    
    Author
    
    Milan, thanks for your pointer. Apparently my kernel (4.1.8-5) did not have the CRYPTO_USER_* options set, so as you said it fallbacks to use the device-mapper temporary device.
    
    On the kernel that have the CRYPTO_USER_* options set, cryptsetup uses the Userspace crypto wrapper instead of the device-mapper temporary device. So this solves the first half of the problem.
    
    In `luksOpen`, however, `LUKS1_activate()` is dynamically creating the device-mapper device for the resulting unencrypted device. How would I give the docker container permission to access this device?
    
    I need to do the luksOpen within the container, because only this container knows the LUKS passphrase for its disk partition.
    
    My cryptsetup is 1.6.6, but I am also using the newer 2.0.0-rc1 for testing.
    
    Edited 5 years ago by [Santa Wiryaman](https://gitlab.com/swiryaman)
    
-   [![Ondrej Kozina](https://secure.gravatar.com/avatar/fefc151b9e67ba6c5ea2057879661e4f?s=80&d=identicon)](https://gitlab.com/oniko)
    
    [Ondrej Kozina](https://gitlab.com/oniko)[@oniko](https://gitlab.com/oniko)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51411827)
    
    Maintainer
    
    Why don't you create LUKS device in docker host, unlock it and pass the unlocked dm-crypt device to the container (instance)? The docker host will see individual containers volume keys anyway.
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51442376)
    
    Author
    
    Thanks for the suggestion Ondrej, but the passphrase for the volume key is known only to the docker container. Just like in a bank (docker host), the bank holds a key (volume key) to your safe deposit box, but it additionally needs a key (passphrase) given to you to open your box.
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51444068)
    
    Author
    
    [@milan](https://gitlab.com/milan "Milan"), I have managed to enable af_alg sockets for cryptsetup to use. With this I can luksFormat the partition from a docker container. On luksOpen, however, I still need to give rw permission of /dev/mapper/control to the docker container. This is because the dm_task_create() in libdevmapper calls dm_check_version() which needs to ioctl /dev/mapper/control to get the kernel dev-mapper versions. Is there another way for libdevmapper to get the versions without the ioctl?
    
    Anyway, even with rw permission to /dev/mapper/control for the ioctl, if no devices created by the ioctls have rw permission, is this still bad?
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51445414)
    
    Author
    
    My strategy now is to run the container with --device=/dev/mapper/control:/dev/mapper/control:rwm to allow the container to luksFormat, luksOpen and luksClose its LUKS devices. This is with the userspace crypto wrapper enabled in the kernel and udev_sync enabled in the docker container.
    
    On LuksOpen, I will create a udev rule in the docker host to respond to the creation and deletion of the luksOpened and luksClosed devices. On open, it will add "b major:minor rwm" to the docker container's devices cgroup, and conversely on close it will remove it from the cgroup.
    
    
    [Milan Broz](https://gitlab.com/mbroz)[@mbroz](https://gitlab.com/mbroz)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51445471)
    
    Owner
    
    This comparison is misleading. Once you have volume key, you can decrypt the volume completely - you do not need passphrase anymore. You have to trust your host completely here in all cases.
    
    Anyway, this is really not a problem of cryptsetup - docker is not a virtual machine, access to device-mapper ioctl is one thing but you do not have needed infrastructure there to create and manage device nodes (udev).
    
    Ondrej is right, it is the correct solution - unlock it in host and then provide device to the container.
    
    Device-mapper require root access (not only control node access but also SYSADMIN capability), once you grant it, there is no security. So while you can work with LUKS header in user privilege, you will never be able to activate device-mapper devices inside container (not only it requires root, but it also requires working udev inside container etc).
    
    Edited 5 years ago by [Milan Broz](https://gitlab.com/mbroz)
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51446665)
    
    Author
    
    How can the docker host luksOpen the device with the volume key? My understanding is that the passphrase is required?
    
-   [![Ondrej Kozina](https://secure.gravatar.com/avatar/fefc151b9e67ba6c5ea2057879661e4f?s=80&d=identicon)](https://gitlab.com/oniko)
    
    [Ondrej Kozina](https://gitlab.com/oniko)[@oniko](https://gitlab.com/oniko)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51447512)
    
    Maintainer
    
    > My strategy now is to run the container with --device=/dev/mapper/control:/dev/mapper/control:rwm to allow the container...
    
    Oh no, that's really not good idea. If you manage to give /dev/mapper/control access rights (ioctls) to the container, you will also allow it to manipulate **every** block device on docker host system. Serious security hole.
    
    
    [Milan Broz](https://gitlab.com/mbroz)[@mbroz](https://gitlab.com/mbroz)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51483279)
    
    Owner
    
    > How can the docker host luksOpen the device with the volume key? My understanding is that the passphrase is required?
    
    The volume key is the key that is directly used to encrypt the device (it is key for AES block cipher). The LUKS keyslots are in principle small area on disk that containing the volume key in encrypted form and the key to unlock this keyslot is generated from your passphrase.
    
    Once the volume key is unlocked, it is installed inside kernel dm-crypt to perform transparent encryption of disk sectors. Anyone with kernel privileges can see this key while device is active (for example thought dm-ioctl). If you have this key, you can configure dm-crypt directly without need of LUKS header and passphrase completely.
    
    So, even if you configure dm-crypt device from container (using luksOpen), the real mapping is created inside host kernel and host can see this volume key.
    
    I really do not understand what are you trying to do here - it does not help security at all. Just unlock LUKS device in host under some unique name and export it to container as a block device.
    
    And do not allow device-mapper ioctl inside container. As Ondrej said, it would allow to access EVERY block device on host - so instead of increasing security, you open huge security hole because you allow container to map anything through dm-ioctl. Really, there is only one kernel - for the host - it is not virtual machine that has own kernel.
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_51486065)
    
    Author
    
    What I want to do is to create servers for multiple clients. For reasons beyond this discussion, the service for each client must run in a separate docker container within the same docker host. The service requires a private client data that is only accessible to that particular client. Only this client has the passphrase to unlock its private data. I am thinking of assigning a disk partition for this purpose, secured by LUKS encryption. This way only this client has read/write access to its data, and we do not have to spend much time sanitizing the data during deprovisioning. The client will send its passphrase securely to the server running in the container. The client do not have login access to the docker container nor docker host, root or otherwise, the only access is through the client-server API.
    
    So now I understand allowing r/w access to /dev/mapper/control to docker containers is a bad idea. Without this, only the docker host can luksOpen the client private data. But the host does not have the master key unless the docker container gets the passphrase from the client and give it to the host in order to be able to decrypt the master key from the LUKS header. I am trying to find acceptable and secure ways of having the container luksOpen the private data, so that the passphrase does not have to travel around and be more vulnerable. Is there any way for luksOpen in the container that does not require access to dm-ioctl?
    
-   [![Ondrej Kozina](https://secure.gravatar.com/avatar/fefc151b9e67ba6c5ea2057879661e4f?s=80&d=identicon)](https://gitlab.com/oniko)
    
    [Ondrej Kozina](https://gitlab.com/oniko)[@oniko](https://gitlab.com/oniko)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_53069971)
    
    Maintainer
    
    > Is there any way for luksOpen in the container that does not require access to dm-ioctl?
    
    No. There is no way to create dm-crypt device without device-mapper kernel API (ioctls).
    
    I'm not sure there's a good example for what you seek to achieve. AFAIK docker doesn't provide any communication interface between host and container for setting up encrypted storage inside container based on device-mapper layer.
    
-   [Ondrej Kozina](https://gitlab.com/oniko) closed [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_53069972)
    
-   [![Santa Wiryaman](https://secure.gravatar.com/avatar/70cf952a311ff1b091208091fee824f1?s=80&d=identicon)](https://gitlab.com/swiryaman)
    
    [Santa Wiryaman](https://gitlab.com/swiryaman)[@swiryaman](https://gitlab.com/swiryaman)· [5 years ago](https://gitlab.com/cryptsetup/cryptsetup/-/issues/353#note_53073653)
    
    Author
    
    I figured as much. Thanks for the help. I was thinking of decrypting/encrypting the LUKS disk myself in the docker container, after I get the volume key from luksDump. But I decided not to at the end.