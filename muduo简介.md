---
date: 2023-04-21 12:10
title: muduo简介
tags:
- muduo
---



# muduo

  [muduo](https://github.com/chenshuo/muduo)是一个高质量的Reactor网络库，采用one loop per thread + thread pool架构实现，代码简洁，逻辑清晰，是学习网络编程的很好的典范。

　　muduo的代码分为两部分，base和net，base部分实现一些基础功能，例如log, thread, threadpool, mutex, queue 等，这些基础模块在后面网络库中很多地方都可以复用， base库的类相互之间耦合性较低，源码阅读起来并不困难，此处不做过多探究。

　net部分使用base中的工具类实现更高层次的逻辑，网络编程无非是对socket和其使用的epoll/poll等进行封装，使其便于使用，屏蔽掉底层网络库的一些 "坑", 在满足了基础的网络IO之后，就需要考虑高性能，高并发的问题，muduo 的是由poll/epoll 这些IO复用模型构成，但是单个IO线程在面对大量请求时难免处理不过来，所以就需要结合多线程或者线程池，一个线程对应一个epoll进行网络IO，这样就可以充分利用硬件多核系统。从软硬两方面综合提升性能。net部分封装的较为彻底，对上层提供的接口简单易用，所以涉及复杂的内部处理，接下来就对其内部实现进行探究。

　　下面这张图是陈硕提供的muduo 网络库的类图，本次讲解主要是围绕下面这张图，弄明白这样就相当于弄明白这个架构了。（图中灰色的类是内部类，白色的是外部类）![](https://images2015.cnblogs.com/blog/1111516/201702/1111516-20170227193859610-1126946307.png)

　　首先是EvenLoop类，他是事件循环（反应器 Reactor），每个线程只能有一个 EventLoop 实体，它负责 IO 和定时器事件的分派。 它用 TimerQueue 作为计时器管理，用 Poller 作为 IO Multiplexing。TimeQueue底层使用timerfd_*系列函数将定时器转换为fd添加到事件循环中，当时间到达后就会自动触发事件， 其内部使用 set 管理一些注册好的Timer，由于set有自动排序功能，所以注册到事件循环的总是第一个需要处理的Timer。Poller是IO mutiplexing的实现，它是一个抽象类，具体实现由其子类PollPoller (封装poll)， EpollPoller (封装epoll) 实现，这是muduo库中唯一一个用面向对象的思想实现的，通过虚函数提供回调功能。Poll中的updateChannel方法用于注册和更新关注的事件，所有的 fd 都需要调用它添加到事件循环中。 除了用TiemQueue和Poller管理时间事件和IO事件外，EvenLoop还包含一个任务队列，它用来做一些计算任务，你可以将自己的任务添加到任务队列中，EvenLoop在一次事件循环中处理完IO事件就会进行依次取出这些任务进行执行，这样当多个线程需要处理同一资源时可以减少锁的复杂性， 将资源的管理固定地交由一个线程来处理，其他线程对资源的处理只需要添加到该线程的任务队列中，由该线程异步执行， 如此只需要在任务队列加锁即可，其他地方无需上锁，减少锁的滥用。 但是有一个问题，如果EvenLoop阻塞在epoll_wait处就无法处理这些计算任务了，毕竟计算任务是在处理完IO事件后才执行的，所以此时需要通过某种通信方式唤醒该线程，被唤醒后就取出队列中的任务进行执行。muduo采用 eventfd(2) 来异步唤醒。

　　muduo中通过Channel对fd 进行封装，其实更合适的说法是对fd事件相关方法的封装，例如负责注册fd的可读或可写事件到EvenLoop，又如fd产生事件后要如何响应。 一个fd对应一个channel, 它们是聚合关系，Channel在析构函数中并不会close掉这个fd。 它有一个handleEvent方法，当该fd有事件产生时EvenLoop会调用handleEvent方法进行处理，在handleEvent内部根据可读或可写事件调用不同的回调函数（回调函数可事先注册）。 它一般做为其他类的成员，例如EvenLoop通过一个vector<Channel*> 对注册到其内的众多fd的管理，毕竟有了Channel就有了fd及其对应的事件处理方法，所以你会看到上图中EvenLoop与Channel是一对多的关系。

　　Socket也是对fd的封装，但不同与channel, 它仅封装 ::socket 产生的fd, 并且提供的方法也是一些获取或设置网络连接属性的方法，他和 fd 是组合关系，当Socke析构时会close掉这个fd。不管如何封装fd， 一些系统函数传递的参数总是fd，所以你会看到上图中一些类中既有 fd 又有Channel或Socket, 这也是在所难免的。

　　TcpConection是对一个连接的抽象，一个TcpConnection包含一个Socket和一个Channel,  上面说到channel::handleEvent会在产生事件后调用事先注册的回调函数，其实在TcpConnection构造的时候就会为其所属的Channel注册好这些回调函数，handleRead,handleWrite....分别对应可读可写事件产生后调用的回调函数。事件产生后会调用handleRead(或handleWrite), TcpConceton会在handleRead中做一些处理，然后转交给上层，提交到上层的具体体现就是调用上层注册的回调函数（又是一样的套路😊），因为Channel是个内部类，所以它的回调函数的注册只能TcpConnection完成，上层只需要将回调函数注册到TcpConnection后其会自动处理。之所以加了TcpConnection这一层是为了解决Tcp协议收发数据时阻塞问题，比如::write时内核缓冲区满了，只能等到下次EPOLLOUT事件产生后再写，对于一个牛逼的网络库来说，应该是上层只需调用一次sendMessage发送全部数据，网络库内部将数据分批::write给peer， TcpConnection就实现了这一点，它内部维护一个应用层的inputBuffer和outputBuffer保证数据的可靠发送，同时再连接断开时也能保证数据发送完成后再断开。

　　Accepter用来接受连接，其内是维护一个listenfd及其对应的channel， 他会对listenfd进行一些初始化操作例如::bind, ::listen，然后就调用channel的方法注册到事件循环中，事件产生后回调其handleRead进行accept连接，然后调用上层注册的newConnectonCallback回调函数。Accepter是个内部类它属于一个TcpServer,  Accepter::newConnectionCallback的实例就是由TcpServer进行注册的， 连接产生后TcpServer主要对这个连接创建一个TcpConnection对象并设置好其对应的回调函数，TcpServer维护一个TcpConnection的map来管理连接到他的client, TcpSever通过线程池来处理并发请求，线程池内是多个IO线程也就是多个EvenLoop，每个连接到来回自动分配到其中一个来创建TcpConnection相关。这样就大大提高了TcpServer的请求处理能力！

　　Connector与TcpClient的关系如同Accepter与TcpServer的关系，只不过它使用来发起主动连接的，它需要注意的就是自动重连等这些功能。

　　掌握上面这些就大致明白整个架构了，因为muduo是基于对象的编程思想，所以上面涉及到了很多回调函数之类的，其实这只是muduo库暴露给用户的接口而已，系统经过层层封装后，每层都需要向上提供定制化的回调函数，所以看起来就比较乱了，慢慢捋就好了

　　这里只是做个简单的总结，内部还有好多实现细节值得我们学习。同时陈硕对一些C++的使用方式也值得我们去探究，希望我们能在这些大牛的后面快速成长吧。