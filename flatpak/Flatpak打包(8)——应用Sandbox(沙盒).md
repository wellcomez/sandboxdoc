---
date: 2022-12-08 14:58
title: Flatpak打包(8)——应用Sandbox(沙盒)
tags:
- flatpak
 - build
---



# Flatpak打包(8)——应用Sandbox(沙盒)

flatpak的主要目标之一是通过将应用程序彼此隔离来提高桌面系统的安全性。这是使用沙盒完成的，并且这意味着，默认情况下，flatpak对主机环境的访问权限非常有限。

-   除运行时，应用程序和`〜/.var/app/$APPID`之外，不能访问任何主机文件.只有最后一个是可写的。
-   无法访问网络。
-   不能访问任何设备节点（除了`/dev/null`等）。
-   无法访问沙箱外的进程。
-   有限的系统调用。例如，应用程序不能使用非标准网络套接字类型或ptrace其他进程。
-   有限的访问会话B-Bus实例 - 应用程序只能在bus上拥有自己的名字。
-   无法访问X，系统D-Bus或PulseAudio等主机服务。

大多数应用程序需要访问其中一些资源才能发挥作用，而flatpak提供了多种方式让应用程序访问它们。  
尽管应用程序可以使用哪些沙箱权限没有限制，但作为良好的做法，建议尽可能使用最少数量的权限。强烈建议不要使用某些权限，例如全面访问系统总线（使用`--socket = system-bus`选项）。

# 配置沙箱权限

使用`build-finish`命令是配置沙箱权限的最简单方法。就像在前面的例子中看到的那样，这可以用来添加对图形套接字和网络的访问：

```shell
$ flatpak build-finish dictionary2 --socket=x11 --share=network --command=gnome-dictionary
```

这些参数可以转换为应用程序元数据(metadata)文件中的几个属性：

```
[Application]
name=org.gnome.Dictionary
runtime=org.gnome.Platform/x86_64/3.22
sdk=org.gnome.Sdk/x86_64/3.22
command=gnome-dictionary

[Context]
shared=network;
sockets=x11;
```

build-finish允许将全部范围的资源添加到应用程序中。这些选项也可以作为`finish-args`属性传递给flatpak-builder。  
本页底部的表格提供了许多沙箱权限的概述。完整列表也可以使用`flatpak build-finish --help`进行查看。  
**注**：  
直到兼容沙盒的后端可用之前，则需要使用以下选项启用对dconf的访问：

```
--filesystem=xdg-run/dconf
--filesystem=~/.config/dconf:ro
--talk-name=ca.desrt.dconf
--env=DCONF_USER_CONFIG_DIR=.config/dconf
```

# Portals

Portals是应用程序可以通过它与沙箱内的主机环境进行交互的机制。通过这种方式，他们可以提供额外的功能来与数据，文件和服务进行交互，而无需添加沙盒权限。  
接口工具包可以为Portals实现透明支持。如果应用程序使用这些工具包之一，则不需要额外的工作来访问它们。  
可以通过Portals访问的功能示例包括：

-   禁止用户会话结束，暂停，空闲或切换
-   网络状态信息
-   通知
-   开放给URI
-   用本机文件选择器对话框打开文件
-   打印
-   截图

不使用支持门户的工具包的应用程序可以参考[xdg-desktop-portal API](http://flatpak.org/xdg-desktop-portal/portal-docs.html)文档以获取有关如何访问它们的信息。

# 覆盖沙箱权限

在开发应用程序时，重写flatpak的沙箱配置有时会很有用。有几种方法可以做到这一点。一种是使用`flatpak run`来覆盖它们，它接受与`build-finish`相同的参数。例如，这会让Dictionary应用程序看到您的主目录：

```shell
$ flatpak run --filesystem=home --command=ls org.gnome.Dictionary ~/
```

`flatpak override`也可用于永久覆盖应用程序的权限：

```shell
$ flatpak --user override --filesystem=home org.gnome.Dictionary
$ flatpak run --command=ls org.gnome.Dictionary ~/
```

也可以使用相同的方法删除权限。可以使用以下命令查看删除访问文件系统时会发生的情况，例如：

```shell
$ flatpak run --nofilesystem=home --command=ls org.gnome.Dictionary ~/
```

# 可用的沙箱权限

flatpak提供了一系列用于控制沙箱权限的选项。以下是一些最有用的：

选项

作用

`--filesystem=host`

访问所有文件

`--filesystem=home`

访问主目录

`--filesystem=home:ro`

访问主目录，只读

`--filesystem=/some/dir --filesystem=~/other/dir`

访问路径

`--filesystem=xdg-download`

访问XDG下载目录

`--nofilesystem=...`

撤消上一层

`--socket=x11 --share=ipc`

使用x11显示窗口[[1][1]](https://blog.csdn.net/beyond_zhangna/article/details/79328415#%5b1%5d)

`--device=dri`

OpenGL渲染

`--socket=wayland`

使用 Wayland显示窗口

`--socket=pulseaudio`

使用PulseAudio播放声音

`--share=network`

访问网络[[2][2]](https://blog.csdn.net/beyond_zhangna/article/details/79328415#%5b2%5d)

`--talk-name=org.freedesktop.secrets`

与会话总线上的命名服务对话

`--system-talk-name=org.freedesktop.GeoClue2`

与系统总线上的命名服务对话

`--socket=system-bus`

无限制地使用所有的D-Bus

[1]: `--share = ipc`表示沙箱与主机共享ipc命名空间。这不一定是必需的，但如果没有它，x共享内存扩展将不起作用，这对x性能非常不利。  

[2]: 赋予网络访问权限还可以访问侦听抽象的unix套接字的所有主机服务（由于网络名称空间的工作原理），并且这些服务没有权限检查。这不幸地影响例如x服务器和默认侦听抽象套接字的会话总线。一个安全的分布应该禁用这些，只需使用普通的套接字。