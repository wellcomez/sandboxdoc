---
date: 2022-12-16 15:35
title: Portal Documentation
tags:
-  flatpak
- portal
---
    
[Flatpak](https://docs.flatpak.org/en/latest/index.html)



# Portal API Reference[](https://docs.flatpak.org/en/latest/portal-api-reference.html#portal-api-reference "Permalink to this headline")

Version 1.14.1

---

**Table of Contents**

[1. Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#common-conventions)

[Portal requests](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771422940768)

[Sessions](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771424242304)

[Parent window identifiers](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window)

[I. Portal API Reference](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771425398192)
- [org.freedesktop.portal.Background](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Background) — Portal for requesting autostart and background activity
- [org.freedesktop.portal.Camera](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Camera) — Camera portal
- [org.freedesktop.portal.Device](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Device) — Portal for device access
- [org.freedesktop.portal.Documents](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Documents) — Document portal
- [org.freedesktop.portal.DynamicLauncher](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.DynamicLauncher) — Portal for installing application launchers onto the
- [org.freedesktop.portal.Email](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Email) — Portal for sending email
- [org.freedesktop.portal.FileChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.FileChooser) — File chooser portal
- [org.freedesktop.portal.FileTransfer](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.FileTransfer) — Portal for transferring files between apps
- [org.freedesktop.portal.Flatpak.UpdateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Flatpak.UpdateMonitor)
- [org.freedesktop.portal.Flatpak](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Flatpak) — Flatpak portal
- [org.freedesktop.portal.GameMode](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.GameMode) — Portal for accessing GameMode
- [org.freedesktop.portal.Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Inhibit) — Portal for inhibiting session transitions
- [org.freedesktop.portal.Location](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Location) — Portal for obtaining information about the location
- [org.freedesktop.portal.MemoryMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.MemoryMonitor) — Memory monitoring portal
- [org.freedesktop.portal.NetworkMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.NetworkMonitor) — Network monitoring portal
- [org.freedesktop.portal.Notification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Notification) — Portal for sending notifications
- [org.freedesktop.portal.OpenURI](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.OpenURI) — Portal for opening URIs
- [org.freedesktop.portal.PowerProfileMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.PowerProfileMonitor) — Power Profile monitoring portal
- [org.freedesktop.portal.Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Print) — Portal for printing
- [org.freedesktop.portal.ProxyResolver](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.ProxyResolver) — Proxy information
- [org.freedesktop.portal.Realtime](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Realtime) — Portal for setting threads to realtime
- [org.freedesktop.portal.RemoteDesktop](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.RemoteDesktop) — Remote desktop portal
- [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Request) — Shared request interface
- [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.ScreenCast) — Screen cast portal
- [org.freedesktop.portal.Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Screenshot) — Portal for taking screenshots
- [org.freedesktop.portal.Secret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Secret) — Portal for retrieving application secret
- [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Session) — Shared session interface
- [org.freedesktop.portal.Settings](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Settings) — Settings interface
- [org.freedesktop.portal.Trash](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Trash) — Portal for trashing files
- [org.freedesktop.portal.Wallpaper](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Wallpaper) — Portal for setting the desktop's Wallpaper

[II. Portal Backend API Reference](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771421251808)
- [org.freedesktop.impl.portal.Access](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Access) — Interface for presenting an access dialog
- [org.freedesktop.impl.portal.Account](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Account) — Backend for the portal for obtaining user information
- [org.freedesktop.impl.portal.AppChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.AppChooser) — Interface for choosing an application
- [org.freedesktop.impl.portal.Background](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Background) — Background portal backend interface
- [org.freedesktop.impl.portal.Email](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Email) — Email portal backend interface
- [org.freedesktop.impl.portal.FileChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.FileChooser) — File chooser portal backend interface
- [org.freedesktop.impl.portal.Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Inhibit) — Inhibit portal backend interface
- [org.freedesktop.impl.portal.Lockdown](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Lockdown) — Lockdown backend interface
- [org.freedesktop.impl.portal.Notification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Notification) — Notification portal backend interface
- [org.freedesktop.impl.portal.PermissionStore](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.PermissionStore) — Database to store permissions
- [org.freedesktop.impl.portal.Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Print) — Print portal backend interface
- [org.freedesktop.impl.portal.RemoteDesktop](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.RemoteDesktop) — Remote desktop portal backend interface
- [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Request) — Shared request interface
- [org.freedesktop.impl.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.ScreenCast) — Screen cast portal backend interface
- [org.freedesktop.impl.portal.Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Screenshot) — Screenshot portal backend interface
- [org.freedesktop.impl.portal.Secret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Secret) — Secret portal backend interface
- [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Session) — Shared session interface
- [org.freedesktop.impl.portal.Settings](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Settings) — Settings portal backend interface
- [org.freedesktop.impl.portal.Wallpaper](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Wallpaper) — Portal for setting the desktop's Wallpaper

##  Common Conventions
[Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#common-conventions)

[Portal requests](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771422940768)

[Sessions](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771424242304)

[Parent window identifiers](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window)

Requests made via portal interfaces generally involve user interaction, and dialogs that can stay open for a long time. Therefore portal APIs don't just use async method calls (which time out after at most 25 seconds), but instead return results via a Response signal on Request objects.

Portal APIs don't use properties very much. This is partially because we need to be careful about the flow of information, and partially because it would be unexpected to have a dialog appear when you just set a property.

The general flow of the portal API is that the application makes a portal request, the portal replies to that method call with a handle (i.e. object path) to Request object that corresponds to the request. The object is exported on the bus and stays alive as long as the user interaction lasts. When the user interaction is over, the portal sends a Response signal back to the application with the results from the interaction, if any.

To avoid a race condition between the caller subscribing to the signal after receiving the reply for the method call and the signal getting emitted, a convention for Request object paths has been established that allows the caller to subscribe to the signal before making the method call.

Some portal requests are connected to each other and need to be used in sequence. The pattern we use in such cases is a Session object. Just like Requests, Sessions are represented by an object path, that is returned by the initial CreateSession call. Subsequent calls take the object path of the session they operate on as an argument.

Sessions can be ended from the application side by calling the Close() method. They can also be closed from the service side, in which case the ::Closed signal is emitted to inform the application.

Most portals interact with the user by showing dialogs. These dialogs should generally be placed on top of the application window that triggered them. To arrange this, the compositor needs to know about the application window. Many portal requests expect a "parent_window" string argument for this reason.

Under X11, the "parent_window" argument should have the form "x11:_`XID`_", where _`XID`_ is the XID of the application window in hexadecimal notation.

Under Wayland, it should have the form "wayland:_`HANDLE`_", where _`HANDLE`_ is a surface handle obtained with the [xdg_foreign](https://github.com/wayland-project/wayland-protocols/blob/master/unstable/xdg-foreign/xdg-foreign-unstable-v2.xml) protocol.

For other windowing systems, or if you don't have a suitable handle, just pass an empty string for "parent_window".

---

Portal interfaces are available to sandboxed applications with the default filtered session bus access of Flatpak. Unless otherwise specified, they appear under the bus name org.freedesktop.portal.Desktop and the object path /org/freedesktop/portal/desktop on the session bus.

## I. Portal API Reference
[I. Portal API Reference](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771425398192)

- [org.freedesktop.portal.Account](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Account) — Portal for obtaining information about the user

- [org.freedesktop.portal.Background](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Background) — Portal for requesting autostart and background activity

- [org.freedesktop.portal.Camera](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Camera) — Camera portal

- [org.freedesktop.portal.Device](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Device) — Portal for device access

- [org.freedesktop.portal.Documents](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Documents) — Document portal

- [org.freedesktop.portal.DynamicLauncher](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.DynamicLauncher) — Portal for installing application launchers onto the

- [org.freedesktop.portal.Email](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Email) — Portal for sending email

- [org.freedesktop.portal.FileChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.FileChooser) — File chooser portal

- [org.freedesktop.portal.FileTransfer](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.FileTransfer) — Portal for transferring files between apps

- [org.freedesktop.portal.Flatpak.UpdateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Flatpak.UpdateMonitor)

- [org.freedesktop.portal.Flatpak](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Flatpak) — Flatpak portal

- [org.freedesktop.portal.GameMode](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.GameMode) — Portal for accessing GameMode

- [org.freedesktop.portal.Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Inhibit) — Portal for inhibiting session transitions

- [org.freedesktop.portal.Location](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Location) — Portal for obtaining information about the location

- [org.freedesktop.portal.MemoryMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.MemoryMonitor) — Memory monitoring portal

- [org.freedesktop.portal.NetworkMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.NetworkMonitor) — Network monitoring portal

- [org.freedesktop.portal.Notification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Notification) — Portal for sending notifications

- [org.freedesktop.portal.OpenURI](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.OpenURI) — Portal for opening URIs

- [org.freedesktop.portal.PowerProfileMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.PowerProfileMonitor) — Power Profile monitoring portal

- [org.freedesktop.portal.Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Print) — Portal for printing

- [org.freedesktop.portal.ProxyResolver](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.ProxyResolver) — Proxy information

- [org.freedesktop.portal.Realtime](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Realtime) — Portal for setting threads to realtime

- [org.freedesktop.portal.RemoteDesktop](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.RemoteDesktop) — Remote desktop portal

- [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Request) — Shared request interface

- [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.ScreenCast) — Screen cast portal

- [org.freedesktop.portal.Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Screenshot) — Portal for taking screenshots

- [org.freedesktop.portal.Secret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Secret) — Portal for retrieving application secret

- [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Session) — Shared session interface

- [org.freedesktop.portal.Settings](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Settings) — Settings interface

- [org.freedesktop.portal.Trash](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Trash) — Portal for trashing files

- [org.freedesktop.portal.Wallpaper](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.Wallpaper) — Portal for setting the desktop's Wallpaper

- Name

org.freedesktop.portal.Account — Portal for obtaining information about the user

- Methods


[GetUserInformation](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Account.GetUserInformation "The GetUserInformation() method") (IN  s     window,
                    IN  a{sv} options,
                    OUT o     handle);


- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Account.version "The "version" property")  readable   u


- Description

This simple interface lets sandboxed applications query basic information about the user, like his name and avatar photo.

This documentation describes version 1 of this interface.

- Method Details

- The GetUserInformation() method

GetUserInformation (IN  s     window,
                    IN  a{sv} options,
                    OUT o     handle);

Gets information about the user.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

reason s

A string that can be shown in the dialog to expain why the information is needed. This should be a complete sentence that explains what the application will do with the returned information, for example: Allows your personal information to be included with recipes you share with your friends.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

id s

The user id.

name s

The users real name.

image s

The uri of an image file for the users avatar photo.

``IN s _`window`_``:

Identifier for the window

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

### The RequestBackground() method
- Name

org.freedesktop.portal.Background — Portal for requesting autostart and background activity

- Methods


[RequestBackground](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Background.RequestBackground "The RequestBackground() method") (IN  s     parent_window,
                   IN  a{sv} options,
                   OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Background.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications request that the application is allowed to run in the background or started automatically when the user logs in.

This documentation describes version 1 of this interface.

- Method Details


RequestBackground (IN  s     parent_window,
                   IN  a{sv} options,
                   OUT o     handle);

Requests that the application is allowed to run in the background.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

reason s

User-visible reason for the request.

autostart b

TRUE if the app also wants to be started automatically at login.

commandline as

Commandline to use add when autostarting at login. If this is not specified, the Exec line from the desktop file will be used.

dbus-activatable b

If TRUE, use D-Bus activation for autostart.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

background b

TRUE if the application is allowed to run in the background.

autostart b

TRUE if the application is will be autostarted.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

### The AccessCamera() method
- Name

org.freedesktop.portal.Camera — Camera portal

- Methods


[AccessCamera](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Camera.AccessCamera "The AccessCamera() method")       (IN  a{sv} options,
                    OUT o     handle);
[OpenPipeWireRemote](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Camera.OpenPipeWireRemote "The OpenPipeWireRemote() method") (IN  a{sv} options,
                    OUT h     fd);

- Properties

[IsCameraPresent](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Camera.IsCameraPresent "The "IsCameraPresent" property")  readable   b
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Camera.version "The "version" property")          readable   u



- Description

The camera portal enables applications to access camera devices, such as web cams.

- Method Details


AccessCamera (IN  a{sv} options,
              OUT o     handle);

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

Following the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal, if granted, [OpenPipeWireRemote()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Camera.OpenPipeWireRemote "The OpenPipeWireRemote() method") can be used to open a PipeWire remote.

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The OpenPipeWireRemote() method

OpenPipeWireRemote (IN  a{sv} options,
                    OUT h     fd);

Open a file descriptor to the PipeWire remote where the camera nodes are available. The file descriptor should be used to create a `pw_core` object, by using `pw_context_connect_fd`.

This method will only succeed if the application already has permission to access camera devices.

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT h _`fd`_``:

File descriptor of an open PipeWire remote.

### The "IsCameraPresent" property
- Property Details

IsCameraPresent  readable   b

A boolean stating whether there is any cameras available.

- The "version" property

version  readable   u

---

### The AccessDevice() method
- Name

org.freedesktop.portal.Device — Portal for device access

- Methods


[AccessDevice](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Device.AccessDevice "The AccessDevice() method") (IN  u     pid,
              IN  as    devices,
              IN  a{sv} options,
              OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Device.version "The "version" property")  readable   u



- Description

This interface lets services ask if an application should get access to devices such as microphones, speakers or cameras. Not a portal in the strict sense, since the API is not directly accessible to applications inside the sandbox.

This documentation describes version 1 of this interface.

- Method Details


AccessDevice (IN  u     pid,
              IN  as    devices,
              IN  a{sv} options,
              OUT o     handle);

Asks for access to a device.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

``IN u _`pid`_``:

the pid of the application on whose behalf the request is made

``IN as _`devices`_``:

a list of devices to request access to. Supported values are 'microphone', 'speakers', 'camera'. Asking for multiple devices at the same time may or may not be supported

``IN a{sv} _`options`_``:

vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Documents — Document portal

- Methods


[GetMountPoint](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.GetMountPoint "The GetMountPoint() method")     (OUT ay     path);
[Add](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.Add "The Add() method")               (IN  h      o_path_fd,
                   IN  b      reuse_existing,
                   IN  b      persistent,
                   OUT s      doc_id);
[AddNamed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.AddNamed "The AddNamed() method")          (IN  h      o_path_parent_fd,
                   IN  ay     filename,
                   IN  b      reuse_existing,
                   IN  b      persistent,
                   OUT s      doc_id);
[AddFull](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.AddFull "The AddFull() method")           (IN  ah     o_path_fds,
                   IN  u      flags,
                   IN  s      app_id,
                   IN  as     permissions,
                   OUT as     doc_ids,
                   OUT a{sv}  extra_out);
[AddNamedFull](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.AddNamedFull "The AddNamedFull() method")      (IN  h      o_path_fd,
                   IN  ay     filename,
                   IN  u      flags,
                   IN  s      app_id,
                   IN  as     permissions,
                   OUT s      doc_id,
                   OUT a{sv}  extra_out);
[GrantPermissions](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.GrantPermissions "The GrantPermissions() method")  (IN  s      doc_id,
                   IN  s      app_id,
                   IN  as     permissions);
[RevokePermissions](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.RevokePermissions "The RevokePermissions() method") (IN  s      doc_id,
                   IN  s      app_id,
                   IN  as     permissions);
[Delete](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.Delete "The Delete() method")            (IN  s      doc_id);
[Lookup](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.Lookup "The Lookup() method")            (IN  ay     filename,
                   OUT s      doc_id);
[Info](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.Info "The Info() method")              (IN  s      doc_id,
                   OUT ay     path,
                   OUT a{sas} apps);
[List](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.List "The List() method")              (IN  s      app_id,
                   OUT a{say} docs);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Documents.version "The "version" property")  readable   u



- Description

The document portal allows to make files from the outside world available to sandboxed applications in a controlled way.

Exported files will be made accessible to the application via a fuse filesystem that gets mounted at /run/user/$UID/doc/. The filesystem gets mounted both outside and inside the sandbox, but the view inside the sandbox is restricted to just those files that the application is allowed to access.

Individual files will appear at /run/user/$UID/doc/$DOC_ID/filename, where $DOC_ID is the ID of the file in the document store. It is returned by the [Add()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.Add "The Add() method") and [AddNamed()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.AddNamed "The AddNamed() method") calls.

The permissions that the application has for a document store entry (see [GrantPermissions()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.GrantPermissions "The GrantPermissions() method")) are reflected in the POSIX mode bits in the fuse filesystem.

The D-Bus interface for the document portal is available under the bus name org.freedesktop.portal.Documents and the object path /org/freedesktop/portal/documents.

This documentation describes version 4 of this interface.

- Method Details

### The GetMountPoint() method

GetMountPoint (OUT ay path);

Returns the path at which the document store fuse filesystem is mounted. This will typically be /run/user/$UID/doc/.

``OUT ay _`path`_``:

the path at which the fuse filesystem is mounted

### The Add() method

Add (IN  h o_path_fd,
     IN  b reuse_existing,
     IN  b persistent,
     OUT s doc_id);

Adds a file to the document store. The file is passed in the form of an open file descriptor to prove that the caller has access to the file.

``IN h _`o_path_fd`_``:

open file descriptor for the file to add

``IN b _`reuse_existing`_``:

whether to reuse an existing document store entry for the file

``IN b _`persistent`_``:

whether to add the file only for this session or permanently

``OUT s _`doc_id`_``:

the ID of the file in the document store

### The AddNamed() method

AddNamed (IN  h  o_path_parent_fd,
          IN  ay filename,
          IN  b  reuse_existing,
          IN  b  persistent,
          OUT s  doc_id);

Creates an entry in the document store for writing a new file.

``IN h _`o_path_parent_fd`_``:

open file descriptor for the parent directory

``IN ay _`filename`_``:

the basename for the file

``IN b _`reuse_existing`_``:

whether to reuse an existing document store entry for the file

``IN b _`persistent`_``:

whether to add the file only for this session or permanently

``OUT s _`doc_id`_``:

the ID of the file in the document store

### The AddFull() method

AddFull (IN  ah    o_path_fds,
         IN  u     flags,
         IN  s     app_id,
         IN  as    permissions,
         OUT as    doc_ids,
         OUT a{sv} extra_out);

Adds multiple files to the document store. The file is passed in the form of an open file descriptor to prove that the caller has access to the file.

If the as-needed-by-app flag is given, files will only be added to the document store if the application does not already have access to them. For files that are not added to the document store, the doc_ids array will contain an empty string.

Additionally, if app_id is specified, it will be given the permissions listed in [GrantPermissions()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.GrantPermissions "The GrantPermissions() method").

The method also returns some extra info that can be used to avoid multiple roundtrips. For now it only contains as "mountpoint", the fuse mountpoint of the document portal.

This method was added in version 2 of the [org.freedesktop.portal.Documents](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Documents.top_of_page) interface.

Support for exporting directories were added in version 4 of the [org.freedesktop.portal.Documents](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Documents.top_of_page) interface.

``IN ah _`o_path_fds`_``:

open file descriptors for the files to export

``IN u _`flags`_``:

flags, 1 == reuse_existing, 2 == persistent, 4 == as-needed-by-app, 8 = export directory

``IN s _`app_id`_``:

an application ID, or empty string

``IN as _`permissions`_``:

the permissions to grant, possible values are 'read', 'write', 'grant-permissions' and 'delete'

``OUT as _`doc_ids`_``:

the IDs of the files in the document store

``OUT a{sv} _`extra_out`_``:

### The AddNamedFull() method

AddNamedFull (IN  h     o_path_fd,
              IN  ay    filename,
              IN  u     flags,
              IN  s     app_id,
              IN  as    permissions,
              OUT s     doc_id,
              OUT a{sv} extra_out);

Creates an entry in the document store for writing a new file.

If the as-needed-by-app flag is given, file will only be added to the document store if the application does not already have access to it. For file that is not added to the document store, the doc_id will contain an empty string.

Additionally, if app_id is specified, it will be given the permissions listed in [GrantPermissions()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Documents.GrantPermissions "The GrantPermissions() method").

The method also returns some extra info that can be used to avoid multiple roundtrips. For now it only contains as "mountpoint", the fuse mountpoint of the document portal.

This method was added in version 3 of the [org.freedesktop.portal.Documents](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Documents.top_of_page) interface.

``IN h _`o_path_fd`_``:

``IN ay _`filename`_``:

the basename for the file

``IN u _`flags`_``:

flags, 1 == reuse_existing, 2 == persistent, 4 == as-needed-by-app

``IN s _`app_id`_``:

an application ID, or empty string

``IN as _`permissions`_``:

the permissions to grant, possible values are 'read', 'write', 'grant-permissions' and 'delete'

``OUT s _`doc_id`_``:

the ID of the file in the document store

``OUT a{sv} _`extra_out`_``:

### The GrantPermissions() method

GrantPermissions (IN  s  doc_id,
                  IN  s  app_id,
                  IN  as permissions);

Grants access permissions for a file in the document store to an application.

This call is available inside the sandbox if the application has the 'grant-permissions' permission for the document.

``IN s _`doc_id`_``:

the ID of the file in the document store

``IN s _`app_id`_``:

the ID of the application to which permissions are granted

``IN as _`permissions`_``:

the permissions to grant, possible values are 'read', 'write', 'grant-permissions' and 'delete'

### The RevokePermissions() method

RevokePermissions (IN  s  doc_id,
                   IN  s  app_id,
                   IN  as permissions);

Revokes access permissions for a file in the document store from an application.

This call is available inside the sandbox if the application has the 'grant-permissions' permission for the document.

``IN s _`doc_id`_``:

the ID of the file in the document store

``IN s _`app_id`_``:

the ID of the application from which permissions are revoked

``IN as _`permissions`_``:

the permissions to revoke, possible values are 'read', 'write', 'grant-permissions' and 'delete'

### The Delete() method

Delete (IN  s doc_id);

Removes an entry from the document store. The file itself is not deleted.

This call is available inside the sandbox if the application has the 'delete' permission for the document.

``IN s _`doc_id`_``:

the ID of the file in the document store

### The Lookup() method

Lookup (IN  ay filename,
        OUT s  doc_id);

Looks up the document ID for a file.

This call is not available inside the sandbox.

``IN ay _`filename`_``:

a path in the host filesystem

``OUT s _`doc_id`_``:

the ID of the file in the document store, or '' if the file is not in the document store

### The Info() method

Info (IN  s      doc_id,
      OUT ay     path,
      OUT a{sas} apps);

Gets the filesystem path and application permissions for a document store entry.

This call is not available inside the sandbox.

``IN s _`doc_id`_``:

the ID of the file in the document store

``OUT ay _`path`_``:

the path for the file in the host filesystem

``OUT a{sas} _`apps`_``:

a dictionary mapping application IDs to the permissions for that application

### The List() method

List (IN  s      app_id,
      OUT a{say} docs);

Lists documents in the document store for an application (or for all applications).

This call is not available inside the sandbox.

``IN s _`app_id`_``:

an application ID, or '' to list all documents

``OUT a{say} _`docs`_``:

a dictonary mapping document IDs to their filesystem path

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.DynamicLauncher — Portal for installing application launchers onto the

- Methods


[Install](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.Install "The Install() method")             (IN  s     token,
                     IN  s     desktop_file_id,
                     IN  s     desktop_entry,
                     IN  a{sv} options);
[PrepareInstall](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.PrepareInstall "The PrepareInstall() method")      (IN  s     parent_window,
                     IN  s     name,
                     IN  v     icon_v,
                     IN  a{sv} options,
                     OUT o     handle);
[RequestInstallToken](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.RequestInstallToken "The RequestInstallToken() method") (IN  s     name,
                     IN  v     icon_v,
                     IN  a{sv} options,
                     OUT s     token);
[Uninstall](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.Uninstall "The Uninstall() method")           (IN  s     desktop_file_id,
                     IN  a{sv} options);
[GetDesktopEntry](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.GetDesktopEntry "The GetDesktopEntry() method")     (IN  s     desktop_file_id,
                     OUT s     contents);
[GetIcon](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.GetIcon "The GetIcon() method")             (IN  s     desktop_file_id,
                     OUT v     icon_v,
                     OUT s     icon_format,
                     OUT u     icon_size);
[Launch](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.Launch "The Launch() method")              (IN  s     desktop_file_id,
                     IN  a{sv} options);

- Properties

[SupportedLauncherTypes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-DynamicLauncher.SupportedLauncherTypes "The "SupportedLauncherTypes" property")  readable   u
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-DynamicLauncher.version "The "version" property")                 readable   u



- Description

desktop

The DynamicLauncher portal allows sandboxed (or unsandboxed) applications to install launchers (.desktop files) which have an icon associated with them and which execute a command in the application. The desktop environment would display the launcher to the user in its menu of installed applications. For example this can be used by a sandboxed browser to install web app launchers. The portal also allows apps to uninstall the launchers, launch them, and read the desktop file and icon data for them.

The standard way to install a launcher is to use the PrepareInstall() method which results in a dialog being presented to the user so they can confirm they want to install the launcher. Then, the token returned by PrepareInstall() would be passed to the Install() method to complete the installation.

However, in the rare circumstance that an unsandboxed process such as a system component needs to install a launcher without user interaction, this can be accomplished by using the RequestInstallToken() method and passing the acquired token to Install().

This documentation describes version 1 of this interface.

- Method Details

### The Install() method

Install (IN  s     token,
         IN  s     desktop_file_id,
         IN  s     desktop_entry,
         IN  a{sv} options);

Installs a .desktop launcher and icon into appropriate directories to allow the desktop environment to find them. Please note that this method overwrites any existing launcher with the same id. If you want to present the user with a confirmation dialog in that case, you can check for it using the GetDesktopEntry() method and clean up any state from the previous launcher if you want.

_`token`_ must be a token that was returned by a previous [PrepareInstall()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.PrepareInstall "The PrepareInstall() method") or[RequestInstallToken()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.RequestInstallToken "The RequestInstallToken() method") call. The token can only be used once and is valid for up to five minutes.

The icon and name used for the launcher will be the ones from the previous [PrepareInstall()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.PrepareInstall "The PrepareInstall() method")or [RequestInstallToken()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.RequestInstallToken "The RequestInstallToken() method") call.

The _`desktop_file_id`_ must have ".desktop" as a suffix. Except in the special case when the calling process has no associated app ID, _`desktop_file_id`_ must have the app ID followed by a period as a prefix, regardless of whether the calling process is sandboxed or unsandboxed.

The _`desktop_entry`_ should be a valid desktop entry file beginning with "[Desktop Entry]", except it should not include Name= or Icon= entries (if present, these will be overwritten by the portal implementation). The Exec= entry will be rewritten to call the sandboxed application e.g. via "flatpak run", if the application is sandboxed.

It is recommended to include a TryExec= line with either a binary name or an absolute path. The launcher will be deleted if the TryExec binary cannot be found on session start.

The _`options`_ vardict currently has no supported entries.

``IN s _`token`_``:

Token proving authorization of the installation

``IN s _`desktop_file_id`_``:

The .desktop file name to be used

``IN s _`desktop_entry`_``:

The text of the Desktop Entry file to be installed, see below

``IN a{sv} _`options`_``:

Vardict with optional further information

### The PrepareInstall() method

PrepareInstall (IN  s     parent_window,
                IN  s     name,
                IN  v     icon_v,
                IN  a{sv} options,
                OUT o     handle);

Presents a dialog to the user to allow them to see the icon, potentially change the name, and confirm installation of the launcher.

Supported keys in the _`options`_ vardict:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

modal b

Whether to make the dialog modal. Defaults to yes.

launcher_type u

The type of launcher being created. For supported values see the SupportedLauncherTypes property. Defaults to "Application".

target s

For a launcher of type "Webapp", this is the URL of the web app being installed. This is displayed in the user-facing dialog. For other launcher types, this is not needed.

editable_name b

If true, the user will be able to edit the name of the launcher. Defaults to true.

editable_icon b

If true, the user will be able to edit the icon of the launcher, if the implementation supports this. Defaults to false.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

name s

The name chosen by the user for the launcher.

token s

Token that can be passed to a subsequent [Install()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-DynamicLauncher.Install "The Install() method") call to complete the installation without another dialog.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`name`_``:

The default name for the launcher

``IN v _`icon_v`_``:

A #GBytesIcon icon as returned by g_icon_serialize(). Must be a png or jpeg no larger than 512x512, or an svg

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The RequestInstallToken() method

RequestInstallToken (IN  s     name,
                     IN  v     icon_v,
                     IN  a{sv} options,
                     OUT s     token);

This method is intended for use only by specific components that have their application ID allowlisted in the portal backend (e.g. GNOME Software and KDE Discover). It is otherwise not guaranteed to work.

The token returned by this method can be used to avoid the need for a confirmation dialog; the token can be passed to the Install() method just as if it were acquired via the PrepareInstall() method.

The _`options`_ vardict currently has no supported entries.

``IN s _`name`_``:

The name that will be used in the desktop file

``IN v _`icon_v`_``:

A #GBytesIcon icon as returned by g_icon_serialize(). Must be a png or jpeg no larger than 512x512, or an svg

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT s _`token`_``:

the token to be used with the Install() method

### The Uninstall() method

Uninstall (IN  s     desktop_file_id,
           IN  a{sv} options);

This method deletes the desktop file and corresponding icon from the appropriate directories to remove the launcher referred to by _`desktop_file_id`_.

The _`desktop_file_id`_ must have ".desktop" as a suffix. Except in the special case when the calling process has no associated app ID, _`desktop_file_id`_ must have the app ID followed by a period as a prefix, regardless of whether the calling process is sandboxed or unsandboxed.

For example, Epiphany, which has the app ID "org.gnome.Epiphany" in stable builds, might use a _`desktop_file_id`_ like "org.gnome.Epiphany.WebApp_e9d0e1e4b0a10856aa3b38d9eb4375de4070d043.desktop" In that example the desktop file would exist at the path "~/.local/share/xdg-desktop-portal/applications/org.gnome.Epiphany.WebApp_e9d0e1e4b0a10856aa3b38d9eb4375de4070d043.desktop" with a sym link in "~/.local/share/applications/". The checksum at the end of the file name is an implementation detail in Epiphany and not required by the portal.

This method is intended to be called by the application that created the launcher, e.g. a web browser, so it can clean up associated data as part of the uninstallation. Consequently, the proper way for a software center to remove a launcher is by using the APIs provided by the application that installed it. For example, for GNOME Software to remove web launchers created by Epiphany, it would use the org.gnome.Epiphany.WebAppProvider D-Bus interface.

Please note that this method call will fail if the specified launcher already does not exist.

The _`options`_ vardict currently has no supported entries.

``IN s _`desktop_file_id`_``:

The .desktop file name

``IN a{sv} _`options`_``:

Vardict with optional further information

### The GetDesktopEntry() method

GetDesktopEntry (IN  s desktop_file_id,
                 OUT s contents);

This function returns the contents of a desktop file with the name _`desktop_file_id`_ in _`contents`_.

The _`desktop_file_id`_ must have ".desktop" as a suffix. Except in the special case when the calling process has no associated app ID, _`desktop_file_id`_ must have the app ID followed by a period as a prefix.

This method only works for desktop files that were created by the dynamic launcher portal.

``IN s _`desktop_file_id`_``:

The .desktop file name

``OUT s _`contents`_``:

the contents of the named .desktop file

### The GetIcon() method

GetIcon (IN  s desktop_file_id,
         OUT v icon_v,
         OUT s icon_format,
         OUT u icon_size);

This function returns the contents of the icon specified in the "Icon" key of the desktop file with the name _`desktop_file_id`_ in _`icon_v`_. The icon \#GVariant can be passed to g_icon_deserialize() to reconstruct the \#GIcon.

The _`desktop_file_id`_ must have ".desktop" as a suffix. Except in the special case when the calling process has no associated app ID, _`desktop_file_id`_ must have the app ID followed by a period as a prefix.

The format and size of the icon are returned in _`icon_format`_ and _`icon_size`_. For svg icons, _`icon_size`_ is currently always set to 4096, but don't depend on that as it may change in the future.

This method only works for desktop files that were created by the dynamic launcher portal.

``IN s _`desktop_file_id`_``:

The .desktop file name

``OUT v _`icon_v`_``:

the icon as a serialized #GBytesIcon

``OUT s _`icon_format`_``:

one of "png", "jpeg", "svg"

``OUT u _`icon_size`_``:

the width and height in pixels of the icon

### The Launch() method

Launch (IN  s     desktop_file_id,
        IN  a{sv} options);

This function launches the app specified by _`desktop_file_id`_.

The _`desktop_file_id`_ must have ".desktop" as a suffix. Except in the special case when the calling process has no associated app ID, _`desktop_file_id`_ must have the app ID followed by a period as a prefix.

This method only works for desktop files that were created by the dynamic launcher portal.

Supported keys in the _`options`_ vardict include:

activation_token s

A token that can be used to activate the chosen application.

The activation_token option was introduced in version 1 of the interface.

``IN s _`desktop_file_id`_``:

The .desktop file name

``IN a{sv} _`options`_``:

Vardict with optional further onformation

- Property Details

### The "SupportedLauncherTypes" property

SupportedLauncherTypes  readable   u

A bitmask of available launcher types. Currently defined types are:

1: Application. A launcher that represents an application.

2: Webapp. A launcher that represents a web app.

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Email — Portal for sending email

- Methods


[ComposeEmail](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Email.ComposeEmail "The ComposeEmail() method") (IN  s     parent_window,
              IN  a{sv} options,
              OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Email.version "The "version" property")  readable   u



- Description

This simple portal lets sandboxed applications request to send an email, optionally providing an address, subject, body and attachments.

This documentation describes version 3 of this interface.

- Method Details

### The ComposeEmail() method

ComposeEmail (IN  s     parent_window,
              IN  a{sv} options,
              OUT o     handle);

Presents a window that lets the user compose an email.

Note that the default email client for the host will need to support mailto: URIs following [RFC 2368](https://tools.ietf.org/html/rfc2368), with "cc", "bcc", "subject" and "body" query keys each corresponding to the email header of the same name, and with each attachment being passed as a "file://" URI as a value in an "attachment" query key.

For example:

        mailto:foo_`bar`_.com,baz_`bar`_.com?cc=ceo_`bar`_.com&subject=Test`20e`-mail`20subject`&attachment=file://path/to/full/file.txt
        

would send a mail to "foo_`bar`_.com", "baz_`bar`_.com", with a CC: to "ceo_`bar`_.com", with the subject "Test e-mail subject" and the file pointed by URI "file://path/to/full/file.txt" as an attachment.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

address s

The email address to send to.

addresses as

Email addresses to send to. This will be used in addition to address.

This option was added in version 3.

cc as

Email addresses to cc.

This option was added in version 3.

bcc as

Email addresses to bcc.

This option was added in version 3.

subject s

The subject for the email.

body s

The body for the email.

attachment_fds ah

File descriptors for files to attach.

All the keys in the options are are optional.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.FileChooser — File chooser portal

- Methods


[OpenFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method")  (IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT o     handle);
[SaveFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.SaveFile "The SaveFile() method")  (IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT o     handle);
[SaveFiles](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.SaveFiles "The SaveFiles() method") (IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-FileChooser.version "The "version" property")  readable   u



- Description

The FileChooser portal allows sandboxed applications to ask the user for access to files outside the sandbox. The portal backend will present the user with a file chooser dialog.

The selected files will be made accessible to the application via the document portal, and the returned URI will point into the document portal fuse filesystem in /run/user/$UID/doc/.

This documentation describes version 3 of this interface.

- Method Details

### The OpenFile() method

OpenFile (IN  s     parent_window,
          IN  s     title,
          IN  a{sv} options,
          OUT o     handle);

Asks to open one or more files.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

accept_label s

Label for the accept button. Mnemonic underlines are allowed.

modal b

Whether the dialog should be modal. Default is yes.

multiple b

Whether multiple files can be selected or not. Default is single-selection.

directory b

Whether to select for folders instead of files. Default is to select files. This option was added in version 3.

filters a(sa(us))

List of serialized file filters.

Each item in the array specifies a single filter to offer to the user. The first string is a user-visible name for the filter. The a(us) specifies a list of filter strings, which can be either a glob-style pattern (indicated by 0) or a mimetype (indicated by 1). Patterns are case-sensitive. To match different capitalizations of, e.g. '*.ico', use a pattern like '*.[iI][cC][oO]'.

Example: [('Images', [(0, '*.ico'), (1, 'image/png')]), ('Text', [(0, '*.txt')])]

Note that filters are purely there to aid the user in making a useful selection. The portal may still allow the user to select files that don't match any filter criteria, and applications must be prepared to handle that.

current_filter (sa(us))

Request that this filter be set by default at dialog creation. If the filters list is nonempty, it should match a filter in the list to set the default filter from the list. Alternatively, it may be specified when the list is empty to apply the filter unconditionally.

choices a(ssa(ss)s)

List of serialized combo boxes to add to the file chooser.

For each element, the first string is an ID that will be returned with the response, the second string is a user-visible label. The a(ss) is the list of choices, each being an ID and a user-visible label. The final string is the initial selection, or "", to let the portal decide which choice will be initially selected. None of the strings, except for the initial selection, should be empty.

As a special case, passing an empty array for the list of choices indicates a boolean choice that is typically displayed as a check button, using "true" and "false" as the choices.

Example: [('encoding', 'Encoding', [('utf8', 'Unicode (UTF-8)'), ('latin15', 'Western')], 'latin15'), ('reencode', 'Reencode', [], 'false')]

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

uris as

An array of strings containing the uris of the selected files.

choices a(ss)

An array of pairs of strings, the first string being the ID of a combobox that was passed into this call, the second string being the selected option.

Example: [('encoding', 'utf8'), ('reencode', 'true')]

current_filter (sa(us))

The filter that was selected. This may match a filter in the filter list or another filter that was applied unconditionally.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The SaveFile() method

SaveFile (IN  s     parent_window,
          IN  s     title,
          IN  a{sv} options,
          OUT o     handle);

Asks for a location to save a file.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

accept_label s

Label for the accept button. Mnemonic underlines are allowed.

modal b

Whether the dialog should be modal. Default is yes.

filters a(sa(us))

List of serialized file filters. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

Request that this filter be set by default at dialog creation. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

choices a(ssa(ss)s)

List of serialized combo boxes. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_name s

Suggested filename.

current_folder ay

Suggested folder to save the file in. The byte array is expected to be null-terminated.

current_file ay

The current file (when saving an existing file). The byte array is expected to be null-terminated.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

uris as

An array of strings containing the uri of the selected file.

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

The filter that was selected. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The SaveFiles() method

SaveFiles (IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT o     handle);

Asks for a folder as a location to save one or more files. The names of the files will be used as-is and appended to the selected folder's path in the list of returned files. If the selected folder already contains a file with one of the given names, the portal may prompt or take some other action to construct a unique file name and return that instead.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

accept_label s

Label for the accept button. Mnemonic underlines are allowed.

modal b

Whether the dialog should be modal. Default is yes.

choices a(ssa(ss)s)

List of serialized combo boxes. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_folder ay

Suggested folder to save the files in. The byte array is expected to be null-terminated.

files aay

An array of file names to be saved. The byte arrays are expected to be null-terminated.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

uris as

An array of strings containing the uri corresponding to each file given by _`options`_, in the same order. Note that the file names may have changed, for example if a file with the same name in the selected folder already exists.

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.FileTransfer — Portal for transferring files between apps

- Methods


[StartTransfer](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StartTransfer "The StartTransfer() method") (IN  a{sv} options,
               OUT s     key);
[AddFiles](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.AddFiles "The AddFiles() method")      (IN  s     key,
               IN  ah    fds,
               IN  a{sv} options);
[RetrieveFiles](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") (IN  s     key,
               IN  a{sv} options,
               OUT as    files);
[StopTransfer](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StopTransfer "The StopTransfer() method")  (IN  s     key);

### Signals

[TransferClosed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-FileTransfer.TransferClosed "The "TransferClosed" signal") (s key);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-FileTransfer.version "The "version" property")  readable   u



- Description

The [org.freedesktop.portal.FileTransfer](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-FileTransfer.top_of_page) portal operates as a middle-man between apps when transferring files via drag-and-drop or copy-paste, taking care of the necessary exporting of files in the document portal.

Toolkits are expected to use the application/vnd.portal.filetransfer mimetype when using this mechanism for file exchange via copy-paste or drag-and-drop.

The data that is transmitted with this mimetype should be the key returned by the [StartTransfer()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StartTransfer "The StartTransfer() method") method. Upon receiving this mimetype, the target should call [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method")with the key, to obtain the list of files. The portal will take care of exporting files in the document store as necessary to make them accessible to the target.

The D-Bus interface for the this portal is available under the bus name org.freedesktop.portal.Documents and the object path /org/freedesktop/portal/documents.

This documentation describes version 1 of this interface.

- Method Details

### The StartTransfer() method

StartTransfer (IN  a{sv} options,
               OUT s     key);

Starts a session for a file transfer. The caller should call [AddFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.AddFiles "The AddFiles() method") at least once, to add files to this session.

Another application can then call [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") to obtain them, if it has the _`session`_ and _`secret`_.

Supported keys in the _`options`_ vardict include:

writable b

Whether to allow the chosen application to write to the files. Default: False

This key only takes effect for files that need to be exported in the document portal for the receiving app. But it does require the passed-in file descriptors to be writable.

autostop b

Whether to stop the transfer automatically after the first [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") call. Default: True

``IN a{sv} _`options`_``:

Vardict with optional further onformation

``OUT s _`key`_``:

a key that needs to be passed to [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") to obtain the files

### The AddFiles() method

AddFiles (IN  s     key,
          IN  ah    fds,
          IN  a{sv} options);

Adds files to a session. This method can be called multiple times on a given session. Note that only regular files (not directories) can be added.

Note that the session bus often has a low limit of file descriptors per message (typically, 16), so you may have to send large file lists with multiple [AddFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.AddFiles "The AddFiles() method") calls.

``IN s _`key`_``:

A key returned by [StartTransfer()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StartTransfer "The StartTransfer() method")

``IN ah _`fds`_``:

File descriptors for the files to register

``IN a{sv} _`options`_``:

### The RetrieveFiles() method

RetrieveFiles (IN  s     key,
               IN  a{sv} options,
               OUT as    files);

Retrieves files that were previously added to the session with [AddFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.AddFiles "The AddFiles() method"). The files will be exported in the document portal as-needed for the caller, and they will be writable if the owner of the session allowed it.

After the first [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") call, the session will be closed by the portal, unless _`autostop`_has been set to False.

``IN s _`key`_``:

A key returned by [StartTransfer()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StartTransfer "The StartTransfer() method")

``IN a{sv} _`options`_``:

Vardict with optional further onformation

``OUT as _`files`_``:

list of paths

### The StopTransfer() method

StopTransfer (IN  s key);

Ends the transfer. Further calls to [AddFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.AddFiles "The AddFiles() method") or [RetrieveFiles()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.RetrieveFiles "The RetrieveFiles() method") for this key will return an error.

``IN s _`key`_``:

A key returned by [StartTransfer()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileTransfer.StartTransfer "The StartTransfer() method")

### Signal Details

### The "TransferClosed" signal

TransferClosed (s key);

The TransferClosed signal is emitted to the caller of StartTransfer when the transfer is closed.

``s _`key`_``:

key returned by StartTransfer

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Flatpak.UpdateMonitor

- Methods


[Close](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak-UpdateMonitor.Close "The Close() method")  ();
[Update](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak-UpdateMonitor.Update "The Update() method") (IN  s     parent_window,
        IN  a{sv} options);

### Signals

[UpdateAvailable](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Flatpak-UpdateMonitor.UpdateAvailable "The "UpdateAvailable" signal") (a{sv} update_info);
[Progress](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Flatpak-UpdateMonitor.Progress "The "Progress" signal")        (a{sv} info);



- Description

- Method Details

### The Close() method

Close ();

Ends the update monitoring and cancels any ongoing installation.

### The Update() method

Update (IN  s     parent_window,
        IN  a{sv} options);

see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers") _`options`_: Vardict with optional further information

Asks to install an update of the calling app. During the installation, ["Progress"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Flatpak-UpdateMonitor.Progress "The "Progress" signal") signals will be emitted to communicate the status and progress.

Note that updates are only allowed if the new version has the same permissions (or less) than the currently installed version. If the new version requires a new permission then the operation will fail with the error org.freedesktop.DBus.Error.NotSupported and updates has to be done with the system tools.

Currently, no options are accepted.

``IN s _`parent_window`_``:

Identifier for the application window,

``IN a{sv} _`options`_``:

### Signal Details

### The "UpdateAvailable" signal

UpdateAvailable (a{sv} update_info);

Gets emitted when a new update is available. This is only sent once with the same information, but can be sent many times if new updates appear.

The following information may be included in the _`update_info`_ dictionary:

running-commit s

The commit of the currently running instance.

local-commit s

The commit that is currently installed. Restarting the application will cause this commit to be used.

remote-commit s

The commit that is available as an update from the remote. Updating the application will deploy this commit.

``a{sv} _`update_info`_``:

More information about the available update

### The "Progress" signal

Progress (a{sv} info);

Gets emitted to indicate progress of the installation. It's undefined exactly how often this is sent, but it will be emitted at least once at the end with non-zero status field. For each successful operation in the update we're also guaranteed to send one (and only one) signal with progress 100.

The following fields may be included in the info:

n_ops u

The number of operations that the update consists of.

op u

The position of the currently active operation.

progress u

The progress of the currently active operation, as a number between 0 and 100.

status u

The overall status of the update.

0: Running

1: Empty. No update to install

2: Done

3: Failed

error s

The error name, sent when status is Failed

error_message s

The error message, sent when status is Failed

``a{sv} _`info`_``:

More information about the update progress

---

- Name

org.freedesktop.portal.Flatpak — Flatpak portal

- Methods


[Spawn](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method")               (IN  ay    cwd_path,
                     IN  aay   argv,
                     IN  a{uh} fds,
                     IN  a{ss} envs,
                     IN  u     flags,
                     IN  a{sv} options,
                     OUT u     pid);
[SpawnSignal](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.SpawnSignal "The SpawnSignal() method")         (IN  u     pid,
                     IN  u     signal,
                     IN  b     to_process_group);
[CreateUpdateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.CreateUpdateMonitor "The CreateUpdateMonitor() method") (IN  a{sv} options,
                     OUT o     handle);

### Signals

[SpawnStarted](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Flatpak.SpawnStarted "The "SpawnStarted" signal") (u pid,
              u relpid);
[SpawnExited](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Flatpak.SpawnExited "The "SpawnExited" signal")  (u pid,
              u exit_status);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Flatpak.version "The "version" property")   readable   u
[supports](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Flatpak.supports "The "supports" property")  readable   u



- Description

The flatpak portal exposes some interactions with flatpak on the host to the sandbox. For example, it allows you to restart the applications or start a more sandboxed instance.

This portal is available on the D-Bus session bus under the bus name org.freedesktop.portal.Flatpak and the object path /org/freedesktop/portal/Flatpak.

This documentation describes version 6 of this interface.

- Method Details

### The Spawn() method

Spawn (IN  ay    cwd_path,
       IN  aay   argv,
       IN  a{uh} fds,
       IN  a{ss} envs,
       IN  u     flags,
       IN  a{sv} options,
       OUT u     pid);

This method lets you start a new instance of your application, optionally enabling a tighter sandbox.

The following flags values are supported:

1 (FLATPAK_SPAWN_FLAGS_CLEAR_ENV)

Clear the environment.

2 (FLATPAK_SPAWN_FLAGS_LATEST_VERSION)

Spawn the latest version of the app.

4 (FLATPAK_SPAWN_FLAGS_SANDBOX)

Spawn in a sandbox (equivalent of the sandbox option of flatpak run).

8 (FLATPAK_SPAWN_FLAGS_NO_NETWORK)

Spawn without network (equivalent of the unshare=network option of flatpak run).

16 (FLATPAK_SPAWN_FLAGS_WATCH_BUS)

Kill the sandbox when the caller disappears from the session bus.

32 (FLATPAK_SPAWN_FLAGS_EXPOSE_PIDS)

Expose the sandbox pids in the callers sandbox, only supported if using user namespaces for containers (not setuid), see the support property.

This was added in version 3 of this interface (available from flatpak 1.6.0 and later).

64 (FLATPAK_SPAWN_FLAGS_NOTIFY_START)

Emit a SpawnStarted signal once the sandboxed process has been fully started.

This was added in version 4 of this interface (available from flatpak 1.8.0 and later).

128 (FLATPAK_SPAWN_FLAGS_SHARE_PIDS)

Expose the sandbox process IDs in the caller's sandbox and the caller's process IDs in the new sandbox. Only supported if using user namespaces for containers (not setuid), see the support property.

This was added in version 5 of this interface (available from flatpak 1.10.0 and later).

256 (FLATPAK_SPAWN_FLAGS_EMPTY_APP)

Don't provide app files at `/app` in the new sandbox. Instead, `/app` will be an empty directory. This flag and the `app-fd` option are mutually exclusive.

As with the `app-fd` option, the caller's Flatpak app files and extensions will be mounted on `/run/parent/app`, with filenames like `/run/parent/app/bin/myapp`.

This was added in version 6 of this interface (available from flatpak 1.12.0 and later).

Unknown (unsupported) flags are an error and will cause Spawn() to fail.

Unknown (unsupported) options are ignored. The following options are supported:

sandbox-expose as

A list of filenames for files inside the sandbox that will be exposed to the new sandbox, for reading and writing. Note that absolute paths or subdirectories are not allowed.

The files must be in the `sandbox` subdirectory of the instance directory (i.e. `~/.var/app/$APP_ID/sandbox`).

sandbox-expose-ro as

A list of filenames for files inside the sandbox that will be exposed to the new sandbox, readonly. Note that absolute paths or subdirectories are not allowed.

The files must be in the `sandbox` subdirectory of the instance directory (i.e. `~/.var/app/$APP_ID/sandbox`).

sandbox-expose-fd ah

A list of file descriptor for files inside the sandbox that will be exposed to the new sandbox, for reading and writing (if the caller has write access). The file descriptors must be opened with O_PATH and O_NOFOLLOW and cannot be symlinks.

This was added in version 3 of this interface (available from flatpak 1.6.0 and later).

sandbox-expose-fd-ro ah

A list of file descriptor for files inside the sandbox that will be exposed to the new sandbox, readonly. The file descriptors must be opened with O_PATH and O_NOFOLLOW and cannot be symlinks.

This was added in version 3 of this interface (available from flatpak 1.6.0 and later).

sandbox-flags u

Flags affecting the created sandbox. The following flags values are supported:

1

Share the display access (X11, wayland) with the caller.

2

Share the sound access (pulseaudio) with the caller.

4

Share the gpu access with the caller.

8

Allow sandbox access to (filtered) session bus.

16

Allow sandbox access to accessibility bus.

This was added in version 3 of this interface (available from flatpak 1.6.0 and later).

unset-env as

A list of environment variables to unset (remove from the environment).

This was added in version 5 of this interface (available from flatpak 1.10.0 and later).

usr-fd h

A file descriptor for the directory that will be used as `/usr` in the new sandbox, instead of the `files` directory from the caller's Flatpak runtime. The new sandbox's `/etc` will be based on the `etc` subdirectory of the given directory, and compatibility symlinks in its root directory (`/lib`, `/bin` and so on) will point into the given directory. The caller's Flatpak runtime and its extensions will be mounted on `/run/parent/usr`, with filenames like `/run/parent/usr/bin/env`, and compatibility symlinks like `/run/parent/bin` → `usr/bin`.

The file descriptor must be opened with O_PATH and O_NOFOLLOW and cannot be a symlink.

This was added in version 6 of this interface (available from flatpak 1.12.0 and later).

app-fd h

A file descriptor for the directory that will be used as `/app` in the new sandbox, instead of the `files` directory from the caller's Flatpak app. The caller's Flatpak app files and extensions will be mounted on `/run/parent/app`, with filenames like `/run/parent/app/bin/myapp`.

This option and the `FLATPAK_SPAWN_FLAGS_EMPTY_APP` flag are mutually exclusive.

The file descriptor must be opened with O_PATH and O_NOFOLLOW and cannot be a symlink.

This was added in version 6 of this interface (available from flatpak 1.12.0 and later).

``IN ay _`cwd_path`_``:

the working directory for the new process

``IN aay _`argv`_``:

the argv for the new process, starting with the executable to launch

``IN a{uh} _`fds`_``:

an array of file descriptors to pass to the new process

``IN a{ss} _`envs`_``:

an array of variable/value pairs for the environment of the new process

``IN u _`flags`_``:

flags

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`pid`_``:

the PID of the new process

### The SpawnSignal() method

SpawnSignal (IN  u pid,
             IN  u signal,
             IN  b to_process_group);

This method lets you send a Unix signal to a process that was started with [Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method"). The _`pid`_ argument here should be the PID that is returned by the Spawn() call: it is not necessarily valid in the caller's PID namespace.

``IN u _`pid`_``:

the PID inside the container to signal

``IN u _`signal`_``:

the signal to send (see signal(7))

``IN b _`to_process_group`_``:

whether to send the signal to the process group

### The CreateUpdateMonitor() method

CreateUpdateMonitor (IN  a{sv} options,
                     OUT o     handle);

Creates an update monitor object that will emit signals when an update for the caller becomes available, and can be used to install it.

The handle will be of the form /org/freedesktop/portal/Flatpak/update_monitor/SENDER/TOKEN, where SENDER is the caller's unique name, with the initial ':' removed and all '.' replaced by '_', and TOKEN is a unique token that the caller can optionally provide with the 'handle_token' key in the options vardict.

Currently, no other options are accepted.

This was added in version 2 of this interface (available from flatpak 1.5.0 and later).

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Flatpak.UpdateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Flatpak-UpdateMonitor.top_of_page) object

### Signal Details

### The "SpawnStarted" signal

SpawnStarted (u pid,
              u relpid);

This is only non-zero if the expose PIDs flag (32) or the share PIDs flag (128) was passed to[Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method"), and it may still be zero if the process exits before its relative PID could be read.

Emitted when a process started by [Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method") has fully started. In other words, [Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method")returns once the sandbox has been started, and this signal is emitted once the process inside itself is started.

Only emitted by version 4 of this interface (available from flatpak 1.8.0 and later) and if the notify start flag (64) was passed to [Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method").

``u _`pid`_``:

the PID of the process that has been started

``u _`relpid`_``:

the PID of the process relative to the current namespace.

### The "SpawnExited" signal

SpawnExited (u pid,
             u exit_status);

Emitted when a process started by [Spawn()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Flatpak.Spawn "The Spawn() method") exits. Use g_spawn_check_exit_status(), or the macros such as WIFEXITED documented in waitpid(2), to interpret the _`exit_status`_.

This signal is not emitted for processes that were not launched directly by Spawn(), for example if a process launched by Spawn() runs foreground or background child processes.

``u _`pid`_``:

the PID of the process that has ended

``u _`exit_status`_``:

the wait status (see waitpid(2))

- Property Details

- The "version" property

version  readable   u

### The "supports" property

supports  readable   u

Flags marking what optional features are available. The following flags values are supported:

1 (FLATPAK_SPAWN_SUPPORT_FLAGS_EXPOSE_PIDS)

Supports the expose sandbox pids flag of Spawn. If the version of this interface is 5 or later, this also indicates that the share sandbox pids flag is available.

This was added in version 3 of this interface (available from flatpak 1.6.0 and later).

---

- Name

org.freedesktop.portal.GameMode — Portal for accessing GameMode

- Methods


[QueryStatus](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.QueryStatus "The QueryStatus() method")           (IN  i pid,
                       OUT i result);
[RegisterGame](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGame "The RegisterGame() method")          (IN  i pid,
                       OUT i result);
[UnregisterGame](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGame "The UnregisterGame() method")        (IN  i pid,
                       OUT i result);
[QueryStatusByPid](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.QueryStatusByPid "The QueryStatusByPid() method")      (IN  i target,
                       IN  i requester,
                       OUT i result);
[RegisterGameByPid](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGameByPid "The RegisterGameByPid() method")     (IN  i target,
                       IN  i requester,
                       OUT i result);
[UnregisterGameByPid](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGameByPid "The UnregisterGameByPid() method")   (IN  i target,
                       IN  i requester,
                       OUT i result);
[QueryStatusByPIDFd](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.QueryStatusByPIDFd "The QueryStatusByPIDFd() method")    (IN  h target,
                       IN  h requester,
                       OUT i result);
[RegisterGameByPIDFd](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGameByPIDFd "The RegisterGameByPIDFd() method")   (IN  h target,
                       IN  h requester,
                       OUT i result);
[UnregisterGameByPIDFd](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGameByPIDFd "The UnregisterGameByPIDFd() method") (IN  h target,
                       IN  h requester,
                       OUT i result);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-GameMode.version "The "version" property")  readable   u



- Description

Interface for accessing GameMode from within the sandbox. It is analogous to the com.feralinteractive.GameMode interface and will proxy request there, but with additional permission checking and pid mapping. The latter is necessary in the case that sandbox has pid namespace isolation enabled. See the man page for pid_namespaces(7) for more details, but briefly, it means that the sandbox has its own process id namespace which is separated from the one on the host. Thus there will be two separate process ids (pids) within two different namespaces that both identify same process. One id from the pid namespace inside the sandbox and one id from the host pid namespace. Since org.freedesktop.portal.GameMode expects pids from the host pid namespace but programs inside the sandbox can only know pids from the sandbox namespace, process ids need to be translated from the portal to the host namespace. The portal will do that transparently for all calls where this is necessary.

Note: [org.freedesktop.portal.GameMode](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-GameMode.top_of_page) will monitor active clients, i.e. games and other programs that have successfully called [RegisterGame()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGame "The RegisterGame() method"). In the event that a client terminates without a call to the [UnregisterGame()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGame "The UnregisterGame() method") method, GameMode will automatically un-register the client. This might happen with a (small) delay.

This documentation describes version 3 of this interface.

- Method Details

### The QueryStatus() method

QueryStatus (IN  i pid,
             OUT i result);

Query the GameMode status for a process. If the caller is running inside a sandbox with pid namespace isolation, the pid will be translated to the respective host pid. See the general introduction for details. Will return: 0 if GameMode is inactive, 1 if GameMode is active, 2 if GameMode is active and _`pid`_ is registered, -1 if the query failed inside GameMode

``IN i _`pid`_``:

Process id to query the GameMode status of

``OUT i _`result`_``:

The GameMode status for _`pid`_ '

### The RegisterGame() method

RegisterGame (IN  i pid,
              OUT i result);

Register a game with GameMode and thus request GameMode to be activated. If the caller is running inside a sandbox with pid namespace isolation, the pid will be translated to the respective host pid. See the general introduction for details. If the GameMode has already been requested for _`pid`_ before, this call will fail, i.e. result will be -1. Will return: 0 if the game with _`pid`_ was successfully registered, -1 if the request was rejected by GameMode

``IN i _`pid`_``:

Process id of the game to register

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

### The UnregisterGame() method

UnregisterGame (IN  i pid,
                OUT i result);

Un-register a game from GameMode; if the call is successful and there are no other games or clients registered, GameMode will be deactivated. If the caller is running inside a sandbox with pid namespace isolation, the pid will be translated to the respective host pid. Will return: 0 if the game with _`pid`_ was successfully un-registered, -1 if the request was rejected by GameMode

``IN i _`pid`_``:

Process id of the game to un-register

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

### The QueryStatusByPid() method

QueryStatusByPid (IN  i target,
                  IN  i requester,
                  OUT i result);

Query the GameMode status for a process.

Like [QueryStatus()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.QueryStatus "The QueryStatus() method") but _`requester`_ acting on behalf of _`target`_.

``IN i _`target`_``:

Process id to query the GameMode status of

``IN i _`requester`_``:

Process id of the process requesting the information

``OUT i _`result`_``:

The GameMode status for _`target`_

### The RegisterGameByPid() method

RegisterGameByPid (IN  i target,
                   IN  i requester,
                   OUT i result);

Register a game with GameMode.

Like [RegisterGame()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGame "The RegisterGame() method") but _`requester`_ acting on behalf of _`target`_.

``IN i _`target`_``:

Process id of the game to register

``IN i _`requester`_``:

Process id of the process requesting the registration

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

### The UnregisterGameByPid() method

UnregisterGameByPid (IN  i target,
                     IN  i requester,
                     OUT i result);

Un-register a game with GameMode.

Like [UnregisterGame()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGame "The UnregisterGame() method") but _`requester`_ acting on behalf of _`target`_.

``IN i _`target`_``:

Process id of the game to un-register

``IN i _`requester`_``:

Process id of the process requesting the un-registration

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

### The QueryStatusByPIDFd() method

QueryStatusByPIDFd (IN  h target,
                    IN  h requester,
                    OUT i result);

Query the GameMode status for a process.

Like [QueryStatusByPid()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.QueryStatusByPid "The QueryStatusByPid() method") but _`requester`_ and _`target`_ are pidfds representing the processes.

``IN h _`target`_``:

Pidfd to query the GameMode status of

``IN h _`requester`_``:

Pidfd of the process requesting the information

``OUT i _`result`_``:

The GameMode status for _`target`_

### The RegisterGameByPIDFd() method

RegisterGameByPIDFd (IN  h target,
                     IN  h requester,
                     OUT i result);

Register a game with GameMode.

Like [RegisterGameByPid()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.RegisterGameByPid "The RegisterGameByPid() method") but _`requester`_ and _`target`_ are pidfds representing the processes.

``IN h _`target`_``:

Pidfd of the game to register

``IN h _`requester`_``:

Pidfd of the process requesting the registration

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

### The UnregisterGameByPIDFd() method

UnregisterGameByPIDFd (IN  h target,
                       IN  h requester,
                       OUT i result);

Un-register a game with GameMode.

Like [UnregisterGameByPid()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-GameMode.UnregisterGameByPid "The UnregisterGameByPid() method") but _`requester`_ and _`target`_ are pidfds representing the processes.

``IN h _`target`_``:

Pidfd of the game to un-register

``IN h _`requester`_``:

Pidfd of the process requesting the un-registration

``OUT i _`result`_``:

Status of the request: 0 for success, -1 indicates an error

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Inhibit — Portal for inhibiting session transitions

- Methods


[Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Inhibit.Inhibit "The Inhibit() method")          (IN  s     window,
                  IN  u     flags,
                  IN  a{sv} options,
                  OUT o     handle);
[CreateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Inhibit.CreateMonitor "The CreateMonitor() method")    (IN  s     window,
                  IN  a{sv} options,
                  OUT o     handle);
[QueryEndResponse](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Inhibit.QueryEndResponse "The QueryEndResponse() method") (IN  o     session_handle);

### Signals

[StateChanged](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Inhibit.StateChanged "The "StateChanged" signal") (o     session_handle,
              a{sv} state);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Inhibit.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications inhibit the user session from ending, suspending, idling or getting switched away.

This documentation describes version 3 of this interface.

- Method Details

### The Inhibit() method

Inhibit (IN  s     window,
         IN  u     flags,
         IN  a{sv} options,
         OUT o     handle);

Inhibits a session status changes. To remove the inhibition, call [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Request.Close "The Close() method") on the returned handle.

The flags determine what changes are inhibited:

1: Logout

2: User Switch

4: Suspend

8: Idle

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

reason s

User-visible reason for the inhibition.

``IN s _`window`_``:

Identifier for the window

``IN u _`flags`_``:

Flags identifying what is inhibited

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The CreateMonitor() method

CreateMonitor (IN  s     window,
               IN  a{sv} options,
               OUT o     handle);

Creates a monitoring session. While this session is active, the caller will receive StateChanged signals with updates on the session state.

A successfully created session can at any time be closed using org.freedesktop.portal.Session::Close, or may at any time be closed by the portal implementation, which will be signalled via ["Closed"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Session.Closed "The "Closed" signal").

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

session_handle_token s

A string that will be used as the last element of the session handle. Must be a valid object path element. See the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) documentation for more information about the session handle.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

session_handle o

The session handle. An object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object representing the created session.

This method was added in version 2 of this interface.

``IN s _`window`_``:

the parent window

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The QueryEndResponse() method

QueryEndResponse (IN  o session_handle);

Acknowledges that the caller received the ["StateChanged"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Inhibit.StateChanged "The "StateChanged" signal") signal. This method should be called within one second or receiving a StateChanged signal with the 'Query End' state.

Since version 3.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

### Signal Details

### The "StateChanged" signal

StateChanged (o     session_handle,
              a{sv} state);

The StateChanged signal is sent to active monitoring sessions when the session state changes.

When the session state changes to 'Query End', clients with active monitoring sessions are expected to respond by calling [QueryEndResponse()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Inhibit.QueryEndResponse "The QueryEndResponse() method") within a second of receiving the StateChanged signal. They may call [Inhibit()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Inhibit.Inhibit "The Inhibit() method") first to inhibit logout, to prevent the session from proceeding to the Ending state.

The following information may get returned in the _`state`_ vardict:

screensaver-active b

Whether the screensaver is active.

session-state u

The state of the session. This member is new in version 3.

1: Running

2: Query End

3: Ending

``o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``a{sv} _`state`_``:

Vardict with information about the session state

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Location — Portal for obtaining information about the location

- Methods


[CreateSession](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Location.CreateSession "The CreateSession() method") (IN  a{sv} options,
               OUT o     handle);
[Start](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Location.Start "The Start() method")         (IN  o     session_handle,
               IN  s     parent_window,
               IN  a{sv} options,
               OUT o     handle);

### Signals

[LocationUpdated](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Location.LocationUpdated "The "LocationUpdated" signal") (o     session_handle,
                 a{sv} location);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Location.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications query basic information about the location.

This documentation describes version 1 of this interface.

- Method Details

### The CreateSession() method

CreateSession (IN  a{sv} options,
               OUT o     handle);

Create a location session. A successfully created session can at any time be closed using [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Session.Close "The Close() method"), or may at any time be closed by the portal implementation, which will be signalled via ["Closed"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Session.Closed "The "Closed" signal").

Supported keys in the _`options`_ vardict include:

session_handle_token s

A string that will be used as the last element of the session handle. Must be a valid object path element. See the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) documentation for more information about the session handle.

distance-threshold u

Distance threshold in meters. Default is 0.

time-threshold u

Time threshold in seconds. Default is 0.

accuracy u

Requested accuracy. Default is EXACT. Values: NONE 0, COUNTRY 1, CITY 2, NEIGHBORHOOD 3, STREET 4, EXACT 5

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the created [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

### The Start() method

Start (IN  o     session_handle,
       IN  s     parent_window,
       IN  a{sv} options,
       OUT o     handle);

Start the location session. An application can only attempt start a session once.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### Signal Details

### The "LocationUpdated" signal

LocationUpdated (o     session_handle,
                 a{sv} location);

The LocationUpdated signal is emitted when the location has changed, as well as when the initial location has been determined.

The following results may get returned via the _`location`_:

latitude d

The latitude, in degrees.

longitude d

The longitude, in degrees.

altitude d

The altitude, in meters.

accuracy d

The accuracy, in meters.

speed d

The speed, in meters per second.

heading d

The heading, in degrees, going clockwise. North 0, East 90, South 180, West 270.

timestamp (tt)

The timestamp, as seconds and microsections since the Unix epoch.

``o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``a{sv} _`location`_``:

Vardict with the current location data

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.MemoryMonitor — Memory monitoring portal

### Signals

[LowMemoryWarning](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-MemoryMonitor.LowMemoryWarning "The "LowMemoryWarning" signal") (y level);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-MemoryMonitor.version "The "version" property")  readable   u



- Description

The Memory Monitor interface provides information about low system memory to sandboxed applications. It is not a portal in the strict sense, since it does not involve user interaction. Applications are expected to use this interface indirectly, via a library API such as the GLib GMemoryMonitor interface.

This documentation describes version 1 of this interface.

### Signal Details

### The "LowMemoryWarning" signal

LowMemoryWarning (y level);

Signal emitted when a particular low memory situation happens with 0 being the lowest level of memory availability warning, and 255 being the highest. Those levels are defined and documented in [Low Memory Monitor's documentation](https://hadess.pages.freedesktop.org/low-memory-monitor/).

``y _`level`_``:

An integer representing the level of low memory warning.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.NetworkMonitor — Network monitoring portal

- Methods


[GetAvailable](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-NetworkMonitor.GetAvailable "The GetAvailable() method")    (OUT b     available);
[GetMetered](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-NetworkMonitor.GetMetered "The GetMetered() method")      (OUT b     metered);
[GetConnectivity](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-NetworkMonitor.GetConnectivity "The GetConnectivity() method") (OUT u     connectivity);
[GetStatus](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-NetworkMonitor.GetStatus "The GetStatus() method")       (OUT a{sv} status);
[CanReach](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-NetworkMonitor.CanReach "The CanReach() method")        (IN  s     hostname,
                 IN  u     port,
                 OUT b     reachable);

### Signals

[changed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-NetworkMonitor.changed "The "changed" signal") ();

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-NetworkMonitor.version "The "version" property")  readable   u



- Description

The NetworkMonitor interface provides network status information to sandboxed applications. It is not a portal in the strict sense, since it does not involve user interaction. Applications are expected to use this interface indirectly, via a library API such as the GLib GNetworkMonitor interface.

This documentation describes version 3 of this interface.

- Method Details

### The GetAvailable() method

GetAvailable (OUT b available);

Returns whether the network is considered available. That is, whether the system as a default route for at least one of IPv4 or IPv6.

This method was added in version 2 to replace the available property.

``OUT b _`available`_``:

whether the network is available

### The GetMetered() method

GetMetered (OUT b metered);

Returns whether the network is considered metered. That is, whether the system as traffic flowing through the default connection that is subject ot limitations by service providers.

This method was added in version 2 to replace the metered property.

``OUT b _`metered`_``:

whether the network is metered

### The GetConnectivity() method

GetConnectivity (OUT u connectivity);

Returns more detailed information about the host's network connectivity. The meaning of the value is:

1: Local only. The host is not configured with a route to the internet.

2: Limited connectivity. The host is connected to a network, but can't reach the full internet.

3: Captive portal. The host is behind a captive portal and cannot reach the full internet.

4: Full network. The host connected to a network, and can reach the full internet.

This method was added in version 2 to replace the connectivity property.

``OUT u _`connectivity`_``:

the level of connectivity

### The GetStatus() method

GetStatus (OUT a{sv} status);

Returns the three values all at once.

The following results get returned via _`status`_:

available b

Whether the network is available.

metered b

Whether the network is metered.

connectivity u

The level of connectivity.

This method was added in version 3 to avoid multiple round-trips.

``OUT a{sv} _`status`_``:

a dictionary with the current values

### The CanReach() method

CanReach (IN  s hostname,
          IN  u port,
          OUT b reachable);

Returns whether the given hostname is believed to be reachable. This method was added in version 3.

``IN s _`hostname`_``:

the hostname to reach

``IN u _`port`_``:

the port to reach

``OUT b _`reachable`_``:

Whether the hostname:port was reachable

### Signal Details

### The "changed" signal

changed ();

Emitted when the network configuration changes.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Notification — Portal for sending notifications

- Methods


[AddNotification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Notification.AddNotification "The AddNotification() method")    (IN  s     id,
                    IN  a{sv} notification);
[RemoveNotification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Notification.RemoveNotification "The RemoveNotification() method") (IN  s     id);

### Signals

[ActionInvoked](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Notification.ActionInvoked "The "ActionInvoked" signal") (s  id,
               s  action,
               av parameter);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Notification.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications send and withdraw notifications. It is not possible for the application to learn if the notification was actually presented to the user. Not a portal in the strict sense, since there is no user interaction.

Note that in contrast to most other portal requests, notifications are expected to outlast the running application. If a user clicks on a notification after the application has exited, it will get activated again.

Notifications can specify actions that can be activated by the user. Actions whose name starts with 'app.' are assumed to be exported and will be activated via the ActivateAction() method in the org.freedesktop.Application interface. Other actions are activated by sending the ["ActionInvoked"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Notification.ActionInvoked "The "ActionInvoked" signal") signal to the application.

This documentation describes version 1 of this interface.

- Method Details

### The AddNotification() method

AddNotification (IN  s     id,
                 IN  a{sv} notification);

Sends a notification.

The ID can be used to later withdraw the notification. If the application reuses the same ID without withdrawing, the notification is replaced by the new one.

The format of the serialized notification is a vardict, with the following supported keys, all of which are optional:

title s

User-visible string to display as the title.

body s

User-visible string to display as the body.

icon v

Serialized icon (see g_icon_serialize()).

The portal only accepts serialized GThemedIcon and GBytesIcons. Both of these have the form (sv). For themed icons, the string is "themed", and the value is an array of strings containing the icon names. For bytes icons, the string is "bytes", and the value is a bytestring containing the icon data in png, jpeg or svg form. For historical reasons, it is also possible to send a simple string for themed icons with a single icon name.

There may be further restrictions on the supported kinds of icons.

priority s

The priority for the notification. Supported values: low, normal, high, urgent.

default-action s

Name of an action that is exported by the application. This action will be activated when the user clicks on the notification.

default-action-target v

Target parameter to send along when activating the default action.

buttons aa{sv}

Array of serialized buttons to add to the notification.

The format for serialized buttons is a vardict with the following supported keys:

label s

User-visible label for the button. Mandatory.

action s

Name of an action that is exported by the application. The action will be activated when the user clicks on the button. Mandatory.

target v

Target parameter to send along when activating the action.

``IN s _`id`_``:

Application-provided ID for this notification

``IN a{sv} _`notification`_``:

Vardict with the serialized notification

### The RemoveNotification() method

RemoveNotification (IN  s id);

Withdraws a notification.

``IN s _`id`_``:

Application-provided ID for this notification

### Signal Details

### The "ActionInvoked" signal

ActionInvoked (s  id,
               s  action,
               av parameter);

for the action, if one was specified

Send to the application when a non-exported action is activated.

``s _`id`_``:

the application-provided ID for the notification

``s _`action`_``:

the name of the action

``av _`parameter`_``:

array which will contain the target parameter

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.OpenURI — Portal for opening URIs

- Methods


[OpenURI](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-OpenURI.OpenURI "The OpenURI() method")       (IN  s     parent_window,
               IN  s     uri,
               IN  a{sv} options,
               OUT o     handle);
[OpenFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-OpenURI.OpenFile "The OpenFile() method")      (IN  s     parent_window,
               IN  h     fd,
               IN  a{sv} options,
               OUT o     handle);
[OpenDirectory](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-OpenURI.OpenDirectory "The OpenDirectory() method") (IN  s     parent_window,
               IN  h     fd,
               IN  a{sv} options,
               OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-OpenURI.version "The "version" property")  readable   u



- Description

The OpenURI portal allows sandboxed applications to open URIs (e.g. a http: link to the applications homepage) under the control of the user.

This documentation describes version 4 of this interface.

- Method Details

### The OpenURI() method

OpenURI (IN  s     parent_window,
         IN  s     uri,
         IN  a{sv} options,
         OUT o     handle);

Asks to open a uri.

Note that file:// uris are explicitly not supported by this method. To request opening local files, use [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-OpenURI.OpenFile "The OpenFile() method").

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

writable b

Whether to allow the chosen application to write to the file.

This key only takes effect the uri points to a local file that is exported in the document portal, and the chosen application is sandboxed itself.

ask b

Whether to ask the user to choose an app. If this is not passed, or false, the portal may use a default or pick the last choice.

The ask option was introduced in version 3 of the interface.

activation_token s

A token that can be used to activate the chosen application.

The activation_token option was introduced in version 4 of the interface.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`uri`_``:

The uri to open

``IN a{sv} _`options`_``:

Vardict with optional further onformation

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The OpenFile() method

OpenFile (IN  s     parent_window,
          IN  h     fd,
          IN  a{sv} options,
          OUT o     handle);

Asks to open a local file.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

writable b

Whether to allow the chosen application to write to the file.

This key only takes effect the uri points to a local file that is exported in the document portal, and the chosen application is sandboxed itself.

ask b

Whether to ask the user to choose an app. If this is not passed, or false, the portal may use a default or pick the last choice.

The ask option was introduced in version 3 of the interface.

activation_token s

A token that can be used to activate the chosen application.

The activation_token option was introduced in version 4 of the interface.

The OpenFile method was introduced in version 2 of the OpenURI portal API.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN h _`fd`_``:

File descriptor for the file to open

``IN a{sv} _`options`_``:

Vardict with optional further onformation

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The OpenDirectory() method

OpenDirectory (IN  s     parent_window,
               IN  h     fd,
               IN  a{sv} options,
               OUT o     handle);

Asks to open the directory containing a local file in the file browser.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

activation_token s

A token that can be used to activate the chosen application.

The activation_token option was introduced in version 4 of the interface.

The OpenDirectory method was introduced in version 3 of the OpenURI portal API.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN h _`fd`_``:

File descriptor for a file

``IN a{sv} _`options`_``:

Vardict with optional further onformation

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.PowerProfileMonitor — Power Profile monitoring portal

- Properties

[power-saver-enabled](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-PowerProfileMonitor.power-saver-enabled "The "power-saver-enabled" property")  readable   b
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-PowerProfileMonitor.version "The "version" property")              readable   u



- Description

The Power Profile Monitor interface provides information about the user-selected system-wide power profile, to sandboxed applications. It is not a portal in the strict sense, since it does not involve user interaction. Applications are expected to use this interface indirectly, via a library API such as the GLib GPowerProfileMonitor interface.

This documentation describes version 1 of this interface.

- Property Details

### The "power-saver-enabled" property

power-saver-enabled  readable   b

Whether “Power Saver” mode is enabled on the system.

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Print — Portal for printing

- Methods


[Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.Print "The Print() method")        (IN  s     parent_window,
              IN  s     title,
              IN  h     fd,
              IN  a{sv} options,
              OUT o     handle);
[PreparePrint](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.PreparePrint "The PreparePrint() method") (IN  s     parent_window,
              IN  s     title,
              IN  a{sv} settings,
              IN  a{sv} page_setup,
              IN  a{sv} options,
              OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Print.version "The "version" property")  readable   u



- Description

The Print portal allows sandboxed applications to print.

Due to the way in which printing requires bi-directional communication, using this portal will often require applications to make multiple requests. First, use [PreparePrint()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.PreparePrint "The PreparePrint() method") to obtain print settings, use them to format your output, then use [Print()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.Print "The Print() method") to print the formatted document. It is expected that high-level toolkit APIs such as GtkPrintOperation will hide most of this complexity.

This documentation describes version 1 of this interface.

- Method Details

### The Print() method

Print (IN  s     parent_window,
       IN  s     title,
       IN  h     fd,
       IN  a{sv} options,
       OUT o     handle);

Asks to print a file.

The file must be passed in the form of a file descriptor open for reading. This ensures that sandboxed applications only print files that they have access to.

If a valid token is present in the _`options`_, then this call will print with the settings from the Print call that the token refers to. If no token is present, then a print dialog will be presented to the user.

Note that it is up to the portal implementation to determine how long it considers tokens valid.

Supported keys in the _`options`_ vardict:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

modal b

Whether to make the dialog modal. Defaults to yes.

token u

Token that was returned by a previous [PreparePrint()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.PreparePrint "The PreparePrint() method") call.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the print dialog

``IN h _`fd`_``:

File descriptor for reading the content to print

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The PreparePrint() method

PreparePrint (IN  s     parent_window,
              IN  s     title,
              IN  a{sv} settings,
              IN  a{sv} page_setup,
              IN  a{sv} options,
              OUT o     handle);

Presents a print dialog to the user and returns print settings and page setup.

Supported keys in the _`options`_ vardict:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

modal b

Whether to make the dialog modal. Defaults to yes.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

settings a{sv}

Print settings as set up by the user in the print dialog.

page-setup a{sv}

Page setup as set up by the user in the print dialog.

token u

Token that can be passed to a subsequent [Print()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.Print "The Print() method") call to bypass the print dialog.

The following keys are supported in the print settings vardict:

orientation s

One of landscape, portrait, reverse_landscape or reverse_portrait.

paper-format s

A paper name according to [PWG 5101.1-2002](ftp://ftp.pwg.org/pub/pwg/candidates/cs-pwgmsn10-20020226-5101.1.pdf).

paper-width s

Paper width, in millimeters.

paper-height s

Paper height, in millimeters.

n-copies s

The number of copies to print.

default-source s

The default paper source.

quality s

Print quality, one of normal, high, low or draft.

resolution s

The resolution, sets both resolution-x and resolution-y.

use-color s

Whether to use color, one of true or false.

duplex s

Duplex printing mode, one of simplex, horizontal or vertical.

collate s

Whether to collate copies, one of true or false.

reverse s

Whether to reverse the order of printed pages, one of true or false.

media-type s

A media type according to [PWG 5101.1-2002](ftp://ftp.pwg.org/pub/pwg/candidates/cs-pwgmsn10-20020226-5101.1.pdf).

dither s

The dithering to use, one of fine, none, coarse, lineart, grayscale or error-diffusion.

scale s

The scale in percent.

print-pages s

What pages to print, one of all, selection, current or ranges.

page-ranges s

A list of page ranges, formatted like this: 0-2,4,9-11.

Note that page ranges are 0-based, even if the are displayed as 1-based when presented to the user.

page-set s

What pages to print, one of all, even or odd.

finishings s

number-up s

The number of pages per sheet.

number-up-layout s

One of lrtb, lrbt, rltb, rlbt, tblr, tbrl, btlr, btrl.

output-bin s

resolution-x s

The horizontal resolution in dpi.

resolution-y s

The vertical resolution in dpi.

printer-lpi s

The resolution in lpi (lines per inch).

output-basename s

Basename to use for print-to-file.

output-file-format s

Format to use for print-to-file, one of PDF, PS, SVG.

output-uri s

The uri used for print-to-file.

The following keys are supported in the page setup vardict:

PPDName s

The PPD name.

Name s

The name of the page setup.

DisplayName s

User-visible name for the page setup.

Width d

Paper width in millimeters.

Height d

Paper height in millimeters.

MarginTop d

Top margin in millimeters.

MarginBottom d

Bottom margin in millimeters.

MarginLeft d

Left margin in millimeters.

MarginRight d

Right margin in millimeters.

Orientation s

Orientation, one of portrait, landscape, reverse-portrait or reverse-landscape.

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the print dialog

``IN a{sv} _`settings`_``:

Serialized print settings

``IN a{sv} _`page_setup`_``:

Serialized page setup

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.ProxyResolver — Proxy information

- Methods


[Lookup](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ProxyResolver.Lookup "The Lookup() method") (IN  s  uri,
        OUT as proxies);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-ProxyResolver.version "The "version" property")  readable   u



- Description

The ProxyResolver interface provides network proxy information to sandboxed applications. It is not a portal in the strict sense, since it does not involve user interaction. Applications are expected to use this interface indirectly, via a library API such as the GLib GProxyResolver interface.

This documentation describes version 1 of this interface.

- Method Details

### The Lookup() method

Lookup (IN  s  uri,
        OUT as proxies);

Looks up which proxy to use to connect to _`uri`_. The returned proxy uri are of the form 'protocol://[user[:password]_`host`_:port'. The protocol can be http, rtsp, socks or another proxying protocol. 'direct://' is used when no proxy is needed.

``IN s _`uri`_``:

Destination to connect to

``OUT as _`proxies`_``:

List of proxy uris

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Realtime — Portal for setting threads to realtime

- Methods


[MakeThreadRealtimeWithPID](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Realtime.MakeThreadRealtimeWithPID "The MakeThreadRealtimeWithPID() method")     (IN  t process,
                               IN  t thread,
                               IN  u priority);
[MakeThreadHighPriorityWithPID](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Realtime.MakeThreadHighPriorityWithPID "The MakeThreadHighPriorityWithPID() method") (IN  t process,
                               IN  t thread,
                               IN  i priority);

- Properties

[MaxRealtimePriority](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Realtime.MaxRealtimePriority "The "MaxRealtimePriority" property")  readable   x
[MinNiceLevel](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Realtime.MinNiceLevel "The "MinNiceLevel" property")         readable   i
[RTTimeUSecMax](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Realtime.RTTimeUSecMax "The "RTTimeUSecMax" property")        readable   i
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Realtime.version "The "version" property")              readable   u



- Description

Interface for setting a thread to realtime from within the sandbox. It is analogous to the org.freedesktop.RealtimeKit1 interface and will proxy requests there but with pid mapping. The latter is necessary in the case that sandbox has pid namespace isolation enabled.

Note that this proxy does not bypass any limitations that RealtimeKit imposes on processes which are documented here: https://git.0pointer.net/rtkit.git/tree/README

This documentation describes version 1 of this interface.

- Method Details

### The MakeThreadRealtimeWithPID() method

MakeThreadRealtimeWithPID (IN  t process,
                           IN  t thread,
                           IN  u priority);

``IN t _`process`_``:

Process id

``IN t _`thread`_``:

Thread id

``IN u _`priority`_``:

Priority

### The MakeThreadHighPriorityWithPID() method

MakeThreadHighPriorityWithPID (IN  t process,
                               IN  t thread,
                               IN  i priority);

``IN t _`process`_``:

Process id

``IN t _`thread`_``:

Thread id

``IN i _`priority`_``:

Priority

- Property Details

### The "MaxRealtimePriority" property

MaxRealtimePriority  readable   x

### The "MinNiceLevel" property

MinNiceLevel  readable   i

### The "RTTimeUSecMax" property

RTTimeUSecMax  readable   i

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.RemoteDesktop — Remote desktop portal

- Methods


[CreateSession](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.CreateSession "The CreateSession() method")               (IN  a{sv} options,
                             OUT o     handle);
[SelectDevices](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.SelectDevices "The SelectDevices() method")               (IN  o     session_handle,
                             IN  a{sv} options,
                             OUT o     handle);
[Start](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.Start "The Start() method")                       (IN  o     session_handle,
                             IN  s     parent_window,
                             IN  a{sv} options,
                             OUT o     handle);
[NotifyPointerMotion](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyPointerMotion "The NotifyPointerMotion() method")         (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  d     dx,
                             IN  d     dy);
[NotifyPointerMotionAbsolute](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyPointerMotionAbsolute "The NotifyPointerMotionAbsolute() method") (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  d     x,
                             IN  d     y);
[NotifyPointerButton](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyPointerButton "The NotifyPointerButton() method")         (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     button,
                             IN  u     state);
[NotifyPointerAxis](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyPointerAxis "The NotifyPointerAxis() method")           (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  d     dx,
                             IN  d     dy);
[NotifyPointerAxisDiscrete](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyPointerAxisDiscrete "The NotifyPointerAxisDiscrete() method")   (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     axis,
                             IN  i     steps);
[NotifyKeyboardKeycode](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyKeyboardKeycode "The NotifyKeyboardKeycode() method")       (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     keycode,
                             IN  u     state);
[NotifyKeyboardKeysym](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyKeyboardKeysym "The NotifyKeyboardKeysym() method")        (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     keysym,
                             IN  u     state);
[NotifyTouchDown](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyTouchDown "The NotifyTouchDown() method")             (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  u     slot,
                             IN  d     x,
                             IN  d     y);
[NotifyTouchMotion](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyTouchMotion "The NotifyTouchMotion() method")           (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  u     slot,
                             IN  d     x,
                             IN  d     y);
[NotifyTouchUp](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-RemoteDesktop.NotifyTouchUp "The NotifyTouchUp() method")               (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     slot);

- Properties

[AvailableDeviceTypes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-RemoteDesktop.AvailableDeviceTypes "The "AvailableDeviceTypes" property")  readable   u
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-RemoteDesktop.version "The "version" property")               readable   u



- Description

The Remote desktop portal allows to create remote desktop sessions.

This documentation describes version 1 of this interface.

- Method Details

### The CreateSession() method

CreateSession (IN  a{sv} options,
               OUT o     handle);

Create a remote desktop session.

A remote desktop session is used to allow remote controlling a desktop session. It can also be used together with a screen cast session (see [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)), but may only be started and stopped with this interface.

To also get a screen content, call the [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method") with the[org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object created with this method.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

session_handle_token s

A string that will be used as the last element of the session handle. Must be a valid object path element. See the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) documentation for more information about the session handle.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

session_handle o

The session handle. An object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object representing the created session.

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The SelectDevices() method

SelectDevices (IN  o     session_handle,
               IN  a{sv} options,
               OUT o     handle);

Select input devices to remote control.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

types u

Bitmask of what device types to request remote controlling of. Default is all.

For available source types, see the AvailableDeviceTypes property.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The Start() method

Start (IN  o     session_handle,
       IN  s     parent_window,
       IN  a{sv} options,
       OUT o     handle);

Start the remote desktop session. This will typically result in the portal presenting a dialog letting the user select what to share, including devices and optionally screen content if screen cast sources was selected.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

devices u

A bitmask of the devices selected by the user.

If a screen cast source was selected, the results of the[org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page).Start response signal may be included.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The NotifyPointerMotion() method

NotifyPointerMotion (IN  o     session_handle,
                     IN  a{sv} options,
                     IN  d     dx,
                     IN  d     dy);

Notify about a new relative pointer motion event. The (dx, dy) vector represents the new pointer position in the streams logical coordinate space.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN d _`dx`_``:

Relative movement on the x axis

``IN d _`dy`_``:

Relative movement on the y axis

### The NotifyPointerMotionAbsolute() method

NotifyPointerMotionAbsolute (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  d     x,
                             IN  d     y);

Notify about a new absolute pointer motion event. The (x, y) position represents the new pointer position in the streams logical coordinate space (see the logical_size stream property in [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN d _`x`_``:

Pointer motion x coordinate

``IN d _`y`_``:

Pointer motion y coordinate

### The NotifyPointerButton() method

NotifyPointerButton (IN  o     session_handle,
                     IN  a{sv} options,
                     IN  i     button,
                     IN  u     state);

The pointer button is encoded according to Linux Evdev button codes.

May only be called if POINTER access was provided after starting the session.

Available button states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`button`_``:

The pointer button was pressed or released

``IN u _`state`_``:

The new state of the button

### The NotifyPointerAxis() method

NotifyPointerAxis (IN  o     session_handle,
                   IN  a{sv} options,
                   IN  d     dx,
                   IN  d     dy);

The axis movement from a 'smooth scroll' device, such as a touchpad. When applicable, the size of the motion delta should be equivalent to the motion vector of a pointer motion done using the same advice.

May only be called if POINTER access was provided after starting the session.

Supported keys in the _`options`_ vardict include:

finish b

If set to true, this is the last axis event in a series, for example as a result of the fingers being lifted from a touchpad after a two-finger scroll. Default is false.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN d _`dx`_``:

Relative axis movement on the x axis

``IN d _`dy`_``:

Relative axis movement on the y axis

### The NotifyPointerAxisDiscrete() method

NotifyPointerAxisDiscrete (IN  o     session_handle,
                           IN  a{sv} options,
                           IN  u     axis,
                           IN  i     steps);

May only be called if POINTER access was provided after starting the session.

Available axes:

0: Vertical scroll

1: Horizontal scroll

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`axis`_``:

The axis that was scrolled

``IN i _`steps`_``:

The number of steps scrolled

### The NotifyKeyboardKeycode() method

NotifyKeyboardKeycode (IN  o     session_handle,
                       IN  a{sv} options,
                       IN  i     keycode,
                       IN  u     state);

May only be called if KEYBOARD access was provided after starting the session.

Available keyboard keysym states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`keycode`_``:

Keyboard code that was pressed or released

``IN u _`state`_``:

New state of keyboard keysym

### The NotifyKeyboardKeysym() method

NotifyKeyboardKeysym (IN  o     session_handle,
                      IN  a{sv} options,
                      IN  i     keysym,
                      IN  u     state);

May only be called if KEYBOARD access was provided after starting the session.

Available keyboard keysym states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`keysym`_``:

Keyboard symbol that was pressed or released

``IN u _`state`_``:

New state of keyboard keysym

### The NotifyTouchDown() method

NotifyTouchDown (IN  o     session_handle,
                 IN  a{sv} options,
                 IN  u     stream,
                 IN  u     slot,
                 IN  d     x,
                 IN  d     y);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch down event. The (x, y) position represents the new touch point position in the streams logical coordinate space (see the logical_size stream property in[org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN u _`slot`_``:

Touch slot where touch point appeared

``IN d _`x`_``:

Touch down x coordinate

``IN d _`y`_``:

Touch down y coordinate

### The NotifyTouchMotion() method

NotifyTouchMotion (IN  o     session_handle,
                   IN  a{sv} options,
                   IN  u     stream,
                   IN  u     slot,
                   IN  d     x,
                   IN  d     y);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch motion event. The (x, y) position represents where the touch point position in the streams logical coordinate space moved (see the logical_size stream property in [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN u _`slot`_``:

Touch slot where touch point appeared

``IN d _`x`_``:

Touch motion x coordinate

``IN d _`y`_``:

Touch motion y coordinate

### The NotifyTouchUp() method

NotifyTouchUp (IN  o     session_handle,
               IN  a{sv} options,
               IN  u     slot);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch up event.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`slot`_``:

Touch slot where touch point appeared

- Property Details

### The "AvailableDeviceTypes" property

AvailableDeviceTypes  readable   u

A bitmask of available source types. Currently defined types are:

1: KEYBOARD

2: POINTER

4: TOUCHSCREEN

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Request — Shared request interface

- Methods


[Close](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Request.Close "The Close() method") ();

### Signals

[Response](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") (u     response,
          a{sv} results);



- Description

The Request interface is shared by all portal interfaces. When a portal method is called, the reply includes a handle (i.e. object path) for a Request object, which will stay alive for the duration of the user interaction related to the method call.

The portal indicates that a portal request interaction is over by emitting the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal")signal on the Request object.

The application can abort the interaction calling [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Request.Close "The Close() method") on the Request object.

Since version 0.9 of xdg-desktop-portal, the handle will be of the form /org/freedesktop/portal/desktop/request/SENDER/TOKEN, where SENDER is the callers unique name, with the initial ':' removed and all '.' replaced by '_', and TOKEN is a unique token that the caller provided with the handle_token key in the options vardict.

This change was made to let applications subscribe to the Response signal before making the initial portal call, thereby avoiding a race condition. It is recommended that the caller should verify that the returned handle is what it expected, and update its signal subscription if it isn't. This ensures that applications will work with both old and new versions of xdg-desktop-portal.

The token that the caller provides should be unique and not guessable. To avoid clashes with calls made from unrelated libraries, it is a good idea to use a per-library prefix combined with a random number.

- Method Details

### The Close() method

Close ();

Closes the portal request to which this object refers and ends all related user interaction (dialogs, etc). A Response signal will not be emitted in this case.

### Signal Details

### The "Response" signal

Response (u     response,
          a{sv} results);

Emitted when the user interaction for a portal request is over.

The _`response`_ indicates how the user interaction ended:

0: Success, the request is carried out

1: The user cancelled the interaction

2: The user interaction was ended in some other way

``u _`response`_``:

Numeric response

``a{sv} _`results`_``:

Vardict with results. The keys and values in the vardict depend on the request.

---

- Name

org.freedesktop.portal.ScreenCast — Screen cast portal

- Methods


[CreateSession](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.CreateSession "The CreateSession() method")      (IN  a{sv} options,
                    OUT o     handle);
[SelectSources](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method")      (IN  o     session_handle,
                    IN  a{sv} options,
                    OUT o     handle);
[Start](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.Start "The Start() method")              (IN  o     session_handle,
                    IN  s     parent_window,
                    IN  a{sv} options,
                    OUT o     handle);
[OpenPipeWireRemote](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.OpenPipeWireRemote "The OpenPipeWireRemote() method") (IN  o     session_handle,
                    IN  a{sv} options,
                    OUT h     fd);

- Properties

[AvailableSourceTypes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-ScreenCast.AvailableSourceTypes "The "AvailableSourceTypes" property")  readable   u
[AvailableCursorModes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-ScreenCast.AvailableCursorModes "The "AvailableCursorModes" property")  readable   u
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-ScreenCast.version "The "version" property")               readable   u



- Description

The Screen cast portal allows to create screen cast sessions.

This documentation describes version 4 of this interface.

- Method Details

### The CreateSession() method

CreateSession (IN  a{sv} options,
               OUT o     handle);

Create a screen cast session. A successfully created session can at any time be closed using [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Session.Close "The Close() method"), or may at any time be closed by the portal implementation, which will be signalled via ["Closed"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Session.Closed "The "Closed" signal").

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

session_handle_token s

A string that will be used as the last element of the session handle. Must be a valid object path element. See the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) documentation for more information about the session handle.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

session_handle o

The session handle. An object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object representing the created session.

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The SelectSources() method

SelectSources (IN  o     session_handle,
               IN  a{sv} options,
               OUT o     handle);

Configure what the screen cast session should record. This method must be called before starting the session.

Passing invalid input to this method will cause the session to be closed. An application may only attempt to select sources once per session.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

types u

Bitmask of what types of content to record. Default is MONITOR.

multiple b

Whether to allow selecting multiple sources. Default is no.

cursor_mode u

Determines how the cursor will be drawn in the screen cast stream. It must be one of the cursor modes advertised in [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page).AvailableCursorModes. Setting a cursor mode not advertised will cause the screen cast session to be closed. The default cursor mode is 'Hidden'.

This option was added in version 2 of this interface.

restore_token s

The token to restore a previous session.

If the stored session cannot be restored, this value is ignored and the user will be prompted normally. This may happen when, for example, the session contains a monitor or a window that is not available anymore, or when the stored permissions are withdrawn.

The restore token is invalidated after using it once. To restore the same session again, use the new restore token sent in response to starting this session.

This option was added in version 4 of this interface.

persist_mode u

How this session should persist. Default is 0. Accepted values are:

0: Do not persist (default)

1: Permissions persist as long as the application is running

2: Permissions persist until explicitly revoked

Remote desktop screen cast sessions cannot persist. The only allowed persist_mode for remote desktop sessions is 0.

If the permission for the session to persist is granted, a restore token will be returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal of the [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page).Start method.

This option was added in version 4 of this interface.

For available source types, see the AvailableSourceTypes property.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The Start() method

Start (IN  o     session_handle,
       IN  s     parent_window,
       IN  a{sv} options,
       OUT o     handle);

Start the screen cast session. This will typically result the portal presenting a dialog letting the user do the selection set up by [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method"). An application can only attempt start a session once.

A screen cast session may only be started after having selected sources using [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method").

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

streams a(ua{sv})

An array of PipeWire streams. Each stream consists of a PipeWire node ID (the first element in the tuple, and a Vardict of properties.

The array will contain a single stream if 'multiple' (see [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method")) was set to 'false', or at least one stream if 'multiple' was set to 'true' as part of the [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method")method.

restore_token s

The restore token. This token is a single use token that can later be used to restore a session. See [SelectSources()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-ScreenCast.SelectSources "The SelectSources() method") for details.

This response option was added in version 4 of this interface.

Stream properties include:

id s

Opaque identifier. Will be unique for this stream and local to this session. Will persist with future sessions, if they are restored using a restore token. This property was added in version 4 of this interface. Optional.

position (ii)

A tuple consisting of the position (x, y) in the compositor coordinate space. Note that the position may not be equivalent to a position in a pixel coordinate space. Only available for monitor streams. Optional.

size (ii)

A tuple consisting of (width, height). The size represents the size of the stream as it is displayed in the compositor coordinate space. Note that this size may not be equivalent to a size in a pixel coordinate space. The size may differ from the size of the stream. Optional.

source_type u

The type of the content which is being screen casted. For available source types, see the AvailableSourceTypes property. This property was added in version 3 of this interface.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The OpenPipeWireRemote() method

OpenPipeWireRemote (IN  o     session_handle,
                    IN  a{sv} options,
                    OUT h     fd);

Open a file descriptor to the PipeWire remote where the screen cast streams are available. The file descriptor should be used to create a `pw_core` object, by using`pw_context_connect_fd`. Only the screen cast stream nodes will be available from this PipeWire node.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT h _`fd`_``:

File descriptor of an open PipeWire remote.

- Property Details

### The "AvailableSourceTypes" property

AvailableSourceTypes  readable   u

A bitmask of available source types. Currently defined types are:

1: MONITOR

2: WINDOW

4: VIRTUAL

### The "AvailableCursorModes" property

AvailableCursorModes  readable   u

A bitmask of available cursor modes.

Available cursor mode values:

1: Hidden. The cursor is not part of the screen cast stream.

2: Embedded: The cursor is embedded as part of the stream buffers.

4: Metadata: The cursor is not part of the screen cast stream, but sent as PipeWire stream metadata.

This property was added in version 2 of this interface.

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Screenshot — Portal for taking screenshots

- Methods


[Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Screenshot.Screenshot "The Screenshot() method") (IN  s     parent_window,
            IN  a{sv} options,
            OUT o     handle);
[PickColor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Screenshot.PickColor "The PickColor() method")  (IN  s     parent_window,
            IN  a{sv} options,
            OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Screenshot.version "The "version" property")  readable   u



- Description

This simple portal lets sandboxed applications request a screenshot.

The screenshot will be made accessible to the application via the document portal, and the returned URI will point into the document portal fuse filesystem in /run/user/$UID/doc/.

This documentation describes version 2 of this interface.

- Method Details

### The Screenshot() method

Screenshot (IN  s     parent_window,
            IN  a{sv} options,
            OUT o     handle);

Supported keys in the _`options`_ vardict include: _`parent_window`_: Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers") _`options`_: Vardict with optional further information_`handle`_: Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

Takes a screenshot.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

modal b

Whether the dialog should be modal. Default is yes.

interactive b

Hint shether the dialog should offer customization before taking a screenshot. Default is no. Since version 2.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

uri s

String containing the uri of the screenshot.

``IN s _`parent_window`_``:

``IN a{sv} _`options`_``:

``OUT o _`handle`_``:

### The PickColor() method

PickColor (IN  s     parent_window,
           IN  a{sv} options,
           OUT o     handle);

Obtains the color of a single pixel.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

color (ddd)

The color, rgb values in the range [0,1].

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Secret — Portal for retrieving application secret

- Methods


[RetrieveSecret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Secret.RetrieveSecret "The RetrieveSecret() method") (IN  h     fd,
                IN  a{sv} options,
                OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Secret.version "The "version" property")  readable   u



- Description

The Secret portal allows sandboxed applications to retrieve a per-application secret. The secret can then be used for encrypting confidential data inside the sandbox.

This documentation describes version 1 of this interface.

- Method Details

### The RetrieveSecret() method

RetrieveSecret (IN  h     fd,
                IN  a{sv} options,
                OUT o     handle);

Retrieves a master secret for a sandboxed application.

The master secret is unique per application and does not change as long as the application is installed (once it has been created). In a typical backend implementation, it is stored in the user's keyring, under the application ID as a key.

While the master secret can be used for encrypting any confidential data in the sandbox, the format is opaque to the application. In particular, the length of the secret might not be sufficient for the use with certain encryption algorithm. In that case, the application is supposed to expand it using a KDF algorithm.

The portal may return an additional identifier associated with the secret in the results vardict of ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal. In the next call of this method, the application shall indicate it through a token element in _`options`_.

Supported keys in the _`options`_ vardict include:

token s

An opaque string returned by a previous [RetrieveSecret()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Secret.RetrieveSecret "The RetrieveSecret() method") call.

``IN h _`fd`_``:

Writable file descriptor for transporting the secret

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Session — Shared session interface

- Methods


[Close](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Session.Close "The Close() method") ();

### Signals

[Closed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Session.Closed "The "Closed" signal") (a{sv} details);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Session.version "The "version" property")  readable   u



- Description

The Session interface is shared by all portal interfaces that involve long lived sessions. When a method that creates a session is called, if successful, the reply will include a session handle (i.e. object path) for a Session object, which will stay alive for the duration of the session.

The duration of the session is defined by the interface that creates it. For convenience, the interface contains a method [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Session.Close "The Close() method"), and a signal ["Closed"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Session.Closed "The "Closed" signal"). Whether it is allowed to directly call [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Session.Close "The Close() method") depends on the interface.

The handle of a session will be of the form /org/freedesktop/portal/desktop/session/SENDER/TOKEN, where SENDER is the callers unique name, with the initial ':' removed and all '.' replaced by '_', and TOKEN is a unique token that the caller provided with the session_handle_token key in the options vardict of the method creating the session.

The token that the caller provides should be unique and not guessable. To avoid clashes with calls made from unrelated libraries, it is a good idea to use a per-library prefix combined with a random number.

A client who started a session vanishing from the D-Bus is equivalent to closing all active sessions made by said client.

- Method Details

### The Close() method

Close ();

Closes the portal session to which this object refers and ends all related user interaction (dialogs, etc).

### Signal Details

### The "Closed" signal

Closed (a{sv} details);

Emitted when a session is closed.

The content of _`details`_ is specified by the interface creating the session.

``a{sv} _`details`_``:

A key value Vardict with details about the closed session.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Settings — Settings interface

- Methods


[ReadAll](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Settings.ReadAll "The ReadAll() method") (IN  as        namespaces,
         OUT a{sa{sv}} value);
[Read](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Settings.Read "The Read() method")    (IN  s         namespace,
         IN  s         key,
         OUT v         value);

### Signals

[SettingChanged](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Settings.SettingChanged "The "SettingChanged" signal") (s namespace,
                s key,
                v value);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Settings.version "The "version" property")  readable   u



- Description

This interface provides read-only access to a small number of host settings required for toolkits similar to XSettings. It is not for general purpose settings.

Currently the interface provides the following keys:

org.freedesktop.appearance color-scheme u

Indicates the system's preferred color scheme. Supported values are:

0: No preference

1: Prefer dark appearance

2: Prefer light appearance

Unknown values should be treated as 0 (no preference).

Implementations can provide other keys; they are entirely implementation details that are undocumented. If you are a toolkit and want to use this please open an issue.

This documentation describes version 1 of this interface.

- Method Details

### The ReadAll() method

ReadAll (IN  as        namespaces,
         OUT a{sa{sv}} value);

If _`namespaces`_ is an empty array or contains an empty string it matches all. Globbing is supported but only for trailing sections, e.g. "org.example.*".

``IN as _`namespaces`_``:

List of namespaces to filter results by, supports simple globbing explained below.

``OUT a{sa{sv}} _`value`_``:

Dictionary of namespaces to its keys and values.

### The Read() method

Read (IN  s namespace,
      IN  s key,
      OUT v value);

Reads a single value. Returns an error on any unknown namespace or key.

``IN s _`namespace`_``:

Namespace to look up _`key`_ in.

``IN s _`key`_``:

The key to get.

``OUT v _`value`_``:

The value _`key`_ is set to.

### Signal Details

### The "SettingChanged" signal

SettingChanged (s namespace,
                s key,
                v value);

Emitted when a setting changes.

``s _`namespace`_``:

Namespace of changed setting.

``s _`key`_``:

The key of changed setting.

``v _`value`_``:

The new value.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Trash — Portal for trashing files

- Methods


[TrashFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Trash.TrashFile "The TrashFile() method") (IN  h fd,
           OUT u result);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Trash.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications send files to the trashcan.

This documentation describes version 1 of this interface.

- Method Details

### The TrashFile() method

TrashFile (IN  h fd,
           OUT u result);

Sends a file to the trashcan. Applications are allowed to trash a file if they can open it in r/w mode.

``IN h _`fd`_``:

file descriptor for the file to trash

``OUT u _`result`_``:

the result. 0 if trashing failed, 1 if trashing succeeded, other values may be returned in the future

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.portal.Wallpaper — Portal for setting the desktop's Wallpaper

- Methods


[SetWallpaperURI](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Wallpaper.SetWallpaperURI "The SetWallpaperURI() method")  (IN  s     parent_window,
                  IN  s     uri,
                  IN  a{sv} options,
                  OUT o     handle);
[SetWallpaperFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Wallpaper.SetWallpaperFile "The SetWallpaperFile() method") (IN  s     parent_window,
                  IN  h     fd,
                  IN  a{sv} options,
                  OUT o     handle);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-portal-Wallpaper.version "The "version" property")  readable   u



- Description

This simple interface lets sandboxed applications set the user's desktop background picture.

This documentation describes version 1 of this interface.

- Method Details

### The SetWallpaperURI() method

SetWallpaperURI (IN  s     parent_window,
                 IN  s     uri,
                 IN  a{sv} options,
                 OUT o     handle);

Asks to set a given picture as the desktop background picture.

Note that file: uris are explicitly not supported here. To use a local image file as background, use the [SetWallpaperFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Wallpaper.SetWallpaperFile "The SetWallpaperFile() method") method.

Supported keys in the _`options`_ vardict include:

show-preview b

whether to show a preview of the picture. Note that the portal may decide to show a preview even if this option is not set

set-on s

where to set the wallpaper. Possible values are 'background', 'lockscreen' or 'both'

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`uri`_``:

The picture file uri

``IN a{sv} _`options`_``:

Options that influence the behavior of the portal

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

### The SetWallpaperFile() method

SetWallpaperFile (IN  s     parent_window,
                  IN  h     fd,
                  IN  a{sv} options,
                  OUT o     handle);

Asks to set a given local file as the desktop background picture.

Supported keys in the _`options`_ vardict include:

show-preview b

whether to show a preview of the picture. Note that the portal may decide to show a preview even if this option is not set

set-on s

where to set the wallpaper. Possible values are 'background', 'lockscreen' or 'both'

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN h _`fd`_``:

File descriptor for the file to open

``IN a{sv} _`options`_``:

Options that influence the behavior of the portal

``OUT o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

- Property Details

- The "version" property

version  readable   u

---

The backend interfaces are used by the portal frontend to carry out portal requests. They are provided by a separate process (or processes), and are not accessible to sandboxed applications.

The separation or the portal infrastructure into frontend and backend is a clean way to provide suitable user interfaces that fit into different desktop environments, while sharing the portal frontend.

The portal backends are focused on providing user interfaces and accessing session- or host-specific APIs and resources. Details of interacting with the containment infrastructure such as checking access, registering files in the document portal, etc., are handled by the portal frontend.

## II. Portal Backend API Reference
[II. Portal Backend API Reference](https://docs.flatpak.org/en/latest/portal-api-reference.html#idm45771421251808)

- [org.freedesktop.impl.portal.Access](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Access) — Interface for presenting an access dialog

- [org.freedesktop.impl.portal.Account](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Account) — Backend for the portal for obtaining user information

- [org.freedesktop.impl.portal.AppChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.AppChooser) — Interface for choosing an application

- [org.freedesktop.impl.portal.Background](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Background) — Background portal backend interface

- [org.freedesktop.impl.portal.Email](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Email) — Email portal backend interface

- [org.freedesktop.impl.portal.FileChooser](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.FileChooser) — File chooser portal backend interface

- [org.freedesktop.impl.portal.Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Inhibit) — Inhibit portal backend interface

- [org.freedesktop.impl.portal.Lockdown](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Lockdown) — Lockdown backend interface

- [org.freedesktop.impl.portal.Notification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Notification) — Notification portal backend interface

- [org.freedesktop.impl.portal.PermissionStore](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.PermissionStore) — Database to store permissions

- [org.freedesktop.impl.portal.Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Print) — Print portal backend interface

- [org.freedesktop.impl.portal.RemoteDesktop](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.RemoteDesktop) — Remote desktop portal backend interface

- [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Request) — Shared request interface

- [org.freedesktop.impl.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.ScreenCast) — Screen cast portal backend interface

- [org.freedesktop.impl.portal.Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Screenshot) — Screenshot portal backend interface

- [org.freedesktop.impl.portal.Secret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Secret) — Secret portal backend interface

- [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Session) — Shared session interface

- [org.freedesktop.impl.portal.Settings](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Settings) — Settings portal backend interface

[org.freedesktop.impl.portal.Wallpaper](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.impl.portal.Wallpaper) — Portal for setting the desktop's Wallpaper

- Name

org.freedesktop.impl.portal.Access — Interface for presenting an access dialog

- Methods


[AccessDialog](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Access.AccessDialog "The AccessDialog() method") (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  s     title,
              IN  s     subtitle,
              IN  s     body,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);



- Description

This backend can be used by portal implementations that need to ask a direct access question, such as "May xyz use the microphone?"

- Method Details

### The AccessDialog() method

AccessDialog (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  s     title,
              IN  s     subtitle,
              IN  s     body,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);

Presents a "deny/grant" question to the user.

Supported keys in the _`options`_ include:

modal b

Whether to make the dialog modal. Defaults to true.

deny_label s

Label for the Deny button.

grant_label s

Label for the Grant button.

icon s

Icon name for an icon to show in the dialog. This should be a symbolic icon name.

choices a(ssa(ss)s)

List of serialized choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

The following results get returned via the _`results`_ vardict:

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

``IN o _`handle`_``:

Object path to export the Request object at

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the dialog

``IN s _`subtitle`_``:

Subtitle for the dialog

``IN s _`body`_``:

Body text, may be ""

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

---

- Name

org.freedesktop.impl.portal.Account — Backend for the portal for obtaining user information

- Methods


[GetUserInformation](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Account.GetUserInformation "The GetUserInformation() method") (IN  o     handle,
                    IN  s     app_id,
                    IN  s     window,
                    IN  a{sv} options,
                    OUT u     response,
                    OUT a{sv} results);



- Description

This Account portal lets sandboxed applications query basic information about the user, like his name and avatar photo. The portal backend will present the user with a dialog to confirm which (if any) information to share.

- Method Details

### The GetUserInformation() method

GetUserInformation (IN  o     handle,
                    IN  s     app_id,
                    IN  s     window,
                    IN  a{sv} options,
                    OUT u     response,
                    OUT a{sv} results);

Gets information about the user.

Supported keys in the _`options`_ vardict include:

reason s

A string that can be shown in the dialog to expain why the information is needed. This should be a complete sentence that explains what the application will do with the returned information, for example: Allows your personal information to be included with recipes you share with your friends.

The following results get returned via the _`results`_ vardict:

id s

The user id.

name s

The users real name.

image s

The uri of an image file for the users avatar photo.

``IN o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`window`_``:

Identifier for the window

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

---

- Name

org.freedesktop.impl.portal.AppChooser — Interface for choosing an application

- Methods


[ChooseApplication](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-AppChooser.ChooseApplication "The ChooseApplication() method") (IN  o     handle,
                   IN  s     app_id,
                   IN  s     parent_window,
                   IN  as    choices,
                   IN  a{sv} options,
                   OUT u     response,
                   OUT a{sv} results);
[UpdateChoices](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-AppChooser.UpdateChoices "The UpdateChoices() method")     (IN  o     handle,
                   IN  as    choices);



- Description

This backend can be used by portal implementations that need to choose an application from a list of applications.

This documentation describes version 2 of this interface.

- Method Details

### The ChooseApplication() method

ChooseApplication (IN  o     handle,
                   IN  s     app_id,
                   IN  s     parent_window,
                   IN  as    choices,
                   IN  a{sv} options,
                   OUT u     response,
                   OUT a{sv} results);

Presents a list of applications to the user to choose one.

Supported keys in the _`options`_ vardict include:

last_choice s

The app id that was selected the last time.

modal b

Whether to make the dialog modal. Defaults to yes.

content_type s

The content type to choose an application for.

uri s

The uri to choose an application for.

filename s

The filename to choose an application for. Note that this is just a basename, without a path.

activation_token s

A token that can be used to activate the application chooser.

The activation_token option was introduced in version 2 of the interface.

The following results get returned via the _`results`_ vardict:

choice s

The app id that was selected.

activation_token s

A token that can be used to activate the chosen application. If the application selection has involved user interaction, a new token should be generated by the portal implementation. Otherwise, this token may be the same as the one passed in _`options`_.

The activation_token option was introduced in version 2 of the interface.

``IN o _`handle`_``:

Object path to export the Request object at

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN as _`choices`_``:

App ids of applications to let the user choose from

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The UpdateChoices() method

UpdateChoices (IN  o  handle,
               IN  as choices);

This method can be called between the time of a ChooseApplication call and receiving the Response signal, to update the list of applications that are offered by the backend.

``IN o _`handle`_``:

the request handle

``IN as _`choices`_``:

App ids of applications to let the user choose from

---

- Name

org.freedesktop.impl.portal.Background — Background portal backend interface

- Methods


[GetAppState](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Background.GetAppState "The GetAppState() method")      (OUT a{sv} apps);
[NotifyBackground](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Background.NotifyBackground "The NotifyBackground() method") (IN  o     handle,
                  IN  s     app_id,
                  IN  s     name,
                  OUT u     response,
                  OUT a{sv} results);
[EnableAutostart](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Background.EnableAutostart "The EnableAutostart() method")  (IN  s     app_id,
                  IN  b     enable,
                  IN  as    commandline,
                  IN  u     flags,
                  OUT b     result);

### Signals

[RunningApplicationsChanged](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Background.RunningApplicationsChanged "The "RunningApplicationsChanged" signal") ();



- Description

This interface provides APIs related to applications that are running in the background.

- Method Details

### The GetAppState() method

GetAppState (OUT a{sv} apps);

Gets information about running apps. Each entry has an application ID as key. The returned values are of type u and have the following meaning:

0: Background (no open window)

1: Running (at least one open window)

2: Active (in the foreground)

``OUT a{sv} _`apps`_``:

an array with information about running apps

### The NotifyBackground() method

NotifyBackground (IN  o     handle,
                  IN  s     app_id,
                  IN  s     name,
                  OUT u     response,
                  OUT a{sv} results);

Notifies the user that an application is running in the background.

The following results get returned via the _`results`_ vardict:

result u

The choice that the user made regarding the background activity:

0: Forbid background activity by this app

1: Allow background activity by this app

2: Allow this instance of background activity

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`name`_``:

A display name for the application

``OUT u _`response`_``:

Numeric response, not used

``OUT a{sv} _`results`_``:

Vardict with results of the call

### The EnableAutostart() method

EnableAutostart (IN  s  app_id,
                 IN  b  enable,
                 IN  as commandline,
                 IN  u  flags,
                 OUT b  result);

Enables or disables autostart for an application.

The following flags are understood:

1: Use D-Bus activation

``IN s _`app_id`_``:

App id of the application

``IN b _`enable`_``:

TRUE to enable autostart, FALSE to disable it

``IN as _`commandline`_``:

The commandline to use in the autostart file

``IN u _`flags`_``:

Flags influencing the details of the autostarting

``OUT b _`result`_``:

TRUE if autostart was enabled, FALSE otherwise

### Signal Details

### The "RunningApplicationsChanged" signal

RunningApplicationsChanged ();

This signal gets emitted when applications change their state and it is worth calling GetAppState again.

---

- Name

org.freedesktop.impl.portal.Email — Email portal backend interface

- Methods


[ComposeEmail](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Email.ComposeEmail "The ComposeEmail() method") (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);



- Description

This Email portal lets sandboxed applications request sending an email.

- Method Details

### The ComposeEmail() method

ComposeEmail (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);

Lets the user compose an email.

Supported keys in the _`options`_ vardict include:

address s

The email address to send to.

addresses as

Email addresses to send to.

cc as

Email addresses to cc.

bcc as

Email addresses to bcc.

subject s

The subject for the email.

body s

The body for the email.

attachments as

The uris for files to attach.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

---

- Name

org.freedesktop.impl.portal.FileChooser — File chooser portal backend interface

- Methods


[OpenFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-FileChooser.OpenFile "The OpenFile() method")  (IN  o     handle,
           IN  s     app_id,
           IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT u     response,
           OUT a{sv} results);
[SaveFile](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-FileChooser.SaveFile "The SaveFile() method")  (IN  o     handle,
           IN  s     app_id,
           IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT u     response,
           OUT a{sv} results);
[SaveFiles](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-FileChooser.SaveFiles "The SaveFiles() method") (IN  o     handle,
           IN  s     app_id,
           IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT u     response,
           OUT a{sv} results);



- Description

The FileChooser portal allows sandboxed applications to ask the user for access to files outside the sandbox. The portal backend will present the user with a file chooser dialog.

- Method Details

### The OpenFile() method

OpenFile (IN  o     handle,
          IN  s     app_id,
          IN  s     parent_window,
          IN  s     title,
          IN  a{sv} options,
          OUT u     response,
          OUT a{sv} results);

Presents a file chooser dialog to the user to open one or more files.

Supported keys in the _`options`_ vardict include:

accept_label s

The label for the accept button. Mnemonic underlines are allowed.

modal b

Whether to make the dialog modal. Default is yes.

multiple b

Whether to allow selection of multiple files. Default is no.

directory b

Whether to select for folders instead of files. Default is to select files.

filters a(sa(us))

A list of serialized file filters. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

Request that this filter be set by default at dialog creation. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

choices a(ssa(ss)s)

A list of serialized combo boxes. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

The following results get returned via the _`results`_ vardict:

uris as

An array of strings containing the uris of the selected files.

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

The filter that was selected. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

writable b

Whether the file is opened with write access. Default is no.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The SaveFile() method

SaveFile (IN  o     handle,
          IN  s     app_id,
          IN  s     parent_window,
          IN  s     title,
          IN  a{sv} options,
          OUT u     response,
          OUT a{sv} results);

Presents a file chooser dialog to the user to save a file.

Supported keys in the _`options`_ vardict include:

accept_label s

The label for the accept button. Mnemonic underlines are allowed.

modal b

Whether to make the dialog modal. Default is yes.

multiple b

Whether to allow selection of multiple files. Default is no.

filters a(sa(us))

A list of serialized file filters. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

Request that this filter be set by default at dialog creation. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

choices a(ssa(ss)s)

A list of serialized combo boxes. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_name s

A suggested filename.

current_folder ay

A suggested folder to save the file in.

current_file ay

The current file (when saving an existing file).

The following results get returned via the _`results`_ vardict:

uris as

An array of strings containing the uri of the selected file.

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_filter (sa(us))

The filter that was selected. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The SaveFiles() method

SaveFiles (IN  o     handle,
           IN  s     app_id,
           IN  s     parent_window,
           IN  s     title,
           IN  a{sv} options,
           OUT u     response,
           OUT a{sv} results);

Asks for a folder as a location to save one or more files. The names of the files will be used as-is and appended to the selected folder's path in the list of returned files. If the selected folder already contains a file with one of the given names, the portal may prompt or take some other action to construct a unique file name and return that instead.

Supported keys in the _`options`_ vardict include:

handle_token s

A string that will be used as the last element of the _`handle`_. Must be a valid object path element. See the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) documentation for more information about the _`handle`_.

accept_label s

Label for the accept button. Mnemonic underlines are allowed.

modal b

Whether the dialog should be modal. Default is yes.

choices a(ssa(ss)s)

List of serialized combo boxes. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

current_folder ay

Suggested folder to save the files in. The byte array is expected to be null-terminated.

files aay

An array of file names to be saved. The array and byte arrays are expected to be null-terminated.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

uris as

An array of strings containing the uri corresponding to each file given by _`options`_, in the same order. Note that the file names may have changed, for example if a file with the same name in the selected folder already exists.

choices a(ss)

An array of pairs of strings, corresponding to the passed-in choices. See [OpenFile()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-FileChooser.OpenFile "The OpenFile() method") for details.

``IN o _`handle`_``:

Object path for the [org.freedesktop.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the file chooser dialog

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

``OUT a{sv} _`results`_``:

---

- Name

org.freedesktop.impl.portal.Inhibit — Inhibit portal backend interface

- Methods


[Inhibit](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Inhibit.Inhibit "The Inhibit() method")          (IN  o     handle,
                  IN  s     app_id,
                  IN  s     window,
                  IN  u     flags,
                  IN  a{sv} options);
[CreateMonitor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Inhibit.CreateMonitor "The CreateMonitor() method")    (IN  o     handle,
                  IN  o     session_handle,
                  IN  s     app_id,
                  IN  s     window,
                  OUT u     response);
[QueryEndResponse](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Inhibit.QueryEndResponse "The QueryEndResponse() method") (IN  o     session_handle);

### Signals

[StateChanged](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Inhibit.StateChanged "The "StateChanged" signal") (o     session_handle,
              a{sv} state);



- Description

The inhibit portal lets sandboxed applications inhibit the user session from ending, suspending, idling or getting switched away.

- Method Details

### The Inhibit() method

Inhibit (IN  o     handle,
         IN  s     app_id,
         IN  s     window,
         IN  u     flags,
         IN  a{sv} options);

Inhibits session status changes. As a side-effect of this call, a [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object is exported on the object path _`handle`_. To end the inhibition, call [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Request.Close "The Close() method") on that object.

The flags determine what changes are inhibited:

1: Logout

2: User Switch

4: Suspend

8: Idle

Supported keys in the _`options`_ vardict include:

reason s

User-visible reason for the inhibition.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

Application id

``IN s _`window`_``:

Identifier for the window

``IN u _`flags`_``:

Flags identifying what is inhibited

``IN a{sv} _`options`_``:

Vardict with optional further information

### The CreateMonitor() method

CreateMonitor (IN  o handle,
               IN  o session_handle,
               IN  s app_id,
               IN  s window,
               OUT u response);

Creates a monitoring session. While this session is active, the caller will receive StateChanged signals with updates on the session state.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

Object path for the created [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Session.top_of_page) object

``IN s _`app_id`_``:

App id of the application

``IN s _`window`_``:

the parent window

``OUT u _`response`_``:

the result of the operation (0 == success)

### The QueryEndResponse() method

QueryEndResponse (IN  o session_handle);

Acknowledges that the caller received the ["StateChanged"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Inhibit.StateChanged "The "StateChanged" signal") signal. This method should be called within one second or receiving a StateChanged signal with the 'Query End' state.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Session.top_of_page) object

### Signal Details

### The "StateChanged" signal

StateChanged (o     session_handle,
              a{sv} state);

The StateChanged signal is sent to active monitoring sessions when the session state changes.

When the session state changes to 'Query End', clients with active monitoring sessions are expected to respond by calling [QueryEndResponse()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Inhibit.QueryEndResponse "The QueryEndResponse() method") within a second of receiving the StateChanged signal.

The following information may get returned in the _`state`_ vardict:

screensaver-active b

Whether the screensaver is active.

session-state u

The state of the session.

1: Running

2: Query End

3: Ending

``o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``a{sv} _`state`_``:

Vardict with information about the session state

---

- Name

org.freedesktop.impl.portal.Lockdown — Lockdown backend interface

- Properties

[disable-printing](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-printing "The "disable-printing" property")              readwrite  b
[disable-save-to-disk](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-save-to-disk "The "disable-save-to-disk" property")          readwrite  b
[disable-application-handlers](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-application-handlers "The "disable-application-handlers" property")  readwrite  b
[disable-location](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-location "The "disable-location" property")              readwrite  b
[disable-camera](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-camera "The "disable-camera" property")                readwrite  b
[disable-microphone](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-microphone "The "disable-microphone" property")            readwrite  b
[disable-sound-output](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Lockdown.disable-sound-output "The "disable-sound-output" property")          readwrite  b



- Description

This interface provides access to high-level desktop lockdown settings that can disable entire portals, such as disabling printing or location services.

This is meant to be used by different portal frontends.

- Property Details

### The "disable-printing" property

disable-printing  readwrite  b

### The "disable-save-to-disk" property

disable-save-to-disk  readwrite  b

### The "disable-application-handlers" property

disable-application-handlers  readwrite  b

### The "disable-location" property

disable-location  readwrite  b

### The "disable-camera" property

disable-camera  readwrite  b

### The "disable-microphone" property

disable-microphone  readwrite  b

### The "disable-sound-output" property

disable-sound-output  readwrite  b

---

- Name

org.freedesktop.impl.portal.Notification — Notification portal backend interface

- Methods


[AddNotification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Notification.AddNotification "The AddNotification() method")    (IN  s     app_id,
                    IN  s     id,
                    IN  a{sv} notification);
[RemoveNotification](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Notification.RemoveNotification "The RemoveNotification() method") (IN  s     app_id,
                    IN  s     id);

### Signals

[ActionInvoked](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Notification.ActionInvoked "The "ActionInvoked" signal") (s  app_id,
               s  id,
               s  action,
               av parameter);



- Description

This notification interface lets sandboxed applications send and withdraw notifications.

- Method Details

### The AddNotification() method

AddNotification (IN  s     app_id,
                 IN  s     id,
                 IN  a{sv} notification);

Sends a notification.

The ID can be used to later withdraw the notification. If the application reuses the same ID without withdrawing, the notification is replaced by the new one.

The format of the _`notification`_ is the same as for [AddNotification()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Notification.AddNotification "The AddNotification() method").

``IN s _`app_id`_``:

App id of the application

``IN s _`id`_``:

Application-provided ID for this notification

``IN a{sv} _`notification`_``:

Vardict with the serialized notification

### The RemoveNotification() method

RemoveNotification (IN  s app_id,
                    IN  s id);

Withdraws a notification.

``IN s _`app_id`_``:

App id of the application

``IN s _`id`_``:

Application-provided ID for this notification

### Signal Details

### The "ActionInvoked" signal

ActionInvoked (s  app_id,
               s  id,
               s  action,
               av parameter);

for the action, if one was specified

Send to the application when a non-exported action is activated.

``s _`app_id`_``:

App id of the application

``s _`id`_``:

the application-provided ID for the notification

``s _`action`_``:

the name of the action

``av _`parameter`_``:

array which will contain the target parameter

---

- Name

org.freedesktop.impl.portal.PermissionStore — Database to store permissions

- Methods


[Lookup](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.Lookup "The Lookup() method")           (IN  s      table,
                  IN  s      id,
                  OUT a{sas} permissions,
                  OUT v      data);
[Set](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.Set "The Set() method")              (IN  s      table,
                  IN  b      create,
                  IN  s      id,
                  IN  a{sas} app_permissions,
                  IN  v      data);
[Delete](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.Delete "The Delete() method")           (IN  s      table,
                  IN  s      id);
[SetValue](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.SetValue "The SetValue() method")         (IN  s      table,
                  IN  b      create,
                  IN  s      id,
                  IN  v      data);
[SetPermission](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.SetPermission "The SetPermission() method")    (IN  s      table,
                  IN  b      create,
                  IN  s      id,
                  IN  s      app,
                  IN  as     permissions);
[DeletePermission](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.DeletePermission "The DeletePermission() method") (IN  s      table,
                  IN  s      id,
                  IN  s      app);
[GetPermission](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.GetPermission "The GetPermission() method")    (IN  s      table,
                  IN  s      id,
                  IN  s      app,
                  OUT as     permissions);
[List](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-PermissionStore.List "The List() method")             (IN  s      table,
                  OUT as     ids);

### Signals

[Changed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-PermissionStore.Changed "The "Changed" signal") (s      table,
         s      id,
         b      deleted,
         v      data,
         a{sas} permissions);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-PermissionStore.version "The "version" property")  readable   u



- Description

The permission store can be used by portals to store permissions that sandboxed applications have to various resources, such as files outside the sandbox.

Since the resources managed by portals can be varied, the permission store is fairly free-form: there can be multiple tables; resources are identified by an ID, as are applications, and permissions are stored as string arrays. None of these strings are interpreted by the permission store in any way.

In addition, the permission store allows to associate extra data (in the form of a GVariant) with each resource.

This document describes version 2 of the permission store interface.

- Method Details

### The Lookup() method

Lookup (IN  s      table,
        IN  s      id,
        OUT a{sas} permissions,
        OUT v      data);

Looks up the entry for a resource in one of the tables and returns all associated application permissions and data.

``IN s _`table`_``:

the name of the table to use

``IN s _`id`_``:

the resource ID to look up

``OUT a{sas} _`permissions`_``:

map from application ID to permissions

``OUT v _`data`_``:

data that is associated with the resource

### The Set() method

Set (IN  s      table,
     IN  b      create,
     IN  s      id,
     IN  a{sas} app_permissions,
     IN  v      data);

Writes the entry for a resource in the given table.

``IN s _`table`_``:

the name of the table to use

``IN b _`create`_``:

whether to create the table if it does not exist

``IN s _`id`_``:

the resource ID to modify

``IN a{sas} _`app_permissions`_``:

map from application ID to permissions

``IN v _`data`_``:

data to associate with the resource

### The Delete() method

Delete (IN  s table,
        IN  s id);

Removes the entry for a resource in the given table.

``IN s _`table`_``:

the name of the table to use

``IN s _`id`_``:

the resource ID to delete

### The SetValue() method

SetValue (IN  s table,
          IN  b create,
          IN  s id,
          IN  v data);

Sets just the data for a resource in the given table.

``IN s _`table`_``:

the name of the table to use

``IN b _`create`_``:

whether to create the table if it does not exist

``IN s _`id`_``:

the resource ID to modify

``IN v _`data`_``:

data to associate with the resource

### The SetPermission() method

SetPermission (IN  s  table,
               IN  b  create,
               IN  s  id,
               IN  s  app,
               IN  as permissions);

Sets the permissions for an application and a resource in the given table.

``IN s _`table`_``:

the name of the table to use

``IN b _`create`_``:

whether to create the table if it does not exist

``IN s _`id`_``:

the resource ID to modify

``IN s _`app`_``:

the application ID to modify

``IN as _`permissions`_``:

permissions to set

### The DeletePermission() method

DeletePermission (IN  s table,
                  IN  s id,
                  IN  s app);

Removes the entry for an application and a resource in the given table.

This method was added in version 2.

``IN s _`table`_``:

the name of the table to use

``IN s _`id`_``:

the resource ID to modify

``IN s _`app`_``:

the application ID to modify

### The GetPermission() method

GetPermission (IN  s  table,
               IN  s  id,
               IN  s  app,
               OUT as permissions);

Gets the entry for an application and a resource in the given table.

``IN s _`table`_``:

the name of the table to use

``IN s _`id`_``:

the resource ID to modify

``IN s _`app`_``:

the application ID to modify

``OUT as _`permissions`_``:

permissions to get

### The List() method

List (IN  s  table,
      OUT as ids);

Returns all the resources that are present in the table.

``IN s _`table`_``:

the name of the table to use

``OUT as _`ids`_``:

IDs of all resources that are present in the table

### Signal Details

### The "Changed" signal

Changed (s      table,
         s      id,
         b      deleted,
         v      data,
         a{sas} permissions);

The Changed signal is emitted when the entry for a resource is modified or deleted. If the entry was deleted, then _`data`_ and _`permissions`_ contain the last values that were found in the database. If the entry was modified, they contain the new values.

``s _`table`_``:

the name of the table

``s _`id`_``:

``b _`deleted`_``:

whether the resource was deleted

``v _`data`_``:

the data that is associated the resource

``a{sas} _`permissions`_``:

the permissions that are associated with the resource

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Print — Print portal backend interface

- Methods


[Print](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Print.Print "The Print() method")        (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  s     title,
              IN  h     fd,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);
[PreparePrint](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Print.PreparePrint "The PreparePrint() method") (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  s     title,
              IN  a{sv} settings,
              IN  a{sv} page_setup,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);



- Description

The Print portal allows sandboxed applications to print.

- Method Details

### The Print() method

Print (IN  o     handle,
       IN  s     app_id,
       IN  s     parent_window,
       IN  s     title,
       IN  h     fd,
       IN  a{sv} options,
       OUT u     response,
       OUT a{sv} results);

Prints a file.

The file is given in the form of a file descriptor open for reading.

If a valid token is present in the _`options`_, then this call will print with the settings from the Print call that the token refers to. If no token is present, then a print dialog will be presented to the user.

Note that it is up to the portal implementation to determine how long it considers tokens valid.

Supported keys in the _`options`_ vardict:

modal b

Whether to make the dialog modal. Defaults to yes.

token u

Token that was returned by a previous [PreparePrint()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Print.PreparePrint "The PreparePrint() method") call.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the print dialog

``IN h _`fd`_``:

File descriptor from which to read the content to print

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The PreparePrint() method

PreparePrint (IN  o     handle,
              IN  s     app_id,
              IN  s     parent_window,
              IN  s     title,
              IN  a{sv} settings,
              IN  a{sv} page_setup,
              IN  a{sv} options,
              OUT u     response,
              OUT a{sv} results);

Presents a print dialog to the user and returns print settings and page setup.

Supported keys in the _`options`_ vardict:

modal b

Whether to make the dialog modal. Defaults to yes.

The following results get returned via the _`results`_ vardict:

settings a{sv}

Print settings as set up by the user in the print dialog.

page-setup a{sv}

Page setup as set up by the user in the print dialog.

token u

Token that can be passed to a subsequent [Print()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Print.Print "The Print() method") call to bypass the print dialog.

The [Print()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-portal-Print.Print "The Print() method") documentation has details about the supported keys in settings and page-setup.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN s _`title`_``:

Title for the print dialog

``IN a{sv} _`settings`_``:

Serialized print settings

``IN a{sv} _`page_setup`_``:

Serialized page setup

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

``OUT a{sv} _`results`_``:

---

- Name

org.freedesktop.impl.portal.RemoteDesktop — Remote desktop portal backend interface

- Methods


[CreateSession](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.CreateSession "The CreateSession() method")               (IN  o     handle,
                             IN  o     session_handle,
                             IN  s     app_id,
                             IN  a{sv} options,
                             OUT u     response,
                             OUT a{sv} results);
[SelectDevices](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.SelectDevices "The SelectDevices() method")               (IN  o     handle,
                             IN  o     session_handle,
                             IN  s     app_id,
                             IN  a{sv} options,
                             OUT u     response,
                             OUT a{sv} results);
[Start](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.Start "The Start() method")                       (IN  o     handle,
                             IN  o     session_handle,
                             IN  s     app_id,
                             IN  s     parent_window,
                             IN  a{sv} options,
                             OUT u     response,
                             OUT a{sv} results);
[NotifyPointerMotion](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyPointerMotion "The NotifyPointerMotion() method")         (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  d     dx,
                             IN  d     dy);
[NotifyPointerMotionAbsolute](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyPointerMotionAbsolute "The NotifyPointerMotionAbsolute() method") (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  d     x,
                             IN  d     y);
[NotifyPointerButton](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyPointerButton "The NotifyPointerButton() method")         (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     button,
                             IN  u     state);
[NotifyPointerAxis](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyPointerAxis "The NotifyPointerAxis() method")           (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  d     dx,
                             IN  d     dy);
[NotifyPointerAxisDiscrete](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyPointerAxisDiscrete "The NotifyPointerAxisDiscrete() method")   (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     axis,
                             IN  i     steps);
[NotifyKeyboardKeycode](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyKeyboardKeycode "The NotifyKeyboardKeycode() method")       (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     keycode,
                             IN  u     state);
[NotifyKeyboardKeysym](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyKeyboardKeysym "The NotifyKeyboardKeysym() method")        (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  i     keysym,
                             IN  u     state);
[NotifyTouchDown](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyTouchDown "The NotifyTouchDown() method")             (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  u     slot,
                             IN  d     x,
                             IN  d     y);
[NotifyTouchMotion](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyTouchMotion "The NotifyTouchMotion() method")           (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  u     slot,
                             IN  d     x,
                             IN  d     y);
[NotifyTouchUp](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-RemoteDesktop.NotifyTouchUp "The NotifyTouchUp() method")               (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     slot);

- Properties

[AvailableDeviceTypes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-RemoteDesktop.AvailableDeviceTypes "The "AvailableDeviceTypes" property")  readable   u
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-RemoteDesktop.version "The "version" property")               readable   u



- Description

The Remote desktop portal allows to create remote desktop sessions.

- Method Details

### The CreateSession() method

CreateSession (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);

Create a remote desktop session.

A remote desktop session is used to allow remote controlling a desktop session. It can also be used together with a screen cast session (see org.freedesktop.portal.ScreenCast), but may only be started and stopped with this interface.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

session s

The session id. A string representing the created screen cast session.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

``IN s _`app_id`_``:

App id of the application

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The SelectDevices() method

SelectDevices (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);

``IN o _`handle`_``:

``IN o _`session_handle`_``:

``IN s _`app_id`_``:

``IN a{sv} _`options`_``:

``OUT u _`response`_``:

``OUT a{sv} _`results`_``:

### The Start() method

Start (IN  o     handle,
       IN  o     session_handle,
       IN  s     app_id,
       IN  s     parent_window,
       IN  a{sv} options,
       OUT u     response,
       OUT a{sv} results);

Start the remote desktop session.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The NotifyPointerMotion() method

NotifyPointerMotion (IN  o     session_handle,
                     IN  a{sv} options,
                     IN  d     dx,
                     IN  d     dy);

Notify about a new relative pointer motion event. The (dx, dy) vector represents the new pointer position in the streams logical coordinate space.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN d _`dx`_``:

Relative movement on the x axis

``IN d _`dy`_``:

Relative movement on the y axis

### The NotifyPointerMotionAbsolute() method

NotifyPointerMotionAbsolute (IN  o     session_handle,
                             IN  a{sv} options,
                             IN  u     stream,
                             IN  d     x,
                             IN  d     y);

Notify about a new absolute pointer motion event. The (x, y) position represents the new pointer position in the streams logical coordinate space (see the logical_size stream property in [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN d _`x`_``:

Pointer motion x coordinate

``IN d _`y`_``:

Pointer motion y coordinate

### The NotifyPointerButton() method

NotifyPointerButton (IN  o     session_handle,
                     IN  a{sv} options,
                     IN  i     button,
                     IN  u     state);

The pointer button is encoded according to Linux Evdev button codes.

May only be called if POINTER access was provided after starting the session.

Available button states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`button`_``:

The pointer button was pressed or released

``IN u _`state`_``:

The new state of the button

### The NotifyPointerAxis() method

NotifyPointerAxis (IN  o     session_handle,
                   IN  a{sv} options,
                   IN  d     dx,
                   IN  d     dy);

The axis movement from a 'smooth scroll' device, such as a touchpad. When applicable, the size of the motion delta should be equivalent to the motion vector of a pointer motion done using the same advice.

May only be called if POINTER access was provided after starting the session.

Supported keys in the _`options`_ vardict include:

finish b

If set to true, this is the last axis event in a series, for example as a result of the fingers being lifted from a touchpad after a two-finger scroll. Default is false.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN d _`dx`_``:

Relative axis movement on the x axis

``IN d _`dy`_``:

Relative axis movement on the y axis

### The NotifyPointerAxisDiscrete() method

NotifyPointerAxisDiscrete (IN  o     session_handle,
                           IN  a{sv} options,
                           IN  u     axis,
                           IN  i     steps);

May only be called if POINTER access was provided after starting the session.

Available axes:

0: Vertical scroll

1: Horizontal scroll

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`axis`_``:

The axis that was scrolled

``IN i _`steps`_``:

The number of steps scrolled

### The NotifyKeyboardKeycode() method

NotifyKeyboardKeycode (IN  o     session_handle,
                       IN  a{sv} options,
                       IN  i     keycode,
                       IN  u     state);

May only be called if KEYBOARD access was provided after starting the session.

Available keyboard keysym states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`keycode`_``:

Keyboard code that was pressed or released

``IN u _`state`_``:

New state of keyboard keysym

### The NotifyKeyboardKeysym() method

NotifyKeyboardKeysym (IN  o     session_handle,
                      IN  a{sv} options,
                      IN  i     keysym,
                      IN  u     state);

May only be called if KEYBOARD access was provided after starting the session.

Available keyboard keysym states:

0: Released

1: Pressed

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN i _`keysym`_``:

Keyboard symbol that was pressed or released

``IN u _`state`_``:

New state of keyboard keysym

### The NotifyTouchDown() method

NotifyTouchDown (IN  o     session_handle,
                 IN  a{sv} options,
                 IN  u     stream,
                 IN  u     slot,
                 IN  d     x,
                 IN  d     y);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch down event. The (x, y) position represents the new touch point position in the streams logical coordinate space (see the logical_size stream property in[org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN u _`slot`_``:

Touch slot where touch point appeared

``IN d _`x`_``:

Touch down x coordinate

``IN d _`y`_``:

Touch down y coordinate

### The NotifyTouchMotion() method

NotifyTouchMotion (IN  o     session_handle,
                   IN  a{sv} options,
                   IN  u     stream,
                   IN  u     slot,
                   IN  d     x,
                   IN  d     y);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch motion event. The (x, y) position represents where the touch point position in the streams logical coordinate space moved (see the logical_size stream property in [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page)).

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`stream`_``:

The PipeWire stream node the coordinate is relative to

``IN u _`slot`_``:

Touch slot where touch point appeared

``IN d _`x`_``:

Touch motion x coordinate

``IN d _`y`_``:

Touch motion y coordinate

### The NotifyTouchUp() method

NotifyTouchUp (IN  o     session_handle,
               IN  a{sv} options,
               IN  u     slot);

May only be called if TOUCHSCREEN access was provided after starting the session.

Notify about a new touch up event.

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-Session.top_of_page) object

``IN a{sv} _`options`_``:

Vardict with optional further information

``IN u _`slot`_``:

Touch slot where touch point appeared

- Property Details

### The "AvailableDeviceTypes" property

AvailableDeviceTypes  readable   u

A bitmask of available source types. Currently defined types are:

1: KEYBOARD

2: POINTER

4: TOUCHSCREEN

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Request — Shared request interface

- Methods


[Close](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Request.Close "The Close() method") ();



- Description

The Request interface is shared by all portal backend interfaces. When a backend method is called, the backend exports a Request object on the object path that was sent with the method call. The Request will stay alive for the duration of the user interaction related to the method call.

The portal can abort the interaction calling [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Request.Close "The Close() method") on the Request object.

- Method Details

### The Close() method

Close ();

Ends the user interaction to which this object refers. Dialogs and other UIs related to the request should be closed.

---

- Name

org.freedesktop.impl.portal.ScreenCast — Screen cast portal backend interface

- Methods


[CreateSession](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-ScreenCast.CreateSession "The CreateSession() method") (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);
[SelectSources](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-ScreenCast.SelectSources "The SelectSources() method") (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);
[Start](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-ScreenCast.Start "The Start() method")         (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  s     parent_window,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);

- Properties

[AvailableSourceTypes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-ScreenCast.AvailableSourceTypes "The "AvailableSourceTypes" property")  readable   u
[AvailableCursorModes](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-ScreenCast.AvailableCursorModes "The "AvailableCursorModes" property")  readable   u
[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-ScreenCast.version "The "version" property")               readable   u



- Description

The Screen cast portal allows to create screen cast sessions.

This documentation describes version 4 of this interface.

- Method Details

### The CreateSession() method

CreateSession (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);

Create a screen cast session.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

session_id s

The session id. A string representing the created screen cast session.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Session.top_of_page) object representing the session being created

``IN s _`app_id`_``:

App id of the application

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The SelectSources() method

SelectSources (IN  o     handle,
               IN  o     session_handle,
               IN  s     app_id,
               IN  a{sv} options,
               OUT u     response,
               OUT a{sv} results);

Configure what the screen cast session should record.

Supported keys in the _`options`_ vardict include:

types u

Bitmask of what type of content to record. Default is MONITOR.

multiple b

Whether to allow selecting multiple sources. Default is no.

restore_data (suv)

The data to restore from a previous session.

If the stored session cannot be restored, this value is ignored and the user will be prompted normally. This may happen when, for example, the session contains a monitor or a window that is not available anymore, or when the stored permissions are withdrawn.

The restore data is composed of the vendor name (e.g. "GNOME" or "KDE"), the version of the implementation-specific private data, and the implementation-specific private data itself.

This option was added in version 4 of this interface.

persist_mode u

How this session should persist. Default is 0. Accepted values are:

0: Do not persist (default)

1: Permissions persist as long as the application is running

2: Permissions persist until explicitly revoked

If the permission for the session to persist is granted, a restore token will be returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal of the [org.freedesktop.portal.ScreenCast](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-portal-ScreenCast.top_of_page).SelectSources method.

This option was added in version 4 of this interface.

For available source types, see the AvailableSourceTypes property.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Session.top_of_page) object representing the session

``IN s _`app_id`_``:

App id of the application

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The Start() method

Start (IN  o     handle,
       IN  o     session_handle,
       IN  s     app_id,
       IN  s     parent_window,
       IN  a{sv} options,
       OUT u     response,
       OUT a{sv} results);

Start the screen cast session. This will typically result the portal presenting a dialog letting the user do the selection set up by SelectSources.

The following results get returned via the ["Response"](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-portal-Request.Response "The "Response" signal") signal:

streams a(ua{sv})

An array of PipeWire streams. Each stream consists of a PipeWire node ID (the first element in the tuple, and a Vardict of properties.

The array will contain a single stream if 'multiple' (see SelectSources) was set to 'false', or at least one stream if 'multiple' was set to 'true' as part of the SelectSources method.

persist_mode u

Portal implementations can optionally offer ways to "reduce" the persist mode, for example by explicitly asking the user. See the 'persist_mode' option of SelectSources for a list of possible values.

If no persist mode is returned by the Start request, it is assumed to be the same persist mode received during SelectSources.

This option was added in version 4 of this interface.

restore_data (suv)

The data to be restored. This is the data that a portal implementation sent in reaction to transient or persistent modes.

This data will be stored in the permission store if the effective persist mode is 2, and may be passed in the future as part of the SelectSources() method call. The portal backend should store enough information in 'restore_data' to be able to restore the session later. The portal backend can deal with unavailable sources however they see fit.

Data generated by a portal implementation must be consumable by the same portal implementation, but not necessarily by others. Thus, it is important to properly handle 'restore_data' not being in a valid format. This may happen when, e.g., the user switched to another desktop environment, or is using a different portal implementation.

This response option was added in version 4 of this interface.

Stream properties include:

position (ii)

A tuple consisting of the position (x, y) in the compositor coordinate space. Note that the position may not be equivalent to a position in a pixel coordinate space. Only available for monitor streams.

size (ii)

A tuple consisting of (width, height). The size represents the size of the stream as it is displayed in the compositor coordinate space. Note that this size may not be equivalent to a size in a pixel coordinate space. The size may differ from the size of the stream.

source_type u

The type of the content which is being screen casted. For available source types, see the AvailableSourceTypes property. This property was added in version 3 of this interface.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN o _`session_handle`_``:

Object path for the [org.freedesktop.impl.portal.Session](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Session.top_of_page) object representing the session

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

- Property Details

### The "AvailableSourceTypes" property

AvailableSourceTypes  readable   u

A bitmask of available source types. Currently defined types are:

1: MONITOR

2: WINDOW

4: VIRTUAL

### The "AvailableCursorModes" property

AvailableCursorModes  readable   u

Available cursor mode values:

1: Hidden. The cursor is not part of the screen cast stream.

2: Embedded: The cursor is embedded as part of the stream buffers.

4: Metadata: The cursor is not part of the screen cast stream, but sent as PipeWire stream metadata.

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Screenshot — Screenshot portal backend interface

- Methods


[Screenshot](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Screenshot.Screenshot "The Screenshot() method") (IN  o     handle,
            IN  s     app_id,
            IN  s     parent_window,
            IN  a{sv} options,
            OUT u     response,
            OUT a{sv} results);
[PickColor](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Screenshot.PickColor "The PickColor() method")  (IN  o     handle,
            IN  s     app_id,
            IN  s     parent_window,
            IN  a{sv} options,
            OUT u     response,
            OUT a{sv} results);



- Description

This screenshot portal lets sandboxed applications request a screenshot.

- Method Details

### The Screenshot() method

Screenshot (IN  o     handle,
            IN  s     app_id,
            IN  s     parent_window,
            IN  a{sv} options,
            OUT u     response,
            OUT a{sv} results);

Takes a screenshot.

Supported keys in the _`options`_ vardict include:

modal b

Whether the dialog should be modal. Defaults to yes.

interactive b

Hint whether the dialog should offer customization before taking a screenshot. Defaults to no.

The following results get returned via the _`results`_ vardict:

uri s

A string containing the uri of the screenshot.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

### The PickColor() method

PickColor (IN  o     handle,
           IN  s     app_id,
           IN  s     parent_window,
           IN  a{sv} options,
           OUT u     response,
           OUT a{sv} results);

Obtains the value of a pixel.

The following results get returned via the _`results`_ vardict:

color (ddd)

The color, rgb values in the range [0,1].

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see [Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers")

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

---

- Name

org.freedesktop.impl.portal.Secret — Secret portal backend interface

- Methods


[RetrieveSecret](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Secret.RetrieveSecret "The RetrieveSecret() method") (IN  o     handle,
                IN  s     app_id,
                IN  h     fd,
                IN  a{sv} options,
                OUT u     response,
                OUT a{sv} results);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Secret.version "The "version" property")  readable   u



- Description

The Secret portal allows sandboxed applications to retrieve a per-application master secret.

- Method Details

### The RetrieveSecret() method

RetrieveSecret (IN  o     handle,
                IN  s     app_id,
                IN  h     fd,
                IN  a{sv} options,
                OUT u     response,
                OUT a{sv} results);

Retrieves a master secret for a sandboxed application.

Supported keys in the _`options`_ vardict include:

token s

An opaque string associated with the retrieve secret.

``IN o _`handle`_``:

Object path for the [org.freedesktop.impl.portal.Request](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-interface-org-freedesktop-impl-portal-Request.top_of_page) object representing this call

``IN s _`app_id`_``:

App id of the application

``IN h _`fd`_``:

Writable file descriptor for transporting the secret

``IN a{sv} _`options`_``:

Vardict with optional further information

``OUT u _`response`_``:

Numeric response

``OUT a{sv} _`results`_``:

Vardict with the results of the call

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Session — Shared session interface

- Methods


[Close](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Session.Close "The Close() method") ();

### Signals

[Closed](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Session.Closed "The "Closed" signal") ();

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Session.version "The "version" property")  readable   u



- Description

The Session interface is shared by all portal interfaces that involve long lived sessions. When a method that creates a session is called, the reply will include a session handle (i.e. object path) for a Session object, which will stay alive for the duration of the session.

The portal can abort the interaction by calling [Close()](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Session.Close "The Close() method") on the Session object.

- Method Details

### The Close() method

Close ();

Close the session.

### Signal Details

### The "Closed" signal

Closed ();

The session was closed by the portal implementation.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Settings — Settings portal backend interface

- Methods


[ReadAll](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Settings.ReadAll "The ReadAll() method") (IN  as        namespaces,
         OUT a{sa{sv}} value);
[Read](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Settings.Read "The Read() method")    (IN  s         namespace,
         IN  s         key,
         OUT v         value);

### Signals

[SettingChanged](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-signal-org-freedesktop-impl-portal-Settings.SettingChanged "The "SettingChanged" signal") (s namespace,
                s key,
                v value);

- Properties

[version](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-property-org-freedesktop-impl-portal-Settings.version "The "version" property")  readable   u



- Description

This interface provides read-only access to a small number of host settings required for toolkits similar to XSettings. It is not for general purpose settings.

Currently the interface provides the following keys:

org.freedesktop.appearance color-scheme u

Indicates the system's preferred color scheme. Supported values are:

0: No preference

1: Prefer dark appearance

2: Prefer light appearance

Unknown values should be treated as 0 (no preference).

Implementations can provide other keys; they are entirely implementation details that are undocumented. If you are a toolkit and want to use this please open an issue.

Note that the Settings portal can operate without this backend, implementing it is optional.

- Method Details

### The ReadAll() method

ReadAll (IN  as        namespaces,
         OUT a{sa{sv}} value);

If _`namespaces`_ is an empty array or contains an empty string it matches all. Globbing is supported but only for trailing sections, e.g. "org.example.*".

``IN as _`namespaces`_``:

List of namespaces to filter results by, supports simple globbing explained below.

``OUT a{sa{sv}} _`value`_``:

Dictionary of namespaces to its keys and values.

### The Read() method

Read (IN  s namespace,
      IN  s key,
      OUT v value);

Reads a single value. Returns an error on any unknown namespace or key.

``IN s _`namespace`_``:

Namespace to look up _`key`_ in.

``IN s _`key`_``:

The key to get.

``OUT v _`value`_``:

The value _`key`_ is set to.

### Signal Details

### The "SettingChanged" signal

SettingChanged (s namespace,
                s key,
                v value);

Emitted when a setting changes.

``s _`namespace`_``:

Namespace of changed setting.

``s _`key`_``:

The key of changed setting.

``v _`value`_``:

The new value.

- Property Details

- The "version" property

version  readable   u

---

- Name

org.freedesktop.impl.portal.Wallpaper — Portal for setting the desktop's Wallpaper

- Methods


[SetWallpaperURI](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-method-org-freedesktop-impl-portal-Wallpaper.SetWallpaperURI "The SetWallpaperURI() method") (IN  o     handle,
                 IN  s     app_id,
                 IN  s     parent_window,
                 IN  s     uri,
                 IN  a{sv} options,
                 OUT u     response);



- Description

This simple interface lets sandboxed applications set the user's desktop background picutre.

- Method Details

### The SetWallpaperURI() method

SetWallpaperURI (IN  o     handle,
                 IN  s     app_id,
                 IN  s     parent_window,
                 IN  s     uri,
                 IN  a{sv} options,
                 OUT u     response);

[Common Conventions](https://docs.flatpak.org/en/latest/portal-api-reference.html#parent_window "Parent window identifiers") _`options`_: Options that influence the behavior of the portal _`response`_: Numberic response

Asks to set a given picture as the desktop background picture.

The following options are supported: show-preview: (b) whether to show a preview of the picture. Note that the portal may decide to show a preview even if this option is not set set-on: (s) where to set the wallpaper. Possible values are 'background', 'lockscreen' or 'both'

``IN o _`handle`_``:

Object path to export the Request object at

``IN s _`app_id`_``:

App id of the application

``IN s _`parent_window`_``:

Identifier for the application window, see

``IN s _`uri`_``:

``IN a{sv} _`options`_``:

``OUT u _`response`_``:

