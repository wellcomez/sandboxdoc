---
date: 2022-12-16 15:32
title: flatpak-Appdata files
tags:
- flatpak
 - build
---

# flatpak-Appdata files


## Appdata files
	
Appdata files are used by application stores (e.g. KDE Discover, GNOME Software) in order to display metadata about your application, such as a description, screenshots, changelogs when updates are available, and other miscellaneous things.

Your Appdata file should be prefixed with your application’s appid and placed in `/app/share/metainfo/`. You should also use `appstream-util validate-relax` to check your file for errors before including it.

Example:
~~~
/app/share/metainfo/org.gnome.Dictionary.appdata.xml
~~~

If interested, you can read the full spec [here](https://www.freedesktop.org/software/appstream/docs/).