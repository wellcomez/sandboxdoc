---
date: 2023-04-13 13:48
title: mount
tags:
- 
---

# mount


##  1. Mount root slave
```c
/* Mark everything as slave, so that we still
* receive mounts from the real root, but don't
* propagate mounts to the real root. */
if (mount(NULL, "/", NULL, MS_SLAVE | MS_REC, NULL) < 0)
  die_with_error("Failed to make / slave");
```

## 2. create tmpfs
```c
/* Create a tmpfs which we will use as / in the namespace */
if (mount("tmpfs", base_path, "tmpfs", MS_NODEV | MS_NOSUID, NULL) != 0)
  die_with_error("Failed to mount tmpfs");
```

##   3. change current pwd
```c
old_cwd = get_current_dir_name();

/* Chdir to the new root tmpfs mount. This will be the CWD during
  the entire setup. Access old or new root via "oldroot" and "newroot". */
if (chdir(base_path) != 0)
  die_with_error("chdir base_path");
```
## 4. chroot to "newroot"
```c
  /* We create a subdir "$base_path/newroot" for the new root, that
   * way we can pivot_root to base_path, and put the old root at
   * "$base_path/oldroot". This avoids problems accessing the oldroot
   * dir if the user requested to bind mount something over / (or
   * over /tmp, now that we use that for base_path). */

  if (mkdir("newroot", 0755))
    die_with_error("Creating newroot failed");

  if (mount("newroot", "newroot", NULL, MS_MGC_VAL | MS_BIND | MS_REC, NULL) <
      0)
    die_with_error("setting up newroot bind");

  if (mkdir("oldroot", 0755))
    die_with_error("Creating oldroot failed");

  if (pivot_root(base_path, "oldroot"))
    die_with_error("pivot_root");
```
------------------
```c
  if (chdir("/") != 0)
    die_with_error("chdir / (base path)");
```
----------------
## 5. 带特权
```c
  if (is_privileged) {
    pid_t child;
    int privsep_sockets[2];

    if (socketpair(AF_UNIX, SOCK_SEQPACKET | SOCK_CLOEXEC, 0,
                   privsep_sockets) != 0)
      die_with_error("Can't create privsep socket");

    child = fork();
    if (child == -1)
      die_with_error("Can't fork unprivileged helper");

    if (child == 0) {
      /* Unprivileged setup process */
      drop_privs(FALSE, TRUE);
      close(privsep_sockets[0]);
      setup_newroot(opt_unshare_pid, privsep_sockets[1]);
      exit(0);
    } else {
      int status;
      uint32_t buffer[2048]; /* 8k, but is int32 to guarantee nice alignment */
      uint32_t op, flags;
      const char *arg1, *arg2;
      cleanup_fd int unpriv_socket = -1;

      unpriv_socket = privsep_sockets[0];
      close(privsep_sockets[1]);

      do {
        op = read_priv_sec_op(unpriv_socket, buffer, sizeof(buffer), &flags,
                              &arg1, &arg2);
        privileged_op(-1, op, flags, arg1, arg2);
        if (write(unpriv_socket, buffer, 1) != 1)
          die("Can't write to op_socket");
      } while (op != PRIV_SEP_OP_DONE);

      waitpid(child, &status, 0);
      /* Continue post setup */
    }
  } 
```
## 6. 无特权
```c
  else {
    setup_newroot(opt_unshare_pid, -1);
  }
```

## 7. umount oldroot
### 7.1. 把oldroot先改成 private
```c
  close_ops_fd();

  /* The old root better be rprivate or we will send unmount events to the
   * parent namespace */
  if (mount("oldroot", "oldroot", NULL, MS_REC | MS_PRIVATE, NULL) != 0)
    die_with_error("Failed to make old root rprivate");
```

### 7.2. umount oldroot
```c
  if (umount2("oldroot", MNT_DETACH))
    die_with_error("unmount old root");

```
## 8. 第二次 chroot
```c
  /* This is our second pivot. It's like we're a Silicon Valley startup flush
   * with cash but short on ideas!
   *
   * We're aiming to make /newroot the real root, and get rid of /oldroot. To do
   * that we need a temporary place to store it before we can unmount it.
   */
  {
    cleanup_fd int oldrootfd = open("/", O_DIRECTORY | O_RDONLY);
    if (oldrootfd < 0)
      die_with_error("can't open /");
    if (chdir("/newroot") != 0)
      die_with_error("chdir /newroot");
    /* While the documentation claims that put_old must be underneath
     * new_root, it is perfectly fine to use the same directory as the
     * kernel checks only if old_root is accessible from new_root.
     *
     * Both runc and LXC are using this "alternative" method for
     * setting up the root of the container:
     *
     * https://github.com/opencontainers/runc/blob/master/libcontainer/rootfs_linux.go#L671
     * https://github.com/lxc/lxc/blob/master/src/lxc/conf.c#L1121
     */
    if (pivot_root(".", ".") != 0)
      die_with_error("pivot_root(/newroot)");
    if (fchdir(oldrootfd) < 0)
      die_with_error("fchdir to oldroot");
    if (umount2(".", MNT_DETACH) < 0)
      die_with_error("umount old root");
    if (chdir("/") != 0)
      die_with_error("chdir /");
  }
```
## 9. 处理权限
```c
#if 0  处理安装
  if (opt_userns2_fd > 0 && setns(opt_userns2_fd, CLONE_NEWUSER) != 0)
    die_with_error("Setting userns2 failed");
#endif

  if (opt_unshare_user &&
      (ns_uid != opt_sandbox_uid || ns_gid != opt_sandbox_gid) &&
      opt_userns_block_fd == -1) {
    /* Now that devpts is mounted and we've no need for mount
       permissions we can create a new userspace and map our uid
       1:1 */

    if (unshare(CLONE_NEWUSER))
      die_with_error("unshare user ns");

    /* We're in a new user namespace, we got back the bounding set, clear it
     * again */
    drop_cap_bounding_set(FALSE);

    write_uid_gid_map(opt_sandbox_uid, ns_uid, opt_sandbox_gid, ns_gid, -1,
                      FALSE, FALSE);
  }


  /* All privileged ops are done now, so drop caps we don't need */
  drop_privs(!is_privileged, TRUE);

  if (opt_block_fd != -1) {
    char b[1];
    (void)TEMP_FAILURE_RETRY(read(opt_block_fd, b, 1));
    close(opt_block_fd);
  }
```
## 10. seccomp
```c
  if (opt_seccomp_fd != -1) {
    seccomp_data = load_file_data(opt_seccomp_fd, &seccomp_len);
    if (seccomp_data == NULL)
      die_with_error("Can't read seccomp data");

    if (seccomp_len % 8 != 0)
      die("Invalid seccomp data, must be multiple of 8");

    seccomp_prog.len = seccomp_len / 8;
    seccomp_prog.filter = (struct sock_filter *)seccomp_data;

    close(opt_seccomp_fd);
  }

  umask(old_umask);

  new_cwd = "/";
  if (opt_chdir_path) {
    if (chdir(opt_chdir_path))
      die_with_error("Can't chdir to %s", opt_chdir_path);
    new_cwd = opt_chdir_path;
  } else if (chdir(old_cwd) == 0) {
    /* If the old cwd is mapped in the sandbox, go there */
    new_cwd = old_cwd;
  } else {
    /* If the old cwd is not mapped, go to home */
    const char *home = getenv("HOME");
    if (home != NULL && chdir(home) == 0)
      new_cwd = home;
  }
  xsetenv("PWD", new_cwd, 1);
  free(old_cwd);

  if (opt_new_session && setsid() == (pid_t)-1)
    die_with_error("setsid");

  if (label_exec(opt_exec_label) == -1)
    die_with_error("label_exec %s", argv[0]);

  __debug__(("forking for child\n"));

  if (!opt_as_pid_1 &&
      (opt_unshare_pid || lock_files != NULL || opt_sync_fd != -1)) {
    /* We have to have a pid 1 in the pid namespace, because
     * otherwise we'll get a bunch of zombies as nothing reaps
     * them. Alternatively if we're using sync_fd or lock_files we
     * need some process to own these.
     */

    pid = fork();
    if (pid == -1)
      die_with_error("Can't fork for pid 1");

    if (pid != 0) {
      drop_all_caps(FALSE);

      /* Close fds in pid 1, except stdio and optionally event_fd
         (for syncing pid 2 lifetime with monitor_child) and
         opt_sync_fd (for syncing sandbox lifetime with outside
         process).
         Any other fds will been passed on to the child though. */
      {
        int dont_close[3];
        int j = 0;
        if (event_fd != -1)
          dont_close[j++] = event_fd;
        if (opt_sync_fd != -1)
          dont_close[j++] = opt_sync_fd;
        dont_close[j++] = -1;
        fdwalk(proc_fd, close_extra_fds, dont_close);
      }

      return do_init(event_fd, pid,
                     seccomp_data != NULL ? &seccomp_prog : NULL);
    }
  }

  __debug__(("launch executable %s\n", argv[0]));

  if (proc_fd != -1)
    close(proc_fd);

  /* If we are using --as-pid-1 leak the sync fd into the sandbox.
     --sync-fd will still work unless the container process doesn't close this
     file.  */
  if (!opt_as_pid_1) {
    if (opt_sync_fd != -1)
      close(opt_sync_fd);
  }

  /* We want sigchild in the child */
  unblock_sigchild();

  /* Optionally bind our lifecycle */
  handle_die_with_parent();

  if (!is_privileged)
    set_ambient_capabilities();

  /* Should be the last thing before execve() so that filters don't
   * need to handle anything above */
  if (seccomp_data != NULL &&
      prctl(PR_SET_SECCOMP, SECCOMP_MODE_FILTER, &seccomp_prog) != 0)
    die_with_error("prctl(PR_SET_SECCOMP)");

  if (setup_finished_pipe[1] != -1) {
    char data = 0;
    res = write_to_fd(setup_finished_pipe[1], &data, 1);
    /* Ignore res, if e.g. the parent died and closed setup_finished_pipe[0]
       we don't want to error out here */
  }
```
## 11. 启动目标进程
```c
  if (execvp(argv[0], argv) == -1) {
    if (setup_finished_pipe[1] != -1) {
      int saved_errno = errno;
      char data = 0;
      res = write_to_fd(setup_finished_pipe[1], &data, 1);
      errno = saved_errno;
      /* Ignore res, if e.g. the parent died and closed setup_finished_pipe[0]
         we don't want to error out here */
    }
    die_with_error("execvp %s", argv[0]);
  }

  return 0;
```