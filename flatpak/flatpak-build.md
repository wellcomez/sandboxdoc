---
date: 2023-04-13 13:54
title: flatpak-build
tags:
- flatpak-build
- flatpak
---

# flatpak-build


## flatpak-build-bundle — Create a single-file bundle from a local repository

### Synopsis
`flatpak build-bundle` [OPTION...] LOCATION FILENAME NAME [BRANCH]

### Description
Creates a single-file named FILENAME for the application (or runtime) named NAME in the repository at LOCATION . If a BRANCH is specified, this branch of the application is used.

The collection ID set on the repository at LOCATION (if set) will be used for the bundle.

Unless `--oci` is used, the format of the bundle file is that of an ostree static delta (against an empty base) with some flatpak specific metadata for the application icons and appdata.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--runtime`

Export a runtime instead of an application.

`--arch=ARCH`

The arch to create a bundle for. See **flatpak --supported-arches** for architectures supported by the host.

`--repo-url=URL`

The URL for the repository from which the application can be updated. Installing the bundle will automatically configure a remote for this URL.

`--runtime-repo=URL`

The URL for a `.flatpakrepo` file that contains the information about the repository that supplies the runtimes required by the app.

`--gpg-keys=FILE`

Add the GPG key from FILE (use - for stdin).

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings.

`--from-commit=COMMIT`

The OSTree commit to create a delta bundle from.

`--oci`

Export to an OCI image instead of a Flatpak bundle.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
**$ flatpak build-bundle /var/lib/flatpak/repo gnome-calculator.flatpak org.gnome.Calculator stable**

**$ flatpak build-bundle ~/.local/share/flatpak/repo gnome-calculator.flatpak org.gnome.Calculator stable**

##  flatpak-build-commit-from — Create new commits based on existing one (possibly from another repository)

### Synopsis
`flatpak build-commit-from` [OPTION...] DST-REPO DST-REF...

### Description
Creates new commits on the DST-REF branch in the DST-REPO , with the contents (and most of the metadata) taken from another branch, either from another repo, or from another branch in the same repository.

The collection ID set on DST-REPO (if set) will be used for the newly created commits.

This command is very useful when you want to maintain a branch with a clean history that has no unsigned or broken commits. For instance, you can import the head from a different repository from an automatic builder when you've verified that it worked. The new commit will have no parents or signatures from the autobuilder, and can be properly signed with the official key.

Any deltas that affect the original commit and that match parent commits in the destination repository are copied and rewritten for the new commit id.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--src-repo=SRC-REPO`

The (local) repository to pull the source branch from. Defaults to the destination repository.

`--src-ref=SRC-REF`

The branch to use as the source for the new commit. Defaults to the same as the destination ref, which is useful only if a different source repo has been specified.

`--extra-collection-id=COLLECTION-ID`

Add an extra collection-ref binding for this collection, in addition to whatever would normally be added due to the destination repository collection id. This option can be used multiple times.

`--subset=SUBSET`

Mark the commit to be included in the named subset. This will cause the commit to be put in the named subset summary (in addition to the main one), allowing users to see only this subset instead of the whole repo.

`--untrusted`

The source repostory is not trusted, all objects are copied (not hardlinked) and all checksums are verified.

`-s`, `--subject=SUBJECT`

One line subject for the commit message. If not specified, will be taken from the source commit.

`-b`, `--body=BODY`

Full description for the commit message. If not specified, will be taken from the source commit.

`--update-appstream`

Update the appstream branch after the build.

`--no-update-summary`

Don't update the summary file after the new commit is added. This means the repository will not be useful for serving over http until build-update-repo has been run. This is useful is you want to do multiple repo operations before finally updating the summary.

`--force`

Create new commit even if the content didn't change from the existing branch head.

`--disable-fsync`

Don't fsync when writing to the repository. This can result in data loss in exceptional situations, but can improve performance when working with temporary or test repositories.

`--gpg-sign=KEYID`

Sign the commit with this GPG key. This option can be used multiple times.

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings

`--end-of-life=REASON`

Mark build as end-of-life

`--end-of-life-rebase=OLDID=NEWID`

Mark new refs as end-of-life. Unlike `--end-of-life`, this one takes an ID that supersedes the current one. By the user's request, the application data may be preserved for the new application. Note, this is actually a prefix match, so if you say org.the.app=org.new.app, then something like org.the.app.Locale will be rebased to org.new.app.Locale.

`--timestamp=TIMESTAMP`

Override the timestamp of the commit. Use an ISO 8601 formatted date, or NOW for the current time

`--disable-fsync`

Don't fsync when writing to the repository. This can result in data loss in exceptional situations, but can improve performance when working with temporary or test repositories.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
To revert a commit to the commit before:

**$ flatpak build-commit-from --timestamp=NOW --src-ref=app/org.gnome.gedit/x86_64/master^ repo app/org.gnome.gedit/x86_64/master**

##  flatpak-build-export — Create a repository from a build directory

### Synopsis
`flatpak build-export` [OPTION...] LOCATION DIRECTORY [BRANCH]

### Description
Creates or updates a repository with an application build. LOCATION is the location of the repository. DIRECTORY must be a finalized build directory. If BRANCH is not specified, it is assumed to be "master".

If LOCATION exists, it is assumed to be an OSTree repository, otherwise a new OSTree repository is created at this location. The repository can be inspected with the **ostree** tool.

The contents of DIRECTORY are committed on the branch with name `app/APPNAME/ARCH/BRANCH`, where ARCH is the architecture of the runtime that the application is using. A commit filter is used to enforce that only the contents of the `files/` and `export/` subdirectories and the `metadata` file are included in the commit, anything else is ignored.

When exporting a flatpak to be published to the internet, `--collection-id=COLLECTION-ID` should be specified as a globally unique reverse DNS value to identify the collection of flatpaks this will be added to. Setting a globally unique collection ID allows the apps in the repository to be shared over peer to peer systems without needing further configuration.

The build-update-repo command should be used to update repository metadata whenever application builds are added to a repository.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`-s`, `--subject=SUBJECT`

One line subject for the commit message.

`-b`, `--body=BODY`

Full description for the commit message.

`--collection-id=COLLECTION-ID`

Set as the collection ID of the repository. Setting a globally unique collection ID allows the apps in the repository to be shared over peer to peer systems without needing further configuration. If exporting to an existing repository, the collection ID must match the existing configured collection ID for that repository.

`--subset=SUBSET`

Mark the commit to be included in the named subset. This will cause the commit to be put in the named subset summary (in addition to the main one), allowing users to see only this subset instead of the whole repo.

`--arch=ARCH`

Specify the architecture component of the branch to export. Only host compatible architectures can be specified; see **flatpak --supported-arches** for valid values.

`--exclude=PATTERN`

Exclude files matching PATTERN from the commit. This option can be used multiple times.

`--include=PATTERN`

Don't exclude files matching PATTERN from the commit, even if they match the `--exclude` patterns. This option can be used multiple times.

`--metadata=FILENAME`

Use the specified filename as metadata in the exported app instead of the default file (called `metadata`). This is useful if you want to commit multiple things from a single build tree, typically used in combination with `--files` and `--exclude`.

`--files=SUBDIR`

Use the files in the specified subdirectory as the file contents, rather than the regular `files` directory.

`--timestamp=DATE`

Use the specified ISO 8601 formatted date or NOW, for the current time, in the commit metadata and, if `--update-appstream` is used, the appstream data.

`--end-of-life=REASON`

Mark the build as end-of-life. REASON is a message that may be shown to users installing this build.

`--end-of-life-rebase=ID`

Mark the build as end-of-life. Unlike `--end-of-life`, this one takes an ID that supersedes the current one. By the user's request, the application data may be preserved for the new application.

`--disable-fsync`

Don't fsync when writing to the repository. This can result in data loss in exceptional situations, but can improve performance when working with temporary or test repositories.

`--update-appstream`

Update the appstream branch after the build.

`--no-update-summary`

Don't update the summary file after the new commit is added. This means the repository will not be useful for serving over http until build-update-repo has been run. This is useful is you want to do multiple repo operations before finally updating the summary.

`--gpg-sign=KEYID`

Sign the commit with this GPG key. This option can be used multiple times.

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings

`-r`, `--runtime`

Export a runtime instead of an app (this uses the usr subdir as files).

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
**$ flatpak build-export ~/repos/gnome-calculator/ ~/build/gnome-calculator/ org.gnome.Calculator**

```
Commit: 9d0044ea480297114d03aec85c3d7ae3779438f9d2cb69d717fb54237acacb8c
Metadata Total: 605
Metadata Written: 5
Content Total: 1174
Content Written: 1
Content Bytes Written: 305

```

##  flatpak-build-finish — Finalize a build directory

### Synopsis
`flatpak build-finish` [OPTION...] DIRECTORY

### Description
Finalizes a build directory, to prepare it for exporting. DIRECTORY is the name of the directory.

The result of this command is that desktop files, icons and D-Bus service files from the `files` subdirectory are copied to a new `export` subdirectory. In the `metadata` file, the command key is set in the [Application] group, and the supported keys in the [Environment] group are set according to the options.

As part of finalization you can also specify permissions that the app needs, using the various options specified below. Additionally during finalization the permissions from the runtime are inherited into the app unless you specify `--no-inherit-permissions`

You should review the exported files and the application metadata before creating and distributing an application bundle.

It is an error to run build-finish on a directory that has not been initialized as a build directory, or has already been finalized.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--command=COMMAND`

The command to use. If this option is not specified, the first executable found in `files/bin` is used.

Note that the command is used when the application is run via **flatpak run**, and does not affect what gets executed when the application is run in other ways, e.g. via the desktop file or D-Bus activation.

`--require-version=MAJOR.MINOR.MICRO`

Require this version or later of flatpak to install/update to this build.

`--share=SUBSYSTEM`

Share a subsystem with the host session. This updates the [Context] group in the metadata. SUBSYSTEM must be one of: network, ipc. This option can be used multiple times.

`--unshare=SUBSYSTEM`

Don't share a subsystem with the host session. This updates the [Context] group in the metadata. SUBSYSTEM must be one of: network, ipc. This option can be used multiple times.

`--socket=SOCKET`

Expose a well-known socket to the application. This updates the [Context] group in the metadata. SOCKET must be one of: x11, wayland, fallback-x11, pulseaudio, system-bus, session-bus, ssh-auth, pcsc, cups. This option can be used multiple times.

The fallback-x11 option makes the X11 socket available only if there is no Wayland socket. This option was introduced in 0.11.3. To support older Flatpak releases, specify both x11 and fallback-x11. The fallback-x11 option takes precedence when both are supported.

`--nosocket=SOCKET`

Don't expose a well known socket to the application. This updates the [Context] group in the metadata. SOCKET must be one of: x11, wayland, fallback-x11, pulseaudio, system-bus, session-bus, ssh-auth, pcsc, cups. This option can be used multiple times.

`--device=DEVICE`

Expose a device to the application. This updates the [Context] group in the metadata. DEVICE must be one of: dri, kvm, shm, all. This option can be used multiple times.

`--nodevice=DEVICE`

Don't expose a device to the application. This updates the [Context] group in the metadata. DEVICE must be one of: dri, kvm, shm, all. This option can be used multiple times.

`--allow=FEATURE`

Allow access to a specific feature. This updates the [Context] group in the metadata. FEATURE must be one of: devel, multiarch, bluetooth, canbus, per-app-dev-shm. This option can be used multiple times.

The `devel` feature allows the application to access certain syscalls such as `ptrace()`, and `perf_event_open()`.

The `multiarch` feature allows the application to execute programs compiled for an ABI other than the one supported natively by the system. For example, for the `x86_64` architecture, 32-bit `x86` binaries will be allowed as well.

The `bluetooth` feature allows the application to use bluetooth (AF_BLUETOOTH) sockets. Note, for bluetooth to fully work you must also have network access.

The `canbus` feature allows the application to use canbus (AF_CAN) sockets. Note, for this work you must also have network access.

The `per-app-dev-shm` feature shares a single instance of `/dev/shm` between the application, any unrestricted subsandboxes that it creates, and any other instances of the application that are launched while it is running.

`--disallow=FEATURE`

Disallow access to a specific feature. This updates the [Context] group in the metadata. FEATURE must be one of: devel, multiarch, bluetooth, canbus, per-app-dev-shm. This option can be used multiple times.

`--filesystem=FS`

Allow the application access to a subset of the filesystem. This updates the [Context] group in the metadata. FS can be one of: home, host, host-os, host-etc, xdg-desktop, xdg-documents, xdg-download, xdg-music, xdg-pictures, xdg-public-share, xdg-templates, xdg-videos, xdg-run, xdg-config, xdg-cache, xdg-data, an absolute path, or a homedir-relative path like ~/dir or paths relative to the xdg dirs, like xdg-download/subdir. The optional :ro suffix indicates that the location will be read-only. The optional :create suffix indicates that the location will be read-write and created if it doesn't exist. This option can be used multiple times. See the "[Context] filesystems" list in [flatpak-metadata(5)](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-metadata) for details of the meanings of these filesystems.

`--nofilesystem=FILESYSTEM`

Remove access to the specified subset of the filesystem from the application. This overrides to the Context section from the application metadata. FILESYSTEM can be one of: home, host, host-os, host-etc, xdg-desktop, xdg-documents, xdg-download, xdg-music, xdg-pictures, xdg-public-share, xdg-templates, xdg-videos, an absolute path, or a homedir-relative path like ~/dir. This option can be used multiple times.

`--add-policy=SUBSYSTEM.KEY=VALUE`

Add generic policy option. For example, "--add-policy=subsystem.key=v1 --add-policy=subsystem.key=v2" would map to this metadata:

```
[Policy subsystem]
key=v1;v2;

```

This option can be used multiple times.

`--remove-policy=SUBSYSTEM.KEY=VALUE`

Remove generic policy option. This option can be used multiple times.

`--env=VAR=VALUE`

Set an environment variable in the application. This updates the [Environment] group in the metadata. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--unset-env=VAR`

Unset an environment variable in the application. This updates the unset-environment entry in the [Context] group of the metadata. This option can be used multiple times.

``--env-fd=_`FD`_``

Read environment variables from the file descriptor _`FD`_, and set them as if via `--env`. This can be used to avoid environment variables and their values becoming visible to other users.

Each environment variable is in the form _`VAR`_\=_`VALUE`_ followed by a zero byte. This is the same format used by `env -0` and `/proc/*/environ`.

`--own-name=NAME`

Allow the application to own the well known name NAME on the session bus. If NAME ends with .\*, it allows the application to own all matching names. This updates the [Session Bus Policy] group in the metadata. This option can be used multiple times.

`--talk-name=NAME`

Allow the application to talk to the well known name NAME on the session bus. If NAME ends with .\*, it allows the application to talk to all matching names. This updates the [Session Bus Policy] group in the metadata. This option can be used multiple times.

`--system-own-name=NAME`

Allow the application to own the well known name NAME on the system bus. If NAME ends with .\*, it allows the application to own all matching names. This updates the [System Bus Policy] group in the metadata. This option can be used multiple times.

`--system-talk-name=NAME`

Allow the application to talk to the well known name NAME on the system bus. If NAME ends with .\*, it allows the application to talk to all matching names. This updates the [System Bus Policy] group in the metadata. This option can be used multiple times.

`--persist=FILENAME`

If the application doesn't have access to the real homedir, make the (homedir-relative) path FILENAME a bind mount to the corresponding path in the per-application directory, allowing that location to be used for persistent data. This updates the [Context] group in the metadata. This option can be used multiple times.

`--runtime=RUNTIME`, `--sdk=SDK`

Change the runtime or sdk used by the app to the specified partial ref. Unspecified parts of the ref are taken from the old values or defaults.

`--metadata=GROUP=KEY[=VALUE]`

Set a generic key in the metadata file. If value is left out it will be set to "true".

`--extension=NAME=VARIABLE[=VALUE]`

Add extension point info. See the documentation for [flatpak-metadata(5)](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-metadata) for the possible values of _`VARIABLE`_ and _`VALUE`_.

`--remove-extension=NAME`

Remove extension point info.

`--extension-priority=VALUE`

Set the priority (library override order) of the extension point. Only useful for extensions. 0 is the default, and higher value means higher priority.

`--extra-data=NAME:SHA256:DOWNLOAD-SIZE:INSTALL-SIZE:URL`

Adds information about extra data uris to the app. These will be downloaded and verified by the client when the app is installed and placed in the `/app/extra` directory. You can also supply an `/app/bin/apply_extra` script that will be run after the files are downloaded.

`--no-exports`

Don't look for exports in the build.

`--no-inherit-permissions`

Don't inherit runtime permissions in the app.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
**$ flatpak build-finish /build/my-app --socket=x11 --share=ipc**

```
Exporting share/applications/gnome-calculator.desktop
Exporting share/dbus-1/services/org.gnome.Calculator.SearchProvider.service
More than one executable
Using gcalccmd as command
Please review the exported files and the metadata

```

##  flatpak-build-import-bundle — Import a file bundle into a local repository

### Synopsis
`flatpak build-import-bundle` [OPTION...] LOCATION FILENAME

### Description
Imports a bundle from a file named FILENAME into the repository at LOCATION .

The format of the bundle file is that generated by build-bundle.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

`--ref=REF`

Override the ref specified in the bundle.

`--oci`

Import an OCI image instead of a Flatpak bundle.

`--update-appstream`

Update the appstream branch after the build.

`--no-update-summary`

Don't update the summary file after the new commit is added. This means the repository will not be useful for serving over http until build-update-repo has been run. This is useful is you want to do multiple repo operations before finally updating the summary.

`--gpg-sign=KEYID`

Sign the commit with this GPG key. This option can be used multiple times.

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings

##  flatpak-build-init — Initialize a build directory

### Synopsis
`flatpak build-init` [OPTION...] DIRECTORY APPNAME SDK RUNTIME [BRANCH]

### Description
Initializes a separate build directory. DIRECTORY is the name of the directory. APPNAME is the application id of the app that will be built. SDK and RUNTIME specify the sdk and runtime that the application should be built against and run in. BRANCH specify the version of sdk and runtime

Initializes a directory as build directory which can be used as target directory of **flatpak build**. It creates a `metadata` inside the given directory. Additionally, empty `files` and `var` subdirectories are created.

It is an error to run build-init on a directory that has already been initialized as a build directory.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--arch=ARCH`

The architecture to use. See **flatpak --supported-arches** for architectures supported by the host.

`-v`, `--var=RUNTIME`

Initialize var from the named runtime.

`-w`, `--writable-sdk`

Initialize /usr with a copy of the sdk, which is writable during flatpak build. This can be used if you need to install build tools in /usr during the build. This is stored in the `usr` subdirectory of the app dir, but will not be part of the final app.

`--tag=TAG`

Add a tag to the metadata file. This option can be used multiple times.

`--sdk-extension=EXTENSION`

When using `--writable-sdk`, in addition to the sdk, also install the specified extension. This option can be used multiple times.

`--extension=NAME=VARIABLE[=VALUE]`

Add extension point info.

`--sdk-dir`

Specify a custom subdirectory to use instead of `usr` for `--writable-sdk`.

`--update`

Re-initialize the sdk and var, don't fail if already initialized.

`--base=APP`

Initialize the application with files from another specified application.

`--base-version=VERSION`

Specify the version to use for `--base`. If not specified, will default to "master".

`--base-extension=EXTENSION`

When using `--base`, also install the specified extension from the app. This option can be used multiple times.

`--type=TYPE`

This can be used to build different types of things. The default is "app" which is a regular app, but "runtime" creates a runtime based on an existing runtime, and "extension" creates an extension for an app or runtime.

`--extension-tag=EXTENSION_TAG`

If building an extension, the tag to use when searching for the mount point of the extension.

`--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
**$ flatpak build-init /build/my-app org.example.myapp org.gnome.Sdk org.gnome.Platform 3.36**

##  flatpak-build-sign — Sign an application or runtime

### Synopsis
`flatpak build-sign` [OPTION...] LOCATION ID [BRANCH]

### Description
Signs the commit for a specified application or runtime in a local repository. LOCATION is the location of the repository. ID is the name of the application, or runtime if --runtime is specified. If BRANCH is not specified, it is assumed to be "master".

Applications can also be signed during build-export, but it is sometimes useful to add additional signatures later.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--gpg-sign=KEYID`

Sign the commit with this GPG key. This option can be used multiple times.

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings

`--runtime`

Sign a runtime instead of an app.

`--arch=ARCH`

The architecture to use. See **flatpak --supported-arches** for architectures supported by the host.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

### Examples
**$ flatpak build-sign --gpg-sign=D8BA6573DDD2418027736F1BC33B315E53C1E9D6 /some/repo org.my.App**

##  flatpak-build-update-repo — Create a repository from a build directory

### Synopsis
`flatpak build-update-repo` [OPTION...] LOCATION

### Description
Updates repository metadata for the repository at LOCATION . This command generates an OSTree summary file that lists the contents of the repository. The summary is used by flatpak remote-ls and other commands to display the contents of remote repositories.

After this command, LOCATION can be used as the repository location for flatpak remote-add, either by exporting it over http, or directly with a file: url.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`--redirect-url=URL`

Redirect this repo to a new URL.

`--title=TITLE`

A title for the repository, e.g. for display in a UI. The title is stored in the repository summary.

`--comment=COMMENT`

A single-line comment for the remote, e.g. for display in a UI. The comment is stored in the repository summary.

`--description=DESCRIPTION`

A full-paragraph description for the remote, e.g. for display in a UI. The description is stored in the repository summary.

`--homepage=URL`

URL for a website for the remote, e.g. for display in a UI. The url is stored in the repository summary.

`--icon=URL`

URL for an icon for the remote, e.g. for display in a UI. The url is stored in the repository summary.

`--default-branch=BRANCH`

A default branch for the repository, mainly for use in a UI.

`--gpg-import=FILE`

Import a new default GPG public key from the given file.

`--collection-id=COLLECTION-ID`

The globally unique identifier of the remote repository, to allow mirrors to be grouped. This must be set to a globally unique reverse DNS string if the repository is to be made publicly available. If a collection ID is already set on an existing repository, this will update it. If not specified, the existing collection ID will be left unchanged.

`--deploy-collection-id`

Deploy the collection ID (set using `--collection-id`) in the static remote configuration for all clients. This is irrevocable once published in a repository. Use it to decide when to roll out a collection ID to users of an existing repository. If constructing a new repository which has a collection ID, you should typically always pass this option.

`--deploy-sideload-collection-id`

This is similar to --deploy-collection-id, but it only applies the deploy to clients newer than flatpak 1.7 which supports the new form of sideloads.

`--gpg-sign=KEYID`

Sign the commit with this GPG key. This option can be used multiple times.

`--gpg-homedir=PATH`

GPG Homedir to use when looking for keyrings

`--generate-static-deltas`

Generate static deltas for all references. This generates from-empty and delta static files that allow for faster download.

`--static-delta-jobs=NUM-JOBS`

Limit the number of parallel jobs creating static deltas. The default is the number of cpus.

`--static-delta-ignore-ref=PATTERN`

Don't generate deltas for runtime or application id matching this pattern. For instance, --static-delta-ignore-ref=\*.Sources means there will not be any deltas for source refs.

`--prune`

Remove unreferenced objects in repo.

`--prune-depth`

Only keep at most this number of old versions for any particular ref. Default is -1 which means infinite.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

##  flatpak-build — Build in a directory

### Synopsis
`flatpak build` [OPTION...] DIRECTORY [COMMAND [ARG...]]

### Description
Runs a build command in a directory. DIRECTORY must have been initialized with **flatpak build-init**.

The sdk that is specified in the `metadata` file in the directory is mounted at `/usr` and the `files` and `var` subdirectories are mounted at `/app` and `/var`, respectively. They are writable, and their contents are preserved between build commands, to allow accumulating build artifacts there.

### Options
The following options are understood:

`-h`, `--help`

Show help options and exit.

`-v`, `--verbose`

Print debug information during command processing.

`--ostree-verbose`

Print OSTree debug information during command processing.

`-r`, `--runtime`

Use the non-devel runtime that is specified in the application metadata instead of the devel runtime.

`-p`, `--die-with-parent`

Kill the build process and all children when the launching process dies.

`--bind-mount=DEST=SOURCE`

Add a custom bind mount in the build namespace. Can be specified multiple times.

`--build-dir=PATH`

Start the build in this directory (default is in the current directory).

`--share=SUBSYSTEM`

Share a subsystem with the host session. This overrides the Context section from the application metadata. SUBSYSTEM must be one of: network, ipc. This option can be used multiple times.

`--unshare=SUBSYSTEM`

Don't share a subsystem with the host session. This overrides the Context section from the application metadata. SUBSYSTEM must be one of: network, ipc. This option can be used multiple times.

`--socket=SOCKET`

Expose a well-known socket to the application. This overrides to the Context section from the application metadata. SOCKET must be one of: x11, wayland, fallback-x11, pulseaudio, system-bus, session-bus, ssh-auth, pcsc, cups. This option can be used multiple times.

`--nosocket=SOCKET`

Don't expose a well-known socket to the application. This overrides to the Context section from the application metadata. SOCKET must be one of: x11, wayland, fallback-x11, pulseaudio, system-bus, session-bus, ssh-auth, pcsc, cups. This option can be used multiple times.

`--device=DEVICE`

Expose a device to the application. This overrides to the Context section from the application metadata. DEVICE must be one of: dri, kvm, shm, all. This option can be used multiple times.

`--nodevice=DEVICE`

Don't expose a device to the application. This overrides to the Context section from the application metadata. DEVICE must be one of: dri, kvm, shm, all. This option can be used multiple times.

`--allow=FEATURE`

Allow access to a specific feature. This updates the [Context] group in the metadata. FEATURE must be one of: devel, multiarch, bluetooth, canbus, per-app-dev-shm. This option can be used multiple times.

See [flatpak-build-finish(1)](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-build-finish) for the meaning of the various features.

`--disallow=FEATURE`

Disallow access to a specific feature. This updates the [Context] group in the metadata. FEATURE must be one of: devel, multiarch, bluetooth, canbus, per-app-dev-shm. This option can be used multiple times.

`--filesystem=FILESYSTEM[:ro|:create]`

Allow the application access to a subset of the filesystem. This overrides to the Context section from the application metadata. FILESYSTEM can be one of: home, host, host-os, host-etc, xdg-desktop, xdg-documents, xdg-download, xdg-music, xdg-pictures, xdg-public-share, xdg-templates, xdg-videos, xdg-run, xdg-config, xdg-cache, xdg-data, an absolute path, or a homedir-relative path like ~/dir or paths relative to the xdg dirs, like xdg-download/subdir. The optional :ro suffix indicates that the location will be read-only. The optional :create suffix indicates that the location will be read-write and created if it doesn't exist. This option can be used multiple times. See the "[Context] filesystems" list in [flatpak-metadata(5)](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-metadata) for details of the meanings of these filesystems.

`--nofilesystem=FILESYSTEM`

Remove access to the specified subset of the filesystem from the application. This overrides to the Context section from the application metadata. FILESYSTEM can be one of: home, host, host-os, host-etc, xdg-desktop, xdg-documents, xdg-download, xdg-music, xdg-pictures, xdg-public-share, xdg-templates, xdg-videos, an absolute path, or a homedir-relative path like ~/dir. This option can be used multiple times.

`--with-appdir`

Expose and configure access to the per-app storage directory in `$HOME/.var/app`. This is not normally useful when building, but helps when testing built apps.

`--add-policy=SUBSYSTEM.KEY=VALUE`

Add generic policy option. For example, "--add-policy=subsystem.key=v1 --add-policy=subsystem.key=v2" would map to this metadata:

```
[Policy subsystem]
key=v1;v2;

```

This option can be used multiple times.

`--remove-policy=SUBSYSTEM.KEY=VALUE`

Remove generic policy option. This option can be used multiple times.

`--env=VAR=VALUE`

Set an environment variable in the application. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--unset-env=VAR`

Unset an environment variable in the application. This overrides the unset-environment entry in the [Context] group of the metadata, and the [Environment] group. This option can be used multiple times.

``--env-fd=_`FD`_``

Read environment variables from the file descriptor _`FD`_, and set them as if via `--env`. This can be used to avoid environment variables and their values becoming visible to other users.

Each environment variable is in the form _`VAR`_\=_`VALUE`_ followed by a zero byte. This is the same format used by `env -0` and `/proc/*/environ`.

`--own-name=NAME`

Allow the application to own the well-known name NAME on the session bus. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--talk-name=NAME`

Allow the application to talk to the well-known name NAME on the session bus. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--system-own-name=NAME`

Allow the application to own the well-known name NAME on the system bus. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--system-talk-name=NAME`

Allow the application to talk to the well-known name NAME on the system bus. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--persist=FILENAME`

If the application doesn't have access to the real homedir, make the (homedir-relative) path FILENAME a bind mount to the corresponding path in the per-application directory, allowing that location to be used for persistent data. This overrides to the Context section from the application metadata. This option can be used multiple times.

`--sdk-dir=DIR`

Normally if there is a `usr` directory in the build dir, this is used for the runtime files (this can be created by `--writable-sdk` or `--type=runtime` arguments to build-init). If you specify `--sdk-dir`, this directory will be used instead. Use this if you passed `--sdk-dir` to build-init.

`--readonly`

Mount the normally writable destination directories read-only. This can be useful if you want to run something in the sandbox but guarantee that it doesn't affect the build results. For example tests.

`--metadata=FILE`

Use the specified filename as metadata in the exported app instead of the default file (called `metadata`). This is useful if you build multiple things from a single build tree (such as both a platform and a sdk).

`--log-session-bus`

Log session bus traffic. This can be useful to see what access you need to allow in your D-Bus policy.

`--log-system-bus`

Log system bus traffic. This can be useful to see what access you need to allow in your D-Bus policy.

### Examples
**$ flatpak build /build/my-app rpmbuild my-app.src.rpm**
