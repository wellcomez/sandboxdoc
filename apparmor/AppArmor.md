---
layout: post
tags:
- apparmor
title: "AppArmor"

---

# AppArmor

Related articles

-   [Security](https://wiki.archlinux.org/title/Security "Security")
-   [SELinux](https://wiki.archlinux.org/title/SELinux "SELinux")
-   [TOMOYO Linux](https://wiki.archlinux.org/title/TOMOYO_Linux "TOMOYO Linux")

[AppArmor](https://apparmor.net/) is a [Mandatory Access Control](https://wiki.archlinux.org/title/Mandatory_Access_Control "Mandatory Access Control") (MAC) system, implemented upon the [Linux Security Modules](https://en.wikipedia.org/wiki/Linux_Security_Modules "wikipedia:Linux Security Modules") (LSM).

AppArmor, like most other LSMs, supplements rather than replaces the default Discretionary Access Control (DAC). As such it is impossible to grant a process more privileges than it had in the first place.

Ubuntu, SUSE and a number of other distributions use it by default. RHEL (and its variants) use SELinux which requires good userspace integration to work properly. SELinux attaches labels to all files, processes and objects and is therefore very flexible. However configuring SELinux is considered to be very complicated and requires a supported filesystem. AppArmor on the other hand works using file paths and its configuration can be easily adapted.

AppArmor proactively protects the operating system and applications from external or internal threats and even zero-day attacks by enforcing a specific rule set on a per application basis. Security policies completely define what system resources individual applications can access, and with what privileges. Access is denied by default if no profile says otherwise. A few default policies are included with AppArmor and using a combination of advanced static analysis and learning-based tools, AppArmor policies for even very complex applications can be deployed successfully in a matter of hours.

Every breach of policy triggers a message in the system log, and AppArmor can be configured to notify users with real-time violation warnings popping up on the desktop.

## Contents

-   [1Installation](https://wiki.archlinux.org/title/AppArmor#Installation)
    -   [1.1Custom kernel](https://wiki.archlinux.org/title/AppArmor#Custom_kernel)
-   [2Usage](https://wiki.archlinux.org/title/AppArmor#Usage)
    -   [2.1Display current status](https://wiki.archlinux.org/title/AppArmor#Display_current_status)
    -   [2.2Parsing profiles](https://wiki.archlinux.org/title/AppArmor#Parsing_profiles)
    -   [2.3Disabling loading](https://wiki.archlinux.org/title/AppArmor#Disabling_loading)
-   [3Configuration](https://wiki.archlinux.org/title/AppArmor#Configuration)
    -   [3.1Auditing and generating profiles](https://wiki.archlinux.org/title/AppArmor#Auditing_and_generating_profiles)
    -   [3.2Understanding profiles](https://wiki.archlinux.org/title/AppArmor#Understanding_profiles)
-   [4Tips and tricks](https://wiki.archlinux.org/title/AppArmor#Tips_and_tricks)
    -   [4.1Get desktop notification on DENIED actions](https://wiki.archlinux.org/title/AppArmor#Get_desktop_notification_on_DENIED_actions)
    -   [4.2Speed-up AppArmor start by caching profiles](https://wiki.archlinux.org/title/AppArmor#Speed-up_AppArmor_start_by_caching_profiles)
-   [5Troubleshooting](https://wiki.archlinux.org/title/AppArmor#Troubleshooting)
    -   [5.1Failing to start Samba SMB/CIFS server](https://wiki.archlinux.org/title/AppArmor#Failing_to_start_Samba_SMB/CIFS_server)
    -   [5.2No events catched by aa-logprof](https://wiki.archlinux.org/title/AppArmor#No_events_catched_by_aa-logprof)
-   [6See also](https://wiki.archlinux.org/title/AppArmor#See_also)

## Installation

AppArmor is available in all [officially supported kernels](https://wiki.archlinux.org/title/Kernel#Officially_supported_kernels "Kernel").

[Install](https://wiki.archlinux.org/title/Install "Install") [apparmor](https://archlinux.org/packages/?name=apparmor) for userspace tools and libraries to control AppArmor. To load all AppArmor profiles on startup, [enable](https://wiki.archlinux.org/title/Enable "Enable") `apparmor.service`.

To enable AppArmor as default security model on every boot, set the following [kernel parameter](https://wiki.archlinux.org/title/Kernel_parameter "Kernel parameter"):

lsm=landlock,lockdown,yama,integrity,apparmor,bpf

**Note:** The `lsm=` kernel parameter sets the initialization order of Linux security modules. The kernel's configured `lsm=` value can be found with `zgrep CONFIG_LSM= /proc/config.gz` and the current value with `cat /sys/kernel/security/lsm`.

-   Make sure that `apparmor` is the first "major" module in the list.[[1]](https://docs.kernel.org/admin-guide/LSM/index.html) Examples of valid values and their order can be found in [security/Kconfig](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/security/Kconfig).
-   `capability` should be omitted from `lsm=` as it will always get included automatically.

### Custom kernel

When [compiling the kernel](https://wiki.archlinux.org/title/Kernels#Compilation "Kernels"), it is required to set at least the following options:

~~~
CONFIG_SECURITY_APPARMOR=y
CONFIG_AUDIT=y
~~~

To enable the AppArmor Linux security model by default and omit the need to set kernel parameters, additionally set the `CONFIG_LSM` option and specify `apparmor` as the first "major" module in the list:

CONFIG_LSM="landlock,lockdown,yama,integrity,apparmor,bpf"

## Usage

### Display current status

To test if AppArmor has been correctly enabled:
~~~

$ aa-enabled

Yes
~~~

To display the current loaded status use [aa-status(8)](https://man.archlinux.org/man/aa-status.8):

~~~
# aa-status

apparmor module is loaded.
44 profiles are loaded.
44 profiles are in enforce mode.
 ...
0 profiles are in complain mode.
0 processes have profiles defined.
0 processes are in enforce mode.
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
~~~

### Parsing profiles

To load (enforce or complain), unload, reload, cache and stat profiles use `apparmor_parser`. The default action (`-a`) is to load a new profile in enforce mode, loading it in complain mode is possible using the `-C` switch, in order to overwrite an existing profile use the `-r` option and to remove a profile use `-R`. Each action may also apply to multiple profiles. Refer to [apparmor_parser(8)](https://man.archlinux.org/man/apparmor_parser.8) man page for more information.

### Disabling loading

Disable AppArmor by unloading all profiles for the current session:

~~~
# aa-teardown 
~~~

To prevent AppArmor profiles from loading at the next boot [disable](https://wiki.archlinux.org/title/Disable "Disable") `apparmor.service`. To prevent the kernel from loading AppArmor, remove the `lsm=` [kernel parameter](https://wiki.archlinux.org/title/Kernel_parameter "Kernel parameter") that was added when [setting up AppArmor](https://wiki.archlinux.org/title/AppArmor#Installation).

## Configuration

### Auditing and generating profiles

To create new profiles the [Audit framework](https://wiki.archlinux.org/title/Audit_framework "Audit framework") should be running. This is because Arch Linux adopted [systemd](https://wiki.archlinux.org/title/Systemd "Systemd") and does not do kernel logging to file by default. AppArmor can grab kernel audit logs from the userspace auditd daemon, allowing you to build a profile.

New AppArmor profiles can be created by utilizing [aa-genprof(8)](https://man.archlinux.org/man/aa-genprof.8) or [aa-autodep(8)](https://man.archlinux.org/man/aa-autodep.8). The profile is first created in _complain mode_: in this mode policy violations are only reported but not enforced. The rules are interactively created by the [aa-logprof(8)](https://man.archlinux.org/man/aa-logprof.8) tool available in [apparmor](https://archlinux.org/packages/?name=apparmor) package. Finally the profile should be set into _enforce mode_ with [aa-enforce(8)](https://man.archlinux.org/man/aa-enforce.8). In this mode the policy defined by the rules in the respecting profile are enforced. If necessary, additional rules can be added by repeatedly executing [aa-logprof(8)](https://man.archlinux.org/man/aa-logprof.8), or the profile can be set back to complain mode with [aa-complain(8)](https://man.archlinux.org/man/aa-complain.8). Detailed guide about using those tools is available at [AppArmor wiki - Profiling with tools](https://gitlab.com/apparmor/apparmor/wikis/Profiling_with_tools).

Note that [aa-logprof(8)](https://man.archlinux.org/man/aa-logprof.8) also offers _deny_ rules which are actually not strictly necessary as according to the basic AppArmor logic everything is forbidden what is not explicitly allowed by a rule. However, _deny_ rules serve two purposes:

1.  _deny_ rules take precedence over _allow_ rules. They are often used in many [abstractions](https://man.archlinux.org/man/extra/apparmor/apparmor.d.5.en##include_mechanism) located in `/etc/apparmor.d/abstractions` in order to block any access to important folders/files. This makes sure that inadvertently created allow rules do not make a profile too permissive.
2.  _deny_ rules silence logging and make subsequent runs of _aa-logprof_ less noisy. It is important to keep in mind that _deny_ rules are enforced also in _complain mode_ - hence, if an application does not work properly even in complain mode it should be checked if a deny rule in the profile or in one of the included abstractions is the culprit.

Alternatively profiles may be also created manually, see guide available at [AppArmor wiki - Profiling by hand](https://gitlab.com/apparmor/apparmor/wikis/Profiling_by_hand).

In addition to the default profiles in `/etc/apparmor.d/`, there are more predefined profiles in `/usr/share/apparmor/extra-profiles/`. Note that those are not necessarily deemed production-ready, so manual intervention or usage of [aa-logprof(8)](https://man.archlinux.org/man/aa-logprof.8) may be required.

### Understanding profiles

Profiles are human readable text files residing under `/etc/apparmor.d/` describing how binaries should be treated when executed. A basic profile looks similar to this:
~~~

/etc/apparmor.d/usr.bin.test

#include <tunables/global>

profile test /usr/lib/test/test_binary {
    #include <abstractions/base>

    # Main libraries and plugins
    /usr/share/TEST/** r,
    /usr/lib/TEST/** rm,

    # Configuration files and logs
    @{HOME}/.config/ r,
    @{HOME}/.config/TEST/** rw,
}
~~~

Strings preceded by a `@` symbol are variables defined by abstractions (`/etc/apparmor.d/abstractions/`), tunables (`/etc/apparmor.d/tunables/`) or by the profile itself. `#include` includes other profile-files directly. Paths followed by a set of characters are [access permissions](https://man.archlinux.org/man/extra/apparmor/apparmor.d.5.en#Access_Modes). Pattern matching is done using [AppArmor's globbing syntax](https://man.archlinux.org/man/extra/apparmor/apparmor.d.5.en#Globbing).

Most common use cases are covered by the following statements:

-   `r` — read: read data
-   `w` — write: create, delete, write to a file and extend it
-   `m` — memory map executable: memory map a file executable
-   `x` — execute: execute file; needs to be preceded by a [qualifier](https://gitlab.com/apparmor/apparmor/wikis/AppArmor_Core_Policy_Reference#execute-rules)

Remember that those permission do not allow binaries to exceed the permission dictated by the Discretionary Access Control (DAC).

This is merely a short overview, for a more detailed guide be sure to have a look at the [apparmor.d(5)](https://man.archlinux.org/man/apparmor.d.5) man page and [documentation](https://gitlab.com/apparmor/apparmor/wikis/AppArmor_Core_Policy_Reference).

## Tips and tricks

### Get desktop notification on DENIED actions

The notification daemon displays desktop notifications whenever AppArmor denies a program access. To automatically start _aa-notify_ daemon on login follow below steps:

Install the [Audit framework](https://wiki.archlinux.org/title/Audit_framework "Audit framework") and enable and start the userspace Linux Audit daemon. Allow your desktop user to read audit logs in `/var/log/audit` by adding it to `audit` [user group](https://wiki.archlinux.org/title/User_group "User group"):
~~~
# groupadd -r audit
# gpasswd -a _user_ audit
~~~

Add `audit` group to `auditd.conf`:
~~~

/etc/audit/auditd.conf

log_group = audit
~~~

**Tip:** You may use other already existing system groups like `wheel` or `adm`.

[Install](https://wiki.archlinux.org/title/Install "Install") [python-notify2](https://archlinux.org/packages/?name=python-notify2) and [python-psutil](https://archlinux.org/packages/?name=python-psutil).

Create a [desktop launcher](https://wiki.archlinux.org/title/Desktop_launcher "Desktop launcher") with the below content:

~~~
~/.config/autostart/apparmor-notify.desktop

[Desktop Entry]
Type=Application
Name=AppArmor Notify
Comment=Receive on screen notifications of AppArmor denials
TryExec=aa-notify
Exec=aa-notify -p -s 1 -w 60 -f /var/log/audit/audit.log
StartupNotify=false
NoDisplay=true
~~~

Reboot and check if the `aa-notify` process is running:

~~~
$ pgrep -ax aa-notify
~~~

**Note:** Depending on your specific system configuration there could be A LOT of notifications displayed.

For more information, see [aa-notify(8)](https://man.archlinux.org/man/aa-notify.8).

### Speed-up AppArmor start by caching profiles

Since AppArmor has to translate the configured profiles into a binary format it may significantly increase the boot time. You can check current AppArmor startup time with:

~~~
$ systemd-analyze blame | grep apparmor
~~~

To enable caching AppArmor profiles, uncomment:

~~~
/etc/apparmor/parser.conf
~~~

## Turn creating/updating of the cache on by default
write-cache

To change default cache location add:

~~~
/etc/apparmor/parser.conf

cache-loc=/path/to/location
~~~

**Note:** Since 2.13.1 default cache location is `/var/cache/apparmor/`, previously it was `/etc/apparmor.d/cache.d/`.

Reboot and check AppArmor startup time again to see improvement:

$ systemd-analyze blame | grep apparmor

## Troubleshooting

### Failing to start Samba SMB/CIFS server

See [Samba#Permission issues on AppArmor](https://wiki.archlinux.org/title/Samba#Permission_issues_on_AppArmor "Samba").

### No events catched by aa-logprof

[Audit framework](https://wiki.archlinux.org/title/Audit_framework "Audit framework") logs may have the special character `0x1d`[[2]](https://github.com/linux-audit/audit-userspace/issues/3). There is a [AppArmor bug report](https://gitlab.com/apparmor/apparmor/-/issues/271), but a workaround is to run:

~~~
# aa-logprof -f <(sed 's/\x1d.*//' < /var/log/audit/audit.log)
~~~

## See also

-   [Wikipedia:AppArmor](https://en.wikipedia.org/wiki/AppArmor "wikipedia:AppArmor")
-   [AppArmor wiki](https://gitlab.com/apparmor/apparmor/wikis/home)
-   [AppArmor Core Policy Reference](https://gitlab.com/apparmor/apparmor/wikis/AppArmor_Core_Policy_Reference) — Detailed description of available options in a profile
-   [Ubuntu Tutorial](https://ubuntuforums.org/showthread.php?t=1008906) — General overview of available utilities and profile creation
-   [Ubuntu Wiki](https://help.ubuntu.com/community/AppArmor) — Basic command overview
-   [AppArmor Versions](https://gitlab.com/apparmor/apparmor/wikis/AppArmor_versions) — Version overview and links to the respective release notes
-   [Kernel Interfaces](https://gitlab.com/apparmor/apparmor/wikis/Kernel_interfaces) — Low level interfaces to the AppArmor kernel module
-   [wikipedia:Linux Security Modules](https://en.wikipedia.org/wiki/Linux_Security_Modules "wikipedia:Linux Security Modules") — Linux kernel module on which basis AppArmor is build upon
-   [AppArmor in openSUSE Security Guide](https://doc.opensuse.org/documentation/leap/security/single-html/book-security/index.html#part-apparmor)

[Categories](https://wiki.archlinux.org/title/Special:Categories "Special:Categories"): 

-   [Access control](https://wiki.archlinux.org/title/Category:Access_control "Category:Access control")
-   [Kernel](https://wiki.archlinux.org/title/Category:Kernel "Category:Kernel")

-   This page was last edited on 18 November 2022, at 10:15.
-   Content is available under [GNU Free Documentation License 1.3 or later](https://www.gnu.org/copyleft/fdl.html) unless otherwise noted.

-   [Privacy policy](https://terms.archlinux.org/docs/privacy-policy/ "archlinux-service-agreements:privacy-policy")
-   [About ArchWiki](https://wiki.archlinux.org/title/ArchWiki:About "ArchWiki:About")
-   [Disclaimers](https://wiki.archlinux.org/title/ArchWiki:General_disclaimer "ArchWiki:General disclaimer")