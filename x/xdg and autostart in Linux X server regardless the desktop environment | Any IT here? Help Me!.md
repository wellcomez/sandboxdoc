---
title: "xdg and autostart in Linux X server regardless the desktop environment | Any IT here? Help Me!"
layout: post
---


# xdg and autostart in Linux X server regardless the desktop environment | Any IT here? Help Me!
<a style="text-decoration:underline" href="https://ahelpme.com/linux/tips/xdg-and-autostart-in-linux-x-server-regardless-the-desktop-environment/">xdg and autostart in Linux X server regardless the desktop environment | Any IT here? Help Me!</a><br>

There is a tool **xdg**, which manages application integration with the different GUI Desktops in the Linux world. One of the features it offers is to autostart an application when the X window system starts and it is perfectly normal to have a bunch of running programs that cannot be found in the Windowing manager settings like **KDE System Settings -> Autostart**, **GNOME Tweak tool** and Autostart and so on.

> xdg offers autostart of Linux appilcations mainly Desktop when the GUI windowing system starts

There two main paths to look for entries to autostart:

1.  **/etc/xdg/autostart** – called system-wide and most of the application will place files when they are installed.
2.  [user’s home]/.config/autostart – user’s applications to start when the user logs in .

With **xdg** autostart feature the user can explain himself why the Windowing systems like **KDE** or **GNOME** start tens of applications (not exactly related to the base GUI windowing system).

> There is a security problem here, which is sometimes installing a package will place an autostart file there because the maintainer decided it is important but the package might be just a dependency and the next time the user logs in unwanted program might execute and open ports!

For example, **Rygel** is an open-source UPnP/DLNA MediaServer and it might be installed as a dependency but it places an autostart file, which starts a UPnP/DLNA server and exports the **/home/[user’s directory]/Videos**, **/home/[user’s directory]/Pictures** and more to the local network. Another example is with the **GNOME** index system tracker and the tracker-store, which may easily eat the RAM, disk, CPU, battery on a system without **GNOME** but with a different **GUI**!

### Here is what a typical Ubuntu 18.04 system might **auto**start

```
at-spi-dbus-bus.desktop
gnome-initial-setup-copy-worker.desktop
gnome-initial-setup-first-login.desktop
gnome-keyring-pkcs11.desktop
gnome-keyring-secrets.desktop
gnome-keyring-ssh.desktop
gnome-software-service.desktop
gnome-welcome-tour.desktop
nautilus-autostart.desktop
nm-applet.desktop
orca-autostart.desktop
org.gnome.DejaDup.Monitor.desktop
org.gnome.SettingsDaemon.A11ySettings.desktop
org.gnome.SettingsDaemon.Clipboard.desktop
org.gnome.SettingsDaemon.Color.desktop
org.gnome.SettingsDaemon.Datetime.desktop
org.gnome.SettingsDaemon.DiskUtilityNotify.desktop
org.gnome.SettingsDaemon.Housekeeping.desktop
org.gnome.SettingsDaemon.Keyboard.desktop
org.gnome.SettingsDaemon.MediaKeys.desktop
org.gnome.SettingsDaemon.Mouse.desktop
org.gnome.SettingsDaemon.Power.desktop
org.gnome.SettingsDaemon.PrintNotifications.desktop
org.gnome.SettingsDaemon.Rfkill.desktop
org.gnome.SettingsDaemon.ScreensaverProxy.desktop
org.gnome.SettingsDaemon.Sharing.desktop
org.gnome.SettingsDaemon.Smartcard.desktop
org.gnome.SettingsDaemon.Sound.desktop
org.gnome.SettingsDaemon.Wacom.desktop
org.gnome.SettingsDaemon.XSettings.desktop
print-applet.desktop
pulseaudio.desktop
snap-userd-autostart.desktop
spice-vdagent.desktop
update-notifier.desktop
user-dirs-update-gtk.desktop
xdg-user-dirs.desktop

```

### Not so typical Gentoo KDE system might **auto**start

```
at-spi-dbus-bus.desktop
baloo_file.desktop
blueman.desktop
evolution-alarm-notify.desktop
fcitx-autostart.desktop
Gentoo-print-applet.desktop
geoclue-demo-agent.desktop
gmenudbusmenuproxy.desktop
gnome-keyring-pkcs11.desktop
gnome-keyring-secrets.desktop
gnome-keyring-ssh.desktop
gsettings-data-convert.desktop
klipper.desktop
kmix_autostart.desktop
konqy_preload.desktop
org.kde.kdeconnect.daemon.desktop
org.kde.kgpg.desktop
org.kde.plasmashell.desktop
pam_kwallet_init.desktop
polkit-gnome-authentication-agent-1.desktop
polkit-kde-authentication-agent-1.desktop
powerdevil.desktop
pulseaudio.desktop
restore_kmix_volumes.desktop
tracker-extract.desktop
tracker-miner-fs.desktop
tracker-miner-rss.desktop
tracker-store.desktop
user-dirs-update-gtk.desktop
xdg-user-dirs.desktop
xembedsniproxy.desktop
xfce4-clipman-plugin-autostart.desktop
xfce4-power-manager.desktop
xfce4-settings-helper-autostart.desktop
xfce4-tips-autostart.desktop
xfsettingsd.desktop
xscreensaver.desktop

```

It worth noting not all of the files execute an application in all GUI Windowing environments because some of them have options when to start – KDE or GNOME or other systems only.

There are a couple of examples:

GNOME keyring secrets daemon will starts only under GNOME, Unity and MATE:  
**/etc/xdg/autostart/gnome-keyring-secrets.desktop**

```
[Desktop Entry]
Type=Application
Name=Secret Storage Service
Comment=GNOME Keyring: Secret Service
Exec=/usr/bin/gnome-keyring-daemon --start --components=secrets
OnlyShowIn=GNOME;Unity;MATE;
NoDisplay=true
X-GNOME-Autostart-Phase=PreDisplayServer
X-GNOME-AutoRestart=false
X-GNOME-Autostart-Notify=true
X-GNOME-Bugzilla-Bugzilla=GNOME
X-GNOME-Bugzilla-Product=gnome-keyring
X-GNOME-Bugzilla-Component=general
X-GNOME-Bugzilla-Version=3.28.0.2
X-Ubuntu-Gettext-Domain=gnome-keyring

```

Skip some GUI desktops as GNOME, KDE, Unity, MATE, LXQt, but in all other will start the XFce4 power manager – **/etc/xdg/autostart/xfce4-power-manager.desktop** ???

```
[Desktop Entry]
Name=Power Manager
Comment=Power management for the Xfce desktop
Icon=xfce4-power-manager-settings
Exec=xfce4-power-manager
Terminal=false
Type=Application
NotShowIn=GNOME;KDE;Unity;MATE;LXQt;
StartupNotify=false

```

More details in the manual here – [https://specifications.freedesktop.org/desktop-entry-spec/latest/](https://specifications.freedesktop.org/desktop-entry-spec/latest/)

Post navigation
---------------