---
title: "Run gui application on startup"
layout: post
---

# Run gui application on startup
```
ls -a
```

was just an example. The goal was to use both GUI and command line apps. Just to compare, I will use this example: SSH!
When you SSH into a target Linux machine, you can execute commands. If you

```
export DISPLAY=:0
```

you can then literally display X11 Windows on the screen! Well, that is what I meant by "GUI". The idea is to "act as selected user" and execute commands as this user, under their own environment (which includes X11 windows - GUI). 

So if I wanted

```
gnome-terminal
```

, that should work. Also,

```
rhythmbox-client
```

has to reach it's own running "server" (rhythmbox player) and change the music. This is what failed.

berbae wrote:

> Probably you need some 'Environment=' or 'StandardInput=', 'StandardOutput=', 'TTYPath=' parameters to be set: see 'man systemd.exec'.

Thanks for your help! I've looked into it and saw the entry explaining the environment variables and it helped me solving this problem. But, not in this way. I do not use my service file to set the variables because different users (with different privileges) have to login to the machine using my own app. The daemon TCP server (which starts on boot using systemd) we talked about checks whether the entered information is correct or not and then, if login succeeded, tries to "act as the target user". I've put a lot of effort in this, and it worked in the end (but hey, there are new problems now ![big_smile](https://bbs.archlinux.org/img/smilies/big_smile.png)).
So what I basically did was: first, I created a script that checks for the environment data of a specific user (that has to be logged in first); then I receive the output of the script; finally I change the environment variables of my own app, and change current uid with setuid();

```
strings=$(loginctl list-sessions | grep $1 | tail -n1)
items=($strings)

if [ "${#items[@]}" = "4" ]; then
        echo ${items[0]}
        echo ${items[1]}
        echo ${items[2]}
        echo ${items[3]}
        fgconsole
else
        echo
fi
```

This script has to be run as root.
Yes, I know having external scripts is a bad idea. ![big_smile](https://bbs.archlinux.org/img/smilies/big_smile.png)

This is a structure used in the code below:

```
typedef struct user_info {
	char *sessionID;
	char *UID;
	char *userName;
	char *seatName;
    char *virtualTermNum;
} userInfo;
```

This is the code that receives the data

```
userInfo *getEnvData(char *user) {
        userInfo *result = malloc(sizeof(userInfo));
	char msg[100];
	sprintf(&msg, "Getting environment data for %s ...", user);
	log_message(LOG_FILE, &msg);
	FILE *fp;
	char command[30], path[1035];
	sprintf(&command, "%s/getEnv %s\0", SCRIPTS_DIR, user);
  	char tmpUsername[10], tmpUID[10], tmpSeat[10], tmpSessionID[10], tmpVirtTermNum[10];
  	/* Open the command for reading. */
  	fp = popen(&command, "r");
  	if (fp == NULL) {
		return NULL;
  	}
    fgets(tmpSessionID, sizeof(path), fp);
    if (strcmp(tmpSessionID, "\n") == 0) return NULL;
    fgets(tmpUID, sizeof(path), fp);
    fgets(tmpUsername, sizeof(path), fp);
    fgets(tmpSeat, sizeof(path), fp);
    fgets(tmpVirtTermNum, sizeof(path), fp);
    tmpSessionID[strlen(tmpSessionID)-1] = '\0';
    tmpUID[strlen(tmpUID)-1] = '\0';
    tmpUsername[strlen(tmpUsername)-1] = '\0';
    tmpSeat[strlen(tmpSeat)-1] = '\0';
    tmpVirtTermNum[strlen(tmpVirtTermNum)-1] = '\0';
    result->seatName = strdup(tmpSeat);
    result->sessionID = strdup(tmpSessionID);
    result->UID = strdup(tmpUID);
    result->userName = strdup(tmpUsername);
    result->virtualTermNum = strdup(tmpVirtTermNum);
  	/* close */
  	pclose(fp);
	log_message(LOG_FILE, "Environment data received.");
	return result;
}
```

This is how you act as a specific user:

```
void actAs(userInfo *mUserInfo) {
        char homeDIR[50], runtimeDIR[50];
        sprintf(&homeDIR, "/home/%s\0", mUserInfo->userName);
        sprintf(&runtimeDIR, "/run/user/%s\0", mUserInfo->UID);
        setenv("USER", mUserInfo->userName, 1);
        setenv("LOGNAME", mUserInfo->userName, 1);
        setenv("HOME", &homeDIR, 1);
        setenv("SHELL", "/bin/bash", 1);
        setenv("XDG_SESSION_ID", mUserInfo->sessionID, 1);
        setenv("XDG_SEAT", mUserInfo->seatName, 1);
        setenv("XDG_VTNR", mUserInfo->virtualTermNum, 1);
        setenv("XDG_RUNTIME_DIR", &runtimeDIR, 1);
        setuid(atoi(mUserInfo->UID));
}
```

The problem with this is, once you switch to a standard user, the whole app becomes powerless.
You can not execute commands as root (and the daemon has to do this from time to time).

Thanks for your replies. I know that sometimes I sound confusing. English is not my native language ![smile](https://bbs.archlinux.org/img/smilies/smile.png)
I hope that I have explained everything well, now.

berbae wrote:

> And you didn't post the output of 'journalctl -u tcpd' or 'systemctl status tcpd.service' to show the exact messages generated while using the service.

There wasn't anything at all at that point except "TTY Unknown" which I stated during one of the posts.
Here it is.. I've looked back into the journal...

```
 root : TTY=unknown ; PWD=/usr/share/tcpd ; USER=twister ; COMMAND=scripts/play
Jul 16 16:11:05 linuxbox sudo[10018]: pam_unix(sudo:session): session opened for user twister by (uid=0)
Jul 16 16:11:05 linuxbox sudo[10018]: pam_unix(sudo:session): session closed for user twister
```

------

Proud Arch user, programmer & guitarist.
