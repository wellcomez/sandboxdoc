---
date: 2023-10-07 11:11
title: PPD file to enable Watermark printing in CUPS
tags:
  - watermark
  - ppd
  - cups
---
## PPD file to enable Watermark printing in CUPS

- [Index](https://bbs.archlinux.org/index.php)
- » [System Administration](https://bbs.archlinux.org/viewforum.php?id=50)
- » **[Watermark printing in CUPS](https://bbs.archlinux.org/viewtopic.php?id=193085)**

1[2015-02-02 12:59:25](https://bbs.archlinux.org/viewtopic.php?pid=1498954#p1498954)

**jxfish2**

**Member**

Registered: 2015-02-02

Posts: 1

I added a printer through CUPS.

I then added code to the newly created PPD file to enable this code:

     [http://www.cups.org/documentation.php/spec-ppd.html](http://www.cups.org/documentation.php/spec-ppd.html)  -  Custom Options section of the document

After adding the code to the PPD file and bouncing CUPS, the new Watermark options now appear inside of the CUPS web admin page.

I can select exactly what I want to configure inside of the GUI, but when I attempt to print, no watermark ever appears.

Has anyone else had this same issue before?

If so, does anyone have a solution to the problem?




## Watermark Filter with cups.
https://cups-dev.easysw.narkive.com/QJtBfCsY/watermark-filter-with-cups
### Brian Epstein

20 years ago

[Permalink](https://narkive.com/QJtBfCsY.1)

Hi. I'm trying to create a filter to use with cups that adds a  
watermark to every file that is printed. I'm pretty sure that I want to  
create a filter, as opposed to a backend, but I'm new to CUPS and would  
like your help.  
  
I've searched around, and haven't found the answers I'm looking for,  
yet, my apologies if this has been gone over.  
  
I'm running RedHat Linux 9 with cups-1.1.17-13.3. I've created my  
filter and placed it in /usr/lib/cups/filter. Here is my filter  
thusfar, I need help making it conform with the cups standard.  
~~~sh
/usr/lib/cups/filter/watermark ----  
#!/usr/bin/perl  
# job-id, user, title, copies, options, [filename or stdin]  
  
($job_id, $user, $title, $copies, $options, $filename) = @ARGV;  
  
$flag = 1;  
$HOSTNAME = `hostname`;  
$LOCALTIME=localtime();  
chomp($HOSTNAME);  
while (<>) {  
if (/^%%BeginPageSetup/) {  
print $_;  
if ($flag) {  
print "grestore\n";  
}  
$flag = 1;  
print $_;  
print "gsave\n";  
print ".95 setgray\n";  
print "/Helvetica-Bold findfont 12 scalefont setfont\n";  
print "80 80 800 { 306 exch moveto\n";  
  
print "(${user}\@${HOSTNAME}:${job_id} ${LOCALTIME}) dup\n";  
print "stringwidth pop 2 div neg 0 rmoveto show } for\n";  
print "grestore\n";  
} else {  
print;  
}  
}  
~~~
Now, how do I tell CUPS to use this filter on everything that goes to my  
printer. I've tried adding the line:  
  
*cupsFilter: application/vnd.cups-postscript 0 watermark  
  
to /usr/share/cups/model/postscript.ppd.gz, but to no avail.  
  
Thanks for your help!  
  
Brian  

--  
Brian Epstein <ep-OblpNrxUgVUdnm+***@public.gmane.org>  
Key fingerprint = F9C8 A715 933E 6A64 C220 482B 02CF B6C8 DB7F 41B4  

### Anonymous 20 years ago

[Permalink](https://narkive.com/QJtBfCsY.2)

...

First of all: I am not a good PostScript expert, and not even a Perl expert  
at all -- but I believe your filter can't work, since it tries to put the Watermark PS code at the wrong place.  
  
Just to see if it works as you intend, put these lines into a PostScript  
testfile:  
~~~
gsave  
.95 setgray  
/Helvetica-Bold findfont 72 scalefont setfont  
80 80 800 { 306 exch moveto  
(Brian at localhost:testprint.ps Wed Jun 11 00:59:40 2003) dup  
stringwidth pop 2 div neg 0 rmoveto show } for  
grestore  
gsave  
~~~
  
  
Put the lines at different places. Just for proofing your perl script  
works as a CUPS filter, put it right before a "showpage" command. (This  
will make it write across other content and parially cover it, but it  
is just for now).  
  
Running your filter manually, as it is:  
  
~~~
"./watermark /path/to/testprint.ps > testprint-with-watermark.ps"  
~~~
  
doesn't show any difference between "testprint-with-watermark.ps" and "testprint.ps".  
  
I changed the line  
  
~~~
"if (/^%%BeginPageSetup/) {"  
~~~
  
to  
  
~~~
"if (/^%%Page:/) {"  
~~~
  
and it worked. Re-running above command resulted in a new  
"testprint-with-watermark.ps" which previewed the watermark nicely  
in "gv".  
  
But this command syntax is not the right one for CUPS. It should be  
running like this:  
  
~~~
"./watermark $job_id $user $title $copies $options /path/to/testprint.ps > testprint-with-watermark.ps"  
~~~
  
..i.e. the file to be converted should be the 6th argument to the  
script, but I don't know how to make this happen in Perl. (Somehow the  
line "while (<>)" is wrong.....)  
  
Now for the integration into the CUPS filtering system....  

>[_Post by Brian Epstein_](https://cups-dev.easysw.narkive.com/QJtBfCsY/watermark-filter-with-cups#post1)  
>*cupsFilter: application/vnd.cups-postscript 0 watermark  
>to /usr/share/cups/model/postscript.ppd.gz, but to no avail.  

This is not wrong and should work now that your watermark script  
is fixed.  
  
Another alternative is to define an additional filtering rule for your watermark script in "<mark style="background: #E73308E3;">/etc/cups/mime.convs</mark>". Add a line like  
~~~ 
application/vnd.cups-postscript application/vnd.cups-raw 0 watermark  
~~~ 
  
(or better add it into a separate "**/etc/cups/my_own_mime.conv**s" file to  inoculate your changes against overwriting the mime.convs file during updates. CUPS does parse every *.convs file in "**/etc/cups/**" for filtering  rules...)  
  
The above line tells cups that after the "watermark" filter the jobfile  
should go directly to the backend. So this assumes a PostScript printer,  
of course (as indicated by your quoting of the postscript.ppd.gz file).  
  
Should you need to print to a different printer, you need to make further  
changes to the *.types and the *.convs files to make it happen. You could  
f.e. have these lines in mime.convs:  
~~~sh 
application/postscript application/vnd.cups-postscript_pre 66 pstops  
application/vnd.cups-postscript_pre application/vnd.cups-postscript 0 watermark  
~~~
  
<mark style="background: #ABF7F7A6;">Note that this would enforce the watermark on all printjobs, on all  
queues (only leaving out those you print with the "-o raw" option).  </mark>
  
To have the filter applied selectively per queue, the way to do it is  
to add the "*cupsFilter" line to the printqueue's PPD.  

>[_Post by Brian Epstein_](https://cups-dev.easysw.narkive.com/QJtBfCsY/watermark-filter-with-cups#post1)  
>Thanks for your help!  
>Brian  

Cheers,  
Kurt  

### Brian Epstein

20 years ago

[Permalink](https://narkive.com/QJtBfCsY.3)

This post might be inappropriate. Click to display it.

### Helge Blischke

20 years ago

[Permalink](https://narkive.com/QJtBfCsY.4)

...

Look into the links section on www.cups.org for "an alternate pstops  
filter".  
Though it is designed for CUPS 1.1.18, I think it will work with 1.1.17  
as well.  
This filter supports the option "page-label=name" in the sense that, if  
in a  
special directory there is a PostScript snippet the name of which is  
"name",  
this will executed at the very beginning of each page.  
  
Helge  

--  
H.Blischke-***@public.gmane.org  
H.Blischke-5dK1KTCurcY4Q++***@public.gmane.org  
H.Blischke-***@public.gmane.org  

### Kurt Pfeifle

20 years ago

[Permalink](https://narkive.com/QJtBfCsY.5)

...

There is: do it at the end of the page (just before showpage). Yes, it  
ends up laying on top of the rest of the PostScript. But then do it  
by drawing with a "transparent" gray. You can create a transparent gray  
by defining a "pattern" (see "makepattern" and "setpattern") of raster  
dots.  
  
Cheers,  
Kurt