---
date: 2023-10-07 13:22
title: Filter and Backend Programming
tags:
  - cups
  - cups-filter
  - watermark
---



# Filter and Backend Programming

|Headers|cups/backend.h  <br>cups/sidechannel.h|
|---|---|
|Library|-lcups|
|See Also|Programming: [Introduction to CUPS Programming](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-overview.html)  <br>Programming: [CUPS API](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-cups.html)  <br>Programming: [PPD API](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-ppd.html)  <br>Programming: [Raster API](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-raster.html)  <br>Programming: [Developing PostScript Printer Drivers](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/postscript-driver.html)  <br>Programming: [Developing Raster Printer Drivers](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/raster-driver.html)  <br>Specifications: [CUPS Design Description](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/spec-design.html)|

## Contents

- [Overview](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#OVERVIEW)
    - [Security Considerations](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#SECURITY)
    - [Canceled Jobs and Signal Handling](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#SIGNALS)
    - [File Permissions](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#PERMISSIONS)
    - [Temporary Files](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TEMPFILES)
    - [Copy Generation](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#COPIES)
    - [Exit Codes](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#EXITCODES)
    - [Environment Variables](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#ENVIRONMENT)
    - [Communicating with the Scheduler](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#MESSAGES)
    - [Communicating with the Backend](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#COMMUNICATING_BACKEND)
    - [Communicating with Filters](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#COMMUNICATING_FILTER)
    - [Doing SNMP Queries with Network Printers](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#SNMP)
- [Sandboxing on OS X](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#SANDBOXING)
- [Functions](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#FUNCTIONS)
    - [cupsBackChannelRead](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackChannelRead "Read data from the backchannel.")
    - [cupsBackChannelWrite](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackChannelWrite "Write data to the backchannel.")
    - [cupsBackendDeviceURI](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackendDeviceURI "Get the device URI for a backend.")
    - [cupsBackendReport](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackendReport "Write a device line from a backend.")
    - [cupsSideChannelDoRequest](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest "Send a side-channel command to a backend and wait for a response.")
    - [cupsSideChannelRead](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelRead "Read a side-channel message.")
    - [cupsSideChannelSNMPGet](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPGet "Query a SNMP OID's value.")
    - [cupsSideChannelSNMPWalk](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPWalk "Query multiple SNMP OID values.")
    - [cupsSideChannelWrite](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelWrite "Write a side-channel message.")
- [Data Types](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TYPES)
    - [cups_backend_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_backend_t "Backend exit codes")
    - [cups_sc_bidi_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_bidi_t "Bidirectional capabilities")
    - [cups_sc_command_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_t "Request command codes")
    - [cups_sc_connected_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_connected_t "Connectivity values")
    - [cups_sc_state_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_state_t "Printer state bits")
    - [cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t "Response status codes")
    - [cups_sc_walk_func_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_walk_func_t "SNMP walk callback")
- [Constants](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#ENUMERATIONS)
    - [cups_backend_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_backend_e "Backend exit codes")
    - [cups_sc_bidi_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_bidi_e "Bidirectional capability values")
    - [cups_sc_command_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_e "Request command codes")
    - [cups_sc_connected_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_connected_e "Connectivity values")
    - [cups_sc_state_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_state_e "Printer state bits")
    - [cups_sc_status_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_e "Response status codes")

## Overview

Filters (which include printer drivers and port monitors) and backends are used to convert job files to a printable format and send that data to the printer itself. All of these programs use a common interface for processing print jobs and communicating status information to the scheduler. Each is run with a standard set of command-line arguments:

|param|detail|
|---|---|
|argv[1]|The job ID|
|argv[2]|The user printing the job|
|argv[3]|The job name/title|
|argv[4]|The number of copies to print|
|argv[5]|The options that were provided when the job was submitted|
|argv[6]|The file to print (first program only)

The scheduler runs one or more of these programs to print any given job. The first filter reads from the print file and writes to the standard output, while the remaining filters read from the standard input and write to the standard output. The backend is the last filter in the chain and writes to the device.

Filters are always run as a non-privileged user, typically "lp", with no connection to the user's desktop. Backends are run either as a non-privileged user or as root if the file permissions do not allow user or group execution. The [file permissions](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#PERMISSIONS) section talks about this in more detail.

### Security Considerations

It is always important to use security programming practices. Filters and most backends are run as a non-privileged user, so the major security consideration is resource utilization - filters should not depend on unlimited amounts of CPU, memory, or disk space, and should protect against conditions that could lead to excess usage of any resource like infinite loops and unbounded recursion. In addition, filters must _never_ allow the user to specify an arbitrary file path to a separator page, template, or other file used by the filter since that can lead to an unauthorized disclosure of information. _Always_ treat input as suspect and validate it!

If you are developing a backend that runs as root, make sure to check for potential buffer overflows, integer under/overflow conditions, and file accesses since these can lead to privilege escalations. When writing files, always validate the file path and _never_ allow a user to determine where to store a file.

> **Note:**
> 
> _Never_ write files to a user's home directory. Aside from the security implications, CUPS is a network print service and as such the network user may not be the same as the local user and/or there may not be a local home directory to write to.
> 
> In addition, some operating systems provide additional security mechanisms that further limit file system access, even for backends running as root. On OS X, for example, no backend may write to a user's home directory. See the [Sandboxing on OS X](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#SANDBOXING) section for more information.

### Canceled Jobs and Signal Handling

The scheduler sends `SIGTERM` when a printing job is canceled or held. Filters, backends, and port monitors _must_ catch `SIGTERM` and perform any cleanup necessary to produce a valid output file or return the printer to a known good state. The recommended behavior is to end the output on the current page, preferably on the current line or object being printed.

Filters and backends may also receive `SIGPIPE` when an upstream or downstream filter/backend exits with a non-zero status. Developers should generally ignore `SIGPIPE` at the beginning of `main()` with the following function call:

~~~c
#include <signal.h>>

...

int
main(int argc, char *argv[])
{
  signal(SIGPIPE, SIG_IGN);

  ...
}
~~~

### File Permissions

For security reasons, CUPS will only run filters and backends that are owned by root and do not have world or group write permissions. The recommended permissions for filters and backends are 0555 - read and execute but no write. Backends that must run as root should use permissions of 0500 - read and execute by root, no access for other users. Write permissions can be enabled for the root user only.

To avoid a warning message, the directory containing your filter(s) must also be owned by root and have world and group write disabled - permissions of 0755 or 0555 are strongly encouraged.

### Temporary Files

Temporary files should be created in the directory specified by the "TMPDIR" environment variable. The [`cupsTempFile2`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsTempFile2) function can be used to safely create temporary files in this directory.

### Copy Generation

The `argv[4]` argument specifies the number of copies to produce of the input file. In general, you should only generate copies if the _filename_ argument is supplied. The only exception to this are filters that produce device-independent PostScript output, since the PostScript filter pstops is responsible for generating copies of PostScript files.

### Exit Codes

Filters must exit with status 0 when they successfully generate print data or 1 when they encounter an error. Backends can return any of the [`cups_backend_t`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_backend_t) constants.

### Environment Variables

The following environment variables are defined by the printing system when running print filters and backends:

|Evnvir|Detail|
|---|---|
|APPLE_LANGUAGE|The Apple language identifier associated with the job (OS X only).|
|CHARSET|The job character set, typically "utf-8".|
|CLASS|When a job is submitted to a printer class, contains the name of the destination printer class. Otherwise this environment variable will not be set.|
|CONTENT_TYPE|The MIME type associated with the file (e.g. application/postscript).|
|CUPS_CACHEDIR|The directory where cache files can be stored. Cache files can be used to retain information between jobs or files in a job.|
|CUPS_DATADIR|The directory where (read-only) CUPS data files can be found.|
|CUPS_FILETYPE|The type of file being printed: "job-sheet" for a banner page and "document" for a regular print file.|
|CUPS_SERVERROOT|The root directory of the server.|
|DEVICE_URI|The device-uri associated with the printer.|
|FINAL_CONTENT_TYPE|The MIME type associated with the printer (e.g. application/vnd.cups-postscript).|
|LANG|The language locale associated with the job.|
|PPD|The full pathname of the PostScript Printer Description (PPD) file for this printer.|
|PRINTER|The queue name of the class or printer.|
|RIP_CACHE|The recommended amount of memory to use for Raster Image Processors (RIPs).|
|TMPDIR|The directory where temporary files should be created.

### Communicating with the Scheduler

Filters and backends communicate with the scheduler by writing messages to the standard error file. The scheduler reads messages from all filters in a job and processes the message based on its prefix. For example, the following code sets the current printer state message to "Printing page 5":
~~~c
int page = 5;

fprintf(stderr, "INFO: Printing page %d\n", page);
~~~

Each message is a single line of text starting with one of the following prefix strings:

|---|---|
|---|---|
|ALERT: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "alert" log level.
|ATTR: attribute=value [attribute=value]|Sets the named printer or job attribute(s). Typically this is used to set the `marker-colors`, `marker-high-levels`, `marker-levels`, `marker-low-levels`, `marker-message`, `marker-names`, `marker-types`, `printer-alert`, and `printer-alert-description` printer attributes. Standard `marker-types` values are listed in [Table 1](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TABLE1). String values need special handling - see [Reporting Attribute String Values](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#ATTR_STRINGS) below.|
CRIT: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "critical" log level.|DEBUG: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "debug" log level.|
DEBUG2: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "debug2" log level.|EMERG: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "emergency" log level.|
ERROR: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "error" log level. Use "ERROR:" messages for non-persistent processing errors.|
INFO: message|Sets the printer-state-message attribute. If the current log level is set to "debug2", also adds the specified message to the current error log file using the "info" log level.|
NOTICE: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "notice" log level.|PAGE: page-number #-copies|PAGE: total #-pages|Adds an entry to the current page log file. The first form adds #-copies to the job-media-sheets-completed attribute. The second form sets the job-media-sheets-completed attribute to #-pages.|
PPD: keyword=value [keyword=value ...]|Changes or adds keywords to the printer's PPD file. Typically this is used to update installable options or default media settings based on the printer configuration.|STATE: + printer-state-reason [printer-state-reason ...]|
STATE: - printer-state-reason [printer-state-reason ...]|Sets or clears printer-state-reason keywords for the current queue. Typically this is used to indicate persistent media, ink, toner, and configuration conditions or errors on a printer. [Table 2](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TABLE2) lists the standard state keywords - use vendor-prefixed ("com.example.foo") keywords for custom states. See [Managing Printer State in a Filter](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#MANAGING_STATE) for more information.
|WARNING: message|Sets the printer-state-message attribute and adds the specified message to the current error log file using the "warning" log level.|Messages without one of these prefixes are treated as if they began with the "DEBUG:" prefix string.|


Table 1: Standard marker-types Values

|marker-type|Description|
|---|---|
|developer|Developer unit|
|fuser|Fuser unit|
|fuser-cleaning-pad|Fuser cleaning pad|
|fuser-oil|Fuser oil|
|ink|Ink supply|
|opc|Photo conductor|
|solid-wax|Wax supply|
|staples|Staple supply|
|toner|Toner supply|
|transfer-unit|Transfer unit|
|waste-ink|Waste ink tank|
|waste-toner|Waste toner tank|
|waste-wax|Waste wax tank|

  

Table 2: Standard State Keywords

|Keyword|Description|
|---|---|
|connecting-to-device|Connecting to printer but not printing yet.|
|cover-open|The printer's cover is open.|
|input-tray-missing|The paper tray is missing.|
|marker-supply-empty|The printer is out of ink.|
|marker-supply-low|The printer is almost out of ink.|
|marker-waste-almost-full|The printer's waste bin is almost full.|
|marker-waste-full|The printer's waste bin is full.|
|media-empty|The paper tray (any paper tray) is empty.|
|media-jam|There is a paper jam.|
|media-low|The paper tray (any paper tray) is almost empty.|
|media-needed|The paper tray needs to be filled (for a job that is printing).|
|paused|Stop the printer.|
|timed-out|Unable to connect to printer.|
|toner-empty|The printer is out of toner.|
|toner-low|The printer is low on toner.|

#### Reporting Attribute String Values

When reporting string values using "ATTR:" messages, a filter or backend must take special care to appropriately quote those values. The scheduler uses the CUPS option parsing code for attributes, so the general syntax is:

~~~sh
name=simple
name=simple,simple,...
name='complex value'
name="complex value"
name='"complex value"','"complex value"',...
~~~

Simple values are strings that do not contain spaces, quotes, backslashes, or the comma and can be placed verbatim in the "ATTR:" message, for example:

~~~c
int levels[4] = { 40, 50, 60, 70 }; /* CMYK */

fputs("ATTR: marker-colors=#00FFFF,#FF00FF,#FFFF00,#000000\n", stderr);
fputs("ATTR: marker-high-levels=100,100,100,100\n", stderr);
fprintf(stderr, "ATTR: marker-levels=%d,%d,%d,%d\n", levels[0], levels[1],
        levels[2], levels[3], levels[4]);
fputs("ATTR: marker-low-levels=5,5,5,5\n", stderr);
fputs("ATTR: marker-types=toner,toner,toner,toner\n", stderr);
~~~

Complex values that contains spaces, quotes, backslashes, or the comma must be quoted. For a single value a single set of quotes is sufficient:

~~~c
fputs("ATTR: marker-message='Levels shown are approximate.'\n", stderr);
~~~

When multiple values are reported, each value must be enclosed by a set of single and double quotes:

~~~c
fputs("ATTR: marker-names='\"Cyan Toner\"','\"Magenta Toner\"',"
      "'\"Yellow Toner\"','\"Black Toner\"'\n", stderr);
~~~

The IPP backend includes a quote_string function that may be used to properly quote a complex value in an "ATTR:" message:
~~~c
static const char *                     /* O - Quoted string */
quote_string(const char *s,             /* I - String */
             char       *q,             /* I - Quoted string buffer */
             size_t     qsize)          /* I - Size of quoted string buffer */
{
  char  *qptr,                          /* Pointer into string buffer */
        *qend;                          /* End of string buffer */


  qptr = q;
  qend = q + qsize - 5;

  if (qend < q)
  {
    *q = '\0';
    return (q);
  }

  *qptr++ = '\'';
  *qptr++ = '\"';

  while (*s && qptr < qend)
  {
    if (*s == '\\' || *s == '\"' || *s == '\'')
    {
      if (qptr < (qend - 4))
      {
        *qptr++ = '\\';
        *qptr++ = '\\';
        *qptr++ = '\\';
      }
      else
        break;
    }

    *qptr++ = *s++;
  }

  *qptr++ = '\"';
  *qptr++ = '\'';
  *qptr   = '\0';

  return (q);
}
~~~
#### Managing Printer State in a Filter

Filters are responsible for managing the state keywords they set using "STATE:" messages. Typically you will update _all_ of the keywords that are used by the filter at startup, for example:

~~~c
if (foo_condition != 0)
  fputs("STATE: +com.example.foo\n", stderr);
else
  fputs("STATE: -com.example.foo\n", stderr);

if (bar_condition != 0)
  fputs("STATE: +com.example.bar\n", stderr);
else
  fputs("STATE: -com.example.bar\n", stderr);
~~~

Then as conditions change, your filter sends "STATE: +keyword" or "STATE: -keyword" messages as necessary to set or clear the corresponding keyword, respectively.

State keywords are often used to notify the user of issues that span across jobs, for example "media-empty-warning" that indicates one or more paper trays are empty. These keywords should not be cleared unless the corresponding issue no longer exists.

Filters should clear job-related keywords on startup and exit so that they do not remain set between jobs. For example, "connecting-to-device" is a job sub-state and not an issue that applies when a job is not printing.

> **Note:**
> 
> "STATE:" messages often provide visible alerts to the user. For example, on OS X setting a printer-state-reason value with an "-error" or "-warning" suffix will cause the printer's dock item to bounce if the corresponding reason is localized with a cupsIPPReason keyword in the printer's PPD file.
> 
> When providing a vendor-prefixed keyword, _always_ provide the corresponding standard keyword (if any) to allow clients to respond to the condition correctly. For example, if you provide a vendor-prefixed keyword for a low cyan ink condition ("com.example.cyan-ink-low") you must also set the "marker-supply-low-warning" keyword. In such cases you should also refrain from localizing the vendor-prefixed keyword in the PPD file - otherwise both the generic and vendor-specific keyword will be shown in the user interface.

#### Reporting Supply Levels

CUPS tracks several "marker-*" attributes for ink/toner supply level reporting. These attributes allow applications to display the current supply levels for a printer without printer-specific software. [Table 3](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TABLE3) lists the marker attributes and what they represent.

Filters set marker attributes by sending "ATTR:" messages to stderr. For example, a filter supporting an inkjet printer with black and tri-color ink cartridges would use the following to initialize the supply attributes:
~~~c
fputs("ATTR: marker-colors=#000000,#00FFFF#FF00FF#FFFF00\n", stderr);
fputs("ATTR: marker-low-levels=5,10\n", stderr);
fputs("ATTR: marker-names=Black,Tri-Color\n", stderr);
fputs("ATTR: marker-types=ink,ink\n", stderr);
~~~

Then periodically the filter queries the printer for its current supply levels and updates them with a separate "ATTR:" message:

int black_level, tri_level;
...
fprintf(stderr, "ATTR: marker-levels=%d,%d\n", black_level, tri_level);

Table 3: Supply Level Attributes

|Attribute|Description|
|---|---|
|marker-colors|A list of comma-separated colors; each color is either "none" or one or more hex-encoded sRGB colors of the form "#RRGGBB".|
|marker-high-levels|A list of comma-separated "almost full" level values from 0 to 100; a value of 100 should be used for supplies that are consumed/emptied like ink cartridges.|
|marker-levels|A list of comma-separated level values for each supply. A value of -1 indicates the level is unavailable, -2 indicates unknown, and -3 indicates the level is unknown but has not yet reached capacity. Values from 0 to 100 indicate the corresponding percentage.|
|marker-low-levels|A list of comma-separated "almost empty" level values from 0 to 100; a value of 0 should be used for supplies that are filled like waste ink tanks.|
|marker-message|A human-readable supply status message for the user like "12 pages of ink remaining."|
|marker-names|A list of comma-separated supply names like "Cyan Ink", "Fuser", etc.|
|marker-types|A list of comma-separated supply types; the types are listed in [Table 1](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#TABLE1).|

### Communicating with the Backend

Filters can communicate with the backend via the [`cupsBackChannelRead`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackChannelRead) and [`cupsSideChannelDoRequest`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest) functions. The [`cupsBackChannelRead`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackChannelRead) function reads data that has been sent back from the device and is typically used to obtain status and configuration information. For example, the following code polls the backend for back-channel data:
~~~c
#include <cups/cups.h>

char buffer[8192];
ssize_t bytes;

/* Use a timeout of 0.0 seconds to poll for back-channel data */
bytes = cupsBackChannelRead(buffer, sizeof(buffer), 0.0);
~~~

Filters can also use `select()` or `poll()` on the back-channel file descriptor (3 or `CUPS_BC_FD`) to read data only when it is available.

The [`cupsSideChannelDoRequest`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest) function allows you to get out-of-band status information and do synchronization with the device. For example, the following code gets the current IEEE-1284 device ID string from the backend:

~~~c
#include <cups/sidechannel.h>

char data[2049];
int datalen;
[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) status;

/* Tell cupsSideChannelDoRequest() how big our buffer is, less 1 byte for
   nul-termination... */
datalen = sizeof(data) - 1;

/* Get the IEEE-1284 device ID, waiting for up to 1 second */
status = [cupsSideChannelDoRequest](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest)(CUPS_SC_CMD_GET_DEVICE_ID, data, &datalen, 1.0);

/* Use the returned value if OK was returned and the length is non-zero */
if (status == CUPS_SC_STATUS_OK && datalen > 0)
  data[datalen] = '\0';
else
  data[0] = '\0';
~~~

#### Forcing All Output to a Printer

The [`cupsSideChannelDoRequest`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest) function allows you to tell the backend to send all pending data to the printer. This is most often needed when sending query commands to the printer. For example:

~~~c
#include <cups/cups.h>
#include <cups/sidechannel.h>

char data[1024];
int datalen = sizeof(data);
[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) status;

/* Flush pending output to stdout */
fflush(stdout);

/* Drain output to backend, waiting for up to 30 seconds */
status = [cupsSideChannelDoRequest](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelDoRequest)(CUPS_SC_CMD_DRAIN_OUTPUT, data, &datalen, 30.0);

/* Read the response if the output was sent */
if (status == CUPS_SC_STATUS_OK)
{
  ssize_t bytes;

  /* Wait up to 10.0 seconds for back-channel data */
  bytes = cupsBackChannelRead(data, sizeof(data), 10.0);
  /* do something with the data from the printer */
}
~~~
### Communicating with Filters

Backends communicate with filters using the reciprocal functions [`cupsBackChannelWrite`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsBackChannelWrite), [`cupsSideChannelRead`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelRead), and [`cupsSideChannelWrite`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelWrite). We recommend writing back-channel data using a timeout of 1.0 seconds:

~~~c
#include <cups/cups.h>

char buffer[8192];
ssize_t bytes;

/* Obtain data from printer/device */
...

/* Use a timeout of 1.0 seconds to give filters a chance to read */
cupsBackChannelWrite(buffer, bytes, 1.0);
~~~

The [`cupsSideChannelRead`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelRead) function reads a side-channel command from a filter, driver, or port monitor. Backends can either poll for commands using a `timeout` of 0.0, wait indefinitely for commands using a `timeout` of -1.0 (probably in a separate thread for that purpose), or use `select` or `poll` on the `CUPS_SC_FD` file descriptor (4) to handle input and output on several file descriptors at the same time.

Once a command is processed, the backend uses the [`cupsSideChannelWrite`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelWrite) function to send its response. For example, the following code shows how to poll for a side-channel command and respond to it:

~~~c
#include <cups/sidechannel.h>

[cups_sc_command_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_t) command;
[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) status;
char data[2048];
int datalen = sizeof(data);

/* Poll for a command... */
if (![cupsSideChannelRead](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelRead)(&command, &status, data, &datalen, 0.0))
{
  switch (command)
  {
    /* handle supported commands, fill data/datalen/status with values as needed */

    default :
        status  = CUPS_SC_STATUS_NOT_IMPLEMENTED;
	datalen = 0;
	break;
  }

  /* Send a response... */
  [cupsSideChannelWrite](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelWrite)(command, status, data, datalen, 1.0);
}
~~~

### Doing SNMP Queries with Network Printers

The Simple Network Management Protocol (SNMP) allows you to get the current status, page counter, and supply levels from most network printers. Every piece of information is associated with an Object Identifier (OID), and every printer has a _community_ name associated with it. OIDs can be queried directly or by "walking" over a range of OIDs with a common prefix.

The two CUPS SNMP functions provide a simple API for querying network printers through the side-channel interface. Each accepts a string containing an OID like ".1.3.6.1.2.1.43.10.2.1.4.1.1" (the standard page counter OID) along with a timeout for the query.

The [`cupsSideChannelSNMPGet`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPGet) function queries a single OID and returns the value as a string in a buffer you supply:

~~~c
#include <cups/sidechannel.h>

char data[512];
int datalen = sizeof(data);

if ([cupsSideChannelSNMPGet](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPGet)(".1.3.6.1.2.1.43.10.2.1.4.1.1", data, &datalen, 5.0)
        == CUPS_SC_STATUS_OK)
{
  /* Do something with the value */
  printf("Page counter is: %s\n", data);
}
~~~

The [`cupsSideChannelSNMPWalk`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPWalk) function allows you to query a whole group of OIDs, calling a function of your choice for each OID that is found:

~~~c
#include <cups/sidechannel.h>

void
my_callback(const char *oid, const char *data, int datalen, void *context)
{
  /* Do something with the value */
  printf("%s=%s\n", oid, data);
}

...

void *my_data;
~~~

[cupsSNMPSideChannelWalk](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPWalk)(".1.3.6.1.2.1.43", 5.0, my_callback, my_data);

## Sandboxing on OS X

Starting with OS X 10.6, filters and backends are run inside a security "sandbox" which further limits (beyond the normal UNIX user/group permissions) what a filter or backend can do. This helps to both secure the printing system from malicious software and enforce the functional separation of components in the CUPS filter chain. What follows is a list of actions that are explicitly allowed for all filters and backends:

1. Reading of files: pursuant to normal UNIX file permissions, filters and backends can read files for the current job from the /private/var/spool/cups directory and other files on mounted filesystems _except_ for user home directories under /Users.
2. Writing of files: pursuant to normal UNIX file permissions, filters and backends can read/write files to the cache directory specified by the `CUPS_CACHEDIR` environment variable, to the state directory specified by the `CUPS_STATEDIR` environment variable, to the temporary directory specified by the `TMPDIR` environment variable, and under the /private/var/db, /private/var/folders, /private/var/lib, /private/var/mysql, /private/var/run, /private/var/spool (except /private/var/spool/cups), /Library/Application Support, /Library/Caches, /Library/Logs, /Library/Preferences, /Library/WebServer, and /Users/Shared directories.
3. Execution of programs: pursuant to normal UNIX file permissions, filters and backends can execute any program not located under the /Users directory. Child processes inherit the sandbox and are subject to the same restrictions as the parent.
4. Bluetooth and USB: backends can access Bluetooth and USB printers through IOKit. _Filters cannot access Bluetooth and USB printers directly._
5. Network: filters and backends can access UNIX domain sockets under the /private/tmp, /private/var/run, and /private/var/tmp directories. Backends can also create IPv4 and IPv6 TCP (outgoing) and UDP (incoming and outgoing) socket, and bind to local source ports. _Filters cannot directly create IPv4 and IPv6 TCP or UDP sockets._
6. Notifications: filters and backends can send notifications via the Darwin `notify_post()` API.

> **Note:** The sandbox profile used in CUPS 2.0 still allows some actions that are not listed above - these privileges will be removed over time until the profile matches the list above.

## Functions

###  CUPS 1.2/OS X 10.5 cupsBackChannelRead

Read data from the backchannel.

~~~c
ssize_t cupsBackChannelRead (  
    char *buffer,  
    size_t bytes,  
    double timeout  
);
~~~

#### Parameters
|param|detail|
|---|---|
|buffer|Buffer to read into
|bytes|Bytes to read
|timeout|Timeout in seconds, typically 0.0 to poll

#### Return Value|Bytes read or -1 on error

#### Discussion

Reads up to "bytes" bytes from the backchannel/backend. The "timeout" parameter controls how many seconds to wait for the data - use 0.0 to return immediately if there is no data, -1.0 to wait for data indefinitely.

###  CUPS 1.2/OS X 10.5 cupsBackChannelWrite

Write data to the backchannel.

~~~c
ssize_t cupsBackChannelWrite (  
    const char *buffer,  
    size_t bytes,  
    double timeout  
);
~~~

#### Parameters

|param|detail|
|---|---|
|buffer|Buffer to write
|bytes|Bytes to write
|timeout|Timeout in seconds, typically 1.0

#### Return Value

Bytes written or -1 on error

#### Discussion

Writes "bytes" bytes to the backchannel/filter. The "timeout" parameter controls how many seconds to wait for the data to be written - use 0.0 to return immediately if the data cannot be written, -1.0 to wait indefinitely.

###  CUPS 1.2/OS X 10.5 cupsBackendDeviceURI

Get the device URI for a backend.

~~~c
const char *cupsBackendDeviceURI (  
    char **argv  
);
~~~

#### Parameters
|---|---|
|---|---|
|argv|Command-line arguments

#### Return Value

Device URI or `NULL`

#### Discussion

The "argv" argument is the argv argument passed to main(). This function returns the device URI passed in the DEVICE_URI environment variable or the device URI passed in argv[0], whichever is found first.

###  CUPS 1.4/OS X 10.6 cupsBackendReport

Write a device line from a backend.

~~~c
void cupsBackendReport (  
    const char *device_scheme,  
    const char *device_uri,  
    const char *device_make_and_model,  
    const char *device_info,  
    const char *device_id,  
    const char *device_location  
);
~~~

#### Parameters

|param|detail|
|---|---|
|device_scheme|device-scheme string|
|device_uri|device-uri string|
|device_make_and_model|device-make-and-model string or `NULL`|
|device_info|device-info string or `NULL`|
|device_id|device-id string or `NULL`|
|device_location|device-location string or `NULL`|

#### Discussion

This function writes a single device line to stdout for a backend. It handles quoting of special characters in the device-make-and-model, device-info, device-id, and device-location strings.

###  CUPS 1.3/OS X 10.5 cupsSideChannelDoRequest

Send a side-channel command to a backend and wait for a response.

[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) cupsSideChannelDoRequest (  
    [cups_sc_command_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_t) command,  
    char *data,  
    int *datalen,  
    double timeout  
);

#### Parameters

|param|detail|
|---|---|
|command|Command to send|
|data|Response data buffer pointer|
|datalen|Size of data buffer on entry, number of bytes in buffer on return|
|timeout|Timeout in seconds

#### Return Value

Status of command

#### Discussion

This function is normally only called by filters, drivers, or port monitors in order to communicate with the backend used by the current printer. Programs must be prepared to handle timeout or "not implemented" status codes, which indicate that the backend or device do not support the specified side-channel command.  
  
The "datalen" parameter must be initialized to the size of the buffer pointed to by the "data" parameter. cupsSideChannelDoRequest() will update the value to contain the number of data bytes in the buffer.

###  CUPS 1.3/OS X 10.5 cupsSideChannelRead

Read a side-channel message.

int cupsSideChannelRead (  
    [cups_sc_command_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_t) *command,  
    [cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) *status,  
    char *data,  
    int *datalen,  
    double timeout  
);

#### Parameters

|param|detail|
|---|---|
|command|Command code|
|status|Status code|
|data|Data buffer pointer|
|datalen|Size of data buffer on entry, number of bytes in buffer on return|
|timeout|Timeout in seconds

#### Return Value

0 on success, -1 on error

#### Discussion

This function is normally only called by backend programs to read commands from a filter, driver, or port monitor program. The caller must be prepared to handle incomplete or invalid messages and return the corresponding status codes.  
  
The "datalen" parameter must be initialized to the size of the buffer pointed to by the "data" parameter. cupsSideChannelDoRequest() will update the value to contain the number of data bytes in the buffer.

###  CUPS 1.4/OS X 10.6 cupsSideChannelSNMPGet

Query a SNMP OID's value.

[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) cupsSideChannelSNMPGet (  
    const char *oid,  
    char *data,  
    int *datalen,  
    double timeout  
);

#### Parameters

|param|detail|
|---|---|
|oid|OID to query|
|data|Buffer for OID value|
|datalen|Size of OID buffer on entry, size of value on return|
|timeout|Timeout in seconds

#### Return Value

Query status

#### Discussion

This function asks the backend to do a SNMP OID query on behalf of the filter, port monitor, or backend using the default community name.  
  
"oid" contains a numeric OID consisting of integers separated by periods, for example ".1.3.6.1.2.1.43". Symbolic names from SNMP MIBs are not supported and must be converted to their numeric forms.  
  
On input, "data" and "datalen" provide the location and size of the buffer to hold the OID value as a string. HEX-String (binary) values are converted to hexadecimal strings representing the binary data, while NULL-Value and unknown OID types are returned as the empty string. The returned "datalen" does not include the trailing nul. `CUPS_SC_STATUS_NOT_IMPLEMENTED` is returned by backends that do not support SNMP queries. `CUPS_SC_STATUS_NO_RESPONSE` is returned when the printer does not respond to the SNMP query.

###  CUPS 1.4/OS X 10.6 cupsSideChannelSNMPWalk

Query multiple SNMP OID values.

[cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) cupsSideChannelSNMPWalk (  
    const char *oid,  
    double timeout,  
    [cups_sc_walk_func_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_walk_func_t) cb,  
    void *context  
);

#### Parameters

|param|detail|
|---|---|
|oid|First numeric OID to query|
|timeout|Timeout for each query in seconds|
|cb|Function to call with each value|
|context|Application-defined pointer to send to callback

#### Return Value

Status of first query of `CUPS_SC_STATUS_OK` on success

#### Discussion

This function asks the backend to do multiple SNMP OID queries on behalf of the filter, port monitor, or backend using the default community name. All OIDs under the "parent" OID are queried and the results are sent to the callback function you provide.  
  
"oid" contains a numeric OID consisting of integers separated by periods, for example ".1.3.6.1.2.1.43". Symbolic names from SNMP MIBs are not supported and must be converted to their numeric forms.  
  
"timeout" specifies the timeout for each OID query. The total amount of time will depend on the number of OID values found and the time required for each query.  
  
"cb" provides a function to call for every value that is found. "context" is an application-defined pointer that is sent to the callback function along with the OID and current data. The data passed to the callback is the same as returned by [`cupsSideChannelSNMPGet`](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cupsSideChannelSNMPGet). `CUPS_SC_STATUS_NOT_IMPLEMENTED` is returned by backends that do not support SNMP queries. `CUPS_SC_STATUS_NO_RESPONSE` is returned when the printer does not respond to the first SNMP query.

###  CUPS 1.3/OS X 10.5 cupsSideChannelWrite

Write a side-channel message.

int cupsSideChannelWrite (  
    [cups_sc_command_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_t) command,  
    [cups_sc_status_t](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_t) status,  
    const char *data,  
    int datalen,  
    double timeout  
);

#### Parameters

|param|detail|
|---|---|
|command|Command code|
|status|Status code|
|data|Data buffer pointer|
|datalen|Number of bytes of data|
|timeout|Timeout in seconds

#### Return Value

0 on success, -1 on error

#### Discussion

This function is normally only called by backend programs to send responses to a filter, driver, or port monitor program.

## Data Types

### cups_backend_t

Backend exit codes

typedef enum [cups_backend_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_backend_e) cups_backend_t;

### cups_sc_bidi_t

Bidirectional capabilities

typedef enum [cups_sc_bidi_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_bidi_e) cups_sc_bidi_t;

### cups_sc_command_t

Request command codes

typedef enum [cups_sc_command_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_command_e) cups_sc_command_t;

### cups_sc_connected_t

Connectivity values

typedef enum [cups_sc_connected_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_connected_e) cups_sc_connected_t;

### cups_sc_state_t

Printer state bits

typedef enum [cups_sc_state_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_state_e) cups_sc_state_t;

### cups_sc_status_t

Response status codes

typedef enum [cups_sc_status_e](https://opensource.apple.com/source/cups/cups-435/cups/doc/help/api-filter.html#cups_sc_status_e) cups_sc_status_t;

### cups_sc_walk_func_t

SNMP walk callback

typedef void (*cups_sc_walk_func_t)(const char *oid, const char *data, int datalen, void *context);

## Constants

### cups_backend_e

Backend exit codes

#### Constants

|Const|Detail|
|---|---|
|CUPS_BACKEND_AUTH_REQUIRED|Job failed, authentication required|
|CUPS_BACKEND_CANCEL|Job failed, cancel job|
|CUPS_BACKEND_FAILED|Job failed, use error-policy|
|CUPS_BACKEND_HOLD|Job failed, hold job|CUPS_BACKEND_OK|Job completed successfully|
|CUPS_BACKEND_RETRY|Job failed, retry this job later|
|CUPS_BACKEND_RETRY_CURRENT|Job failed, retry this job immediately|
|CUPS_BACKEND_STOP|Job failed, stop queue

### cups_sc_bidi_e

Bidirectional capability values

#### Constants
|Const|Detail|
|---|---|
|CUPS_SC_BIDI_NOT_SUPPORTED|Bidirectional I/O is not supported|
|CUPS_SC_BIDI_SUPPORTED|Bidirectional I/O is supported

### cups_sc_command_e

Request command codes

#### Constants
|Const|Detail|
|---|---|
|CUPS_SC_CMD_DRAIN_OUTPUT|Drain all pending output|
|CUPS_SC_CMD_GET_BIDI|Return bidirectional capabilities|
|CUPS_SC_CMD_GET_CONNECTED  CUPS 1.5/OS X 10.7 |Return whether the backend is "connected" to the printer|
|CUPS_SC_CMD_GET_DEVICE_ID|Return the IEEE-1284 device ID|
|CUPS_SC_CMD_GET_STATE|Return the device state|
|CUPS_SC_CMD_SNMP_GET  CUPS 1.4/OS X 10.6 |Query an SNMP OID|
|CUPS_SC_CMD_SNMP_GET_NEXT  CUPS 1.4/OS X 10.6 |Query the next SNMP OID|
|CUPS_SC_CMD_SOFT_RESET|Do a soft reset

### cups_sc_connected_e

Connectivity values

#### Constants
|Const|Detail|
|---|---|
CUPS_SC_CONNECTED|Backend is "connected" to printer
CUPS_SC_NOT_CONNECTED|Backend is not "connected" to printer

### cups_sc_state_e

Printer state bits

#### Constants

|Const|Detail|
|---|---|
|CUPS_SC_STATE_BUSY|Device is busy
|CUPS_SC_STATE_ERROR|Other error condition
|CUPS_SC_STATE_MARKER_EMPTY|Toner/ink out condition
|CUPS_SC_STATE_MARKER_LOW|Toner/ink low condition
|CUPS_SC_STATE_MEDIA_EMPTY|Paper out condition
|CUPS_SC_STATE_MEDIA_LOW|Paper low condition
|CUPS_SC_STATE_OFFLINE|Device is offline
|CUPS_SC_STATE_ONLINE|Device is online

### cups_sc_status_e

Response status codes

#### Constants

|Const|Detail|
|---|---|
|CUPS_SC_STATUS_BAD_MESSAGE|The command/response message was invalid
|CUPS_SC_STATUS_IO_ERROR|An I/O error occurred
|CUPS_SC_STATUS_NONE|No status
|CUPS_SC_STATUS_NOT_IMPLEMENTED|Command not implemented
|CUPS_SC_STATUS_NO_RESPONSE|The device did not respond
|CUPS_SC_STATUS_OK|Operation succeeded
|CUPS_SC_STATUS_TIMEOUT|The backend did not respond
|CUPS_SC_STATUS_TOO_BIG|Response too big