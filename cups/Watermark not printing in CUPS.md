---
date: 2023-10-07 14:56
title: Watermark not printing in CUPS
tags:
  - cups
  - watermark
---



# [cups] Watermark not printing in CUPS

**Joseph Fisher** [jxfish2 at gmail.com](mailto:cups%40cups.org?Subject=Re%3A%20%5Bcups%5D%20Watermark%20not%20printing%20in%20CUPS&In-Reply-To=%3CCAA%3DFgcTrWGOyXGNKA1MPWN0isMawhBrF5wCUGhE-ocoO40%3Dj9Q%40mail.gmail.com%3E "[cups] Watermark not printing in CUPS")  
_Mon Feb 2 06:01:32 PST 2015_

-   Previous message (by thread): [[cups-devel] [UNKN] STR #4568: Job suspended at the same time switch error_log](https://lists.cups.org/pipermail/cups/2015-February/072228.html)
-   Next message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054997.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2015-February/date.html#54996) [[ thread ]](https://lists.cups.org/pipermail/cups/2015-February/thread.html#54996) [[ subject ]](https://lists.cups.org/pipermail/cups/2015-February/subject.html#54996) [[ author ]](https://lists.cups.org/pipermail/cups/2015-February/author.html#54996)

---

To whom it may concern,

I added a printer through CUPS.

I then added code to the newly created PPD file to enable this code:

     [http://www.cups.org/documentation.php/spec-ppd.html](http://www.cups.org/documentation.php/spec-ppd.html)  -  Custom Options
section of the document

**% Base JCL key code option *JCLOpenUI JCLPasscode/Key Code: PickOne
*OrderDependency: 10 JCLSetup *JCLPasscode *DefaultJCLPasscode: None
*JCLPasscode None/No Code: "" *JCLPasscode 1111: "@PJL SET PASSCODE =
1111<0A>" *JCLPasscode 2222: "@PJL SET PASSCODE = 2222<0A>" *JCLPasscode
3333: "@PJL SET PASSCODE = 3333<0A>" *JCLCloseUI: *JCLPasscode *% Custom
JCL key code option *CustomJCLPasscode True: "@PJL SET PASSCODE = \1<0A>"
*ParamCustomJCLPasscode Code/Key Code: 1 passcode 4 4 *

**% Base PostScript watermark option*
*OpenUI WatermarkText/Watermark Text: PickOne
*OrderDependency: 10 AnySetup *WatermarkText
*DefaultWatermarkText: None
*WatermarkText None: ""
*WatermarkText Draft: "<</cupsString1(Draft)>>setpagedevice"
*CloseUI: *WatermarkText
**% Custom PostScript watermark option*
*CustomWatermarkText True: "<</cupsString1 3 -1 roll>>setpagedevice"
*ParamCustomWatermarkText Text: 1 string 0 32
**% Base PostScript gamma/density option*
*OpenUI GammaDensity/Gamma and Density: PickOne
*OrderDependency: 10 AnySetup *GammaDensity
*DefaultGammaDensity: Normal
*GammaDensity Normal/Normal: "<</cupsReal1 1.0/cupsReal2 1.0>>setpagedevice"
*GammaDensity Light/Lighter: "<</cupsReal1 0.9/cupsReal2 0.67>>setpagedevice"
*GammaDensity Dark/Darker: "<</cupsReal1 1.1/cupsReal2 1.5>>setpagedevice"
*CloseUI: *GammaDensity
**% Custom PostScript gamma/density option*
*CustomGammaDensity True: "<</cupsReal1 3 -1 roll/cupsReal2 5 -1>>setpagedevice"
*ParamCustomGammaDensity Gamma: 1 curve 0.1 10
*ParamCustomGammaDensity Density: 2 real 0 2



After adding the code to the PPD file and bouncing CUPS, the new Watermark
options now appear inside of the CUPS web admin page.

Everything looks perfect inside of the GUI.

I can select exactly what I want to configure inside of the GUI, but when I
attempt to print, no watermark ever appears.

Has anyone else had this same issue before?

If so, does anyone have a solution to the problem?

Did I simply miss putting something else in the PPD file that was required?

Thanks in advance,

JCF

---

-   Previous message (by thread): [[cups-devel] [UNKN] STR #4568: Job suspended at the same time switch error_log](https://lists.cups.org/pipermail/cups/2015-February/072228.html)
-   Next message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054997.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2015-February/date.html#54996) [[ thread ]](https://lists.cups.org/pipermail/cups/2015-February/thread.html#54996) [[ subject ]](https://lists.cups.org/pipermail/cups/2015-February/subject.html#54996) [[ author ]](https://lists.cups.org/pipermail/cups/2015-February/author.html#54996)

---

[More information about the cups mailing list](https://lists.cups.org/mailman/listinfo/cups)

##  [cups] Watermark not printing in CUPS

**Michael Sweet** [msweet at apple.com](mailto:cups%40cups.org?Subject=Re%3A%20%5Bcups%5D%20Watermark%20not%20printing%20in%20CUPS&In-Reply-To=%3C0CD7217B-F8F2-4DEA-AAD4-84A23D48D37A%40apple.com%3E "[cups] Watermark not printing in CUPS")  
_Mon Feb 2 06:09:16 PST 2015_

-   Previous message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054996.html)
-   Next message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054998.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2015-February/date.html#54997) [[ thread ]](https://lists.cups.org/pipermail/cups/2015-February/thread.html#54997) [[ subject ]](https://lists.cups.org/pipermail/cups/2015-February/subject.html#54997) [[ author ]](https://lists.cups.org/pipermail/cups/2015-February/author.html#54997)

---

Joseph,

The examples in the CUPS PPD spec are not directly usable in a PostScript printer or in a CUPS driver - you need additional code (PostScript or otherwise) in order to make the options do something.


> _On Feb 2, 2015, at 9:01 AM, Joseph Fisher <[jxfish2 at gmail.com](https://lists.cups.org/mailman/listinfo/cups)> wrote:_
> 
> _To whom it may concern,_
> 
> _I added a printer through CUPS._
> 
> _I then added code to the newly created PPD file to enable this code:_
> 
>     _[http://www.cups.org/documentation.php/spec-ppd.html](http://www.cups.org/documentation.php/spec-ppd.html)  -  Custom Options_
> _section of the document_
> 
> _**% Base JCL key code option *JCLOpenUI JCLPasscode/Key Code: PickOne_
> _*OrderDependency: 10 JCLSetup *JCLPasscode *DefaultJCLPasscode: None_
> _*JCLPasscode None/No Code: "" *JCLPasscode 1111: "@PJL SET PASSCODE =_
> _1111<0A>" *JCLPasscode 2222: "@PJL SET PASSCODE = 2222<0A>" *JCLPasscode_
> _3333: "@PJL SET PASSCODE = 3333<0A>" *JCLCloseUI: *JCLPasscode *% Custom_
> _JCL key code option *CustomJCLPasscode True: "@PJL SET PASSCODE = \1<0A>"_
> _*ParamCustomJCLPasscode Code/Key Code: 1 passcode 4 4 *_
> 
> _**% Base PostScript watermark option*_
> _*OpenUI WatermarkText/Watermark Text: PickOne_
> _*OrderDependency: 10 AnySetup *WatermarkText_
> _*DefaultWatermarkText: None_
> _*WatermarkText None: ""_
> _*WatermarkText Draft: "<</cupsString1(Draft)>>setpagedevice"_
> _*CloseUI: *WatermarkText_
> _**% Custom PostScript watermark option*_
> _*CustomWatermarkText True: "<</cupsString1 3 -1 roll>>setpagedevice"_
> _*ParamCustomWatermarkText Text: 1 string 0 32_
> _**% Base PostScript gamma/density option*_
> _*OpenUI GammaDensity/Gamma and Density: PickOne_
> _*OrderDependency: 10 AnySetup *GammaDensity_
> _*DefaultGammaDensity: Normal_
> _*GammaDensity Normal/Normal: "<</cupsReal1 1.0/cupsReal2 1.0>>setpagedevice"_
> _*GammaDensity Light/Lighter: "<</cupsReal1 0.9/cupsReal2 0.67>>setpagedevice"_
> _*GammaDensity Dark/Darker: "<</cupsReal1 1.1/cupsReal2 1.5>>setpagedevice"_
> _*CloseUI: *GammaDensity_
> _**% Custom PostScript gamma/density option*_
> _*CustomGammaDensity True: "<</cupsReal1 3 -1 roll/cupsReal2 5 -1>>setpagedevice"_
> _*ParamCustomGammaDensity Gamma: 1 curve 0.1 10_
> _*ParamCustomGammaDensity Density: 2 real 0 2_
> 
> 
> 
> _After adding the code to the PPD file and bouncing CUPS, the new Watermark_
> _options now appear inside of the CUPS web admin page._
> 
> _Everything looks perfect inside of the GUI._
> 
> _I can select exactly what I want to configure inside of the GUI, but when I_
> _attempt to print, no watermark ever appears._
> 
> _Has anyone else had this same issue before?_
> 
> _If so, does anyone have a solution to the problem?_
> 
> _Did I simply miss putting something else in the PPD file that was required?_
> 
> _Thanks in advance,_
> 
> _JCF_
> _________________________________________________
> _cups mailing list_
> _[cups at cups.org](https://lists.cups.org/mailman/listinfo/cups)_
> _[https://www.cups.org/mailman/listinfo/cups](https://www.cups.org/mailman/listinfo/cups)_
_________________________________________________________
Michael Sweet, Senior Printing System Engineer, PWG Chair

---

-   Previous message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054996.html)
-   Next message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054998.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2015-February/date.html#54997) [[ thread ]](https://lists.cups.org/pipermail/cups/2015-February/thread.html#54997) [[ subject ]](https://lists.cups.org/pipermail/cups/2015-February/subject.html#54997) [[ author ]](https://lists.cups.org/pipermail/cups/2015-February/author.html#54997)

---

[More information about the cups mailing list](https://lists.cups.org/mailman/listinfo/cups)


## [cups] Watermark not printing in CUPS

**Helge Blischke** [helgeblischke at web.de](mailto:cups%40cups.org?Subject=Re%3A%20%5Bcups%5D%20Watermark%20not%20printing%20in%20CUPS&In-Reply-To=%3CCFB3F71E-D234-420B-A426-C97B8ABDBBA3%40web.de%3E "[cups] Watermark not printing in CUPS")  
_Mon Feb 2 07:18:23 PST 2015_

-   Previous message (by thread): [[cups] Watermark not printing in CUPS](https://lists.cups.org/pipermail/cups/2015-February/054997.html)
-   Next message (by thread): [[cups-devel] [LOW] STR #4569: Incorrect documentation for sanitize_title](https://lists.cups.org/pipermail/cups/2015-February/072221.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2015-February/date.html#54998) [[ thread ]](https://lists.cups.org/pipermail/cups/2015-February/thread.html#54998) [[ subject ]](https://lists.cups.org/pipermail/cups/2015-February/subject.html#54998) [[ author ]](https://lists.cups.org/pipermail/cups/2015-February/author.html#54998)

---

A convenient way to provide additional PostScript code is to put it into the
value of the PPD key
*JobPatchFile n "……“
where n is an integer number (needed in case you need several such files)
and …… stands for the PostScript code to execute.

Note that this code is inserted at the beginning of the prolog section of the job file
and should normally only define procedures and „global“ variables.
Note further that the example code you used makes use of key/value pairs for
the setpagedevice operator that are not standard and my be refused (generate an
error) by the PS interpreter, so you probably need to redefine the setpagedevice
operator by a procedure which handles the custom keys.



# Command for Watermark printing

**Helge Blischke** [h.blischke at srz.de](mailto:cups%40cups.org?Subject=Re%3A%20Command%20for%20Watermark%20printing&In-Reply-To=%3C32808-cups.general%40news.easysw.com%3E "Command for Watermark printing")  
_Tue Jun 3 03:11:20 PDT 2008_

-   Previous message (by thread): [Command for Watermark printing](https://lists.cups.org/pipermail/cups/2008-June/044570.html)
-   Next message (by thread): [Command for Watermark printing](https://lists.cups.org/pipermail/cups/2008-June/044806.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2008-June/date.html#44573) [[ thread ]](https://lists.cups.org/pipermail/cups/2008-June/thread.html#44573) [[ subject ]](https://lists.cups.org/pipermail/cups/2008-June/subject.html#44573) [[ author ]](https://lists.cups.org/pipermail/cups/2008-June/author.html#44573)

---

nivi wrote:
> _hi,_
> 
> 
> _I would like to know the command used for printing Watermark in Unix._
> 
> _Is it something like this?_
> _lpr -P printername -o watermark= file name_
> 
> _watermark text=_
> _watermark font=_
> _watermark fontsize =_
> 
> _Please let me know in case you know the command._
> 
> _Thanks,_
> 
> 
> 
Look at the "alternate pstops filter" (link on the CUPS website).
It supports watermark printing via an extended page-label
option processing.

Though the currently published version stems from CUPS 1.1.18,
it still works; an updated version based on CUPS 1.3.5 will be
published soon.

Helge


# Cups PCL preprocessing (graphical watermark)

**Kurt Pfeifle** [k1pfeifle at gmx.net](mailto:cups%40cups.org?Subject=Re%3A%20Cups%20PCL%20preprocessing%20%28graphical%20watermark%29&In-Reply-To=%3C30013-cups.general%40news.easysw.com%3E "Cups PCL preprocessing (graphical watermark)")  
_Tue Jul 17 13:29:52 PDT 2007_

-   Previous message (by thread): [Cups PCL preprocessing (graphical watermark)](https://lists.cups.org/pipermail/cups/2007-July/041772.html)
-   Next message (by thread): [[cups.general] Cups PCL preprocessing (graphical watermark)](https://lists.cups.org/pipermail/cups/2007-July/041781.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2007-July/date.html#41776) [[ thread ]](https://lists.cups.org/pipermail/cups/2007-July/thread.html#41776) [[ subject ]](https://lists.cups.org/pipermail/cups/2007-July/subject.html#41776) [[ author ]](https://lists.cups.org/pipermail/cups/2007-July/author.html#41776)

---

Umberto Bernardi wrote:
> _Hi all !_
>  _I'm burning my brain trying to solve a big problem_
>  _I've an AS400 system that print on my linuxbox from a remote queue using lpd protocol..._
>  _the spool generated from my as400 is in PCL format_
Uff... that's a "hard nut to crack"....

>  _I've to put a graphical header on all pages printed_
Maybe it's easy for someone fluent in PCL (which version of PCL, BTW?!)
to add a watermark with PCL-native command (BTW, is this watermark/header
a static one, or a dynamically changing one?)

>  _I've read half internet, but I haven't find a solution_
What I'd do is this: look into GhostPCL (made by the same people who
develop Ghostscript). GhostPCL is not available pre-packaged for any
Linux distribution I'm aware of (not even Debian), but you may download
and compile the source tarball (if you accept the AFPL license).

With GhostPCL you can view PCL on-screen as well as convert PCL files
to PostScript and PDF. From there, it should be more easy to add water-
marks, overlays, headers and stationaries (but you'll still have to use
your own scripting skills, or hire someone to do it for you).

-- 
Kurt Pfeifle
System & Network Printing Consultant ---- Linux/Unix/Windows/Samba/CUPS
Infotec Deutschland GmbH  .....................  Hedelfinger Strasse 58
A RICOH Company  ...........................  D-70327 Stuttgart/Germany

---

-   Previous message (by thread): [Cups PCL preprocessing (graphical watermark)](https://lists.cups.org/pipermail/cups/2007-July/041772.html)
-   Next message (by thread): [[cups.general] Cups PCL preprocessing (graphical watermark)](https://lists.cups.org/pipermail/cups/2007-July/041781.html)
-   **Messages sorted by:** [[ date ]](https://lists.cups.org/pipermail/cups/2007-July/date.html#41776) [[ thread ]](https://lists.cups.org/pipermail/cups/2007-July/thread.html#41776) [[ subject ]](https://lists.cups.org/pipermail/cups/2007-July/subject.html#41776) [[ author ]](https://lists.cups.org/pipermail/cups/2007-July/author.html#41776)

---

[More information about the cups mailing list](https://lists.cups.org/mailman/listinfo/cups)