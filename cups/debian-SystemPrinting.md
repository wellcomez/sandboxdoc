---
date: 2023-10-07 23:38
title: debian-SystemPrinting
tags:
  - cups
  - printer
---



# SystemPrinting

- [SystemPrinting](https://wiki.debian.org/SystemPrinting)

[Translation(s)](https://wiki.debian.org/DebianWiki/EditorGuide#translation): [العربية](https://wiki.debian.org/ar/SystemPrinting) - [Ελληνικά](https://wiki.debian.org/el/SystemPrinting) - English - [Français](https://wiki.debian.org/fr/SystemPrinting) - [Italiano](https://wiki.debian.org/it/SystemPrinting) - [Polski](https://wiki.debian.org/pl/SystemPrinting) - [Русский](https://wiki.debian.org/ru/SystemPrinting) - [简体中文](https://wiki.debian.org/zh_CN/SystemPrinting)

---

A basic view of the CUPS Printing system. Aimed at Debian 11 (bullseye) but it should fit most of what is provided by Debian 10 (buster).

目录

1. [Introduction](https://wiki.debian.org/SystemPrinting#Introduction)
2. [Driverless Printing](https://wiki.debian.org/SystemPrinting#Driverless_Printing)
3. [Software Installation](https://wiki.debian.org/SystemPrinting#Software_Installation)
4. [Summary](https://wiki.debian.org/SystemPrinting#Summary)
5. [Print Queue Setup Utilities](https://wiki.debian.org/SystemPrinting#Print_Queue_Setup_Utilities)
6. [The CUPS Web Interface](https://wiki.debian.org/SystemPrinting#The_CUPS_Web_Interface)
7. [system-config-printer](https://wiki.debian.org/SystemPrinting#system-config-printer)
8. [Printer Status and Control](https://wiki.debian.org/SystemPrinting#Printer_Status_and_Control)
9. [Print to a PDF](https://wiki.debian.org/SystemPrinting#Print_to_a_PDF)
10. [Print from Linux client, to CUPS server over network, via CLI](https://wiki.debian.org/SystemPrinting#Print_from_Linux_client.2C_to_CUPS_server_over_network.2C_via_CLI)
11. [CUPS as Print Server for Windows Machines](https://wiki.debian.org/SystemPrinting#CUPS_as_Print_Server_for_Windows_Machines)
12. [CUPS and Samba](https://wiki.debian.org/SystemPrinting#CUPS_and_Samba)
13. [Printer Sharing Using Macintosh as the Client and Debian as the Server](https://wiki.debian.org/SystemPrinting#Printer_Sharing_Using_Macintosh_as_the_Client_and_Debian_as_the_Server)
14. [Printing Pictures](https://wiki.debian.org/SystemPrinting#Printing_Pictures)
15. [See Also](https://wiki.debian.org/SystemPrinting#See_Also)

### Introduction

The Debian printing system has undergone many significant changes over the past few years, with much of the [printer management](https://wiki.debian.org/CUPSPrintQueues) taking advantage of the advances in [modern printer](https://wiki.debian.org/CUPSDriverlessPrinting#intro) technology and the proliferation of [IPP printers](https://wiki.debian.org/PrintingGlossaryandIndex#ipp). [Modern printers](https://wiki.debian.org/CUPSQuickPrintQueues#intro) are catered for by CUPS and supported by [OpenPrinting](https://openprinting.github.io/) initiatives such as [cups-filters](https://openprinting.github.io/projects/). Notwithstanding this, there are still many legacy (non-modern) printers and their [drivers](https://openprinting.org/drivers) in use. This page and [another](https://wiki.debian.org/CUPSPrintQueues) intend to cater for users with both types of printer device.

Legacy (non-modern) printers require printer drivers. Should a user be unsure what is suitable for the printer at hand, a comprehensive set of [free PPDs](https://wiki.debian.org/CUPSPrintQueues#ppd) and drivers would be put on the system with

apt install printer-driver-all

Depending on the printer, it may also be desirable to install one or more of the following:

- [foomatic-db-engine](https://packages.debian.org/foomatic-db-engine "DebianPkg").
    
- [hp-ppd](https://packages.debian.org/hp-ppd "DebianPkg").
    
- [openprinting-ppds](https://packages.debian.org/openprinting-ppds "DebianPkg").
    

The [OpenPrinting website](https://openprinting.org/printers) is a good source of information for matching printers with [free](https://www.debian.org/social_contract) drivers. For a printer that requires a non-free driver a user would have to see [what the manufacturer has to offer](https://wiki.debian.org/CUPSPrintQueues#nonfree).

### Driverless Printing

A proportion of the material on the [Printing Portal](https://wiki.debian.org/Printing) pages is applicable to installing [printer drivers](https://wiki.debian.org/CUPSPrintQueues#driver) (free and [non-free](https://wiki.debian.org/CUPSPrintQueues#nonfree)) and [PPDs](https://wiki.debian.org/CUPSPrintQueues#ppd) and setting up a [print queue](https://wiki.debian.org/CUPSPrintQueues#printqueue) for legacy printers. However, it is as well to be aware that drivers and PPDs are [deprecated in CUPS](https://github.com/OpenPrinting/cups/issues/103) and eventually they will not be catered for as they are now. This has been a long-term objective of the CUPS project for some time.

Users possessing a [modern printer](https://wiki.debian.org/SystemPrinting#intro) are urged to consider the following points and explore a [driverless printing solution](https://wiki.debian.org/CUPSDriverlessPrinting) for their printing needs, whether or not the deprecation is a motivating factor,.

- Driverless printing was introduced to [CUPS](https://wiki.debian.org/CUPSDriverlessPrinting#shortlpadmin) and [cups-browsed](https://wiki.debian.org/CUPSDriverlessPrinting#shortcupsbrowsed) in Debian 9 (stretch).
    
- Support for driverless printing with [CUPS](https://wiki.debian.org/CUPSDriverlessPrinting#shorttempq) and [cups-browsed](https://wiki.debian.org/CUPSDriverlessPrinting#cupsbrowsed) is considerably extended in [Debian 10 (buster)](https://wiki.debian.org/CUPSQuickPrintQueues) and [Debian 11 (bullseye)](https://wiki.debian.org/CUPSDriverlessPrinting#debian).
    
- Printers sold in the last 10 years or so are almost always [AirPrint](https://wiki.debian.org/AirPrint) devices and therefore would [support driverless printing](https://wiki.debian.org/CUPSQuickPrintQueues#intro) when the device is connected by ethernet or wireless. Additionally, a USB connected modern printer might be capable of driverless printing if it is [IPP-over-USB-capable](https://wiki.debian.org/DriverlessPrinting#ippoverusb).
    

### Software Installation

CUPS and cups-filters are central to the printing system and both are installed with

apt install cups

This installation provides a functional printing system that is entirely suitable for use with a [modern printer](https://wiki.debian.org/SystemPrinting#driverless) without any further software installation. Legacy printers would require a [driver installation](https://wiki.debian.org/SystemPrinting#intro).

- [cups-browsed](https://wiki.debian.org/CUPSPrintQueues#cupsbrowsed) is installed as a recommended package.
    
- Modern [USB](https://wiki.debian.org/CUPSDriverlessPrinting#ippoverusb) and [ethernet and wireless](https://wiki.debian.org/CUPSQuickPrintQueues) connected printers on Debian 11 should be detected and auto-setup by cups-browsed.
    
- Debian 10 will handle a modern USB connected printer driverlessly by following [this ipp-usb advice](https://wiki.debian.org/CUPSDriverlessPrinting#summary). Otherwise, [a driver](https://wiki.debian.org/SystemPrinting#intro) will be required.
    
- Modern IPP printers are printers capable of [driverless printing](https://wiki.debian.org/SystemPrinting#driverless); that is, free or non-free vendor packages or plugins are not required.
    

Alternatively, should it be thought necessary or desirable, a user may manually install a print queue (remote or local) with [lpadmin](https://wiki.debian.org/CUPSPrintQueues#lpadmin), the [web interface](https://wiki.debian.org/SystemPrinting#webinterface) of CUPS or [system-config-printer](https://wiki.debian.org/SystemPrinting#scp). Successful printing is usually ensured if a modern printer is [set up as a driverless printer](https://wiki.debian.org/CUPSDriverlessPrinting) or a legacy printer is supported by one of the [installed packages](https://wiki.debian.org/SystemPrinting#intro).

### Summary

- Do you have a [modern or a legacy](https://wiki.debian.org/SystemPrinting#intro) printer? Is there a [driver](https://wiki.debian.org/SystemPrinting#intro) for your legacy printer? Is your modern printer using facilities on buster or bulleye? Do you [appreciate](https://wiki.debian.org/SystemPrinting#driverless) that bullseye handles USB and network connected modern printers whereas buster deals only with networked modern printers? Have you [checked](https://wiki.debian.org/SystemPrinting#utilities) whether cups-browsed has auto-setup a working print queue before making alterations to the system? Are you fully aware that your USB connected modern printer [shouldn't be set up](https://wiki.debian.org/SystemPrinting#local) as a USB printer if it is capable of IPP-over-USB? Why are you using HPLIP and its utilities if you have a modern printer?
    
- A modern printer or [MFD](https://wiki.debian.org/PrintingGlossaryandIndex#mfd) on bullseye should not require a driver, [even for scanning](https://wiki.debian.org/SaneOverNetwork#escl). Packages such as [HPLIP](https://packages.debian.org/HPLIP "DebianPkg") and [printer-driver-escpr](https://packages.debian.org/printer-driver-escpr "DebianPkg") need not be on the system.
    
- A legacy printer device will require a [PPD](https://wiki.debian.org/SystemPrinting#intro), and probably a driver too.
    
- A print queue for a modern printer may be [set up manually](https://wiki.debian.org/SystemPrinting#utilities) if cups-browsed is not installed or there are issues with its behaviour.
    

### Print Queue Setup Utilities

Setup utilities for a manual installation of printers or print queues are:

- [lpadmin](https://wiki.debian.org/CUPSPrintQueues#lpadmin).
    
- The [CUPS web interface](https://wiki.debian.org/SystemPrinting#webinterface).
    
- [system-config-printer](https://wiki.debian.org/SystemPrinting#scp).
    

Should cups-browsed not be on the system or be thought not to be behaving correctly, a print queue may be set up manually. It would be as well to check whether any functioning print queues have been automatically installed by cups-browsed prior to a manual setup. This can be done with

lpstat -a

A non-empty output indicates a local queue has been formed. Such an entry can only be present when cups-browsed has auto-setup a queue or the user has manually set it up.

Looking under the _Printers_ tab of the CUPS web interface or the opening screen of system-config-printer would also reveal whether possible auto-setup by cups-browsed has taken place.

### The CUPS Web Interface

CUPS has a [web interface](https://wiki.debian.org/CUPSPrintQueues#webinterface) for setting up a [print queue](https://wiki.debian.org/CUPSPrintQueues#printqueue) and administering printers. Use a browser to [display the interface](http://localhost:631/) located at _localhost:631_ and to add a printer via the Administration screen. If a username and password are requested, see [here](https://wiki.debian.org/PrintQueuesCUPS#webinterface).

An _Add Printer_ screen for Debian 11 is shown below. Recollect that [ipp-usb is managing a modern printer](https://wiki.debian.org/DriverlessPrinting#debian). An EPSON Stylus Photo RX420, an HP psc 1300 series and an HP ENVY 4500 series are connected by USB to the computer. The ENVY 4500 is the only modern printer. [As will be seen later](https://wiki.debian.org/SystemPrinting#local), this is an important factor in choosing how to set up a print queue for this device.

![printers-2.3.3.jpg](https://wiki.debian.org/SystemPrinting?action=AttachFile&do=get&target=printers-2.3.3.jpg "printers-2.3.3.jpg")

- **Local Printers**
    

Printers that are usually attached to the machine being used are displayed in this category. Most users would be looking to set up a USB connected printer. A [URI](https://wiki.debian.org/CUPSPrintQueues#deviceuri) for such a device is displayed after _Continue_ is activated. The legacy psc 1300 has two entries; it is probably best to choose the entry that has a URI beginning _hp:/usb_ as this will give [easier access](https://wiki.debian.org/Scanner#aio) to the scanner on the device. With a [driver](https://wiki.debian.org/PrintQueuesCUPS#ppd) on the system or supplied by the user the remainder of the queue setup for the legacy psc 1300 and EPSON Stylus should proceed successfully to completion. Administration of the queue from the web interface includes facilities to modify or delete it, set default options for it, pause or resume printing, reject jobs and edit [/etc/cups/cupsd.conf](https://manpages.debian.org/man/cupsd.conf "DebianMan").

Now look carefully at the _Discovered Network Printers_ category and note that the ENVY 4500 is listed again. This entry would not be displayed at all on buster and it is not displayed on bullseye unless the printer understands the [IPP-over-USB protocol](https://wiki.debian.org/DriverlessPrinting#ippoverusb). bullseye has [ipp-usb](https://wiki.debian.org/CUPSDriverlessPrinting#ippoverusb) installed and this effectively turns the modern IPP-over-USB-capable ENVY 4500 into a network device, with the consequence that

The USB entry for the ENVY 4500 [cannot be used](https://wiki.debian.org/CUPSDriverlessPrinting#debian) to set up a **working** print queue when bullseye is the OS.

- **Discovered Network Printers**
    

The second section is for printers or print queues that have been discovered on the local network. Entries with @ in their description are print queues advertised by a remote CUPS server. An entry without the @ indicates a printer. The same print queues are automatically discovered by [cups-browsed](https://wiki.debian.org/PrintQueuesCUPS#cups-browsed) and become available as local queues, making it unnecessary for them to be set up from the web interface unless that is what is wanted.

A discovered network print queue/printer should be capable of being added just as easily as a USB printer. Its [connection](https://wiki.debian.org/PrintQueuesCUPS#deviceuri) will be worked out for you and given on the second page. If the connection begins [dnssd://](https://wiki.debian.org/PrintQueuesCUPS#dnssd) you should select _RAW_ as the _Make/Manufacturer_ on the third page. The server receiving the job will be doing the filtering so it should be [sent there unprocessed](https://wiki.debian.org/CUPSPrintQueues#doublefiltering). Unless the printer itself can process the [document format](https://wiki.debian.org/PrintingGlossaryandIndex#pdl) of submitted jobs a [PPD](https://wiki.debian.org/PrintQueuesCUPS#ppd) will have to chosen for the setting up of it.

Regarding modern printers that understand the [IPP-over-USB protocol](https://wiki.debian.org/DriverlessPrinting#ippoverusb) connected by USB on bullseye: look under the model/driver entries on the third page for entries containing _driverless_ or _IPP Everywhere_. [Do not expect to get a working print queue](https://wiki.debian.org/SystemPrinting#local) by using the printer's entry under _Local Printers_.

- **Other Network Printers**
    

The _Other Network Printers_ section requires you to specify the destination for the remote print queue/printer, which could be on the local network or many kilometres away. [AppSocket](https://wiki.debian.org/PrintQueuesCUPS#networkuri) is almost always available on a network printer and other devices and requires only the IP address of the printer and a port number. An [Internet Printing Protocol (IPP) URI](https://wiki.debian.org/PrintQueuesCUPS#networkuri) is the preferred choice for connecting to another CUPS server because it is CUPS' native protocol. A [Line Printer Daemon (LPD) URI](https://wiki.debian.org/PrintQueuesCUPS#networkuri) could be considered if the remote printing service does not support IPP satisfactorily or at all. [As before](https://wiki.debian.org/SystemPrinting#networkdiscovered), when a remote print queue is doing the filtering, choose _RAW_ as the _Make/Manufacturer_.

### system-config-printer

[system-config-printer](https://packages.debian.org/system-config-printer "DebianPkg") is an administration tool that functions in a similar way to the [CUPS web interface](https://wiki.debian.org/SystemPrinting#webinterface) for configuration of printers and print queues, but it is a native application rather than a web page.

Opening system-config-printer presents a screen that is empty or that shows installed print queues. The installed print queues will have been auto-setup by cups-browsed or manually set up by the user. If none of these entries gives a working print queue or the expected printer is not displayed, it may be appropriate to add another queue.

Adding another print queue is a matter of activating the _Add_ button. Below is the outcome from this action when an EPSON Stylus Photo RX420, an HP psc 1300 series and an HP ENVY 4500 series are connected by USB to the computer:

![scp2020-1.jpg](https://wiki.debian.org/SystemPrinting?action=AttachFile&do=get&target=scp2020-1.jpg "scp2020-1.jpg")

The differences between this display and what is shown in the [CUPS web interface](https://wiki.debian.org/SystemPrinting#webinterface) are slight. For example, the HP psc 1300 is shown only once, but, when clicked on, two URIs will be offered for selection. [As before](https://wiki.debian.org/SystemPrinting#local), the URI beginning _hp:/usb_ is the recommended URI to use. Even after reading about the [CUPS web interface](https://wiki.debian.org/SystemPrinting#webinterface) it is worth repeating:

Regarding modern printers that understand the [IPP-over-USB protocol](https://wiki.debian.org/DriverlessPrinting#ippoverusb) connected by USB on bullseye: look under Network Printer and use the connection for _Driverless IPP_. [Do not expect to get a working print queue](https://wiki.debian.org/SystemPrinting#local) by using the printer's entry in the first _Devices_ section.

### Printer Status and Control

For a full picture of whether the scheduler (cupsd) is running, what the system default destination is, whether a local queue is accepting jobs and what is yet to be printed you can use [lpstat](https://manpages.debian.org/man/lpstat "DebianMan"):

~~~
lpstat -t
~~~

On Debian 10

~~~
lpstat -l -e
~~~

will show all available destinations (local and remote).

For displaying or setting print queue options and defaults:

lpoptions -p <print_queue_name> -l

Stopping and starting print queues. Acceptance and rejection of jobs sent to a destination:

~~~
cupsdisable <print_queue_name> 
cupsenable <print_queue_name>
cupsaccept <print_queue_name>
cupsreject <print_queue_name>
~~~

Cancel all jobs on a destination and additionally delete job data files:

~~~
cancel -a <print_queue_name>
cancel -a -x <print_queue_name>
~~~

### Print to a PDF

The primary purpose of a printing system is to put toner or ink on a medium and have an output which is professional looking and suitable for the user's needs. However, the files produced at various stages of the [filtering process](https://wiki.debian.org/CUPSDebugging#The_PDF-centric_Workflow) can also be of [interest and use](https://wiki.debian.org/ThecupsfilterUtility) to a user. For example, there was time when converting a file to a PDF from an application was not the most straightforward of tasks. A solution was to use [cups-pdf](https://packages.debian.org/stable/printer-driver-cups-pdf "DebianPkg") as a virtual PDF printer in the application's print dialog. The output PDF is not sent to a real printer but stored on disk. Since then many applications have acquired _Print to PDF_ and _Export to PDF_ options in their dialogs and the situation has improved.

cups-pdf is a [backend filter](https://manpages.debian.org/man/7/backend "DebianMan") which takes a PostScript file and converts it to a PDF with [Ghostscript](https://packages.debian.org/stable/ghostscript "DebianPkg"). It works in conjunction with the CUPS filtering system, which becomes responsible for producing the needed PostScript using _/etc/cups/ppd/PDF.ppd_ and passing it to the backend. The package containing the backend and the PPD is installed with

  apt install printer-driver-cups-pdf

The installation sets up a [print queue](https://wiki.debian.org/PrintQueuesCUPS) with the name PDF. The queue will appear as a printer in an application's print dialog and in the output of _lpstat -t_. Printing from the command line is done with

  lp -d PDF <file_to_be_converted>

The PDF file produced is stored in a user's home directory in the PDF directory, created after the first ever job is sent.

A PDF may or may not be searchable or capable of having text copied from it. PDFs produced from PostScript files submitted to a queue generally do have these features; PDFs from text files do not because they are first filtered by [texttopdf](https://wiki.debian.org/ThetexttopdfFilter#Using_texttpdf_to_Produce_PDFs).

Alternative methods for PDF production can be based on using [cupsfilter](https://wiki.debian.org/ThecupsfilterUtility) and [cups-tea4cups](https://wiki.debian.org/Tea4CUPS#Virtual_PDF_printer).

### Print from Linux client, to CUPS server over network, via CLI

In this example we have a router, 2 computers (client and server), and a printer. The computers are connected to the router, the Linux-compatible printer is connected to the server via USB ("do one thing and do it well"), and we want to be able to send things to the printer from the client, over the network.

Though some may prefer the convenience of driverless to this setup, others may prefer to [avoid proprietary firmware](https://media.ccc.de/v/35c3-9462-what_the_fax).

The server must already be setup with:

- CUPS installed via apt install cups
    
- CUPS is running - check service cups status
    
- A printer has been added to CUPS (e.g. via hp-setup -i if you are using a HP printer, or by directly using lpadmin - instructions vary by manufacturer)
    
- The port 631 is not blocked (i.e. by firewall or missing IP address)

This tutorial uses the following example variables, and you will need to edit them for your own setup:

- Server IP: 10.0.0.2
    
- CUPS printer name on server: example_printer
    
- Client non-root user: example_user
    

On the server, allow remote connections:
~~~
# cupsctl --remote-admin --remote-any --share-printers

Note: this command makes configuration edits in /etc/cups/ and restarts the CUPS service

You should now be able to connect to the server via the client on the network, by making a HTTP request via a web browser or curl, to [http://10.0.0.2:631](http://10.0.0.2:631/)

Now, on the client, to be able to send data to the server, install the CUPS package, and start it:

# apt install cups
# service cups start
~~~

Notice that the CUPS software is needed on _both_ the server and client.

Next, on the client, so that printing commands are sent to the right place, add the following:

~~~
# echo 'ServerName 10.0.0.2' > /etc/cups/client.conf
# chmod o+r /etc/cups/client.conf

Note: the o+r permission is essential to allow non-root users to print.

The example user on the client needs to be added to the lp group:

# usermod -a -G lp example_user
~~~

We should now be able to see the printer from the client, from the example user:

~~~
example_user $ lpstat -t
scheduler is running
device for example_printer: manufacturer_name:/usb/full_product_name
~~~

Now, to print, it is as simple as:

example_user $ lp -d example_printer file.pdf
request id is example_printer-1 (1 file(s))

Important note: a CUPS server for infrequent use has very low processor/RAM/disk requirements, so please use an energy-efficient computer.

### CUPS as Print Server for Windows Machines

You can setup CUPS to allow Windows machines to print to a CUPS server using an http address.

First, install the [samba](https://packages.debian.org/samba "DebPkg") package. When you are asked to use WINS, say **yes**.

~~~
$ apt install samba

Next setup your /etc/cups/cupsd.conf file.

# Our private connection we listen to
Listen *:49631
# Allow machines on local network to use printers
<Location /printers>
  Order allow,deny
  Allow 192.168.0.*
  Allow 192.168.1.*
</Location>
~~~

This will listen on port 49631 from any network. You may use some other port number besides 631. Note that the dynamic and/or private ports as specified by the [IANA](http://www.iana.org/assignments/port-numbers) are in the range 49152 through 65535. Also, this will only allow computers from the local network to print to the CUPS printers.

Next, restart the CUPS daemon

~~~
# service cups restart
~~~

Now on each Windows machine, Choose that you want to install a network printer and that you want to install a printer on the Internet or home/office network. The URL you will use should be:

http://<cups_server_hostname>:49631/printers/<printer_name>

Lastly, the driver to install is in the Generic section labeled **MS Publisher Color Printer**.

### CUPS and Samba

This section needs updating as you can setup print server via samba print sharing just by uploading drivers.

When printing to windows printers in an NT domain using SMB the Device URI should read

  smb://username:password@domain/server/printername

This allows Samba to authenticate against a domain controller for acces to the printer queue.

You may also omit the username:password@ part when the server does not need authentification. As the hostname both, a windows computer name or an IP address would work. To determine the printername you may use the smbclient command line tool. Run the following command, where computername refers to the name of the machine that shares the printer:

~~~
  smbclient -L computername
~~~

Check the ouput for entries with Type "Printer":

~~~
$ smbclient -L base
        Sharename       Type      Comment
        ---------       ----      -------
        print$          Disk      Druckertreiber
        HPOffice        Printer   HP Officejet J4500 Series
        ADMIN$          Disk      Remoteadmin
        C$              Disk      Standardfreigabe
~~~

The resulting URL from the output above would be

~~~
  smb://base/HPOffice
~~~

When printing to a Debian CUPS printer from a machine that connects through Samba, you may need to [set up a CUPS class](http://www.owlfish.com/thoughts/winipp-cups-2003-07-20.html) to get things to work.

### Printer Sharing Using Macintosh as the Client and Debian as the Server

You have a printer on a debian machine and you want to share it with a Macintosh, so that the Mac can print to the Debian-based printer. Try this:

1. Use cups to set up the printer on Debian as described above. The Gnome tool for printer configuration works well, and the 100% bullet-proof way is to use the browser-based CUPS configuration.

2. On the Mac (OS X 10.4+) start Print and Fax from System Preferences. Use the + button to add a printer.

3. Look first in the "Default" tab. If the automagic printer-sharing has worked, and your Mac is connected to the local network properly, then the Debian-based printer should already be visible in the list. Just select it and use the recommended print driver. Your results may vary but you can't go to far wrong if you pick a driver that matches the one you are using on Debian. So, for example, you might like to try using Gutenprint drivers at both ends.

4. If your printer is **not** visible in the Default tab, then try adding it on the "IP" tab. Pick IPP as the protocol, give the plain IP address of the server in the address box, and in the Queue box put "printers/<debian-queue-name>". Put whatever helps you identify the printer in the Name and Location boxes, and choose a printer driver than matches what you used to set up the printer on Debian. Finally cross your fingers, hold your breath, recite a prayer/spell/incantation, and press the Add button.

As they say: your mileage may vary.

### Printing Pictures

Professional looking photos may be obtained when using high quality glossy photo paper on an inkjet printer but care must be taken when submitting the print job. Printing from The Gimp image manipulation program produces very good results. The print dialog will vary depending upon the printer used. The most important settings are type of paper, glossy photo or whatever sort you are using, and image type, "photograph". The size of the printed image is adjusted in the printer dialog as well.

### See Also