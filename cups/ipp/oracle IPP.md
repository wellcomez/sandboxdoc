---
date: 2023-10-06 16:59
title: oracle IPP
tags:
  - ipp
  - cups
---



https://docs.oracle.com/cd/E38902_01/html/820-5488/gdtcv.html#scrolltoc

## 支持使用 PPD 文件设置打印机

Oracle Solaris 打印子系统中已并入了使用 PPD 文件设置和管理打印机的支持。提供了两个接口脚本，即 standard_foomatic 和 netstandard_foomatic。这些接口脚本提供 Oracle Solaris 假脱机程序与打印服务器的后端进程之间的通用接口。

以下是支持的打印机类型的示例：

- Lexmark Optra E312
    
- Epson Stylus Photo 1280
    
- Canon BJC-55
    
- QMS magicolor 2+
    

### 光栅图像处理器支持

借助 Oracle Solaris OS 中的光栅图像支持 (Raster Image Support, RIP)，可以打印到不具备驻留 PostScript 处理功能的打印机。打印软件提供了打印服务器 RIP 和支持技术。RIP 在后台进行处理。但是，要使用适当的驱动程序，需要使用 Oracle Solaris 打印管理器或 lpadmin -n 命令来配置每台打印机。有关使用 lpadmin -n 命令的逐步说明，请参见[如何使用 LP 打印命令添加新的直接连接的打印机](https://docs.oracle.com/cd/E38902_01/html/820-5488/gfhdc.html#ertru)。

### 对 PostScript 打印机描述文件的支持

lpadmin 和 lpstat 命令，以及 Oracle Solaris 打印管理器打印机定义屏幕都支持使用 PPD 文件。

以下新软件包与此功能相关联：

- SUNWa2psr
    
- SUNWa2psu
    
- SUNWespgs
    
- SUNWffiltersr
    
- SUNWffiltersu
    
- SUNWfppd
    
- SUNWgimpprint
    
- SUNWhpijs
    
- SUNWimagick
    
- SUNWpsutils
    

### 在何处查找有关支持的打印机和可用 PPD 文件的信息


|   |   |
|---|---|
|![注意](https://docs.oracle.com/cd/E38902_01/html/820-5488/graphics/caution.gif "注意")|**注意 -** PPD 文件和 ppdcache 文件的存储位置是专用的，ppdcache 文件的内容也是专用的。这些文件的位置和 ppdcache 的内容可能会更改。请不要生成依赖于当前位置的这些文件或依赖于当前所用格式的数据的软件。|


如果打印机需要的文件不可用，则可以添加自己的 PPD 文件。如果使用 lpadmin -n 命令创建新的打印队列，则可以将自己的 PPD 文件存储在所选的任何位置。如果您运行的是 Oracle Solaris 10 OS，并且使用 Oracle Solaris 打印管理器来创建打印队列，则 PPD 文件必须在 ppdcache 文件中有一个对应项。

如果您运行的是受支持的 Oracle Solaris 发行版，则 PPD 文件位于系统上的以下四个系统信息库中的任何一个：

/usr/share/ppd

指定 system 系统信息库。

/usr/local/share/ppd

指定 admin 系统信息库。

/opt/share/ppd

指定 vendor 系统信息库。

/var/lp/ppd

指定 user 系统信息库。

结合使用 lpadmin 命令和 -n 选项或结合使用 -a 选项和 ppdmgr 命令指定的 PPD 文件的副本存储在 user 系统信息库中，并以相同的 PPD 文件名命名。

如果您使用带有 -a 和 -R 选项的 ppdmgr 实用程序，则指定的 PPD 文件的副本可存储在 admin 系统信息库中。

如果使用 Oracle Solaris 打印管理器创建打印队列，并且 ppdcache 文件中不存在与 PPD 文件对应的项，则可以使用 ppdmgr 实用程序将该文件添加到系统中。Oracle Solaris 打印管理器使用的 PPD 文件信息的高速缓存然后将会更新，以反映使用这两种方法之一所做的任何更改。

在以后的 Oracle Solaris 发行版中，PPD 文件位于 /usr/lib/lp/model/ppd/system 目录或任何由您指定的备用目录。

ls 命令的输出列出特定打印机制造商的所有 PPD 文件。

有关其他与任务相关的信息，请参见[管理与 PPD 文件关联的打印机（任务列表）](https://docs.oracle.com/cd/E38902_01/html/820-5488/gfjno.html#scrolltoc)。

|   |
|---|
|版权所有 © 2006, 2011, Oracle 和/或其附属公司。 保留所有权利。 [法律声明](https://docs.oracle.com/cd/E38902_01/html/820-5488/docinfo.html#scrolltoc)|
## PPD 文件管理实用程序

本节包含有关管理 PPD 文件的参考信息。

使用 ppdmgr 实用程序将 PPD 文件添加到系统时，指定的 PPD 文件的压缩（采用 gzip）副本将存储在系统中。其目的是维护系统上所有已知 PPD 文件中的 PPD 文件信息的当前高速缓存。

复制到系统的 PPD 文件的完整路径如下：

repository/label/manufacturer/ppd-file-name

repository

是指定的系统信息库。如果没有使用 -R 选项指定系统信息库，则缺省系统信息库为用户系统信息库 /var/lp/ppd/。

label

是指定的标签。如果没有使用 -L 选项指定标签，则缺省标签为 user 系统信息库中的 user。

manufacturer

是 PPD 文件中包含的制造商的名称。此名称可能会根据 /var/lp/ppd/manufaliases 文件中定义的制造商别名进行修改。请参见[生产商别名文件](https://docs.oracle.com/cd/E38902_01/html/820-5488/geqnf.html#gfhxx)。

ppd-file-name

与使用 ppdmgr 实用程序指定的原始 PPD 文件名相同。如果压缩了 PPD 文件，则此文件还可以包含 .gz 扩展名。

下图显示了典型 PPD 文件系统信息库的布局以及 ppdmgr 实用程序目录布局（其中包含 ppdmgr 提供和生成的所有相关文件，包括 PPD user 文件系统信息库）。

**图 12-1 PPD 文件系统信息库布局**

![image:图中显示 PPD 文件系统信息库的目录布局以及所提供和生成的 ppdmgr 文件的目录布局。](https://docs.oracle.com/cd/E38902_01/html/820-5488/figures/ppdfile_dir_tree3.png "图中显示 PPD 文件系统信息库的目录布局以及所提供和生成的 ppdmgr 文件的目录布局。")

### PPD 文件系统信息库

下表介绍了位于系统中的 PPD 文件系统信息库。

**表 12-12 PPD 文件系统信息库的说明**

|系统信息库|位置|内容|用于添加或修改的方法|
|---|---|---|---|
|admin|/usr/local/share/ppd/|此 PPD 文件系统信息库用于存储由系统管理员使用的 PPD 文件。|可以使用 ppdmgr 实用程序或 pkgadd 命令将 PPD 文件手动添加到此系统信息库中。|
|all|表示系统上的所有 PPD 系统信息库|此系统信息库表示系统上所有受支持的 PPD 系统信息库位置。|请求使用 ppdmgr 实用程序更新或重新生成 PPD 高速缓存文件时，只能指定 all 系统信息库。|
|system|/usr/share/ppd/|此系统信息库包含随 Oracle Solaris 一同提供的 PPD 文件。|可以使用 pkgadd 和 patchadd 命令将 Oracle 提供的 PPD 文件添加到 system 系统信息库中。

不应手动或使用 ppdmgr 实用程序来修改此系统信息库中的 PPD 文件。如果手动修改此系统信息库，则所做的更改可能会丢失。|
|user|/var/lp/ppd|此系统信息库由管理员和具备相应特权（打印机管理）的用户按照需要使用。|除非另行指定，否则使用带 -a 选项的 ppdmgr 命令添加到系统的 PPD 文件将被添加到该系统信息库。|
|vendor|/opt/share/ppd/|此系统信息库是用于存储由供应商提供给 Oracle Solaris 的 PPD 文件的中央位置。|可以使用 pkgadd 命令将 PPD 文件添加到此系统信息库。


**注 -** 不能使用 ppdmgr 实用程序修改此系统信息库。


|---|---|
|![注意](https://docs.oracle.com/cd/E38902_01/html/820-5488/graphics/caution.gif "注意")|**注意 -** PPD 文件和 ppdcache 文件的位置是专用的，因此可能会更改。请**不要**生成依赖于当前位置的这些文件或当前格式的数据的软件。|

---

### PPD 文件标签

在运行 Oracle Solaris 软件的系统上，可以将 PPD 文件存储在缺省标签目录中。您还可以指定自己选择的标签以组织 PPD 文件，只要该标签未被系统保留。

以下标签名是保留名称：

- caches
    
- ppdcache
    
- manufaliases
    
- all
    

除了 all 标签名以外，不能使用 ppdmgr 实用程序的 -L 或 -R 选项来指定这些标签名。不过，在使用 r 和 -u 选项时可以使用 -L 或 -R 选项来指定 -all 标签名。以 SUNW 开头的任何标签名都被保留供 Oracle 使用，但是并不禁止使用。

如果将 PPD 文件添加到系统中并且指定了一个并不存在的标签，则将在指定的系统信息库中创建具有该标签名的目录。缺省情况下，如果没有指定 PPD 文件系统信息库，则此目录为 /var/lp/ppd/label。有关在将 PPD 文件添加到系统时指定标签的更多信息，请参见[ppdmgr 实用程序的命令行选项的说明](https://docs.oracle.com/cd/E38902_01/html/820-5488/geqnf.html#geqok)。

### Oracle Solaris 打印管理器中的打印机驱动程序字段的说明

选择 "Add New Printer (attached or network)"（添加新的打印机（连接或网络））或 "Modify Printer Attributes (attached or network)"（修改打印机属性（连接或网络））菜单选项时，将显示 Oracle Solaris 打印管理器中的 "Printer Driver"（打印机驱动程序）字段。根据您所选择的打印机型号，此字段包含 PPD 高速缓存文件中的打印机驱动程序描述。为了区分 PPD 文件系统信息库中具有不同标签的重复打印机驱动程序描述，还会显示 PPD 文件系统信息库名称的标签和缩写。

打印机驱动程序描述所采用的格式如下：

label(repository-letter): driver-description

例如，以下 PPD 文件位于 user PPD 文件系统信息库中的 PHOTOS 标签中：

/var/lp/ppd/PHOTOS/HP/HP-PhotoSmart_P1100-hpijs.ppd.gz

此 PPD 文件会出现在 Oracle Solaris 打印管理器的 "Printer Driver"（打印机驱动程序）字段选择列表中，如下所示：

PHOTOS(U): Foomatic/hpijs（推荐）

在以下示例中，以下 PPD 文件位于 system PPD 文件系统信息库中的 SUNWfoomatic 标签中：

此 PPD 文件会出现在 Oracle Solaris 打印管理器的 "Printer Driver"（打印机驱动程序）字段选择列表中，如下所示：

SUNWfoomatic(S): Foomatic/hpijs（推荐）

下表介绍了 PPD 文件系统信息库字母、这些字母所代表的系统信息库，以及这些系统信息库在系统中所处的位置。

|系统信息库缩写|系统信息库名称|系统信息库位置|
|---|---|---|
|A|admin|/usr/local/share/ppd|
|S|system|/usr/share/ppd|
|U|user|/var/lp/ppd|
|V|vendor|/opt/share/ppd||

### 生产商别名文件

生产商目录（每个生产商都有一个对应的目录）位于系统上的 PPD 系统信息库中。将 PPD 文件添加到系统时，使用 PPD 文件中包含的生产商名称来确定要将该 PPD 文件复制到哪个生产商目录中。专用文件 /var/lp/ppd/manufaliases 包含 PPD 文件中的所有生产商项的别名。引用 manufaliases 文件的目的是确定要将该 PPD 文件复制到哪个生产商目录中。此过程可确保每个生产商都有一个目录，而不是每个生产商别名都有一个目录。例如，如果某个 PPD 文件包含生产商名称 Hewlett-Packard，并且 Hewlett-Packard 的 HP 别名列在 manufaliases 文件中，则该 PPD 文件会存储在 HP 目录中。此策略适用于使用 ppdmgr 实用程序和 lpadmin - n 命令添加到系统的所有 PPD 文件。


**注 -** manufaliases 文件是专用文件。请**勿**编辑此文件。请**不要**生成依赖于当前位置的文件或当前格式的数据的软件。


### PPD 文件高速缓存

专用的 PPD 文件高速缓存在 /var/lp/ppd/caches/ 目录（每个系统信息库中的每个标签都有一个对应目录）中进行维护。

使用的高速缓存文件名的格式如下：

PPD-repository: label

PPD 高速缓存文件中有关 PPD 文件的信息使用 ppdmgr 实用程序进行维护。请**勿**手动编辑 PPD 高速缓存文件。请注意，/var/lp/ppd/caches 目录中的高速缓存文件用于生成专用 PPD 高速缓存文件 /var/lp/ppd/ppdcache。此文件由 printmgr 实用程序使用。有关更多信息，请参见 [printmgr(1M)](http://www.oracle.com/pls/topic/lookup?ctx=E26505&id=REFMAN1Mprintmgr-1m) 手册页。


**注 -** ppdcache 及其内容的位置是专用的。请**不要**生成依赖于当前位置的此文件或依赖于当前格式的数据的软件。此信息适用于生成或提供以供 ppdmgr 实用程序使用的任何专用文件。


### ppdmgr 实用程序的命令行选项的说明

本节介绍了 ppdmgr 实用程序的命令行选项。此外，还介绍了有关使用 ppdmgr 实用程序管理 PPD 文件的过程、指导和限制的其他信息。

PPD 管理器 (ppdmgr) 实用程序位于 /usr/sbin/ppdmgr 中。

**要将 PPD 文件添加到系统中，应使用以下语法：**

**ppdmgr -a ppd-file-path**

-a 选项可用于将在 ppd-file-path 中指定的 PPD 文件复制到 PPD 系统信息库中，然后更新 PPD 高速缓存文件，以反映所做的更改。如果您没有使用 -R 选项指定 PPD 文件系统信息库，则 PPD 文件会存储在 user PPD 文件系统信息库中。如果您没有使用 -L 选项指定标签，则 PPD 文件会存储在 user 标签目录中。

结合使用 -a 选项和 ppdmgr 实用程序时，将会执行以下验证：

- **标签验证**－标签名**不**得是保留的标签名。
    
    以下标签名是保留名称：
    
    - caches
        
    - ppdcache
        
    - manufaliases
        
    - all
        
- **PPD 文件路径验证**－指定的 ppd-file-path 必须可访问，且必须包含 .pdd 或 ppd.gz 扩展名。
    
- **PPD 文件验证**－在 ppd-file-path 中指定的 PPD 文件必须是有效的 PPD 文件。
    

---

**注 -** 如果您提供的信息未通过各种验证检查，或者如果 ppdmgr 实用程序执行的任何操作未成功，则将显示一条错误消息，且该实用程序将退出。

---

**执行的其他操作：**

- 如果需要，将会创建目标路径的父目录。
    
- 如果扩展名为 .gz 的 PPD 文件版本已存在于 PPD 文件系统信息库中，且压缩的版本不重复，则将显示一条错误消息。
    
- ppd-file-path 将被复制到目标路径。
    
- 为反映 ppdcache 文件中的更改，随后将应用更新操作。
    

**要指定 PPD 文件系统信息库，应使用以下语法：**

**ppdmgr -R repository**

带有 repository 的 -R 选项用于标识受支持的 PPD 文件系统信息库之一。如果未指定 -R 选项，则缺省的 repository 为 user。随 -a 选项一起指定 -R 选项时，有效的系统信息库名称为 user 和 admin。有关所有受支持的系统信息库名称及其位置的更多信息，请参见[PPD 文件系统信息库](https://docs.oracle.com/cd/E38902_01/html/820-5488/geqnf.html#geqmb)。

**要指定标签，应使用以下语法：**

**ppdmgr -L label-name**

带有 label-name 的 -L 选项用于标识 PPD 文件系统信息库中的一组 PPD 文件。标签也是位于 PPD 文件系统信息库中的目录的名称。标签可由可移植字符集中的任何字符组成。但是，标签不能包含分号 (;)。

如果**没有**指定 -L 选项，则以下为用于指定标签名的缺省选项。

|ppdmgr 命令行选项|缺省标签|
|---|---|
|-a|如果 ppd-file-path 位于受支持的系统信息库中，则缺省为 ppd-file-path 中的 label。否则，label 缺省为 user。|
|-r|缺省为 all 标签。|
|-u|缺省为 all 标签。||

**要请求更新 PPD 高速缓存文件，应使用以下语法：**

**ppdmgr -u**

此选项可用于更新高速缓存文件，以反映 PPD 文件系统信息库中的修改。只有在检测到修改时，才会更新 PPD 高速缓存文件。

---

**注 -** 如果指定了 -a 选项，则将自动更新 PPD 高速缓存文件，以反映在其中复制 PPD 文件的系统信息库中标签目录中的更改。

---

如果未指定 -R 或 -L 选项，则将更新 PPD 高速缓存文件，以反映 user 系统信息库中 all 标签目录中的修改。

**要请求重新生成 PPD 高速缓存文件，应使用以下语法：**

**ppdmgr -r**

-r 选项通过删除并重新生成与指定的 PPD 文件系统信息库中指定标签关联的任何中间高速缓存文件，来重新生成高速缓存。如果删除了任何中间高速缓存文件，则此操作会导致更新 PPD 高速缓存文件 /var/lp/ppd/ppdcache。由于需要在指定的 PPD 文件系统信息库重新生成指定的标签，因此重新生成 PPD 高速缓存信息可能非常耗时。重新生成 PPD 高速缓存文件所用的时间取决于受影响的 PPD 文件数目。因此，仅在怀疑 PPD 高速缓存文件损坏时才应使用 -r 选项。

如果没有指定 -R 或 - L 选项，则将删除与 user PPD 文件系统信息库中所有标签关联的中间高速缓存。这些修改随后将反映在 PPD 高速缓存文件中。

**要显示系统信息库中 PPD 文件的完整路径，应使用以下语法：**

**ppdmgr -a ppd-file-path -w**

-w 选项必须随 -a 选项一起指定 ，并将 PPD 文件成功添加到系统中，PPD 文件的完整目标路径才会显示在 stdout 中。否则，将忽略此选项。
## IPP 侦听服务概述

IPP 侦听服务（也称为**侦听程序**）提供了一种 IPP 网络协议服务，该服务为打印客户机系统提供一种与运行侦听程序的系统上的打印服务进行交互的方法。此侦听程序实现了服务器端 IPP 支持，其中包括一组标准操作和属性。此侦听程序是在 Oracle Solaris 中作为 Apache 模块和一系列共享库（包含 IPP 操作和网络支持）实现的。在系统上安装 Oracle Solaris OS 时，将会安装 IPP 软件栈。IPP 侦听服务是一个 SMF 服务，它依赖于要运行的打印服务。因此，在添加第一个打印队列后，会在打印服务器上自动启用 IPP。同时，在删除最后一个打印队列后，也会禁用该服务。

在前端，IPP 服务器支持位于 HTTP Version 1.1 上面的一层。服务器通过 HTTP POST 请求接收 IPP 操作。然后，服务器执行所请求的操作，并通过 HTTP 将响应发回到客户机。这些操作包括但不限于提交和取消打印作业，以及查询打印机和已排入打印机队列的某个或全部打印作业的属性。在后端，IPP 侦听程序通过与打印假脱机程序进行通信来执行操作。在 Oracle Solaris OS 中，此假脱机程序当前是 lpsched 守护进程。

### IPP 侦听服务的工作原理

IPP 侦听服务实现（服务器端支持）嵌入在 Apache Web 服务器下方。Web 服务器通过 HTTP POST 请求接收 IPP 操作。接收到 HTTP POST 请求后，会将其传递到 Apache IPP 模块 (mod_ipp.so)。根据配置情况，Apache Web 服务还可以提供一种验证服务，并用于在打印客户机与服务器之间进行加密。侦听服务作为其自己的专用 Apache 实例来运行。

此过程如下所示：

1. 将 IPP 请求从客户机发送到服务器。
    
2. Apache Web 服务器接受此连接。
    
3. Apache Web 服务器随后将连接传递到 mod_ipp。
    
4. mod_ipp 将连接和配置数据传递到 libipp-listener。
    
5. libipp-listener 使用 lipipp-core 读取请求。
    
6. libipp-listener 将请求分发到位于 lipipp-listener 中的操作处理程序。
    
7. 操作处理程序将请求转换为 PAPI 调用，然后进行该调用。
    
8. 通过使用 psm-lpsched，将 PAPI 调用转换为特定于打印服务的请求。
    
9. 打印服务响应该请求。
    
10. psm-lpsched 命令将响应转换为 PAPI 结果。
    
11. libpapi 操作返回到 libipp-listener 操作处理程序。
    
12. libipp-listener 操作处理程序将结果传递给分发程序。
    
13. libipp-listener 分发程序使用 libipp-core 库将结果写入到客户机。
    
14. 分发程序返回 mod_ipp 入口点。
## IPP 组件

下表介绍了 Oracle Solaris OS 中构成 IPP 支持的组件。

**表 A-1 IPP 组件**

|组件|功能|
|---|---|
|httpd|Apache Web 服务器。它提供了一个 HTTP 传输侦听程序，用于在 IANA 注册 IPP 端口 tcp/631 上侦听 HTTP 请求。一旦收到请求，会将请求传递到 IPP Apache 模块。|
|mod_ipp.so|Apache IPP 模块。此 Apache 模块检查客户机的 HTTP 请求，以确定它是否类似于 IPP 请求（application/ipp 的 mime-type 和 HTTP POST 操作）。一旦确定它是 IPP 请求，则会将其传递到 IPP 侦听程序库。此模块还引入和处理了特定于 IPP 的 Apache 配置指令。|
|libipp-listener.so|IPP 侦听程序库。此库利用核心 IPP 编组库来解码 IPP 请求，并将其分发给它的一个 IPP 操作实现函数。这些函数将 IPP 请求转换为 PAPI 调用，以便与本地打印服务进行交互。进行处理后，侦听程序库对请求进行编码，并将结果发回到发出请求的客户机。|
|libipp-core.so|IPP 编组库对 IPP 字节流进行解码和编码，以通过线路进行接收和传输。|
|libpapi.so|PAPI 库为应用程序（如 IPP 侦听服务）提供了一种与打印服务进行交互的方法。||

### IPP 库

**IPP 侦听服务库** (libipp-listener)－是进行大部分协议请求处理的位置。该库使用核心 IPP 库 libipp-core.so 读取和验证请求。在验证请求后，会将请求转换为一系列客户机 API 调用。然后，使用核心 IPP 库将这些调用的结果转换为相应的 IPP 响应。Web 服务器会将响应返回到客户机系统。侦听服务库的接口是一个特定于 IPP 服务器端实现的项目专用接口。

**IPP 核心库** (libipp-core.so)－在客户机操作与服务器操作之间共享。IPP 核心库包含一些用于读取和写入协议请求和响应的例程。该库将在标准二进制表示形式和一组通用数据结构之间转换 IPP 请求和响应数据。从根本上讲，这种通用数据表示形式用于将请求转换为打印服务中立的表示形式，或者从打印服务中立的表示形式进行转换，并通过通用打印接口 libpapi.so 传递这种通用数据表示形式。由于客户端和服务器端 IPP 支持都必须执行此功能，因此，客户机和服务器共享此 IPP 核心库。

**PAPI 库** (libpapi.so)－为应用程序提供了一种打印服务中立的方法，来与打印服务或协议进行交互。在本例中，PAPI 库为 Apache IPP 侦听服务提供一种与本地 LP 服务进行交互的方法。PAPI 库根据 printers.conf 配置数据库中存储的客户端队列配置数据，确定要与之交互的打印服务。

## IPP 支持模型

以下各节介绍了 IPP 支持模型的各个方面。

### IPP 对象模型

IPP 包含两种基本对象类型： 打印机和作业。每种对象类型都包含实际打印机或实际打印作业的特征。每种对象类型都被定义为这种特定对象类型可支持的一组可能属性。

为了明确引用所有打印机和作业对象，所有这些对象都用统一资源标识符 (Uniform Resource Identifier, URI) 加以标识。URI 概念以及作为标识符的实现方式非常有用，因为它所提供的手段既能够唯一标识与打印服务 (IPP) 进行通信的方法，又能够唯一标识打印机队列 (//server/printers/queue) 或作业的不同网络标识符。

创建打印请求时，生成的 IPP 协议消息必须包含将对其执行操作的打印机对象的 printer-uri。可以从打印机对象或命名服务 printer-uri-supported 属性检索 printer-uri 的可能值。

### IPP 打印机对象

打印机对象是 IPP 模型中的主要对象。打印机对象可为 IPP 提供服务器端支持。打印机对象包含的功能通常与物理输出设备相关联。这些功能包括假脱机、调度、变换和管理多个与打印服务器关联的设备。打印机对象用 printer-uri 唯一地标识。为了搜索和查找有关打印机对象的静态信息（如名称、上下文和打印机功能），可以将这些打印机对象注册为目录条目。动态信息（例如打印机的排队作业数目、错误和警告）与打印机对象本身相关联。

---

**注 -** 只要语义与打印机对象的语义一致，就可以使用打印机对象来表示实际设备或虚拟设备。

---

IPP 客户机在客户端实现协议，以便为您或代表您运行的程序提供查询打印机对象的能力，目的是为了提交和管理打印作业。IPP 服务器是打印机对象的一部分，用于实现打印服务的应用程序语义。打印机对象可以嵌入输出设备中，也可以在与输出设备进行通信的网络主机上实现。

将作业提交到打印机对象时，打印机对象将验证请求中的属性，然后创建作业对象。当您查询作业状态或监视其进度时，就在与作业对象进行交互。如果您取消打印作业，则使用的是作业对象的 Cancel-job 操作。有关作业对象操作的更多信息，请参见[IPP 操作关键字](https://docs.oracle.com/cd/E38902_01/html/820-5488/gdxsq.html#gedvz)。

### IPP 作业对象

作业对象用于为打印作业建模。作业对象包含文档。如果您通过 IPP 客户机将打印请求发送给打印机对象，则创建作业对象所需的信息将以创建请求的形式发送到打印服务器。打印机对象将验证创建请求，如果接受，打印机对象随后将创建新的作业对象。IPP 作业对象用 printer-uri 和 job-id 属性或 job-uri 属性的组合唯一地标识。有关更多详细信息，请参见[IPP 操作关键字](https://docs.oracle.com/cd/E38902_01/html/820-5488/gdxsq.html#gedvz)。
## IPP 服务器端支持

IPP 侦听服务提供了一个 IPP 网络协议服务，该服务为打印客户机系统提供一种与运行侦听程序的系统上的打印服务进行交互的方法。此侦听程序实现了服务器端 IPP 协议支持，其中包括一组广泛的标准操作和属性。此侦听程序是在 Oracle Solaris OS 中作为 Apache 模块和一系列共享库（包含 IPP 操作和网络支持）实现的。在系统上安装 Oracle Solaris OS 时，将会安装 IPP 软件栈。IPP 侦听服务是一个 SMF 服务，它依赖于要运行的打印服务。因此，在添加第一个打印队列后，会在打印服务器上自动启用 IPP。在删除最后一个打印队列后，会禁用 IPP。

IPP 的服务器端支持从 IPP 模块 mod_ipp 开始。侦听服务使用 Apache Web 服务器，这是因为 Oracle Solaris OS 附带提供 Apache 软件。Apache 模块使用动态共享目标文件 (Dynamic Shared Object, DSO) 接口以插入到 Web 服务器下面。通过使用 DSO 接口，此模块包含对 IPP 侦听服务的配置支持，并包含 Web 服务器入口点以便为侦听程序提供 HTTP 连接。通过这种模块化方法，IPP 支持可以重用 Apache 提供的加密和验证机制。

**图 A-1 IPP 服务器配置**

![image:构成 IPP 服务器配置的组件图。周围文字给出了进一步的说明。](https://docs.oracle.com/cd/E38902_01/html/820-5488/figures/IPP_comp.png "构成 IPP 服务器配置的组件图。周围文字给出了进一步的说明。")

### IPP 服务器端数据的配置

IPP 侦听服务配置文件 /etc/apache/httpd-standalone-ipp.conf 与任何标准的 Apache 1.3 配置文件类似。这些配置文件可采用您要使用的任何 Apache 1.3 配置指令。

缺省配置包括以下功能：

- 在端口 631 上侦听。
    
- 装入最小的一组 Apache 模块。
    
- 在 /printers/path (ipp://server/printers/) 下启用所有支持的 IPP 操作，而无需进行验证。
    

为 /printers/ 启用的缺省操作仅限于一组具有较小安全风险的操作。但是，所有操作都在 /admin/path (ipp://server/admin/) 下启用，并且需要进行基本验证。

下表说明了可以选择的 mod_ipp Apache 配置选项。

**表 A-2 mod_ipp Apache 模块配置选项**

|**值**|**说明**|
|---|---|
|ipp-conformance|选择协议检查级别。缺省值为 **automatic**，它提供最大限度的客户机交互。|
|ipp-operation|使您可以为多个 IPP 操作有选择性地启用或禁用 IPP 操作支持。|
|ipp-default-user|选择与本地打印服务联系时使用的用户名。缺省为 lp 打印用户，它可启用功能更强的代理。|
|ip-default-service|选择将请求定向到的缺省打印服务。缺省为 lpsched 守护进程，当前仅针对 lpsched 进行了测试。||

下表说明了 Apache Web 服务器配置的一致性检查类型。使用的语法为：

ipp-conformance value

**表 A-3 Apache Web 服务器一致性检查类型**


|**值**|**含义**|
|---|---|
|Automatic|仅检查协议侦听程序是否支持所请求的操作。（缺省）|
|1.0|检查请求是否符合 IPP/1.0 规范。|
|1.1|检查请求是否符合 IPP/1.1 规范。|

以下是包含注释的 Apache 配置文件示例：
~~~sh
if mod_ipp is loaded User lp run as "lp"
URI: ipp://{host]/printers/{queue}
SetHandler application/ipp use mod_ipp for this location
ipp-conformance strict enable strict protocol checking (default)
ipp-operation all enable enable all supported operations
~~~

### IPP 操作关键字

IPP 对象支持操作。操作由请求和响应组成。当打印客户机与 IPP 对象进行通信时，该客户机会向该对象的 URI 发出操作请求。操作请求和响应具有标识操作的参数。操作还具有影响操作的运行时间特征的属性。这些特定于操作的属性被定义为操作属性。打印请求包含操作属性、对象属性和执行特定操作所需的文档数据。每个请求都要求获得来自对象的响应。每个响应都以对应的状态代码作为响应参数来指示操作的成败。响应包含操作属性、对象属性，以及在执行操作请求期间生成的状态消息。

下表说明了 Apache Web 服务器配置的 IPP 操作关键字。

**表 A-4 IPP 操作关键字**

|**值**|**含义**|
|---|---|
|All|此关键字用于代替某个操作。此关键字用于表示已选择 mod_ipp 支持的所有操作。|
|Required|此关键字用于代替某个操作。此关键字用于表示已选择 RFC-2911 中定义的所有必需操作，其中包括以下操作： print-job、cancel-job、get-job-attributes、get-jobs 和 get-printer-attributes。|
|Print-job|客户机要提交一个仅包含单个文档的打印作业。文档数据将随请求一起发送。|
|Print-uri|不受支持。|
|Validate-job|在提交打印作业之前，客户机要验证调度程序能否处理打印作业。|
|Create-job|客户机要提交一个包含多个文档的打印作业。文档是使用 send-document 和 send-uri 操作发送的。|
|Send-document|客户机要将文档添加到使用 print-job 操作创建的打印作业中。文档数据将随请求一起发送。|
|Send-uri|不受支持。|
|Cancel-job|客户机要取消打印作业。|
|Get-job-attributes|客户机要收集有关打印作业的信息。|
|Get-jobs|客户机要收集特定打印队列中打印作业的列表。|
|Get-printer-attributes|客户机要收集有关特定打印队列的信息。|
|Hold-job|客户机要保留特定打印作业。|
|Release-job|客户机要释放特定打印作业。|
|Restart-job|客户机要重新启动特定打印作业。|
|Pause-printer|客户机要暂停（禁用）特定打印队列。此操作将停止处理队列中的打印请求。请注意，此操作不会禁止队列接受作业。|
|Resume-printer|客户机要恢复（启用）处理特定打印队列中的作业。|
|Purge-jobs|客户机要删除特定打印队列中的所有作业。|
|Set-printer-attributes|创建或修改打印机属性。|
|Set-job-attributes|修改现有打印作业的属性。|
|Enable-printer|恢复（接受）将打印作业排入队列。|
|Disable-printer|禁止（拒绝）将打印作业排入队列。|
|cups-get-default|检索打印服务的缺省目标。|
|cups-get-printers|枚举通过打印服务提供的所有打印机。|
|cups-get-classes|枚举通过打印服务提供的所有类。|
|cups-accept-jobs|特定于 CUPS 的 Enable-printer 等效项。|
|cups-reject-jobs|特定于 CUPS 的 Disable-printer 等效项。|
|cups-move-jobs|在相同打印服务的队列之间移动作业。||




## IPP 客户端支持

Oracle Solaris 中的 IPP 客户端支持是在 PAPI 下面实现的。借助于此支持，使用 PAPI 的任何应用程序都可以使用 IPP 以及其他打印服务和协议。

这些应用程序包括：

- GNOME 桌面环境－使用 libgnomeprint 的应用程序
    
- BSD 命令－BSD UNIX LPD 打印服务命令：
    
    - lpr
        
    - lpq
        
    - lprm
        
    - lpc
        
- LP 命令－System V UNIX LP 打印服务命令：
    
    - lp
        
    - lpstat
        
    - lpmove
        
    - accept
        
    - reject
        
    - enable
        
    - disable
        

应用程序的 IPP 客户端支持是通过可装入的模块 psm-ipp.so 提供的，该模块将根据打印机 printer-uri 或正在处理的作业在运行时装入。

由于 IPP 在 HTTP 传输的上面一层，因此，客户端和服务器端支持都需要具有读取和写入 HTTP 协议的功能。在服务器端，此支持是由 Apache Web 服务器提供的。在客户端，此支持是由 HTTP 库 libhttp-core.so 提供的。

### lpsched 支持

psm-lpsched 命令提供打印服务中立的 PAPI 表示形式与 LP 打印假脱机程序 (lpsched) 之间的转换。此命令提取传递到各种 PAPI 函数中的 PAPI 属性，并将这些属性转换为数据的内部 lpsched 表示形式。然后，psm-lpsched 联系 lpsched 以执行请求的操作。执行后，psm-lpsched 将结果转换回打印服务中立的 PAPI 表示形式，并将这些结果返回给调用者。

LP 打印假脱机程序 (lpsched) 提供假脱机服务、将作业数据转换为打印机支持的格式以及将作业数据传输到物理打印机。


## IPP 属性

对于每个对象实例，都有一组支持的属性和值来描述该对象的特定实现。

对象的属性和值包括有关该对象的以下信息：

- 状态
    
- 能力
    
- 功能
    
- 作业处理功能
    
- 缺省行为
    
- 缺省特征
    

用于定义对象的每个属性都包括在一组属性中。特定对象的这组属性包括该对象可能支持的所有属性。对于标记为 REQUIRED 的属性，每个对象必须支持该属性。如果属性被标记为 OPTIONAL，则每个对象可以支持该属性。

打印机属性被分为两组：

作业模板

这些属性描述受支持的作业处理能力以及打印机对象缺省值。

打印机描述

这些属性包括标识、状态、位置以及对有关打印机对象的其他信息源的引用。

支持打印机对象的配置示例包括以下各项：

- 不具备假脱机能力的输出设备
    
- 具备内置假脱机程序的输出设备
    
- 支持 IPP 的打印服务器，以及一个或多个适用以下条件的关联输出设备：
    
    - 可能能够处理假脱机作业，或不能处理假脱机作业
        
    - 可能支持 IPP，或无法支持 IPP
        

作业对象的特征也由其属性加以描述。

作业属性被分为两组：

- 作业模板－这些属性由您或打印客户机提供。这些属性包含的作业处理指令专用来覆盖任何打印机对象缺省值或可能嵌入在文档数据中的指令。
    
- 作业描述－这些属性包括有关作业对象的以下信息：
    
    - 标识
        
    - 状态
        
    - 大小
        

请注意，打印客户机提供其中一些属性，而打印机对象生成其他属性。有一种实现方式可支持每作业对象多个文档，但是它必须至少支持每作业对象一个文档。

---

**注 -** 在 IPP 版本 1.0 和版本 1.1 中，文档不作为 IPP 对象进行建模。因此，文档没有对象标识符或关联的属性。所有的作业处理指令都作为作业对象属性进行建模。这些属性称为作业模板属性。这些属性同等地应用到作业对象中的所有文档。

---

IPP 对象具备的关系是随同对象属性的永久性存储永久保持的。

有关与任务相关的信息，请参见[配置 Internet 打印协议](https://docs.oracle.com/cd/E38902_01/html/820-5488/geduc.html#scrolltoc)。