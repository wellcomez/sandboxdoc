---
date: 2023-10-06 16:58
title: ipp协议
tags:
  - ipp
  - cups
---



##  IPP

互联网打印协议（IPP；Internet Printing Protocol）是一个在互联网上打印的标准网络协议，它容许用户可以透过互联网作遥距打印及管理打印工作等工作。用户可以透过相关界面来控制打印品所使用的纸张种类、分辨率等各种参数。

与其他基于互联网的协议一样，IPP可以用于内联网及互联网等基于IP协议的网络上。不过，与一般IP协议不同的是：IPP亦同时支援安全连结。所以，用户可以透过网络进行存取控制、认证及加密，使打印过程更安全。

协议规范

[https://datatracker.ietf.org/doc/search/?name=Internet+Printing+Protocol&activedrafts=on&rfcs=on](https://datatracker.ietf.org/doc/search/?name=Internet+Printing+Protocol&activedrafts=on&rfcs=on)

### 简单示例

```
curl \
  --request POST \
  --header "Content-Type: application/ipp" \
  --data-binary "@binary" \
  http://localhost:631/printers/EPSON_L310_Series
```

附上二进制文件：[binary.zip](https://github.com/jschyz/blog/files/2322863/binary.zip)

在 Terminal 上输入，得出结果为

[![image](https://user-images.githubusercontent.com/3281438/44646953-48f44700-aa0f-11e8-892b-228e01b8ff1e.png)](https://user-images.githubusercontent.com/3281438/44646953-48f44700-aa0f-11e8-892b-228e01b8ff1e.png)

其中 offline-report 含义代表打印机离线状态

### IPP 对象模型

IPP 包含两种基本对象类型： 打印机和作业。每种对象类型都包含实际打印机或实际打印作业的特征。每种对象类型都被定义为这种特定对象类型可支持的一组可能属性。

为了明确引用所有打印机和作业对象，所有这些对象都用统一资源标识符 (Uniform Resource Identifier, URI) 加以标识。URI 概念以及作为标识符的实现方式非常有用，因为它所提供的手段既能够唯一标识与打印服务 (IPP) 进行通信的方法，又能够唯一标识打印机队列 (//server/printers/queue) 或作业的不同网络标识符。

创建打印请求时，生成的 IPP 协议消息必须包含将对其执行操作的打印机对象的 printer-uri。可以从打印机对象或命名服务 printer-uri-supported 属性检索 printer-uri 的可能值。

#### IPP 打印机对象

打印机对象是 IPP 模型中的主要对象。打印机对象可为 IPP 提供服务器端支持。打印机对象包含的功能通常与物理输出设备相关联。这些功能包括假脱机、调度、变换和管理多个与打印服务器关联的设备。打印机对象用 printer-uri 唯一地标识。为了搜索和查找有关打印机对象的静态信息（如名称、上下文和打印机功能），可以将这些打印机对象注册为目录条目。动态信息（例如打印机的排队作业数目、错误和警告）与打印机对象本身相关联。

注 - 只要语义与打印机对象的语义一致，就可以使用打印机对象来表示实际设备或虚拟设备。

IPP 客户机在客户端实现协议，以便为您或代表您运行的程序提供查询打印机对象的能力，目的是为了提交和管理打印作业。IPP 服务器是打印机对象的一部分，用于实现打印服务的应用程序语义。打印机对象可以嵌入输出设备中，也可以在与输出设备进行通信的网络主机上实现。

将作业提交到打印机对象时，打印机对象将验证请求中的属性，然后创建作业对象。当您查询作业状态或监视其进度时，就在与作业对象进行交互。如果您取消打印作业，则使用的是作业对象的 Cancel-job 操作。有关作业对象操作的更多信息，请参见[IPP 操作关键字](https://www.cups.org/doc/spec-ipp.html#IPP_OPERATIONS)。

#### IPP 作业对象

作业对象用于为打印作业建模。作业对象包含文档。如果您通过 IPP 客户机将打印请求发送给打印机对象，则创建作业对象所需的信息将以创建请求的形式发送到打印服务器。打印机对象将验证创建请求，如果接受，打印机对象随后将创建新的作业对象。IPP 作业对象用 printer-uri 和 job-id 属性或 job-uri 属性的组合唯一地标识。有关更多详细信息，请参见[IPP 操作关键字](https://docs.oracle.com/cd/E38902_01/html/820-5488/gdxsq.html#gedvz)。

### IPP 操作关键字

|Operation Name|CUPS|Code|Brief Description|
|---|---|---|---|
|Print-Job|1.0|0x0002|Print a file.|
|Validate-Job|1.0|0x0004|Validate job attributes.|
|Create-Job|1.1|0x0005|Create a print job.|
|Send-Document|1.1|0x0006|Send a file for a print job.|
|Cancel-Job|1.0|0x0008|Cancel a print job.|
|Get-Job-Attributes|1.0|0x0009|Get job attributes.|
|Get-Jobs|1.0|0x000A|Get all jobs.|
|Get-Printer-Attributes|1.0|0x000B|Get printer attributes.|
|Hold-Job|1.1|0x000C|Hold a job for printing.|
|Release-Job|1.1|0x000D|Release a job for printing.|
|Restart-Job|1.1|0x000E|Restarts a print job.|
|Pause-Printer|1.0|0x0010|Pause printing on a printer.|
|Resume-Printer|1.0|0x0011|Resume printing on a printer.|
|Purge-Jobs|1.0|0x0012|Purge all jobs.|
|Set-Printer-Attributes|1.4|0x0013|Set attributes for a printer.|
|Set-Job-Attributes|1.1|0x0014|Set attributes for a pending or held job.|
|Create-Printer-Subscription|1.2|0x0016|Creates a subscription associated with a printer or the server.|
|Create-Job-Subscription|1.2|0x0017|Creates a subscription associated with a job.|
|Get-Subscription-Attributes|1.2|0x0018|Gets the attributes for a subscription.|
|Get-Subscriptions|1.2|0x0019|Gets the attributes for zero or more subscriptions.|
|Renew-Subscription|1.2|0x001A|Renews a subscription.|
|Cancel-Subscription|1.2|0x001B|Cancels a subscription.|
|Get-Notifications|1.2|0x001C|Get notification events for ippget subscriptions.|
|Enable-Printer|1.2|0x0022|Accepts jobs on a printer.|
|Disable-Printer|1.2|0x0023|Rejects jobs on a printer.|
|Hold-New-Jobs|1.4|0x0025|Hold new jobs by default.|
|Release-Held-New-Jobs|1.4|0x0026|Releases all jobs that were previously held.|
|Cancel-Jobs|1.5|0x0038|Cancel all jobs (administrator).|
|Cancel-My-Jobs|1.5|0x0039|Cancel all jobs (user).|
|Close-Job|1.5|0x003b|Close a created job.|

### 打印机状态

|Keyword|Description|
|---|---|
|connecting-to-device|Connecting to printer but not printing yet.|
|cover-open|The printer's cover is open.|
|input-tray-missing|The paper tray is missing.|
|marker-supply-empty|The printer is out of ink.|
|marker-supply-low|The printer is almost out of ink.|
|marker-waste-almost-full|The printer's waste bin is almost full.|
|marker-waste-full|The printer's waste bin is full.|
|media-empty|The paper tray (any paper tray) is empty.|
|media-jam|There is a paper jam.|
|media-low|The paper tray (any paper tray) is almost empty.|
|media-needed|The paper tray needs to be filled (for a job that is printing).|
|paused|Stop the printer.|
|timed-out|Unable to connect to printer.|
|toner-empty|The printer is out of toner.|
|toner-low|The printer is low on toner.|