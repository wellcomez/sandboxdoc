---
date: 2023-10-06 17:02
title: Internet Printing Protocol-wiki
tags:
  - cups
  - ipp
---



# Internet Printing Protocol

From Wikipedia, the free encyclopedia

The **Internet Printing Protocol** (**IPP**) is a specialized [communication protocol](https://en.wikipedia.org/wiki/Communication_protocol "Communication protocol") for communication between client devices (computers, mobile phones, tablets, etc.) and printers (or [print servers](https://en.wikipedia.org/wiki/Print_server "Print server")). It allows clients to submit one or more [print jobs](https://en.wikipedia.org/wiki/Print_job "Print job") to the network-attached printer or print server, and perform tasks such as querying the status of a [printer](https://en.wikipedia.org/wiki/Printer_(computing) "Printer (computing)"), obtaining the status of print jobs, or cancelling individual print jobs.

Like all [IP](https://en.wikipedia.org/wiki/Internet_Protocol "Internet Protocol")-based protocols, IPP can run locally or over the [Internet](https://en.wikipedia.org/wiki/Internet "Internet"). Unlike other printing protocols, IPP also supports [access control](https://en.wikipedia.org/wiki/Access_control "Access control"), [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication"), and [encryption](https://en.wikipedia.org/wiki/Encryption "Encryption"), making it a much more capable and secure printing mechanism than older ones.

IPP is the basis of several printer logo certification programs including [AirPrint](https://en.wikipedia.org/wiki/AirPrint "AirPrint"), IPP Everywhere,[[1]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-1) and [Mopria Alliance](https://en.wikipedia.org/wiki/Mopria_Alliance "Mopria Alliance"), and is supported by over 98% of printers sold today.[[2]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-2)

## History[[edit](https://en.wikipedia.org/w/index.php?title=Internet_Printing_Protocol&action=edit&section=1 "Edit section: History")]

IPP began as a proposal by [Novell](https://en.wikipedia.org/wiki/Novell "Novell") for the creation of an Internet printing [protocol](https://en.wikipedia.org/wiki/Communications_protocol "Communications protocol") project in 1996. The result was a draft written by Novell and [Xerox](https://en.wikipedia.org/wiki/Xerox "Xerox") called the Lightweight Document Printing Application (LDPA), derived from ECMA-140: [Document Printing Application](https://en.wikipedia.org/w/index.php?title=Document_Printing_Application&action=edit&redlink=1 "Document Printing Application (page does not exist)") (DPA). At about the same time, [Lexmark](https://en.wikipedia.org/wiki/Lexmark "Lexmark") publicly proposed something called the [HyperText](https://en.wikipedia.org/wiki/HyperText "HyperText") Printing Protocol (HTPP), and both [HP](https://en.wikipedia.org/wiki/Hewlett-Packard "Hewlett-Packard") and [Microsoft](https://en.wikipedia.org/wiki/Microsoft "Microsoft") had started work on new print services for what became [Windows 2000](https://en.wikipedia.org/wiki/Windows_2000 "Windows 2000"). Each of the companies chose to start a common Internet Printing Protocol project in the [Printer Working Group](https://en.wikipedia.org/wiki/Printer_Working_Group "Printer Working Group") (PWG) and negotiated an IPP [birds-of-a-feather (or BOF)](https://en.wikipedia.org/wiki/Birds_of_a_feather_(computing) "Birds of a feather (computing)") session with the Application Area Directors in the [Internet Engineering Task Force](https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force "Internet Engineering Task Force") (IETF). The BOF session in December 1996[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] showed sufficient interest in developing a printing protocol, leading to the creation of the IETF Internet Printing Protocol (ipp)[[3]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-3) working group, which concluded in 2005.

Work on IPP continues in the PWG [Internet Printing Protocol workgroup](https://www.pwg.org/ipp) with the publication of 23 candidate standards, 1 new and 3 updated IETF RFCs, and several registration and best practice documents providing extensions to IPP and support for different services including [3D Printing](https://www.pwg.org/3d), scanning, facsimile, cloud-based services, and overall system and resource management.

IPP/1.0 was published as a series of experimental documents (RFC 2565,[[4]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-4) RFC 2566,[[5]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-5) RFC 2567,[[6]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-6) RFC 2568,[[7]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-7) RFC 2569,[[8]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-8) and RFC 2639[[9]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-9)) in 1999.

IPP/1.1 followed as a draft standard in 2000 with support documents in 2001, 2003, and 2015 (RFC 2910,[[10]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-10) RFC 2911,[[11]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-11) RFC 3196,[[12]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-12) RFC 3510[[13]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-13) RFC 7472[[14]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-14)). IPP/1.1 was updated as a proposed standard in January 2017 (RFC 8010,[[15]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-15) RFC 8011,[[16]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-16)) and then adopted as Internet Standard 92 (STD 92,[[17]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-17)) in June 2018.

IPP 2.0 was published as a PWG Candidate Standard in 2009 (PWG 5100.10-2009,[[18]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-18)) and defined two new IPP versions (2.0 for printers and 2.1 for print servers) with additional conformance requirements beyond IPP 1.1. A subsequent Candidate Standard replaced it in 2011 defining an additional 2.2 version for production printers (PWG 5100.12-2011,[[19]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-19)). This specification was updated and approved as a full PWG Standard (PWG 5100.12-2015,[[20]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-20)) in 2015.

[IPP Everywhere](https://www.pwg.org/ipp/everywhere.html) was published in 2013 and provides a common baseline for printers to support so-called "driverless" printing from client devices. It builds on IPP and specifies additional rules for interoperability, such as a list of document formats printers need to support. A corresponding self-certification manual and tool suite was published in 2016 allowing printer manufacturers and print server implementors to certify their solutions against the published specification and be listed on the [IPP Everywhere printers](https://www.pwg.org/printers) page maintained by the PWG.

## Implementation[[edit](https://en.wikipedia.org/w/index.php?title=Internet_Printing_Protocol&action=edit&section=2 "Edit section: Implementation")]

IPP is implemented using the [Hypertext Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol "Hypertext Transfer Protocol") (HTTP) and inherits all of the HTTP streaming and security features. For example, [authorization](https://en.wikipedia.org/wiki/Authorization "Authorization") can take place via HTTP's [Digest access authentication](https://en.wikipedia.org/wiki/Digest_access_authentication "Digest access authentication") mechanism, [GSSAPI](https://en.wikipedia.org/wiki/GSSAPI "GSSAPI"), or any other HTTP authentication methods. [Encryption](https://en.wikipedia.org/wiki/Encryption "Encryption") is provided using the [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security "Transport Layer Security") protocol-layer, either in the traditional always-on mode used by [HTTPS](https://en.wikipedia.org/wiki/HTTPS "HTTPS") or using the HTTP Upgrade extension to HTTP (RFC 2817[[21]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-21)). [Public key certificates](https://en.wikipedia.org/wiki/Public_key_certificates "Public key certificates") can be used for authentication with TLS. Streaming is supported using HTTP chunking. The document to be printed is usually sent as a data stream.

IPP accommodates various formats for documents to be printed. The [PWG](https://en.wikipedia.org/wiki/Printer_Working_Group "Printer Working Group") defined an image format called **PWG Raster** specifically for this purpose. Other formats include [PDF](https://en.wikipedia.org/wiki/PDF "PDF") or [JPEG](https://en.wikipedia.org/wiki/JPEG "JPEG"), depending on the capabilities of the destination printer.[[22]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-22)

IPP uses the traditional client–server model, with clients sending IPP request messages with the [MIME](https://en.wikipedia.org/wiki/MIME "MIME") media type "application/ipp" in HTTP POST requests to an IPP printer. IPP request messages consist of key–value pairs using a custom binary encoding followed by an "end of attributes" tag and any document data required for the request (such as the document to be printed). The IPP response is sent back to the client in the HTTP POST response, again using the "application/ipp" MIME media type.

Among other things, IPP allows a client to:

- query a printer's capabilities (such as supported character sets, media types and document formats)
- submit print jobs to a printer
- query the status of a printer
- query the status of one or more print jobs
- cancel previously submitted jobs

IPP uses [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol "Transmission Control Protocol") with port 631 as its [well-known port](https://en.wikipedia.org/wiki/Well-known_port "Well-known port").

Products using the Internet Printing Protocol include Universal Print from Microsoft,[[23]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-23) [CUPS](https://en.wikipedia.org/wiki/CUPS "CUPS") (which is part of [Apple](https://en.wikipedia.org/wiki/Apple_Inc. "Apple Inc.") [macOS](https://en.wikipedia.org/wiki/Mac_OS_X "Mac OS X") and many [BSD](https://en.wikipedia.org/wiki/BSD "BSD") and [Linux](https://en.wikipedia.org/wiki/Linux "Linux") distributions and is the reference implementation for most versions of IPP [[24]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-24)), [Novell](https://en.wikipedia.org/wiki/Novell "Novell") [iPrint](https://en.wikipedia.org/wiki/IPrint "IPrint"), and [Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows "Microsoft Windows") versions starting from [MS](https://en.wikipedia.org/wiki/Microsoft "Microsoft") [Windows 2000](https://en.wikipedia.org/wiki/Windows_2000 "Windows 2000").[[25]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-25) [Windows XP](https://en.wikipedia.org/wiki/Windows_XP "Windows XP") and [Windows Server 2003](https://en.wikipedia.org/wiki/Windows_Server_2003 "Windows Server 2003") offer IPP printing via [HTTPS](https://en.wikipedia.org/wiki/HTTPS "HTTPS"). [Windows Vista](https://en.wikipedia.org/wiki/Windows_Vista "Windows Vista"), [Windows 7](https://en.wikipedia.org/wiki/Windows_7 "Windows 7"),[[26]](https://en.wikipedia.org/wiki/Internet_Printing_Protocol#cite_note-26) [Windows Server 2008](https://en.wikipedia.org/wiki/Windows_Server_2008 "Windows Server 2008") and [2008 R2](https://en.wikipedia.org/wiki/Windows_Server_2008_R2 "Windows Server 2008 R2") also support IPP printing over [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call "Remote procedure call") in the "Medium-Low" [security zone](https://en.wikipedia.org/w/index.php?title=Security_zone&action=edit&redlink=1 "Security zone (page does not exist)").

## See also[[edit](https://en.wikipedia.org/w/index.php?title=Internet_Printing_Protocol&action=edit&section=3 "Edit section: See also")]

- [CUPS](https://en.wikipedia.org/wiki/CUPS "CUPS")
- [Job Definition Format](https://en.wikipedia.org/wiki/Job_Definition_Format "Job Definition Format")
- [Line Printer Daemon protocol](https://en.wikipedia.org/wiki/Line_Printer_Daemon_protocol "Line Printer Daemon protocol")
- [T.37 (ITU-T recommendation)](https://en.wikipedia.org/wiki/T.37_(ITU-T_recommendation) "T.37 (ITU-T recommendation)")