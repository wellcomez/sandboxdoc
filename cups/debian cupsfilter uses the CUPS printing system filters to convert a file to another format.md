---
date: 2023-10-07 15:20
title: debian cupsfilter uses the CUPS printing system filters to convert a file to another format
tags:
  - cups
---

# CUPSFilter 
- [CUPSFilter](https://wiki.debian.org/CUPSFilter)

[Translation(s)](https://wiki.debian.org/DebianWiki/EditorGuide#translation): none


cupsfilter uses the CUPS printing system filters to convert a file to another format.

目录

1. [Introduction](https://wiki.debian.org/CUPSFilter#Introduction)
2. [File Viewers](https://wiki.debian.org/CUPSFilter#File_Viewers)
3. [The pdfinfo Utility](https://wiki.debian.org/CUPSFilter#The_pdfinfo_Utility)
4. [Using a PPD File with cupsfilter](https://wiki.debian.org/CUPSFilter#Using_a_PPD_File_with_cupsfilter)
5. [Specifying the Destination File Type](https://wiki.debian.org/CUPSFilter#Specifying_the_Destination_File_Type)
6. [Obtaining the File which is Sent to the Printer](https://wiki.debian.org/CUPSFilter#Obtaining_the_File_which_is_Sent_to_the_Printer)
7. [See Also](https://wiki.debian.org/CUPSFilter#See_Also)

### Introduction

[cupsfilter](https://manpages.debian.org/man/cupsfilter "DebianMan") comes with the [cups](https://packages.debian.org/cups "DebianPkg") package. The idea behind it is to use the filter subsystem (provided in the [cups](https://packages.debian.org/cups "DebianPkg"), [cups-server-common](https://packages.debian.org/cups-server-common "DebianPkg"), [cups-daemon](https://packages.debian.org/cups-daemon "DebianPkg"), [cups-filters](https://packages.debian.org/cups-filters "DebianPkg") and [cups-filters-core-drivers](https://packages.debian.org/cups-filters-core-drivers "DebianPkg") packages) to convert a file to a specific format. The processing is identical to that done by CUPS and the filter subsystem when a file makes its way to the printer and ink or toner is put on paper; except there isn't paper output.

We can use cupsfilter to examine the state of the filter conversions at each stage of the filtering process and determine whether behaviour is reasonabl and correct. The filters in _/usr/lib/cups/filter_ can also be used as standalone programs to examine the conversions but this is not generally encouraged. Please see the _WARNING_ or _NOTES_ section in the [filter](https://manpages.debian.org/man/7/filter "DebianMan") manual.

When tracking down a problem with printing the cupsfilter utilty is a useful tool to complement what is produced by an [error_log](https://wiki.debian.org/CUPSDebugging#errorlog).

A user can access much of what cupsfilter offers when it is executed as /usr/sbin/cupsfilter. However, when a [PPD is called for](https://wiki.debian.org/CUPSFilter#ppd), as it is with the _-e_ option, root privileges may be required.

### File Viewers

The ease of examining the output from cupsfilter at a particular stage of a filtering process depends on the file type produced. PDF and PostScript can be viewed by [gv](https://packages.debian.org/gv "DebianPkg"), [xpdf](https://packages.debian.org/xpdf "DebianPkg"), [mupdf](https://packages.debian.org/mupdf "DebianPkg"), [evince](https://packages.debian.org/evince "DebianPkg"), [okular](https://packages.debian.org/okular "DebianPkg") or [zathura](https://packages.debian.org/zathura "DebianPkg"). CUPS, [PWG and Apple](https://wiki.debian.org/CUPSDriverlessPrinting#pdls) raster output viewing is handled by [rasterview](https://packages.debian.org/rasterview "DebianPkg"). The PWG format is based on [CUPS raster](https://www.cups.org/doc/spec-raster.html) and Apple Raster is very similar to PWG raster.

The source code for RasterView may be downloaded from the [principal CUPS developer's web site](https://www.msweet.org/rasterview/). For successful compiling you are advised to obtain source code which has a version greater than v1.5. The following command would do this for you:

~~~
wget https://github.com/michaelrsweet/rasterview/releases/download/v1.8/rasterview-1.8.tar.gz
~~~

The executable can be compiled after installing the [g++](https://packages.debian.org/g++ "DebianPkg") and [libfltk1.3-dev](https://packages.debian.org/libfltk1.3-dev "DebianPkg") packages and doing
~~~sh
mkdir rasterview
mv rasterview-1.8.tar.gz rasterview
cd rasterview
tar zvxf rasterview-1.8.tar.gz
./configure
make
~~~

The rasterview executable produced would be put in _/usr/local/bin_.

Using RasterView isn't too hard. Zooming in and out is mouse-driven with its left and right buttons; or the - and = keys can be used. The bottom left of the window has buttons < and > for moving forwards and backwards in a document; or use the SPACE and BACK keys from the keyboard. The ESC key exits the program.

Viewing the file which is actually sent to the printer (the printer-ready file) may or may not be possible because it depends on whether it is PostScript, PDF, PCL, [raster data](https://wiki.debian.org/CUPSDriverlessPrinting#rasters) or vendor-specific data. [Ghostpcl](https://www.ghostscript.com/releases/index.html) is a PCL viewer. It is sufficient to put the binary in _/usr/local/bin_ for it to function. Often the only way to view a file which is classified by the [file](https://manpages.debian.org/man/1/file "DebianMan") utility as data is to send it to the printer without passing through the filtering system. One way is:

~~~
lp -d <print_queue> -o raw <final_file>
~~~

The use examples described on this page use the Debian 11 (bullseye) version of cupsfilter and its [documentation](https://manpages.debian.org/man/cupsfilter "DebianMan"), but there is very little difference in the utility's behaviour on previous distributions.

### The pdfinfo Utility

[pdfinfo](https://manpages.debian.org/man/pdfinfo "DebianMan") is in the [poppler-utils](https://packages.debian.org/poppler-utils "DebianPkg") package. It is a useful tool for examining information extracted from a PDF submited to CUPS and comparing it to a PDF produced by the filtering system. The [pdftopdfandPageRotation](https://wiki.debian.org/pdftopdfandPageRotation) wiki page uses this technique to track rotation of pages in a PDF document from the _Page rot:_ attribute.

By default pdfinfo <PDF> will display only information about the first page in a PDF file. The -f and -l options specify the range of pages to examine.

For the number of pages, N, in a file:

~~~
pdfinfo <PDF> | grep Pages
~~~

For information on a range of pages in the file:

~~~
pdfinfo -f 4 -l 12 <PDF>
~~~

For information on all the pages in the file:

~~~
pdfinfo -l N <PDF>
~~~

### Using a PPD File with cupsfilter

You almost certainly want to use a PPD file with cupsfilter to test the detailed operation of a particular print queue. As root you will have access to the PPD for the queue in _/etc/cups/ppd_; as a user you will not because the PPD files in _/etc/cups/ppd/_ are, by default, not world-readable. If you want to test the filtering of an installed print queue as a user you can, however, get its PPD file using [cups-driverd](https://manpages.debian.org/man/cups-driverd "DebianMan").

- This technique applies only to legacy printing. That is, printing with those printers having print queues set up with vendor drivers.

Let's suppose the print queue is for a LaserJet 2200 using the Gutenprint PPD. A searchable list of PPDs on the system is available to a user with

~~~
/usr/sbin/lpinfo -m | less
~~~

and the URI for the PPD is

~~~
gutenprint.5.2://hp-lj_2200/expert
~~~

Then

~~~
/usr/lib/cups/daemon/cups-driverd cat gutenprint.5.2://hp-lj_2200/expert > laserjet2200.ppd

~~~
gives you the PPD.

For a [modern printer](https://wiki.debian.org/CUPSDriverlessPrinting) a PPD may be obtained from

~~~
driverless <URI>
~~~

The command

~~~
driverless
~~~

gives the URI.

Commands using /usr/sbin/cupsfilter are intended for a user who has obtained a PPD as described above. For a user with root privileges the command cupsfilter would suffice. In this case the _-p_ option would be _/etc/cups/ppd/<PPD>_ because _<PPD>_ is readable by such a user.

### Specifying the Destination File Type

The cupsfilter -m option allows files at each stage of the filtering process to be produced and examined. _application/pdf_ is the default output without anything for -m specified, but, depending on the filtering chain,

- _application/postscript_
    
- _application/vnd.cups-pdf_
    
- _application/vnd.cups-postscript_
    
- _application/vnd.cups-raster_
    

are also possibly wanted for this option.

In other words:

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m application/vnd.cups-raster -o number-up=2 test.ps > out.ps 2> log
~~~

would give a raster file output if that was appropriate for the print queue. The filters used with a particular queue may be obtained with [(see below):](https://wiki.debian.org/CUPSFilter#final)

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m printer/foo -e --list-filters
~~~

_vnd.cups-*_ indicates a printer-ready file; that is, a file which has print options using [the -o option](https://manpages.debian.org/man/lp "DebianMan") incorporated in it. Print options, for example, N-up, scaling and rotation, are applied to the pages in the submitted file by a filter, usually [pdftopdf](https://wiki.debian.org/DissectingandDebuggingtheCUPSPrintingSystem#pdfcentric).

Here is a small PostScript file,[test.ps](https://wiki.debian.org/CUPSFilter?action=AttachFile&do=view&target=test.ps), which you can use. As a user:

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m application/postscript -o number-up=2 test.ps > out.ps 2> log
~~~

You should find out.ps looks no different from test.ps. In fact, it is also the same size as test.ps. The log shows the only filter which runs is gziptoany. gziptoany is designed to _Copy (and uncompress) files to stdout_. The result is not unexpected because conversion is from MIME meda type PostScript to MIME media type PostScript. The -o option has not been applied. Nothing much has happened here.

How about

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m application/pdf -o number-up=2 test.ps > out.ps 2> log ?
~~~

From the log we see Ghostscript converting PostScript to PDF with pstopdf. The output looks no different from the input though. Again, the -o has not been taken account of. That's to be expected for conversion to MIME media type _application/pdf_. Progessing to

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m application/vnd.cups-pdf -o number-up=2 test.ps > out.pdf 2> log
~~~

to convert to a file suitable to be sent to a printer, probably after some further processing. pstopdf is invoked again but it is followed by pdftopdf. The latter filter performs the very important task of page management; the application of N-up is obvious in a PDF viewer.

### Obtaining the File which is Sent to the Printer

A number of the [previous commands](https://wiki.debian.org/CUPSFilter#dest) has relied solely on the _*.convs_ files in _/usr/share/cups/mime_ to determine the filters used in the filtering chain. <mark>But filters can also be specified in the PPD file with a </mark>[*cupsFilter](https://openprinting.github.io/cups/doc/spec-ppd.html#cupsFilter) or [*cupsFilter2](https://openprinting.github.io/cups/doc/spec-ppd.html#cupsFilter2) line. This will be the last filter applied in the filter chain; laserjet2200.ppd has

~~~c
*cupsFilter "application/vnd.cups-raster 100 rastertogutenprint 5.2"

~~~
To use this filter with cupsfilter

- the -e option to cupsfilter is required and
- the [destination file type](https://wiki.debian.org/CUPSFilter#dest) has to indicate that conversion to a format suitable to be sent to the printer is necessary.
    

The command to ascertain which filters will be used:

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m printer/foo -e -o number-up=2 --list-filters test.ps
~~~

The outcome:

1. gstopdf
2. pdftopdf
3. gstoraster
4. rastertogutenprint.5.2

Or diagramatically:
~~~
         +---------+     +----------+              +------------+                 +------------------------+
         |         | pdf |          | vnd.cups-pdf |            | vnd.cups-raster |                        |  
test.ps=>| gstopdf |====>| pdftopdf |=============>| gstoraster |================>| rastertogutenprint.5.2 |=>PCL
         |         |     |          |              |            |                 |                        |                           
         +---------+     +----------+              +------------+                 +------------------------+

~~~
An output file and a log of the process is obtained with:

~~~sh
/usr/sbin/cupsfilter -p laserjet2200.ppd -m printer/foo -o number-up=2 test.ps -e > out.pcl 2> log
~~~

_out.pcl_, identified as _HP PCL printer data_ by [file](https://manpages.debian.org/man/file "DebianMan"), should be viewable with [ghostpcl](https://wiki.debian.org/CUPSFilter#viewers). The file outputs for the intermediate stages can be obtained [as indicted earlier](https://wiki.debian.org/CUPSFilter#dest) and are viewable with a [PDF viewer and RasterView](https://wiki.debian.org/CUPSFilter#viewers).

As another example: the [everywhere PPD](https://wiki.debian.org/CUPSDriverlessPrinting#generator1) for an HP ENVY 4500 contains the lines

~~~c
*cupsFilter2: "image/jpeg image/jpeg 0 -"
*cupsFilter2: "image/urf image/urf 100 -"
~~~

and

~~~sh
/usr/sbin/cupsfilter -p ENVY4500.ppd -m printer/foo -e /etc/services --list-filters
~~~

shows all the filters to be employed:

1. texttopdf
2. pdftopdf
3. gstoraster
4. rastertopwg

The output of the final filter is [Apple raster](https://wiki.debian.org/CUPSDriverlessPrinting#pdls), viewable with [RasterView](https://wiki.debian.org/CUPSFilter#viewers).

### See Also

- [The Debian Printing Portal](https://wiki.debian.org/Printing)
    

---

[CategoryPrinter](https://wiki.debian.org/CategoryPrinter)

CUPSFilter ([最后修改时间 2022-02-21 17:54:06](https://wiki.debian.org/CUPSFilter?action=info))

- Debian [privacy policy](https://www.debian.org/legal/privacy), Wiki [team](https://wiki.debian.org/Teams/DebianWiki), [bugs](https://bugs.debian.org/wiki.debian.org) and [config](https://salsa.debian.org/debian/wiki.debian.org).
- Powered by [MoinMoin](https://moinmo.in/ "This site uses the MoinMoin Wiki software.") and [Python](https://moinmo.in/Python "MoinMoin is written in Python."), with hosting provided by [Metropolitan Area Network Darmstadt](https://www.man-da.de/).