import os
def walk(root):
    ret = [root]
    for a in os.listdir(root):
        if a=='.git':continue
        path = os.path.join(root,a)
        # print(a)
        if os.path.isdir(path):
            # print(a)
            ret.extend(walk(path))
        else:
            continue
    return ret
root =os.getcwd() 
dirs = walk(root)
for d in dirs:
    os.chdir(d)
    dirname = os.path.dirname(d)
    if len(dirname)>1 and dirname[0]=='.':
        continue
    os.system("python '%s'/createsidebar.py"%(root))
    pass
