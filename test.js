function run(hs) {
  let {text} = hs;
  var instance = new Mark('article');
  let nodelist = [];
  function filter(textNode, foundTerm, totalCounter, counter) {
    let text = `node${textNode.nodeName} \n${textNode.textContent} \nfinds:${foundTerm}`;
    console.log(text);
    nodelist.push(textNode);
    return true; // must return either true or false
  }
  let wildcards = 'disabled';
  let accuracy = 'partially'; // "complementary";
  let separateWordSearch = false;
  let acrossElements = true;
  let element = 'i';
  let className = 'mark-pre  docsify-highlighter';

  // <i class="docsify-highlighter highlight-wrap-hover"  data-highlight-split-type="both" data-highlight-id-extra=""
  // >filesystem tree), allowing different containers to have different views of that resource. Namespaces are tightly tied to process trees; they are created with new processes </i>
  let done = (counter) => {
    let ii = Array.from(document.querySelectorAll('.mark-pre'));
    let highlight_id = '9c211d5f-961a-44b3-bf1c-062b58bc6543';
    let style = 'background-color: rgb(51, 255, 51);color:yellow;';
    ii.forEach(element => {
      const data_highlighter = 'data-highlight-id';
      element.setAttribute(data_highlighter, highlight_id);
      element.setAttribute('style', style);
      console.log(element);
    });
  };
  let options = {
    filter,
    wildcards,
    accuracy,
    separateWordSearch,
    acrossElements,
    element,
    className,
    done,
  };
  let ret = instance.mark(text, options);
  console.log(nodelist);
  console.log(ret);
  return instance;
}
window.searchtext = text => run({text});
