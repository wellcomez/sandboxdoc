---
date: 2023-03-24 12:18
title: deniable encryption
tags:
-  deniable Encryption
---


# deniable Encryption

From Wikipedia, the free encyclopedia

在[密码学](https://zh.wikipedia.org/wiki/%E5%AF%86%E7%A0%81%E5%AD%A6 "密码学")和[隐写术](https://zh.wikipedia.org/wiki/%E9%9A%90%E5%86%99%E6%9C%AF "隐写术")中，合情**可否认加密**描述了一种[加密](https://zh.wikipedia.org/wiki/%E5%8A%A0%E5%AF%86 "加密")技术，其中某加密文件或信息的存在性在使对方无法证明[明文](https://zh.wikipedia.org/wiki/%E6%98%8E%E6%96%87 "明文")存在的意义上可以否认。[[1]](https://zh.wikipedia.org/wiki/%E5%8F%AF%E5%90%A6%E8%AE%A4%E5%8A%A0%E5%AF%86#cite_note-1)

用户可以[合理地推诿](https://zh.wikipedia.org/wiki/%E5%90%88%E7%90%86%E6%8E%A8%E8%AF%BF "合理推诿")，否认特定数据经加密，否认用户能解密特定数据，或否认特定加密数据存在。这种否认可能为真，也可能不是。例如，如果用户不合作，可能无法证明数据是加密的。如果数据是加密的，用户可能确实无法将其解密。可否认加密可以用来削弱攻击者对数据是加密的或数据拥有者能够解密并提供相关[明文](https://zh.wikipedia.org/wiki/%E6%98%8E%E6%96%87 "明文")的信心。

In [cryptography](https://en.wikipedia.org/wiki/Cryptography "Cryptography") and [steganography](https://en.wikipedia.org/wiki/Steganography "Steganography"), plausibly **deniable encryption** describes [encryption](https://en.wikipedia.org/wiki/Encryption "Encryption") techniques where the existence of an encrypted file or message is deniable in the sense that an adversary cannot prove that the [plaintext](https://en.wikipedia.org/wiki/Plaintext "Plaintext") data exists.[[1]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-1)

The users may [convincingly deny](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability") that a given piece of data is encrypted, or that they are able to decrypt a given piece of encrypted data,[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] or that some specific encrypted data exists. Such denials may or may not be genuine. For example, it may be impossible to prove that the data is encrypted without the cooperation of the users. If the data is encrypted, the users genuinely may not be able to decrypt it. Deniable encryption serves to undermine an attacker's confidence either that data is encrypted, or that the person in possession of it can decrypt it and provide the associated [plaintext](https://en.wikipedia.org/wiki/Plaintext "Plaintext").

## Function

Deniable encryption makes it impossible to prove the existence of the plaintext message without the proper decryption key. This may be done by allowing an encrypted message to be decrypted to different sensible plaintexts, depending on the [key](https://en.wikipedia.org/wiki/Key_(cryptography) "Key (cryptography)") used. This allows the sender to have [plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability") if compelled to give up their encryption key. The notion of "deniable encryption" was used by [Julian Assange](https://en.wikipedia.org/wiki/Julian_Assange "Julian Assange") and [Ralf Weinmann](https://en.wikipedia.org/w/index.php?title=Ralf_Weinmann&action=edit&redlink=1 "Ralf Weinmann (page does not exist)") in the [Rubberhose filesystem](https://en.wikipedia.org/wiki/Rubberhose_(file_system) "Rubberhose (file system)")[[2]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-2) and explored in detail in a paper by [Ran Canetti](https://en.wikipedia.org/wiki/Ran_Canetti "Ran Canetti"), [Cynthia Dwork](https://en.wikipedia.org/wiki/Cynthia_Dwork "Cynthia Dwork"), [Moni Naor](https://en.wikipedia.org/wiki/Moni_Naor "Moni Naor"), and [Rafail Ostrovsky](https://en.wikipedia.org/wiki/Rafail_Ostrovsky "Rafail Ostrovsky")[[3]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-3) in 1996.

### Scenario

Deniable encryption allows the sender of an encrypted message to deny sending that message. This requires a trusted third party. A possible scenario works like this:

1.  Bob suspects his wife [Alice](https://en.wikipedia.org/wiki/Alice_and_Bob "Alice and Bob") is engaged in adultery. That being the case, Alice wants to communicate with her secret lover Carl. She creates two keys, one intended to be kept secret, the other intended to be sacrificed. She passes the secret key (or both) to Carl.
2.  Alice constructs an innocuous message M1 for Carl (intended to be revealed to Bob in case of discovery) and an incriminating love letter M2 to Carl. She constructs a cipher-text C out of both messages, M1 and M2, and emails it to Carl.
3.  Carl uses his key to decrypt M2 (and possibly M1, in order to read the fake message, too).
4.  Bob finds out about the email to Carl, becomes suspicious and forces Alice to decrypt the message.
5.  Alice uses the sacrificial key and reveals the innocuous message M1 to Bob. Since it is impossible for Bob to know for sure that there might be other messages contained in C, he might assume that there _are_ no other messages (alternatively, Bob may not be familiar with the concept of plausible encryption in the first place, and thus may not be aware it is even possible for C to contain more than one message).

Another possible scenario involves Alice sending the same ciphertext (some secret instructions) to Bob and Carl, to whom she has handed different keys. Bob and Carl are to receive different instructions and must not be able to read each other's instructions. Bob will receive the message first and then forward it to Carl.

1.  Alice constructs the ciphertext out of both messages, M1 and M2, and emails it to Bob.
2.  Bob uses his key to decrypt M1 and isn't able to read M2.
3.  Bob forwards the ciphertext to Carl.
4.  Carl uses his key to decrypt M2 and isn't able to read M1.

## Forms of deniable encryption

Normally, ciphertexts decrypt to a single plaintext that is intended to be kept secret. However, one form of deniable encryption allows its users to decrypt the ciphertext to produce a different (innocuous but plausible) plaintext and plausibly claim that it is what they encrypted. The holder of the ciphertext will not be able to differentiate between the true plaintext, and the bogus-claim plaintext. In general, one [ciphertext](https://en.wikipedia.org/wiki/Ciphertext "Ciphertext") cannot be decrypted to all possible [plaintexts](https://en.wikipedia.org/wiki/Plaintext "Plaintext") unless the key is as large as the [plaintext](https://en.wikipedia.org/wiki/Plaintext "Plaintext"), so it is not practical in most cases for a ciphertext to reveal no information whatsoever about its plaintext.[[4]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-4) However, some schemes allow decryption to decoy plaintexts that are close to the original in some metric (such as [edit distance](https://en.wikipedia.org/wiki/Edit_distance "Edit distance")). [[5]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-5)

Modern deniable encryption techniques exploit the fact that without the key, it is infeasible to distinguish between ciphertext from [block ciphers](https://en.wikipedia.org/wiki/Block_cipher "Block cipher") and data generated by a [cryptographically secure pseudorandom number generator](https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator "Cryptographically secure pseudorandom number generator") (the cipher's [pseudorandom permutation](https://en.wikipedia.org/wiki/Pseudorandom_permutation "Pseudorandom permutation") properties).[[6]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-6)

This is used in combination with some [decoy](https://en.wikipedia.org/wiki/Decoy "Decoy") data that the user would plausibly want to keep confidential that will be revealed to the attacker, claiming that this is all there is. This is a form of [steganography](https://en.wikipedia.org/wiki/Steganography "Steganography").

If the user does not supply the correct key for the truly secret data, decrypting it will result in apparently random data, indistinguishable from not having stored any particular data there.

One example of deniable encryption is a [cryptographic filesystem](https://en.wikipedia.org/wiki/Cryptographic_filesystem "Cryptographic filesystem") that employs a concept of abstract "layers", where each layer can be decrypted with a different encryption key. Additionally, special "[chaff](https://en.wikipedia.org/wiki/Chaff_(radar_countermeasure) "Chaff (radar countermeasure)") layers" are filled with random data in order to have [plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability") of the existence of real layers and their encryption keys. The user can store decoy files on one or more layers while denying the existence of others, claiming that the rest of space is taken up by chaff layers. Physically, these types of filesystems are typically stored in a single directory consisting of equal-length files with filenames that are either [randomized](https://en.wikipedia.org/wiki/Randomness "Randomness") (in case they belong to chaff layers), or [cryptographic hashes](https://en.wikipedia.org/wiki/Cryptographic_hash_function "Cryptographic hash function") of strings identifying the blocks. The [timestamps](https://en.wikipedia.org/wiki/Timestamp "Timestamp") of these files are always randomized. Examples of this approach include [Rubberhose filesystem](https://en.wikipedia.org/wiki/Rubberhose_(file_system) "Rubberhose (file system)") and [PhoneBookFS](https://en.wikipedia.org/w/index.php?title=PhoneBookFS&action=edit&redlink=1 "PhoneBookFS (page does not exist)").

Another approach used by some conventional [disk encryption software](https://en.wikipedia.org/wiki/Disk_encryption_software "Disk encryption software") suites is creating a second encrypted [volume](https://en.wikipedia.org/wiki/Volume_(computing) "Volume (computing)") within a container volume. The container volume is first formatted by filling it with encrypted random data,[[7]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-LibreCrypt-7) and then initializing a filesystem on it. The user then fills some of the filesystem with legitimate, but plausible-looking decoy files that the user would seem to have an incentive to hide. Next, a new encrypted volume (the hidden volume) is allocated within the free space of the container filesystem which will be used for data the user actually wants to hide. Since an adversary cannot differentiate between encrypted data and the random data used to initialize the outer volume, this inner volume is now undetectable. [LibreCrypt](https://en.wikipedia.org/wiki/LibreCrypt "LibreCrypt")[[8]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-LibreCrypt-plausible-deniability-8) and [BestCrypt](https://en.wikipedia.org/wiki/BestCrypt "BestCrypt") can have many hidden volumes in a container; [TrueCrypt](https://en.wikipedia.org/wiki/TrueCrypt "TrueCrypt") is limited to one hidden volume.[[9]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-truecrypt.org-9)

### Detection

The existence of hidden encrypted data may be revealed by flaws in the implementation.[[10]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-10) It may also be revealed by a so-called [watermarking attack](https://en.wikipedia.org/wiki/Watermarking_attack "Watermarking attack") if an inappropriate cipher mode is used.[[11]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-11) The existence of the data may be revealed by it 'leaking' into non-encrypted disk space [[12]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-12) where it can be detected by [forensic](https://en.wikipedia.org/wiki/Computer_forensics "Computer forensics") tools.[[13]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-13)

Doubts have been raised about the level of plausible deniability in 'hidden volumes'[[14]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-14) – the contents of the "outer" container filesystem have to be 'frozen' in its initial state to prevent the user from corrupting the hidden volume (this can be detected from the access and modification timestamps), which could raise suspicion. This problem can be eliminated by instructing the system not to protect the hidden volume, although this could result in lost data.

### Drawbacks

Possession of deniable encryption tools could lead attackers to continue torturing a user even after the user has revealed all their keys, because the attackers could not know whether the user had revealed their last key or not. However, knowledge of this fact can disincentivize users from revealing any keys to begin with, since they will never be able to prove to the attacker that they have revealed their last key. [[15]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-15)

## Deniable authentication

Some in-transit encrypted messaging suites, such as [Off-the-Record Messaging](https://en.wikipedia.org/wiki/Off-the-Record_Messaging "Off-the-Record Messaging"), offer [deniable authentication](https://en.wikipedia.org/wiki/Deniable_authentication "Deniable authentication") which gives the participants [plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability") of their conversations. While deniable authentication is not technically "deniable encryption" in that the encryption of the messages is not denied, its deniability refers to the inability of an adversary to prove that the participants had a conversation or said anything in particular.

This is achieved by the fact that all information necessary to forge messages is appended to the encrypted messages – if an adversary is able to create digitally authentic messages in a conversation (see [hash-based message authentication code](https://en.wikipedia.org/wiki/Hash-based_message_authentication_code "Hash-based message authentication code") (HMAC)), they are also able to [forge](https://en.wikipedia.org/wiki/Forgery "Forgery") messages in the conversation. This is used in conjunction with [perfect forward secrecy](https://en.wikipedia.org/wiki/Perfect_forward_secrecy "Perfect forward secrecy") to assure that the compromise of encryption keys of individual messages does not compromise additional conversations or messages.

## Software

-   [OpenPuff](https://en.wikipedia.org/wiki/OpenPuff "OpenPuff"), freeware semi-open-source steganography for MS Windows.
-   [LibreCrypt](https://en.wikipedia.org/wiki/LibreCrypt "LibreCrypt"), [opensource](https://en.wikipedia.org/wiki/Opensource "Opensource") [transparent disk encryption](https://en.wikipedia.org/wiki/Disk_encryption "Disk encryption") for MS Windows and PocketPC PDAs that provides both deniable encryption and [plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability").[[7]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-LibreCrypt-7)[[16]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-16) Offers an extensive range of encryption options, and doesn't need to be installed before use as long as the user has administrator rights.
-   [Off-the-Record Messaging](https://en.wikipedia.org/wiki/Off-the-Record_Messaging "Off-the-Record Messaging"), a cryptographic technique providing true deniability for instant messaging.
-   [Rubberhose](https://en.wikipedia.org/wiki/Rubberhose_(file_system) "Rubberhose (file system)"), defunct project (last release in 2000, not compatible with modern Linux distributions)
-   [StegFS](https://en.wikipedia.org/wiki/StegFS "StegFS"), the current successor to the ideas embodied by the Rubberhose and PhoneBookFS filesystems.
-   [VeraCrypt](https://en.wikipedia.org/wiki/VeraCrypt "VeraCrypt") (a successor to a discontinued [TrueCrypt](https://en.wikipedia.org/wiki/TrueCrypt "TrueCrypt")), an [on-the-fly disk encryption](https://en.wikipedia.org/wiki/OTFE "OTFE") software for Windows, Mac and Linux providing limited deniable encryption[[17]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-17) and to some extent (due to limitations on the number of hidden volumes which can be created[[9]](https://en.wikipedia.org/wiki/Deniable_encryption#cite_note-truecrypt.org-9)) [plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability"), without needing to be installed before use as long as the user has full administrator rights.
-   [Vanish](https://en.wikipedia.org/wiki/Vanish_(computer_science) "Vanish (computer science)"), a research prototype implementation of self-destructing data storage.

## See also

-   [Chaffing and winnowing](https://en.wikipedia.org/wiki/Chaffing_and_winnowing "Chaffing and winnowing") – cryptographic technique to achieve confidentiality without using encryption
-   [Deniable authentication](https://en.wikipedia.org/wiki/Deniable_authentication "Deniable authentication") – message authentication between a set of participants where the participants themselves can be confident in the authenticity of the messages, but it cannot be proved to a third party after the event
-   [dm-crypt](https://en.wikipedia.org/wiki/Dm-crypt "Dm-crypt") – disk encryption software
-   [Key disclosure law](https://en.wikipedia.org/wiki/Key_disclosure_law "Key disclosure law") – Legislation that requires individuals to surrender cryptographic keys to law enforcement
-   [Plausible deniability](https://en.wikipedia.org/wiki/Plausible_deniability "Plausible deniability") – Ability to deny responsibility
-   [Rubber-hose cryptanalysis](https://en.wikipedia.org/wiki/Rubber-hose_cryptanalysis "Rubber-hose cryptanalysis") – Extraction of cryptographic secrets by coercion or torture
-   [Steganography](https://en.wikipedia.org/wiki/Steganography "Steganography") – Hiding messages in other messages
-   [Unicity distance](https://en.wikipedia.org/wiki/Unicity_distance "Unicity distance") – Length of ciphertext needed to unambiguously break a cipher