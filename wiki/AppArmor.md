---
layout: post
tags:
- apparmor
- selinux
title: "AppArmor"

---

# AppArmor
AppArmor 最初由 Immunix 开发，随后由 Novell 维护，它是 SELinux 的替代方法，也使用了 Linux 安全模块（LSM）框架。由于 SELinux 和 AppArmor 使用了同样的框架，所以它们可以互换。AppArmor 的开发初衷是因为人们认为 SELinux 太过复杂，不适合普通用户管理。AppArmor 包含一个完全可配置的 MAC 模型和一个学习模式。SELinux 的一个问题在于，它需要一个支持扩展属性的文件系统；而 AppArmor 对文件系统没有任何要求。您可以在 SUSE、OpenSUSE，以及 Ubuntu 中找到 AppArmor。 同样，在redhat、centos、fedora上，也会找到SElinux 。

### 一、易用性与安全性

易用性：Novell 出的[apparmor-overview文档](http://pan.baidu.com/s/1sjlVT3B)的第19页中做了一个对比，同样对一个ftp程序做相同的限制，使用apparmor的规则代码只是SELinux的1/4 ，而且从代码的可读性上来看，apparmor的代码也更易理解和易读。

安全性：安全性上，毋庸置疑，SELinux更安全，看下[SELinux](http://baike.baidu.com/view/487687.htm)的出身，其是美国国家安全局「NSA」和SCC（Secure Computing Corporation）开发的 Linux的一个扩张强制访问控制安全模块。再从理论上也可以了解到SELinux与Apparmor最大的区别在于：Apparmor使用文件名（路径名）最为安全标签，而SELinux使用文件的inode作为安全标签，这就意味着，Apparmor机制可以通过修改文件名而被绕过，另外，在文件系统中，只有inode才具有唯一性。

### 二、apparmor的用法相关

#### 1、查询当前Apparmor的状态

~~~
1.  ops@361way:~$ sudo apparmor_status
2.  apparmor module is loaded.
3.  6 profiles are loaded.
4.  6 profiles are in enforce mode.
5.  /sbin/dhclient
6.  /usr/lib/NetworkManager/nm-dhcp-client.action
7.  /usr/lib/connman/scripts/dhclient-script
8.  /usr/sbin/mysqld
9.  /usr/sbin/ntpd
10.  /usr/sbin/tcpdump
11.  0 profiles are in complain mode.
12.  2 processes have profiles defined.
13.  2 processes are in enforce mode.
14.  /usr/sbin/mysqld (1025)
15.  /usr/sbin/ntpd (2819)
16.  0 processes are in complain mode.
17.  0 processes are unconfined but have a profile defined.
~~~

Apparmor包含了6个profile文件，而且都处于enforce状态，没有处于enforce状态的文件。

Apparmor的profile文件分为两类：enforce与complain mode，存在于/etc/apparmor.d/目录下，对此官方也对于两种状态给予了相应的解释：

**Enforcing**

1.  Enforcing: This means the profile is actively protecting the application. By default, Ubuntu already locks down the CUPS daemon for you, but you will see several other profiles listed that you can set to enforce mode at any time.

简单理解就是：如果某个程序不符合其profile文件的限制，程序行为将会失败。

**Complain**

1.  Complain: This means a profile exists but is not yet actively protecting the application. Instead, it is sort of in "debug" mode and will put "complain" messages into /var/log/messages. What this means is that if the application wants to read, write, or execute something that isn't listed in the profile, it will complain. This is how you generally create a profile.

简单理解就是：如果某个程序不符合其profile文件的限制，改程序就会被apparmor“打小报告”，即将该程序的行为记录在系统日志中，但是程序访问行为会成功，比如本来没有让某个程序访问某个文件，但就是访问，仅仅报告一下，文件访问会成功，如果在enforce模式下，文件访问就会失败。

#### 2、更改profile文件的状态

如果想把某个profile置为enforce状态，执行如下命令：

1.  sudo enforce <application_name>

如果想把某个profile置为complain状态，执行如下命令：

1.  sudo complain <application_name>

在修改了某个profile的状态后，执行如下命令使之生效：

1.  sudo /etc/init.d/apparmor restart

#### 3、profile文件的构建与管理

ubuntu发行版预定义了一些profile，可以通过如下命令安装ubuntu下的些预定义的profile文件：

1.  sudo apt-get install apparmor-profiles

通过工具来管理profile，比较著名是：apparmor-utils，通过如下命令进行安装：

1.  sudo apt-get install apparmor-utils

此工具最常用的两个命令为：aa-genprof和aa-logprof，前者用来生成profile文件，后者用来查询处于apparmor的日志记录，如：

1.  sudo aa-genprof slapd //生成openldap程序的profile文件

该包中也还有其他工具，具体可以参看[ubuntu server guide apparmor](https://wiki.ubuntu.com/AppArmor) 页面，这里列出了最近版本的用法。

#### 4、手工编写profile文件

一个示例如下：
~~~c
1.  #include <tunables/global>
2.  /usr/bin/kopete { //需要限制的应用程序的名称
3.  #include <abstractions/X>
4.  #include <abstractions/audio>
5.  #include <abstractions/base>
6.  #include <abstractions/kde>
7.  #include <abstractions/nameservice>
8.  #include <abstractions/user-tmp>
9.  //限制其在对家目录下几个文件的读写权限
10.  deny @{HOME}/.bash* rw,
11.  deny @{HOME}/.cshrc rw,
12.  deny @{HOME}/.profile rw,
13.  deny @{HOME}/.ssh/* rw,
14.  deny @{HOME}/.zshrc rw,
15.  //对以下文件具有读、写、或可执行的权限
16.  /etc/X11/cursors/oxy-white.theme r,
17.  /etc/default/apport r,
18.  /etc/kde4/* r,
19.  /etc/kde4rc r,
20.  /etc/kderc r,
21.  /etc/security/* r,
22.  /etc/ssl/certs/* r,
23.  owner /home/*/ r,
24.  /opt/firefox/firefox.sh Px,
25.  /usr/bin/convert rix,
26.  /usr/bin/kde4 rix,
27.  /usr/bin/kopete r,
28.  /usr/bin/kopete_latexconvert.sh rix,
29.  /usr/bin/launchpad-integration ix,
30.  /usr/bin/xdg-open mrix,
31.  /usr/lib/firefox*/firefox.sh Px,
32.  /usr/lib/kde4/**.so mr,
33.  /usr/lib/kde4/libexec/drkonqi ix,
34.  /usr/share/emoticons/ r,
35.  /usr/share/emoticons/** r,
36.  /usr/share/enchant/** r,
37.  /usr/share/kde4/** r,
38.  /usr/share/kubuntu-default-settings/** r,
39.  /usr/share/locale-langpack/** r,
40.  /usr/share/myspell/** r,
41.  owner @{HOME}/.config/** rwk,
42.  owner @{HOME}/.kde/** rwlk,
43.  owner @{HOME}/.local/share/mime/** r,
44.  owner @{HOME}/.thumbnails/** rw,
45.  owner @{HOME}/Downloads/ rw,
46.  owner @{HOME}/Downloads/** rw,
47.  }
~~~
想了解更多，可以查看[ubuntu论坛](http://ubuntuforums.org/showthread.php?t=1008906)里的相关内容。这里只对部他内容作下简要说明：

~~~
1.  r = read
2.  w = write
3.  l = link
4.  k = lock
5.  a = append
6.  ix = inherit = Inherit the parent's profile.
7.  px = requires a separate profile exists for the application, with environment scrubbing.
8.  Px = requires a separate profile exists for the application, without environment scrubbing.
9.  ux and Ux = Allow execution of an application unconfined, with and without environmental scrubbing. (use with caution if at all).
10.  m = allow executable mapping.
~~~

注：apparmor的相关信息也可以参看[debian wiki页面](https://wiki.debian.org/AppArmor/HowTo) 。

### 三、SELinux相关

#### 1、SELinux模式

SELinux 拥有三个基本的操作模式，当中 Enforcing 是缺省的模式。此外，它还有一个 targeted 或 mls 的修饰语。这管制 SELinux 规则的应用有多广泛，当中 targeted 是较宽松的级别。

1.  Enforcing： 这个缺省模式会在系统上启用并实施 SELinux 的安全性政策，拒绝访问及记录行动
2.  Permissive： 在 Permissive 模式下，SELinux 会被启用但不会实施安全性政策，而只会发出警告及记录行动。Permissive 模式在排除 SELinux 的问题时很有用
3.  Disabled： SELinux 已被停用

从上面不难看出，两者在强制模式上作用一样，而SELinux的宽松模式（Permissive）对应着apparmor的complain模式。

#### 2、SELinux的模式查看与修改

可以通过sestatus命令查看SELinux的状况：

~~~
1.  # sestatus
2.  SELinux status: enabled
3.  SELinuxfs mount: /selinux
4.  Current mode: enforcing
5.  Mode from config file: enforcing
6.  Policy version: 21
7.  Policy from config file: targeted
~~~

可以运行如下命令，修改SELinux的运行状态：

1.  setenforce [ Enforcing | Permissive | 1 | 0 ]

#### 3、查看程序或文件的SELinux信息

常见的属于 [coreutils 的工具](http://www.gnu.org/software/coreutils/manual/html_node/index.html)如 ps、ls 等等，可以通过增加 Z 选项的方式获知 SELinux 方面的信息。如：

~~~
1.  #ps auxZ | grep lldpad
2.  system_u:system_r:initrc_t:s0 root 1000 8.9 0.0 3040 668 ? Ss 21:01
3.  6:08 /usr/sbin/lldpad -d
4.  #ls -Z /usr/lib/xulrunner-2/libmozjs.so
5.  -rwxr-xr-x. root root system_u:object_r:lib_t:s0 /usr/lib/xulrunner-2/libmozjs.so
~~~

#### 4、配置示例

需求：让 Apache 可以访问位于非默认目录下的网站文件/data1/361way.com

1、先用 semanage fcontext -l | grep '/var/www' 获知默认 /var/www 目录的 SELinux 上下文：

1.  /var/www(/.*)? all files system_u:object_r:httpd_sys_content_t:s0

从中可以看到 Apache 只能访问包含 httpd_sys_content_t 标签的文件。假设希望 Apache 使用 /data1/361way.com 作为网站文件目录，那么就需要给这个目录下的文件增加 httpd_sys_content_t 标签，分两步实现：

**a、** /data1/361way.com 这个目录下的文件添加默认标签类型

1.  semanage fcontext -a -t httpd_sys_content_t '/data1/361way.com(/.*)?'

**b、**用新的标签类型标注已有文件

1.  restorecon -Rv /data1/361way.com

之后 Apache 就可以使用该目录下的文件构建网站了。

其中 restorecon 在 SELinux 管理中很常见，起到恢复文件默认标签的作用。比如当从用户主目录下将某个文件复制到 Apache 网站目录下时，Apache 默认是无法访问，因为用户主目录的下的文件标签是 user_home_t。此时就需要 restorecon 将其恢复为可被 Apache 访问的 httpd_sys_content_t 类型：

~~~
1.  restorecon -v /data1/361way.com/foo.com/html/file.html
2.  restorecon reset /data1/361way.com/foo.com/html/file.html context unconfined_u:object_r:user_home_t:s0->system_u:object_r:httpd_sys_content_t:s0
~~~

SELinux更多的介绍和用法，可以参看centos wiki页在上的相关介绍：

[http://wiki.centos.org/zh/HowTos/SELinux](http://wiki.centos.org/zh/HowTos/SELinux)

[http://wiki.centos.org/zh/TipsAndTricks/SelinuxBooleans](http://wiki.centos.org/zh/TipsAndTricks/SelinuxBooleans)

### 四、第三方的对方比数据

关于这方面的资料查找到了 [日立软件 AppArmor与SELinux的内部使用情况对比](http://pan.baidu.com/s/1i3EC2zF) 测试报告。最后做个小总结，个人对于centos/redhat系 和 opensuse/ubuntu系 都没有偏见，毕竟都是优秀的产品。在apparmor与selinux 的选择上，也都有各自的考量。如果仅从配置与入用的难易程序上来说，apparmor做的更好上些；从追求更高的安全性上来说，selinux上更优一些，而且从日立软件的测试结果来看，也比较推荐使用selinux而不是apparmor 。

注：也可以试试两者的图形化配置工具，apparmor有图形化的配置工具(记不住名字了 suse的图形配置中心里可以找到)，selinux也有相应的图形配置工具system-config-selinux 。

作者：[张瑞153169](http://www.cnblogs.com/zhangrui153169/)

出处：[http://www.cnblogs.com/zhangrui153169/](http://www.cnblogs.com/zhangrui153169/)