---
date: 2023-03-28 15:07
title: linux from scratch
tags:
- linux-from-scratch
---


# linux from scratch

### 7.2. Changing Ownership
The commands in the remainder of this book must be performed while logged in as user root and no longer as user lfs. Also, double check that $LFS is set in root's environment.

Currently, the whole directory hierarchy in $LFS is owned by the user lfs, a user that exists only on the host system. If the directories and files under $LFS are kept as they are, they will be owned by a user ID without a corresponding account. This is dangerous because a user account created later could get this same user ID and would own all the files under $LFS, thus exposing these files to possible malicious manipulation.

To address this issue, change the ownership of the $LFS/* directories to user root by running the following command:

~~~sh
chown -R root:root $LFS/{usr,lib,var,etc,bin,sbin,tools}
case $(uname -m) in
  x86_64) chown -R root:root $LFS/lib64 ;;
esac
~~~


### 7.3. Preparing Virtual Kernel File Systems
https://www.linuxfromscratch.org/lfs/view/development/chapter07/kernfs.html
Applications running in userspace utilize various file systems created by the kernel to communicate with the kernel itself. These file systems are virtual: no disk space is used for them. The content of these file systems resides in memory. These file systems must be mounted in the $LFS directory tree so the applications can find them in the chroot environment.

Begin by creating the directories on which these virtual file systems will be mounted:

```sh
mkdir -pv $LFS/{dev,proc,sys,run}
```
#### 7.3.1. Mounting and Populating /dev
During a normal boot of an LFS system, t<mark style="background: #FF5582A6;">he kernel automatically mounts the `devtmpfs` file system on the /dev directory; </mark>the<mark style="background: #D2B3FFA6;"> kernel creates device nodes on that virtual file system </mark>during the boot process, or when a device is first detected or accessed. The udev daemon may change the ownership or permissions of the device nodes created by the kernel, and create new device nodes or symlinks, to ease the work of distro maintainers and system administrators. (See Section 9.3.2.2, “Device Node Creation” for details.) <mark style="background: #D2B3FFA6;">If the host kernel supports devtmpfs, we can simply mount a devtmpfs at $LFS/dev and rely on the kernel to populate it.</mark>

But some host kernels lack devtmpfs support; these host distros use different methods to create the content of /dev. So the only host-agnostic way to populate the $LFS/dev directory is by bind mounting the host system's /dev directory. A bind mount is a special type of mount that makes a directory subtree or a file visible at some other location. Use the following command to do this.
~~~
mount -v --bind /dev $LFS/dev
~~~
#### 7.3.2. Mounting Virtual Kernel File Systems
Now mount the remaining virtual kernel file systems:

~~~sh
mount -v --bind /dev/pts $LFS/dev/pts
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys
mount -vt tmpfs tmpfs $LFS/run
~~~
In some host systems, <mark style="background: #FFB8EBA6;">/dev/shm is a symbolic link to /run/shm. The /run tmpfs was mounted above so in this case only a directory needs to be created</mark>.

In other host systems /dev/shm is a mount point for a tmpfs. In that case the mount of /dev above will only create /dev/shm as a directory in the chroot environment. In this situation we must explicitly mount a tmpfs:

~~~sh
if [ -h $LFS/dev/shm ]; then
  mkdir -pv $LFS/$(readlink $LFS/dev/shm)
else
  mount -t tmpfs -o nosuid,nodev tmpfs $LFS/dev/shm
fi
~~~

### 7.4. Entering the Chroot Environment
Now that all the packages which are required to build the rest of the needed tools are on the system, it is time to enter the chroot environment and finish installing the temporary tools. This environment will also be used to install the final system. As user root, run the following command to enter the environment that is, at the moment, populated with nothing but temporary tools:
~~~sh
chroot "$LFS" /usr/bin/env -i   \
    HOME=/root                  \
    TERM="$TERM"                \
    PS1='(lfs chroot) \u:\w\$ ' \
    PATH=/usr/bin:/usr/sbin     \
    /bin/bash --login
~~~
    
The -i option given to the env command will clear all the variables in the chroot environment. After that, only the HOME, TERM, PS1, and PATH variables are set again. The TERM=$TERM construct sets the TERM variable inside chroot to the same value as outside chroot. This variable is needed so programs like vim and less can operate properly. If other variables are desired, such as CFLAGS or CXXFLAGS, this is a good place to set them.

From this point on, there is no need to use the LFS variable any more because all work will be restricted to the LFS file system; the chroot command runs the Bash shell with the root (/) directory set to $LFS.

Notice that /tools/bin is not in the PATH. This means that the cross toolchain will no longer be used.

Note that the bash prompt will say I have no name! This is normal because the /etc/passwd file has not been created yet.

[Note] Note
It is important that all the commands throughout the remainder of this chapter and the following chapters are run from within the chroot environment. If you leave this environment for any reason (rebooting for example), ensure that the virtual kernel filesystems are mounted as explained in Section 7.3.1, “Mounting and Populating /dev” and Section 7.3.2, “Mounting Virtual Kernel File Systems” and enter chroot again before continuing with the installation.