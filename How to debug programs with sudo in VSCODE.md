
---
date: 2023-04-06 22:43
title: How to debug programs with sudo in VSCODE
tags:
- debug
  
---



#  How to debug programs with "sudo" in VSCODE


[Stack Overflow](https://stackoverflow.com/)

1.  [About](https://stackoverflow.co/)
2.  [Products](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#)
3.  [For Teams](https://stackoverflow.com/teams)

3.  [Log in](https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2fquestions%2f40033311%2fhow-to-debug-programs-with-sudo-in-vscode%2f75823517)
4.  [Sign up](https://stackoverflow.com/users/signup?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2fquestions%2f40033311%2fhow-to-debug-programs-with-sudo-in-vscode%2f75823517)

**Join Stack Overflow** to find the best answer to your technical question, help others answer theirs.

[Sign up with email](https://stackoverflow.com/users/signup?ssrc=hero&returnurl=https%3a%2f%2fstackoverflow.com%2fquestions%2f40033311%2fhow-to-debug-programs-with-sudo-in-vscode%2f75823517) Sign up with Google Sign up with GitHub Sign up with Facebook

1.  [
    
    Home
    
    
    
    ](https://stackoverflow.com/)
2.  1.  PUBLIC
    2.  [Questions](https://stackoverflow.com/questions)
    3.  [
        
        Tags
        
        
        
        ](https://stackoverflow.com/tags)
    4.  [
        
        Users
        
        
        
        ](https://stackoverflow.com/users)
    5.  [
        
        Companies
        
        
        
        ](https://stackoverflow.com/jobs/companies?so_medium=stackoverflow&so_source=SiteNav)
    6.  COLLECTIVES
        
    7.  [Explore Collectives](https://stackoverflow.com/collectives)
3.  TEAMS
    
    **Stack Overflow for Teams** – Start collaborating and sharing organizational knowledge.![](https://cdn.sstatic.net/Img/teams/teams-illo-free-sidebar-promo.svg?v=47faa659a05e)[Create a free Team](https://try.stackoverflow.co/why-teams/?utm_source=so-owned&utm_medium=side-bar&utm_campaign=campaign-38&utm_content=cta) [Why Teams?](https://stackoverflow.co/teams)
    

# [How to debug programs with "sudo" in VSCODE](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode)

[Ask Question](https://stackoverflow.com/questions/ask)

Asked 6 years, 6 months ago

Modified [20 days ago](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517?lastactivity "2023-03-23 13:18:45Z")

Viewed 47k times

32

[](https://stackoverflow.com/posts/40033311/timeline)

I am trying to debug a program in VSCODE. The program needs to be launched as root or with "sudo" on Ubuntu. What's the best way to achieve this? An example launch configuration would be helpful. Thanks.

-   [c++](https://stackoverflow.com/questions/tagged/c%2b%2b "show questions tagged 'c++'")
-   [c](https://stackoverflow.com/questions/tagged/c "show questions tagged 'c'")
-   [visual-studio-code](https://stackoverflow.com/questions/tagged/visual-studio-code "show questions tagged 'visual-studio-code'")

[Share](https://stackoverflow.com/q/40033311 "Short permalink to this question")

[Improve this question](https://stackoverflow.com/posts/40033311/edit)

Follow

asked Oct 14, 2016 at 0:58

[![SecureStack's user avatar](https://www.gravatar.com/avatar/3bf18e6ae47f9ebb0a3834290b9b7f71?s=64&d=identicon&r=PG&f=1)](https://stackoverflow.com/users/7016597/securestack)[SecureStack](https://stackoverflow.com/users/7016597/securestack)

32311 gold badge33 silver badges55 bronze badges

-   7
    
    The only way to debug a process running as root is with a debugger running as root. This is a fundamental aspect of the POSIX security model. Vscode cannot do anything about it. So, you'll have to run vscode as root. 
    
    – [Sam Varshavchik](https://stackoverflow.com/users/3943312/sam-varshavchik "110,988 reputation")
    
     [Oct 14, 2016 at 1:00](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment67343706_40033311)
    
-   4
    
    @SamVarshavchik: At least when running on Linux, vscode does debugging by invoking a debugger (gdb or lldb) as a separate process, the path to which is specified as `miDebuggerPath` ain a file named `launch.json`. You can probably specify `sudo /usr/bin/gdb`, but you'll probably also want to use `--askpass` to specify an alternative way for it to get credentials, since the user probably won't be able to interact directly with `sudo` to authenticate. 
    
    – [Jerry Coffin](https://stackoverflow.com/users/179910/jerry-coffin "469,968 reputation")
    
     [Oct 14, 2016 at 1:34](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment67344177_40033311)
    
-   [Relevant](https://github.com/Microsoft/vscode-cpptools/blob/master/launch.md). 
    
    – [Jerry Coffin](https://stackoverflow.com/users/179910/jerry-coffin "469,968 reputation")
    
     [Oct 14, 2016 at 2:01](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment67344569_40033311)
    

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

---

## 8 Answers

Sorted by:

                                              Highest score (default)                                                                   Trending (recent votes count more)                                                                   Date modified (newest first)                                                                   Date created (oldest first)                              

28

[](https://stackoverflow.com/posts/64804066/timeline)

I have been in a similar situation recently- I have solved it by adding {"sudo": true} in launch.json file under .vscode directory.

just added the following line in .vscode>launch.json

```bash
{
    "version": "0.2.0",
    "configurations": [
        {
            "other..." : "configs...",
            "request": "launch",
            "console": "integratedTerminal",
            "args": [
                "${file}"
            ],
            "sudo": true
        }
    ]
}
```

VS code version I'm using is -

> Version: 1.49.1 OS: Ubuntu 16.04 LTS, 64-bit

This appears to not work on all languages. For me it worked for python 3.x Other users reported it doesn't work for C/C++.

[Share](https://stackoverflow.com/a/64804066 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/64804066/edit)

Follow

[edited Dec 19, 2021 at 5:55](https://stackoverflow.com/posts/64804066/revisions "show all edits to this post")

[![bolov's user avatar](https://www.gravatar.com/avatar/442d249754d1725d9510cf06fa46b964?s=64&d=identicon&r=PG&f=1)](https://stackoverflow.com/users/2805305/bolov)[bolov](https://stackoverflow.com/users/2805305/bolov)

71.2k1515 gold badges142142 silver badges217217 bronze badges

answered Nov 12, 2020 at 12:36

[![Chetan's user avatar](https://graph.facebook.com/1701234329/picture?type=large)](https://stackoverflow.com/users/5290876/chetan)[Chetan](https://stackoverflow.com/users/5290876/chetan)

61466 silver badges77 bronze badges

-   4
    
    This not work when debuging C/C++ code, maybe for other languages? (OP didn't specify language) 
    
    – [Payne](https://stackoverflow.com/users/3172286/payne "456 reputation")
    
     [Dec 20, 2020 at 16:36](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment115591776_64804066)
    
-   2
    
    Sorry for not mentioning the language. It was Python 3.x 
    
    – [Chetan](https://stackoverflow.com/users/5290876/chetan "614 reputation")
    
     [Dec 21, 2020 at 16:23](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment115617106_64804066)
    
-   This worked for me on Python3.6 and using a remote machine. 
    
    – [Alan Abraham](https://stackoverflow.com/users/14291629/alan-abraham "66 reputation")
    
     [Jul 20, 2021 at 22:00](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment120993483_64804066)
    

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

26

[](https://stackoverflow.com/posts/58206689/timeline)

I did the following:

1.  create a script called "gdb" in e.g. my home directory, containing: `pkexec /usr/bin/gdb "$@"`
2.  make it executable
3.  modify the launch.json in VSCode to call the script (obviously change _username_ accordingly) by adding "miDebuggerPath":

```erlang
...
            "externalConsole": false,
            "miDebuggerPath": "/home/<username>/gdb",
            "MIMode": "gdb",
...
```

4.  whilst debugging, use `top` or such like to verify the process is running as root.

That should be enough.

[Share](https://stackoverflow.com/a/58206689 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/58206689/edit)

Follow

answered Oct 2, 2019 at 17:45

[![Den-Jason's user avatar](https://www.gravatar.com/avatar/c7eac48b60589f9a7e483887c07c26c2?s=64&d=identicon&r=PG)](https://stackoverflow.com/users/1607937/den-jason)[Den-Jason](https://stackoverflow.com/users/1607937/den-jason)

2,18911 gold badge2020 silver badges1515 bronze badges

-   4
    
    Super helpful. If only I knew how to make pkexec remember my password! Another note that is VSCode does not end the debug session if you cancel the prompt. 
    
    – [Ashley Duncan](https://stackoverflow.com/users/7050363/ashley-duncan "786 reputation")
    
     [Apr 6, 2020 at 0:39](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment108007581_58206689)
    
-   1
    
    Does not work with passwordless key-based authentication. 
    
    – [Alex Jansen](https://stackoverflow.com/users/986696/alex-jansen "1,455 reputation")
    
     [Nov 9, 2021 at 8:30](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment123552391_58206689)
    

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

16

[](https://stackoverflow.com/posts/63738783/timeline)

My solution:

add `/usr/bin/gdb` to `/etc/sudoers` like [here](https://linuxhandbook.com/sudo-without-password/)

add a executable file whose content is

```bash
sudo /usr/bin/gdb "$@"
```

set `miDebuggerPath` to the file

[Share](https://stackoverflow.com/a/63738783 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/63738783/edit)

Follow

answered Sep 4, 2020 at 9:49

[![Yong-Hao Zou's user avatar](https://www.gravatar.com/avatar/96747654f5e0367cc132c0dbe929dec7?s=64&d=identicon&r=PG&f=1)](https://stackoverflow.com/users/4439991/yong-hao-zou)[Yong-Hao Zou](https://stackoverflow.com/users/4439991/yong-hao-zou)

17111 silver badge44 bronze badges

-   user_name ALL=(ALL) NOPASSWD:/usr/bin/gdb. That worked. Yes, this sucks in that we added sudo for gdb. Security hole and all but I am working on a total local scenario with a Raspberry Pi which is an IoT device never going to production, so works for me. 
    
    – [Sanjeev Dwivedi](https://stackoverflow.com/users/2300591/sanjeev-dwivedi "91 reputation")
    
     [Jan 2, 2021 at 22:05](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment115882288_63738783) 
    
-   This solution worked for me when working on a remote Ubuntu target. Just note that it is better to add _ALL ALL=(ALL) NOPASSWD:/usr/bin/gdb_ in a separate file in /etc/sudoers.d folder than changing the /etc/sudoers file 
    
    – [Ivar Simensen](https://stackoverflow.com/users/12180887/ivar-simensen "183 reputation")
    
     [Jun 1, 2021 at 7:04](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment119811283_63738783)
    
-   it works for my C++ project when working on a remote ubuntu server. 
    
    – [stickers](https://stackoverflow.com/users/14525356/stickers "83 reputation")
    
     [Jun 8, 2022 at 21:49](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment128163325_63738783)
    

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

9

[](https://stackoverflow.com/posts/42667590/timeline)

Do not know the way to make vscode to run sudo gdb. But you can sudo to run vscode so natually you can sudo gdb for debug.

> sudo code . --user-data-dir='.'

[Share](https://stackoverflow.com/a/42667590 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/42667590/edit)

Follow

answered Mar 8, 2017 at 9:33

[![user7469511's user avatar](https://www.gravatar.com/avatar/0d1ce0857b50f93fcd95775f9e4d8b6d?s=64&d=identicon&r=PG&f=1)](https://stackoverflow.com/users/7469511/user7469511)[user7469511](https://stackoverflow.com/users/7469511/user7469511)

10111 silver badge44 bronze badges

-   5
    
    I am using visual studio code remote to edit code on a remote ubuntu server. Any idea How can I do the same? 
    
    – [rbansal](https://stackoverflow.com/users/2737008/rbansal "1,114 reputation")
    
     [Apr 7, 2021 at 5:28](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment118396012_42667590)
    
-   1
    
    This caused VSCode to freakout. 
    
    – [ATL_DEV](https://stackoverflow.com/users/148298/atl-dev "9,183 reputation")
    
     [Jan 18, 2022 at 22:54](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517#comment125098736_42667590)
    

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

1

[](https://stackoverflow.com/posts/70409254/timeline)

I did the following:

1.  try what [Jason](https://stackoverflow.com/a/58206689/10776571) did
2.  tired with endless annoying dialogue box for password
3.  write a python script to resolve that

**launch.json**:

```erlang
...
"externalConsole": false,
"miDebuggerPath": "${workspaceFolder}/scripts/gdb_root.py",
"MIMode": "gdb",
...
```

**gdb_root.py**:

```python
#!/usr/bin/python3
import os
import sys
import stat
import time
import json
import hashlib
from pathlib import Path

GRANT_EXPIRE_SEC = 2 * 60 * 60
TARGET_EXECUTABLE = '/usr/bin/gdb'
TMP_BIN_DIR = '/tmp/exec_as_root'


class ExecFailed(Exception):
    pass


def instance_hash(argv):
    command = ' '.join(argv)
    return hashlib.md5(command.encode()).hexdigest()


def write_c_code(path, argv):
    CODE_TEMPLATE = '''
    #include <time.h>
    #include <stdlib.h>
    #include <unistd.h>

    int main(int argc, char *argv[])
    {
        int test_mode = (argc > 1);
        if (time(NULL) > (time_t)EXPIRE) {
            char path[1024] = { 0 };
            readlink("/proc/self/exe", path, sizeof(path));
            unlink(path);
            return !test_mode;
        }

        if (!test_mode)
            execl(PARAMS, NULL);

        return 0;
    }'''

    expire = time.time() + GRANT_EXPIRE_SEC
    params = [argv[0]] + argv
    params = json.dumps(params)[1:-1]
    content = CODE_TEMPLATE \
        .replace('EXPIRE', str(expire)) \
        .replace('PARAMS', params)

    with open(path, 'w') as f:
        f.write(content)


def system(cmd):
    is_ok = os.system(cmd) == 0
    if not is_ok:
        cmd_str = json.dumps(cmd)
        summary = 'failed to execute:'
        os.system(f'notify-send -- "{summary}" {cmd_str}')
        raise ExecFailed()


def build_instance(instance, argv):
    c_file = instance.with_suffix('.c')
    write_c_code(c_file, argv)
    compile_cmd = f'gcc -o {instance} {c_file}'
    system(compile_cmd)


def did_grant_root(file):
    fs = os.stat(file)
    root_own = (fs.st_uid == 0)
    uid_set = fs.st_mode & stat.S_ISUID
    return root_own and uid_set


def grant_root(file):
    setuid_cmd = f'chown root:root {file}; chmod 4111 {file}'
    pkexec_cmd = f'pkexec bash -c "{setuid_cmd}"'
    system(pkexec_cmd)


def main():
    bin_dir = Path(TMP_BIN_DIR)
    bin_dir.mkdir(exist_ok=True)

    argv = [TARGET_EXECUTABLE] + sys.argv[1:]
    instance = bin_dir / instance_hash(argv)

    if instance.exists():
        system(f'{instance} test')

    if not instance.exists():
        build_instance(instance, argv)

    if not did_grant_root(instance):
        grant_root(instance)

    os.execv(instance, [instance.name])


if __name__ == '__main__':
    try:
        main()
    except ExecFailed:
        exit(1)
```

---

This is a portable solution, no effect to system, and do not forget: `chmod +x scripts/gdb_root.py`

[Share](https://stackoverflow.com/a/70409254 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/70409254/edit)

Follow

[edited Dec 19, 2021 at 6:05](https://stackoverflow.com/posts/70409254/revisions "show all edits to this post")

answered Dec 19, 2021 at 5:50

[![Pamela's user avatar](https://i.stack.imgur.com/aH31e.jpg?s=64&g=1)](https://stackoverflow.com/users/10776571/pamela)[Pamela](https://stackoverflow.com/users/10776571/pamela)

49944 silver badges77 bronze badges

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

1

[](https://stackoverflow.com/posts/72743105/timeline)

This worked for java, but maybe for other languages too:

When first running a debug session, VSCode creates a terminal called e.g. `Debug: Main`. Just open the terminal, press Ctrl+C, type`sudo su`, enter your password and next time the debug session will be launched from this terminal with root privileges

[Share](https://stackoverflow.com/a/72743105 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/72743105/edit)

Follow

answered Jun 24, 2022 at 11:02

[![Matej Vargovčík's user avatar](https://i.stack.imgur.com/P9ZOY.jpg?s=64&g=1)](https://stackoverflow.com/users/4003774/matej-vargov%c4%8d%c3%adk)[Matej Vargovčík](https://stackoverflow.com/users/4003774/matej-vargov%c4%8d%c3%adk)

72577 silver badges1414 bronze badges

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

0

[](https://stackoverflow.com/posts/70165972/timeline)

As from the previous answers I had to use the method with a executable file in the home directory like: /home/youruser/gdbasroot

But I had to use the content:

> sudo /usr/bin/gdb "$@"

instead of gdbk because I couldn't get gdbk to work without prompting for a password (which didnt work as its called by vscode remote debuger). I did it according this post (the upvoted and accepted answer):

[https://askubuntu.com/questions/542397/change-default-user-for-authentication](https://askubuntu.com/questions/542397/change-default-user-for-authentication)

When using sudo you can issue a sudo command in the vscode terminal and from then on you can use the "sudo debugger" without the password. Otherwise vscode gets prompted and can't handle it.

Greetings

[Share](https://stackoverflow.com/a/70165972 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/70165972/edit)

Follow

answered Nov 30, 2021 at 7:55

[![Alex's user avatar](https://www.gravatar.com/avatar/77d0b58875ecd4a5b02668bac2519c5b?s=64&d=identicon&r=PG&f=1)](https://stackoverflow.com/users/11534535/alex)[Alex](https://stackoverflow.com/users/11534535/alex)

1755 bronze badges

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

0

[](https://stackoverflow.com/posts/75823517/timeline)

Very easy way to solve this for golang is to:

```shell
$ sudo chown root:$(id -g) ~/go/bin/dlv
$ sudo chmod u+s ~/go/bin/dlv
```

This way dlv will be executed as root.

> It is pretty much the same for other languages. ALlow the debugger to run as root instead of relying in other config files or wrappers.

[Share](https://stackoverflow.com/a/75823517 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/75823517/edit)

Follow

[edited Mar 23 at 13:18](https://stackoverflow.com/posts/75823517/revisions "show all edits to this post")

answered Mar 23 at 13:17

[![rafaeldtinoco's user avatar](https://lh3.googleusercontent.com/-BoixVDKYAVk/AAAAAAAAAAI/AAAAAAAAAaQ/wmzIWrg0z7c/photo.jpg?sz=64)](https://stackoverflow.com/users/8530731/rafaeldtinoco)[rafaeldtinoco](https://stackoverflow.com/users/8530731/rafaeldtinoco)

133 bronze badges

[Add a comment](https://stackoverflow.com/questions/40033311/how-to-debug-programs-with-sudo-in-vscode/75823517# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---
