---
title: "setns(2): reassociate thread with namespace - Linux man page"
layout: post
---
# setns(2): reassociate thread with namespace - Linux man page
<a style="text-decoration:underline" href="https://linux.die.net/man/2/setns">setns(2): reassociate thread with namespace - Linux man page</a><br>
setns(2) - Linux man page
-------------------------

Name
----

setns - reassociate thread with a namespace

Synopsis
--------

```c
#define _GNU_SOURCE             /* See [feature_test_macros](https://linux.die.net/man/7/feature_test_macros)(7) */
#include <[sched.h](https://linux.die.net/include/sched.h)\>

int setns(int _fd_, int _nstype_);

```

Description
-----------

Given a file descriptor referring to a namespace, reassociate the calling thread with that namespace.

The _fd_ argument is a file descriptor referring to one of the namespace entries in a _/proc/[pid]/ns/_ directory; see **[proc](https://linux.die.net/man/5/proc)**(5) for further information on _/proc/[pid]/ns/_. The calling thread will be reassociated with the corresponding namespace, subject to any constraints imposed by the _nstype_ argument.

The _nstype_ argument specifies which type of namespace the calling thread may be reassociated with. This argument can have one of the following values:

- **0**

  Allow any type of namespace to be joined. **CLONE_NEWIPC**

  _fd_ must refer to an IPC namespace.

- **CLONE_NEWNET**

  _fd_ must refer to a network namespace.

- **CLONE_NEWUTS**

  _fd_ must refer to a UTS namespace.

  Specifying _nstype_ as 0 suffices if the caller knows (or does not care) what type of namespace is referred to by _fd_. Specifying a nonzero value for _nstype_ is useful if the caller does not know what type of namespace is referred to by _fd_ and wants to ensure that the namespace is of a particular type. (The caller might not know the type of the namespace referred to by _fd_ if the file descriptor was opened by another process and, for example, passed to the caller via a UNIX domain socket.)

Return Value
------------

On success, _setns_() returns 0. On failure, -1 is returned and _errno_ is set to indicate the error.

Errors
------
- **EBADF**

  _fd_ is not a valid file descriptor.

- **EINVAL**

  _fd_ refers to a namespace whose type does not match that specified in _nstype_, or there is problem with reassociating the the thread with the specified namespace.

- **ENOMEM**

  Cannot allocate sufficient memory to change the specified namespace.

- **EPERM**

  The calling thread did not have the required privilege (**CAP_SYS_ADMIN**) for this operation.

Versions
--------

The **setns**() system call first appeared in Linux in kernel 3.0; library support was added to glibc in version 2.14.

Conforming To
-------------

The **setns**() system call is Linux-specific.

Notes
-----

Not all of the attributes that can be shared when a new thread is created using **[clone](https://linux.die.net/man/2/clone)**(2) can be changed using **setns**().

Example
-------

The program below takes two or more arguments. The first argument specifies the pathname of a namespace file in an existing _/proc/[pid]/ns/_ directory. The remaining arguments specify a command and its arguments. The program opens the namespace file, joins that namespace using **setns**(), and executes the specified command inside that namespace.

The following shell session demonstrates the use of this program (compiled as a binary named _ns_exec_) in conjunction with the **CLONE_NEWUTS** example program in the **[clone](https://linux.die.net/man/2/clone)**(2) man page (complied as a binary named _newuts_).

We begin by executing the example program in **[clone](https://linux.die.net/man/2/clone)**(2) in the background. That program creates a child in a separate UTS namespace. The child changes the hostname in its namespace, and then both processes display the hostnames in their UTS namespaces, so that we can see that they are different.

```sh
    $ su                   # Need privilege for namespace operations
    Password:
    # ./newuts bizarro &
    [1] 3549
    clone() returned 3550
    uts.nodename in child:  bizarro
    uts.nodename in parent: antero
    # uname -n             # Verify hostname in the shell
    antero
```

We then run the program shown below, using it to execute a shell. Inside that shell, we verify that the hostname is the one set by the child created by the first program:

```sh
    # ./ns_exec /proc/3550/ns/uts /bin/bash
    # uname -n             # Executed in shell started by ns_exec
    bizarro
```

**Program source**

```c
#define _GNU_SOURCE
#include <[fcntl.h](https://linux.die.net/include/fcntl.h)\>
#include <[sched.h](https://linux.die.net/include/sched.h)\>
#include <[unistd.h](https://linux.die.net/include/unistd.h)\>
#include <[stdlib.h](https://linux.die.net/include/stdlib.h)\>
#include <[stdio.h](https://linux.die.net/include/stdio.h)\>

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE); \\
                        } while (0)

int
main(int argc, char *argv[])
{
    int fd;

   if (argc < 3) {
        fprintf(stderr, "%s /proc/PID/ns/FILE cmd args...\\n", argv[0]);
        exit(EXIT_FAILURE);
    }

   fd = open(argv[1], O_RDONLY);   /* Get descriptor for namespace */
    if (fd == -1)
        errExit("open");

   if (setns(fd, 0) == -1)         /* Join that namespace */
        errExit("setns");

   execvp(argv[2], &argv[2]);      /* Execute a command in namespace */
    errExit("execvp");
}
```

See Also
--------

**[clone](https://linux.die.net/man/2/clone)**(2)[[clone(2) - Linux manual page]],
**[fork](https://linux.die.net/man/2/fork)**(2), **[vfork](https://linux.die.net/man/2/vfork)**(2), **[proc](https://linux.die.net/man/5/proc)**(5), **[unix](https://linux.die.net/man/7/unix)**(7)

Referenced By
-------------

**[capabilities](https://linux.die.net/man/7/capabilities)**(7), **[unshare](https://linux.die.net/man/2/unshare)**(2)
