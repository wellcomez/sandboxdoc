---
layout: post
tags:
- unshare
title: "unshare(2) - Linux manual page"

---

# unshare(2) - Linux man page

## Name

unshare - disassociate parts of the process execution context

## Synopsis
~~~c
#define _GNU_SOURCE             
#include <sched.h>
int unshare(int _flags_);
~~~

## Description

**unshare**() allows a process to disassociate parts of its execution context that are currently being shared with other processes. Part of the execution context, such as the mount namespace, is shared implicitly when a new process is created using **[fork](https://linux.die.net/man/2/fork)**(2) or **[vfork](https://linux.die.net/man/2/vfork)**(2), while other parts, such as virtual memory, may be shared by explicit request when creating a process using **[clone](https://linux.die.net/man/2/clone)**(2)[[clone(2) - Linux manual page]].

The main use of **unshare**() is to allow a process to control its shared execution context without creating a new process.

The _flags_ argument is a bit mask that specifies which parts of the execution context should be unshared. This argument is specified by ORing together zero or more of the following constants:

- **CLONE_FILES**
  Reverse the effect of the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_FILES** flag. Unshare the file descriptor table, so that the calling process no longer shares its file descriptors with any other process.

- **CLONE_FS**
  Reverse the effect of the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_FS** flag. Unshare file system attributes, so that the calling process no longer shares its root directory (**[chroot](https://linux.die.net/man/2/chroot)**(2)), current directory (**[chdir](https://linux.die.net/man/2/chdir)**(2)), or umask (**[umask](https://linux.die.net/man/2/umask)**(2)) attributes with any other process.

- **CLONE_NEWIPC** (since Linux 2.6.19)
  This flag has the same effect as the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_NEWIPC** flag. Unshare the System V IPC namespace, so that the calling process has a private copy of the System V IPC namespace which is not shared with any other process. Specifying this flag automatically implies **CLONE_SYSVSEM** as well. Use of **CLONE_NEWIPC** requires the **CAP_SYS_ADMIN** capability.

- **CLONE_NEWNET** (since Linux 2.6.24)
  This flag has the same effect as the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_NEWNET** flag. Unshare the network namespace, so that the calling process is moved into a new network namespace which is not shared with any previously existing process. Use of **CLONE_NEWNET** requires the **CAP_SYS_ADMIN** capability.

- **CLONE_NEWNS**
  This flag has the same effect as the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_NEWNS** flag. Unshare the mount namespace, so that the calling process has a private copy of its namespace which is not shared with any other process. Specifying this flag automatically implies **CLONE_FS** as well. Use of **CLONE_NEWNS** requires the **CAP_SYS_ADMIN** capability.

- **CLONE_NEWUTS** (since Linux 2.6.19)
  This flag has the same effect as the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_NEWUTS** flag. Unshare the UTS IPC namespace, so that the calling process has a private copy of the UTS namespace which is not shared with any other process. Use of **CLONE_NEWUTS** requires the **CAP_SYS_ADMIN** capability.

- **CLONE_SYSVSEM** (since Linux 2.6.26)
  This flag reverses the effect of the **[clone](https://linux.die.net/man/2/clone)**(2) **CLONE_SYSVSEM** flag. Unshare System V semaphore undo values, so that the calling process has a private copy which is not shared with any other process. Use of **CLONE_SYSVSEM** requires the **CAP_SYS_ADMIN** capability.

  If _flags_ is specified as zero, then **unshare**() is a no-op; no changes are made to the calling process's execution context.

## Return Value

On success, zero returned. On failure, -1 is returned and _errno_ is set to indicate the error.

## Errors

- **EINVAL**
  An invalid bit was specified in _flags_.
- **ENOMEM**
  Cannot allocate sufficient memory to copy parts of caller's context that need to be unshared.
- **EPERM**
  The calling process did not have the required privileges for this operation.

## Versions

The **unshare**() system call was added to Linux in kernel 2.6.16.

## Conforming To

The **unshare**() system call is Linux-specific.

## Notes

Not all of the process attributes that can be shared when a new process is created using **[clone](https://linux.die.net/man/2/clone)**(2) can be unshared using **unshare**(). In particular, as at kernel 3.8, **unshare**() does not implement flags that reverse the effects of **CLONE_SIGHAND**, **CLONE_THREAD**, or **CLONE_VM**. Such functionality may be added in the future, if required.

## See Also

**[clone](https://linux.die.net/man/2/clone)**(2), **[fork](https://linux.die.net/man/2/fork)**(2), **[kcmp](https://linux.die.net/man/2/kcmp)**(2), **[setns](https://linux.die.net/man/2/setns)**(2), **[vfork](https://linux.die.net/man/2/vfork)**(2)

_Documentation/unshare.txt_ in the Linux kernel source tree

## Referenced By

**[capabilities](https://linux.die.net/man/7/capabilities)**(7), **[guestmount](https://linux.die.net/man/1/guestmount)**(1), **[mount](https://linux.die.net/man/2/mount)**(2), **[unshare](https://linux.die.net/man/1/unshare)**(1)
