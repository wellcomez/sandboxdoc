---
date: 2023-03-31 17:39
title: LSM模块动态加载
tags:
- lsm
---



# LSM模块动态加载

https://zhuanlan.zhihu.com/p/423247670

LSM模块的加载在不同内核版本的Linux中方式有所不同

1.  在4.2之前的版本中,hook方式是通过一个全局的security_operations来进行函数点的注册具体hook方式可以看该[文章1](https://link.zhihu.com/?target=https%3A//www.cnblogs.com/LittleHann/p/4134939.html)
2.  在4.2后的hook方式巨大不同,采用类似链表的形式,可以一次性挂载多个hook点,介绍可以看该[文章2](https://link.zhihu.com/?target=https%3A//blog.csdn.net/weixin_45574485/article/details/108132539)

由于在多版本适配中发现4.2后添加hook的方式具有极大改变,且只能通过编译内核的方式进行hook,但业务需求必须要在客户机上动态载入因此在这方面寻找了很多资料(网上基本没有多少东西),请教了大佬,终于成功.不得不说学习内核真的是很苦恼的事情,资源少版本多,不同版本简直天差地别,能请教的人也不多

## **4.2版本hook**

## **基本框架**

从文章2可以大致了解基本的LSM的hook方式,但是实际操作过程中会发现很多函数没法用(内核符号未导出),例如:security_add_hook,security_hook_heads等.

解决上述问题的办法有两种

1.  使用内核地址来进行函数和结构体的调用 _**sudo cat /proc/kallsyms | grep security_hook_heads**_
2.  复写函数,例如security_add_hooks,把源码的实现手动复写一遍

  

在这个过程中如果编译遇到 **unkown symbol** 即需要通过内核符号来导出.我只导出了必要的几个(security_hook_heads,lsm_names,init_mm).

## 只读问题

在导出符号后,编译通过加载,发现并不能直接加载成功,查找源码可以看到很多符号是只读的,没法直接调用.在这个问题上我思考了好久,查了很多资料都没啥收获,经同事指点了解到修改页表的方式来修改只读问题.

重点介绍几个实用函数

```cpp
int follow_pte_pmd(struct mm_struct *mm, unsigned long address,unsigned long *start,
 unsigned long *end,pte_t **ptepp, pmd_t **pmdpp, spinlock_t **ptlp)
```

这个函数跟踪pte和pmd,通过传入地址可以获取相应的pmd(获取pte为NULL,但是pmd正常还没深入研究该问题)该函数实际需要参数为mm和address其他都是自定义的空参数

```cpp
 pte_t pmd_mkwrite(pte_t pmd)
```

将页表改为可写.

```cpp
set_pmd_at(mm, addr, pmdp, pmd)
```

设置pmdp为pmd.

  

## **实例代码**

插入模块需传入相对应的内核符号地址,动态加载的重点就是如何让只读的hook点变为可写,然后注入自己的函数.

  

内核学习道阻且艰啊

```cpp
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/lsm_hooks.h>
#include <linux/security.h>
#include <linux/binfmts.h>
#include <linux/fs.h>
#include <linux/moduleparam.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/err.h>
#include <linux/elf.h>
#include <linux/string.h>


/* extern void security_add_hooks(struct security_hook_list *hooks, int count, char *lsm); */
extern int my_netlink_init(void);
extern void my_netlink_exit(void);
extern int send_user_msg(char *pbuf, uint16_t len);
extern int denied_all_update;
static unsigned long security_hook_heads_addr = 0;
static unsigned long lsm_names_addr = 0;
static unsigned long init_mm_addr = 0;
/* static unsigned long security_add_hooks_addr = 0; */
module_param(security_hook_heads_addr, ulong, 0644);
module_param(lsm_names_addr, ulong, 0644);
module_param(init_mm_addr, ulong, 0644);
/* module_param(security_add_hooks_addr, ulong, 0644); */
void (*pfun)(struct security_hook_list *hooks, int count, char *lsm);
/* extern struct security_hook_heads security_hook_heads; */
struct security_hook_heads *my_hook_head; 
/* #define MY_HOOK_INIT(HEAD, HOOK){ \ */
/* 	.head = &my_hook_head.HEAD, .hook = {.HEAD = HOOK}} */

void make_memory_rw(unsigned long address){
	pte_t *ptep = NULL;
	pmd_t *pmdp= NULL, npmd;
	spinlock_t *ptl;
	unsigned long start, end;
	int flag = 0;
	flag = follow_pte_pmd(init_mm_addr, address, &start, &end, &ptep, &pmdp, &ptl);
	printk("******%lx*******%lx*****%d***%lx\n", init_mm_addr, pmdp, flag, ptep);
	npmd = pmd_mkwrite(*pmdp);
	set_pmd_at(init_mm_addr, address, pmdp, npmd);
	printk("***********%d*************\n", pmd_write(npmd));
	if(pmdp){
		spin_unlock(ptl);
	}else{
		pte_unmap_unlock(ptep, ptl);
	}

}

pid_t g_user_pid = 0;

static int check_user_pid(void)
{
	if (current->tgid == g_user_pid)
		return 1;
	else
		return 0;
}

static int my_file_open(struct file *file, const struct cred *cred)
{
	printk("call %s\n", __func__)
}
static bool my_match_last_lsm(const char *list, const char *lsm){
	const char *last;
	if(WARN_ON(!list || !lsm))
		return false;
	last = strrchr(list, ',');
	if(last)
		last++;
	else
		last = list;
	return !strcmp(last, lsm);
}

struct security_hook_list hooks[2];

void my_init_security_hook_list(void){
	union security_list_options my_hook;
	hooks[0].head = &my_hook_head->file_open;
	my_hook.file_open = my_file_open;
	hooks[0].hook = my_hook;
}

static int my_lsm_append(char *new, char **result){
	char *cp;
	if(*result == NULL){
		*result = kstrdup(new, GFP_KERNEL);
	}else{
		if(my_match_last_lsm(*result, new))
			return 0;
		cp = kasprintf(GFP_KERNEL, "%s,%s", *result, new);
		if(cp == NULL)
			return -ENOMEM;
		kfree(*result);
		*result = cp;
	}
	return 0;
}
static void my_add_hooks(struct security_hook_list *hooks, int count, char *lsm){
	int i;
	for(i = 0; i < count; i++){
		hooks[i].lsm = lsm;
		list_add_tail_rcu(&hooks[i].list, hooks[i].head);
	}
	/* if(my_lsm_append(lsm, (char **)lsm_names_addr) < 0){ */
	/* 	panic("%s - Cannot get early memory.\n", __func__); */
	/* } */
}


static int __init my_init(void)
{

	/* pfun security_add_hooks = 0xffffffff9daee13c; */
	/* printk("************my security start**********\n"); */
	/* printk("************0x%lx**************\n", init_mm_addr); */
	/* printk("0x%lx 0x%lx\n", security_hook_heads_addr, lsm_names_addr); */
	make_memory_rw(security_hook_heads_addr);
	make_memory_rw(lsm_names_addr);
	/* printk("**************memory is rw*****************\n"); */
	my_hook_head = (struct security_hook_heads*)security_hook_heads_addr;
	my_init_security_hook_list();
	/* pfun=(void(*)(struct security_hook_list *hooks, int count, char *lsm))security_add_hooks_addr; */
	/* pfun(hooks, 2, "mylsm"); */
	my_add_hooks(hooks, 1,"mylsm");	
	return 0;
}

static void __exit my_exit(void)
{
	printk("***************my security exit*************\n");
}

MODULE_LICENSE("GPL");
module_init(my_init);
module_exit(my_exit);
```

编辑于 2022-03-14 15:14

