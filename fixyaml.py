#!/usr/bin/env python3
import sys
import os
import re

def fix(path):
    fp = open(path)
    aaa = fp.read()
    fp.close()
    print(os.path.basename(path))
    if os.path.basename(path).lower() in set(["readme.md", "_sidebar.md", "index.md", "url.md", "template.md"]):
        os.system("rm -fr \"%s\"" % (path))
        print("delete ", path)
        return
    ext = path.split('.')[-1]
    if ext.upper() != "MD":
        return
    title = os.path.basename(path)
    aa = title.split('.')[:-1]
    format = None
    title = ".".join(aa)
    title = title.replace("\\", "")
    title = title.replace("\"", "")
    try:
        b = None
        for i in range(len(aaa)):
            try:
                if aaa[i:i+3] == '---':
                    b = i
                    break
                a = aaa[i]
                if a not in ['\n', "\s"]:
                    break
            except:
                break
        if b is None:
            raise Exception("not b")
        e = aaa[b+3:].index("---")+b+3
        document = aaa[b+3:e]
        try:
            import yaml
            data = yaml.load(document, yaml.FullLoader)
            keys = data.keys()
            t = title 
            try:
                t = data["title"]
            except:
                pass

            data["title"] = t 
            data["title"] = data['title'].replace("\"", "")
            data["layout"] = "post"
            format = yaml.dump(data, encoding="utf-8")
            format = "\n".join(
                ["---", format.decode("utf-8"), "---", aaa[e+3:]])
            # try:
            #     from yaml import CLoader as Loader
            #     data = yaml.load(document, Loader)
            # except ImportError:
            #     from yaml import Loader
            #     data = yaml.load(document, Loader)
        except Exception as e:
            raise Exception("yarl error %s" % (e))
            pass
    except Exception as e:
        format = "\n".join(["---", "title: \"%s\"" % (title),
                            "layout: post",
                            "---", aaa, ])
    if len(format)!=len(aaa):
        print(len(format),len(aaa))
    sss = open(path, 'w', encoding='utf8').write(format)
    print(sss, path)
    ff = os.path.basename(path)
    d = os.path.dirname(path)
    ff = ff.replace("\\", " ")
    ff = ff.replace("%5C", " ")
    ff = ff.replace("#", "NO.")
    import shutil
    shutil.move(path, os.path.join(d, ff))
    pass


def walk2(a):
    for root, dirs, files in os.walk(a, topdown=False):
        for name in files:
            try:
                fix(os.path.join(root, name))
            except Exception as e:
                pass
        for name in dirs:
            try:
                dirpath = os.path.join(root, name)
                create_index(dirpath, a)
            except Exception as e:
                pass

# def walk(a):
#     for root, dirs, files in os.walk(a):
#         if len(dirs) == 0:
#             for f in files:
#                 path = os.path.join(root, f)
#                 try:
#                     fix(path)
#                 except Exception as e:
#                     pass
#         else:
#             new_func(root, dirs)
#     new_func(a, None)


# fix('/Users/jialaizhu/Documents/sandboxdoc/selinux/16.5 SELinux 初探.md')
def escapePath(path):
    path = path.replace('\\', "%5C")
    path = path.replace(" ", "%20")
    path = path.replace(":", "%3A")
    path = path.replace(",", "%2C")
    path = path.replace('"', "%22")
    path = path.replace('#', "%23")
    return path


def create_index(dir, root):
    outlineformat = '''< a href="{{< relref "%s" >}}">%s</a>'''
    yamlhead = '''---
#    title: "%s"
---
目录
''' % (os.path.basename(dir))
#    type: "tags"
#    layout: "tags"
    nn = dir.replace(root, "/docs")
    files = yamlhead.split('\n')
    for a in os.listdir(dir):
        file = os.path.join(dir, a).replace(root, "/docs")
        file = file.replace("\"", "\\\"")
        file = file.replace("?", '%3F')
        title = ".".join(a.split(".")[:-1])
        if os.path.isdir(os.path.join(dir, a)):
            a = outlineformat % (file+"/", title)
            files.append(a)
        else:
            ext = a.split('.')[-1]
            if ext != "md":
                continue
            title = ".".join(a.split(".")[:-1])
            a = outlineformat % (file, title)
            files.append(a)
    data = "\n".join(files)
    index = os.path.join(dir, "_index.md")
    # rc = open(index, "w").write(data)
    rc = open(index, "w").write(yamlhead)
    print(rc)


if __name__ == "__main__":
    # fix("/Users/jialaizhu/Downloads/tesxxxxx/POSIX message queues in Linux - SoftPrayog.md")
    try:
        dst = sys.argv[1]
    except:
        dst = "/Users/jialaizhu/Downloads/hugobook2/content/docs"

    os.system("rm -rf %s" % (dst))
    os.system("mkdir -p %s" % (dst))
    os.system("cp -rf ~/Documents/sandboxdoc/* %s" % (dst))
    walk2(dst)
