---
date: 2023-04-13 14:30
title: How to highlight and color gdb output during interactive debugging
tags:
- 
---



# How to highlight and color gdb output during interactive debugging?

Asked 14 years, 5 months ago



Viewed 74k times

#  197[](https://stackoverflow.com/posts/209534/timeline)

Please don't reply I should use ddd, nemiver, emacs, vim, or any other front-end, I just prefer gdb as it is, but would like to see its output with some terminal colors.

-   [gdb](https://stackoverflow.com/questions/tagged/gdb "show questions tagged 'gdb'")
-   [terminal](https://stackoverflow.com/questions/tagged/terminal "show questions tagged 'terminal'")
-   [colors](https://stackoverflow.com/questions/tagged/colors "show questions tagged 'colors'")

[Share](https://stackoverflow.com/q/209534 "Short permalink to this question")

[Improve this question](https://stackoverflow.com/posts/209534/edit)

Follow

[edited May 8, 2015 at 10:49](https://stackoverflow.com/posts/209534/revisions "show all edits to this post")

)]Ciro Santilli OurBigBook.com](https://stackoverflow.com/users/895245/ciro-santilli-ourbigbook-com)

335k9797 gold badges11671167 silver badges957957 bronze badges

asked Oct 16, 2008 at 17:33

)]elmarco](https://stackoverflow.com/users/1277510/elmarco)

31.2k2121 gold badges6363 silver badges6868 bronze badges

-   4
    
    It won't give you colors (so I won't call this an Answer), but some ~/.gdbinit configuration will improve the experience. I use this as a bare minimum: set history save on set print pretty set output-radix 16 set height 0 
    
    – [activout.se](https://stackoverflow.com/users/20444/activout-se "6,050 reputation")
    
     [Nov 19, 2008 at 19:35](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment147889_209534)
    
-   Once highlight current line on `l` gets implemented [sourceware.org/bugzilla/show_bug.cgi?id=21044](https://sourceware.org/bugzilla/show_bug.cgi?id=21044) , I'll just add `l` to a `hook-stop` and enter Dev Nirvana. 
    
    – [Ciro Santilli OurBigBook.com](https://stackoverflow.com/users/895245/ciro-santilli-ourbigbook-com "335,367 reputation")
    
     [Aug 2, 2017 at 7:38](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment77869458_209534) 
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

---

## 11 Answers

Sorted by:

#  207[](https://stackoverflow.com/posts/17341335/timeline)

# .gdbinit

You can tweak your `~/.gdbinit` to have colors. You can use mammon's `.gdbinit` which is available here:

[https://github.com/gdbinit/gdbinit](https://github.com/gdbinit/gdbinit)

You can tweak it as much as you want too. I found this thanks to [this SO answer](https://stackoverflow.com/questions/5526432/gdb-in-backtrack). Here's the kind of output that you can obtain:

![.gdbinit](https://i.stack.imgur.com/Nhuwf.png)

A GitHub repository is also available: [https://github.com/gdbinit/Gdbinit](https://github.com/gdbinit/Gdbinit)

On a side note, the same idea was also [applied to lldb](https://github.com/deroko/lldbinit).

## GDB Dashboard

Following the same concept, [GDB Dashboard](https://github.com/cyrus-and/gdb-dashboard) provides a modular visual interface for GDB in Python.

[![GDB Dashboard](https://i.stack.imgur.com/mHC8f.png)](https://i.stack.imgur.com/mHC8f.png)

## (void)walker

Another similar project uses GDB's Python support to provide more extensibility, so this is worth checking out: [https://github.com/dholm/voidwalker](https://github.com/dholm/voidwalker)

@dholm also provides his own [.gdbinit](https://github.com/dholm/dotgdb) inspired from the previous one.

![(void)walker](https://raw.githubusercontent.com/dholm/voidwalker/master/screenshot.png)

## pwndbg

Some projects provide a set of useful functions, including improved display. This is the case for [PEDA](https://github.com/longld/peda) or [pwndbg](https://github.com/zachriggle/pwndbg). The latter gives the following description:

> A PEDA replacement. In the spirit of our good friend `windbg`, `pwndbg` is pronounced `pwnd-bag`.
> 
> -   Speed
> -   Resiliency
> -   Clean code

It provides commands to support debugging and exploit development similar to the ones from PEDA, and better display (although this is not the main focus of the project). The software is still under development, and has not been properly released yet.

[![pwndbg](https://i.stack.imgur.com/BuVHv.png)](https://i.stack.imgur.com/BuVHv.png)

## voltron

The [project](https://github.com/snare/voltron) description states:

> Voltron is an extensible debugger UI for hackers. It allows you to attach utility views running in other terminals to your debugger (LLDB or GDB), displaying helpful information such as disassembly, stack contents, register values, etc, while still giving you the same debugger CLI you're used to.

You can modify your `.gdbinit` to automatically integrate it. However, the display itself is outside of GDB (e.g. in a tmux split).

[![voltron](https://i.stack.imgur.com/7eWF1.png)](https://i.stack.imgur.com/7eWF1.png)

## GEF

[GEF](https://github.com/hugsy/gef) is another option, and it is described as:

> It is aimed to be used mostly by exploiters and reverse-engineers, to provide additional features to GDB using the Python API to assist during the process of dynamic analysis and exploit development.

)]Share](https://stackoverflow.com/a/17341335 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/17341335/edit)

Follow

[edited Feb 22, 2018 at 1:57](https://stackoverflow.com/posts/17341335/revisions "show all edits to this post")

)]Jeff Trull](https://stackoverflow.com/users/192737/jeff-trull)

1,2261111 silver badges1616 bronze badges

answered Jun 27, 2013 at 10:58

)]BenC](https://stackoverflow.com/users/1043187/benc)

8,66933 gold badges5050 silver badges6868 bronze badges

-   many thanks for your answer, do you have an idea how to switch off register output? (i am using gdb for C++ code and need no assembler level right away) 
    
    – [vak](https://stackoverflow.com/users/470108/vak "1,644 reputation")
    
     [Jun 5, 2015 at 12:47](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment49395786_17341335)
    
-   1
    
    @vak did you try `set $SHOWCPUREGISTERS = 0`? Basically you've got [several parameters that you can set](https://github.com/gdbinit/Gdbinit/blob/master/gdbinit#L70-L124), and you can always modify the code to suit your needs. 
    
    – [BenC](https://stackoverflow.com/users/1043187/benc "8,669 reputation")
    
     [Jun 13, 2015 at 3:50](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment49677374_17341335)
    
-   That `~/.gdbinit` file breaks TUI mode, and it doesn't show the C or C++ code line anymore when you run `f` or `frame` :(. 
    
    – [Gabriel Staples](https://stackoverflow.com/users/4561887/gabriel-staples "32,092 reputation")
    
     [Oct 13, 2021 at 0:18](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment122929598_17341335) 
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  106[](https://stackoverflow.com/posts/249750/timeline)

It's not colours, but consider gdb's [text gui](http://davis.lbl.gov/Manuals/GDB/gdb_21.html). It makes a vast difference to how usable gdb is.

You can launch it with:

```
gdb -tui executable.out
```

Screenshot:

![enter image description here](https://i.stack.imgur.com/wHN7R.png)

As you can see, the main features are:

-   shows what line of the source we are on and surrounding lines
-   shows breakpoints

[Share](https://stackoverflow.com/a/249750 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/249750/edit)

Follow

[edited May 9, 2015 at 9:22](https://stackoverflow.com/posts/249750/revisions "show all edits to this post")

)]Ciro Santilli OurBigBook.com](https://stackoverflow.com/users/895245/ciro-santilli-ourbigbook-com)

335k9797 gold badges11671167 silver badges957957 bronze badges

answered Oct 30, 2008 at 10:21

)]John Carter](https://stackoverflow.com/users/8331/john-carter)

53.5k2626 gold badges111111 silver badges143143 bronze badges

-   6
    
    Wow! Thanks! I've been looking for a long time for a nice graphical shell around gdb, and I have tried xxgdb, kgdb and ddd, but none of them worked very good for me, so I've stuck with the plain old command line interface. But this is absolutely perfect! 
    
    – [Thomas Padron-McCarthy](https://stackoverflow.com/users/15727/thomas-padron-mccarthy "26,992 reputation")
    
     [Jan 10, 2009 at 19:47](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment255014_249750)
    
-   54
    
    Ctrl-x Ctrl-a: Enter this to switch to and from the text gui mode, works even without the command line option. 
    
    – [jturcotte](https://stackoverflow.com/users/56315/jturcotte "1,273 reputation")
    
     [Nov 5, 2010 at 14:10](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment4420886_249750)
    
-   3
    
    Printing to stdout from the program breaks the interface for me. Any workarounds besides redirecting it? 
    
    – [Ciro Santilli OurBigBook.com](https://stackoverflow.com/users/895245/ciro-santilli-ourbigbook-com "335,367 reputation")
    
     [May 9, 2015 at 9:25](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment48385523_249750)
    
-   2
    
    I get the same problem with stdout breaking the interface. Ctrl-L or whatever your redraw binding is makes it usable at least. For people with vi editing mode enabled, Ctrl-X Ctrl-A doesn't work, but the command 'layout src' will put you in TUI mode with the source shown like the image. 
    
    – [wilywampa](https://stackoverflow.com/users/752720/wilywampa "1,250 reputation")
    
     [May 28, 2015 at 3:41](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment49070711_249750) 
    
-   2
    
    Open a second terminal and then issue the command: $ tty Use the result to direct stdout from the gdb session to that terminal using the (gdb) set inferior-tty command. ex. from my .gdbinit set inferior-tty /dev/tty2 now your stdout will not mess up $gdb -tui. 
    
    – [netskink](https://stackoverflow.com/users/1008596/netskink "3,825 reputation")
    
     [Jan 18, 2016 at 0:02](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment57431286_249750) 
    

[Show **3** more comments](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Expand to show all comments on this post")

#  46[](https://stackoverflow.com/posts/522192/timeline)

I know you did not want a frontend. But how about [cgdb](http://cgdb.github.io/) it is very close to gdb, it is textmode but has a source window above with syntax highlight on the code.

![](https://i.imgur.com/T9Iepi9.png?1)

[Share](https://stackoverflow.com/a/522192 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/522192/edit)

Follow

[edited Feb 11, 2015 at 4:40](https://stackoverflow.com/posts/522192/revisions "show all edits to this post")

)]Justin M. Keyes](https://stackoverflow.com/users/152142/justin-m-keyes)

6,54333 gold badges3232 silver badges5959 bronze badges

answered Feb 6, 2009 at 21:10

)]Johan](https://stackoverflow.com/users/51425/johan)

19.8k2828 gold badges9292 silver badges110110 bronze badges

-   2
    
    sudo apt-get install cgdb 
    
    – [cs01](https://stackoverflow.com/users/2893090/cs01 "5,117 reputation")
    
     [Feb 24, 2016 at 18:29](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment58904502_522192)
    
-   Just tried running it: it doesn't see any gdb history, and it also has a screwed up prompt, where there is some whitespace between the caret and the actual place where symbol being typed _(this is likely because I have [a colored prompt in gdb](https://github.com/Hi-Angel/dotfiles/blob/eac639ee200f0203f58028a244ba157a5b4373a5/.gdbinit#L67))_. Not impressed at all. 
    
    – [Hi-Angel](https://stackoverflow.com/users/2388257/hi-angel "4,795 reputation")
    
     [Jun 10, 2019 at 11:59](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment99635576_522192)
    
-   @Hi-Angel I guess cgdb isn’t using [GNU Readline](https://en.wikipedia.org/wiki/GNU_Readline) or any other line-editing library. If that’s the case, then it won’t have decent prompt. Another powerful functionality provided by line-editor is to jump to previous word (`Alt` `f` by default for Readline). By the way, it doesn’t correctly shows color on my terminal, but some garbage like `[?2004h` instead. 
    
    – [Franklin Yu](https://stackoverflow.com/users/4451732/franklin-yu "8,460 reputation")
    
     [Jul 22, 2020 at 1:16](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment111452248_522192) 
    
-   1
    
    According to [GDB Wiki](https://sourceware.org/gdb/wiki/GDB%20Front%20Ends#Using_the_old_.28deprecated.29_annotations_mechanism_.28please_fix_them.21.29), cgdb is using a _deprecated_ mechanism to interact with GDB. 
    
    – [Franklin Yu](https://stackoverflow.com/users/4451732/franklin-yu "8,460 reputation")
    
     [Jul 22, 2020 at 1:25](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment111452341_522192)
    
-   1
    
    Oh, about the terminal output garbage it’s [known issue not fixed](https://github.com/cgdb/cgdb/issues/189). Sadly maintainer didn’t reply this issue. 
    
    – [Franklin Yu](https://stackoverflow.com/users/4451732/franklin-yu "8,460 reputation")
    
     [Jul 22, 2020 at 1:45](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment111452568_522192)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  22[](https://stackoverflow.com/posts/55595391/timeline)

New in upcoming **GDB 8.3!**

[https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git;a=blob;f=gdb/NEWS](https://sourceware.org/git/gitweb.cgi?p=binutils-gdb.git;a=blob;f=gdb/NEWS)

> Terminal styling is now available for the CLI and the TUI. GNU Source Highlight can additionally be used to provide styling of source code snippets. See the "set style" commands, below, for more information.

)]Share](https://stackoverflow.com/a/55595391 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/55595391/edit)

Follow

answered Apr 9, 2019 at 14:38

)]elmarco](https://stackoverflow.com/users/1277510/elmarco)

31.2k2121 gold badges6363 silver badges6868 bronze badges

-   1
    
    note your machine need at least **512MB** of RAM, otherwise, the `gcc` compiler will start thrashing. 
    
    – [user](https://stackoverflow.com/users/4934640/user "8,230 reputation")
    
     [Jul 3, 2019 at 18:47](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment100301126_55595391) 
    
-   Note: gdb must have been compiled with this feature enabled. If you happen to be on Gentoo something like this `echo 'sys-devel/gdb source-highlight' >> /etc/portage/package.use/my.use && emerge sys-devel/gdb` is what is needed to recompile with it enabled. 
    
    – [Chris](https://stackoverflow.com/users/2974621/chris "2,138 reputation")
    
     [Jul 25, 2021 at 18:29](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment121097287_55595391)
    
-   1
    
    Note: compiling with source-highlight is only necessary for _source code_ styling, the rest of the interface is colored by default since GDB 8.3. Note also that [since GDB 10.1](https://github.com/bminor/binutils-gdb/commit/f6474de9aacec990d44d2d65a337668b389efd99), you get source highlighting if you have the Pygments Python package installed. No need to have GDB compiled with source-highlight! 
    
    – [philb](https://stackoverflow.com/users/6026237/philb "1,688 reputation")
    
     [Sep 3, 2021 at 14:04](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment122029845_55595391)
    
-   Crap, RHEL 8 ships with 8.2 by default xD 
    
    – [Dan M.](https://stackoverflow.com/users/4114447/dan-m "3,748 reputation")
    
     [May 16, 2022 at 16:38](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment127668480_55595391)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  18[](https://stackoverflow.com/posts/3256580/timeline)

It is possible to greatly enhance the appears of gdb through the use of colors. This is done via any of the following methods:

1.  Colorized prompt via the "set prompt". E.g., make the prompt bold and red:
    
    `set prompt \033[1;31m(gdb) \033[m`
    
    or make the prompt a new shape, bold and red:
    
    `set prompt \033[01;31m\n\n#####################################> \033[0m`
    
    [![enter image description here](https://i.stack.imgur.com/f8NZl.png)](https://i.stack.imgur.com/f8NZl.png)
    
2.  Colorized commands via hooks
    
3.  Colorized syntax highlighting of the "list" command.

All examples are available at the following blog posts written by Michael Kelleher:

["Beautify GDB", May 12, 2010 (via archive.org)](https://web.archive.org/web/20101217181141/http://www.michaelkelleher.info/?p=1)

["Experimental GDB syntax highlighting", May 15, 2010 (via archive.org)](https://web.archive.org/web/20101217181206/http://www.michaelkelleher.info/?p=51)

[Share](https://stackoverflow.com/a/3256580 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/3256580/edit)

Follow

[edited Nov 12, 2018 at 10:35](https://stackoverflow.com/posts/3256580/revisions "show all edits to this post")

)]Scott Yang](https://stackoverflow.com/users/4751658/scott-yang)

31933 silver badges66 bronze badges

answered Jul 15, 2010 at 14:33

)]Mike](https://stackoverflow.com/users/14717/mike)

73511 gold badge77 silver badges1010 bronze badges

-   9
    
    @Mike: it would be useful to post the contents of those links here as the site is no longer accessible and robots.txt prevented archive.org from indexing it. 
    
    – [Lucian Adrian Grijincu](https://stackoverflow.com/users/303778/lucian-adrian-grijincu "2,451 reputation")
    
     [Apr 27, 2012 at 18:35](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment13342708_3256580)
    
-   1
    
    You can get the relevant information here: [sourceware.org/gdb/current/onlinedocs/gdb/Prompt.html](https://sourceware.org/gdb/current/onlinedocs/gdb/Prompt.html) 
    
    – [musiphil](https://stackoverflow.com/users/424632/musiphil "3,787 reputation")
    
     [Sep 14, 2013 at 0:35](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment27719508_3256580)
    
-   1
    
    The links are now pointed to an archive.org cache of the blog posts. 
    
    – [Alex Quinn](https://stackoverflow.com/users/500022/alex-quinn "3,943 reputation")
    
     [Sep 24, 2014 at 20:50](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment40765952_3256580)
    
-   1
    
    @Mike, it appears that you're the author of the blog posts; if so, you should disclose that in the answer. 
    
    – [David Z](https://stackoverflow.com/users/56541/david-z "126,780 reputation")
    
     [Feb 20, 2015 at 1:14](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment45542721_3256580)
    
-   For minimalist answer better use `set prompt \1\033[1;31m\2(gdb) \1\033[m\2` otherwise line editing on the prompt will be broken. 
    
    – [catpnosis](https://stackoverflow.com/users/1478629/catpnosis "536 reputation")
    
     [Oct 13, 2018 at 10:05](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment92504168_3256580)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  8[](https://stackoverflow.com/posts/15916463/timeline)

`cgdb` is much better than `gdb -tui`

[Share](https://stackoverflow.com/a/15916463 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/15916463/edit)

Follow

answered Apr 10, 2013 at 3:34

)]justin.yqyang](https://stackoverflow.com/users/1012014/justin-yqyang)

9911 silver badge22 bronze badges

-   Agreed. Simple lines printed by my program trashed gdb -tui's console. This does not happen under cgdb. Thanks for the tip! 
    
    – [Randall Cook](https://stackoverflow.com/users/974063/randall-cook "6,678 reputation")
    
     [Sep 10, 2014 at 23:07](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment40312733_15916463)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  7[](https://stackoverflow.com/posts/10418251/timeline)

```
#into .gdbinit
shell mkfifo /tmp/colorPipe

define hook-disassemble
echo \n
shell cat /tmp/colorPipe | c++filt | highlight --syntax=asm -s darkness -Oxterm256 &
set logging redirect on
set logging on /tmp/colorPipe
end 

define hookpost-disassemble
hookpost-list
end 

define hook-list
echo \n
shell cat /tmp/colorPipe | c++filt | highlight --syntax=cpp -s darkness -Oxterm256 &
set logging redirect on
set logging on /tmp/colorPipe
end 

define hookpost-list
set logging off 
set logging redirect off 
shell sleep 0.1s
end 

define hook-quit
shell rm /tmp/colorPipe
end 

define re
hookpost-disassemble
echo \033[0m
end 
document re
Restore colorscheme
end 
```

Warning: Buggy. No TUI support, 'user-mode' hack.

Found the main part [here](http://bluec0re.blogspot.com/2011/05/gdb-disassembly-syntax-highlighting-cpp.html) and modified it a bit. Needs highlight, c++filt. If colors get messed up issue re command.

[Share](https://stackoverflow.com/a/10418251 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/10418251/edit)

Follow

[edited May 3, 2012 at 11:09](https://stackoverflow.com/posts/10418251/revisions "show all edits to this post")

answered May 2, 2012 at 16:51

)]ftk](https://stackoverflow.com/users/1370490/ftk)

7111 silver badge22 bronze badges

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  5[](https://stackoverflow.com/posts/26463000/timeline)

Neat, I just found this hack using colout: [https://github.com/nojhan/colout/blob/master/colout/example.gdbinit](https://github.com/nojhan/colout/blob/master/colout/example.gdbinit)

![before - after](https://i.stack.imgur.com/TTZ9H.png)

[Share](https://stackoverflow.com/a/26463000 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/26463000/edit)

Follow

answered Oct 20, 2014 at 10:02

)]elmarco](https://stackoverflow.com/users/1277510/elmarco)

31.2k2121 gold badges6363 silver badges6868 bronze badges

-   It says to me `bash: colout: command not found` when I run `bt f` 
    
    – [user](https://stackoverflow.com/users/4934640/user "8,230 reputation")
    
     [Jul 3, 2019 at 17:57](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment100299808_26463000)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  4[](https://stackoverflow.com/posts/21201688/timeline)

I wanted to highlight as follows: emphasise the lines of a stack trace which belong to my source files (rather than libraries).

The solution was to use gdb-python (on MSYS; on Linux typically `gdb` comes with Python built-in already?), hook `backtrace`, use

```
python stack_trace = gdb.execute('backtrace', False, True')
```

Then process `stack_trace` with Python's regexes, and print them out. Bold and other colours are achieved by a function like this:

```python
def term_style(*v):
    """1 is bold, 30--37 are the 8 colours, but specifying bold may also
    change the colour. 40--47 are background colours."""
    return '\x1B['+';'.join(map(str, v))+'m'

#Use like this:
print term_style(1) + 'This will be bold' + term_style(0) #Reset.
print term_style(1,30) + 'This will be bold and coloured' + term_style(0)
print term_style(1,30,40) + 'Plus coloured background' + term_style(0)
```

[Share](https://stackoverflow.com/a/21201688 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/21201688/edit)

Follow

answered Jan 18, 2014 at 8:09

)]Evgeni Sergeev](https://stackoverflow.com/users/1143274/evgeni-sergeev)

22.1k1717 gold badges106106 silver badges123123 bronze badges

-   A working example that uses the hook — even if minimal — would be probably be more welcome. 
    
    – [Hi-Angel](https://stackoverflow.com/users/2388257/hi-angel "4,795 reputation")
    
     [Jun 10, 2019 at 11:49](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment99635304_21201688) 
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

#  4[](https://stackoverflow.com/posts/46733327/timeline)

Another good combination of colors is given by [this configuration](https://github.com/RAttab/dotfiles/blob/master/colors.gdb "this configuration"). It renders inspecting the backtraces a lot easier. To use it, just save that file as `~/.gdbinit` and run gdb normally

[Share](https://stackoverflow.com/a/46733327 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/46733327/edit)

Follow

answered Oct 13, 2017 at 15:31

)]Andrea Araldo](https://stackoverflow.com/users/2110769/andrea-araldo)

1,2921414 silver badges2020 bronze badges

-   Thanks, this was precisely what i was looking for. I was looking into a multi-threaded application with a long call stack and this is perfect for those backtraces. 
    
    – [Johan Bjäreholt](https://stackoverflow.com/users/2127148/johan-bj%c3%a4reholt "722 reputation")
    
     [Jan 29, 2018 at 14:05](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging#comment83999686_46733327)
    

[Add a comment](https://stackoverflow.com/questions/209534/how-to-highlight-and-color-gdb-output-during-interactive-debugging# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

-#  3[](https://stackoverflow.com/posts/1123400/timeline)

you can get whatever colors you want;

```
# gdb
(gdb) shell echo -en '\E[47;34m'"\033[1m"
...
anything is now blue foreground and white background
...
(gdb) shell tput sgr0
... back to normal
```

[Share](https://stackoverflow.com/a/1123400 "Short permalink to this answer")

[Improve this answer](https://stackoverflow.com/posts/1123400/edit)

Follow

answered Jul 14, 2009 at 4:16

)]RandomNickName42](https://stackoverflow.com/users/67819/randomnickname42)

5,91311 gold badge3535 silver badges3535 bronze badges




