---
title: "interfaces-add desktop and desktop-legacy 3719"
layout: post
---
# interfaces-add desktop and desktop-legacy 3719

[snapcore/snapd](https://github.com/snapcore/snapd) Merged
[interfaces: add desktop and desktop-legacy](https://github.com/snapcore/snapd/pull/3719#top)#3719

[jdstrand](https://github.com/jdstrand) merged 18 commits into [snapcore:master](https://github.com/snapcore/snapd "snapcore/snapd:master") from [jdstrand:desktop-interfaces](https://github.com/jdstrand/snapd/tree/desktop-interfaces "jdstrand/snapd:desktop-interfaces") on 30 Aug 2017 `Merged`


[jdstrand](https://github.com/jdstrand) merged 18 commits into [snapcore:master](https://github.com/snapcore/snapd "snapcore/snapd:master") from [jdstrand:desktop-interfaces](https://github.com/jdstrand/snapd/tree/desktop-interfaces "jdstrand/snapd:desktop-interfaces") on 30 Aug 2017  +578 −0 

 [Conversation 38](https://github.com/snapcore/snapd/pull/3719) [Commits 18](https://github.com/snapcore/snapd/pull/3719/commits) [Checks 0](https://github.com/snapcore/snapd/pull/3719/checks) [Files changed 6](https://github.com/snapcore/snapd/pull/3719/files)

## Conversation

[![jdstrand](https://avatars.githubusercontent.com/u/1663079?s=60&v=4)](https://github.com/jdstrand)



### jdstrand on 11 Aug 2017

This PR adds two new interfaces: desktop and desktop-legacy (and small addition to wayland for using snaps on plasma-desktop/wayland) for working with contemporary non-X based desktops. See the forum for the discussion: [https://forum.snapcraft.io/t/desktop-interfaces-moving-forward/1652](https://forum.snapcraft.io/t/desktop-interfaces-moving-forward/1652)

Jamie Strandboge added 8 commits [5 years ago](https://github.com/snapcore/snapd/pull/3719#commits-pushed-aa8fa4e)

[interfaces/desktop: add new 'desktop' interface for modern DEs](https://github.com/snapcore/snapd/pull/3719/commits/aa8fa4ea40be84a821e45c1553dd43ed4840812c "interfaces/desktop: add new 'desktop' interface for modern DEs")

[aa8fa4e](https://github.com/snapcore/snapd/pull/3719/commits/aa8fa4ea40be84a821e45c1553dd43ed4840812c)

[Merge branch 'wayland-interface' into modern-desktop-interfaces](https://github.com/snapcore/snapd/pull/3719/commits/4e8d49f3e53aa81664c1b6aad29bd6f8434a5cea "Merge branch 'wayland-interface' into modern-desktop-interfaces")

[4e8d49f](https://github.com/snapcore/snapd/pull/3719/commits/4e8d49f3e53aa81664c1b6aad29bd6f8434a5cea)

[interfaces/builtin/desktop_test.go: use modern testing techniques](https://github.com/snapcore/snapd/pull/3719/commits/baac71ab0bbb7f1f74b472c7e7ce73ff7012cf5a "interfaces/builtin/desktop_test.go: use modern testing techniques")

[baac71a](https://github.com/snapcore/snapd/pull/3719/commits/baac71ab0bbb7f1f74b472c7e7ce73ff7012cf5a)

[interfaces/desktop-accessibility: add transitional accessibility inte…](https://github.com/snapcore/snapd/pull/3719/commits/1cb9473392912560fcbc47c653435434885fd879 "interfaces/desktop-accessibility: add transitional accessibility interface") …

[1cb9473](https://github.com/snapcore/snapd/pull/3719/commits/1cb9473392912560fcbc47c653435434885fd879)

[update desktop-accessibility for gnome-shell accessibility](https://github.com/snapcore/snapd/pull/3719/commits/0720f6f8a775d06dfed7a01a2a0beb40bb5b50ad "update desktop-accessibility for gnome-shell accessibility")

[0720f6f](https://github.com/snapcore/snapd/pull/3719/commits/0720f6f8a775d06dfed7a01a2a0beb40bb5b50ad)

[add a couple of accesses needed by check-a11y](https://github.com/snapcore/snapd/pull/3719/commits/9f9a4abc1b3df689532a09571d2892b165716dd0 "add a couple of accesses needed by check-a11y")

[9f9a4ab](https://github.com/snapcore/snapd/pull/3719/commits/9f9a4abc1b3df689532a09571d2892b165716dd0)

[interfaces/wayland: allow read on /etc/drirc for Plasma desktop](https://github.com/snapcore/snapd/pull/3719/commits/17b55c5898fa5b73671efb1491a6b127a07c4e76 "interfaces/wayland: allow read on /etc/drirc for Plasma desktop")

[17b55c5](https://github.com/snapcore/snapd/pull/3719/commits/17b55c5898fa5b73671efb1491a6b127a07c4e76)

[Merge remote-tracking branch 'upstream/master' into desktop-interfaces](https://github.com/snapcore/snapd/pull/3719/commits/29163a642c2490b812a92115255ec379a4a2ad99 "Merge remote-tracking branch 'upstream/master' into desktop-interfaces")

[29163a6](https://github.com/snapcore/snapd/pull/3719/commits/29163a642c2490b812a92115255ec379a4a2ad99)

 [jdstrand](https://github.com/jdstrand) requested a review from [niemeyer](https://github.com/niemeyer) [5 years ago](https://github.com/snapcore/snapd/pull/3719#event-1202776226)

[![zyga](https://avatars.githubusercontent.com/u/784262?s=60&v=4)](https://github.com/zyga)

zyga reviewed [on 11 Aug 2017](https://github.com/snapcore/snapd/pull/3719#pullrequestreview-55823721)

[View changes](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99)



### zyga left a comment

LGTM with suggestion to document those new interfaces on the wiki and use docURL.

[interfaces/builtin/desktop_accessibility_test.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-f6ba6b33cf0cb4c2590beeaa6aefb47d1fad3aecfd08a5a9444bec2f8c992c47 "interfaces/builtin/desktop_accessibility_test.go")Outdated

  
```diff
+type DesktopAccessibilityInterfaceSuite struct {

+iface interfaces.Interface

+coreSlot *interfaces.Slot
```



### zyga on 11 Aug 2017

Nice, I will make this true everywhere.


[interfaces/builtin/desktop.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-bd855c70cfa352c5da5bd179f2ed75d297be385945d8e0959db52fe7dfa89e8a "interfaces/builtin/desktop.go")Outdated

  
```diff
  

+package builtin

  

+const desktopSummary = `allows running in modern Desktop Environments`
```



### zyga on 11 Aug 2017

Mildly worried about which interfaces an average app will need to use. I think this is okay and we need a clear rule on how things work and what they do. Perhaps this is the right time to use `docURL` and document the two new interfaces?

### Author jdstrand on 11 Aug 2017

Well, the idea is that we are changing how we are doing this. Please see the forum for discussion.

The original plan was to have an interface per DE (unity7, unity8, gnome-shell, plasma), per service (password-manager-service, etc) and per display server (mir, wayland, x11). Per service continues to make sense (you specify the services you want) and per display server makes sense (you don't necessarily just get mir and wayland for free-- you need to code your app to use them).

Per DE I think is the wrong approach and we really want to have only two: `desktop` and `desktop-legacy`. `desktop` is anything modern with safe APIs for application isolation, and `desktop-legacy` is for the old-world "everything in the user's desktop session is trusted". By adding `desktop` we remove the requirement that developers must add new DE interfacess as they are supported in snappy, so it makes things maintainable and future-proof going forward. Eg, some new DE "ShinyWayland" comes up, we just add the accesses to `desktop`and snaps using `desktop` just work there and everyone wins.

This is similar to how we added kde and cinnamon DBus APIs to unity7 today. `unity7` _is_ `desktop-legacy` for all intents and purposes today, and perhaps we want to provide an alias for it, but I didn't want to conflate a name transition/alias with this PR.



### Contributor niemeyer on 13 Aug 2017

Lowercase on "Desktop Environments" please. We can probably also get rid of "modern" here since this is somewhat subjective, and I wonder how to explain that this interface doesn't in fact offer running access unless other interfaces are connected. Perhaps:

`allows access to basic graphic desktop resources`

?




### Author jdstrand on 11 Aug 2017

[niemeyer](https://github.com/niemeyer) - in addition to the approach (please see the forum topic), can you also provide feedback on:

-   the names of the interfaces
-   whether or not desktop-accessibility should be auto-connected



### Author jdstrand on 12 Aug 2017

[@zyga](https://github.com/zyga) - I'll definitely add them to the wiki once this is approved (note, I went through the wiki the other day and brought it up to date for all committed interfaces-- we missed quite a few since 2.24).

As for docURL-- I wonder if it should be constructed dynamically. Eg

```
iface.docURL = fmt.Sprintf("https://github.com/snapcore/snapd/wiki/Interfaces#%s", iface.Name())
```


niemeyer requested changes [on 13 Aug 2017](https://github.com/snapcore/snapd/pull/3719#pullrequestreview-55949770)

[View changes](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99)

### Contributor niemeyer left a comment

Looks nice. A few changes recommended.

[interfaces/builtin/desktop.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-bd855c70cfa352c5da5bd179f2ed75d297be385945d8e0959db52fe7dfa89e8a "interfaces/builtin/desktop.go")Outdated

```diff
    
+package builtin

  
+const desktopSummary = `allows running in modern Desktop Environments`
```


### niemeyer on 13 Aug 2017

Lowercase on "Desktop Environments" please. We can probably also get rid of "modern" here since this is somewhat subjective, and I wonder how to explain that this interface doesn't in fact offer running access unless other interfaces are connected. Perhaps:

`allows access to basic graphic desktop resources`


[interfaces/builtin/desktop.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-bd855c70cfa352c5da5bd179f2ed75d297be385945d8e0959db52fe7dfa89e8a "interfaces/builtin/desktop.go")Outdated

```diff
+const desktopConnectedPlugAppArmor = `

+# Description: Can access modern Desktop Environments such as gnome-shell. To

+# be used with other interfaces (eg, wayland).

```
### Contributor niemeyer on 13 Aug 2017

Can be updated per note above as well.

### Author jdstrand on 15 Aug 2017

Done


[interfaces/builtin/desktop.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-bd855c70cfa352c5da5bd179f2ed75d297be385945d8e0959db52fe7dfa89e8a "interfaces/builtin/desktop.go")Outdated
```diff
+# Snappy's 'xdg-open' talks to the snapd-xdg-open service which currently works

+# only in environments supporting dbus-send (eg, X11). In the future once

+# snappy's xdg-open supports all snaps images, this access may move to another
```

### Contributor niemeyer on 13 Aug 2017

We'll actually be able to drop the DBus usage altogether pretty soon by moving to snapctl. That said, quite likely the "snap userd" daemon action may end up calling something else, but that won't be confined anymore.

### Author jdstrand on 15 Aug 2017

Rephrased to be less specific.



[interfaces/builtin/desktop_accessibility.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-6607a8c2608fa536b97d68b248309ca04c7c04a31d11c7f6274f043dd930d445 "interfaces/builtin/desktop_accessibility.go")Outdated

```diff
+const desktopAccessibilitySummary = `allows using desktop accessibility`

+const desktopAccessibilityBaseDeclarationSlots = `

+desktop-accessibility:
```


### niemeyer on 13 Aug 2017

Should probably hint that this is more unsafe than it sounds:

"allows wide-open access to keyboard interactions for accessibility purposes"

This is only telling half the story, but hints at the real reason that this interface exists, instead of being part of desktop itself.

### Author jdstrand on 15 Aug 2017

Done (though reworded slightly)



[interfaces/builtin/desktop_accessibility.go](https://github.com/snapcore/snapd/pull/3719/files/29163a642c2490b812a92115255ec379a4a2ad99#diff-6607a8c2608fa536b97d68b248309ca04c7c04a31d11c7f6274f043dd930d445 "interfaces/builtin/desktop_accessibility.go")Outdated
```diff
+const desktopAccessibilitySummary = `allows using desktop accessibility`

+const desktopAccessibilityBaseDeclarationSlots = `

+desktop-accessibility:
```


### niemeyer on 13 Aug 2017

Still needs renaming per forum agreement.

 ### Author jdstrand on 15 Aug 2017





### Contributor jhenstridge on 15 Aug 2017

As mentioned on the forum, I am a bit concerned about splitting off a11y into a separate interface. Ideally when people create desktop snaps they should default to being usable to everyone.

I realise that the current at-spi interface is not really designed with confinement in mind, but splitting it off doesn't seem like the right idea. If it is separate we'll want to encourage everyone to include it as a plug and have it autoconnect, and that puts us right back where we started.

I also notice that you dropped the rules that gave access to various input methods. That's going to mean apps using this new interface may not be usable by people who speak certain languages where they otherwise might have been. I realise that these have similar issues to a11y (e.g. as the Flatpak guys have seen when looking at IBus: [flatpak/flatpak#675](https://github.com/flatpak/flatpak/issues/675)), but I think I really don't want to end up in a situation where confined apps can only be used by able bodied Europeans.



Jamie Strandboge added 3 commits [5 years ago](https://github.com/snapcore/snapd/pull/3719#commits-pushed-fd3986d)

[Merge remote-tracking branch 'upstream/master' into desktop-interfaces](https://github.com/snapcore/snapd/pull/3719/commits/fd3986d5291c47e1bcc6013c7e897f351302c4be "Merge remote-tracking branch 'upstream/master' into desktop-interfaces")

[fd3986d](https://github.com/snapcore/snapd/pull/3719/commits/fd3986d5291c47e1bcc6013c7e897f351302c4be)

[remove TODOs](https://github.com/snapcore/snapd/pull/3719/commits/35cc0607e4a661ddc449efd036af14cdd0550f6e "remove TODOs")

[35cc060](https://github.com/snapcore/snapd/pull/3719/commits/35cc0607e4a661ddc449efd036af14cdd0550f6e)

[rename 'desktop-accessibility' to 'accessibility'](https://github.com/snapcore/snapd/pull/3719/commits/4988c0894f50433095b9a4a946ef959f9e68d334 "rename 'desktop-accessibility' to 'accessibility'")

[4988c08](https://github.com/snapcore/snapd/pull/3719/commits/4988c0894f50433095b9a4a946ef959f9e68d334)



### Author jdstrand on 15 Aug 2017

Note I commented extensively in the forum post regarding a11y (I won't repeat it here, but please read it). If an application is accessible, I think it should be added as a plug. Whether it is auto-connectable or not is a different conversation (which we should have. I personally would prefer it _not_ be auto-connected, but even if it is auto-connectable, it is at least disconnectable, which is better than what we have now). We strive for auto-connection for safe accesses, but we acknowledge that we must have transitional interfaces that are not safe but needed because the world isn't ready for arbitrary application isolation. If we keep the transitional rules in separate interfaces from the safe interfaces, it provides us a path to transition to safe while supporting applications today.

As for the input methods, I was planning on add them but they didn't show up in my testing. Thank you for pointing out they are missing. Do note that I figured that this interface was preliminary, and we'd add new accesses or other interfaces as needed and that we want to support input methods in some manner.

Currently we added all input methods that were possible in the unity7 interface, but before I just blindly add all of them here I have a few questions:

-   what is the input method that upstream gnome-shell and plasma have standardized on? (this might indicate what to (possibly) have by default with others (possibly) in other interfaces. It sounds like GNOME cares most about ibus...
-   do applications need to do anything special to be driven by different input methods? This might indicate whether to split into different interfaces (eg, ibus, fcitx, mozc, etc)

In Ubuntu Touch we looked at input methods rather extensively and, like accessibility, found they were quite problematic (as mentioned in the flatpak bug). I'm inclined to treat this like accessibility: break it out so it can be disconnectable and when upstream provides safe APIs, then add into `desktop` by default. I imagine we would want to auto-connect these and security-conscious people can experiment with disconnecting them.

Jamie Strandboge added 3 commits [5 years ago](https://github.com/snapcore/snapd/pull/3719#commits-pushed-fccd0a1)

[update descriptions (thanks to niemeyer)](https://github.com/snapcore/snapd/pull/3719/commits/fccd0a14593c6b5f41854ccd25c8e5e15aabd162 "update descriptions (thanks to niemeyer)")

[fccd0a1](https://github.com/snapcore/snapd/pull/3719/commits/fccd0a14593c6b5f41854ccd25c8e5e15aabd162)

[make xdg-open comment less specific since it will be changing soon](https://github.com/snapcore/snapd/pull/3719/commits/83cd7497e910462f0a0be29de6eaf0ed05cf5d68 "make xdg-open comment less specific since it will be changing soon")

[83cd749](https://github.com/snapcore/snapd/pull/3719/commits/83cd7497e910462f0a0be29de6eaf0ed05cf5d68)

[update description for accessibility (thanks niemeyer)](https://github.com/snapcore/snapd/pull/3719/commits/ad122ed2396b6170fe6372c5f0065f9cf5400535 "update description for accessibility (thanks niemeyer)")

[ad122ed](https://github.com/snapcore/snapd/pull/3719/commits/ad122ed2396b6170fe6372c5f0065f9cf5400535)



### Author jdstrand on 15 Aug 2017

For the purposes of moving the conversation forward, I'll add a new 'desktop-input' interface to this PR.



### Contributor niemeyer on 16 Aug 2017

[@jdstrand](https://github.com/jdstrand) If this PR continues changing with related but independent changes, it'll take a very long time for anything to go in at all.

[add desktop-input interface](https://github.com/snapcore/snapd/pull/3719/commits/163f40400dfd54112bd8165105c45a0773d33977 "add desktop-input interface")

[163f404](https://github.com/snapcore/snapd/pull/3719/commits/163f40400dfd54112bd8165105c45a0773d33977)

 [jdstrand](https://github.com/jdstrand) changed the title ~~interfaces: add new desktop and desktop-accessibility~~ interfaces: add new desktop, desktop-input and accessibility [on 16 Aug 2017](https://github.com/snapcore/snapd/pull/3719#event-1206720757)



### Author jdstrand on 16 Aug 2017

[@niemeyer](https://github.com/niemeyer) - I believe this is the last of what is needed for 'basic functionality'. I meant to have this all along. I'll add new accesses in future PRs.



### Author jdstrand on 16 Aug 2017

As for 'desktop-input', I've added it to have the three input methods we had in unity7: ibus, fcitx, and mozc. I consider this interface transitional and requiring auto-connection.

I don't consider having ibus, fcitx and mozc separated into individual interfaces to be necessary since the input methods (AIUI) are not something that applications have to opt into when using established libraries like gtk (ie, the desktop session installs some packages and does some configuring and then apps typically just work with whatever input method is configured). As a result, if they were separated out, developers would simply add all of them. Also, none are currently more safe to use than the others.

It looks like GNOME is trying to make ibus safe. When it is, we can add the dbus rules for org.freedesktop.portals.IBUS, etc as needed to the desktop interface and leave these privileged ibus rules in this transitional interface.



### Author jdstrand on 16 Aug 2017

Oh, I also named the interface 'desktop-input' since I thought 'input' could be confused with console input, /dev/uinput, etc and I thought 'input-methods' was too jargony.



### Contributor jhenstridge on 16 Aug 2017

To my knowledge, there isn't a single standardised input method for gnome-shell. GTK supports pluggable input methods loaded as shared libraries: some of these run in-process, while others communicate with some other service (e.g. the ibus and fcitx). Some of these input methods only support a single language, while others are frameworks that support multiple languages. It is all a bit of a mess, as evidenced by the rules we had in the `unity7` interface.

It seems likely that the work towards supporting confined applications (both us and flatpak) will involve picking some winners.



### Author jdstrand on 16 Aug 2017

[@jhenstridge](https://github.com/jhenstridge) - ok, thanks. This is consistent with my understanding and the reasoning for putting all this in `desktop-input`.

[update testsuite for desktop and desktop-input](https://github.com/snapcore/snapd/pull/3719/commits/c366ae084bc91851974bf8955d5c83a044b3b579 "update testsuite for desktop and desktop-input")

[c366ae0](https://github.com/snapcore/snapd/pull/3719/commits/c366ae084bc91851974bf8955d5c83a044b3b579)


### codecov-io on 17 Aug 2017

[Codecov](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=h1) Report

> Merging [#3719](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=desc) into [master](https://codecov.io/gh/snapcore/snapd/commit/6a96aa5c358e04eebd0d0fc2efb36629881f00dc?src=pr&el=desc) will **increase** coverage by `0.01%`.  
> The diff coverage is `100%`.

[![Impacted file tree graph](https://camo.githubusercontent.com/bed668c9a4de9a2c40718cb0bcbaab3e883d2a6219b405ca92b050f18489c058/68747470733a2f2f636f6465636f762e696f2f67682f736e6170636f72652f736e6170642f70756c6c2f333731392f6772617068732f747265652e7376673f77696474683d363530267372633d7072266865696768743d31353026746f6b656e3d543275366c68455a5336)](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree)
```
@@            Coverage Diff             @@
##           master    #3719      +/-   ##
==========================================
+ Coverage   75.81%   75.83%   +0.01%     
==========================================
  Files         402      404       +2     
  Lines       34793    34812      +19     
==========================================
+ Hits        26380    26400      +20     
+ Misses       6540     6539       -1     
  Partials     1873     1873
```




| [Impacted Files](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree)||||Coverage Δ|
|---------|---------|---------|---------|---------|
|[interfaces/builtin/wayland.go](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree#diff-aW50ZXJmYWNlcy9idWlsdGluL3dheWxhbmQuZ28=) |100% |<ø> |(ø) |⬆️|
|[interfaces/builtin/desktop.go](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree#diff-aW50ZXJmYWNlcy9idWlsdGluL2Rlc2t0b3AuZ28=)|100%|<100%> |(ø)|
|[interfaces/builtin/desktop_legacy.go](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree#diff-aW50ZXJmYWNlcy9idWlsdGluL2Rlc2t0b3BfbGVnYWN5Lmdv) |100%| <100%> |(ø)|
|[cmd/snap/cmd_aliases.go](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree#diff-Y21kL3NuYXAvY21kX2FsaWFzZXMuZ28=) |93.33% |<0%> |(-1.67%) |⬇️|
|[interfaces/sorting.go](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=tree#diff-aW50ZXJmYWNlcy9zb3J0aW5nLmdv) |100% |<0%> |(+2.56%)| ⬆️|


[Continue to review full report at Codecov](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=continue).

> **Legend** - [Click here to learn more](https://docs.codecov.io/docs/codecov-delta)  
> `Δ = absolute <relative> (impact), `ø = not affected`, `? = missing data`  
> Powered by [Codecov](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=footer). Last update [6a96aa5...b094401](https://codecov.io/gh/snapcore/snapd/pull/3719?src=pr&el=lastupdated). Read the [comment docs](https://docs.codecov.io/docs/pull-request-comments).



### Author jdstrand on 17 Aug 2017

The xenial autopkgtest failures are unrelated:  
2017-08-16 21:37:46 Failed tasks: 1  
- autopkgtest:ubuntu-16.04-i386:tests/main/refresh-all-undo  
2017-08-16 21:30:22 Failed tasks: 1  
- autopkgtest:ubuntu-16.04-amd64:tests/main/refresh-all



### Author jdstrand on 17 Aug 2017

[@niemeyer](https://github.com/niemeyer) - I addressed all your requested changes. This should be ready to re-review.

 [mvo5](https://github.com/mvo5) added this to the  [2.28](https://github.com/snapcore/snapd/milestone/13) milestone [on 23 Aug 2017](https://github.com/snapcore/snapd/pull/3719#event-1217326223)



### Contributor niemeyer on 26 Aug 2017

We had a good call and given all the feedback and ideas received around this PR, we decided to change its shape slightly and go with _desktop_ and _desktop-legacy_ interfaces, with the latter encompassing all the APIs that require unsafe access into the system and will be eventually deprecated.

[@jhenstridge](https://github.com/jhenstridge) This also addresses some of your valid worries about people ignoring accessibility concerns because it would require a manual step on their end. Now it's all bundled in a single pack that includes input and other common needs, which will encourage developers to ship snaps with accessibility support even before this is turned into a safe API.

[![niemeyer](https://avatars.githubusercontent.com/u/378683?s=60&v=4)](https://github.com/niemeyer) niemeyer approved these changes [on 26 Aug 2017](https://github.com/snapcore/snapd/pull/3719#pullrequestreview-58737344)

[View changes](https://github.com/snapcore/snapd/pull/3719/files/c366ae084bc91851974bf8955d5c83a044b3b579)

### Contributor niemeyer left a comment

LGTM assuming desktop-input and accessibility is bundled into a single _desktop-legacy_ interface, as we discussed on the call today and detailed above.

 [niemeyer](https://github.com/niemeyer) changed the title ~~interfaces: add new desktop, desktop-input and accessibility~~ interfaces: add desktop and desktop-legacy [on 26 Aug 2017](https://github.com/snapcore/snapd/pull/3719#event-1221989390)



### Author jdstrand on 26 Aug 2017

Ok, Gustavo and I talked about this with James' comments in mind. The plan of action is:

-   create the 'desktop' interface that contains only safe accesses (ie, what is proposed)
-   create a 'desktop-legacy' interface that is auto-connected today that contains accessibility and input methods
-   adjust the snapcraft desktop part to suggest adding these interfaces

What this allows is:

-   any snaps that currently use unity7, x11, etc continue to work without modification
-   for snaps that add desktop and desktop-legacy
    -   they get support for gnome-shell, plasma, sway, etc
    -   we'll add safe accesses to the desktop interface as they arrive (eg, portals, upstream ibus changes, etc) and snaps benefit without modification of snapcraft.yaml
    -   we have the opportunity to add other unsafe accesses to the 'desktop-legacy' catch-all interface in case there are other services/etc that should be added and snaps benefit without modification of snapcraft.yaml
    -   sometime in the future, when 'desktop' has everything a desktop snap reasonably needs, we'll make desktop-legacy, x11 and unity7 manually connected (not necessarily at the same time). This will allow snaps using standard toolkits to continue to work without needing to update their snapcraft.yaml (ie, they may continue to plugs both, but only 'desktop' is auto-connected). Snaps legitimately needing desktop-legacy can have a snap declartation for it
    -   security-conscious people and developers improving desktop security can experiment with disconnecting 'desktop-legacy' today

Thanks [@niemeyer](https://github.com/niemeyer) and [@jhenstridge](https://github.com/jhenstridge)! I'll adjust this PR accordingly.

Jamie Strandboge added 2 commits [5 years ago](https://github.com/snapcore/snapd/pull/3719#commits-pushed-bf98e52)

[Merge remote-tracking branch 'upstream/master' into desktop-interfaces](https://github.com/snapcore/snapd/pull/3719/commits/bf98e5290d9d78635ce9673e60b2fb943ec57e5f "Merge remote-tracking branch 'upstream/master' into desktop-interfaces")

[bf98e52](https://github.com/snapcore/snapd/pull/3719/commits/bf98e5290d9d78635ce9673e60b2fb943ec57e5f)

[move accessibility and desktop-input into new desktop-legacy interface](https://github.com/snapcore/snapd/pull/3719/commits/b09440158bdf0e5eed9564f5e7534031bae808f8 "move accessibility and desktop-input into new desktop-legacy interface")

[b094401](https://github.com/snapcore/snapd/pull/3719/commits/b09440158bdf0e5eed9564f5e7534031bae808f8)



### Author jdstrand on 26 Aug 2017

[@niemeyer](https://github.com/niemeyer) - fyi, the agreed to changes are implemented and ready for your review.

[![zyga](https://avatars.githubusercontent.com/u/784262?s=60&v=4)](https://github.com/zyga)zyga approved these changes [on 30 Aug 2017](https://github.com/snapcore/snapd/pull/3719#pullrequestreview-59510399)

[View changes](https://github.com/snapcore/snapd/pull/3719/files/b09440158bdf0e5eed9564f5e7534031bae808f8)



### zyga left a comment

LGTM.

I added one remark about the upcoming snapd-userd stuff that changes the DBus interface name for the safe launcher but let's do a follow-up for that.

[interfaces/builtin/desktop.go](https://github.com/snapcore/snapd/pull/3719/files/b09440158bdf0e5eed9564f5e7534031bae808f8#diff-bd855c70cfa352c5da5bd179f2ed75d297be385945d8e0959db52fe7dfa89e8a "interfaces/builtin/desktop.go")

```diff
+dbus (send)

+	bus=session

+	path=/

+	interface=com.canonical.SafeLauncher
```



### zyga on 30 Aug 2017

There's a second interface for this as you probably know. That is `io.snapcraft.SafeLauncher`. It should be added (can be in a follow-up PR)

### Author jdstrand on 30 Aug 2017

Ok, thanks. I'll add in a folowup


[interfaces/builtin/desktop_legacy.go](https://github.com/snapcore/snapd/pull/3719/files/b09440158bdf0e5eed9564f5e7534031bae808f8#diff-506219f0be20e1559a6c6079077ce94bcdcda7fe3b77a83b4db9669c5bbd869a "interfaces/builtin/desktop_legacy.go")
```diff
+bus=fcitx
+path=/org/freedesktop/DBus
+interface=org.freedesktop.DBus
+member={Hello,AddMatch,RemoveMatch,GetNameOwner,NameHasOwner,StartServiceByName}
```


### zyga on 30 Aug 2017

Hello? Wow I didn't know such a method exits. [https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces](https://dbus.freedesktop.org/doc/dbus-specification.html#standard-interfaces) is interesting read!



### Author jdstrand on 30 Aug 2017

We have two approvals so merging this. Thanks!

 [jdstrand](https://github.com/jdstrand) merged commit [`2d87bbc`](https://github.com/snapcore/snapd/commit/2d87bbc212b573c23164f41b2b55f8d2fe947579) into snapcore:master [on 30 Aug 2017](https://github.com/snapcore/snapd/pull/3719#event-1227613589)



### Author jdstrand on 30 Aug 2017

Thanks to everyone for the reviews! :)



 [jdstrand](https://github.com/jdstrand) deleted the desktop-interfaces branch [5 years ago](https://github.com/snapcore/snapd/pull/3719#event-1231149253)
