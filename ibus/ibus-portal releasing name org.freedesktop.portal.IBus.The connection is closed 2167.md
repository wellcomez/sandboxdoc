---
layout: post
tags:
- ibus
- flatpak
title: "ibus-portal releasing name org.freedesktop.portal.IBus.The connection is closed 2167"

---

# ibus-portal releasing name org.freedesktop.portal.IBus.The connection is

[ibus-portal[6268]: Error releasing name org.freedesktop.portal.IBus: The connection is closed #2167](https://github.com/ibus/ibus/issues/2167) Closed

[CentUser](https://github.com/CentUser) opened this issue on 24 Dec 2019 · 4 comments




### CentUser on 24 Dec 2019

Please fill in the following items if you don't know the root cause.

#### Which distribution and version?:  
CentOS Linux release 7.6.1810 (Core)

#### Which desktop environment and version?:  
GNOME-Classic:GNOME  
gnome-shell-3.28.3-6.el7.x86_64

#### Which session type?:  
(X11 or Wayland. Check $XDG_SESSION_TYPE)  
x11  
#### Which application and version?:  
(E.g. gedit 3.24, leafpad, kate, xterm)  
emacs and others including gnome desktop

#### IBus version?:  
(Run `ibus version`)  
IBus 1.5.21  
#### Issue description:  
Originally, I am using distribution included ibus version and I encountered the problem that emacs not responding to ibus pinyin. I tried the LC_TYPE? OR LC_ALL, with no effects, I also tried ibus-el(which I originally used the day before when it was ok with emacs). I restarted ibus using ibus-daemon -rxdv , still no effect.

So, I installed the latest ibus from this very git, which is 1.5.21. Successfully compiled and installed, I could read the ibus version output as 1.5.21.

while I think to put it into effect, perhaps I should restart the ibus, so , executed ibus-daemon -rxdv, only to find out that it was of no use.

Then, I rebooted the whole system, find that ibus could not startup, and the input method indicator at the up right corner of the desktop is also gone.

Checking journal -ex --no-pager, find out the title error.

#### Steps to reproduce:

1.  git clone latest version
2.  sh autogen.sh && sh ./configure --prefix=/usr --enable-wayland --enable-python-library
3.  make && make install
4.  reboot

#### Can you reproduce your problem when you restart ibus-daemon? (yes / no):  
(Run `ibus exit` and `ibus-daemon --xim &`)


#### Do you see any errors when you run ibus-daemon with the verbose option?:  
(Run `ibus-daemon --xim --verbose &` and look at the output when you encounter your problem.)  
after executing ibus-daemon --xim &  
it said current session already has an ibus-daemon  
Maybe this means ibus-daemon has already startup, but gnome panel still does not display the im indicator, and does not respond the IM change key stroke.

####  Can you reproduce your problem with a new user account instead of the current your account? (yes / no):  
Not tried.



### Author CentUser on 24 Dec 2019

I tried to erase distribution included ibus with yum erase ibus, but it is so tightly bundled with the gnome-settings and gdm, that I cannot erase it which could probably broke the graphic system.

And I think an IM should stay independent of the other systems, or else, we may run into this dilemma upgrading the IM needs to update gdm.

If I do not erase the original ibus, and I installed ibus to /usr/bin, should it have broken some files?



Member

### fujiwarat on 25 Dec 2019

I don't understand your problem at all.  
Steps to reproduce means how you open your application with steps, what you type exactly and what is your problem.

But you described installation steps and it does not explain your problem.



### Author CentUser on 26 Dec 2019

Emmm...... The problem is after a successful compile and installation, I find my CentOS "gnome-control-center region" input source could not add IBus as input source.

I later find out that gnome-control-center may crash.



Member

### fujiwarat on 26 Dec 2019

Can you write the steps to reproduce your issue correctly

 [fujiwarat](https://github.com/fujiwarat) closed this as [completed](https://github.com/ibus/ibus/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 27 Dec 2019](https://github.com/ibus/ibus/issues/2167#event-2909996757)

