---
layout: post
tags:
- flatpak
title: "Feature Request Packaging IBus engines with flatpak 3019"

---

# Feature Request Packaging IBus engines with flatpak 3019
[flatpak](https://github.com/flatpak)/[flatpak](https://github.com/flatpak/flatpak)
[[Feature Request] Packaging IBus engines with flatpak #3019](https://github.com/flatpak/flatpak/issues/3019#top)Open

[subins2000](https://github.com/subins2000) opened this issue on 20 Jul 2019 · 19 comments





### subins2000 on 20 Jul 2019

#### Linux distribution and version

Linux Mint 19 Tara

#### Flatpak version

Flatpak 1.0.8

#### Description of the problem

I'm trying to package this [IBus engine](https://github.com/varnamproject/libvarnam-ibus). Here's the [packaging repo](https://github.com/subins2000/varnam-flatpak). I gave enough permissions for the engine to connect to parent `ibus-daemon` but it fails :

```
GLib-GIO _g_io_module_get_default: Found default implementation local (GLocalVfs) for ?gio-vfs?
IBUS ibus_factory_new: assertion 'G_IS_DBUS_CONNECTION (connection)' failed
GLib-GObject g_object_ref_sink: assertion 'G_IS_OBJECT (object)' failed
IBUS ibus_factory_add_engine: assertion 'IBUS_IS_FACTORY (factory)' failed
IBUS ibus_bus_call_sync: assertion 'ibus_bus_is_connected (bus)' failed
```

To test if it's a connection problem, I ran the default [ibus-tmpl engine](https://github.com/ibus/ibus-tmpl), but that also fails establishing a connection :

```
Traceback (most recent call last):
  File "main.py", line 121, in <module>
	main()
  File "main.py", line 118, in main
	launch_engine(exec_by_ibus)
  File "main.py", line 78, in launch_engine
	IMApp(exec_by_ibus).run()
  File "main.py", line 59, in __init__
	self.__factory = IBus.Factory.new(self.__bus.get_connection())
TypeError: Argument 0 does not allow None as a value
```

So it seems that it is failing in a connection to ibus. I read [#675](https://github.com/flatpak/flatpak/issues/675), perhaps flatpak is limiting the access of an engine ?

Does flatpak's implementation of ibus portal supports packaging an engine ?  
Or am I doing it wrong ? Here are the [permissions](https://github.com/subins2000/varnam-flatpak/blob/ibus/com.varnamproject.Varnam.json) :

"finish-args": [
		"--share=ipc",
		"--own-name=org.freedesktop.IBus.EnchantPython",
		"--talk-name=org.freedesktop.DBus",
		"--talk-name=org.freedesktop.IBus",
		"--talk-name=org.freedesktop.portal.IBus",
		"--filesystem=xdg-run/dconf",
		"--filesystem=~/.config/dconf:ro",
		"--talk-name=ca.desrt.dconf",
		"--env=DCONF_USER_CONFIG_DIR=.config/dconf"
],





### AnwarShah on 6 Aug 2019

I have a similar problem, though I haven't started packaging yet




### subins2000 on 11 Aug 2019

[@AnwarShah](https://github.com/AnwarShah) what problem do you face ? Is the debug/error message similar to this ?



### AnwarShah on 13 Aug 2019

I haven't gone to packaging. I am assessing whether it is possible to use flatpak for providing ibus engine to be used by system ibus.





### mominul on 16 Aug 2019

I am maintaining an iBus engine, so I am also interested in this issue.






### subins2000 on 16 Aug 2019

[@mominul](https://github.com/mominul) Nice to see there's a Bangla ibus engine :)

Can you try packaging yours ? Perhaps another set of eyes will help in making it work. I don't know C much and haven't got into the engine's code. I'm just trying to package it.





### mominul on 16 Aug 2019

[@subins2000](https://github.com/subins2000) Thanks for your appreciation!

But I want to know about is it possible to pack IMEs with Flatpak in the first place as IME(ibus engines) might require special permissions. I don't have enough knowledge of dbus or Flatpak to know if this is possible. It'd be great if a person familiar with Flatpak would help us.

cc [@fujiwarat](https://github.com/fujiwarat) [@matthiasclasen](https://github.com/matthiasclasen)





### fujiwarat on 21 Aug 2019

Do you have ibus-portal?  
[https://github.com/ibus/ibus/tree/master/portal](https://github.com/ibus/ibus/tree/master/portal)



### Author subins2000 on 21 Aug 2019

[@fujiwarat](https://github.com/fujiwarat) This should included in the flatpak package ?

How do I know if I have it or not ? Is this a host thing or a package thing ?



### fujiwarat on 22 Aug 2019

ibus-portal is running in the host side. and sandbox also should include GTK IM module for IBus.  
I mean old IBus version does not implement ibus-portal.





### Author subins2000 on 26 Aug 2019

[@fujiwarat](https://github.com/fujiwarat) I'm using Ubuntu 18.04. Is `ibus-portal` installed from the default `ibus` package (`ibus amd64 1.5.17-3ubuntu5`) in official repos ? How do I know if it's running ?

How do I include GTK IM module in sandbox ? Any examples ?



### mominul on 26 Aug 2019

I think you have to use the latest version [https://github.com/ibus/ibus/releases](https://github.com/ibus/ibus/releases)



### fujiwarat on 26 Aug 2019

ibus 1.5.17 has ibus-portal. You can check it's running with `ps` command.  
You can find im-ibus.so with `find` command in your sandbox. E.g. org.freedesktop.Platform and org.gnome.Platform includes IBus GTK IM module.  
If your application uses Atom applications, you need ibus 1.5.20 or later.



### Author subins2000 on 28 Aug 2019

[@fujiwarat](https://github.com/fujiwarat) Just checked, I have `1.5.17`, `ibus-portal` is running and the sandbox has `im-ibus.so`(org.freedesktop.Platform). But the error still comes. Here's the full output :
```shell
sh-4.4$ ./ibus-engine-varnam  --ibus -l ml -n "varnam.ml"
(null) varnamd is not running. Launching a new instance
GLib posix_spawn avoided (automatic reaping requested) (fd close requested) (null) Failed to start varnamd. Failed to execute child process ?varnamd? (No such file or directory)
(null) Failed to start varnamd. Sync will be disabled
GVFS-RemoteVolumeMonitor Error: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: org.freedesktop.DBus.Error.ServiceUnknown
GVFS cannot open directory /usr/share/gvfs/remote-volume-monitors: Error opening directory ?/usr/share/gvfs/remote-volume-monitors?: No such file or directoryGVFS org.gtk.vfs.MountTracker.listMountableInfo call failed: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: org.freedesktop.DBus.Error.ServiceUnknown (g-dbus-error-quark, 2)
GLib-GIO _g_io_module_get_default: Found default implementation local (GLocalVfs) for ?gio-vfs?IBUS ibus_factory_new: assertion 'G_IS_DBUS_CONNECTION (connection)' failedGLib-GObject g_object_ref_sink: assertion 'G_IS_OBJECT (object)' failedIBUS ibus_factory_add_engine: assertion 'IBUS_IS_FACTORY (factory)' failedIBUS ibus_bus_call_sync: assertion 'ibus_bus_is_connected (bus)' failed

```


### fujiwarat on 28 Aug 2019

<mark class="hltr-pink">Sorry, I misread your issue. You tries to package an ibus engine but not application.  
IBus assumes to run IBus engines in the host side but not sandbox.  
You can run applications in the sandbox.</mark>




### subins2000 on 28 Aug 2019

[@fujiwarat](https://github.com/fujiwarat) so there's no way to package an IBus engine with flatpak ? Or any workarounds or something ?



### fujiwarat on 28 Aug 2019

There is no way and necessary for ibus engines.




### subins2000 on 30 Aug 2019

[@fujiwarat](https://github.com/fujiwarat) It would be useful for packaging and distributing engines across various distros with ease. So I guess this issue would be a feature request for flatpak and IBus.



 [subins2000](https://github.com/subins2000) changed the title ~~Unable to establish a IBus connection~~ [Feature Request] Packaging IBus engines with flatpak [on 31 Aug 2019](https://github.com/flatpak/flatpak/issues/3019#event-2597859978)



### fujiwarat on 5 Sep 2019

There is no reason to run ibus-daemon in host and engines in sandbox.



Member

### TingPing on 6 Sep 2019

-  [subins2000](https://github.com/subins2000) mentioned this issue [on 15 Aug 2020](https://github.com/flatpak/flatpak/issues/3019#ref-pullrequest-679305838)
	[Add com.varnamproject.Varnam software flathub/flathub#1747](https://github.com/flathub/flathub/pull/1747) Closed

-  [subins2000](https://github.com/subins2000) mentioned this issue [on 29 Aug 2020](https://github.com/flatpak/flatpak/issues/3019#ref-issue-688321712)
	[Is it possible to package fcitx engines with flatpak ? fcitx/fcitx5#108](https://github.com/fcitx/fcitx5/issues/108) Closed

 - [luongthanhlam](https://github.com/luongthanhlam) mentioned this issue [on 25 Sep 2020](https://github.com/flatpak/flatpak/issues/3019#ref-issue-666212352)
	[Không gõ được trên wps office BambooEngine/ibus-bamboo#153](https://github.com/BambooEngine/ibus-bamboo/issues/153)