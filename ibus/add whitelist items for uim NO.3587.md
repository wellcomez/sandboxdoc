---
title: "add whitelist items for uim NO.3587"
layout: post
---

# add whitelist items for uim NO.3587
[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public

[add whitelist items for uim #3587](https://github.com/netblue30/firejail/pull/3587) Merged

[rusty-snake](https://github.com/rusty-snake) merged 2 commits into [netblue30:master](https://github.com/netblue30/firejail "netblue30/firejail:master") from [antonv6:patch-1](https://github.com/antonv6/firejail/tree/patch-1 "antonv6/firejail:patch-1") on 25 Aug 2020

+2 −0 

 [Conversation 1](https://github.com/netblue30/firejail/pull/3587) [Commits 2](https://github.com/netblue30/firejail/pull/3587/commits) [Checks 0](https://github.com/netblue30/firejail/pull/3587/checks) [Files changed 2](https://github.com/netblue30/firejail/pull/3587/files)

## Conversation


### Contributor antonv6 on 14 Aug 2020

If your PR isn't about profiles or you have no idea how to do one of these, skip the following and go ahead with this PR.

If you make a PR for new profiles or changeing profiles please do the following:

-   The ordering of options follow the rules descripted in [/usr/share/doc/firejail/profile.template](https://github.com/netblue30/firejail/blob/master/etc/templates/profile.template).
    
    > Hint: The profile-template is very new, if you install firejail with your package-manager, it maybe missing, therefore, and to follow the latest rules, it is recommended to use the template from the repository.
    
-   Order the arguments of options alphabetical, you can easy do this with the [sort.py](https://github.com/netblue30/firejail/tree/master/contrib/sort.py).  
The path to it depends on your distro:
|Distro|Path|
|------|----|
|Arch/Fedora|`/usr/lib64/firejail/sort.py`|
|Debian/Ubuntu/Mint|`/usr/lib/x86_64-linux-gnu/firejail/sort.py`|
|local git clone| `contrib/sort.py`|
Note also that the sort.py script exists only since firejail `0.9.61`.
    

See also [CONTRIBUTING.md](https://github.com/CONTRIBUTING.md).

- [antonv6](https://github.com/antonv6) added 2 commits [2 years ago](https://github.com/netblue30/firejail/pull/3587#commits-pushed-4b336e9)
  1. [add ~/.uim.d directory to whitelist-common.inc](https://github.com/netblue30/firejail/pull/3587/commits/4b336e980142447313410d2c6130fff7f691eea2)  [4b336e9](https://github.com/netblue30/firejail/pull/3587/commits/4b336e980142447313410d2c6130fff7f691eea2) Verified
    add ~/.uim.d directory to whitelist-common.inc uim is a multilingual input method framework (similar to ibus, which has its own entry in this file" … 

	2.  [add /var/lib/uim to whitelist-var-common.inc](https://github.com/netblue30/firejail/pull/3587/commits/503fcc48b649a2f89de78ae2c916893930cf2f99)  [503fcc4](https://github.com/netblue30/firejail/pull/3587/commits/503fcc48b649a2f89de78ae2c916893930cf2f99) Verified
    add /var/lib/uim to whitelist-var-common.inc When user installs an uim module (for example, an input method like anthy or mozc), it gets registered in a file in this directory." 

[rusty-snake](https://github.com/rusty-snake) merged commit [e8b4527](https://github.com/netblue30/firejail/commit/e8b4527e25e03dc87097a65310628c7875191824) into netblue30:master [on 25 Aug 2020](https://github.com/netblue30/firejail/pull/3587#event-3686291185)



###  rusty-snake on 25 Aug 2020

Thanks for this contribution!

 [tedliosu](https://github.com/tedliosu) mentioned this pull request [on 10 Dec 2020](https://github.com/netblue30/firejail/pull/3587#ref-issue-583991420)

[[UIM input method switching not working with firefox NO.3284]] [#3284](https://github.com/netblue30/firejail/issues/3284)Closed

 [matu3ba](https://github.com/matu3ba) mentioned this pull request [on 8 Oct 2021](https://github.com/netblue30/firejail/pull/3587#ref-pullrequest-1020438819) [luarocks matu3ba/firejail#1](https://github.com/matu3ba/firejail/pull/1)Closed

