---
layout: post
tags:
- fcitx
- flatpak
- dbus-proxy
title: "sd-bus D-Bus authentication is broken when proxied via xdg-dbus-proxy no.99"

---

# sd-bus D-Bus authentication is broken when proxied via xdg-dbus-proxy no.99
[fcitx](https://github.com/fcitx)/[fcitx5](https://github.com/fcitx/fcitx5)Public
[sd-bus D-Bus authentication is broken when proxied via xdg-dbus-proxy #99](https://github.com/fcitx/fcitx5/issues/99)Closed

[tinywrkb](https://github.com/tinywrkb) opened this issue on 22 Aug 2020 · 4 comments


### Contributor tinywrkb on 22 Aug 2020

Sorry for not giving enough debug information and not debugging this properly, and I'm not sure whom to blame, Fcitx, sd-bus or xdg-dbus proxy, but I thought it should at least be reported.

I've packaged Fcitx5 as a Flatpak package ([the manifest is here](https://github.com/tinywrkb/flatpaks/blob/master/org.fcitx_im.fcitx5/org.fcitx_im.fcitx5.yaml), a bit hacky ATM) and I noticed that when building against sd-bus Fcitx5 is failing to start and outputs the error message: `addonloader.cpp:57] Failed to create addon: dbus Unable to request dbus name`.  
Flatpak (when started with `flatpak run -v --log-session-bus`) is reporting the error `Invalid AUTH line, aborting`that's `FIND_AUTH_END_ABORT` from [here](https://github.com/flatpak/xdg-dbus-proxy/blob/0.1.2/flatpak-proxy.c#L2569) 
```c
if (!auth_line_is_valid (line_start, line_end))

return FIND_AUTH_END_ABORT;
```
and [here](https://github.com/flatpak/xdg-dbus-proxy/blob/0.1.2/flatpak-proxy.c#L2585)
```c
if (client->auth_buffer->len >= 16 * 1024)
	return FIND_AUTH_END_ABORT;
return FIND_AUTH_END_CONTINUE;
```
.

With libdbus the issue is not occurring.  
<mark class="hltr-green">Also, when D-Bus filtering is not enabled for the user session bus (Flatpak cmdline option `--socket=session-bus`) then even when built against sd-bus, Fcitxt5 is running correctly.</mark>



### Member wengxt on 22 Aug 2020

Can you try following things?

1.  build without systemd, simply by using -DUSE_SYSTEMD=Off, then it will use libdbus and libevent. There's no difference in functionality. It could be either sd-bus, or my implementation in sd-bus missing something.
2.  <mark class="hltr-grey">My suspect is that flatpak doesn't allow certain dbus service name to be requested by default. Maybe by default only something like "org.freedesktop.portal.*" is possible. Is still possible to explicitly allow certain dbus name?</mark>



###  Author tinywrkb on 22 Aug 2020

1.  I already did that, it works perfectly with dbus/libdbus.
2.  I'm using overrides to give permissive access to whatever I saw in the Fcitx5 code and noticed via dbus-monitor and it works fine with libdbus.

```
  - --own-name=com.canonical.dbusmenu
  - --own-name=org.fcitx.*
  - --own-name=org.fcitx.Fcitx.Controller1
  - --own-name=org.fcitx.Fcitx5
  - --own-name=org.freedesktop.IBus
  - --own-name=org.freedesktop.IBus.*
  - --own-name=org.freedesktop.IBus.Panel
  - --own-name=org.freedesktop.portal.Fcitx
  - --own-name=org.freedesktop.portal.IBus
  - --own-name=org.kde.*
  - --talk-name=org.fcitx.GnomeHelper
  - --talk-name=org.freedesktop.Notifications
  - --talk-name=org.kde.StatusNotifierWatcher
```



### Member wengxt on 22 Aug 2020

I tried to strace and indeed even with sd-bus it send AUTH line.

I suspect it's the behavior described in [systemd/systemd#16610](https://github.com/systemd/systemd/issues/16610)

"Seems like a bug in xdg-dbus-proxy. It should process client requests in order, and hence not get confused."

You can keep an eye on [flatpak/xdg-dbus-proxy#21](https://github.com/flatpak/xdg-dbus-proxy/issues/21)



### Author tinywrkb on 22 Aug 2020

Thanks, so I guess there's no reason to keep this issue open, especially as this seems to only affect the Fcitx5 daemon and not the widget toolkits IM modules (GTK3 is using dbus-glib and QT5 libQt5DBus), and only the latter would find their way later on into the Flatpak runtimes.

 [wengxt](https://github.com/wengxt) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 22 Aug 2020](https://github.com/fcitx/fcitx5/issues/99#event-3681782207)

 [wengxt](https://github.com/wengxt) mentioned this issue [on 29 Aug 2020](https://github.com/fcitx/fcitx5/issues/99#ref-issue-688321712) [Is it possible to package fcitx engines with flatpak ? #108](https://github.com/fcitx/fcitx5/issues/108) Closed

 [X-m7](https://github.com/X-m7) mentioned this issue [on 21 Nov 2020](https://github.com/fcitx/fcitx5/issues/99#ref-issue-746420192)["Invalid AUTH line, aborting" message when trying to request a D-Bus service name with sd-bus flatpak/flatpak#3978](https://github.com/flatpak/flatpak/issues/3978) Closed

