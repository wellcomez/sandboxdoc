---
layout: post
tags:
- flathub
title: "flathub-im.riot.Riot Can''t use ibus IME 34"

---

# flathub-im.riot.Riot Can''t use ibus IME 34
[Can't use ibus IME #34](https://github.com/flathub/im.riot.Riot/issues/34) Closed

[wi24rd](https://github.com/wi24rd) opened this issue on 28 Feb 2019 · 2 comments


### wi24rd on 28 Feb 2019

I'm using ibus rime to input Chinese, but it doesn't work in this app which doesn't pop up the input candidate.  
Anyone also encounter this problem?



###  SISheogorath on 12 Mar 2019

Have the same problem. Seems to be related to [[IBus IME not working with electron app io.atom.electron.BaseApp-4]]

Seems to be fixed (on it's way to) upstream and needs to get down here over time.





###  SISheogorath on 1 Apr 2019

This should be fixed with the latest platform update (worked on my machine)








