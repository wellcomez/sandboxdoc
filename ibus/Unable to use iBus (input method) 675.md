---
layout: post
tags:
- flatpak
- ibus
- IBUS_ADDRESS
title: "Unable to use iBus (input method) 675"

---

[genggoro](https://github.com/genggoro) opened this issue on 31 Mar 2017 · 112 comments

[Unable to use iBus (input method)](https://github.com/flatpak/flatpak/issues/675#top)#675 Closed

[genggoro](https://github.com/genggoro) opened this issue on 31 Mar 2017 · 112 comments





### genggoro   on 31 Mar 2017

Is this simply because the GNOME runtime currently doesn't include `im-ibus.so`? If so, how are Flatpak apps supposed to work with the IM on the host system in general, considering that different people use different IMs?



###  matthiasclasen   on 31 Mar 2017

There is no 'supposed' way for this.  
For gnome apps and gnome on the host, we will continue to rely on ibus as the framework that is well-integrated on the host side. <mark style="background: #D2B3FFA6;">So we need to make sure that im-ibus is available and enabled in the runtime</mark>



### Member alexlarsson   on 4 Apr 2017

<mark style="background: #FFB86CA6;">Not only that, we also need to expose the host ibus in a safe way. I guess the clients talk to it using dbus? Is the apis safe for sandboxed use?</mark>



###  matthiasclasen   on 4 Apr 2017

I will look at this



### derekdai   on 4 Apr 2017

Not only iBus, but also other input method engine such as scim, fcitx, ...



###  matthiasclasen   on 5 Apr 2017

Here is the org.freedesktop.IBus interface:

[https://github.com/ibus/ibus/blob/master/bus/ibusimpl.c#L180](https://github.com/ibus/ibus/blob/master/bus/ibusimpl.c#L180)

<mark style="background: yellow;">The client-side im module calls CreateInputContext to create the object that it then works with.  
We probably want to not allow anything beyond that in the sandbox</mark>

Here is the org.freedesktop.IBus.InputContext interface:

[https://github.com/ibus/ibus/blob/master/bus/inputcontext.c#L216](https://github.com/ibus/ibus/blob/master/bus/inputcontext.c#L216)

<mark style="background: #FFB86CA6;">I haven't looked through this in detail yet</mark>



### juhp   on 6 Apr 2017

CC [@fujiwarat](https://github.com/fujiwarat)



###  matthiasclasen   on 6 Apr 2017

There's not much of any parameter validation in the input context code, that probably needs some improvements. Eg `bus_input_context_set_capabilities` passes the client-provided capabilities on to the engine without filtering out any extra bits that might be set. <mark style="background: #FF5582A6;">Same for the arguments of ProcessKeyEvent and SetCursorLocation.</mark>

It is not clear to me that the InputContext.SetEngine method is called from the client-side code. If it is not needed, maybe it shouldn't be exposed.



### fujiwarat   on 6 Apr 2017

> There's not much of any parameter validation in the input context code, that probably needs some improvements.

Sounds good to me.

> It is not clear to me that the InputContext.SetEngine method is called from the client-side code. If it is not needed, maybe it shouldn't be exposed.

I think an idea of a parameter likes `ibus-daemon --secure` to keep API.



### epico   on 6 Apr 2017

> I think an idea of a parameter likes ibus-daemon --secure to keep API.

I think maybe good to share the ibus-daemon for the sandbox and native apps.



###  matthiasclasen   on 6 Apr 2017

> I think an idea of a parameter likes ibus-daemon --secure to keep API.

I don't think that makes much sense, tbh. Would we run one ibus-daemon for sandboxed apps in 'secure' mode, and another one for the rest of the desktop ? If the 'secure' mode is good enough for all of the desktop, then why have the insecure mode at all ? Just drop the apis that are not needed, or add a smaller interface that we can safely expose to sandboxed apps



### epico   on 6 Apr 2017

<mark style="background: red;">IIUC, only "CreateInputContext" method of "org.freedesktop.IBus" interface is allowed in the sandbox.</mark>
<mark style="background: #ABF7F7A6;">
Also I think "BecomeMonitor" method of "org.freedesktop.DBus" interface should be disallowed.
</mark>


###  matthiasclasen   on 6 Apr 2017

What do you mean ? Are you saying ibus already has special code to handle being inside a sandbox ? where is that code ?



### epico   on 6 Apr 2017

Sorry, I just try to summarize my understanding, and made a spelling mistake, corrected it now.



### fujiwarat   on 6 Apr 2017

> then why have the insecure mode at all ?

Probably we will keep ibus_input_context_set_engine() and InputContext.SetEngine() until the version is bumped. Currently we have no plan to update the IBus version to 1.6 for a while.



### fujiwarat   on 6 Apr 2017

I mean `--secure` option would be an idea if you like to drop the API immediately since currently we have no plan to bump the version.  
Seems you don't bustle up the implementation so I cancel that idea now.

> What do you mean ?

I think he put his idea not to allow CreateInputContext beyond that in the sandbox since "BecomeMonitor" method is used by dbus-monitor.



###  matthiasclasen   on 6 Apr 2017

Here is a concern that alex mentioned on irc:

clients probably get a path to the context object, and can easily guess other clients context paths - what happens if they make calls there ?



###  matthiasclasen   on 6 Apr 2017

I was misstating things a bit, I think.

<mark style="background: #FF5582A6;">We actually need to use a separate unique bus name to prevent the client from talking to other, 'unsafe' interfaces. So, the suggestion is have a separate bus connection, take a different well-known name, and expose limited, safe interfaces on that name. And strictly validate that clients only make calls on 'their' objects</mark>.



###  matthiasclasen   on 7 Apr 2017

Here is how the context object paths get created:
```

src/ibusshare.h:#define IBUS_PATH_INPUT_CONTEXT "/org/freedesktop/IBus/InputContext_%d"

bus/inputcontext.c: gchar *path = g_strdup_printf (IBUS_PATH_INPUT_CONTEXT, ++id);
```



###  matthiasclasen   on 7 Apr 2017

And I can't find any validation that would check that Inputcontext calls are coming from the owner of the context.



###  matthiasclasen   on 7 Apr 2017

<mark style="background: #FFB86CA6;">An extra complication here is that ibus is using not just its own bus but its own daemon </mark>implementation which may or may not cause us extra problems when proxying - d-feet certainly has issues talking to it.



### epico   on 7 Apr 2017

For d-feet, we filed a bug before and write a draft patch for it.

URL: [https://bugzilla.redhat.com/show_bug.cgi?id=1111033](https://bugzilla.redhat.com/show_bug.cgi?id=1111033)



### fujiwarat   on 7 Apr 2017

CC'ing [@phuang](https://github.com/phuang)



###  matthiasclasen   on 7 Apr 2017

That d-feet patch seems wrong. Instead of making d-feet connect to unsupported addresses, why not make ibus provide a supported one ? Or, if that is hard, make gio support the address in question.

You can already get d-feet to connect to ibus by doing

`DBUS_SESSION_BUS_ADDRESS=$IBUS_ADDRESS d-feet`

That is how I did my initial investigation of ibus protocol.



###  matthiasclasen   on 7 Apr 2017

From irc:

alexlarsson> mclasen: honestly, the right way would probably to use a per-client connection instead of a separate bus  
mclasen> ok  
alexlarsson> just do the create-context on the normal bus  
then pass an fd to the client for dbus connection  
mclasen> there's multiple contexts in an app  
alexlarsson> and make sure you only allow access to that context from that fd  
mclasen> I think  
alexlarsson> so, one peer connection per unique id then  
and then only handle the context:es for that connection from that connection  
if you know what i mean



###  matthiasclasen   on 10 Apr 2017

A longer-term perspective for wayland is that this should really move to the compositor, and use the text protocol. That would solve the sandboxing issues for good.



###  matthiasclasen   on 9 May 2017

To clarify some of the last comments:

We want: one peer-to-peer connection per client (ie per application), not one per context (it turns out that clients generally use many contexts (one per text field)).



### fujiwarat   on 5 Jul 2017

<mark style="background: yellow;">I tried to get a dbus address per client with the following way:</mark>  
[https://github.com/fujiwarat/ibus/commits/dedicated-bus](https://github.com/fujiwarat/ibus/commits/dedicated-bus)

1.  Get a shared dbus address from $XDG_CONFIG_HOME/ibus/bus/*
2.  Connect ibus-daemon with the shared dbus address.
3.  Get a dedicted dbus address from the shared connection.
4.  Connect ibus-daemon with the dedicated dbus address.

But the packet is still readable by dbus-monitor with the shared dbus address.



### epico   on 7 Jul 2017

I just read the comments of dbus-proxy recently.  
Will ibus also use dbus-proxy for filtering?

<mark style="background: #FFB86CA6;">Is it not secure to expose non-well-known names  
to ibus client through dbus-proxy,  
such as "/org/freedesktop/IBus/InputContext_%d"?</mark>



### fujiwarat   on 10 Jul 2017

> Will ibus also use dbus-proxy for filtering?

I don't know how to use dbus-proxy. I guess g_dbus_connection_add_filter() does not help to avoid dbus-monitor.

> Is it not secure to expose non-well-known names to ibus client through dbus-proxy,

Right. But primarily I'm thinking to access by client.



### fujiwarat   on 12 Jul 2017

<mark style="background: #FFB86CA6;">I tried `flatpak run org.gnome.gedit` command and understood it runs `/usr/libexec/flatpak-dbus-proxy` internally</mark>.  
My understanding is flatpak-dbus-proxy sets the policy of "org.gnome.gedit*" to FLATPAK_POLICY_OWN but seems the dbus methods whose destination is "org.gnome.gedit*" are called three times only.  
<mark style="background: #FFB86CA6;">I can see most packets with `dbus-monitor --address 'unix:path=/run/user/$UID/bus'` but don't see any differences between `flatpak run org.gnome.gedit` and `flatpak --socket=session-bus run org.gnome.gedit`, the latter works without any filters as far as I understand.</mark>

Is it a good example to test flatpak-dbus-monitor?

Is it a expected result that dbus-monitor can observe the most packets of ibus?  
If yes, I do not understand what we try to restrict in the dbus connection but maybe my patch is fine at the moment.



### fujiwarat   on 20 Jul 2017

It seems I have no permission to install ibus in /usr with `flatpak build` and tried to install ibus in /app.  
I created org.freedesktop.IBus and org.freedesktop.IBus.Setup but how does ibus_bus_new() work between sandboxes?

```shell
% cd $BUILDDIR/ibus-1.5.16
% flatpak build $PKGDIR/ibus ./configure --prefix=/app --with-python=python3 --disable-emoji-dict
% flatpak build $PKGDIR/ibus make
% flatpak build $PKGDIR/ibus make install \
  pyoverridesdir=/app/lib/python3.5/site-packages/gi/overrides \
  py2overridesdir=/app/lib/python2.7/site-packages/gi/overrides \
  DISABLE_DCONF_UPDATE=1
% flatpak build $PKGDIR/ibus-setup make install \
  pyoverridesdir=/app/lib/python3.5/site-packages/gi/overrides \
  py2overridesdir=/app/lib/python2.7/site-packages/gi/overrides \
  DISABLE_DCONF_UPDATE=1
% cd $PKGDIR
% flatpak build-finish ibus --socket=x11 --share=network --command=ibus-daemon
% flatpak build-finish ibus-setup --socket=x11 --share=network --command=ibus-setup
...
% flatpak build-export repo ibus
% flatpak --user install tutorial-repo org.freedesktop.IBus
% flatpak build-export repo ibus-setup
% flatpak --user install tutorial-repo org.freedesktop.IBus.Setup
% flatpak run org.freedesktop.IBus
% flatpak run org.freedesktop.IBus.Setup
```

Then ibus-setup cannot connect to ibus-daemon.



### fujiwarat   on 21 Jul 2017

Filed `dconf update` issue; [https://bugzilla.gnome.org/show_bug.cgi?id=785208](https://bugzilla.gnome.org/show_bug.cgi?id=785208)



### fujiwarat   on 24 Jul 2017

Could you point me any test codes to establish a DBus connection between sandboxes?  
I tried the following files and run `flatpak run org.test.DBus.Example` and `flatpak run org.test.DBus.Example.Client` but failed.  
[https://fujiwara.fedorapeople.org/dbus/dbus-get-hello.c](https://fujiwara.fedorapeople.org/dbus/dbus-get-hello.c)  
[https://fujiwara.fedorapeople.org/dbus/dbus-send-hello.c](https://fujiwara.fedorapeople.org/dbus/dbus-send-hello.c)



###  matthiasclasen   on 25 Jul 2017

I'm not sure I understand the question. Flatpak provides filtered access to the session bus, but it is still a single bus. If one sandbox owns a name, and another sandbox talks to that name, that is a dbus connection between sandboxes, I guess ?

But why would you want that ? The way I see things, input method support is provided by the host system to the sandboxes. Maybe you are interested in sandboxing part of ibus itself ? That is not really relevant for our immediate problem, I think. And even if we end up doing that, we probably don't want direct connections between sandboxed apps and ibus backends in a different sandbox.



### fujiwarat   on 25 Jul 2017

[@matthiasclasen](https://github.com/matthiasclasen)  
Thank you for your explanation. I could not find in which host system or sandbox ibus-daemon runs.  
And you're right. ibus-daemon runs in the host system and the ibus clients run in a sandbox.

Now I succeeded to run the above test programs of `./dbus-get-hello` in the host system and `flatpak run org.test.DBus.Example` in a sandbox.  
Seems I have to add `sleep()` prior to `dbus_message_unref()` in the client program so that the sent message can be received by 
`flatpak/dbus-proxy/flatpak-proxy.c:get_dbus_method_handler()`.  
I will continue to investigate flatpak.



### epico   on 28 Jul 2017

I wrote some draft patches for ibus client to connect ibus daemon from sandbox.

URL: [https://github.com/epico/ibus/commits/flatpak](https://github.com/epico/ibus/commits/flatpak)

In the above four patches, the code logic is like:

-   ibus client will first try to access "org.freedesktop.IBus" interface,  
    if succeed, continue as normal, not in sandbox.
-   if not, try to access "org.freedesktop.sandbox.IBus" interface,  
    and use the returned file descriptor stored in `input_context_connection`,  
    then create new IBusInputContext using `input_context_connection`.
-   if input_context_connection is not null,  
    <mark style="background: #FF5582A6;">use input_context_connection directly for new IBusInputContex</mark>t,  
    as it is already in sandbox.

Currently I am thinking how to construct a file descriptor in ibus-daemon.



### fujiwarat   on 28 Jul 2017

> if not, try to access "org.freedesktop.sandbox.IBus" interface, and use the returned file descriptor stored in input_context_connection,

I don't see why the access needs to be failed in flatpak and also<mark style="background: #ABF7F7A6;"> I'm thinking unique connection per client but not per context.</mark>

> Currently I am thinking how to construct a file descriptor in ibus-daemon.
<mark style="background: #FF5582A6;">
I'm thinking to use DBUS_BUS_SESSION at first and change the dedicated connection as I linked the patch.</mark>



### Contributor 问题 ramcq   on 2 Aug 2017

We discussed this at GUADEC today, to summarise - we have two problems to solve:

- <mark style="background: #FFB8EBA6;">  how can the ibus clients inside the flatpak connect to the bus</mark>
- <mark style="background: #BBFABBA6;">  how do we restrict the behaviour of those clients so that they cannot influence the behaviour of other apps and the session a s a whole.</mark>

<mark style="background: #FFB86CA6;">For connecting, we don't want to need to add extra whitelisted paths/interfaces/etc in the .json description of every app, because then it would need a patch to every app to enable access to ibus - what we thought might work well is if the IBus session bus daemon implements an service named org.freedesktop.portal.IBus (with an interface named the same), which is accessible to all Flatpak apps by default, it could provide a way for apps inside to connect and get a private d-bus socket connecting them to the ibus daemon.</mark>

This interface could have a simple Connect method which returned the FD which was one end of a socketpair() which was added to the monitoring loop/etc like any incoming connection on the daemon.

In terms of constraining the behaviour of these sandboxed clients, the daemon must record that the connection comes from a sandboxed client, and then based on this check:

-   <mark style="background: #ADCCFFA6;">prevent access to read or write contexts or input events coming from any other client</mark>
-  <mark style="background: #ADCCFFA6;"> prevent access to any other interfaces which access the host, load code, modify behaviour for other clients etc - eg no configuration changes, module loading/unloading, etc</mark>



### Contributor ramcq   on 2 Aug 2017

[@cosimoc](https://github.com/cosimoc) FYI



### 解决思路 fujiwarat   on 2 Aug 2017

[@ramcq](https://github.com/ramcq) Thank you for your discussion.  
<mark style="background: #FF5582A6;">Probably I could understand the suggestions much better than the previous discussions.  
I'd like to use a name service likes org.freedesktop.portal.IBus.</mark>

What I'm implementing currently is:

1.  ibus-daemon runs `g_bus_own_name(G_BUS_TYPE_SESSION, "org.freedesktop.portal.IBus",...)` and provides a shared (public) connection.
2.  IBus clients connect it and get a dedicated (private) connection with a DBus method "<mark style="background: #FF5582A6;">GenerateDedicatedAddress</mark>"
3.  <mark style="background: blue;">ibus-daemon runs g_dbus_server_new_sync() per clients with the dedicated connection whose service name is "org.freedesktop.portal.IBus" with the address unix:abstract=/tmp/dbus-foo1,guid=foo2  </mark>
    The half of the implementation was done one month ago; [https://github.com/fujiwarat/ibus/commits/dedicated-bus](https://github.com/fujiwarat/ibus/commits/dedicated-bus)
4.  Probably I think g_dbus_connection_add_filter() can restrict the connections

<mark style="background: purple ;">Regarding to the monitoring, The current IBus uses </mark>`$XDG_CONFIG_DIR/.ibus/bus/$dbus_machine_id-$hostname-$display_number` before the IBus clients establish the DBus connections.  
I'm thinking to drop the monitoring file path and check the DBus name only with the shared connection.



### Contributor Rely to solution  ramcq   on 2 Aug 2017

Hi [@fujiwarat](https://github.com/fujiwarat), thanks for working on this!

For [#1](https://github.com/flatpak/flatpak/issues/1) you should create a separate bus connection to the session bus, and only export this name and a very restricted interface on this connection. <mark class="hltr-green">Then you know that any sandboxed client will only connect to you on this connection and interface</mark>, and not be able to contact any of the other methods on the ibus daemon - so you can restrict their access from the very start.

I'm not sure if 2/3 will work between a client which is inside the sandbox, and the daemon outside.<mark class="hltr-cyan"> The sandboxed clients have their own filesystem namespace and no access to /tmp, so anything which is passing a socket path from outside to inside won't work</mark>. <mark class="hltr-pink">This is why I suggested the socketpair and using D-Bus FD passing - this means there is no additional access needed for the sandboxed clients (they can already talk to the portal services) but also they will never be able to get a connection other than the one given to them.</mark>

Any connection that is provided this way (to a sandboxed client) can be handled differently inside the ibus-daemon and given very restricted access. If you create the socket pair you can control which objects are exported on a per-client basis. You can see eg in the gdbus tests how to create a GDBusConnection from a socketpair: [https://github.com/gnome/glib/blob/master/gio/tests/gdbus-overflow.c#L114](https://github.com/gnome/glib/blob/master/gio/tests/gdbus-overflow.c#L114)

For [#4](https://github.com/flatpak/flatpak/issues/4), I'm not sure if a filter is needed particularly, because after setting up the private connection to the sandboxed client, <mark class="hltr-orange">you know that only one client can possibly be connected there - you can control what objects and interfaces you export to that client. Ideal would be that you only export, eg the context for their own app - and not any of the other normal IBus objects and interfaces, but obviously you need the client library to work properly. </mark>So, if there are some methods that need to behave differently based on the type of connection (sandboxed vs normal) - so if you have some object tracking the state of one client which you use as the user_data in g_dbus_connection_register_object, you can add a member like gboolean confined = TRUE, and check this in the method implementation before permitting access.

Hope that all makes sense! I just got back from GUADEC and havn't had enough sleep / coffee. :)



### fujiwarat   on 2 Aug 2017

I confirmed the way works fine between server in a host and client in a sandbox  
[https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server3.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server3.c)  
[https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client3.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client3.c)

`flatpak run org.test.GDBus.Example` works fine.



### Contributor ramcq   on 2 Aug 2017

Hmm <mark class="hltr-purple">- this probably only works if you have --share=networking - an app with its own networking namespace isn't likely to be able to connect to an abstract socket like this. </mark>But I'm at the edge of my sandboxing knowledge, perhaps [@alexlarsson](https://github.com/alexlarsson) or [@matthiasclasen](https://github.com/matthiasclasen) would know better. :)



### fujiwarat   on 2 Aug 2017

<mark class="hltr-red">[@ramcq](https://github.com/ramcq) You're right. It only works with `--share=networking`.  
a socketpair works without shared options?</mark>

> For [#1](https://github.com/flatpak/flatpak/issues/1) you should create a separate bus connection to the session bus

Do you have any problems in my example code? I mean to prepare a shared connection to get the separated connection by client.



### Contributor ramcq   on 2 Aug 2017

> a socketpair works without shared options?

I believe so, yes. The D-Bus connection is proxied in to the new namespace by Flatpak, and then other file descriptors etc can be exchanged between namespaces provided they go via D-Bus.

> Do you have any problems in my example code? I mean to prepare a shared connection to get the separated connection by client.

<mark class="hltr-orange">In your current approach, you would need to make the whole org.freedesktop.IBus interface available over the session bus on the org.freedesktop.portal.IBus name, so that sandboxed clients can call the GenerateDedicatedAddress method.</mark> Unfortunately this means that the other methods can be called by the sandboxed client - eg Exit, or SetGlobalEngine, or other things which could be used by a malicious/compromised sandboxed app to disrupt or intercept the input handled for the desktop or other applications.

<mark class="hltr-green">If you make a new interface which you export as an object on a new, separate connection to the session bus, access to this org.freedesktop.portal.IBus name is then "confined" to only access methods on this interface.</mark> You should assume any of these methods can be called by a potentially hostile processes and you assume responsibility for implementing this interface with care, so that they cannot be abused to disrupt the system (outside of the sandboxed app) without user approval.

<mark class="hltr-green">The same logic applies to the private connection after it's been created/established, but you need to have this mindset for everything you export through the portal name too, as it can be accessed by any sandboxed client</mark>.



### 关于portal 服务 fujiwarat   on 2 Aug 2017

Right. Probably I understand it.  
However how can IBus clients get the connections from ibus-daemon?  
Even if the socketpair is used, I think it needs a way to share it between the server and client?  
E.g. Create a socketpair and send it with G_BUS_TYPE_SESSION.



####  fujiwarat   on 4 Aug 2017

[@ramcq](https://github.com/ramcq)

> [https://github.com/gnome/glib/blob/master/gio/tests/gdbus-overflow.c#L114](https://github.com/gnome/glib/blob/master/gio/tests/gdbus-overflow.c#L114)

How does the sever detect the client connection?  
<mark class="hltr-orange">E.g. GDBusServer emits "new-connection" signal and it can calls g_dbus_connection_register_object() druing the signal.  
I'm trying g_threaded_socket_service_new() but it does not emit "incoming" or "run" signal in my code.  
Probably the server needs to call g_dbus_connection_register_object() before it gets "Hello" method from g_dbus_connection_add_filter().</mark>



#### Contributor   ramcq   on 4 Aug 2017

> However how can IBus clients get the connections from ibus-daemon?  
> Even if the socketpair is used, I think it needs a way to share it between the server and client?  
> E.g. Create a socketpair and send it with G_BUS_TYPE_SESSION.

This is correct - you use a method on your new org.freedesktop.portal.IBus interface (with a private connection to the bus, exporting a object with just this interface) to send one end of the socket pair with D-Bus fd passing.

> How does the sever detect the client connection?  
> E.g. GDBusServer emits "new-connection" signal and it can calls g_dbus_connection_register_object() druing the signal.  
> I'm trying g_threaded_socket_service_new() but it does not emit "incoming" or "run" signal in my code.  
> Probably the server needs to call g_dbus_connection_register_object() before it gets "Hello" method from g_dbus_connection_add_filter().

You don't need any new server object or socket service - this is basically doing listen() and accept() for you on the socket - because the two ends of the socketpair are already connected - you can behave like accept() has already been called, create your GDBusConnection to export objects on it and then wait for the callbacks. Because the client receives the socket in an already connected state, they can also just create a GDBusConnection for it and start calling methods.



### 支持noshare fujiwarat   on 4 Aug 2017

[@ramcq](https://github.com/ramcq) Thank you for your help.  
I can establish the connections between a server in a host system and a client in a Flatpak system without shared=network

[https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server4.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server4.c)  
[https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client4.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client4.c)

I'd like to apply this way to IBus.



### Contributor ramcq   on 4 Aug 2017

[@fujiwarat](https://github.com/fujiwarat) Thanks for working on this! This looks very promising and seems to follow the approach I was thinking about - maybe [@alexlarsson](https://github.com/alexlarsson) or [@matthiasclasen](https://github.com/matthiasclasen) could review the code in more detail as my GLib skills are a bit out of date. :)



###  两种候选方案 fujiwarat   on 8 Aug 2017

[@ramcq](https://github.com/ramcq)

The current IBus uses a path of `$XDG_CONFIG_DIR/ibus/bus/$machine_id-$hostname-$display_number` to connect a DBus session between ibus-daemon and IBus clients.  
And now I'm trying to drop the path and use g_bus_own_name(G_BUS_TYPE_SESSION) to get a socketpair.  
I think it works with a single ibus-daemon but ibus-daemon runs by display.

I'm thinking two options.

1.  Bind `"org.freedesktop.portal.IBus.$display_number"` with g_bus_own_name(G_BUS_TYPE_SESSION) by ibus-dameon.
2.  Share `/run/use/$UID/ibus/bus/$machine_id-$display_number` between the host and sandboxes.

The either option needs a patch to send the host display to sandboxes:

```
--- flatpak-0.9.7/common/flatpak-run.c.orig	2017-07-28 15:06:08.217727814 +0900
+++ flatpak-0.9.7/common/flatpak-run.c	2017-07-28 15:18:16.503056317 +0900
@@ -1934,6 +1934,7 @@ flatpak_run_add_x11_args (GPtrArray *arg
       g_autofree char *d = NULL;
       g_autofree char *tmp_path = NULL;
 
+      *envp_p = g_environ_setenv (*envp_p, "FLATPAK_HOST_DISPLAY", display, TRUE);
       while (g_ascii_isdigit (*display_nr_end))
         display_nr_end++;

```

What do you think?  
I'm thinking the option 2 now and always bind a unique name "org.freedesktop.portal.IBus"



#### epico   on 8 Aug 2017

[@fujiwarat](https://github.com/fujiwarat)  
I like the option 2.



### Contributor ibus PerUser hadess   on 8 Aug 2017

Why does IBus support multiple ibus-daemons running per-user when we don't support this for anything else in the desktop sessions?



#### epico   on 8 Aug 2017

Maybe ibus contains some personal sensitive information.

Some input method will remember the user input history to provide suggestions.



#### Contributor hadess   on 8 Aug 2017

> Maybe ibus contains some personal sensitive information.
> 
> Some input method will remember the user input history to provide suggestions.

That still doesn't explain multiple ibus-daemons running for each user. There should only be one per user.



#### fujiwarat   on 8 Aug 2017

I think a system can provide multiple displays likes diskless clients or remote display.  
E.g. GDM can open local and remote displays and it allows the same UID logins the session.  
E.g. anonymous users accesses one disk server from diskless clients.



#### Contributor hadess   on 8 Aug 2017

> I think a system can provide multiple displays likes diskless clients or remote display.  
> E.g. GDM can open local and remote displays and it allows the same UID logins the session.

We don't allow this, no.

> E.g. anonymous users accesses one disk server from diskless clients.

That would be a separate session, so would have a separate D-Bus session bus.

You can pretty equate:  
1 user == 1 display == 1 D-Bus session bus

So you don't need a display identifier in the D-Bus well-known name on the session bus because you can only access the session bus for that user whose session you're already part of.



#### fujiwarat   on 8 Aug 2017

[@hadess](https://github.com/hadess)  
Thank you for your explanation.

> That would be a separate session, so would have a separate D-Bus session bus.

Probably I think you mean one g_bus_own_name(G_BUS_TYPE_SESSION) run by one ibus-daemon and g_bus_get_sync(G_BUS_TYPE_SESSION) run by multiple IBus clients but the session buses are separated.

OK, I see and let IBus try to support the different displays.



#### fujiwarat   on 8 Aug 2017

[@hadess](https://github.com/hadess)

> > E.g. anonymous users accesses one disk server from diskless clients.

> That would be a separate session, so would have a separate D-Bus session bus.

But IBus panel runes by display to open pre-edit windows, lookup windows, XKB.  
If multiple panels run by display, the D-Bus names are conflicted?



#### Contributor ramcq   on 8 Aug 2017

The D-Bus names exist only one one bus - there is one session bus for every display. You don't need to ever handle more than one display on one session bus.



### fujiwarat   on 8 Aug 2017

[@ramcq](https://github.com/ramcq) Thank you for your explanation.  
Probably I have to try some tests by myself.



### 提交patch  fujiwarat   on 8 Aug 2017

[@epico](https://github.com/epico) Thank you for your comment.

[fujiwarat](https://github.com/fujiwarat) added a commit to fujiwarat/flatpak that referenced this issue [on 10 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-618535b)[dbus-proxy: Own IBus bus name](https://github.com/fujiwarat/flatpak/commit/618535b87402611469acbdc70a9f02b1432fae61 "dbus-proxy: Own IBus bus name https://github.com/flatpak/flatpak/issues/675 Signed-off-by: Takao Fujiwara <tfujiwar@redhat.com>") … Unverified [618535b](https://github.com/fujiwarat/flatpak/commit/618535b87402611469acbdc70a9f02b1432fae61)



#### Patch from fujiwarat   on 10 Aug 2017

Creating a patch of IBus:  
[https://github.com/fujiwarat/ibus/commits/dedicated-bus2](https://github.com/fujiwarat/ibus/commits/dedicated-bus2)

Seems to works fine with me.

The Flatpak patch is:  
[https://github.com/fujiwarat/flatpak/commits/ibus-own-bus](https://github.com/fujiwarat/flatpak/commits/ibus-own-bus)



#### Patch from epico   on 11 Aug 2017

I wrote some draft patches for the "Connect" method of "org.freedesktop.Portals.IBus" interface.

URL: [https://github.com/epico/ibus/commits/flatpakv2](https://github.com/epico/ibus/commits/flatpakv2)

Please review it, thanks!

The code just passed compile, will debug it later.



##### Client 
epico   on 11 Aug 2017

For ibus client, the code logic is like:

<mark class="hltr-orange">ibus client will first try to call "CreateInputContext" method of "org.freedesktop.IBus" interface,  
if succeed, continue as normal, not in sandbox.</mark>

<mark class="hltr-cyan">if not, try to call"Connect" method of "org.freedesktop.Portals.IBus" interface;  
then use the returned fd as connection, close and replace original connection.</mark>

<mark class="hltr-cyan">And call "CreateInputContext" method of "org.freedesktop.IBus" interface again,  
with the returned connection.</mark>



##### ibus Daemon  
epico   on 11 Aug 2017

For ibus daemon, the code logic is like:

when "Connect" method is called,  
ibus will create a pair of socket fd using socketpair function,  
this first one will send to ibus client;  
the second one will save in ibus daemon.

Added allowed_paths in BusConnection struct,  
if not set, the connection is not in sandbox;  
if set, the connection is from the sandbox, and will filter the message by destination field.



##### fujiwarat   on 14 Aug 2017

> if succeed, continue as normal, not in sandbox.

As I noted before, there is no reason to fail the connection in the sandbox.  
Also I'm thinking to secure all connections besides IBusInputContext.  
I'm not sure why you're revising your similar patch with mine.



##### epico   on 14 Aug 2017

> As I noted before, there is no reason to fail the connection in the sandbox.

It is some method to detect whether the application is in sandbox,  
and this call is reserved for normal application.

Actually I just updated the patch to explain my understanding of  
how to make input context secure for sandbox.

The patch is different I think.



##### fujiwarat   on 14 Aug 2017

> It is some method to detect whether the application is in sandbox,  
> and this call is reserved for normal application.

So I think there is no reason to prepare two kind of the D-Bus and I think IBus always need to be secure.





### 关于socketpair fujiwarat   on 15 Aug 2017

When socketpair() is used, seems the server does get when the client close the connection.  
I called on the server:

```
    g_signal_connect (dbus_connection, "closed",
                      G_CALLBACK (bus_connection_closed_cb),
                      NULL);
```

But the callback is not called. Do you have any ideas to detect the client disconnection?  
The detection is important to restart ibus-daemon.

> [https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server4.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-server4.c)  
> [https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client4.c](https://fujiwara.fedorapeople.org/ibus/20170802/gdbus-hello-client4.c)

Also Is there a document about a Flatpak extension package?

```
% cd flatpak
% flatpak build-init --type=extension ibus-libs org.freedesktop.IBus.Libs org.gnome.Sdk org.gnome.Platform 3.24
% cat ibus-libs/metadata
[Runtime]
name=org.freedesktop.IBus.Libs
runtime=org.gnome.Platform/x86_64/3.24
sdk=org.gnome.Sdk/x86_64/3.24

[ExtensionOf]
ref=runtime/org.gnome.Platform/x86_64/3.24
% cd ../build/ibus-1.5.16/
% flatpak build ../../flatpak/ibus-libs mkdir /app/lib
execvp mkdir: No such file or directory
```

<mark class="hltr-red">I could succeed to patch GtkIMModule to add "/app/lib/gtk-3.0/3.0.0/immodules.cache"</mark>


[interfaces: add desktop and desktop-legacy snapcore/snapd#3719](https://github.com/snapcore/snapd/pull/3719)

 Merged



### Member [[Flatpak]] 需要补丁 alexlarsson   on 15 Aug 2017

> The Flatpak patch is:  
> [https://github.com/fujiwarat/flatpak/commits/ibus-own-bus](https://github.com/fujiwarat/flatpak/commits/ibus-own-bus)

This should not be needed, all clients are always allowed to talk to all org.freedesktop.portal.* names due to: [https://github.com/flatpak/flatpak/blob/master/common/flatpak-run.c#L3443](https://github.com/flatpak/flatpak/blob/master/common/flatpak-run.c#L3443)

The assumption is that such names have had sufficient review to be safe.



#### fujiwarat   on 15 Aug 2017

[@alexlarsson](https://github.com/alexlarsson) You're right. I didn't notice the patch is not needed.

I appreciate your code review.

> Creating a patch of IBus:  
> [https://github.com/fujiwarat/ibus/commits/dedicated-bus2](https://github.com/fujiwarat/ibus/commits/dedicated-bus2)

I think the patch almost works with Flatpak except for restarting ibus-daemon.

I hope `lib/libibus-*so.*`, `lib/gtk-3.0/3.0.0/immodules/im-ibus.so`, and `lib/gtk-3.0/3.0.0/immodules.cache` are available for IBus clients in sandboxes.



### Member  [[Flatpak]] 集成 alexlarsson   on 15 Aug 2017

Yay! Great that people are working on this!

However, I think there are still some disconnects in exactly what we need and what the risk model is here. I'm in no way an ibus expert, but I spent some time today researching it and trying to figure out  
the best approach going forward, and documenting the requirements. So, this comment may be a bit long, and I may make mistakes on the ibus side, and if a do, feel free to correct me.

First of all, lets talk about the scope. Flatpak is an _application_ sandboxing system. This means that there will always be things that are considered "host" and others that are "app". The host side is privileged and part of the operating system. Most of IBus is part of the "host", and as such works fine the way things are. It is not in our scope to run ibus itself, ibus engines, engine switching or ibus configuration inside a flatpak. So, for those things, everything can keep working as before.

The only thing we focus on here is the client. The goal is to ship immodules/im-ibus.so in the standard runtime, built so that applications automatically can access the host ibus service (if it is installed). Additionally we want such clients to _only_ be able to do the operations they have to, on the objects they need to.

<mark class="hltr-orange">From a quick look at the client code, this means applications should be able to:</mark>

-   Detect when ibus is available and goes away
-   Connect to ibus
-   Call CreateInputContext
-   Call org.freedesktop.IBus.InputContext methods and recieve signals,  
    but only on contexts created by the application itself.

(I may be missing some required operation here)

<mark class="hltr-orange">Here is a (non-exhaustive) list of operations that we do _not_ want to allow applications to do. And by that I mean that access to these operations is blocked on the server side, no by limitations in the client library.</mark>

-   Implement an ibus engine
-   Own a name (for instance org.freedesktop.IBus.Panel) on the ibus bus
-   Become a monitor on the ibus bus
-   Start a named service on the ibus bus
-   Talk directly to an engine
-   Guess an object path for an InputContext created by a different app  
    and send messages to it
-   Send messages to another client application via the ibus bus
-   Call SetGlobalEngine, Exit or other global configuration methods

<mark class="hltr-green">Looking at the current ibus implementation, it runs into issues directly with connecting to ibus. First it tries to read ~/.config/ibus/bus/ for the address, which is not visible to sandboxed apps, and then it tries to connect to the address from that file, which is not possible if the application does not have network access.</mark>

<mark class="hltr-red">The solution to this first step is what you have been working on. ibus should own the name org.freedesktop.portal.IBus on the regular session bus, and then you can bootstrap things from there. </mark>One approach is to have a single call there which returns you a file descriptor for a connection to the ibus bus. Then the sandboxed client will have full access to the ibus bus, and things will work.

However, this is not actually a great idea, because such a client will have full access (dbus filtering works only for the regular bus), and can do all the forbidden operations above. One possibile fix for this is to mark this particular client on the ibus bus as "sandboxed" and have code everywhere to limit what it can do. Since there are a lot of operations it is likely that some case is missed though, leading to a constant source of security vulnerabilities.

<mark class="hltr-red">Instead I propose that ibus owns the name org.freedesktop.portal.IBus on the session bus, and there exposes a different interface, say "org.freedesktop.portal.IBus" which only has a CreateInputContext method, and which records the unique session bus id of the client with the BusInputContext, only allowing calls from it, and sending signals to it directly to the target client</mark>.

<mark class="hltr-pink">The implementation of this should be pretty simple, because all the code, both in the client and the bus will work on the same type of inputcontext interfaces, just routed in different ways</mark>. The new interface should work fine for non-sandboxed client apps too, but if needed im-ibus.so could also fall back to the old way to connect for older ibus daemons.

Also, the way filtering works on dbus is a bit weird. Once you're allowed to talk to a given name, like org.freedesktop.portal.IBus, then you're also allowed to talk directly to the unique id that own that name (because this is how many dbus using apps work). <mark class="hltr-pink">This means that you can send message to _any_ object path and interface registered on that GDbusConnection, not only the once you might expect. So, you should create a custom GDBusConnection rather than relying on the shared one returned from g_bus_get() and register the portal implementation on that one only.</mark>



#### fujiwarat   on 15 Aug 2017

[@alexlarsson](https://github.com/alexlarsson) Thank you very much for the explanation.  
I like the concept of "host" (likes "desktop"), and "app".  
Probably my purpose and the Flatpak's one would be different.  
Now I'm thinking to revert the IBus Bus path and if ibus_get_address() is NULL, the IBus clients are assumed the connection is not trusted and get the connection from G_BUS_TYPE_SESSION and the connection is restricted.  
<mark class="hltr-red">ibus-daemon provides the interface in both the session bus and a custom GDBusServer which is the current bus server.  </mark>
I think this way can avoid the IBus client can enable IBusEngine, IBusPanelService in the sandbox.



#### fujiwarat   on 15 Aug 2017

However I think checking ibus_get_address() is not good to maintain IBus if sandbox and non-sandbox clients have the different type of buses.  
<mark class="hltr-green">Probably I'd think the possible solution is that:</mark>

-   The full g_dbus_connection_register_object() will be called after "RequestName" D-Bus method is received.
-   IBus panel runs all engines in the key 'sources' in GSettings `org.gnome.desktop.input-sources` for GNOME desktop or key 'preload-engines' in GSettings `org.freedesktop.ibus.general` for non-GNOME desktop to avoid that un-trusted clients enable IBusEngine.
-   ibus-daemon accepts the engines only in that GSettings value.



### Member alexlarsson   on 15 Aug 2017

Its possible in some cases for ibus_get_address() to return non-NULL, but still not be able to connect to the ibus daemon. For instance, if your app has been granted filesystem access to $HOME (so you can read the files) but not network access (so you can't connect to the address).<mark class="hltr-green"> This means you can't rely on that to detect whether you're in a sandboxed mode or not. To really know if connecting to the ibus address works you have to try to connect to it (or add flatpak specific detection code).</mark>

I'm not sure I understand what you mean about the gsetting keys.



#### fujiwarat   on 16 Aug 2017

> I'm not sure I understand what you mean about the gsetting keys.

I mean how to recognize for ibus-damon that the trusted access whose clients are expected to become IBus panel or engines, and none-trusted access whose clients are general applications including malwares.  
Owing the D-Bus unique name is first come and first reserve and the unique names are saved in '/usr/share/ibus/component' and ibus-daemon registers the names in `BusIBusImpl->{regisered_components|registered_engine_list}` but there are too many IBus engines so I'm thinking that ibus-daemon accepts the IBus engine unique names in the GSettings values only and all of those engines are launched by IBus panel at the bootstrap before anonymous "app" owns the unique names.  
The GSettings values are saved by `gnome-center-control region` or `ibus-setup`.  
And I'm thinking ibus-daemon permits the D-Bus methods by the unique names after "RequestName" D-Bus method is received.

> Its possible in some cases for ibus_get_address() to return non-NULL, but still not be able to connect to the ibus daemon.

You're right.

> To really know if connecting to the ibus address works you have to try to connect to it (or add flatpak specific detection code).

I need a callback when IBus clients or ibus-daemon exit for each other.  
It seems the GDBusConnection does not emit "closed" signal from another pair in case the socketpair() is used. The signal is important for ibus-daemon to confirm all clients terminate for the restarting and for IBus clients to close the current connection when ibus-daemon restarts.



#### the IBus interface need to register to dbus session bus, and own the names 
`epico   on 16 Aug 2017`

[@alexlarsson](https://github.com/alexlarsson)

BTW, I have a question about "org.freedesktop.portal.IBus" interface.

Does the IBus interface need to register to dbus session bus,  
and own the name?

Or still register the name to ibus daemon?



##### fujiwarat   on 16 Aug 2017

> Does the IBus interface need to register to dbus session bus, and own the name?

I'm thinking the session bus is the only way to provide a shared D-Bus connection and since Flatpak allows the "org.freedesktop.portal.*" only, both the shared and dedicated connection use the name and the client service registration is used by "RequestName" D-Bus method.  
I'm also thinking to follow the naming "org.freedesktop.portal.IBus" for bus path names and interface names.



### Member alexlarsson   on 16 Aug 2017

> I mean how to recognize for ibus-damon that the trusted access whose clients are expected to become IBus panel or engines, and none-trusted access whose clients are general applications including malwares.

This is the wrong approach, you will never be able to allow things to connect to the current ibus bus in a safe way by checking for permitted things on all codepaths. Instead you should just keep the current code as-is and keep using it for privileged things (like engines, panels, etc). <mark class="hltr-green">Then we make sure that sandboxed apps cannot access this bus (which is already true), and add a new way to talk to the daemon that is extremely limited in what it allows you to do. For instance it should not allow you to send _any_ kind of message directly to the ibus bus.</mark>

> Owing the D-Bus unique name is first come and first reserve and the unique names are saved in '/usr/share/ibus/component' and ibus-daemon registers the names in BusIBusImpl->{regisered_components|registered_engine_list} but there are too many IBus engines so I'm thinking that ibus-daemon accepts the IBus engine unique names in the GSettings values only and all of those engines are launched by IBus panel at the bootstrap before anonymous "app" owns the unique names.

Protecting against this simple attack is nowhere near enough.<mark class="hltr-yellow"> I can think of a hundred ways to attack the host (or other apps) if i can send messages on the ibus bus.</mark> Just do not let any sandboxed app call _anything_ (including RequestName) on the ibus bus and this problem just goes away.

This is important. As soon as you expose _anything_ to org.freedesktop.portal.* then all untrusted, malicious apps will be allowed to talk to all its objects and this will be the basis of the security of the entire system. You can't just add a few checks in some places and hope for the best, you have to be 100% sure that anything the malicious app sends will not be able to gain wider access, because then the entire sandboxing effort is for nothing.

Actually, maybe the best approach would be for org.freedesktop.portal.IBus to be its own process that implements _only_ the minimal API and forwards to the real bus using the traditional protocol. That way some accidental memory overrun in the ibus daemon will not break the system.



### Member alexlarsson   on 16 Aug 2017

> Does the IBus interface need to register to dbus session bus,  
> and own the name?  
> Or still register the name to ibus daemon?

The flatpak dbus filtering only applies to the dbus session bus. It is completely unaware of the ibus bus and any flatpaked app that manages to get on it can do whatever they want.

This means, yes, ibus must own the org.freedesktop.portal.IBus name on the session bus, and there is no reason for this name to exist on the ibus bus.



### Member alexlarsson   on 16 Aug 2017

I'll have a go at creating an ibus-portal implementation to show what i mean.



#### fujiwarat   on 16 Aug 2017

> you will never be able to allow things to connect to the current ibus bus in a safe way by checking for permitted things on all codepaths.

I don't find any ways to recognize which connections come from sandboxes in ibus-daemon side.  
Then how about preparing two names, "org.freedesktop.IBus" and "org.freedesktop.portal.IBus" on the session bus? And sandbox proxy changes "org.freedesktop.IBus" to "org.freedesktop.portal.IBus" internally.

> I'll have a go at creating an ibus-portal implementation to show what i mean.

Thank you.



#### epico   on 17 Aug 2017

> This means, yes, ibus must own the org.freedesktop.portal.IBus name on the session bus, and there is no reason for this name to exist on the ibus bus.

Thanks for the comments!

I think using "Connect" method of "org.freedesktop.portal.IBus" interface is simpler.

If we still use "CreateInputContext" method of "org.freedesktop.portal.IBus"  
interface, some problem may happen.

In my first patch set, I wrote "CreateInputContext" method.  
But the code has some possible locks in ibus daemon.

code logic:

-  <mark class="hltr-green"> ibus client called "CreateInputContext" method, ibus daemon will prepare the connection for ibus client</mark>
-  <mark class="hltr-green"> ibus client still waits for the method reply, ibus daemon waits for ibus client to connect back</mark>
-   <mark class="hltr-green">only after connected, ibus daemon can create the input context and ibus daemon return the method call with input context dbus path</mark>

If ibus uses "Connect" method, then we can re-use the current "CreateInputContext" method of "org.freedesktop.IBus" interface.

In my second patch set uses "Connect" method.  
URL: [https://github.com/epico/ibus/commits/flatpakv2](https://github.com/epico/ibus/commits/flatpakv2)

> I'll have a go at creating an ibus-portal implementation to show what i mean.

Thank you!



### Member Patch  from alexlarsson 

`alexlarsson   on 17 Aug 2017`

I pushed an initial version of this here:  
[https://github.com/alexlarsson/ibus/tree/ibus-portal](https://github.com/alexlarsson/ibus/tree/ibus-portal)

<mark style="background: #D2B3FFA6;">Basically, it adds a separate daemon (ibus-portal) which owns ther org.freedesktop.portal.IBus name on the session bus</mark>. It runs on the host so it is allowed to talk to the regular ibus bus. The dbus API it accepts is a much smaller one than the full ibus api, but is enough for clients to work (i believe). <mark class="hltr-red">It then changes the im-ibus.so module to talk to this bus instead in some cases (if in flatpak or if IBUS_USE_PORTAL env var is set).</mark>

I don't _really_ know ibus, but it seems to work for me. I didn't yet try it in a flatpak, just with the env var, but I will try that soon.

In terms of security, not only is the API available on the portal smaller, but in general there is not a lot of code running in iot that may accidentally leave some hole open.<mark class="hltr-pink"> Also, it uses gdbus code generation that checks all the method invocation types so that we don't forward invalid requests to ibus-daemon, and we also verify that InputContext callers are the actual owner before allowing the calls.</mark>



### Member alexlarsson   on 18 Aug 2017

Here is an example manifest for testing in flatpak:  
[https://gist.github.com/alexlarsson/820f52833238cebe4e86a8f2e6cd996a](https://gist.github.com/alexlarsson/820f52833238cebe4e86a8f2e6cd996a)

Test with:

```
flatpak-builder --force-clean app org.test.IBus.json
flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry
```

Seems to work here, although i seem to lack some chinese fonts in the sandbox...



#### fujiwarat   on 18 Aug 2017

[@alexlarsson](https://github.com/alexlarsson) Thank you very much for the patch.  
<mark style="background: #3BFF9BD9;">I was preparing to export two D-Bus names "org.freedesktop.IBus" and "org.freedesktop.portal.IBus" in ibus-daemon but I agree your approach is more secure for IBusInputContext.  </mark>
It's nice for me to know "/.flatpak-info" and there are also many great implementations.

<mark class="hltr-red">But I still have some concerns.</mark>

<mark>This way secures the InputContext only but ibus engine's connection can be seen by dbus-monitor.  
E.g. even if "ProcessKeyEvent" is not observed, "CommitText" can be observed.</mark>

I still cannot find how to detect that ibus-daemon's connection is closed and sandbox applications cannot reconnect to ibus-daemon.  
You can run `ibus restart` command on host for the test.

> flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry

Thank you for the example.  
I think the "app" type of IBus works for the commands on "runtime".  
But probably I think the "app" type applications also need to work with IBus, `E.g. org.gnome.gedit`  

And then I'm thinking how to build ibus for flatpak.  
I think integrating IBus to org.gnome.Plaform means the build file size is too big for me and I prefer the separating build.

Maybe libexec/ibus-portal is better than bin/ibus-portal?

9 hidden itemsLoad more…

###  alexlarsson on 18 Aug 2017 

> This way secures the InputContext only but ibus engine's connection can be seen by dbus-monitor.  
> E.g. even if "ProcessKeyEvent" is not observed, "CommitText" can be observed.

I'm not sure exactly which monitor you mean here, dbus-monitor on the session bus or on the ibus bus? Anyway, it is safe for multiple reasons. First of all, sandboxed clients cannot connect to the ibus bus at all, so they can see nothing there. <mark class="hltr-purple">Secondly, yes, the keys are sent on the session bus, and dbus-monitor can see them. However, the sandboxed app has a very limited access to the session bus. For instance, it cannot become a monitor, so it will never be able to see any messages not directed directly to it.</mark>

> I still cannot find how to detect that ibus-daemon's connection is closed and sandbox applications cannot reconnect to ibus-daemon.

Yes, this ibus restart is actually a known issue with my patch, I need to add that.

<mark class="hltr-green">The way you do this is in portal.c:name_owner_changed(). When ibus-portal dies it will release ownership of the org.freedesktop.portal.IBus name and we will get told of that. Additionally the portal itself need to listen to IBusBus::disconnected and exit when it gets that.</mark>

> Thank you for the example.  
> I think the "app" type of IBus works for the commands on "runtime".

Yeah, this is clearly just an example for testing. When this has landed upstream I will put the ibus client libraries and the gtk im-modules into the freedesktop runtime so that apps automatically work. You don't have to do that.

> Maybe libexec/ibus-portal is better than bin/ibus-portal?

Yes, that sounds better


###  dbus-monitor security  issue  
`fujiwarat on 23 Aug 2017`

> Yes, this ibus restart is actually a known issue with my patch, I need to add that.  
> The way you do this is in portal.c:name_owner_changed().

Thank you for that suggestion and I tried it.  
<mark class="hltr-red">I think when name_owner_changed() received a new owner of "org.freedesktop.IBus", ibus-daemon restarted but ibus-portal does not connect to ibus-daemon yet and now I think ibus-portal needs to listen IBusBus:connected instead.  
I created a patch on your patch now:  </mark>
[https://github.com/fujiwarat/ibus/commits/ibus-portal](https://github.com/fujiwarat/ibus/commits/ibus-portal)

> <mark>When this has landed upstream I will put the ibus client libraries and the gtk im-modules into the freedesktop runtime so that apps automatically work. You don't have to do that</mark>.

OK, I see. However the runtime build size is too big for me since I usually set up the debug environment on a virtual host. It would be great if I could have an ibus build env only for flatpak without building whole the runtime but I have to accept your suggestion.  
I have the plan to integrate your patch into ibus upstream.

> I'm not sure exactly which monitor you mean here, dbus-monitor on the session bus or on the ibus bus? Anyway, it is safe for multiple reasons.
k
Yes, you pointed out the valid resons however I mean users can crack ibus without sandbox applications. <mark class="hltr-green">E.g. dbus-monitor on the ibus bus, </mark>`dbus-monitor --address $(ibus address)` on the "host", <mark class="hltr-green">and might steal a password via</mark> "CommitText" method from an IBusEngine even <mark class="hltr-green">if methods from IBusInputContext are hidden by ibus-portal</mark>.

How about preparing three kind of ibus-portal?  
one runs for IBusInputContext, another runs for IBusPanel and IBusEngine, other runs for IBusConfig and so on.



####  alexlarsson on 24 Aug 2017

> Yes, you pointed out the valid resons however I mean users can crack ibus without sandbox applications. E.g. dbus-monitor on the ibus bus, dbus-monitor --address $(ibus address) on the "host", and might steal a password via "CommitText" method from an IBusEngine even if methods from IBusInputContext are hidden by ibus-portal.

<mark class="hltr-green">This is no different than the current security model, other than possibly being a bit easier to implement. </mark>And there is really no way to fix that. <mark class="hltr-green">If you're running unconstrained on the host you can do whatever you want, like attaching to ibus with gdb or whatever. Without sandboxing there can never be any security in this</mark>.



###  alexlarsson on 24 Aug 2017

> OK, I see. However the runtime build size is too big for me since I usually set up the debug environment on a virtual host. It would be great if I could have an ibus build env only for flatpak without building whole the runtime but I have to accept your suggestion.

For testing you can build some app like the test I have above. You have to bundle the apps you want to test though.


###  Patch  ibus-poral-2 alexlarsson on 24 Aug 2017

I redid the patch slightly differently, fixing some bugs, and it now supports ibus restarts.  
New version at: [https://github.com/alexlarsson/ibus/tree/ibus-portal-2](https://github.com/alexlarsson/ibus/tree/ibus-portal-2)


####  fujiwarat  on 24 Aug 2017

> This is no different than the current security model, other than possibly being a bit easier to implement. And there is really no way to fix that. If you're running unconstrained on the host you can do whatever you want, like attaching to ibus with gdb or whatever. Without sandboxing there can never be any security in this.

OK, I see. Probably I stop to think about it until any other frameworks will implement the sandboxes for the engine layers. Thank you.

> For testing you can build some app like the test I have above. You have to bundle the apps you want to test though.

OK, I see.

> I redid the patch slightly differently, fixing some bugs, and it now supports ibus restarts.  
> New version at: [https://github.com/alexlarsson/ibus/tree/ibus-portal-2](https://github.com/alexlarsson/ibus/tree/ibus-portal-2)

Who launch ibus-portal again in your patch?  
My understanding is, ibus-portal is activated by running ibus_bus_new_async_client() in the client applications but once ibus-portal exits with on_name_lost(), the clients wait for portal_name_appeared() until ibus-poral restarts.

### 最终提交 
`fujiwarat   on 25 Aug 2017`

[@alexlarsson](https://github.com/alexlarsson) <mark style="background: #E53B7CD9;">Thank you for your quick review.</mark>  
I will start the code review of the patches on behalf of you CC'ing you next week. Of course, yourself also can start it with `cd ibus; git log codereview.settings`.  
I will adjust 80 columns for every line to follow the ibus coding style.

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 30 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-203a3df)[Initial version of ibus portal](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7)Initial version of ibus portal This adds a dbus service called org.freedesktop.portal.IBus on the session bus. It is a very limited service that only implements CreateInputContext and the InputContext interface (and Service.Destroy for lifetime access). It uses gdbus code generation for demarshalling the method calls which means it will verify that all arguments have the right type. Additionally all method calls to the input context object have to be from the client that created it, so each client is isolated. 
- BUG https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com 
- Review URL: https://codereview.appspot.com/326350043 
- <mark style="background: #3BFF9BD9;">Patch from Alexander Larsson</mark> <alexl@redhat.com> … [203a3df](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7)

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 31 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-35ce624) [Support the portal in the gtk im modules](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae) 

Support the portal in the gtk im modules <mark style="background: #3BFF9BD9;">This adds a new way to create an IbusBus, ibus_bus_new_async_client()</mark>. This returns an object that is not guarantee to handle any calls that are not needed by a client, meaning CreateInputContext and handling the input context.<mark style="background: #D2B3FFA6;"> If you are running in a flatpak, or if IBUS_USE_PORTAL is set, then instead of talking to the regular ibus bus we connect to org.freedesktop.portal.IBus on the session bus and use the limited org.freedesktop.IBus.Portal interface instead of the org.freedesktop.IBus interface.</mark> This allows flatpaks (or other sandbox systems) to safely use dbus clients (apps). 
- BUG=https://github.com/flatpak/flatpak/issues/675 
- Review URL: https://codereview.appspot.com/328410043 
- Patch from Alexander Larsson <alexl@redhat.com> …[35ce624](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae)

### Member gtk3 im-module into the freedesktop runtime 
`alexlarsson   on 2 Sep 2017`

I built the gtk3 im-module into the freedesktop runtime now. I guess we should add the gtk2 one to the gnome runtime also.



#### fujiwarat   on 4 Sep 2017

> I built the gtk3 im-module into the freedesktop runtime now. I guess we should add the gtk2 one to the gnome runtime also.

Yes. Also `/usr/libexec/ibus-x11` and `/usr/lib64/qt5/plugins/platforminputcontexts/libibusplatforminputcontextplugin.so` too.
- [fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 4 Sep 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-9772e80)[test: Testing in flatpak](https://github.com/ibus/ibus/commit/9772e800f3e6937510f2609c5ce9a6860c59cb81)test: Testing in flatpak Test with: flatpak-builder --force-clean app org.test.IBus.json flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry 
	- BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com 
	- Review URL: https://codereview.appspot.com/329090043 
	- Patch from Alexander Larsson <alexl@redhat.com> …[9772e80](https://github.com/ibus/ibus/commit/9772e800f3e6937510f2609c5ce9a6860c59cb81)
- [fujiwarat](https://github.com/fujiwarat) added a commit to ibus/ibus that referenced this issue [on 4 Sep 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-9937a0e)[bus: ibus-daemon activates ibus-portal](https://github.com/ibus/ibus/commit/9937a0e4501ccf0cfd238ce7c97733c3099db3f7)( "bus: ibus-daemon activates ibus-portal When ibus-daemon restarts, ibus-portal exits with on_name_lost() and the clients wait for portal_name_appeared() until ibus-poral restarts. Now the clients can connect to ibus-daemon with this way and also they don't have to activate ibus-portal. 
	- BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com 
	- Review URL: https://codereview.appspot.com/321530043 
	- Patch [9937a0e](https://github.com/ibus/ibus/commit/9937a0e4501ccf0cfd238ce7c97733c3099db3f7)

###  X11 
`alexlarsson   on 4 Sep 2017`

What exactly does ibus-x11 do?



#### fujiwarat   on 5 Sep 2017

<mark class="hltr-green">ibus-x11 communicates with X clients and ibus-daemon for the key events with XIM and IBus protocols.  </mark>
X application (X11 or Java) <=> X server <=> ibus-x11 <=> ibus-daemon  
You have to set XMODIFIERS environment variable. E.g. `env XMODIFIERS=@im=ibus xterm`




### Now Fedora IBus RPM includes ibus-portal.  
`fujiwarat   on 15 Sep 2017`

Now Fedora IBus RPM includes ibus-portal.  
Do Fedora and upstream Flatpak enable IBus?




#### alexlarsson   on 18 Sep 2017

The very latest runtimes should have ibus support, yes.



####  fujiwarat   on 21 Sep 2017

Thank you. I forgot to copy ibusimcontext.c to client/gtk3 in fedora build.





### juhp   on 14 Nov 2017

Which are the first flatpak and ibus releases that support this?

### Member  Packages for rpm-based linux distributions ibus 1.5.17  

`alexlarsson   on 16 Nov 2017`

[@juhp](https://github.com/juhp) Ibus 1.5.17, and it is independent on the flatpak version, <mark class="hltr-green">but needs a recent flatpak runtime with the ibus version in it (should be ok with e.g. freedesktop.org 1.6 runtimes and gnome >= 3.24 runtimes).</mark>


### Eelectron Issue 
- [Packages for rpm-based linux distributions signalapp/Signal-Desktop#1630](Packages%20for%20rpm-based%20linux%20distributions%201630.md) closed
- [Missing ibus support flathub/org.signal.Signal#17](Missing%20ibus%20support%2017.md) open
- [electron apps do not recognize ibus 1671](electron%20apps%20do%20not%20recognize%20ibus%201671.md) close



####  slack chenzhiwei   on 12 Oct 2018

Hello, I still can't use ibus in Slack flatpak app, I think both ibus and Slack flat app are latest version:

-   ibus

```shell
zhiwei@deepin:~$ ibus version
IBus 1.5.18
zhiwei@deepin:~$ dpkg -l | grep ibus
ii  gir1.2-ibus-1.0:amd64                                  1.5.18-1                             amd64        Intelligent Input Bus - introspection data
ii  ibus                                                   1.5.18-1                             amd64        Intelligent Input Bus - core
ii  ibus-clutter:amd64                                     0.0+git20090728.a936bacf-5.1+b2      amd64        ibus input method framework for clutter
ii  ibus-gtk:amd64                                         1.5.18-1                             amd64        Intelligent Input Bus - GTK+2 support
ii  ibus-gtk3:amd64                                        1.5.18-1                             amd64        Intelligent Input Bus - GTK+3 support
ii  ibus-qt4                                               1.3.3-1+b1                           amd64        qt-immodule for ibus (QT4) (plugin)
ii  ibus-rime                                              1.2-1+b1                             amd64        Rime Input Method Engine for IBus
ii  libgusb2:amd64                                         0.2.11-1                             amd64        GLib wrapper around libusb1
ii  libibus-1.0-5:amd64                                    1.5.18-1                             amd64        Intelligent Input Bus - shared library
ii  libibus-qt1                                            1.3.3-1+b1                           amd64        qt-immodule for ibus (QT4) (library)
```

-   com.slack.Slack

```shell
zhiwei@deepin:~$ flatpak info com.slack.Slack 
Ref: app/com.slack.Slack/x86_64/stable
ID: com.slack.Slack
Arch: x86_64
Branch: stable
Origin: flathub
Date: 2018-10-08 13:27:56 +0000
Subject: Update to 3.3.3 (9dc0519c)
Commit: af79c798127613ed8f87418ee2406b1ef52c3781d8a054ce727a08cd5409b0fb
Parent: 597977f6d80c3df3c890df167b30240c6f503da12172fbf8fc2782402f91f651
Location: /var/lib/flatpak/app/com.slack.Slack/x86_64/stable/af79c798127613ed8f87418ee2406b1ef52c3781d8a054ce727a08cd5409b0fb
Installed size: 2.8 MB
Runtime: org.freedesktop.Platform/x86_64/18.08
```

There Slack ibus/bus directory was created `.var/app/com.slack.Slack/config/ibus/bus/` but nothing inside, and I tried to `cp .config/ibus/bus/312142facf6a41b9b8ea773d842a66de-unix-0 .var/app/com.slack.Slack/config/ibus/bus/`, but still can't use ibus.

Update: I can use ibus in org.gnome.gedit flat app.

Can someone give me some advice about this issue?



### Contributor hadess   on 12 Oct 2018

> Can someone give me some advice about this issue?

This bug has been closed for a year, and there's a link about Electron apps just above your comment. Following that link would be a good first step. File a separate bug if your issue is a separate problem.



### laichiaheng   on 15 Oct 2018

Why is it closed, I still can't input Chinese in the flatpak version of OBS Studio, Steam, whatever else.  
OS: Ubuntu 18.04.1



### Contributor hadess   on 16 Oct 2018

> Why is it closed, I still can't input Chinese in the flatpak version of OBS Studio, Steam, whatever else.

It's closed because the IBus support got mrged. If you have further problems, file a bug. I'm guessing that in this case, you should file an issue against your distribution, as IBus input works on other distributions.



### jeffshee   on 8 Nov 2018

Having the same issue as well, on Fedora 29.  
Filed a bug to Red Hat Bugzilla already.  
[https://bugzilla.redhat.com/show_bug.cgi?id=1647627](https://bugzilla.redhat.com/show_bug.cgi?id=1647627)



### Let's move discussion on the Electron apps issue to
#### juhp   on 8 Nov 2018
Let's move discussion on the Electron apps issue to
[[electron apps do not recognize ibus 1671]] closed
[[Cant use ibus on typora 2976]]Closed

### flatpak packaging
[[Feature Request Packaging IBus engines with flatpak 3019]] opened


###  Bugfix of snapd 
- [jdstrand](https://github.com/jdstrand) pushed a commit to jdstrand/snapd that referenced this issue [on 20 Aug 2020](https://github.com/flatpak/flatpak/issues/675#ref-commit-8a16d03)  [ interfaces/desktop: allow access to the IBus portal (LP: #1881232)](https://github.com/jdstrand/snapd/commit/8a16d03a22cd1ad2861f89e9f77c6f59296a5e5c)
  - [8a16d03](https://github.com/jdstrand/snapd/commit/8a16d03a22cd1ad2861f89e9f77c6f59296a5e5c)
"interfaces/desktop: allow access to the IBus portal (LP: #1881232) The IBus portal restricts access to IBusContexts to their owner: when using using CreateInputContext() from the org.freedesktop.IBus.Portal DBus interface, the owner of the portal context is tracked such that any other DBus API accesses to IBus require a matching context. Furthermore, IBusContext signals are sent to their context owner (as opposed to broadcast to all listeners). Lastly, IBus's non-portal communications use a separate socket connection from the insecure API. All combined, this is designed to prevent snaps interfering with each other or the session. References: 
https://launchpad.net/bugs/1881232
https://github.com/flatpak/flatpak/issues/675 
https://github.com/ibus/ibus/blob/master/portal/portal.c#L354-L370 
https://github.com/ibus/ibus/blob/master/portal/portal.c#L408-L414") …

~~~diff
    @@ -253,6 +253,20 @@ deny /var/lib/snapd/desktop/icons/{,**/} r,we have better XDG_DATA_DIRS handling, silence these noisy denials.
    # https://github.com/snapcrafters/discord/issues/23#issuecomment-637607843
     deny @{HOME}/.local/share/flatpak/exports/share/** r,#
    +
    +# Allow access to the IBus portal (IBUS_USE_PORTAL=1)
    + dbus (send)
    +       bus=session
    +       path=/org/freedesktop/IBus
    +       interface=org.freedesktop.IBus.Portal
    +       member=CreateInputContext
    +       peer=(name=org.freedesktop.portal.IBus),
    +
    + dbus (send, receive)
    +       bus=session
    +       path=/org/freedesktop/IBus/InputContext_[0-9]*
    +       interface=org.freedesktop.IBus.InputContext
    +       peer=(label=unconfined),
    `

    type desktopInterface struct {
~~~

- [jdstrand](https://github.com/jdstrand) pushed a commit to jdstrand/snapd that referenced this issue [on 20 Aug 2020](https://github.com/flatpak/flatpak/issues/675#ref-commit-1a9f48f)[interfaces/desktop: allow access to the IBus portal (LP: #1881232)](https://github.com/jdstrand/snapd/commit/1a9f48fdfa48f5490bd31fd53ca2bf4d3b802ce9)
  - [1a9f48f](https://github.com/jdstrand/snapd/commit/1a9f48fdfa48f5490bd31fd53ca2bf4d3b802ce9)

    interfaces/desktop: allow access to the IBus portal (LP: #1881232) The IBus portal restricts access to IBusContexts to their owner: when using using CreateInputContext() from the org.freedesktop.IBus.Portal DBus interface, the owner of the portal context is tracked such that any other DBus API accesses to IBus require a matching context. Furthermore, IBusContext signals are sent to their context owner (as opposed to broadcast to all listeners). Lastly, IBus's non-portal communications use a separate socket connection from the insecure API. All combined, this is designed to prevent snaps interfering with each other or the session. References: 
    https://launchpad.net/bugs/1881232
    https://github.com/flatpak/flatpak/issues/675 
    https://github.com/ibus/ibus/blob/master/portal/portal.c#L354-L370 
    https://github.com/ibus/ibus/blob/master/portal/portal.c#L408-L414") …

- [jdstrand](https://github.com/jdstrand) pushed a commit to jdstrand/snapd that referenced this issue [on 21 Aug 2020](https://github.com/flatpak/flatpak/issues/675#ref-commit-6114792)[interfaces/desktop: allow access to the IBus portal (LP: #1881232)](https://github.com/jdstrand/snapd/commit/6114792bc32f76686e21c587cc403f7038ec2194)  
  - [6114792](https://github.com/jdstrand/snapd/commit/6114792bc32f76686e21c587cc403f7038ec2194)
    
    interfaces/desktop: allow access to the IBus portal (LP: #1881232) The IBus portal restricts access to IBusContexts to their owner: when using using CreateInputContext() from the org.freedesktop.IBus.Portal DBus interface, the owner of the portal context is tracked such that any other DBus API accesses to IBus require a matching context. Furthermore, IBusContext signals are sent to their context owner (as opposed to broadcast to all listeners). Lastly, IBus's non-portal communications use a separate socket connection from the insecure API. All combined, this is designed to prevent snaps interfering with each other or the session. References: 
    https://launchpad.net/bugs/1881232 
    https://github.com/flatpak/flatpak/issues/675 
    https://github.com/ibus/ibus/blob/master/portal/portal.c#L354-L370 
    https://github.com/ibus/ibus/blob/master/portal/portal.c#L408-L414") …

### luongthanhlam   on 25 Sep 2020
[Không gõ được trên wps office BambooEngine/ibus-bamboo#153](https://github.com/BambooEngine/ibus-bamboo/issues/153)Closed





