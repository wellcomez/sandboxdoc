---
layout: post
tags:
- snapd
- fcitx
title: "Stage Fcitx frontend 156"

---

# Stage Fcitx frontend 156

[ubuntu](https://github.com/ubuntu)/[snapcraft-desktop-helpers](https://github.com/ubuntu/snapcraft-desktop-helpers)Public

[Stage Fcitx frontend #156](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156)

 Merged

[oSoMoN](https://github.com/oSoMoN) merged 1 commit into [ubuntu:master](https://github.com/ubuntu/snapcraft-desktop-helpers "ubuntu/snapcraft-desktop-helpers:master") from [brlin-tw:patch-fix-fcitx-support](https://github.com/brlin-tw/snapcraft-desktop-helpers/tree/patch-fix-fcitx-support "brlin-tw/snapcraft-desktop-helpers:patch-fix-fcitx-support") on 31 Oct 2018

+1 −0 

 [Conversation 8](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156) [Commits 1](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/commits) [Checks 0](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/checks) [Files changed 1](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/files)

## Conversation


### Contributor brlin-tw on 26 Sep 2018

This allows Fcitx input method framework to be used in Gtk3 snapped apps

Signed-off-by: 林博仁(Buo-ren Lin) [Buo.Ren.Lin@gmail.com](mailto:Buo.Ren.Lin@gmail.com)



[gtk3: Stage Fcitx frontend](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/commits/50aa8289391e1ea1bc7d4e064576c0e1eda55013 "gtk3: Stage Fcitx frontend This allows Fcitx input method framework to be used in Gtk3 snapped apps Signed-off-by: 林博仁(Buo-ren Lin) <Buo.Ren.Lin@gmail.com>") …

[50aa828](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/commits/50aa8289391e1ea1bc7d4e064576c0e1eda55013)

[brlin-tw](https://github.com/brlin-tw) [force-pushed](https://github.com/ubuntu/snapcraft-desktop-helpers/compare/35fc6a363b94e5d43eb712a87098f37fe081c5c8..50aa8289391e1ea1bc7d4e064576c0e1eda55013)
the patch-fix-fcitx-support branch from [`35fc6a3`](https://github.com/ubuntu/snapcraft-desktop-helpers/commit/35fc6a363b94e5d43eb712a87098f37fe081c5c8) to [`50aa828`](https://github.com/ubuntu/snapcraft-desktop-helpers/commit/50aa8289391e1ea1bc7d4e064576c0e1eda55013) [Compare](https://github.com/ubuntu/snapcraft-desktop-helpers/compare/35fc6a363b94e5d43eb712a87098f37fe081c5c8..50aa8289391e1ea1bc7d4e064576c0e1eda55013)[4 years ago](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156#event-1867157531)

[kenvandine](https://github.com/kenvandine) assigned [oSoMoN](https://github.com/oSoMoN) [on 26 Sep 2018](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156#event-1867192369)


oSoMoN reviewed [on 5 Oct 2018](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156#pullrequestreview-162031675)[View changes](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156/files/50aa8289391e1ea1bc7d4e064576c0e1eda55013)

### Member oSoMoN left a comment

This makes sense.

Have you tested that this is enough to make fcitx input work in gtk3 app snaps, or is this only a prerequisite?

Wouldn't other stage packages (libfcitx-gclient1, libfcitx-utils0) be needed for this to actually work?

And if this works we would probably want to add the corresponding packages to other desktop parts (fcitx-frontend-gtk2, fcitx-frontend-qt4, fcitx-frontend-qt5).


### Contributor Author brlin-tw on 5 Oct 2018

> Have you tested that this is enough to make fcitx input work in gtk3 app snaps, or is this only a prerequisite?

I've copied the entire part definition with this addition to one of my snap and built a sample, it works.

> And if this works we would probably want to add the corresponding packages to other desktop parts (fcitx-frontend-gtk2, fcitx-frontend-qt4, fcitx-frontend-qt5).

I agree.



### Member oSoMoN on 31 Oct 2018

I finally got around to testing this, and I'm delighted to report that this works very nicely, no additional work needed. I successfully rebuilt the chromium snap with this additional stage package, and fcitx input (google-pinyin) just worked.

Note that snaps that connect to the gnome-3-26-1604 platform snap won't need this, for them fcitx input already works out of the box.

I also tested adding fcitx-frontend-qt5 to a qt5-based snap app (musescore), and this works well too.



 [oSoMoN](https://github.com/oSoMoN) merged commit [`5a5ab9f`](https://github.com/ubuntu/snapcraft-desktop-helpers/commit/5a5ab9f20c8825c81692db6dced75ec9ed8a8820) into ubuntu:master [on 31 Oct 2018](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156#event-1935567659)



### Member oSoMoN on 31 Oct 2018

And as a follow-up I committed [a264e8f](https://github.com/ubuntu/snapcraft-desktop-helpers/commit/a264e8f692624ec689887cda6358dd586ecf0e4a) to add support for fcitx in qt5-based snaps.



 [brlin-tw](https://github.com/brlin-tw) deleted the patch-fix-fcitx-support branch [4 years ago](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156#event-1935622224)



### 8none1 on 31 Oct 2018

Could someone test with Sogou and report back. That should be a good proof.



Contributor### Author brlin-tw on 1 Nov 2018

> Could someone test with Sogou and report back. That should be a good proof.

What is Sogou exactly? I only heard of a Chinese company with a similar name.



### 8none1 on 1 Nov 2018

[https://pinyin.sogou.com/linux/?r=pinyin](https://pinyin.sogou.com/linux/?r=pinyin)





### Member oSoMoN on 7 Nov 2018

> Could someone test with Sogou and report back. That should be a good proof.

I confirm this works in a bionic VM. I grabbed the 64bits deb from the sogou website, installed it and its dependencies, and successfully used it in various gnome app snaps that connect to the gnome-3-26-1604 platform snap.




 **ProTip!** Add [.patch](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156.patch) or [.diff](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156.diff) to the end of URLs for Git’s plaintext views.



