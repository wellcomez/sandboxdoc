---
layout: post
tags:
- firejail
- ibus
title: "Keyboard no longer working in gedit NO.5100"

---

# Keyboard no longer working in gedit NO.5100
[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public
[Keyboard no longer working in gedit](https://github.com/netblue30/firejail/issues/5100#top)#5100 Open

### Contributor jose1711 on 10 Apr

### Description

Keyboard input is no longer working in `gedit`.

### Steps to Reproduce

_Steps to reproduce the behavior_

1.  Run in bash `LC_ALL=C firejail gedit`
2.  Try to type anything

### Expected behavior

Letters appear on the screen.

### Actual behavior

_What actually happened_

Nothing changes

_What changed calling `LC_ALL=C firejail --noprofile /path/to/program` in a terminal?_

Same behaviour as above.

### Additional context

It used to work in the past. Seems related to system update.

### Environment

-   Linux distribution and version: Arch Linux x86_64
-   Firejail version (`firejail --version`): 0.9.69
-   If you use a development version of firejail, also the commit from which it was compiled: r8330.edbecfb67-1

### Checklist

-    The issues is caused by firejail (i.e. running the program by path (e.g. `/usr/bin/vlc`) "fixes" it).
-    I can reproduce the issue without custom modifications (e.g. globals.local).
-    The program has a profile. (If not, request one in `https://github.com/netblue30/firejail/issues/1139`)
-    The profile (and redirect profile if exists) hasn't already been fixed [upstream](https://github.com/netblue30/firejail/tree/master/etc).
-    I have performed a short search for similar issues (to avoid opening a duplicate).
    -    I'm aware of `browser-allow-drm yes`/`browser-disable-u2f no` in `firejail.config` to allow DRM/U2F in browsers.
-    I used `--profile=PROFILENAME` to set the right profile. (Only relevant for AppImages)



###  rusty-snake on 11 Apr

Do you use IBus?



Contributor### Author jose1711 on 11 Apr

> Do you use IBus?

Hmm.. the `ibus-daemon` process is present so my guess is that I am using it..



###  rusty-snake on 11 Apr

But is gedit (gtk/gdk) configured to use it?

Does `GTK_IM_MODULE=xim` or `GTK_IM_MODULE=ibus` make a difference?

Do you have any special input module like typing-booster?

Does it work with noprofile.profile?



Contributor### Author jose1711 on 11 Apr

With `GTK_IM_MODULE=xim` the input works, if set to `ibus` it does not.

```shell
LANG=C GTK_IM_MODULE=ibus gedit
Reading profile /etc/firejail/gedit.profile
Reading profile /etc/firejail/allow-common-devel.inc
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-exec.inc
Reading profile /etc/firejail/disable-programs.inc
Reading profile /etc/firejail/whitelist-runuser-common.inc
Reading profile /etc/firejail/whitelist-var-common.inc
Parent pid 73627, child pid 73628
Warning: /sbin directory link was not blacklisted
Warning: /usr/sbin directory link was not blacklisted
Warning: not remounting /home/jose/.ssh/authorized_keys2
Warning: not remounting /home/jose/.ssh/config
Blacklist violations are logged to syslog
Warning: cleaning all supplementary groups
Child process initialized in 92.12 ms

(gedit:3): dbind-WARNING **: 12:20:24.984: Couldn't connect to accessibility bus: Failed to connect to socket /run/user/1000/at-spi/bus_0: No such file or directory

(gedit:3): IBUS-WARNING **: 12:20:25.071: Unable to connect to ibus: Could not connect: Connection refused

Parent is shutting down, bye...
```



###  rusty-snake on 12 Apr

Duplicate of [#116](https://github.com/netblue30/firejail/issues/116)

 [rusty-snake](https://github.com/rusty-snake) marked this as a duplicate of [#116](https://github.com/netblue30/firejail/issues/116 "Unable to use ibus-daemon in firejail") [on 12 Apr](https://github.com/netblue30/firejail/issues/5100#event-6410443833)



###  rusty-snake on 12 Apr

but we should really improve the documentation or add a `allow-ibus.inc` or an `ibus yesno` in firejail.config or just hardcoded the fix if possible.



 [rusty-snake](https://github.com/rusty-snake) added this to **To do** in [Release 0.9.72](https://github.com/netblue30/firejail/projects/6#card-82957618) via automation [on 8 Jun](https://github.com/netblue30/firejail/issues/5100#event-6769194921)
