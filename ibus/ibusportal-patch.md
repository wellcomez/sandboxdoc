---
title: "ibusportal-patch"
layout: post
---

# flatpak/freedesktop-sdk-images › ibus-portal.patch  1.6
https://sourcegraph.com/github.com/flatpak/freedesktop-sdk-images/-/blob/ibus-portal.patch?subtree=true

## ibus

### 最终提交 
`fujiwarat   on 25 Aug 2017`

[@alexlarsson](https://github.com/alexlarsson) Thank you for your quick review.  
I will start the code review of the patches on behalf of you CC'ing you next week. Of course, yourself also can start it with `cd ibus; git log codereview.settings`.  
I will adjust 80 columns for every line to follow the ibus coding style.

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 30 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-203a3df)
[Initial version of ibus portal](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7)Initial version of ibus portal This adds a dbus service called org.freedesktop.portal.IBus on the session bus. It is a very limited service that only implements CreateInputContext and the InputContext interface (and Service.Destroy for lifetime access). It uses gdbus code generation for demarshalling the method calls which means it will verify that all arguments have the right type. Additionally all method calls to the input context object have to be from the client that created it, so each client is isolated. BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com Review URL: https://codereview.appspot.com/326350043 Patch from Alexander Larsson <alexl@redhat.com>[203a3df](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7)

[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 31 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-35ce624)[Support the portal in the gtk im modules](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae "Support the portal in the gtk im modules This adds a new way to create an IbusBus, ibus_bus_new_async_client(). This returns an object that is not guarantee to handle any calls that are not needed by a client, meaning CreateInputContext and handling the input context. If you are running in a flatpak, or if IBUS_USE_PORTAL is set, then instead of talking to the regular ibus bus we connect to org.freedesktop.portal.IBus on the session bus and use the limited org.freedesktop.IBus.Portal interface instead of the org.freedesktop.IBus interface. This allows flatpaks (or other sandbox systems) to safely use dbus clients (apps). BUG=https://github.com/flatpak/flatpak/issues/675 Review URL: https://codereview.appspot.com/328410043 Patch from Alexander Larsson <alexl@redhat.com>.[35ce624](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae)


### gtk3 im-module into the freedesktop runtime 
`alexlarsson   on 2 Sep 2017`

I built the gtk3 im-module into the freedesktop runtime now. I guess we should add the gtk2 one to the gnome runtime also.



#### fujiwarat   on 4 Sep 2017

> I built the gtk3 im-module into the freedesktop runtime now. I guess we should add the gtk2 one to the gnome runtime also.

Yes. Also `/usr/libexec/ibus-x11` and `/usr/lib64/qt5/plugins/platforminputcontexts/libibusplatforminputcontextplugin.so` too.
[fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 4 Sep 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-9772e80)[test: Testing in flatpak](https://github.com/ibus/ibus/commit/9772e800f3e6937510f2609c5ce9a6860c59cb81 "test: Testing in flatpak Test with: flatpak-builder --force-clean app org.test.IBus.json flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com Review URL: https://codereview.appspot.com/329090043 Patch from Alexander Larsson <alexl@redhat.com> …
- [9772e80](https://github.com/ibus/ibus/commit/9772e800f3e6937510f2609c5ce9a6860c59cb81)`
[fujiwarat](https://github.com/fujiwarat) added a commit to ibus/ibus that referenced this issue [on 4 Sep 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-9937a0e)[bus: ibus-daemon activates ibus-portal](https://github.com/ibus/ibus/commit/9937a0e4501ccf0cfd238ce7c97733c3099db3f7)bus: ibus-daemon activates ibus-portal When ibus-daemon restarts, ibus-portal exits with on_name_lost() and the clients wait for portal_name_appeared() until ibus-poral restarts. Now the clients can connect to ibus-daemon with this way and also they don't have to activate ibus-portal. BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com Review URL: https://codereview.appspot.com/321530043 …[9937a0e](https://github.com/ibus/ibus/commit/9937a0e4501ccf0cfd238ce7c97733c3099db3f7)


## Qt5

1.  [LeeKyuHyuk/QT-Windows-Static-Build](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build) › [qtbase/src/plugins/platforminputcontexts/ibus/interfaces/org.freedesktop.IBus.Portal.xml](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build/-/blob/qtbase/src/plugins/platforminputcontexts/ibus/interfaces/org.freedesktop.IBus.Portal.xml?subtree=true)
``` xml
    <node>  
        <interface name="org.freedesktop.IBus.Portal">  
            <method name="CreateInputContext">             
    
``` 
2.  [LeeKyuHyuk/QT-Windows-Static-Build](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build) › [qtbase/src/plugins/platforminputcontexts/ibus/qibusproxyportal.h](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build/-/blob/qtbase/src/plugins/platforminputcontexts/ibus/qibusproxyportal.h?subtree=true)
    
    ```cpp 
    * This file was generated by qdbusxml2cpp version 0.8  * Command line was: qdbusxml2cpp -N -p qibusproxyportal -c QIBusProxyPortal interfaces/org.freedesktop.IBus.Portal.xml  *             
        
        
    /*  * Proxy class for interface org.freedesktop.IBus.Portal  */             
        
        
        
    static inline const char *staticInterfaceName()  { return "org.freedesktop.IBus.Portal"; }         
    ```
    
    
3.  [LeeKyuHyuk/QT-Windows-Static-Build](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build) › [qtbase/src/plugins/platforminputcontexts/ibus/qibusproxyportal.cpp](https://sourcegraph.com/github.com/LeeKyuHyuk/QT-Windows-Static-Build/-/blob/qtbase/src/plugins/platforminputcontexts/ibus/qibusproxyportal.cpp?subtree=true)

```shell
* This file was generated by qdbusxml2cpp version 0.8  
* Command line was: qdbusxml2cpp -N -p qibusproxyportal -c QIBusProxyPortal interfaces/org.freedesktop.IBus.Portal.xml  *             
    
```
4.  [bitwiseworks/qtbase-os2](https://sourcegraph.com/github.com/bitwiseworks/qtbase-os2) › [src/plugins/platforminputcontexts/ibus/interfaces/org.freedesktop.IBus.Portal.xml](https://sourcegraph.com/github.com/bitwiseworks/qtbase-os2/-/blob/src/plugins/platforminputcontexts/ibus/interfaces/org.freedesktop.IBus.Portal.xml?subtree=true)
    
```xml
<node>  <interface name="org.freedesktop.IBus.Portal">  
<method name="CreateInputContext">             
```

## snapcore/snapd
[snapcore/snapd](https://sourcegraph.com/github.com/snapcore/snapd) › [interfaces/builtin/desktop.go](https://sourcegraph.com/github.com/snapcore/snapd/-/blob/interfaces/builtin/desktop.go?subtree=true)

```go
path=/org/freedesktop/IBus  
interface=org.freedesktop.IBus.Portal  
member=CreateInputContext            
```


## fcitx
[fcitx/fcitx5](https://sourcegraph.com/github.com/fcitx/fcitx5) › [src/frontend/ibusfrontend/ibusfrontend.cpp](https://sourcegraph.com/github.com/fcitx/fcitx5/-/blob/src/frontend/ibusfrontend/ibusfrontend.cpp?subtree=true)


```c 
#define IBUS_PORTAL_DBUS_SERVICE "org.freedesktop.portal.IBus"  
#define IBUS_PORTAL_DBUS_INTERFACE "org.freedesktop.IBus.Portal"    
```



## snapd
[interfaces: add desktop and desktop-legacy snapcore/snapd#3719](https://github.com/snapcore/snapd/pull/3719)
[[interfaces-add desktop and desktop-legacy 3719]]




