---
layout: post
tags:
- fcitx
- flatpak
title: "Is it possible to package fcitx engines with flatpak NO.108"

---

[fcitx](https://github.com/fcitx)/[fcitx5](https://github.com/fcitx/fcitx5)Public
[Is it possible to package fcitx engines with flatpak ? #108](https://github.com/fcitx/fcitx5/issues/108)Closed

[subins2000](https://github.com/subins2000) opened this issue on 29 Aug 2020 · 17 comments



[subins2000](https://github.com/subins2000) opened this issue on 29 Aug 2020 · 17 comments 




### subins2000 on 29 Aug 2020

Hi

I would like to know if it is possible to package fcitx input engines with flatpak and distribute easily to users. I've understood `ibus` engines can't interact with host ibus from flatpak sandboxed environment : [flatpak/flatpak#3019](https://github.com/flatpak/flatpak/issues/3019)[[Feature Request Packaging IBus engines with flatpak 3019]]

Distribution with flatpak will help ship fcitx engines easily



### Member wengxt on 29 Aug 2020

I think:  
Use fcitx + flatpak app is possible.  
Use flatpak fcitx + flatpak fcitx engine + flatpak/host app is possible.  
Use host fcitx + flatpak fcitx engine would never be possible (at least due to inconsistency in version).

There's another people already did this to package fcitx into a flatpak package. FYI: [#99](https://github.com/fcitx/fcitx5/issues/99)[[sd-bus D-Bus authentication is broken when proxied via xdg-dbus-proxy no.99]]

<mark class="hltr-green">One option is to package fcitx and all engine together into one big package. I think that's the case in the issue above (his issue is related to a xdg-dbus-proxy bug). So for now you need to -DUSE_SYSTEMD=Off when building fcitx5.</mark>

But I noticed that flatpak is able to have things like GL platform to work like a plugin to another flatpak package, I think it's might possible to even pack fcitx engine separately.



### Author subins2000 on 1 Sep 2020

> One option is to package fcitx and all engine together into one big package.

Interesting, how would the config be then ? Like this ?

```
export QT_IM_MODULE=flatpak run com.fcitx
```

> I think it's might possible to even pack fcitx engine separately.

I would like to test this, which engine would you recommend for a newbie (to fcitx) to test packaging ? (I can only read English and Malayalam btw)



### Member wengxt on 1 Sep 2020

im module is irrelevant to the server. To sum it up:

Depending on the actual protocol (Currently the most usable on is im module coming from input method project)  
IM Module (fcitx5-qt, fcitx5-gtk) need to be loaded by the application. Which means native app need to have it installed to use fcitx. flatpak app's runtime need to include IM Module to use fcitx (which already happens).

Application load im module as plugin, then send data to fcitx server by some protocol (dbus, X11, wayland etc), fcitx load "engine" as plugin, process the data, and return by to the application.

<mark class="hltr-red">So as you can see, "engines" (fcitx5-anthy, fcitx5-kkc, fcitx5-chinese-addons etc) are the plugin to fcitx. im module is the plugin to the actual application. All of these "plugins" are based on shared library which means they need to be on the same platform.</mark>

As for package the library separately, you need to investigate how to load the plugin from another flatpak package. Maybe try fcitx5-m17n first based on the language you know. I think this is what you need : [https://github.com/flatpak/flatpak/wiki/Extensions](https://github.com/flatpak/flatpak/wiki/Extensions)



### Contributor tinywrkb on 13 Oct 2020

[@subins2000](https://github.com/subins2000) have a look [here](https://github.com/tinywrkb/flatpaks/tree/master/org.fcitx_im.fcitx5). This is pretty working correctly as far as I can tell, except:

-   ~~Mozc is completely broken, though I experienced the same issue on my system even with the distro package.~~ **fixed**
-   Not all the icons in the system tray appears correctly. This is due to a flatpak-builder requirement for exported icon file naming. It can be worked around by patching Fcitx, its engines, and renaming the icons, something that I already did but I most probably missed some icons.

<mark class="hltr-red">Probably the main issue is that the Flatpak runtime will still be stuck with the same Fcits widget toolkit IME modules. Maybe this doesn't matter if Fcitx keeps API compatibility but it would have been better if the widget toolkit IME modules would have been Flatpak runtime extensions that could be updated separately from the runtime, maybe even allow the user to choose the IME module version, something like [what is possible with Mesa](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/wikis/Mesa-git).  </mark>
You can actually get similar behavior by using filesystem override to give Flatpak apps access to the toolkit widget IME module from the Fcitx5 Flaptak app and an environment variable, e.g. `GTK_IM_MODULE_FILE`.

I don't really keep my Flatpak manifests for Fcitx5 and its IME engines updated. Maybe when there will be a stable release.  
I'm not a heavy user of Fcitx, I just slowly learning Chinese so I thought it would be useful.

p.s. I'm also trying to package IBus but something doesn't really work and I haven't debugged this yet. The WIP is in my `ibus` branch.





### Member wengxt on 13 Oct 2020

<mark class="hltr-grey">mozc need to start a separate process mozc_server. It's likely that the path is not correctly set during the build time. --server_dir need to be passed to the build_mozc.py script.</mark>

<mark class="hltr-grey">The as of im module part, latest fcitx4 im module is compatible with fcitx5. (in the sense of dbus interface). Any combination of fcitx 4 or 5 server can be combined with fcitx 4 / 5 im module. (latest fcitx 4 required)
</mark>
<mark class="hltr-grey">And GTK_IM_MODULE can be simply set to fcitx, no need to set to fcitx5</mark>.



### Contributor tinywrkb on 13 Oct 2020

[@wengxt](https://github.com/wengxt) Thank you for the detailed answer.

I'll look again later at Mozc and try your suggestion.

Note that I mentioned `GTK_IM_MODULE_FILE` not `GTK_IM_MODULE` and by that, I meant doing something like this:

```shell
# enter to a new gedit app sandbox session and give filesystem permission (read-only) to access the path where Flatpak applications are installed, specifically where Fcitx5 is installed
flatpak run --command=sh --filesystem=~/.local/share/flatpak:ro org.gnome.gedit

# in the sandbox session let's generate immodules.cache
export LD_LIBRARY_PATH=$HOME/.local/share/flatpak/app/org.fcitx_im.fcitx5/current/active/files/lib
export GTK_PATH=$HOME/.local/share/flatpak/app/org.fcitx_im.fcitx5/current/active/files/lib/gtk-3.0/3.0.0
gtk-query-immodules-3.0 > downloads/tmp/immodules.cache
export GTK_IM_MODULE_FILE=$HOME/downloads/tmp/immodules.cache
gedit &
```

And after we have `immodules.cache` in hand we just need to set global Flatpak overrides with the filesystem permission, the environment variables, and then we can start all our Flatpak apps as usual and the updated IME module will just be picked up.



### Contributor tinywrkb on 14 Oct 2020

[@wengxt](https://github.com/wengxt) setting `--server_dir` fixed the issue with Mozc and it's now working correctly. Thanks again ;)



 [wengxt](https://github.com/wengxt) added this to **In progress** in [Fcitx 5](https://github.com/orgs/fcitx/projects/1#card-47400584) [on 15 Oct 2020](https://github.com/fcitx/fcitx5/issues/108#event-3879418172)



### Member wengxt on 15 Oct 2020

[@tinywrkb](https://github.com/tinywrkb) if you can have any changes that you'd like to be put upstream, feel free to send pull request.



### Member wengxt on 18 Nov 2020

I close this for now. You may reopen it if you need any upstream change to make it be able to be packed in flatpak.

 [wengxt](https://github.com/wengxt) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 18 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4007189964)

[Fcitx 5](https://github.com/orgs/fcitx/projects/1) automation moved this from **In progress** to **Done** [on 18 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4007190014)



### Contributor tinywrkb on 18 Nov 2020

[@wengxt](https://github.com/wengxt) sorry for ignoring this, I'll look at this later this week when I'll update the packaging.



### Member wengxt on 21 Nov 2020

[@tinywrkb](https://github.com/tinywrkb) I checked your packing implementation and found it's actually too hacky. Fcitx's built-in mechanism is already good enough to handle those directories. There's no need to use merge-dirs.

I start to working on my own implementation for it.

 [wengxt](https://github.com/wengxt) reopened this [on 21 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4023422264)



### Contributor tinywrkb on 21 Nov 2020

Yes, it's messy. My package was just a PoC to see if this can actually work in a Flatpak.

 [wengxt](https://github.com/wengxt) moved this from **Done** to **In progress** in [Fcitx 5](https://github.com/orgs/fcitx/projects/1#card-47400584) [on 26 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4039431408)



### Member wengxt on 26 Nov 2020

[https://github.com/fcitx/flatpak-fcitx5](https://github.com/fcitx/flatpak-fcitx5)

 [wengxt](https://github.com/wengxt) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 26 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4040657593)

[Fcitx 5](https://github.com/orgs/fcitx/projects/1) automation moved this from **In progress** to **Done** [on 26 Nov 2020](https://github.com/fcitx/fcitx5/issues/108#event-4040657620)



### Contributor tinywrkb on 27 Nov 2020

[@wengxt](https://github.com/wengxt) very cool and looks extremely tidy.  
Did you test against StatusNotifierItem D-Bus protocol and not only the anachronistic XEmbed protocol? You gonna need to handle the icon exports to have them shown correctly in the system tray.



### Member wengxt on 27 Nov 2020

> [@wengxt](https://github.com/wengxt) very cool and looks extremely tidy.  
> Did you test against StatusNotifierItem D-Bus protocol and not only the anachronistic XEmbed protocol? You gonna need to handle the icon exports to have them shown correctly in the system tray.

To be honest because my KDE's default icon theme provides fcitx icon, I didn't realize this :D. I thought that xembed icon will loaded by fcitx so it's fine, but indeed if goes by SNI the icon need to be loadable by outside. I'm not yet so sure about the icon renaming stuff because this would break others icon theme (because of the name inconsistency).



### Member wengxt on 1 Dec 2020

[@tinywrkb](https://github.com/tinywrkb) actually I just found it doesn't yet work because extension can't export icons: [flatpak/flatpak#4006](https://github.com/flatpak/flatpak/issues/4006)



### Contributor tinywrkb on 1 Dec 2020

[@wengxt](https://github.com/wengxt) this is why I'm first installing locally all the extensions, [copying the icons to the resources dir in the git repo](https://github.com/tinywrkb/flatpaks/blob/20a8de75eab5f7db5b2061aa46f180d8f692e626/org.fcitx_im.fcitx5/copy_icons), and then rebuild the app now with [icons from all the extensions](https://github.com/tinywrkb/flatpaks/blob/20a8de75eab5f7db5b2061aa46f180d8f692e626/org.fcitx_im.fcitx5/install_icons).

 [teohhanhui](https://github.com/teohhanhui) mentioned this issue [on 9 Jun 2021](https://github.com/fcitx/fcitx5/issues/108#ref-issue-915370829)

[Flatpak-only setup? #287](https://github.com/fcitx/fcitx5/issues/287)Closed


