---
layout: post
tags:
- flathub
- qt
title: "Please update the qtbase to 5.11.2 or later in flatpak runtimes 2273"

---

# Please update the qtbase to 5.11.2 or later in flatpak runtimes 2273
[flatpak](https://github.com/flatpak)[flatpak](https://github.com/flatpak/flatpak)


[Please update the qtbase to 5.11.2 or later in flatpak runtimes #2273](https://github.com/flatpak/flatpak/issues/2273#top)Closed

[chenzhiwei](https://github.com/chenzhiwei) opened this issue on 24 Oct 2018 · 2 comments


### chenzhiwei on 24 Oct 2018

#### Linux distribution and version

Ubuntu 18.10

#### Flatpak version

v1.0.4

#### Description of the problem

Can't use ibus IME in flatpak apps. This issue was fixed in QT base 5.11.2 or later, for the investigation discussion please check from here: [telegramdesktop/tdesktop#4449 (comment)](https://github.com/telegramdesktop/tdesktop/issues/4449#issuecomment-426842715)

so could you please update following runtimes with the newer QT base version. The affected runtimes are:

-   org.freedesktop.Platform
-   org.gnome.Platform

#### Steps to reproduce

1.  Install Ubuntu 18.10 OS
2.  Install and enable ibus
3.  Install com.slack.Slack
4.  Open Slack and try to input characters with ibus IME



Member

### TingPing on 25 Oct 2018

KDE runtime bugs go here: [https://bugs.kde.org/](https://bugs.kde.org/)





### juhp on 9 Nov 2018

(Ah I see: [https://github.com/KDE/flatpak-kde-runtime](https://github.com/KDE/flatpak-kde-runtime))



