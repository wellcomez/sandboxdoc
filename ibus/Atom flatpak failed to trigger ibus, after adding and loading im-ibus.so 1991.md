---
layout: post
tags:
- ibus
- flatpak
- electron
- BaseApp
- gtk2
title: "Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so
  1991"

---

# Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so

[ibus](https://github.com/ibus)[ibus](https://github.com/ibus/ibus)
[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so](https://github.com/ibus/ibus/issues/1991#top)#1991 Closed

[cheese](https://github.com/cheese) opened this issue on 22 Mar 2018 · 10 comments


### cheese on 22 Mar 2018

Please fill in the following items if you don't know the root cause.

Which distribution and version?:  
Fedora 26.  
Flatpak Atom based on source from flathub. io.atom.electron.BaseApp patched with

```diff
diff --git a/io.atom.electron.BaseApp.json b/io.atom.electron.BaseApp.json
index cea28b0..1766256 100644
--- a/io.atom.electron.BaseApp.json
+++ b/io.atom.electron.BaseApp.json
@@ -23,7 +23,6 @@
         {
             "name": "gtk2",
             "cleanup": [
-              "/bin",
               "/share/gtk-2.0",
               "/lib/gtk-2.0/include"
             ],
@@ -229,6 +228,53 @@
                     "dest-filename": "autogen.sh"
                 }
             ]
+        },
+        {
+            "name": "ibus",
+            "config-opts": ["--disable-xim", "--disable-static", "--disable-dconf", "--disable-schemas-compile", "--disable-unicode-dict",
+                            "--disable-setup", "--disable-ui", "--disable-engine", "--disable-libnotify", "--disable-emoji-dict",
+                            "--disable-appindicator", "--disable-tests"],
+            "cleanup": [
+                "/bin", "/libexec", "/share/bash-completion", "/share/dbus-1",
+                "/share/icons", "/share/man", "/share/ibus", "/lib/gtk-3.0" ],
+            "post-install": [ "gtk-query-immodules-2.0 --update-cache" ],
+
+            "sources": [
+                {
+                    "type": "archive",
+                    "url": "https://github.com/ibus/ibus/releases/download/1.5.18/ibus-1.5.18.tar.gz",
+                    "sha256": "8551f7d027fb65d48225642fc8f3b232412ea75a4eb375244dd72a4d73c2639e"
+                }
+            ]
+        },
+        {
+            "name": "extra-cmake-modules",
+            "cleanup-platform": ["*"],
+            "config-opts": ["-DENABLE_TESTING=OFF", "-DCMAKE_INSTALL_LIBDIR=lib"],
+            "cmake": true,
+            "sources": [
+                {
+                    "type": "archive",
+                    "url": "https://download.kde.org/stable/frameworks/5.39/extra-cmake-modules-5.39.0.tar.xz",
+                    "sha256": "b80536d7bed8c12fc492857d3d433b3859462402fe231e463efcd3bf44ffbcd1"
+                }
+            ]
+        },
+        {
+            "name": "fcitx",
+            "config-opts": ["-DENABLE_QT=Off", "-DENABLE_PINYIN=Off", "-DENABLE_TABLE=Off", "-DENABLE_GIR=Off", "-DENABLE_GTK3_IM_MODULE=Off",
+                            "-DENABLE_OPENCC=Off", "-DENABLE_CAIRO=Off", "-DENABLE_SPELL=Off", "-DENABLE_GTK2_IM_MODULE=On", "-DENABLE_LIBXML2=Off",
+                            "-DENABLE_X11=Off", "-DENABLE_ENCHANT=Off", "-DENABLE_PRESAGE=Off"],
+            "cleanup": ["/lib/fcitx", "/share", "/bin"],
+            "post-install": [ "gtk-query-immodules-2.0 --update-cache" ],
+            "cmake": true,
+            "sources": [
+                {
+                    "type": "archive",
+                    "url": "https://download.fcitx-im.org/fcitx/fcitx-4.2.9.5.tar.xz",
+                    "sha256": "9479cef9b66e53df0e874637d55759ec08a195cb8790513e133135d8e49ad911"
+                }
+            ]
         }
     ]
 }
```

#### Which desktop environment and version?:  
GNOME 3.24

#### Which session type?:  
X11

#### Which application and version?:  
atom 1.25.0

#### IBus version?:  
Host: ibus-1.5.17-6.fc26.x86_64  
im-ibus.so in flatpak: 1.5.18

#### Issue description:  
The current io.atom.Atom on flathub does not contain a im-ibus.so gtk2 module.  
After I adding the im module with above patch to io.atom.electron.BaseApp, atom now loads im-ibus.so. But it still failed to trigger ibus.  
If set GTK_IM_MODULE to xim, atom in flatpak can trigger ibus.  
If atom installed in the host, atom can trigger ibus.  
If run gtk-demo in the atom flatpak (flatpak run --command=gtk-demo io.atom.Atom), gtk-demo loads im-ibus.so and can trigger ibus.

#### Steps to reproduce:

1.  clone [https://github.com/flathub/io.atom.electron.BaseApp.git](https://github.com/flathub/io.atom.electron.BaseApp.git) [https://github.com/flathub/io.atom.Atom](https://github.com/flathub/io.atom.Atom)
2.  patch io.atom.electron.BaseApp
3.  build and install io.atom.electron.BaseApp
4.  build, install and run io.atom.Atom
5.  try to trigger ibus

#### Can you reproduce your problem when you restart ibus-daemon? (yes / no):  
yes

#### Do you see any errors when you run ibus-daemon with the verbose option?:  
(Run `ibus-daemon --xim --verbose &` and look at the output when you encounter your problem.)  
Not tried.

#### Can you reproduce your problem with a new user account instead of the current your account? (yes / no):  
Not tried.

[fujiwarat](https://github.com/fujiwarat)

Member

### fujiwarat on 23 Mar 2018

You could refer [https://github.com/ibus/ibus/blob/master/test/org.test.IBus.json](https://github.com/ibus/ibus/blob/master/test/org.test.IBus.json)

```
% flatpak-builder --force-clean app org.test.IBus.json
% flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry
```

[fujiwarat](https://github.com/fujiwarat)

Member

### fujiwarat on 23 Mar 2018

> If set GTK_IM_MODULE to xim, atom in flatpak can trigger ibus.

It would not be an expected configuration because the GTK XIM module forwards the keyevents to ibus-x11. we should use GTK_IM_MODULE=ibus

[cheese](https://github.com/cheese)

### Author cheese on 28 Mar 2018

Do you confirm this issue?

[fujiwarat](https://github.com/fujiwarat)

Member

### fujiwarat on 29 Mar 2018

No, I don't.  
What is the Atom?  
<mark class="hltr-pink">If it's an GTK3 application in GNOME, I think it can communicate with /usr/libexec/ibus-portal through the "org.freedesktop.porta</mark>l.IBus" Bus name. So I provided the test application for you if you verify the general applications works fine in Flatpak.  
If it's a desktop environment, it needs to accept the "org.freedesktop.portal.IBus" Bus name.

[cheese](https://github.com/cheese)

### Author cheese on 29 Mar 2018

<mark class="hltr-pink">Atom is [https://atom.io/](https://atom.io/) . It is based on electron that uses gtk2.  
As I said, the 'gtk-demo' application works but not for Atom.</mark>

[fujiwarat](https://github.com/fujiwarat)

Member

### fujiwarat on 29 Mar 2018

I checked out [https://github.com/atom/atom.git](https://github.com/atom/atom.git) but I cannot grep gtk.

[cheese](https://github.com/cheese)

### Author cheese on 7 Jul 2018

It seems we need to investigate how to make Chromium to support IBus portal. Atom is based Electron framework, which is based on Chromium.

[cheese](https://github.com/cheese)

### Author cheese on 26 Jul 2018

I tested Fcitx for the same case and there is no such issue.

[fujiwarat](https://github.com/fujiwarat) added a commit that referenced this issue [on 8 Jan 2019](https://github.com/ibus/ibus/issues/1991#ref-commit-be7fb81)[client/gtk2: Fix Atom and Slack for Flatpak](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)client/gtk2: Fix Atom and Slack for Flatpak Seems Atom, slack, com.visualstudio.code does not enable gtk_key_snooper_install() and this issue causes to call gtk_im_context_filter_keypress instead of calling ibus APIs. BUG=https://github.com/ibus/ibus/issues/1991")… [be7fb81](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)



### fujiwarat on 8 Jan 2019

OK, I got time to evaluate this issue.  
Actually I verified the pure electron applications works fine with ibus under [https://github.com/flathub/io.atom.electron.BaseApp](https://github.com/flathub/io.atom.electron.BaseApp) .

E.g. Electron JavaScript:

```js
function createWindow () {
  win = new electron.BrowserWindow({ width: 800, height: 800 })
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'electron.html'),
    protocol: 'file:',
    slashes: true
  }))
  win.on('closed', () => {
    win = null
  })
}
```

E.g. the electron.html file:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Hello, world!</title>
  </head>
  <body>
    <textarea name="textarea" cols=30 rows=3>test</textarea>
  </body>
</html>
```

<mark class="hltr-red">Then seems Atom and Slack have a special handling with gtk3.  
After I investigated Atom, it seems the Atom GTK client cannot get the key events with gtk_key_snooper_install() but always get them with ibus_im_context_filter_keypress().  
In that case, ibus gtk clients had a bug to check the DBUs activity in Flatpak.</mark>

Now I fixed it in ibus upstream and the fix needs to be integrated to the Flatpak platofrm, E.g. org.freedesktop.[Platform|Sdk], org, org.gnome.Platform

[fujiwarat](https://github.com/fujiwarat) closed this as [completed](https://github.com/ibus/ibus/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 8 Jan 2019](https://github.com/ibus/ibus/issues/1991#event-2059274050)

[fujiwarat](https://github.com/fujiwarat) self-assigned this [on 8 Jan 2019](https://github.com/ibus/ibus/issues/1991#event-2059274388)

[fujiwarat](https://github.com/fujiwarat) added [Type-Defect](https://github.com/ibus/ibus/labels/Type-Defect) [Component-ibus](https://github.com/ibus/ibus/labels/Component-ibus) [OpSys-Linux](https://github.com/ibus/ibus/labels/OpSys-Linux) labels [on 8 Jan 2019](https://github.com/ibus/ibus/issues/1991#event-2059274908)

[fujiwarat](https://github.com/fujiwarat) added this to the [1.5.20](https://github.com/ibus/ibus/milestone/5) milestone [on 8 Jan 2019](https://github.com/ibus/ibus/issues/1991#event-2059275100)

This was referenced on 8 Jan 2019

[[IBus IME not working with electron app io.atom.electron.BaseApp-4]] Closed
[[electron apps do not recognize ibus 1671]] Closed

[[ibus intelligent pinyin not working in telegram-desktop snap apps 4449]] Closed

[fujiwarat](https://github.com/fujiwarat)

Member

### fujiwarat on 4 Mar 2019

Reported [https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues/681](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues/681)

[[Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image 132]]  

- Context
	The old IBus GTK IM module has a bug with Atom and Slack in Flatpak.
	[https://github.com/flatpak/flatpak/issues/1671](https://github.com/flatpak/flatpak/issues/1671)
- Description
	Could you please import ibus 1.5.20 in org.freedesktop.[Platform|Sdk] ?
	The IBus version includes the fixes to work with Atom and Slack applications:
	[https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)
	
	I think to update the version in ibus.bst:
	[https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/18.08/elements/desktop/ibus.bst](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/18.08/elements/desktop/ibus.bst)
- Acceptance Criteria
	IBus 1.5.20 is now released officially.