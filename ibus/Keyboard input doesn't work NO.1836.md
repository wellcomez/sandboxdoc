---
title: "Keyboard input doesn't work NO.1836"
layout: post
---

# Keyboard input doesn't work NO.1836

[netblue30](https://github.com/netblue30)/firejail/Public
[Keyboard input doesn't work #1836](https://github.com/netblue30/firejail/issues/1836#start-of-content) Closed

[Tvax](https://github.com/Tvax) opened this issue on 26 Mar 2018 · 3 comments



### Tvax on 26 Mar 2018

I'm on Ubuntu 17.10

Each time I'm launching an AppImage through Firejail, the keyboard doesn't work. It works fine if I'm launching the app without using Firejails.

The Exec Command:  
`Exec=firejail --env=DESKTOPINTEGRATION=appimaged --private --appimage '[..]/app.AppImage'`



###  SkewedZeppelin on 26 Mar 2018

See [#1579](https://github.com/netblue30/firejail/issues/1579), [#1302](https://github.com/netblue30/firejail/issues/1302), and [#1204](https://github.com/netblue30/firejail/issues/1204)

I don't think we've found a fix yet.





###  chiraag-nataraj on 21 May 2019

Is this still an issue [@Tvax](https://github.com/Tvax)?



### Author Tvax on 24 May 2019

[@chiraag-nataraj](https://github.com/chiraag-nataraj) No it's not!

 [rusty-snake](https://github.com/rusty-snake) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 24 May 2019](https://github.com/netblue30/firejail/issues/1836#event-2363318354)


