---
layout: post
tags:
- fcitx
- flatpak
title: "Flatpak-only setup 287"

---

# Flatpak-only setup 287
[fcitx](https://github.com/fcitx)/[fcitx5](https://github.com/fcitx/fcitx5)Public
[Flatpak-only setup?](https://github.com/fcitx/fcitx5/issues/287#top)#287 Closed

[teohhanhui](https://github.com/teohhanhui) opened this issue on 8 Jun 2021 · 5 comments
 Closed

[teohhanhui](https://github.com/teohhanhui) opened this issue on 8 Jun 2021 · 5 comments 




### teohhanhui on 8 Jun 2021

## Background

I'm using openSUSE MicroOS, which has an immutable root filesystem. All packages installed / updated to the base system require a system restart. Therefore, it'd be ideal to be able to install fcitx entirely through flatpak only.

## Steps to reproduce

1.  I've followed the instructions at [https://fcitx-im.org/wiki/Install_Fcitx_5#Install_Fcitx_5_from_Flatpak](https://fcitx-im.org/wiki/Install_Fcitx_5#Install_Fcitx_5_from_Flatpak)
    
    So I've installed `org.fcitx.Fcitx5` and `org.fcitx.Fcitx5.Addon.ChineseAddons` flatpak packages from flathub.
    
2.  I've copied `/etc/xdg/autostart/fcitx-autostart.desktop` from within the `org.fcitx.Fcitx5` flatpak package, to `~/.config/autostart/fcitx-autostart.desktop` on the host.
    
    And I've modified the `~/.config/autostart/fcitx-autostart.desktop` file as follows:
    
    ```
    Exec=flatpak run --command=fcitx-autostart org.fcitx.Fcitx5
    Icon=org.fcitx.Fcitx5
    ```
    
3.  I've created the `~/.config/environment.d/envvars.conf` file as follows:
    
    ```
    GTK_IM_MODULE=fcitx
    QT_IM_MODULE=fcitx
    XMODIFIERS=@im=fcitx
    ```
    
4.  Reboot.
    

## Problem / questions

So far so good.

 `flatpak run --command=fcitx-diagnose org.fcitx.Fcitx5` output:

It doesn't use `~/.config/fcitx` and `~/.config/fcitx5` for some reason. Passing `--env=XDG_CONFIG_HOME=$HOME/.config` doesn't seem to do anything, maybe due to [flathub/io.neovim.nvim#2 (comment)](https://github.com/flathub/io.neovim.nvim/issues/2#issuecomment-452923174)

And it's not able to detect the running `fcitx` process, but the process seems to be running just fine.

 `sudo journalctl -b | grep -i fcitx` output:

So now the main problem is I don't have access to `fcitx-config-gtk3`. Perhaps I can try installing it in my `toolbox`([https://github.com/kubic-project/microos-toolbox](https://github.com/kubic-project/microos-toolbox)), but it'd be nice to have it as a flatpak package.

Also, is this kind of setup supported? I saw [#108 (comment)](https://github.com/fcitx/fcitx5/issues/108#issuecomment-683123132) where you've mentioned this so I guess it's a yes?

> Use flatpak fcitx + flatpak fcitx engine + flatpak/host app is possible.



### Author teohhanhui on 8 Jun 2021

If I run `fcitx-diagnose` from within the `toolbox` container, it's able to detect the running process.

So I guess it's due to sandbox limitation that `fcitx-diagnose` is not working from within the flatpak.



### Author teohhanhui on 8 Jun 2021

I think I've realized my mistake. The correct thing to do is:

```
ln -s ~/.local/share/flatpak/exports/share/applications/org.fcitx.Fcitx5.desktop ~/.config/autostart/org.fcitx.Fcitx5.desktop
```



### Author teohhanhui on 8 Jun 2021

Now there's only one thing which is still not working. fcitx IME doesn't work in host apps.

 [teohhanhui](https://github.com/teohhanhui) changed the title ~~Provide flatpak package for fcitx-config-gtk3~~ Flatpak-only setup? [on 8 Jun 2021](https://github.com/fcitx/fcitx5/issues/287#event-4859451205)



### Member wengxt on 9 Jun 2021

You still need some im module on your host (not possible to be provided by flatpak).

You can use either: xim (which could be buggy), fcitx 4, fcitx5, or even ibus. I suggest fcitx4 one if fcitx5 is not available by your distro.

[https://fcitx-im.org/wiki/Install_Fcitx_5#Install_Fcitx_5_from_Flatpak](https://fcitx-im.org/wiki/Install_Fcitx_5#Install_Fcitx_5_from_Flatpak)  
[https://fcitx-im.org/wiki/Input_method_related_environment_variables](https://fcitx-im.org/wiki/Input_method_related_environment_variables)



### Member wengxt on 9 Jun 2021

In terms of generally available by default without install new package (my guessing),

Use following values might work, you can also play with other values.

```shell
XMODIFIERS=@im=fcitx # For x11
GTK_IM_MODULE=xim # For Gtk
QT4_IM_MODULE=xim # For Qt4
QT_IM_MODULE=ibus # For Qt5+
```

Also, there's no "fcitx-autostart" in fcitx5, nor "fcitx-diagnose". There's only "fcitx5-diagnose".

Also there's no fcitx-config-gtk3 for fcitx5 and org.fcitx.Fcitx5 already bundles the fcitx5-configtool within it, you can access it easily via the fcitx tray icon.

 [wengxt](https://github.com/wengxt) transferred this issue from fcitx/flatpak-fcitx5 [on 9 Jun 2021](https://github.com/fcitx/fcitx5/issues/287#event-4860514617)

 [wengxt](https://github.com/wengxt) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 9 Jun 2021](https://github.com/fcitx/fcitx5/issues/287#event-4860515840)

 [fcitx](https://github.com/fcitx) locked and limited conversation to collaborators [on 9 Jun 2021](https://github.com/fcitx/fcitx5/issues/287#event-4860515925)

### This issue was moved to a discussion.

You can continue the conversation there. [Go to discussion →](https://github.com/fcitx/fcitx5/discussions/288)

