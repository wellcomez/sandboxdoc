---
layout: post
tags:
- firejail
- ibus
- firefox
- IBUS_ADDRESS
title: "Unable to use ibus-daemon in firejail NO.116"

---

# Unable to use ibus-daemon in firejail NO.116
[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public


[Unable to use ibus-daemon in firejail #116](https://github.com/netblue30/firejail/issues/116#start-of-content) Closed

[pyamsoft](https://github.com/pyamsoft) opened this issue on 3 Nov 2015 · 49 comments




### pyamsoft on 3 Nov 2015

The ibus-daemon is used to change the languages on the system. For example, on my personal machine running Arch Linux 64bit using firejail 0.9.34-rc1, <mark style="background: #FF5582A6;">I use ibus to switch between English (US) input and Japanese inputs.
</mark>
When not running in firejail, programs such as MousePad and rxvt-unicode for example will respect the current ibus language and type in either Japanese or English.

However, when running an application in firejail, because the ibus-daemon is not also running in the jail, <mark style="background: #FFB8EBA6;">and the jailed program has no way of talking to the rest of the processes running on the machine, programs such as Mousepad and Chromium do not properly switch inputs using the ibus-daemon.
</mark>

#### Steps to reproduce:

1.  Install ibus-daemon, and in my case, ibus-anthy for Japanese input and noto-fonts-cjk for East Asian fonts.
2.  Start ibus daemon with `ibus-daemon -drx`
3.  Launch mousepad, switch the language in ibus from English to Japanese.
4.  Upon typing, Japanese characters will be displayed as expected.
5.  Launch a firejail instance of mousepad
6.  Switch language from English to Japanese
7.  Upon typing, English is still output, even though Japanese was requested.





### Owner netblue30 on 3 Nov 2015

I'll look into it, thanks.

 [netblue30](https://github.com/netblue30) <mark style="background: #FF5582A6;">added the</mark> [bug](https://github.com/netblue30/firejail/labels/bug) Something isn't workinglabel [on 3 Nov 2015](https://github.com/netblue30/firejail/issues/116#event-453129623)



### Owner netblue30 on 4 Nov 2015

<mark style="background: #FF5582A6;">Fixed</mark>! [netblue30](https://github.com/netblue30) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 4 Nov 2015](https://github.com/netblue30/firejail/issues/116#event-454423557)



### Author pyamsoft on 5 Nov 2015

This patch introduces a regression<mark class="hltr-yellow"> when using network namespaces with a bridge interface</mark>. Because of <mark class="hltr-blue">the new network ns, the iBus daemon will reject all forms on input.</mark>

- This can be observed via the following:
	1.  Create a new bridge network device `br0`
	2.  Pass the bridge device to firejail using the `--net=br0` option 
	3.  Launch any graphical program like chromium / mousepad (anything that can talk with ibus)
	4.  Attempt to type

- Excepted: 
	Output of some kind  
- Result: 
	iBus daemon rejects all input from the program, making it effectively impossible to run a network bridged namespace while using the ibus daemon.

A new issue can be opened in regards to this, or <mark class="hltr-red">the current one can be reopened</mark> [netblue30](https://github.com/netblue30) reopened this [on 5 Nov 2015](https://github.com/netblue30/firejail/issues/116#event-454879631)



### Owner netblue30 on 5 Nov 2015

I've disabled the previous fix if a network ns is created by the sandbox.



### Contributor  pirate486743186 on 13 Nov 2015

<mark class="hltr-red">beh, it doesn't work at all in Firefox</mark>.  
You have to kill ibus to type anything.
~~~shell
"(firefox:1): IBUS-WARNING **: Events queue growing too big, will start to drop."
~~~
This seams to be a more general problem, with applications unable to communicate with the outside...



### Owner netblue30 on 13 Nov 2015

<mark class="hltr-pink">Yes, I get the same thing if I have a network namespace configured. Without the net namespace it should work fine. I'll bring in a fix for network namespace.</mark>

<mark class="hltr-orange">The apps will not be able to communicate outside when a net namespace is configured. I'll have to proxy that traffic somehow.
</mark>


### ioparaskev on 17 Jan 2016
<mark class="hltr-yellow">
Same issue exists in fedora too..  
After that ibus is probably misbehaving since amixer cannot see any devices after this and gives an "Mixer attach default error: Connection refused" error.. This makes firejail unusable in my case unfortunately</mark>

Version: Fedora 23

How to reproduce:

1.  `firejail firefox`
2.  open new tab and type url
3.  `amixer` shows error `"Mixer attach default error: Connection refused"`



### Owner netblue30 on 19 Jan 2016

This looks more like a sound problem. Do you have PulseAudio installed, or only ALSA?



### ioparaskev on 22 Jan 2016

<mark class="hltr-red">I have also pulseaudio and same happens with pulseaudio.. After the reproduce I'm losing control over the sound device.  </mark>

~~~shell
sh -c "pactl set-sink-mute 0 false ; pactl set-sink-volume 1 -100%"

> shm_open() failed: No such file or directory  
> Connection failure: Protocol error  
> shm_open() failed: No such file or directory  
> Connection failure: Protocol error
~~~



### Owner netblue30 on 22 Jan 2016

So, it is a sound problem, it has nothing to do with ibus. PulseAudio has a problem when running in a PID namespace. <mark class="hltr-red">There is a workaround until they fix the problem. Look at "Known Problems" section here:</mark>

[https://firejail.wordpress.com/support/](https://firejail.wordpress.com/support/)



### ioparaskev on 22 Jan 2016

oh I see.. thanks and sorry for hijacking this



### Owner netblue30 on 23 Jan 2016

No problem!

[pyamsoft](https://github.com/pyamsoft) <mark class="hltr-red">mentioned this issue</mark> [on 28 Apr 2016](https://github.com/netblue30/firejail/issues/116#ref-issue-146083518) [[BUS-WARNING NO.410]] Closed



### Contributor  pirate486743186 on 10 May 2016

Same in Ubuntu 16.04



### dfaerch on 26 Jun 2016

<mark class="hltr-yellow">I wanted to try out firejail (0.9.40 on ubuntu 14.04), but I seem to be having the same issue. I did</mark>

```
$ firejail google-chrome
```

<mark class="hltr-yellow">but keyboard doesn't respond and i get errors in the console:</mark>

```bash
(google-chrome:2): IBUS-WARNING **: Unable to connect to ibus: Could not connect: Connection refused
(google-chrome:2): IBUS-WARNING **: Events queue growing too big, will start to drop.
(google-chrome:2): IBUS-WARNING **: Events queue growing too big, will start to drop.
```





### biergaizi on 9 Oct 2016

Still no possible solutions? It basically disables any keyboard inputs on a iBus-based desktop, since English is also managed by iBus, and effectively made the great Firejail useless...



###  解法 folti on 10 Jan 2017

<mark class="hltr-blue">temporary workaround is to tell Gtk to use a different input module, by setting the GTK_IM_MODULE accordingly. For example:
</mark>

```
firejail opera
```

<mark class="hltr-red">don't have keyboard, while</mark>

```shell
GTK_IM_MODULE=xim firejail opera
```

does.





### Owner netblue30 on 12 Jan 2017

Thanks for the info, I'll document it on the web site.



### anatoli26 on 25 Mar 2017

Hi, I just wanted to try Firejail on Ubuntu 16.04 stock with the latest Firefox 52.0.1 from xenial channel. The FJ version from the official channel (firejail/xenial 0.9.38.10-0ubuntu0.16.04.1 amd64) had a number of issues (especially with sound and broke it for other apps that were started before the FJ install), then I learned that this version is quite outdated and has a known issue with sound, <mark class="hltr-red">so I purged it and installed a new one from this repo (0.9.45), but I get the same issue as other users here:
</mark>
~~~bash
(firefox:7): IBUS-WARNING **: Unable to connect to ibus: Could not connect: Connection refused 
~~~

Then, when trying to type anything, I get:

~~~shell
(firefox:7): IBUS-WARNING **: Events queue growing too big, will start to drop.
~~~

Is there any solution to this problem?

<mark class="hltr-red">_UPDATE_: tried more apps (rhythmbox, wire messenger, etc.), they all seem to have the same problem under FJ: IBUS-WARNING and no keyboard.
</mark>


### Owner netblue30 on 26 Mar 2017

I'll give it a try, thanks.



### pdo-smith on 11 May 2017

I have the same problem in Ubuntu 17.04  
<mark class="hltr-red">The fix given by folti (GTK_IM_MODULE=xim firejail opera) works.</mark>



### cwmke on 18 Aug 2017

<mark class="hltr-blue">I've been using Fedora lately without this issue happening. Tonight I installed the nvidia drivers and the problem started again. I don't have the issue with intel video drivers. It only seems to happen when I'm using nvidia.</mark>



###  chiraag-nataraj on 8 Jun 2018

Is this still an issue and do the workarounds above not work?



### dfaerch on 12 Jun 2018

[@chiraag-nataraj](https://github.com/chiraag-nataraj) <mark class="hltr-pink">my issue has resolved itself over time. _works-for-me_</mark> [Fred-Barclay](https://github.com/Fred-Barclay) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 12 Jun 2018](https://github.com/netblue30/firejail/issues/116#event-1676694753)



### Contributor  graywolf on 5 Jul 2018

I still have the issue, `GTK_IM_MODULE=ibus firefox` does not work. <mark class="hltr-cyan">I don't think this should be closed</mark>. [chiraag-nataraj](https://github.com/chiraag-nataraj) <mark class="hltr-red">reopened</mark> this [on 9 Jul 2018](https://github.com/netblue30/firejail/issues/116#event-1721979089)



###  chiraag-nataraj on 9 Jul 2018

[@graywolf](https://github.com/graywolf) Hmm, okay. Reopening. Maybe someone who uses ibus can help you debug (I used to, but switched over to `setxkbmap` + `emacs` for esoteric keyboards).



###  chiraag-nataraj on 6 Aug 2018

[@graywolf](https://github.com/graywolf), have you tried the workarounds on this thread?



### Contributor  graywolf on 14 Aug 2018

[@chiraag-nataraj](https://github.com/chiraag-nataraj) I've tried (and it seems to work), however guys over at ibus keep telling me that it's wrong thing to do [[Suggested values for environment variables might be wrong 2020]] -> [ibus/ibus#2020 (comment)](https://github.com/ibus/ibus/issues/2020#issuecomment-409171856)



###  chiraag-nataraj on 14 Aug 2018

[@graywolf](https://github.com/graywolf) The way I look at it is: If it works, don't worry about it 😂 Seriously, though...unless there are security implications, this is probably okay. If it stops working, we'll figure something out then 😂



### Contributor  graywolf on 15 Aug 2018

I guess... 🍡



###  chiraag-nataraj on 23 Aug 2018

So if the workarounds here _are_ working (ibus developers' opinions notwithstanding), I'm going to go ahead and close the issue.

 [chiraag-nataraj](https://github.com/chiraag-nataraj) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 23 Aug 2018](https://github.com/netblue30/firejail/issues/116#event-1803292231)

 [chiraag-nataraj](https://github.com/chiraag-nataraj) added the [information](https://github.com/netblue30/firejail/labels/information) Information was/is requiredlabel [on 23 Aug 2018](https://github.com/netblue30/firejail/issues/116#event-1803292531)




### chiraag-nataraj on 23 Aug 2018

On second thought, re-opening, since this is still an issue that should be debugged.

 [chiraag-nataraj](https://github.com/chiraag-nataraj) reopened this [on 23 Aug 2018](https://github.com/netblue30/firejail/issues/116#event-1803295460)



### xelxebar on 4 Nov 2018

For whatever it's worth the proposed `*_IM_MODULE` workaround doesn't fix the issue for me, though I'm running _qutebrowser_instead of _firefox_.

```shell
$ firejail --version
firejail version 0.9.56

Compile time support:
        - AppArmor support is enabled
        - AppImage support is enabled
        - chroot support is enabled
        - file and directory whitelisting support is enabled
        - file transfer support is enabled
        - networking support is enabled
        - overlayfs support is enabled
        - private-home support is enabled
        - seccomp-bpf support is enabled
        - user namespace support is enabled
        - X11 sandboxing support is enabled

$ ibus version
1.5.1.19

$ qutebrowser --version
qutebrowser v1.5.2
Git commit:
Backend: QtWebEngine (Chromium 61.0.3163.140)

CPython: 3.6.7
Qt: 5.10.1
PyQt: 5.10.1

sip: 4.19.8
colorama: no
pypeg2: 2.15
jinja2: 2.10
pygments: 2.2.0
yaml: 3.13
cssutils: no
attr: 18.2.0
PyQt5.QtWebEngineWidgets: yes
PyQt5.QtWebKitWidgets: no
pdf.js: no
sqlite: 3.25.2
QtNetwork SSL: LibreSSL 2.7.4
...
```



### mhva on 4 Feb 2019

<mark class="hltr-purple">Disclaimer: I don't use firejail, but I'm almost positive that this issue is related to the fact that ibus-daemon uses an abstract unix socket for IPC by default. This socket is invisible to firejail sandbox if it resides in a separate network namespace.
</mark>
It should be possible to circumvent this issue in the current master branch of ibus by requesting it to use named socket for IPC. 
This can be done by passing `--address=unix:path=<somedir>/<somefile>` to ibus-daemon and making sure that `<somedir>` and `$HOME/.config/ibus` are both visible in sandbox. 
The latter dir contains ibus-daemon socket address, which is needed for ibus clients to be able to find where they should connect.





### biergaizi on 5 Feb 2019

Awesome! Having the capability to use an alternative IPC socket should definitely solve the problem.



###  chiraag-nataraj on 20 May 2019

Given the workaround [@mhva](https://github.com/mhva) suggested, do people still have this issue?



###  chiraag-nataraj on 22 May 2019

I'm going to go ahead and close this for now. If anyone affected is still running into this, please feel free to re-open!

 [chiraag-nataraj](https://github.com/chiraag-nataraj) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 22 May 2019](https://github.com/netblue30/firejail/issues/116#event-2357691135)



### SailReal on 22 May 2020

I just started using firejail, what an awesome piece of software, thank you all :)

Nearly everything works out of the box but I just stumbled over this issue using Ubuntu 20.04 and the Signal-Desktop client. On my device it can only be started with `GTK_IM_MODULE=xim`, otherwise you can't enter anything using my keyboard.



###  glitsj16 on 22 May 2020

[@SailReal](https://github.com/SailReal) Glad to hear you're enjoying firejail. 
As for the environment variable workaround, you can put that into a `signal-desktop.local` file. 
If you don't have one yet, you will have to create one manually. Either use `/etc/firejail/signal-desktop.local` (affects all users on your machine) or `${HOME}/.config/firejail/signal-desktop.local` (affects your user only). Add the below to that file:

```
env GTK_IM_MODULE=xim
```



 [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 22 May 2020](https://github.com/netblue30/firejail/issues/116#ref-issue-605968621) [[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379]] -> [Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 #3379](https://github.com/netblue30/firejail/issues/3379) Open

 [shemgp](https://github.com/shemgp) mentioned this issue [on 15 Dec 2020](https://github.com/netblue30/firejail/issues/116#ref-issue-220470531) [[Keyboard doesn't work with Chrome browser Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) NO.1204]]->[Keyboard doesn't work with Chrome browser/Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) #1204](https://github.com/netblue30/firejail/issues/1204) Closed



### overchu on 18 Dec 2020

> Disclaimer: I don't use firejail, but I'm almost positive that this issue is related to the fact that ibus-daemon uses an abstract unix socket for IPC by default. This socket is invisible to firejail sandbox if it resides in a separate network namespace.
> 
> It should be possible to circumvent this issue in the current master branch of ibus by requesting it to use named socket for IPC. This can be done by passing `--address=unix:path=<somedir>/<somefile>` to ibus-daemon and making sure that `<somedir>` and `$HOME/.config/ibus` are both visible in sandbox. The latter dir contains ibus-daemon socket address, which is needed for ibus clients to be able to find where they should connect.

Where can you pass these argruments to ibus? I couldn't figure out who is starting ibus in the first place.

1.  `systemd` seems not to start it
2.  neither `systemd --user`
3.  it's also not started by gnome3

But anyway, if I stop ibus-daemon and start it manually with the given options, than I got an error that the address is already in use.
~~~shell
$ ps aux | grep ibus-
alex       33084  2.2  0.0 815460 13488 pts/4    Sl   13:44   0:00 ibus-daemon --replace --panel disable --xim
alex       33105  0.0  0.0 449324  7396 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-dconf
alex       33106  4.0  0.1 483180 28636 pts/4    Sl   13:44   0:01 /usr/libexec/ibus-extension-gtk3
alex       33108  0.1  0.1 404168 22388 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-x11 --kill-daemon
alex       33116  0.0  0.0 449092  7292 ?        Ssl  13:44   0:00 /usr/libexec/ibus-portal
alex       33128  0.5  0.0 375336  7148 pts/4    Sl   13:44   0:00 /usr/libexec/ibus-engine-simple
alex       33207  0.0  0.0 221588   852 pts/4    S+   13:45   0:00 grep --color=auto ibus-

$ kill 33084 # kill ibus-daemon

$ ibus-daemon --replace --panel disable --xim  --address=unix:path=/home/alex/.cache/alex-ibus/ibus-address/test --verbose
(ibus-daemon:35008): IBUS-ERROR **: 13:57:37.755: g_dbus_server_new_sync() is failed with address unix:path=/home/alex/.cache/alex-ibus/ibus-address/test and guid 30d04cbb95ff4a29d0b02d835fdc44d1: Error binding to address (GUnixSocketAddress): Address already in use
~~~
So it seems that even if I find out where ibus is started in will not work anyway.

I am running Fedora 33 with Gnome 3, X, and ibus 1.5.23



###  glitsj16 on 19 Dec 2020

> Where can you pass these argruments to ibus? I couldn't figure out who is starting ibus in the first place.

On Arch Linux the ibus package installs `/usr/share/dbus-1/services/org.freedesktop.IBus.service`:

```bash
$ cat /usr/share/dbus-1/services/org.freedesktop.IBus.service
[D-BUS Service]
Name=org.freedesktop.IBus
Exec=/usr/bin/ibus-daemon --replace --panel disable --xim
```
<mark class="hltr-green">If memory serves you can place an override in /etc/dbus-1/services/org.freedesktop.IBus.service with your desired Exec= command.</mark>

<mark class="hltr-red">Note: As of Jul 2020, Firejail introduced DBus filtering. It is now possible to use the --dbus-user.own=org.freedesktop.IBus parameter to let Firefox access IBus.</mark>



#### overchu on 19 Dec 2020

> On Arch Linux the ibus package installs /usr/share/dbus-1/services/org.freedesktop.IBus.service:

Thank you for the idea of just checking what hte package installs. You are right.
~~~shell
$ rpm -ql ibus | grep -e '\.service'                                                                         │
/usr/share/dbus-1/services/org.freedesktop.IBus.service                                                                │
/usr/share/dbus-1/services/org.freedesktop.portal.IBus.service 
~~~

But I don't see this `.service`-file linked anywhere, so I don't think this is what actually gets used:

~~~shell
$ sudo find -L /etc/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
$ find -L /home/$USER/.config/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
~~~

Both yield empty results.

And no mentioning of ibus in /etc/dbus-1/ either, no results for:

~~~bash
$ tree /etc/dbus-1/ | grep Ibus
~~~



#### overchu on 19 Dec 2020

> Note: As of Jul 2020, Firejail introduced DBus filtering. It is now possible to use the --dbus-user.own=org.freedesktop.IBus parameter to let Firefox access IBus.

<mark class="hltr-green">Sorry, I forgot to mention that I tried this solution unsuccessfully:</mark>
~~~shell
$ firejail --dbus-user.own=org.freedesktop.IBus firefox
Reading profile /etc/firejail/firefox.profile
Reading profile /etc/firejail/whitelist-usr-share-common.inc
Reading profile /etc/firejail/firefox-common.profile
Reading profile /etc/firejail/disable-common.inc
Reading profile /etc/firejail/disable-devel.inc
Reading profile /etc/firejail/disable-exec.inc
Reading profile /etc/firejail/disable-interpreters.inc
Reading profile /etc/firejail/disable-programs.inc
Reading profile /etc/firejail/whitelist-common.inc
Reading profile /etc/firejail/whitelist-var-common.inc
Warning: Warning: NVIDIA card detected, nogroups command disabled
Parent pid 105219, child pid 105223
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Warning: An abstract unix socket for session D-BUS might still be available. Use --net or remove unix from --protocol set.
Warning: cleaning all supplementary groups
Warning: not remounting /run/user/1000/gvfs
Warning: cleaning all supplementary groups
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Warning: cleaning all supplementary groups
Seccomp list in: !chroot, check list: @default-keep, prelist: unknown,
Child process initialized in 216.69 ms

Parent is shutting down, bye...
~~~

<mark class="hltr-green ">I even consider a security bug because it spits out a working firefox (with ibus) that is _not_ sandboxed which is not what you expect if you run `firejail`.  
Above, this might be the most interesting line:</mark>

<mark class="hltr-green">Warning: An abstract unix socket for session D-BUS might still be available. Use --net or remove unix from --protocol set.
</mark>


####  rusty-snake on 19 Dec 2020

> But I don't see this .service-file linked anywhere, so I don't think this is what actually gets used:
> 
> ```shell
> $ sudo find -L /etc/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
> $ find -L /home/$USER/.config/systemd/ -samefile /usr/share/dbus-1/services/org.freedesktop.IBus.service 
> ```
> 
> Both yield empty results.
> 
> And no mentioning of ibus in /etc/dbus-1/ either, no results for:
> 
> ```shell
> $ tree /etc/dbus-1/ | grep Ibus
> ```

It's **not** a systemd unit. It's a D-Bus service (look at the `[D-BUS Service]` line in it).

<mark class="hltr-red">D-Bus configuration is located at `/usr/share/dbus-1` (distributions defaults) and `/etc/dbus-1` (user/admin overrides).
</mark>
> --dbus-user.own=org.freedesktop.IBus

<mark class="hltr-red">`--dbus-user.talk=org.freedesktop.IBus` should be enough.</mark>



###  rusty-snake on 19 Dec 2020

<mark class="hltr-green">FWIW: Fcitx works we allowing the portal, see</mark> [#3732](https://github.com/netblue30/firejail/issues/3732).



### Contributor  graywolf on 1 Nov 2021

<mark class="hltr-blue">The `xim` workaround stopped working for me on firefox 93.0. Should this be re-opened?</mark>



### martinetd on 12 Dec 2021

I don't know about the xim workaround not working (I've never had input stuck because of ibus in default configuration), but the 'solution' that had been accepted years ago is nothing but a workaround that disables ibus, it's not a way of using ibus.

I've spent quite a bit of time on this, here's my take:

-   by default, ibus will try to open `$HOME/.config/ibus/bus/<machineid>-<hostname>-<display>`(`ibus_get_socket_path()`) to guess the ibus socket. While adding `whitelist ${HOME}/.config/ibus` to the profile lets ibus load this file, ibus tries to check if the socket is valid by sending a signal (kill(0) to the pid defined in that file,<mark class="hltr-blue"> and that doesn't work with our PID namespace, so giving access to that file is useless unless we could share the pid namespace, which is not possible</mark> [Ability to turn off pid namespacing? #892](https://github.com/netblue30/firejail/issues/892)
-   <mark class="hltr-green ">instead, one can set `IBUS_ADDRESS` directly in the environment (preload that file before starting firejail), which will skip the PID check</mark>.
    -   The default IBUS_ADDRESS uses an abstract path, <mark class="hltr-pink">so unless firejail creates a different network namespace firefox will be able to connect to it</mark>
    -   For cases with a separate namespace,<mark class="hltr-yellow"> it's possible to start ibus-daemon with `--address=unix:dir=/path/to/somedir` and whitelist that directory to allow firefox to connect to it</mark>. Note ibus allows unix:path=/path/to/sock, but that doesn't work right now, see [ibus-daemon --address=unix:path=something fails with EADDRINUSE because we mkdir it ibus/ibus#2363](https://github.com/ibus/ibus/issues/2363)
    -   <mark class="hltr-red">Note that if `IBUS_ADDRESS` is set to something invalid</mark>, input will not work (like what others describe here<mark class="hltr-red">, ibus-warning "Events queue growing too big, will start to drop.")</mark>
-  <mark class="hltr-purple"> Allowing `dbus-user.talk org.freedesktop.IBus` is also needed as suggested. Ther'e's also a `org.freedesktop.portal.IBus` but I didn't find this to be required.</mark>

Ultimately my conf is as follow (using unix socket in ~/.cache/ibus)

```shell
$ cat ~/.config/firejail/firefox.local
dbus-user.talk org.freedesktop.IBus
whitelist ${HOME}/.cache/ibus
$ grep ibus ~/.xsession
ibus-daemon --xim -d --address=unix:dir=$HOME/.cache/ibus/dbus
$ cat ~/.bin/ff
#!/bin/sh

. "$HOME/.config/ibus/bus/$(cat /var/lib/dbus/machine-id)-unix-${WAYLAND_DISPLAY}"

export IBUS_ADDRESS

exec systemd-run --user --scope --unit=ff-$$.scope \
	-p MemoryMax=3G -p MemoryHigh=2G \
	firejail firefox "$@"
```





###  解法  rusty-snake on 8 Jan

```shell
env GTK_IM_MODULE=ibus
env IBUS_USE_PORTAL=1
dbus-user.talk org.freedesktop.portal.IBus
```

<mark class="hltr-red">seems to work too</mark>

 [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 12 Apr](https://github.com/netblue30/firejail/issues/116#ref-issue-1199012687) [[Keyboard no longer working in gedit NO.5100]] Open




### scottslowe on 26 May

Confirming that [@rusty-snake](https://github.com/rusty-snake)'s workaround from Jan 8 2022 works for me on a newly-upgraded Fedora 36 system (to fix keyboard input for Thunderbird).



### martinetd on 8 Sep

<mark class="hltr-red">Follow up half a year later: turns out my system just doesn't start ibus-portal, running it manually and asking gtk to use it seems to be easier to handle than my old workaround so switched to that as well.</mark>

Thanks [@rusty-snake](https://github.com/rusty-snake) !



