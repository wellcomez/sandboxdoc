#include <dbus/dbus.h>
#include <stdlib.h>
#include <stdio.h>

/*
cc $(pkg-config --cflags --libs dbus-glib-1) -o dbus-send-hello dbus-send-hello.c && ./dbus-send-hello
*/

int main (int argc, char *argv[])
{
	DBusError dberr;
	DBusConnection *dbconn;
	DBusMessage *dbmsg;
	char *text;

	dbus_error_init (&dberr);
	dbconn = dbus_bus_get (DBUS_BUS_SESSION, &dberr);
	if (dbus_error_is_set (&dberr)) {
		fprintf (stderr, "getting session bus failed: %s\n", dberr.message);
		dbus_error_free (&dberr);
		return EXIT_FAILURE;
	}

	dbmsg = dbus_message_new_signal ("/org/test/DBus/Example",
	                                 "org.test.DBus.Example",
	                                 "TestSignal");
	if (dbmsg == NULL) {
		fprintf (stderr, "Could not create a new signal\n");
		return EXIT_FAILURE;
	}

	text = "Hello World";
	dbus_message_append_args (dbmsg, DBUS_TYPE_STRING, &text, DBUS_TYPE_INVALID);

	dbus_connection_send (dbconn, dbmsg, NULL);
	fprintf (stderr, "Sent signal to D-Bus\n");

	dbus_message_unref (dbmsg);

	dbus_connection_unref (dbconn);

	return EXIT_SUCCESS;
}
