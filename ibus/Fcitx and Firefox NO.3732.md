---
layout: post
tags:
- fcitx
- firejail
- firefox
title: "Fcitx and Firefox NO.3732"

---

# Fcitx and Firefox NO.3732
[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail) Public
[Fcitx and Firefox](https://github.com/netblue30/firejail/issues/3732#top)#3732 Closed






### hugthecactus on 7 Nov 2020

I was going to ask for help with this issue but I managed to fix it myself so I'm leaving the solution here in the hope that it will be easier for others to find.

In previous versions of firejail, the way to enable Fcitx input method (for typing in Japanese, for example) was to use `ignore nodbus` but this has recently been deprecated in favour of various `dbus-user` and `dbus-system` options. The one that seems to conflict with Fcitx is `dbus-user filter` in `firefox.profile` so what fixed the issue for me was to add the line `ignore dbus-user filter` to `~/.config/firejail/firefox.local` (create if necessary).

Hope that helps someone.



###  rusty-snake on 7 Nov 2020

Does it work if you add `dbus-user.talk org.fcitx.Fcitx` instead of `ignore dbus-user filter`? This would be mores secure since it only allows a additional name than all session-bus.



### Author hugthecactus on 8 Nov 2020

Just tested `dbus-user.talk org.fcitx.Fcitx` and unfortunately it's not enough to get it working for me. Happy to test some more configurations though, if you have any other ideas.



###  rusty-snake on 8 Nov 2020

No, unfortunately I have no further ideas. But you can check `grep -i fcitx /usr/share/dbus-1/services/*` and `busctl --user list | grep -iE "(fcitx|firefox)"`.



### Author hugthecactus on 8 Nov 2020

Aha! That second command listed a few things, among which is `org.freedesktop.portal.Fcitx`.

Now my `firefox.local` looks like:

`dbus-user.talk org.freedesktop.portal.Fcitx`

and it seems to be working just fine. Thanks very much for pointing me in the right direction!



###  rusty-snake on 8 Nov 2020

Summary: Depending on the dbus-policy of the profile you need to add different command to its local.

If `dbus-user none`:

```
dbus-user filter
dbus-user.talk org.freedesktop.portal.Fcitx
ignore dbus-user none
```

If `dbus-user filter`:

```
dbus-user.talk org.freedesktop.portal.Fcitx
```

I'll add it to the FAQ.





###  rusty-snake on 8 Nov 2020

[https://github.com/netblue30/firejail/wiki/Frequently-Asked-Questions#how-can-i-enable-fcitx](https://github.com/netblue30/firejail/wiki/Frequently-Asked-Questions#how-can-i-enable-fcitx)
>Depending on the dbus-policy of the profile you need to add different command to its local. If the dbus-policy is set to `filter`, it is enough to add `dbus-user.talk org.freedesktop.portal.Fcitx`. If it is set to none, you need to add

```
dbus-user filter
dbus-user.talk org.freedesktop.portal.Fcitx
ignore dbus-user none
```
 [rusty-snake](https://github.com/rusty-snake) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 8 Nov 2020](https://github.com/netblue30/firejail/issues/3732#event-3970535517)
 [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 19 Dec 2020](https://github.com/netblue30/firejail/issues/3732#ref-issue-114736251) [Unable to use ibus-daemon in firejail #116](https://github.com/netblue30/firejail/issues/116) Closed

[rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 20 Jan 2021](https://github.com/netblue30/firejail/issues/3732#ref-issue-465283707)[[Firejail breaks fcitx input on Firefox NO.2841]][](https://github.com/netblue30/firejail/issues/2841) Closed


