---
layout: post
tags:
- fcitx
- firejail
- firefox
title: "Firejail breaks fcitx input on Firefox NO.2841"

---

[netblue30](https://github.com/netblue30)/**[firejail](https://github.com/netblue30/firejail)Public
[Firejail breaks fcitx input on Firefox #2841](https://github.com/netblue30/firejail/issues/2841) closed

[jwittlincohen](https://github.com/jwittlincohen) opened this issue on 8 Jul 2019 · 5 comments




### jwittlincohen on 8 Jul 2019

I'm using firejail 0.9.5.8 on Debian Buster (10.0). Using the default profiles, firejail breaks fcitx input methods in both Firefox 67.0.4 and Chrome 75, by which I mean, entering the key combination to switch inputs (Ctrl + Space) does nothing. If I run either browser without firejail confinement, Ctrl + Space will switch to Pinyin input and allow me to type in Simplified Chinese.

I found a workaround for Chrome by searching this Github, namely adding "ignore nodbus" to google-chrome.local. However, "ignore nodbus" does not resolve the issue for Firefox and I'm not sure what else to try. I've provided the distribution-provided Firefox profile(s) and my local profile below.

firefox-common.profile (untouched from Debian install)

```shell
# Firejail profile for firefox-common
# This file is overwritten after every install/update
# Persistent local customizations
include firefox-common.local
# Persistent global definitions
# already included by caller profile
#include globals.local

# uncomment the following line to allow access to common programs/addons/plugins
#include firefox-common-addons.inc

noblacklist ${HOME}/.pki
noblacklist ${HOME}/.local/share/pki

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc

mkdir ${HOME}/.pki
mkdir ${HOME}/.local/share/pki
whitelist ${DOWNLOADS}
whitelist ${HOME}/.pki
whitelist ${HOME}/.local/share/pki
include whitelist-common.inc
include whitelist-var-common.inc

apparmor
caps.drop all
# machine-id breaks pulse audio; it should work fine in setups where sound is not required
#machine-id
netfilter
# Breaks Gnome connector and KDE Connect
# Also seems to break Ubuntu titlebar menu
# Also breaks enigmail apparently?
# During a stream on Plasma it prevents the mechanism to temporarily bypass the power management, i.e. to keep the screen on
# Therefore disable if you use that
nodbus
nodvd
nogroups
nonewprivs
noroot
notv
?BROWSER_DISABLE_U2F: nou2f
protocol unix,inet,inet6,netlink
seccomp.drop @clock,@cpu-emulation,@debug,@module,@obsolete,@raw-io,@reboot,@resources,@swap,acct,add_key,bpf,fanotify_init,io_cancel,io_destroy,io_getevents,io_setup,io_submit,ioprio_set,kcmp,keyctl,mount,name_to_handle_at,nfsservctl,ni_syscall,open_by_handle_at,personality,pivot_root,process_vm_readv,ptrace,remap_file_pages,request_key,setdomainname,sethostname,syslog,umount,umount2,userfaultfd,vhangup,vmsplice
shell none
#disable tracelog, it breaks or causes major issues with many firefox based browsers, see github issue #1930
#tracelog

disable-mnt
private-dev
# private-etc below works fine on most distributions. There are some problems on CentOS.
#private-etc ca-certificates,ssl,machine-id,dconf,selinux,passwd,group,hostname,hosts,localtime,nsswitch.conf,resolv.conf,xdg,gtk-2.0,gtk-3.0,X11,pango,fonts,mime.types,mailcap,asound.conf,pulse,pki,crypto-policies,ld.so.cache
private-tmp

# breaks DRM binaries
#noexec ${HOME}
noexec /tmp
```

firefox-common.local

```
blacklist /data/backups
blacklist /data/documents
blacklist /data/photos

#Allow fcitx
ignore nodbus

#Allow support for Yubikey Authentication
ignore nou2f
```

 [rusty-snake](https://github.com/rusty-snake) added the [question](https://github.com/netblue30/firejail/labels/question) Further information is requestedlabel [on 8 Jul 2019](https://github.com/netblue30/firejail/issues/2841#event-2466367637)



###  rusty-snake on 9 Jul 2019

Since no one commenting here with an idea, I suggest you to try if one of the following works:

```
ignore private-tmp
ignore protocol
ignore noroot
```

BTW: `ignore nou2f` should no be necessary except you set `browser-disable-u2f yes` in firejail.config. (Or is this default on debian, I know that there is some hardening on the default config in the debian package.)





### Author jwittlincohen on 9 Jul 2019

Thanks for the suggestion! I can confirm that both `ignore protocol` and`ignore nodbus` are required for fcitx on Firefox.

As for `nou2f`, it is definitely required to use a Yubikey on Debian. The distribution provided firefox-common.profile and chromium-common.profile specifies `nodbus` and `?BROWSER_DISABLE_U2F: nou2f` which breaks FIDO2/U2F support. I only began investigating the issue because I couldn't use my Yubikey in Firefox or Chrome with firejail. I might create a bug report on the Debian tracker for this issue. FIDO2/U2F Security keys enhance security by providing a strong second factor that thwarts phishing attacks. I don't see a good reason for firejail to break U2F security keys if it's possible to limit access to other USB devices while allowing U2F keys.

Edit: I just checked [https://github.com/netblue30/firejail/blob/master/etc/firefox-common.profile](https://github.com/netblue30/firejail/blob/master/etc/firefox-common.profile) and it also has `?BROWSER_DISABLE_U2F: nou2f`

 [jwittlincohen](https://github.com/jwittlincohen) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 9 Jul 2019](https://github.com/netblue30/firejail/issues/2841#event-2467574157)

 [tedliosu](https://github.com/tedliosu) mentioned this issue [on 19 Mar 2020](https://github.com/netblue30/firejail/issues/2841#ref-issue-583991420) [[UIM input method switching not working with firefox NO.3284]][](https://github.com/netblue30/firejail/issues/3284) Closed



### zoenglinghou on 19 Jan 2021

[@jwittlincohen](https://github.com/jwittlincohen) sorry for bringing it up again. I tried putting `ignore protocol` and `ignore nodbus` in `~/.config/firejail/firefox-common.local` but `fcitx` is not working (while directory whitelists in `~/.config/firejail/firefox.local` work). Am I missing something here?




###  rusty-snake on 20 Jan 2021

[https://github.com/netblue30/firejail/wiki/Frequently-Asked-Questions#how-can-i-enable-fcitx](https://github.com/netblue30/firejail/wiki/Frequently-Asked-Questions#how-can-i-enable-fcitx) (result from [#3732](https://github.com/netblue30/firejail/issues/3732))
[[Fcitx and Firefox NO.3732]] Closed




### zoenglinghou on 20 Jan 2021

Yep, that just works! Thank you!


