---
layout: post
tags:
- electron
title: "Cant use ibus on typora 2976"

---

# Cant use ibus on typora 2976

[Cant use ibus on typora](https://github.com/typora/typora-issues/issues/2976#top)#2976 close

[iceBear67](https://github.com/iceBear67) opened this issue on 1 Nov 2019 · 2 comments

Comments [iceBear67](https://github.com/iceBear67)

### iceBear67 [on 1 Nov 2019]

```shell
Sys: ArchLinux 5.3.7-arch1-2-ARCH [#1](https://github.com/typora/typora-issues/issues/1) SMP PREEMPT @1572002934 x86_64 GNU/Linux  
TyporaVersion: 0.9.79(beta)  
Sys&IM Locale: zh_CN.UTF8
```

I cant use ibus when im typing on typora.  
and i also tried using this:

```shell
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
```

Could u help me?

[abnerlee](https://github.com/abnerlee)

### Contributor abnerlee [on 7 Nov 2019]

[[electron apps do not recognize ibus 1671]] closed
[[Unable to use iBus (input method) 675]] closed

Do you use flatpak? Please try the binary version since flatpak is not official supported.

[abnerlee](https://github.com/abnerlee) closed this as [completed](https://github.com/typora/typora-issues/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 7 Nov 2019](https://github.com/typora/typora-issues/issues/2976#event-2775924044)


### jjangga0214 on 6 Jan 2021

[@abnerlee](https://github.com/abnerlee) This happens when installed with [the official instruction](https://typora.io/#linux).

**Info**: Ubuntu 20.04 with iBus