---
title: "无法在Chrome(Wayland)中使用fcitx5 263"
layout: post
---
[fcitx](https://github.com/fcitx)/[fcitx5](https://github.com/fcitx/fcitx5) Public
[无法在Chrome(Wayland)中使用fcitx5 #263](https://github.com/fcitx/fcitx5/issues/263) Closed

[thep0y](https://github.com/thep0y) opened this issue on 15 Apr 2021 · 25 comments

## Comments



### thep0y on 15 Apr 2021

**Describe the bug**  
不能在Wayland启动的Chrome中使用fcitx5输入法

**To Reproduce**

1.  启动 Chrome

/usr/bin/google-chrome-stable --enable-features=UseOzonePlatform --ozone-platform=wayland

2.  在任何输入框中都无法输入中文

**Expected behavior**  
正常输入中文

**Desktop (please complete the following information):**

-   Desktop: KDE
-   Display server type: Wayland
-   The output of fcitx5-diagnose if possible.

fcitx5-diagnose输出结果



### Member wengxt on 15 Apr 2021

please report to chrome.

 [wengxt](https://github.com/wengxt) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 15 Apr 2021](https://github.com/fcitx/fcitx5/issues/263#event-4596463921)



### Author thep0y on 15 Apr 2021

你好，请问能否告诉我应该报告什么问题给chromium。



### Member wengxt on 15 Apr 2021

They just don't' support input method. You can report it as a general problem.

Also you might want to mention that OzoneWayland does not use Gtk im module (not so sure if they want to resolve it this way).



### Member wengxt on 15 Apr 2021

[https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2) You can follow this actually.



### ztoiax on 24 Apr 2021

i have the same problem,but i use x11.When I upgrade to chrome 90,fcitx5-chinese-addons doesn't work



### Member wengxt on 24 Apr 2021

[@ztoiax](https://github.com/ztoiax) that's unlikely to be the same problem. You'd better check fcitx5-diagnose for more information.

Also chrome all works for me.



### ztoiax on 25 Apr 2021

> [@ztoiax](https://github.com/ztoiax) that's unlikely to be the same problem. You'd better check fcitx5-diagnose for more information.
> 
> Also chrome all works for me.

我已经解决了,是我卸载错包了



### darr1s on 5 Jun 2021

Do you have a solution for this? I am currently using

.pam_environment

```
# Wayland compatibility
QT_QPA_PLATFORM         DEFAULT=wayland
CLUTTER_BACKEND         DEFAULT=wayland
SDL_VIDEODRIVER         DEFAULT=wayland
MOZ_ENABLE_WAYLAND      DEFAULT=1
MOZ_WEBRENDER           DEFAULT=1
XDG_SESSION_TYPE        DEFAULT=wayland
XDG_CURRENT_DESKTOP     DEFAULT=sway

# QT-related theming
QT_QPA_PLATFORMTHEME    DEFAULT=qt5ct

# FCITX input-related
#GLFW_IM_MODULE         DEFAULT=ibus
GLFW_IM_MODULE          DEFAULT=fcitx
GTK_IM_MODULE           DEFAULT=fcitx
INPUT_METHOD            DEFAULT=fcitx
XMODIFIERS              DEFAULT=@im=fcitx
IMSETTINGS_MODULE       DEFAULT=fcitx
QT_IM_MODULE            DEFAULT=fcitx
```

It doesn't work in Chromium, but apps like Telegram can use the input.





### Author thep0y on 5 Jun 2021

> Do you have a solution for this? I am currently using
> 
> .pam_environment
> 
> ```
> # Wayland compatibility
> QT_QPA_PLATFORM         DEFAULT=wayland
> CLUTTER_BACKEND         DEFAULT=wayland
> SDL_VIDEODRIVER         DEFAULT=wayland
> MOZ_ENABLE_WAYLAND      DEFAULT=1
> MOZ_WEBRENDER           DEFAULT=1
> XDG_SESSION_TYPE        DEFAULT=wayland
> XDG_CURRENT_DESKTOP     DEFAULT=sway
> 
> # QT-related theming
> QT_QPA_PLATFORMTHEME    DEFAULT=qt5ct
> 
> # FCITX input-related
> #GLFW_IM_MODULE         DEFAULT=ibus
> GLFW_IM_MODULE          DEFAULT=fcitx
> GTK_IM_MODULE           DEFAULT=fcitx
> INPUT_METHOD            DEFAULT=fcitx
> XMODIFIERS              DEFAULT=@im=fcitx
> IMSETTINGS_MODULE       DEFAULT=fcitx
> QT_IM_MODULE            DEFAULT=fcitx
> ```
> 
> It doesn't work in Chromium, but apps like Telegram can use the input.

[https://bugs.chromium.org/p/chromium/issues/detail?id=1183262](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262).  
This bug has not been fixed.





### Contributor tinywrkb on 5 Jun 2021

@darrist you should not use `.pam_environment` as by default it's not being read by PAM since v1.4.0, and most likely it will not be supported in the future.

> Do you have a solution for this?

Use the `Google Input Tools` extension instead of Fcitx or run Chromium as an X11/XWayland client.



 [OctopusET](https://github.com/OctopusET) mentioned this issue [on 13 Sep 2021](https://github.com/fcitx/fcitx5/issues/263#ref-issue-994312963)

[Chromium wayland IME 미지원 korean-input/issues#11](https://github.com/korean-input/issues/issues/11)

 Closed



### greenhat616 on 24 Nov 2021

[@tinywrkb](https://github.com/tinywrkb) [@thep0y](https://github.com/thep0y) As [https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2)referred, fcitx can work now by adding flag: `--gtk-version=4`





### greenhat616 on 24 Nov 2021

[![图片](https://user-images.githubusercontent.com/41122242/143155972-5fe519f1-36cc-4b77-8243-a659c18fa1a4.png)](https://user-images.githubusercontent.com/41122242/143155972-5fe519f1-36cc-4b77-8243-a659c18fa1a4.png)



### Contributor tinywrkb on 24 Nov 2021

[@greenhat616](https://github.com/greenhat616) that's with the kimpanel extension, so Gnome only. I'm not sure how is it with Plasma, but AFAIK, for anything wlroots based this isn't solved yet. See [bug report](https://bugs.chromium.org/p/chromium/issues/detail?id=1265118).





### greenhat616 on 24 Nov 2021

[@tinywrkb](https://github.com/tinywrkb) You're right. While i disabled the kimpanel extension, fcitx was not work.



### Member wengxt on 24 Nov 2021

It's not "not work", it's partially work (you can still type, but there will be no popup be shown.



### NgoHuy on 21 Feb

on Archlinux, gtk4 is required by chrome/chromium to make fcitx work.





### Contributor tinywrkb on 21 Feb

[@NgoHuy](https://github.com/NgoHuy) last time I tried (I'm back to XWayland), I needed also the `--gtk-version=4` flag.





### NgoHuy on 21 Feb

> [@NgoHuy](https://github.com/NgoHuy) last time I tried (I'm back to XWayland), I needed also the `--gtk-version=4` flag.

I meant this flag and gtk4 package.



### Contributor tinywrkb on 21 Feb

You might also want to use it with the IBus GTK4 IM module if you need the candidate box.



### ttys3 on 9 Apr

> [@tinywrkb](https://github.com/tinywrkb) [@thep0y](https://github.com/thep0y) As [bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2)referred, fcitx can work now by adding flag: `--gtk-version=4`

just tested  `--gtk-version=4` with Chrome and Edge, both works like a charm



### Contributor tinywrkb on 9 Apr

> just tested `--gtk-version=4` with Chrome and Edge, both works like a charm

Right, there was a fix somewhere in Fcitx5 or Chromium that made the candidate box work recently.  
Like IBus, there's still the issue of candidate box positioning.  
And sometimes, though now pretty rarely, the key repetition issue shows up.

From a user perspective, there's the problem GTK4 is opt-in, and it's not even listed in `chrome://flags`, while for Ozone there's now even an auto platform mode for that prioritize Wayland (`ozone-platform-hint` or the `chrome://flags` entry).

Also, it doesn't seem to work with Electron. Vieb 7.2.0, with Electron 18 that finally has Wayland fixed, crashes when trying to use GTK4 and output the error.

```
GTK 2/3 symbols detected. Using GTK 2/3 and GTK 4 in the same process is not supported
```

Chrome devs need to implement the Wayland protocol extensions, instead of delivering bad experience to the user.





### Aleksanaa on 23 Jun

> > [@tinywrkb](https://github.com/tinywrkb) [@thep0y](https://github.com/thep0y) As [bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2)referred, fcitx can work now by adding flag: `--gtk-version=4`
> 
> just tested `--gtk-version=4` with Chrome and Edge, both works like a charm

This does not work with the flatpak version.



### Contributor tinywrkb on 23 Jun

> > > [@tinywrkb](https://github.com/tinywrkb) [@thep0y](https://github.com/thep0y) As [bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2](https://bugs.chromium.org/p/chromium/issues/detail?id=1183262&q=ime%20wayland&can=2) referred, fcitx can work now by adding flag: `--gtk-version=4`
> > 
> > just tested `--gtk-version=4` with Chrome and Edge, both works like a charm
> 
> This does not work with the flatpak version.

See [flathub/org.chromium.Chromium.BaseApp#2](https://github.com/flathub/org.chromium.Chromium.BaseApp/pull/2)

 [mnixry](https://github.com/mnixry) mentioned this issue [on 22 Aug](https://github.com/fcitx/fcitx5/issues/263#ref-issue-916762053)

[Wayland (Sway) Support #292](https://github.com/fcitx/fcitx5/issues/292)

 Closed



### mnixry on 22 Aug

> Also, it doesn't seem to work with Electron. Vieb 7.2.0, with Electron 18 that finally has Wayland fixed, crashes when trying to use GTK4 and output the error.

It seems we must wait for [electron/electron#33690](https://github.com/electron/electron/issues/33690) to be solved 😢





### ttys3 19 days ago

wait [https://bugs.chromium.org/p/chromium/issues/detail?id=1039161](https://bugs.chromium.org/p/chromium/issues/detail?id=1039161)



Write Preview

Add heading textAdd bold text, <Cmd+b>Add italic text, <Cmd+i>

Add a quote, <Cmd+Shift+.>Add code, <Cmd+e>Add a link, <Cmd+k>

Add a bulleted list, <Cmd+Shift+8>Add a numbered list, <Cmd+Shift+7>Add a task list, <Cmd+Shift+l>

Directly mention a user or teamReference an issue, pull request, or discussionAdd saved reply

Attach files by dragging & dropping, selecting or pasting them.[](https://docs.github.com/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)Styling with Markdown is supported

Comment

Remember, contributions to this repository should follow our [GitHub Community Guidelines](https://docs.github.com/articles/github-community-guidelines).

Assignees

No one assigned

Labels

None yet

Projects

None yet

Milestone

No milestone

Development

No branches or pull requests

Notifications

Customize

Subscribe

You’re not receiving notifications from this thread.

10 participants



## Footer

[](https://github.com/ "GitHub")© 2022 GitHub, Inc.

### Footer navigation

-   [Terms](https://docs.github.com/en/github/site-policy/github-terms-of-service)
-   [Privacy](https://docs.github.com/en/github/site-policy/github-privacy-statement)
-   [Security](https://github.com/security)
-   [Status](https://www.githubstatus.com/)
-   [Docs](https://docs.github.com/)
-   [Contact GitHub](https://support.github.com/?tags=dotcom-footer)
-   [Pricing](https://github.com/pricing)
-   [API](https://docs.github.com/)
-   [Training](https://services.github.com/)
-   [Blog](https://github.blog/)
-   [About](https://github.com/about)