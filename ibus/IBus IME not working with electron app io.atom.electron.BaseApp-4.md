---
layout: post
tags:
- flathub
- electron
- gtk2
title: "IBus IME not working with electron app io.atom.electron.BaseApp-4"

---

# IBus IME not working with electron app io.atom.electron.BaseApp-4

[flathub](https://github.com/flathub)[io.atom.electron.BaseApp](https://github.com/flathub/io.atom.electron.BaseApp)

[IBus IME not working with electron app #4](https://github.com/flathub/io.atom.electron.BaseApp/issues/4#top) Closed

[chenzhiwei](https://github.com/chenzhiwei) opened this issue on 20 Oct 2018 · 7 comments

[chenzhiwei](https://github.com/chenzhiwei)

### chenzhiwei on 20 Oct 2018

Tested with the latest Slack and Atom editor, and use following command can make ibus IME work, please take a look at this issue.

```
 flatpak run with --env=GTK_IM_MODULE=xim io.atom.Atom
```

Related issue:

1.  Atom: [IBus IME not working io.atom.Atom#6](https://github.com/flathub/io.atom.Atom/issues/6)
2.  Slack: [Slack app doesn't work with ibus com.slack.Slack#21](https://github.com/flathub/com.slack.Slack/issues/21)
3.  VS code: [IBus IME not working com.visualstudio.code#24](https://github.com/flathub/com.visualstudio.code/issues/24)

cc [@fujiwarat](https://github.com/fujiwarat) Don't know why `GTK_IM_MODULE=ibus` does not work, could you comment on this?

Thanks!



[chenzhiwei](https://github.com/chenzhiwei) [chenzhiwei](https://github.com/chenzhiwei) mentioned this issue [on 24 Oct 2018](https://github.com/flathub/io.atom.electron.BaseApp/issues/4#ref-issue-300108756)

[Ibus intelligent pinyin not working in telegram-desktop snap apps telegramdesktop/tdesktop#4449](https://github.com/telegramdesktop/tdesktop/issues/4449)

 Closed

[juhp](https://github.com/juhp)

### juhp on 8 Nov 2018

Is it possible to add the ibus gtk2 immodule to this BaseApp?

[TingPing](https://github.com/TingPing)

Member

### TingPing on 9 Nov 2018

Well you are reporting it doesn't work with Gtk3 either, so its probably an Electron bug anyway.

[juhp](https://github.com/juhp)

### juhp on 9 Nov 2018

Discord works for me though, surprisingly - not sure why though.  
(See [[electron apps do not recognize ibus 1671]])

[TingPing](https://github.com/TingPing)

Member

### TingPing on 9 Nov 2018

I think that eliminates GTK being relevant, Discord is GTK2 and has no ibus module.

[juhp](https://github.com/juhp)

### juhp on 10 Nov 2018

Not sure, maybe we should just ignore Discord for now if it is an anomaly.

[fujiwarat](https://github.com/fujiwarat)

### fujiwarat on 8 Jan 2019

> [!NOTE]
> 
> I evaluated Atom and visualstudio in [[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]]  .  

The pure electron application works fine with ibus as I commented the issue.  
Note: they uses ibus in org.freedesktop.Sdk instead of org.freedesktop.Platform and need to port the ibus fix to the Flatpak.  
So I think this can be closed.

[fujiwarat](https://github.com/fujiwarat)

### fujiwarat on 8 Jan 2019

If you use Fedora, I put a fixed binary for the testing ; [https://fujiwara.fedorapeople.org/ibus/20190108/](https://fujiwara.fedorapeople.org/ibus/20190108/)

[TingPing](https://github.com/TingPing) [TingPing](https://github.com/TingPing) closed this as [completed](https://github.com/flathub/io.atom.electron.BaseApp/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 9 Jan 2019](https://github.com/flathub/io.atom.electron.BaseApp/issues/4#event-2061091907)

[SISheogorath](https://github.com/SISheogorath) [SISheogorath](https://github.com/SISheogorath) mentioned this issue [on 12 Mar 2019](https://github.com/flathub/io.atom.electron.BaseApp/issues/4#ref-issue-415208715)

 [[flathub-im.riot.Riot Can't use ibus IME 34]]Closed

[omichaelo](https://github.com/omichaelo) [omichaelo](https://github.com/omichaelo) mentioned this issue [on 10 Feb 2020](https://github.com/flathub/io.atom.electron.BaseApp/issues/4#ref-issue-562230865)

[input method fcitx not working flathub/io.atom.Atom#73](https://github.com/flathub/io.atom.Atom/issues/73)