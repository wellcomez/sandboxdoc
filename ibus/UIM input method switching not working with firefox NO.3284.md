---
layout: post
tags:
- fcitx
- firejail
- firefox
- uim
title: "UIM input method switching not working with firefox NO.3284"

---

[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public

[tedliosu](https://github.com/tedliosu) opened this issue on 19 Mar 2020 · 4 comments


[UIM input method switching not working with firefox](https://github.com/netblue30/firejail/issues/3284#top)#3284 Closed

[tedliosu](https://github.com/tedliosu) opened this issue on 19 Mar 2020 · 4 comments




### tedliosu on 19 Mar 2020

#### System Info
~~~
firejail version 0.9.56.2-LTS  
lsb_release -a:

No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.4 LTS
Release:	18.04
Codename:	bionic

uname -r: 5.3.0-42-generic  
sudo dpkg -l uim | tail -n 1 | cut -d" " -f3-20:

uim            1:1.8.6+gh20180114.64e3173-2build2 amd64

sudo dpkg -l xubuntu-desktop | tail -n 1 | cut -d" " -f3-12

xubuntu-desktop 2.225        amd64
~~~

#### Presenting Issue

I have already tried the options as suggested in [#2841](https://github.com/netblue30/firejail/issues/2841), with the local profile file as attached (I added ".log" to the end of the file name so I could upload it to GIthub): [firefox-common.local.log](https://github.com/netblue30/firejail/files/4351233/firefox-common.local.log) However, I am still unable to switch input methods by pressing CTRL+SHIFT+SPACE (the key binding I'd set for switching input methods with uim) whenever I run firefox with firejail. I checked to make sure that the key binding does work when I ran "firejail --noprofile firefox". Please let me know how I may adjust my firefox-common.local so I have the UIM input method switching functionality again when running firefox with firejail.



###  glitsj16 on 5 Apr 2020

Sadly I don't have a magic one-line answer to your question. And there haven't been any other suggestions made here. If the issue still persists, can you try letting firejail build up a profile on its own instead of using the provided one? Something like `firejail --build /usr/bin/firefox` should do if the firefox executable resides there on Ubuntu 18.04 LTS. Otherwise you'll need to pass it the full path to where it is located on your machine. And are there any errors showing in the firefox console when you try to switch input methods?

 [glitsj16](https://github.com/glitsj16) added the [question](https://github.com/netblue30/firejail/labels/question) Further information is requestedlabel [on 5 Apr 2020](https://github.com/netblue30/firejail/issues/3284#event-3201400078)



###  rusty-snake on 6 May 2020

I'm closing here due to inactivity, please fell free to reopen if you still have this issue.

 [rusty-snake](https://github.com/rusty-snake) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 6 May 2020](https://github.com/netblue30/firejail/issues/3284#event-3309146789)



### Author tedliosu on 10 Dec 2020

I've switched away from UIM since I originally created this issue due to its general user-unfriendliness on Ubuntu for a simplified Chinese input method (not related to firejail in any way). I recommend using fcitx and fcitx-googlepinyin instead (on Ubuntu at least) especially for its autocomplete feature (something which UIM still sorely lacks for Chinese input at least on Ubuntu 18.04 at the time of writing this post).

Nonetheless, in the time that I did manage to get UIM working with Firefox, I found that what ultimately did the trick was that I had to not only add the `ignore nodbus` directive to my "firefox-common.local" file, but also add a `noblacklist` directive for whichever directory that the background `uim-helper-server` process had created and added a special socket file to inside the directory `echo "/run/user/$(id -u)"` (run the command on a POSIX-compliant CLI to get the actual directory), on TOP of whatever I had added before to the ".local" file. However, after a severe PEBKAC caused deletion of over 5+ months worth of files (_ALWAYS_ stay in the habit of making regular backups of your existing data, as I have definitely learned the hard way), I have been unable to replicate my success with the aforementioned modification to my "firefox-common.local" file. Therefore, I no longer have the complete "firefox-common.local" file that made UIM work with Firefox, but I do not want to have to install UIM and go through the hassle all over again since I already have fcitx working with a firejailed firefox. But if by any luck I manage to dig back up that file again from the SSD image that contains my deleted files, then I will post the ".local" file's contents in this issue's thread for others to refer to.

Since it is unlikely I will ever be able to get back that file again, here is some advice for others that are trying to solve similar issues:

-   When checking to make sure that an issue isn't caused by a firejail profile or even firejail itself, not only run firejail with the --noprofile **but also** the --allow-debuggers option, especially if it's a progam which requires multiple processes to communicate with each other.
-   Given how much of setting up a firejail profile for custom programs entails fine-tuning file/directory access directives and dbus directives (the latter only applies for the latest non-LTS firejail version at the time of writing this), use programs like `lsof` and `dbus-monitor` to determine which files another given program needs access to during operation _before_ firejailing the program (if practical) is _highly recommended_ as it will reveal most of the files, directories, and dbus sockets that the program needs access to during runtime.  
    - Any additional files, directories, and sockets the program may need can then be determined (_after_ making adjustments based on feedback from programs like `lsof` and `dbus-monitor`) by running `firejail [insert program name with arguments here]` on the CLI and watching output to stderr, the systemd journal (if applicable; recommended way to access it is running `journalctl -x -b`), the log shown by the command `echo "$HOME/.xsession-errors"` (if applicable), and any additional logging done in the firejailed program's own logs.

Finally, as I've gotten fcitx to work with a firejailed firefox, here's the contents of the "firefox-common.local" file which I am using right now:
~~~bash

# Original file location: /etc/firejail/firefox-common.local 
# local firefox firejail overrides for enabling functionality such as fcitx input methods
ignore nodbus
ignore nodvd
ignore notv

# quiet firefox complaints about not being able to save dconf info
noblacklist ${HOME}/.cache/dconf
noblacklist ${HOME}/.dbus
noblacklist ${HOME}/.dbus/session-bus
noblacklist /var/run/user/*
noblacklist /run/user/*
noblacklist /var/run/user/*/bus
noblacklist /run/user/*/bus
noblacklist /var/run/user/*/dbus-1
noblacklist /run/user/*/dbus-1
noblacklist /var/run/user/*/dbus-1/services
noblacklist /run/user/*/dbus-1/services
noblacklist /var/run/user/*/dconf
noblacklist /run/user/*/dconf
noblacklist /var/run/user/*/dconf/user
noblacklist /run/user/*/dconf/user
noblacklist /var/run/user/*/pulse
noblacklist /run/user/*/pulse

mkdir ${HOME}/.cache/dconf
mkdir ${HOME}/.dbus
mkdir ${HOME}/.dbus/session-bus

whitelist ${HOME}/.cache/dconf
whitelist ${HOME}/.dbus
whitelist ${HOME}/.dbus/session-bus
whitelist /var/run

# Do not permit access to these special user run directories
blacklist /run/user/*/gvfs
blacklist /run/user/*/keyring
blacklist /run/user/*/gnupg
~~~
I understand that not everything I mentioned here is 100% relevant to the issue I had originally created or even firejail itself, but I wanted to leave enough information for others who may encounter similar issues in the future to reference if ever needed. Hopefully this post isn't also too lengthy.

Some additional (hopefully relevant) info for those who may stumble upon this issue thread in the future, i.e. command outputs to /dev/stdout in an interactive bash session for my current setup as of writing this post:

**`firejail --version | head -n1`** ─
~~~

firejail version 0.9.56.2-LTS
~~~

**`lsb_release -a`** ─

~~~
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.5 LTS
Release:	18.04
Codename:	bionic
~~~

**`uname -r`** ─

~~~
5.4.0-56-generic
~~~

**`fcitx --version`** ─

~~~
fcitx version: 4.2.9.6
~~~

**`dpkg -l fcitx-googlepinyin | tail -n1 | tr -s " " | cut -d" " -f2-3`** ─

~~~
fcitx-googlepinyin:amd64 0.1.6-3
~~~

**`xfdesktop --version`** ─
~~~
This is xfdesktop version 4.12.3, running on Xfce 4.14.
Built with GTK+ 2.24.31, linked with GTK+ 2.24.32.
Build options:
    Desktop Menu:        enabled
    Desktop Icons:       enabled
    Desktop File Icons:  enabled
~~~



### Author tedliosu on 10 Dec 2020

Oh and also sorry that I didn't see this before making my previous post, but I guess my original issue was also partly addressed by this firejail update made recently after I had commented about my issue:[[add whitelist items for uim NO.3587]] [#3587](https://github.com/netblue30/firejail/pull/3587)


