---
date: 2023-04-12 22:37
title: 无法在vscode中唤起输入法 no.458
tags:
- fcitx
- firejail
- firefox
---

[fcitx](https://github.com/fcitx)/[fcitx5](https://github.com/fcitx/fcitx5)Public
[无法在vscode中唤起输入法 #458](https://github.com/fcitx/fcitx5/issues/458)Open

[videni](https://github.com/videni) opened this issue on 8 Mar · 7 comments




### videni on 8 Mar

**Describe the bug**

无法在vscode中唤起输入法

**To Reproduce**  
无

**Expected behavior**  
无论是用鼠标切换输入法为汉语，还是通过快捷键，都无法唤起输入法。但在其它软件中可以，比如飞书，Microsoft edge.

**Desktop (please complete the following information):**

-   Desktop: ubuntu 21.10 gnome 40
-   Display server type:Wayland
-   The output of fcitx5-diagnose
-   Addons: , fcitx5-chinese-addons
-   Vscode: 1.65

[[无法在vscode中唤起输入法 no.458-systemlog]]

```markdown
 System Info:
1.  `uname -a`:

        Linux vidy-fx 5.13.0-30-generic #33-Ubuntu SMP Fri Feb 4 17:03:31 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux

2.  `lsb_release -a`:

        No LSB modules are available.
        Distributor ID:	Ubuntu
        Description:	Ubuntu 21.10
        Release:	21.10
        Codename:	impish

3.  `lsb_release -d`:

        Description:	Ubuntu 21.10

4.  `/etc/lsb-release`:

        DISTRIB_ID=Ubuntu
        DISTRIB_RELEASE=21.10
        DISTRIB_CODENAME=impish
        DISTRIB_DESCRIPTION="Ubuntu 21.10"

5.  `/etc/os-release`:

        PRETTY_NAME="Ubuntu 21.10"
        NAME="Ubuntu"
        VERSION_ID="21.10"
        VERSION="21.10 (Impish Indri)"
        VERSION_CODENAME=impish
        ID=ubuntu
        ID_LIKE=debian
        HOME_URL="https://www.ubuntu.com/"
        SUPPORT_URL="https://help.ubuntu.com/"
        BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
        PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
        UBUNTU_CODENAME=impish

6.  Desktop Environment:

    Desktop environment is `gnome3`.

7.  Bash Version:

        BASH_VERSION='5.1.8(1)-release'

# Environment:
1.  DISPLAY:

        DISPLAY=':0'

2.  Keyboard Layout:

    1.  `setxkbmap`:

            xkb_keymap {
            	xkb_keycodes  { include "evdev+aliases(qwerty)"	};
            	xkb_types     { include "complete"	};
            	xkb_compat    { include "complete"	};
            	xkb_symbols   { include "pc+us+inet(evdev)"	};
            	xkb_geometry  { include "pc(pc105)"	};
            };

    2.  `xprop`:

            _XKB_RULES_NAMES(STRING) = "evdev", "pc105", "us", "", ""

3.  Locale:

    1.  All locale:

            C
            C.UTF-8
            en_AG
            en_AG.utf8
            en_AU.utf8
            en_BW.utf8
            en_CA.utf8
            en_DK.utf8
            en_GB.utf8
            en_HK.utf8
            en_IE.utf8
            en_IL
            en_IL.utf8
            en_IN
            en_IN.utf8
            en_NG
            en_NG.utf8
            en_NZ.utf8
            en_PH.utf8
            en_SG.utf8
            en_US.utf8
            en_ZA.utf8
            en_ZM
            en_ZM.utf8
            en_ZW.utf8
            POSIX
            zh_CN.utf8

    2.  Current locale:

            LANG=en_US.UTF-8
            LANGUAGE=
            LC_CTYPE="en_US.UTF-8"
            LC_NUMERIC=zh_CN.UTF-8
            LC_TIME=zh_CN.UTF-8
            LC_COLLATE="en_US.UTF-8"
            LC_MONETARY=zh_CN.UTF-8
            LC_MESSAGES="en_US.UTF-8"
            LC_PAPER=zh_CN.UTF-8
            LC_NAME=zh_CN.UTF-8
            LC_ADDRESS=zh_CN.UTF-8
            LC_TELEPHONE=zh_CN.UTF-8
            LC_MEASUREMENT=zh_CN.UTF-8
            LC_IDENTIFICATION=zh_CN.UTF-8
            LC_ALL=

4.  Directories:

    1.  Home:

            /home/vidy

    2.  `${XDG_CONFIG_HOME}`:

        Environment variable `XDG_CONFIG_HOME` is not set.

        Current value of `XDG_CONFIG_HOME` is `~/.config` (`/home/acme/.config`).

    3.  Fcitx5 Settings Directory:

        Current fcitx5 settings directory is `~/.config/fcitx5` (`/home/acme/.config/fcitx5`).

5.  Current user:

    The script is run as vidy (1000).

# Fcitx State:
1.  executable:

    Found fcitx5 at `/usr/bin/fcitx5`.

2.  version:

    Fcitx version: `5.0.5`

3.  process:

    Found 1 fcitx5 process:

           5366 fcitx5

4.  `fcitx5-remote`:

    `fcitx5-remote` works properly.

5.  DBus interface:

    Using `dbus-send` to check dbus.

    Owner of DBus name `org.fcitx.Fcitx5` is `:1.98`.

    PID of DBus name `org.fcitx.Fcitx5` owner is `5366`.

# Fcitx Configure UI:
1.  Config Tool Wrapper:

    Found fcitx5-configtool at `/usr/bin/fcitx5-configtool`.

2.  Config GUI for qt:

    Found `fcitx5-config-qt` at `/usr/bin/fcitx5-config-qt`.

3.  Config GUI for kde:

    **`kcmshell5` not found.**

# Frontends setup:
## Xim:
1.  `${XMODIFIERS}`:

    Environment variable XMODIFIERS is set to "@im=fcitx" correctly.
    Xim Server Name from Environment variable is fcitx.

2.  XIM_SERVERS on root window:

    Xim server name is the same with that set in the environment variable.

## Qt:
1.  qt4 - `${QT4_IM_MODULE}`:

    Environment variable QT_IM_MODULE is set to "fcitx" correctly.

2.  qt5 - `${QT_IM_MODULE}`:

    Environment variable QT_IM_MODULE is set to "fcitx" correctly.

3.  Qt IM module files:

    Found fcitx5 qt5 module: `/lib/x86_64-linux-gnu/fcitx5/qt5/libfcitx-quickphrase-editor5.so`.
    Found fcitx5 im module for qt5: `/lib/x86_64-linux-gnu/qt5/plugins/platforminputcontexts/libfcitx5platforminputcontextplugin.so`.
    **Cannot find fcitx5 input method module for Qt4.**

## Gtk:
1.  gtk - `${GTK_IM_MODULE}`:

    Environment variable GTK_IM_MODULE is set to "fcitx" correctly.

2.  `gtk-query-immodules`:

    1.  gtk 2:

        **Cannot find `gtk-query-immodules` for gtk 2**

        **Cannot find fcitx5 im module for gtk 2.**

    2.  gtk 3:

        **Cannot find `gtk-query-immodules` for gtk 3**

        **Cannot find fcitx5 im module for gtk 3.**

3.  Gtk IM module cache:

    1.  gtk 2:

        Found immodules cache for gtk `2.24.33` at `/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules.cache`.
        Version Line:

            # Created by /usr/lib/x86_64-linux-gnu/libgtk2.0-0/gtk-query-immodules-2.0 from gtk+-2.24.33

        **Failed to find fcitx5 in immodule cache at `/lib/x86_64-linux-gnu/gtk-2.0/2.10.0/immodules.cache`**

        **Cannot find fcitx5 im module for gtk 2 in cache.**

    2.  gtk 3:

        Found immodules cache for gtk `3.24.30` at `/lib/x86_64-linux-gnu/gtk-3.0/3.0.0/immodules.cache`.
        Version Line:

            # Created by /usr/lib/x86_64-linux-gnu/libgtk-3-0/gtk-query-immodules-3.0 from gtk+-3.24.30

        Found fcitx5 im modules for gtk `3.24.30`.

            "/usr/lib/x86_64-linux-gnu/gtk-3.0/3.0.0/immodules/im-fcitx5.so" 
            "fcitx" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 
            "fcitx5" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 

    3.  gtk 4:

        Found immodules cache for gtk `4` at `/lib/x86_64-linux-gnu/gtk-4.0/4.0.0/immodules/giomodule.cache`.
        Found fcitx5 im modules for gtk `4`.

            libim-fcitx5.so: gtk-im-module

4.  Gtk IM module files:

    1.  gtk 2:

        All found Gtk 2 immodule files exist.

    2.  gtk 3:

        All found Gtk 3 immodule files exist.

    3.  gtk 4:

        All found Gtk 4 immodule files exist.

# Configuration:
## Fcitx Addons:
1.  Addon Config Dir:

    Found fcitx5 addon config directory: `/usr/share/fcitx5/addon`.

2.  Addon List:

    1.  Found 26 enabled addons:

            Simplified and Traditional Chinese Translation
            Classic User Inteface
            Clipboard
            Cloud Pinyin
            DBus
            DBus Frontend
            Emoji
            Fcitx4 Frontend
            Full width character
            IBus Frontend
            Input method selector
            Keyboard
            KDE Input Method Panel
            Status Notifier
            Notification
            Pinyin
            Extra Pinyin functionality
            Punctuation
            Quick Phrase
            Spell
            Table
            Unicode
            Wayland
            Wayland Input method frontend
            XCB
            X Input Method Frontend

    2.  Found 0 disabled addons:

3.  Addon Libraries:

    All libraries for all addons are found.

4.  User Interface:

    Found 2 enabled user interface addons:

        Classic User Inteface
        KDE Input Method Panel

# Log:
1.  `date`:

        2022年 03月 08日 星期二 17:00:38 CST

2.  `/home/acme/.config/fcitx5/crash.log`:

```



### Author videni on 9 Mar

这个问题只在通过snap安装的vscode发生，解决方法，直接安装vscode官网的。

[videni](https://github.com/videni) closed this as [completed](https://github.com/fcitx/fcitx5/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 9 Mar](https://github.com/fcitx/fcitx5/issues/458#event-6207484041)

 [videni](https://github.com/videni) reopened this [on 9 Mar](https://github.com/fcitx/fcitx5/issues/458#event-6207484340)



### CoelacanthusHex on 25 Mar

我认为，有两个可能的原因，snap 版本的 VSCode 没有集成 fcitx 的 GTK 输入法模块，或者 snap 的某些机制（比如沙盒）阻止了输入法模块与外面的输入法的通信



### san-Hei on 25 Mar

可以先查一下这个进程加载的lib，里边有没有fcitx的插件



### wakaka6 on 25 May

> 我认为，有两个可能的原因，snap 版本的 VSCode 没有集成 fcitx 的 GTK 输入法模块，或者 snap 的某些机制（比如沙盒）阻止了输入法模块与外面的输入法的通信

请问一下有办法在沙箱环境中使用输入法吗，比如运行这条命令

firejail firefox

此时在firefox中就无法唤起输入法了，还有通过lxd运行的GUI应用也同样无法使用



### CoelacanthusHex on 25 May

[@wakaka6](https://github.com/wakaka6) firejail 的默认规则阻止了 dbus 通信（通过 uds）



### jcleng on 15 Jun

😓裂开了，nixos用户，升级22.02版本，不知道是VS CODE版本升级还是输入法版本升级，vscode调不起来输入法了，解决不了。这怎么玩，linux依赖问题解决不了就算了，现在升个级输入法也没了，干活的机器输入法用不了，我擦



### CoelacanthusHex on 15 Jul

> 😓裂开了，nixos用户，升级22.02版本，不知道是VS CODE版本升级还是输入法版本升级，vscode调不起来输入法了，解决不了。这怎么玩，linux依赖问题解决不了就算了，现在升个级输入法也没了，干活的机器输入法用不了，我擦

Nix 相关的话可以来 Nix 中文群问  
[https://matrix.to/#/#nixos_zhcn:matrix.org](https://matrix.to/#/#nixos_zhcn:matrix.org)  
或者  
[https://t.me/nixos_zhcn](https://t.me/nixos_zhcn)

[h3fang](https://github.com/h3fang) mentioned this issue [19 days ago](https://github.com/fcitx/fcitx5/issues/458#ref-issue-1406369467)

[[vscode在wayland下无法切换输入法 no.619]](#619)(https://github.com/fcitx/fcitx5/issues/619)Closed

