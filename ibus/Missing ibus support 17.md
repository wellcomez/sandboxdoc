---
layout: post
tags:
- electron
- flathub
- flatpak
- signal
- ibus
title: "Missing ibus support 17"

---

# Missing ibus support 17
[[flat](https://github.com/flathub/org.signal.Signal)](https://github.com/flathub/org.signal.Signal)
[Missing ibus support #17](https://github.com/flathub/org.signal.Signal/issues/17)Closed

[SISheogorath](https://github.com/SISheogorath) opened this issue on 27 Feb 2018 · 6 comments




### SISheogorath on 27 Feb 2018

Right now signal doesn't support any ibus input, which is needed, when you talk with people who don't use non-western languages.

It may comes from the electron base, at the ibus support is also missing on Slack.

Would be nice to solve the problem.



### Member nedrichards on 27 Feb 2018

[@SISheogorath](https://github.com/SISheogorath) which version of ibus do you have installed?

> ibus version

Needs to show [1.5.17 or later](https://github.com/flatpak/flatpak/issues/675). I haven't tested this myself



### Author SISheogorath on 27 Feb 2018

I have 1.5.17 installed. I'm on latest update state on Fedora.



### Member nedrichards on 28 Feb 2018

Hrm, does the ibus test work ok for you?

[https://github.com/ibus/ibus/blob/master/test/org.test.IBus.json](https://github.com/ibus/ibus/blob/master/test/org.test.IBus.json)

> flatpak-builder --force-clean app org.test.IBus.json  
> flatpak-builder --run --nofilesystem=host app org.test.IBus.json zenity --entry

As you say, I think it's very likely that this is an Electron thing - I just want to make sure that it's not more widespread.

[nedrichards](https://github.com/nedrichards) mentioned this issue [on 28 Feb 2018](https://github.com/flathub/org.signal.Signal/issues/17#ref-pullrequest-299043352)[Open source build of Visual Studio Code flathub/flathub#272](https://github.com/flathub/flathub/pull/272)Closed



### Author SISheogorath on 1 Mar 2018

In the ibus test it works. Even when the needed fonts are missing in the frontend:

[![image](https://user-images.githubusercontent.com/8719867/36797853-50ea72e8-1ca9-11e8-95cc-57df4982f3f7.png)](https://user-images.githubusercontent.com/8719867/36797853-50ea72e8-1ca9-11e8-95cc-57df4982f3f7.png)

```shell
flatpak-builder --run --nofilesystem=host ibus-test org.test.IBus.json zenity --entry

** (zenity:2): WARNING **: Couldn't connect to accessibility bus: Failed to connect to socket /tmp/dbus-MDlzxEZ4o9: Connection refused
Gtk-Message: GtkDialog mapped without a transient parent. This is discouraged.
谢谢你
```

 [J-J-Chiarella](https://github.com/J-J-Chiarella) mentioned this issue [on 14 May 2018](https://github.com/flathub/org.signal.Signal/issues/17#ref-issue-322723490)[electron apps do not recognize ibus flatpak/flatpak#1671](https://github.com/flatpak/flatpak/issues/1671) Closed



### Author SISheogorath on 24 Nov 2018

Just tested the workaround mentioned in the [flatpak/flatpak#1671](https://github.com/flatpak/flatpak/issues/1671):

[[electron apps do not recognize ibus 1671]]

`flatpak run --env=GTK_IM_MODULE=xim org.signal.Signal` and as it turns out, this results in a freeze of the entire application when starting to type a word using ibus. So still quite imperfect.






### SISheogorath on 1 Apr 2019

Fixed with latest runtime :) 🎉

 [SISheogorath](https://github.com/SISheogorath) closed this as [completed](https://github.com/flathub/org.signal.Signal/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 1 Apr 2019](https://github.com/flathub/org.signal.Signal/issues/17#event-2242953619)
