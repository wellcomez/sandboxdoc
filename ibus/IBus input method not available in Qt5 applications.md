---
layout: post
tags:
- snapd
- qt
title: "IBus input method not available in Qt5 applications"

---

# IBus input method not available in Qt5 applications
 [IBus input method not available in Qt5 applications](https://forum.snapcraft.io/t/ibus-input-method-not-available-in-qt5-applications/11712)

[Lin-Buo-Ren](https://forum.snapcraft.io/u/Lin-Buo-Ren)[Jun '19](https://forum.snapcraft.io/t/ibus-input-method-not-available-in-qt5-applications/11712)

In Qt5 application communicate with IBus via DBus calls, which is currently not implemented in [the `desktop-legacy` security confinement interface 6](https://github.com/snapcore/snapd/blob/master/interfaces/builtin/desktop_legacy.go).

Refer [https://github.com/qt/qtbase/tree/HEAD/src/plugins/platforminputcontexts/ibus 23](https://github.com/qt/qtbase/tree/HEAD/src/plugins/platforminputcontexts/ibus) for the current implementation.

-   [Compatibility with CJKV input method frameworks1](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)
[[Compatibility with CJKV input method frameworks]]

[jdstrand](https://forum.snapcraft.io/u/jdstrand) [Jun '19](https://forum.snapcraft.io/t/ibus-input-method-not-available-in-qt5-applications/11712/2)

ibus _is_ implemented in desktop_legacy.go: [https://github.com/snapcore/snapd/blob/master/interfaces/builtin/desktop_legacy.go#L116 23](https://github.com/snapcore/snapd/blob/master/interfaces/builtin/desktop_legacy.go#L116). Are you seeing security policy violations when this interface is connected? If so, can you describe steps to reproduce?