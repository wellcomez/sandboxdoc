---
layout: post
tags:
- firejail
- ibus
title: "BUS-WARNING NO.410"

---

# BUS-WARNING NO.410

[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public
[IBUS-WARNING #410](https://github.com/netblue30/firejail/issues/410#start-of-content) Closed

[pazinthru](https://github.com/pazinthru) opened this issue on 6 Apr 2016 · 8 comments




### pazinthru on 6 Apr 2016

Ref: Ubuntu 15.10 - Unity desktop  
Firejail Versions: 0.9.38 and 0.9.40-rc1

Following today's updates, my keyboard stops working within Firefox, and I receive the following errors when starting Firefox sandboxed:

(firefox:2): IBUS-WARNING **: Unable to connect to ibus: Could not connect: Connection refused

(firefox:2): IBUS-WARNING **: Events queue growing too big, will start to drop.

My 15.10 Lubuntu, UbuntuMATE, and Xubuntu virtual machines appear to be working normally.

 [netblue30](https://github.com/netblue30) added the [bug](https://github.com/netblue30/firejail/labels/bug) Something isn't workinglabel [on 7 Apr 2016](https://github.com/netblue30/firejail/issues/410#event-618230101)



### pyamsoft on 28 Apr 2016

This is a possible duplicate of the issue in [#116](https://github.com/netblue30/firejail/issues/116)



### Contributor pirate486743186 on 10 May 2016

Same in Ubuntu 16.04



### Anderath on 30 May 2016

I can second this issue on ubuntu 16.04.

I get the same ibus error message and have no keyboard input into any browser I attempt to use while in a firejail launch fashion.

 [Anderath](https://github.com/Anderath) mentioned this issue [on 30 May 2016](https://github.com/netblue30/firejail/issues/410#ref-issue-157455006)

[Ubuntu 16.04 -- No sound #547](https://github.com/netblue30/firejail/issues/547)

 Closed



### cwmke on 1 Dec 2016

~~Same on Arch with grsecurity kernel and pax restrictions enabled.~~

Edit: grsecurity is no longer in Arch but the same issue still exists with the vanilla kernel.



### Owner netblue30 on 3 Dec 2016

I'll reopen it, thanks.



### cwmke on 10 Aug 2017

I've been using Fedora lately without this issue happening. Tonight I installed the nvidia drivers and the problem started again. Maybe this is the cause?



###  Fred-Barclay on 19 Apr 2018

[@cwmke](https://github.com/cwmke) [@pazinthru](https://github.com/pazinthru) [@pirate486743186](https://github.com/pirate486743186) [@Anderath](https://github.com/Anderath) Are you still seeing this? If so, can you try [#1810 (comment)](https://github.com/netblue30/firejail/issues/1810#issuecomment-382586391) ?  
Thanks!  
Fred



###  chiraag-nataraj on 26 Jul 2018

Closing for inactivity. [@cwmke](https://github.com/cwmke) [@pazinthru](https://github.com/pazinthru) [@pirate486743186](https://github.com/pirate486743186) [@Anderath](https://github.com/Anderath), please feel free to reopen.

 [chiraag-nataraj](https://github.com/chiraag-nataraj) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 26 Jul 2018](https://github.com/netblue30/firejail/issues/410#event-1753821875)


