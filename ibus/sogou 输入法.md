---
layout: post
tags:
- fctx
title: sogou 输入法

---


# 安装指南
https://shurufa.sogou.com/linux/guide

Ubuntu搜狗输入法安装指南

搜狗输入法已支持Ubuntu1604、1804、1910、2004、2010

各系统安装步骤可能略有不同

1、添加中文语言支持

打开 系统设置——区域和语言——管理已安装的语言——在“语言”tab下——点击“添加或删除语言”

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27387%27%20height=%27514%27/%3e)![打开 系统设置——区域和语言——管理已安装的语言——在“语言”tab下——点击“添加或删除语言”](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-1.8106d423.jpg&w=828&q=75)

弹出“已安装语言”窗口，勾选中文（简体），点击应用

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27549%27%20height=%27488%27/%3e)![弹出“已安装语言”窗口，勾选中文（简体），点击应用](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-2.4afc85d1.jpg&w=1200&q=75)

回到“语言支持”窗口，在键盘输入法系统中，选择“fcitx”

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27394%27%20height=%27512%27/%3e)![回到“语言支持”窗口，在键盘输入法系统中，选择“fcitx”](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-3.79400b61.jpg&w=828&q=75)

注:

如果在键盘输入法系统中，没有“fcitx”选项时，建议先打开终端手动安装

fcitx：sudo apt-get install fcitx

等安装成功之后再执行上述步骤

点击“应用到整个系统”，关闭窗口，重启电脑

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27387%27%20height=%27508%27/%3e)![如果在键盘输入法系统中，没有“fcitx”选项时，建议先打开终端手动安装fcitx：sudo apt-get install fcitx等安装成功之后再执行上述步骤点击“应用到整个系统”，关闭窗口，重启电脑](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fstep-4.c5301a59.jpg&w=828&q=75)

2、通过命令行安装搜狗输入法

sudo dpkg -i sogoupinyin_版本号_amd64.deb

注:

如果安装过程中提示缺少相关依赖，则执行如下命令解决：

sudo apt -f install

3、注销计算机即可正常使用搜狗输入法

Ubuntu20.04安装搜狗输入法步骤

1、更新源

在终端执行 sudo apt update

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27743%27%20height=%27494%27/%3e)![在终端执行 sudo apt update](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp5.de1f83bf.png&w=1920&q=75)

2、安装fcitx输入法框架

1. 在终端输入 sudo apt install fcitx

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27749%27%20height=%27492%27/%3e)![1. 在终端输入 sudo apt install fcitx](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp6.41fbe1d2.png&w=1920&q=75)

2. 设置fcitx为系统输入法

点击左下角菜单选择语言支持，将语言选择为fcitx（如下图二）

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%271477%27%20height=%271220%27/%3e)![点击左下角菜单选择语言支持，将语言选择为fcitx（如下图二）](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp7.d6d18871.png&w=3840&q=75)![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27536%27%20height=%27515%27/%3e)![点击左下角菜单选择语言支持，将语言选择为fcitx（如下图二）](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp8.7e2ac26c.png&w=1080&q=75)

3. 设置fcitx开机自启动

在终端执行sudo cp /usr/share/applications/fcitx.desktop /etc/xdg/autostart/

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27741%27%20height=%27491%27/%3e)![在终端执行sudo cp /usr/share/applications/fcitx.desktop /etc/xdg/autostart/](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp9.255a6b8f.png&w=1920&q=75)

4. 卸载系统ibus输入法框架

在终端执行 sudo apt purge ibus

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27739%27%20height=%27484%27/%3e)![在终端执行 sudo apt purge ibus](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp10.7f8c5fb2.png&w=1920&q=75)

3、安装搜狗输入法

1. 在官网下载搜狗输入法安装包，并安装，安装命令 sudo dpkg -i 安装包名

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%271058%27%20height=%27691%27/%3e)![1. 在官网下载搜狗输入法安装包，并安装，安装命令 sudo dpkg -i 安装包名](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp11.df9d31a8.png&w=3840&q=75)

2. 安装输入法依赖

在终端执行

sudo apt install libqt5qml5 libqt5quick5 libqt5quickwidgets5 qml-module-qtquick2

sudo apt install libgsettings-qt1

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%271054%27%20height=%27694%27/%3e)![安装输入法依赖](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp12.64335313.png&w=3840&q=75)

4、重启电脑、调出输入法

1.重启电脑

2.查看右上角，可以看到“搜狗”字样，在输入窗口即可且出搜狗输入法。

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27299%27%20height=%27461%27/%3e)![查看右上角，可以看到“搜狗”字样，在输入窗口即可且出搜狗输入法](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp13.512031f9.png&w=640&q=75)

3. 没有“搜狗”字样，选择配置，将搜狗加入输入法列表即可

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27279%27%20height=%27412%27/%3e)![3. 没有“搜狗”字样，选择配置，将搜狗加入输入法列表即可](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp14.91e727f9.png&w=640&q=75)![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%27951%27%20height=%27534%27/%3e)![3. 没有“搜狗”字样，选择配置，将搜狗加入输入法列表即可](https://shurufa.sogou.com/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fhelp15.c14aafcd.png&w=1920&q=75)

至此，搜狗输入法安装完毕

![](data:image/svg+xml,%3csvg%20xmlns=%27http://www.w3.org/2000/svg%27%20version=%271.1%27%20width=%271023%27%20height=%27641%27/%3e)![至此，搜狗输入法安装完毕](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

商务合作咨询

合作邮箱: [hci_bd@tencent.com](mailto:hci_bd@tencent.com)

问题反馈&建议

联系邮箱: [IMETS@tencent.com](mailto:IMETS@tencent.com)

输入法公众号

官方微博

© 2022 SOGOU.COM 京ICP备11001839号-1[服务协议](https://shouji.sogou.com/wap/htmls/privacy_policy_new.html)|[隐私政策](https://corp.sogou.com/private.html#private-zh)