#include <dbus/dbus.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

/*
cc $(pkg-config --cflags --libs dbus-glib-1) -o dbus-get-hello dbus-get-hello.c && ./dbus-get-hello
*/

static DBusHandlerResult
filter_func (DBusConnection *connection,
             DBusMessage *message,
             void *user_data)
{
	dbus_bool_t handled = FALSE;
	char *signal_text = NULL;

	if (dbus_message_is_signal (message, "org.test.DBus.Example", "TestSignal")) {
		DBusError dberr;

		dbus_error_init (&dberr);
		dbus_message_get_args (message, &dberr, DBUS_TYPE_STRING, &signal_text, DBUS_TYPE_INVALID);
		if (dbus_error_is_set (&dberr)) {
			fprintf (stderr, "Error getting message args: %s", dberr.message);
			dbus_error_free (&dberr);
		} else {
			DBusConnection *dbconn = (DBusConnection*) user_data;

			printf ("Received TestSignal with value of: '%s'\n", signal_text);

			handled = TRUE; } } return (handled ? DBUS_HANDLER_RESULT_HANDLED : DBUS_HANDLER_RESULT_NOT_YET_HANDLED);
}


int main (int argc, char *argv[])
{
	DBusError dberr;
	DBusConnection *dbconn;

	dbus_error_init (&dberr);
	dbconn = dbus_bus_get (DBUS_BUS_SESSION, &dberr);
	if (dbus_error_is_set (&dberr)) {
		fprintf (stderr, "getting session bus failed: %s\n", dberr.message);
		dbus_error_free (&dberr);
		return EXIT_FAILURE;
	}

	dbus_bus_request_name (dbconn, "org.test.DBus.Example",
	                       DBUS_NAME_FLAG_REPLACE_EXISTING, &dberr);
	if (dbus_error_is_set (&dberr)) {
		fprintf (stderr, "requesting name failed: %s\n", dberr.message);
		dbus_error_free (&dberr);
		return EXIT_FAILURE;
	}

	if (!dbus_connection_add_filter (dbconn, filter_func, NULL, NULL))
		return EXIT_FAILURE;

	dbus_bus_add_match (dbconn,
	                    "type='signal',interface='org.test.DBus.Example'",
	                    &dberr);

	if (dbus_error_is_set (&dberr)) {
		fprintf (stderr, "Could not match: %s", dberr.message);
		dbus_error_free (&dberr);
		return EXIT_FAILURE;
	}


	while (dbus_connection_read_write_dispatch (dbconn, -1))
		; /* empty loop body */


	return EXIT_SUCCESS;
}


