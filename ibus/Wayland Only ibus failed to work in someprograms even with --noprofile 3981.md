---
layout: post
tags:
- firejail
- ibus
title: "Wayland Only ibus failed to work in someprograms even with --noprofile 3981"

---

# Wayland Only ibus failed to work in someprograms even with --noprofile 3981
[filejail](https://github.com/netblue30/firejail)
[Wayland Only: ibus failed to work in some programs even with --noprofile #3981](https://github.com/netblue30/firejail/issues/3981#top) Closed


[ixuu153](https://github.com/ixuu153) opened this issue on 13 Feb 2021 · 6 comments


### ixuu153 on 13 Feb 2021

IBus input method framework does not response for some applications like signal-desktop and tor-browser, even with `--noprofile` option. The problem only happens on Wayland. It works fine under X11.

#### Environment

-   openSUSE Tumbleweed (latest snapshot)
-   Firejail version 0.9.64.2, installed from official repository

#### Steps to reproduce (tor-browser as example)

1.  Download and unpack tor-browser bundle from [official website](https://www.torproject.org/download)
2.  Execute the application with command:

```
$ firejail --noprofile /opt/tor-browser_en/Browser/start-tor-browser --verbose
```

3.  After tor browser started, switch ibus between different input methods, e.g. English(US) and Chinese.

#### Result

Keyboard does not work for **BOTH** input methods.  
See **debug output** bellow for error message.

#### Checklist

-    The profile (and redirect profile if exists) hasn't already been fixed [upstream](https://github.com/netblue30/firejail/tree/master/etc).
-    The program has a profile. (If not, request one in `https://github.com/netblue30/firejail/issues/1139`)
-    I have performed a short search for similar issues (to avoid opening a duplicate).
-    If it is a AppImage, `--profile=PROFILENAME` is used to set the right profile.
-    Used `LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8 PROGRAM` to get english error-messages.
-    I'm aware of `browser-allow-drm yes`/`browser-disable-u2f no` in `firejail.config` to allow DRM/U2F in browsers.

 debug output 


###  rusty-snake on 13 Feb 2021

Duplicate of [#3379](https://github.com/netblue30/firejail/issues/3379)

[rusty-snake](https://github.com/rusty-snake) marked this as a duplicate of [#3379](https://github.com/netblue30/firejail/issues/3379 "Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04") [on 13 Feb 2021](https://github.com/netblue30/firejail/issues/3981#event-4326780196)


###  rusty-snake on 13 Feb 2021

tor-browser has no Wayland support yet. Does this only affect XWayland Apps or any App in Wayland sessions.

Do the workaround ([#3379 (comment)](https://github.com/netblue30/firejail/issues/3379#issuecomment-632778490)) work for you?

> Firejail version 0.9.64.2,

You should update, this version is vulnerable for [CVE-2021-26910](https://github.com/advisories/GHSA-2q4h-h5jp-942w "CVE-2021-26910").

> debug output  
> ...

The idea behind this is to see what firejail does, if you strip everything printed by firejail this outpur can not be used for debugging. (here is it unproblematic but in general it should be kept).



### Author ixuu153 on 18 Feb 2021

> tor-browser has no Wayland support yet. Does this only affect XWayland Apps or any App in Wayland sessions.

So far I only tested XWayland programs.

> Do the workaround ([#3379 (comment)](https://github.com/netblue30/firejail/issues/3379#issuecomment-632778490)) work for you? [[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379]]

According to this workaround, I added `--dbus-user=none` option in command line, then at lease ibus can work under English input method. By adding this option, "Events queue growing too big" warning does not appear anymore, but it still has "Unable to connect to ibus" warning message.

However, even with this workaround, all other input methods (Japanese, Chinese) still do not work properly. The character selection window does not appear, only plain letters are inputted. In X, this situation can be solve with `--dbus-user=filter --dbus-user.own=org.freedesktop.IBus` option, but in Wayland this solution does not work, and worse still, it return to the behavior with only `--noprofile` option (even English input method does not work). It seems we have to disable dbus completely to avoid ibus sending connection request to the main program.

> You should update, this version is vulnerable for CVE-2021-26910.

Thanks, just updated.

> The idea behind this is to see what firejail does, if you strip everything printed by firejail this outpur can not be used for debugging. (here is it unproblematic but in general it should be kept).

Here is a complete log:

 debug output 



### Author ixuu153 on 18 Feb 2021

Also tried `strace` and found following lines:

```shell
[pid   63] connect(37, {sa_family=AF_UNIX, sun_path=@"/home/tt/.cache/ibus/dbus-c9sren3v"}, 39 <unfinished ...>
[pid   61] <... getpid resumed>)       = 61
[pid   16] fstat(38,  <unfinished ...>
[pid   63] <... connect resumed>)      = -1 ECONNREFUSED (Connection refused)
```

All I know is that the IBus connection is refused. There is no further information.

[rusty-snake](https://github.com/rusty-snake) added the [duplicate](https://github.com/netblue30/firejail/labels/duplicate) This issue or pull request already existslabel [on 21 Feb 2021](https://github.com/netblue30/firejail/issues/3981#event-4356186941)


###  rusty-snake on 21 Feb 2021

I already guesed that this is because of the PID-namespace, see [#3379 (comment)](https://github.com/netblue30/firejail/issues/3379#issuecomment-632779468).[[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379]]


###  rusty-snake on 13 May 2021

Closing here. [#3379](https://github.com/netblue30/firejail/issues/3379) keeps open to track this.

[rusty-snake](https://github.com/rusty-snake) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 13 May 2021](https://github.com/netblue30/firejail/issues/3981#event-4738116968)