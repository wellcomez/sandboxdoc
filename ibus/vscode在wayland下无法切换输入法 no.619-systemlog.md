---
title: "vscode在wayland下无法切换输入法 no.619-systemlog"
layout: post
---
# System Info:
1.  `uname -a`:

        Linux yoga 5.19.13-zen1-1-zen #1 ZEN SMP PREEMPT_DYNAMIC Tue, 04 Oct 2022 14:37:01 +0000 x86_64 GNU/Linux

2.  `lsb_release -a`:

        LSB Version:	n/a
        Distributor ID:	Arch
        Description:	Arch Linux
        Release:	rolling
        Codename:	n/a

3.  `lsb_release -d`:

        Description:	Arch Linux

4.  `/etc/lsb-release`:

        DISTRIB_ID="Arch"
        DISTRIB_RELEASE="rolling"
        DISTRIB_DESCRIPTION="Arch Linux"

5.  `/etc/os-release`:

        NAME="Arch Linux"
        PRETTY_NAME="Arch Linux"
        ID=arch
        BUILD_ID=rolling
        ANSI_COLOR="38;2;23;147;209"
        HOME_URL="https://archlinux.org/"
        DOCUMENTATION_URL="https://wiki.archlinux.org/"
        SUPPORT_URL="https://bbs.archlinux.org/"
        BUG_REPORT_URL="https://bugs.archlinux.org/"
        LOGO=archlinux-logo

6.  Desktop Environment:

    Cannot determine desktop environment.

7.  XDG SESSION TYPE:

        XDG_SESSION_TYPE='wayland'

8.  Bash Version:

        BASH_VERSION='5.1.16(1)-release'

# Environment:
1.  DISPLAY:

        DISPLAY=':0'


        WAYLAND_DISPLAY='wayland-1'

2.  Keyboard Layout:

    1.  `setxkbmap`:

            WARNING: Running setxkbmap against an XWayland server
            xkb_keymap {
            	xkb_keycodes  { include "evdev+aliases(qwerty)"	};
            	xkb_types     { include "complete"	};
            	xkb_compat    { include "complete"	};
            	xkb_symbols   { include "pc+us+inet(evdev)"	};
            	xkb_geometry  { include "pc(pc105)"	};
            };

    2.  `xprop`:

            _XKB_RULES_NAMES(STRING) = "evdev", "pc105", "us", "", ""

3.  Locale:

    1.  All locales:

            C
            C.UTF-8
            en_US.utf8
            POSIX
            zh_CN.gb18030
            zh_CN.utf8

    2.  Current locale:

            LANG=en_US.UTF-8
            LC_CTYPE="en_US.UTF-8"
            LC_NUMERIC="en_US.UTF-8"
            LC_TIME="en_US.UTF-8"
            LC_COLLATE="en_US.UTF-8"
            LC_MONETARY="en_US.UTF-8"
            LC_MESSAGES="en_US.UTF-8"
            LC_PAPER=zh_CN.UTF-8
            LC_NAME="en_US.UTF-8"
            LC_ADDRESS="en_US.UTF-8"
            LC_TELEPHONE="en_US.UTF-8"
            LC_MEASUREMENT="en_US.UTF-8"
            LC_IDENTIFICATION="en_US.UTF-8"
            LC_ALL=

4.  Directories:

    1.  Home:

            /home/xxx

    2.  `${XDG_CONFIG_HOME}`:

        Environment variable `XDG_CONFIG_HOME` is set to `/home/xxx/.config`.

        Current value of `XDG_CONFIG_HOME` is `~/.config` (`/home/xxx/.config`).

    3.  Fcitx5 Settings Directory:

        Current fcitx5 settings directory is `~/.config/fcitx5` (`/home/xxx/.config/fcitx5`).

5.  Current user:

    The script is run as xxx (1000).

# Fcitx State:
1.  executable:

    Found fcitx5 at `/usr/bin/fcitx5`.

2.  version:

    Fcitx version: `5.0.19`

3.  process:

    Found 1 fcitx5 process:

           1196 fcitx5

4.  `fcitx5-remote`:

    `fcitx5-remote` works properly.

5.  DBus interface:

    Using `dbus-send` to check dbus.

    Owner of DBus name `org.fcitx.Fcitx5` is `:1.19`.

    PID of DBus name `org.fcitx.Fcitx5` owner is `1196`.

    Debug information from dbus:

           Group [x11::0] has 6 InputContext(s)
          IC [a2152e0ef19a477ba45a074323d0e9f2] program:code frontend:dbus cap:6000000000 focus:0
          IC [f6b847cbbdb64ec49dced83e384e90fe] program:zotero frontend:dbus cap:6000000000 focus:0
          IC [14a6b431f9f0451ba3d49514bc4fb2c0] program:zotero frontend:dbus cap:6000000000 focus:0
          IC [eb22d78e66f444b3a4098ad2dbd8ab9b] program:zotero frontend:dbus cap:6000000000 focus:0
          IC [d7e2cb9c7bc143e1bfb0a86a1bc05f8f] program:zotero frontend:dbus cap:6000000000 focus:0
          IC [e06aefb3ec3c4f2994d8ce1c91953853] program:zotero frontend:dbus cap:6000000000 focus:0
        Group [wayland:] has 8 InputContext(s)
          IC [69b007f7cbdb46b697aacf75d0637fe5] program:xfce4-terminal frontend:dbus cap:e001000000 focus:1
          IC [f6185f397dcc408698a742b520351bf0] program:firefox frontend:dbus cap:6001000000 focus:0
          IC [e7f7815e93574d158a928423d5f5981c] program:firefox frontend:dbus cap:6001000000 focus:0
          IC [32d746ee77d148dca68e4b241210bb0d] program:firefox frontend:dbus cap:6001000000 focus:0
          IC [1f6b1642bf8443a0a4954cf265f69e30] program:firefox frontend:dbus cap:e001000040 focus:0
          IC [9291ac98bdaa46e3af56a64f600dae08] program:xfce4-terminal frontend:dbus cap:e001000000 focus:0
          IC [5f6152a7066d4a94841fadd9d6a848bc] program:firefox frontend:dbus cap:6001000000 focus:0
          IC [307e4c8e62db405797dfe2b7c5ee74b6] program: frontend:wayland_v2 cap:40 focus:0
        Input Context without group

# Fcitx Configure UI:
1.  Config Tool Wrapper:

    Found fcitx5-configtool at `/usr/bin/fcitx5-configtool`.

2.  Config GUI for qt:

    Found `fcitx5-config-qt` at `/usr/bin/fcitx5-config-qt`.

3.  Config GUI for kde:

    **`kcmshell5` not found.**

# Frontends setup:
## Xim:
1.  `${XMODIFIERS}`:

    Environment variable XMODIFIERS is set to "@im=fcitx" correctly.
    Xim Server Name from Environment variable is fcitx.

2.  XIM_SERVERS on root window:

    Xim server name is the same with that set in the environment variable.

## Qt:
1.  qt4 - `${QT4_IM_MODULE}`:

    Environment variable QT_IM_MODULE is set to "fcitx" correctly.

2.  qt5 - `${QT_IM_MODULE}`:

    Environment variable QT_IM_MODULE is set to "fcitx" correctly.

3.  Qt IM module files:

    Found fcitx5 qt5 module: `/usr/lib/fcitx5/qt5/libfcitx-quickphrase-editor5.so`.
    Found fcitx5 im module for qt6: `/usr/lib/qt6/plugins/platforminputcontexts/libfcitx5platforminputcontextplugin.so`.
    Found fcitx5 im module for qt: `/usr/lib/qt/plugins/platforminputcontexts/libfcitx5platforminputcontextplugin.so`.
    Found unknown fcitx qt module: `/usr/lib/qt/plugins/kcms/kcm_fcitx5.so`.
    Found unknown fcitx qt module: `/usr/lib/office6/qt/plugins/platforminputcontexts/libfcitxplatforminputcontextplugin.so`.
    **Cannot find fcitx5 input method module for Qt4.**

## Gtk:
1.  gtk - `${GTK_IM_MODULE}`:

    Environment variable GTK_IM_MODULE is set to "fcitx" correctly.

2.  `gtk-query-immodules`:

    1.  gtk 2:

        Found `gtk-query-immodules` for gtk `2.24.33` at `/usr/bin/gtk-query-immodules-2.0`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-2.0 from gtk+-2.24.33

        Found fcitx5 im modules for gtk `2.24.33`.

            "/usr/lib/gtk-2.0/2.10.0/immodules/im-fcitx5.so" 
            "fcitx" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 
            "fcitx5" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 

    2.  gtk 3:

        Found `gtk-query-immodules` for gtk `3.24.34` at `/usr/bin/gtk-query-immodules-3.0`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-3.0 from gtk+-3.24.34

        Found fcitx5 im modules for gtk `3.24.34`.

            "/usr/lib/gtk-3.0/3.0.0/immodules/im-fcitx5.so" 
            "fcitx" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 
            "fcitx5" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 

        Found `gtk-query-immodules` for gtk `3.24.34` at `/usr/bin/gtk-query-immodules-3.0-32`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-3.0-32 from gtk+-3.24.34

        **Failed to find fcitx5 in the output of `/usr/bin/gtk-query-immodules-3.0-32`**

3.  Gtk IM module cache:

    1.  gtk 2:

        Found immodules cache for gtk `2.24.33` at `/usr/lib/gtk-2.0/2.10.0/immodules.cache`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-2.0 from gtk+-2.24.33

        Found fcitx5 im modules for gtk `2.24.33`.

            "/usr/lib/gtk-2.0/2.10.0/immodules/im-fcitx5.so" 
            "fcitx" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 
            "fcitx5" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 

    2.  gtk 3:

        Found immodules cache for gtk `3.24.34` at `/usr/lib/gtk-3.0/3.0.0/immodules.cache`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-3.0 from gtk+-3.24.34

        Found fcitx5 im modules for gtk `3.24.34`.

            "/usr/lib/gtk-3.0/3.0.0/immodules/im-fcitx5.so" 
            "fcitx" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 
            "fcitx5" "Fcitx5 (Flexible Input Method Framework5)" "fcitx5" "/usr/locale" "ja:ko:zh:*" 

        Found immodules cache for gtk `3.24.34` at `/usr/lib32/gtk-3.0/3.0.0/immodules.cache`.
        Version Line:

            # Created by /usr/bin/gtk-query-immodules-3.0-32 from gtk+-3.24.34

        **Failed to find fcitx5 in immodule cache at `/usr/lib32/gtk-3.0/3.0.0/immodules.cache`**

    3.  gtk 4:

        **Cannot find immodules cache for gtk 4**

        **Cannot find fcitx5 im module for gtk 4 in cache.**

4.  Gtk IM module files:

    1.  gtk 2:

        All found Gtk 2 immodule files exist.

    2.  gtk 3:

        All found Gtk 3 immodule files exist.

    3.  gtk 4:

        All found Gtk 4 immodule files exist.

# Configuration:
## Fcitx Addons:
1.  Addon Config Dir:

    Found fcitx5 addon config directory: `/usr/share/fcitx5/addon`.

2.  Addon List:

    1.  Found 26 enabled addons:

            Simplified and Traditional Chinese Translation 5.0.15
            Classic User Interface 5.0.19
            Clipboard 5.0.19
            Cloud Pinyin 5.0.15
            DBus 5.0.19
            DBus Frontend 5.0.19
            Emoji 5.0.19
            Fcitx4 Frontend 5.0.19
            Full width character 5.0.15
            IBus Frontend 5.0.19
            Input method selector 5.0.19
            Keyboard 5.0.19
            KDE Input Method Panel 5.0.19
            Status Notifier 5.0.19
            Notification 5.0.19
            Pinyin 5.0.15
            Extra Pinyin functionality 5.0.15
            Punctuation 5.0.15
            Quick Phrase 5.0.19
            Spell 5.0.19
            Table 5.0.15
            Unicode 5.0.19
            Wayland 5.0.19
            Wayland Input method frontend 5.0.19
            XCB 5.0.19
            X Input Method Frontend 5.0.19

    2.  Found 0 disabled addons:

3.  Addon Libraries:

    All libraries for all addons are found.

4.  User Interface:

    Found 2 enabled user interface addons:

        Classic User Interface
        KDE Input Method Panel

## Input Methods:
1.  `/home/xxx/.config/fcitx5/profile`:

        [Groups/0]
        # Group Name
        Name=Default
        # Layout
        Default Layout=us
        # Default Input Method
        DefaultIM=pinyin

        [Groups/0/Items/0]
        # Name
        Name=keyboard-us
        # Layout
        Layout=

        [Groups/0/Items/1]
        # Name
        Name=pinyin
        # Layout
        Layout=

        [GroupOrder]
        0=Default

# Log:
1.  `date`:

        Wed Oct 12 10:40:06 PM CST 2022

2.  `/home/xxx/.config/fcitx5/crash.log`:

    `/home/xxx/.config/fcitx5/crash.log` not found.

**Warning: the output of fcitx5-diagnose contains sensitive information, including the distribution name, kernel version, name of currently running programs, etc.**

**Though such information can be helpful to developers for diagnostic purpose, please double check and remove as necessary before posting it online publicly.**