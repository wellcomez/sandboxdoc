---
layout: post
tags:
- fctx
- ibus
- dbus-proxy
title: "issuelist"

---

# issuelist
### 1. Electron  gkt2

- Atom
Flatpak Atom based on source from flathub. io.atom.electron.BaseApp patched 

- [[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]]
	>If it's an GTK3 application in GNOME, I think it can communicate with /usr/libexec/ibus-portal through the "org.freedesktop.portal.IBus" Bus name. So I provided the test application for you if you verify the general applications works fine in [[Flatpak]]
	https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1
```diff
	const gchar *owner,

	gpointer data)

	{

	+if (!g_strcmp0 (ibus_bus_get_service_name (_bus), IBUS_SERVICE_PORTAL)) {

	+_daemon_is_running = TRUE;

	+return;

	+}

	/* If ibus-daemon is running and run ssh -X localhost,

	* daemon_name_appeared() is called but ibus_get_address() == NULL

	* because the hostname and display number are different between
```

- **fix for gtk2 fix@ibus 1.5.20**
	-  [client/gtk2: Fix Atom and Slack for Flatpak](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)
		Seems Atom, slack, com.visualstudio.code does not enable gtk_key_snooper_install() and this issue causes to call gtk_im_context_filter_keypress instead of calling ibus APIs.BUG=[#1991](https://github.com/ibus/ibus/issues/1991)[[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]]
		- [[Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image 132]]  closed
- **相关问题**
	- [[IBus IME not working with electron app 4]] Closed
	- [[flathub-im.riot.Riot Can't use ibus IME 34]] closed
		- [[electron apps do not recognize ibus 1671]]  Closed
			- [[Cant use ibus on typora 2976]] closed
			- [[ibus seems not working in Electron flatpak apps 5]] closed
		- [[ibus intelligent pinyin not working in telegram-desktop snap apps 4449]] closed 
		


### 2. QT

- [[Can't insert unicode in Qt Apps 5075]] open
  - tinywrkb on 7 Sep-2
	  Work fine here using the IBus frontend of Fcitx5 in both Wayland and X11.
    Try maybe looking at the messages in the user DBus session using Bustle.
   
- [[Please update the qtbase to 5.11.2 or later in flatpak runtimes 2273]] closed
	- Steps to reproduce
		1.  Install Ubuntu 18.10 OS
		2.  Install and enable ibus
		3.  Install com.slack.Slack
		4.  Open Slack and try to input characters with ibus IME
	- so could you please update following runtimes with the newer QT base version. The affected runtimes are:
		-   org.freedesktop.Platform
		-   org.gnome.Platform
		
	**暂时没有确定kde的修改点**

### 3. Snapd

- snap参考flakpak做的修改
  - [[Unable to use iBus (input method) 675]] # Bugfix of snapd [6114792](https://github.com/jdstrand/snapd/commit/6114792bc32f76686e21c587cc403f7038ec2194)


- [[Compatibility with CJKV input method frameworks]] closed
	- fix [[Stage Fcitx frontend 156]] merged
		- [[IBus input method not available in Qt5 applications]]
- [[Packages for rpm-based linux distributions 1630]] closed


### 4. Singal app

- [[Feature Request Flatpak for Linux 1639]] closed
	集成signal到flathub
- [[Packages for rpm-based linux distributions 1630]]  # 1630 closed
- [[Missing ibus support 17]] closed
	已经使用新的ibus
	Author SISheogorath on 24 Nov 2018
	Just tested the workaround mentioned in the [flatpak/flatpak#1671](https://github.com/flatpak/flatpak/issues/1671):[[electron apps do not recognize ibus 1671]]
	`flatpak run --env=GTK_IM_MODULE=xim org.signal.Signal` and as it turns out, this results in a freeze of the entire application when starting to type a word using ibus. So still quite imperfect.


### 5. packaing ibus engine with flatpak

- [[Feature Request Packaging IBus engines with flatpak 3019]] closed
	`Flatpak 1.0.8`
- 类似问题
	`ibus portal` 只考虑host外部不考虑`sandbox`
	- [subins2000](https://github.com/subins2000) mentioned this issue [on 15 Aug 2020](https://github.com/flatpak/flatpak/issues/3019#ref-pullrequest-679305838)
	[Add com.varnamproject.Varnam software flathub/flathub#1747](https://github.com/flathub/flathub/pull/1747) Closed

	- [subins2000](https://github.com/subins2000) mentioned this issue [on 29 Aug 2020](https://github.com/flatpak/flatpak/issues/3019#ref-issue-688321712)
	[[Is it possible to package fcitx engines with flatpak NO.108]] 
	[#108](https://github.com/fcitx/fcitx5/issues/108) closed

	- [luongthanhlam](https://github.com/luongthanhlam) mentioned this issue [on 25 Sep 2020](https://github.com/flatpak/flatpak/issues/3019#ref-issue-666212352)[[Feature Request Packaging IBus engines with flatpak 3019]] opened
	
	- [Không gõ được trên wps office BambooEngine/ibus-bamboo#153](https://github.com/BambooEngine/ibus-bamboo/issues/153)
	
### 6. fctx

- [[Is it possible to package fcitx engines with flatpak NO.108]]
	<mark style="background:red">后面再考虑和测试</mark>
	I would like to know if it is possible to package fcitx input engines with flatpak and distribute easily to users. I've understood `ibus` engines can't interact with host ibus from flatpak sandboxed environment : [flatpak/flatpak#3019](https://github.com/flatpak/flatpak/issues/3019)
	Distribution with flatpak will help ship fcitx engines easily
	- [[sd-bus D-Bus authentication is broken when proxied via xdg-dbus-proxy no.99]] closed 
		- I suspect it's the behavior described in [systemd/systemd#16610](https://github.com/systemd/systemd/issues/16610)
		- You can keep an eye on [flatpak/xdg-dbus-proxy#21](https://github.com/flatpak/xdg-dbus-proxy/issues/21) `opend`
			- [# Add support for pipelined SASL handshakes #26](https://github.com/flatpak/xdg-dbus-proxy/pull/26) opend 
		- Thanks, so I guess there's no reason to keep this issue open, especially as this seems to o<mark class="hltr-red">nly affect the Fcitx5 daemon and not the widget toolkits IM modules (GTK3 is using dbus-glib and QT5 libQt5DBus), and only the latter would find their way later on into the Flatpak runtimes.</mark>
	- [[Install Fcitx 5]]
	- [[Flatpak-only setup 287]]
	- [[无法在vscode中唤起输入法 no.458]]`firejail` `snapd`   
	- [[vscode在wayland下无法切换输入法 no.619]]
	- [[无法在Chrome(Wayland)中使用fcitx5 263]]
	- [[input method fcitx not working 73]] opend 
- firejail
	- [[Firejail breaks fcitx input on Firefox NO.2841]]  closed
	- [[Fcitx and Firefox NO.3732]] closed
	- https://github.com/netblue30/firejail/wiki/Frequently-Asked-Questions#how-can-i-enable-fcitx
	~~~
		dbus-user filter
		dbus-user.talk org.freedesktop.portal.Fcitx
		ignore dbus-user none
	~~~
	- [[UIM input method switching not working with firefox NO.3284]] closed
    	- [[add whitelist items for uim NO.3587]] merged
	    	- [add ~/.uim.d directory to whitelist-common.inc](https://github.com/netblue30/firejail/pull/3587/commits/4b336e980142447313410d2c6130fff7f691eea2)  [4b336e9](https://github.com/netblue30/firejail/pull/3587/commits/4b336e980142447313410d2c6130fff7f691eea2) Verified
				>add ~/.uim.d directory to whitelist-common.inc uim is a multilingual input method framework (similar to ibus, which has its own entry in this file" … 
    		- [add /var/lib/uim to whitelist-var-common.inc](https://github.com/netblue30/firejail/pull/3587/commits/503fcc48b649a2f89de78ae2c916893930cf2f99)  [503fcc4](https://github.com/netblue30/firejail/pull/3587/commits/503fcc48b649a2f89de78ae2c916893930cf2f99) Verified
				>add /var/lib/uim to whitelist-var-common.inc When user installs an uim module (for example, an input method like anthy or mozc), it gets registered in a file in this directory." 

			[rusty-snake](https://github.com/rusty-snake) merged commit [e8b4527](https://github.com/netblue30/firejail/commit/e8b4527e25e03dc87097a65310628c7875191824) into netblue30:master [on 25 Aug 2020](https://github.com/netblue30/firejail/pull/3587#event-3686291185)

### 7. flatpak-sdk

1.  [[CJK fonts are all tofu in electron apps 1556]] closed
	<mark>fontconfig 问题导致豆腐字, 2.13.0 fixed</mark>
	- libertylocked on 4 Jun 2018
		The issue seems to be gone after a few updates.
		Flatpak version is 0.11.7 and fontconfig version is 2.13.0
	- Yeah, this was a fontconfig 2.13.0 on-host regression that is now fixed in the runtime.
		fontconfig 2.13修改后没有问题, 无非就是copy一些字体文件
		`alexlarsson on 4 Jun 2018`
		[alexlarsson](https://github.com/alexlarsson) [alexlarsson](https://github.com/alexlarsson) closed this as [completed](https://github.com/flatpak/flatpak/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 4 Jun 2018](https://github.com/flatpak/flatpak/issues/1556#event-1660539188)

2. [[Please update the qtbase to 5.11.2 or later in flatpak runtimes 2273]] closed 
	so could you please update following runtimes with the newer QT base version. The affected runtimes are:
	-   org.freedesktop.Platform
	-   org.gnome.Platform
		(Ah I see: [https://github.com/KDE/flatpak-kde-runtime](https://github.com/KDE/flatpak-kde-runtime))   需要flatpak-kde-runtime 支持

3.  [[Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image 132]]  <mark>ibus 1.5.20 集成到flatspak-sdk</mark>
	1. **集成到**[https://gitlab.com/freedesktop-sdk/freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk)
	2. Patched 
		Could you please import ibus 1.5.20 in org.freedesktop.[Platform|Sdk] ?
		The IBus version includes the fixes to work with Atom and Slack applications:  
		[ibus/ibus@be7fb81](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)  
		[ibus/ibus@60e246e](https://github.com/ibus/ibus/commit/60e246ef1593f599e2103070f8fa02649d60ada1)
		[flatpak/flatpak#1671](https://github.com/flatpak/flatpak/issues/1671)

4. [Github Issue of flatpak](https://github.com/fcitx/fcitx5/issues?q=flatpak)

### 8. ibus-portal 

1. [[ibus-portal releasing name org.freedesktop.portal.IBus.The connection is closed 2167]] closed 
	`CentUser on 24 Dec 2019` `IBus 1.5.21  ` <mark class="hltr-yellow">没搞明白</mark>

### 9. ibus

1. [[Unable to use iBus (input method) 675]]
	Member  Packages for rpm-based linux distributions ibus 1.5.17  
   - **675集成如ibus-portal**
   	`fujiwarat   on 25 Aug 2017`
	- [fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 30 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-203a3df)[Initial version of ibus portal](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7 )"Initial version of ibus portal This adds a dbus service called org.freedesktop.portal.IBus on the session bus. It is a very limited service that only implements CreateInputContext and the InputContext interface (and Service.Destroy for lifetime access). It uses gdbus code generation for demarshalling the method calls which means it will verify that all arguments have the right type. Additionally all method calls to the input context object have to be from the client that created it, so each client is isolated. BUG=https://github.com/flatpak/flatpak/issues/675 R=Shawn.P.Huang@gmail.com Review URL: https://codereview.appspot.com/326350043 Patch from Alexander Larsson <alexl@redhat.com>… [203a3df](https://github.com/ibus/ibus/commit/203a3df5a239d644cf42b7bac03a268eb5babfc7)

	- [fujiwarat](https://github.com/fujiwarat) pushed a commit to ibus/ibus that referenced this issue [on 31 Aug 2017](https://github.com/flatpak/flatpak/issues/675#ref-commit-35ce624) [Support the portal in the gtk im modules](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae) "Support the portal in the gtk im modules This adds a new way to create an IbusBus, ibus_bus_new_async_client(). This returns an object that is not guarantee to handle any calls that are not needed by a client, meaning CreateInputContext and handling the input context. If you are running in a flatpak, or if IBUS_USE_PORTAL is set, then instead of talking to the regular ibus bus we connect to org.freedesktop.portal.IBus on the session bus and use the limited org.freedesktop.IBus.Portal interface instead of the org.freedesktop.IBus interface. This allows flatpaks (or other sandbox systems) to safely use dbus clients (apps). BUG=https://github.com/flatpak/flatpak/issues/675 Review URL: https://codereview.appspot.com/328410043 Patch from Alexander Larsson <alexl@redhat.com>." …[35ce624](https://github.com/ibus/ibus/commit/35ce62474fa97a5460d72c360943700a413a07ae)`


2. [[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]] closed 
		[[Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image 132]]  



### 10. ibuss known issue

[ibuss known issue  ](https://github.com/ibus/ibus/labels/Component-ibus)


### 11. firejail

1. [[Suggested values for environment variables might be wrong 2020]] `ibus` 
	- wm默认用ibus但是 firefox,chrome用ibus xim
		What actual advantages does `GTK_IM_MODULE=ibus` have over `GTK_IM_MODULE=xim`? Do I miss on anything by using `xim`? I did not notice any difference so far.
	- fujiwarat on 31 Jul 2018
		> so I guess at the moment my question is what is a downside of this?
		
		Your setting would be wrong and using ibus with GTK_IM_MODULE=xim is also wrong. You should set XMODIFIERS=[@im](https://github.com/im)=none
	- <mark class="hltr-yellow"> graywolf mentioned this issue  on 14 Aug 2018  `Unable to use ibus-daemon in firejail NO.116`</mark>
		[[Unable to use ibus-daemon in firejail NO.116]] Closed
	
2.  [[Unable to use ibus-daemon in firejail NO.116]] closed 
	1.  glitsj16 on 22 May 2020
		[@SailReal](https://github.com/SailReal) Glad to hear you're enjoying firejail. 
		for the environment variable workaround, you can put that into a `signal-desktop.local` file. 
		If you don't have one yet, you will have to create one manually. Either use `/etc/firejail/signal-desktop.local` (affects all users on your machine) or `${HOME}/.config/firejail/signal-desktop.local` (affects your user only). <mark class="hltr-cyan">Add the below to that file:</mark>
		`env GTK_IM_MODULE=xim`
		- [rusty-snake](https://github.com/rusty-snake) mentioned this issue  [[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379]] Open 
		- [[Wayland Only ibus failed to work in someprograms even with --noprofile 3981]] closed
		- [[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379]] open
		- [shemgp](https://github.com/shemgp) mentioned this issue [on 15 Dec 2020](https://github.com/netblue30/firejail/issues/116#ref-issue-220470531) [[Keyboard doesn't work with Chrome browser Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) NO.1204]] Closed.
	2.  rusty-snake on 19 Dec 2020
		It's **not** a systemd unit. It's a D-Bus service (look at the `[D-BUS Service]` line in it).
		
		<mark class="hltr-cyan">`--dbus-user.talk=org.freedesktop.IBus` should be enough.</mark>

		```shell
		env GTK_IM_MODULE=ibus
		env IBUS_USE_PORTAL=1
		dbus-user.talk org.freedesktop.portal.IBus
		```
	3.  rusty-snake on 8 Jan 2022
		- [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 12 Apr](https://github.com/netblue30/firejail/issues/116#ref-issue-1199012687) [[Keyboard no longer working in gedit NO.5100]] Open
		```shell
		env GTK_IM_MODULE=ibus
		env IBUS_USE_PORTAL=1
		dbus-user.talk org.freedesktop.portal.IBus
		```
		- [netblue30/firejail](https://sourcegraph.com/github.com/netblue30/firejail) › [src/firejail/**profile.c**](https://sourcegraph.com/github.com/netblue30/firejail/-/blob/src/firejail/profile.c?subtree=true)

		```c
		else if (strncmp(ptr, "dbus-user.talk ", 15) == 0) {

		#ifdef HAVE_DBUSPROXY

		if (!dbus_check_name(ptr + 15)) {

		fprintf(stderr, "Error: Invalid dbus-user.talk name: %s\n", ptr + 15);

		exit(1);
		```
		- dbus-user.talk profile 
		  [dbus-user.talk](https://sourcegraph.com/search?q=context:global+dbus-user.talk+&patternType=standard)



3. [[Firejail breaks fcitx input on Firefox NO.2841]] 


