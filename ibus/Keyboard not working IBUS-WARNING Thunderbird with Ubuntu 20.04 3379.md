---
layout: post
tags:
- firejail
- ibus
title: "Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379"

---

# Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 3379


[netblue30/firejail](https://github.com/netblue30/firejail)
[Keyboard not working IBUS-WARNING Thunderbird with Ubuntu 20.04 #3379](https://github.com/netblue30/firejail/issues/3379#top)Open

[fabianhick](https://github.com/fabianhick) opened this issue on 24 Apr 2020 · 24 comments 




### fabianhick on 24 Apr 2020

Hey,  
after upgrading my installation to Ubuntu 20.04 I ran into the following issues:

**Describe the bug**  
When running Thunderbird on Ubuntu 20.04 with firejail the keyboard input is broken.

**Behavior change on disabling firejail**  
The keyboard input works as intended

**To Reproduce**  
Steps to reproduce the behavior:

1.  Start Thunderbird: `firejail --private thunderbird`
2.  Try to enter something into the input fields
3.  See error in terminal:

```shell
(thunderbird:17): IBUS-WARNING **: 02:56:35.752: Unable to connect to ibus: Could not connect: Connection refused
(thunderbird:17): IBUS-WARNING **: 02:56:39.831: Events queue growing too big, will start to drop.
(thunderbird:17): IBUS-WARNING **: 02:56:39.877: Events queue growing too big, will start to drop.
(thunderbird:17): IBUS-WARNING **: 02:56:39.905: Events queue growing too big, will start to drop.
```

**Expected behavior**  
Being able to input information with the keyboard.

**Desktop (please complete the following information):**

-   Ubuntu 20.04 LTS (focal)

```shell
firejail version 0.9.62

Compile time support:
	- AppArmor support is enabled
	- AppImage support is enabled
	- chroot support is enabled
	- file and directory whitelisting support is enabled
	- file transfer support is enabled
	- firetunnel support is enabled
	- networking support is enabled
	- overlayfs support is enabled
	- private-home support is enabled
	- seccomp-bpf support is enabled
	- user namespace support is enabled
	- X11 sandboxing support is enabled
```

**Additional context**  
This issues also appears in gedit.



###  rusty-snake on 24 Apr 2020

Can you try `firejail --ignore=nodbus thunderbird`.



### Author fabianhick on 24 Apr 2020

[@rusty-snake](https://github.com/rusty-snake), I did try your command and the issue still persists.



###  rusty-snake on 1 May 2020

I cannot reproduce this in a ubu-focal test VM with firejail-git.



### JKjakay on 2 May 2020

I have the same problem. However, I do not use --private but instead just run:  
firejail thunderbird

The firejailed TB works as usual, but does not accept keyboard input. The ibus warning shows up in the console - to be exact, one time for every keypress, I think.



### Author fabianhick on 2 May 2020

It doesn't work for me both with and without `--private`.  
I can confirm [@JKjakay](https://github.com/JKjakay)'s description of the issue in regards to keypresses.



###  rusty-snake on 2 May 2020

Can you try with `firejail --ignore=private-tmp --ignore=tracelog --ignore="shell none" --ignore=protocol --ignore=nogroups --ignore=noroot --ignore=machine-id`. If this works, remove ignores till it breaks again.



### Author fabianhick on 2 May 2020

Thanks for the new command, [@rusty-snake](https://github.com/rusty-snake).  
Unfortunately the issue still persists:

```shell
(thunderbird:10): IBUS-WARNING **: 23:06:29.152: Unable to connect to ibus: Could not connect: Connection refused
```



###  rusty-snake on 2 May 2020

Then you will need to comment every thing in thunderbird.profile and uncomment line for line to find the problematic line. (You can also uncomment a block of line and only go line for line in the block with the problematic line).



### Author fabianhick on 2 May 2020

I did just try it out and commenting the line containing `ignore nodbus` in `thunderbird.profile` seems to solve the issue.  
The initial error message (Unable to connect to ibus) still appears, but the Events queue isn't growing too big anymore and keyboard input is working again.



###  rusty-snake on 2 May 2020

So calling `firejail (--private) --nodbus thunderbird` works?



### Author fabianhick on 2 May 2020

Yes indeed, [@rusty-snake](https://github.com/rusty-snake).  
But I also just noticed that the `thunderbird.profile` on GitHub is different to mine from the Ubuntu Focal package...



### JKjakay on 2 May 2020

Starting TB with

`firejail --nodbus thunderbird`

works for me, too



### paddyredbeard on 2 May 2020

FWIW - I was having exactly the same issue with Chromium browser on a fresh upgrade to Pop!_OS 20.04 (based on Ubuntu 20.04). Per [@JKjakay](https://github.com/JKjakay)'s comment, `firejail --nodbus chromium` resolves this for me.



### FlyingKawasaki74 on 11 May 2020

I had this issue with several applications (Thunderbird, Firefox, Vivaldi, Brave, Text-Editor).  
Using the --nodbus parameter helped (unless it's explicitely blocked like for Firefox, see issue [#3408](https://github.com/netblue30/firejail/issues/3408)). Alternatively changing the profile files also helped (commenting out all dbus related lines in the main profile file)

Running it on Ubuntu 18.04



###  rusty-snake on 11 May 2020

Unfortunately we still do not know what the real reason is, or how it can be fixed. Blocking D-Bus breaks dconf and other things. It will not work with all programs.



###  rusty-snake on 22 May 2020

@buggitheclown [@paddyredbeard](https://github.com/paddyredbeard) [@JKjakay](https://github.com/JKjakay) [@fabianhick](https://github.com/fabianhick) can you try this: [#116 (comment)](https://github.com/netblue30/firejail/issues/116#issuecomment-632394043)



### paddyredbeard on 22 May 2020

[@rusty-snake](https://github.com/rusty-snake) `GTK_IM_MODULE=xim firejail chromium` works for me.



###  rusty-snake on 23 May 2020

ok, I'm now able to reproduce it. Ubuntu 20.04 (minimal) VM with firejail-git.

```shell
$ LC_ALL=C firejail --env=GTK_IM_MODULE=ibus --noprofile gedit
Parent pid 3034, child pid 3035
Child process initialized in 41.73 ms

(gedit:2): IBUS-WARNING **: 18:01:11.426: Unable to connect to ibus: Could not connect: Connection refused

(gedit:2): IBUS-WARNING **: 18:01:14.737: Events queue growing too big, will start to drop.

(gedit:2): IBUS-WARNING **: 18:01:14.746: Events queue growing too big, will start to drop.

(gedit:2): IBUS-WARNING **: 18:01:14.770: Events queue growing too big, will start to drop.

(gedit:2): IBUS-WARNING **: 18:01:14.815: Events queue growing too big, will start to drop.

Parent is shutting down, bye...
```

Thats are bad news, because it is broken even with `--noprofile`.

Knowed work arounds:

-   `nodbus`/`dbus-user none` or `net none`, both don't work for all programs
-   `env GTK_IM_MODULE=xim` and `env QT_IM_MODULE=xim`





###  rusty-snake on 23 May 2020

Problematic strace line (strace -f):  
`connect(14, {sa_family=AF_UNIX, sun_path=@"/home/ubuntu01/.cache/ibus/dbus-AyGV49uw"}, 41) = -1 ECONNREFUSED (Connection refused)`

Looks like ibus is rejecting the connection. Maybe because of the PID-namespace?





### FlyingKawasaki74 on 23 May 2020

[@rusty-snake](https://github.com/rusty-snake)

That workaround you linked doesn't work for me. I tried creating the signal-desktop file in my home directory and using the console command like [@paddyredbeard](https://github.com/paddyredbeard)  
So far using --noprofile --nodbus worked with all programs that had the issue for me.  
EDIT: I changed my user name so don't be surprised why a new name showed up in the convo



###  rusty-snake on 23 May 2020

electron doesn't use gtk, maybe `QT_IM_MODULE=xim` works.

 [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 1 Sep 2020](https://github.com/netblue30/firejail/issues/3379#ref-issue-690074556)

[Must-fix bugs for release 0.9.64 #3618](https://github.com/netblue30/firejail/issues/3618) Closed
 [reinerh](https://github.com/reinerh) added this to the  [0.9.64](https://github.com/netblue30/firejail/milestone/1) milestone [on 2 Sep 2020](https://github.com/netblue30/firejail/issues/3379#event-3716861756)

 [rusty-snake](https://github.com/rusty-snake) mentioned this issue [on 13 Feb 2021](https://github.com/netblue30/firejail/issues/3379#ref-issue-807554438)

[Wayland Only: ibus failed to work in some programs even with --noprofile #3981](https://github.com/netblue30/firejail/issues/3981) Closed
6 tasks

 [jamesmcm](https://github.com/jamesmcm) mentioned this issue [on 1 Oct](https://github.com/netblue30/firejail/issues/3379#ref-issue-1392286634)

[Can't use keyboard in selected browser (Linux Mint 21) jamesmcm/vopono#193](https://github.com/jamesmcm/vopono/issues/193) Closed



[@rusty-snake](https://github.com/rusty-snake) for my case, I think this caused by multi ibus configuration file under ~/.config/ibus/bus/ and one of them contains wrong socket address.



###  rusty-snake 14 days ago

What do you mean with "wrong address"?



