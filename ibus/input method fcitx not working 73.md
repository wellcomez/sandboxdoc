---
layout: post
tags:
- electron
- fcitx
- flatpak
title: "input method fcitx not working 73"

---

# input method fcitx not working 73
[input method fcitx not working #73](https://github.com/flathub/io.atom.Atom/issues/73)Open

[omichaelo](https://github.com/omichaelo) opened this issue on 10 Feb 2020 · 4 comments



[omichaelo](https://github.com/omichaelo)

### omichaelo on 10 Feb 2020

Fcitx does not seem to activate in flatpak atom. I downloaded the atom from atom.io, and fcitx works.

After some research this may be because the env variables are not set correctly? Although I am not sure, these discussions might be helpful [atom/atom#5447](https://github.com/atom/atom/issues/5447)  [flathub/io.atom.electron.BaseApp#4](https://github.com/flathub/io.atom.electron.BaseApp/issues/4) [flathub/com.visualstudio.code#24](https://github.com/flathub/com.visualstudio.code/issues/24) .

FYI fcitx is an input method for CJKV characters, like ibus. Although I did not test it thoroughly, ibus doesn't seem to work either.

[omichaelo](https://github.com/omichaelo)

### Author omichaelo on 10 Feb 2020

Embarrassing, I just found that `flatpak run --env=GTK_IM_MODULE=xim io.atom.Atom` works.  
Not a programmer at all, but do you guys think it is a good idea to add this env var to runtime or something? Or is their a way to set this env everytime I launch the app, without having to launch it from the terminal every time?

[cpba](https://github.com/cpba)

###  cpba on 10 Feb 2020

-   For your specific case use `override` so you won't need to set it every time:
    
    ```
      flatpak override --env=GTK_IM_MODULE=xim io.atom.Atom
    ```
    
-   For the general case:
    

We could certainly add this env variable to the default bundle. However, I don't understand exactly what it does and I'm worried I might break somebody else's workflow by setting it.

Could you provide some context on this?

[omichaelo](https://github.com/omichaelo)

### Author omichaelo on 10 Feb 2020

Thanks [@cpba](https://github.com/cpba) , it worked! You are right, the default bundle should probably not be changed if it isn't necessary.

After skimming through documentation on [GtkIMContext](https://developer.gnome.org/gtk3/stable/GtkIMContext.html) and [Running GTK+ Applications:Environmental Variables](https://developer.gnome.org/gtk3/stable/gtk-running.html), it seems overriding `GTK_IM_MODULE` may break somebody else's workflow, as you said. The IM module is usually set according to the locale. Perhaps, because of atom running in a flatpak, that change in locale or input method or env variable is somehow no detected?

I am not a programmer but a regular user linux, so I really find it difficult to understand the technical aspects of this topic. This is the best I've got at this point. If I have time, I'll do more research and testing. I'd love to contribute.

[cpba](https://github.com/cpba)

###  cpba on 10 Feb 2020

My understanding of this matter is tenuous at best, as far as I can tell gtk attempts to guess the input method from the user's locale.

`GTK_IM_MODULE` is provided as a way for the user to manually force an input method and ignore the locale.

So maybe this would work properly out of the box when using a cjkv locale?


