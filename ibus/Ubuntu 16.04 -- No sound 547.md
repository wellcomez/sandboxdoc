---
layout: post
tags:
- firejail
- pluseaudio
title: "Ubuntu 16.04 -- No sound 547"

---

# Ubuntu 16.04 -- No sound 547
[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public

[Ubuntu 16.04 -- No sound #547](https://github.com/netblue30/firejail/issues/547) Closed

[Anderath](https://github.com/Anderath) opened this issue on 30 May 2016 · 2 comments




### Anderath on 30 May 2016

Whenever I attempt to use a program in firejail, no output/input devices are available after. I'm not quite sure what I could do to provide proof of this but I will spin up a test VM of a fresh install and report back whether I get the same result or not.

My feeling is that this is somehow related to [#410](https://github.com/netblue30/firejail/issues/410) in that I get the same error messages and symptoms, then directly after I cannot use my regular applications(Clementine, VLC, whatever) with sound because my entire sound hardware appears to have disappeared.



###  Fred-Barclay on 30 May 2016

I wonder if this is related to [#430](https://github.com/netblue30/firejail/issues/430) or [#523](https://github.com/netblue30/firejail/issues/523), which both deal with pulseaudio problems.

Could you try following [https://firejail.wordpress.com/support/known-problems/#pulseaudio](https://firejail.wordpress.com/support/known-problems/#pulseaudio), rebooting, and seeing if that helps?

 [netblue30](https://github.com/netblue30) added the [information](https://github.com/netblue30/firejail/labels/information) Information was/is requiredlabel [on 31 May 2016](https://github.com/netblue30/firejail/issues/547#event-677071823)



### Owner netblue30 on 31 May 2016

Try the workaround from [https://firejail.wordpress.com/support/known-problems/#pulseaudio](https://firejail.wordpress.com/support/known-problems/#pulseaudio)

 [netblue30](https://github.com/netblue30) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 7 Jun 2016](https://github.com/netblue30/firejail/issues/547#event-683137636)



