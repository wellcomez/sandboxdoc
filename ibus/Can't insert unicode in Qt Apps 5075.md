---
layout: post
tags:
- flatpak
- qt
- kde
title: "Can''t insert unicode in Qt Apps 5075"

---

[Can't insert unicode in Qt Apps](https://github.com/flatpak/flatpak/issues/5075#top)#5075 open
[s3rgeym](https://github.com/s3rgeym) opened this issue on 3 Sep · 5 comments


### s3rgeym on 3 Sep


#### Flatpak version

1.14.0

#### What Linux distribution are you using?

Arch Linux

#### Linux distribution version

rolling

#### What architecture are you using?

x86_64

#### How to reproduce

1.  Run any Qt application like telegram
2.  Try to enter `Ctrl + Shift + U`, 2012, 

#### Expected Behavior

A hyphen should be inserted

#### Actual Behavior

2012

#### Additional Information

<<<<<<< HEAD
~~~
flatpak run --env=QT_IM_MODULE=ibus org.telegram.desktop
Gtk-Message: 17:31:28.656: Failed to load module "canberra-gtk-module"
QIBusPlatformInputContext: invalid portal bus.
~~~
=======
>flatpak run --env=QT_IM_MODULE=ibus org.telegram.desktop
>
>Gtk-Message: 17:31:28.656: Failed to load module "canberra-gtk-module"
>QIBusPlatformInputContext: invalid portal bus.

>>>>>>> e6c9456813ee56ac1060fa116fba0487880ca61f
 [s3rgeym](https://github.com/s3rgeym) added the [bug](https://github.com/flatpak/flatpak/labels/bug) label [on 3 Sep](https://github.com/flatpak/flatpak/issues/5075#event-7317172882)



### tinywrkb on 7 Sep 2022

Bring it up with the maintainer of that app, that's sounds like a packaging issue.  
I don't know why it was decided to bundle Qt with the app, but it doesn't seem like a wise choice, as KDE developers already maintaining a runtime for this, and doing a pretty good job at it.  
I suggest that you try a Qt based app that is actually using the KDE runtime with its properly packaged Qt framework.





### s3rgeym on 7 Sep 2022

> Bring it up with the maintainer of that app, that's sounds like a packaging issue. I don't know why it was decided to bundle Qt with the app, but it doesn't seem like a wise choice, as KDE developers already maintaining a runtime for this, and doing a pretty good job at it. I suggest that you try a Qt based app that is actually using the KDE runtime with its properly packaged Qt framework.

All applications based on QT are affected





### s3rgeym on 7 Sep 2022

GTK apps works perfectly





### s3rgeym on 7 Sep 2022

my host de:
<<<<<<< HEAD

~~~
~/work via  v16.17.0 via 🐍 v3.10.6 (google) 
❯ plasmashell --version 
plasmashell 5.25.5
~~~



### tinywrkb on 7 Sep 2022 -2
=======
>>>>>>> e6c9456813ee56ac1060fa116fba0487880ca61f
```shell
~/work via  v16.17.0 via 🐍 v3.10.6 (google) 
❯ plasmashell --version 
plasmashell 5.25.5
```



### tinywrkb on 7 Sep-2

<mark class="hltr-pink">Work fine here using the IBus frontend of Fcitx5 in both Wayland and X11.</mark>

```shell
# Wayland
$ flatpak run \
  --env=GTK_IM_MODULE=ibus \
  --env=QT_IM_MODULE=ibus \
  --env=XMODIFIERS=@im=ibus \
  org.kde.kwrite

# X11
$ flatpak run \
  --socket=x11 \
  --nosocket=wayland \
  --env=QT_QPA_PLATFORM=xcb \
  --env=GTK_IM_MODULE=ibus \
  --env=QT_IM_MODULE=ibus \
  --env=XMODIFIERS=@im=ibus \
  org.kde.kwrite
```

Try maybe looking at the messages in the user DBus session using Bustle.

