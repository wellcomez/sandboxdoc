---
title: "[Feature Request] Packaging IBus engines with flatpak · Issue #3019 · flatpakflatpak"
layout: post
---

# [Feature Request] Packaging IBus engines with flatpak · Issue #3019 · flatpak\flatpak
<a style="text-decoration:underline" href="https://github.com/flatpak/flatpak/issues/3019">[Feature Request] Packaging IBus engines with flatpak · Issue #3019 · flatpak\flatpak</a><br>

Linux distribution and version
------------------------------

Linux Mint 19 Tara

Flatpak version
---------------

Flatpak 1.0.8

Description of the problem
--------------------------

I'm trying to package this [IBus engine](https://github.com/varnamproject/libvarnam-ibus). Here's the [packaging repo](https://github.com/subins2000/varnam-flatpak). I gave enough permissions for the engine to connect to parent `ibus-daemon` but it fails :

```
GLib-GIO _g_io_module_get_default: Found default implementation local (GLocalVfs) for ?gio-vfs?
IBUS ibus_factory_new: assertion 'G_IS_DBUS_CONNECTION (connection)' failed
GLib-GObject g_object_ref_sink: assertion 'G_IS_OBJECT (object)' failed
IBUS ibus_factory_add_engine: assertion 'IBUS_IS_FACTORY (factory)' failed
IBUS ibus_bus_call_sync: assertion 'ibus_bus_is_connected (bus)' failed

```

To test if it's a connection problem, I ran the default [ibus-tmpl engine](https://github.com/ibus/ibus-tmpl), but that also fails establishing a connection :

```
Traceback (most recent call last):
  File "main.py", line 121, in <module>
    main()
  File "main.py", line 118, in main
    launch_engine(exec_by_ibus)
  File "main.py", line 78, in launch_engine
    IMApp(exec_by_ibus).run()
  File "main.py", line 59, in __init__
    self.__factory = IBus.Factory.new(self.__bus.get_connection())
TypeError: Argument 0 does not allow None as a value

```

So it seems that it is failing in a connection to ibus. I read [#675](Unable%20to%20use%20iBus%20(input%20method)%20675.md), perhaps flatpak is limiting the access of an engine ?

Does flatpak's implementation of ibus portal supports packaging an engine ?  
Or am I doing it wrong ? Here are the [permissions](https://github.com/subins2000/varnam-flatpak/blob/ibus/com.varnamproject.Varnam.json) :

```
"finish-args": [
        "\--share=ipc",
        "\--own-name=org.freedesktop.IBus.EnchantPython",
        "\--talk-name=org.freedesktop.DBus",
        "\--talk-name=org.freedesktop.IBus",
        "\--talk-name=org.freedesktop.portal.IBus",
        "\--filesystem=xdg-run/dconf",
        "\--filesystem=~/.config/dconf:ro",
        "\--talk-name=ca.desrt.dconf",
        "\--env=DCONF_USER_CONFIG_DIR=.config/dconf"
],
