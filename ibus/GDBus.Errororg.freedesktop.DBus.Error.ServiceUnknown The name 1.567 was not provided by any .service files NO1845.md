---
layout: post
tags:
- ibus
- flatpak
- session-bus
title: "GDBus.Errororg.freedesktop.DBus.Error.ServiceUnknown The name 1.567 was not provided by any .service files NO1845"

---

#   GDBus.Errororg.freedesktop.DBus.Error.ServiceUnknown The name 1.567 was not provided by any .service files NO1845
[GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name :1.567 was not provided by any .service files](https://github.com/flatpak/flatpak/issues/1845#top)#1845

[mwleeds](https://github.com/mwleeds) opened this issue on Jun 30, 2018 · 14 comments Closed

[flatpak](https://github.com/flatpak/flatpak/)

###  mwleeds on Jun 30, 2018
~~~
## Linux distribution and version

Fedora 28

## Flatpak version

0.99.2 (built from git master)

## Description of the problem

It took me two tries just now to install an app. Here's what happened the first time:

```shell
$ flatpak install eos-apps com.endlessm.wiki_art.en
1 metadata, 0 content objects fetched; 569 B transferred in 1 seconds           
Required runtime for com.endlessm.wiki_art.en/x86_64/eos3 (runtime/com.endlessm.apps.Platform/x86_64/3) found in remote eos-sdk
Do you want to install it? [y/n]: y
Installing in system:
com.endlessm.apps.Platform/x86_64/3                     eos-sdk  07046fc5a683
org.freedesktop.Platform.Icontheme.EndlessOS/x86_64/1.0 eos-sdk  a73d8e9d75d5
com.endlessm.apps.Platform.Locale/x86_64/3              eos-sdk  7b8861e41a47
com.endlessm.wiki_art.en/x86_64/eos3                    eos-apps 59b85f128df8
  permissions: ipc, network, pulseaudio, x11, dri
  dbus access: org.gnome.OnlineAccounts, org.gnome.Software
  system dbus access: com.endlessm.Metrics
Is this ok [y/n]: y
Installing: com.endlessm.apps.Platform/x86_64/3 from eos-sdk
[####################] 2218 metadata, 16209 content objects fetched; 179662 KiB transferred in 1413 seconds
Warning: Failed to install com.endlessm.apps.Platform/x86_64/3: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name :1.567 was not provided by any .service files
Installing: org.freedesktop.Platform.Icontheme.EndlessOS/x86_64/1.0 from eos-sdk
[####################] 28 metadata, 140 content objects fetched; 111 KiB transferred in 4 seconds
error: Failed to install org.freedesktop.Platform.Icontheme.EndlessOS/x86_64/1.0: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name :1.567 was not provided by any .service files
```

## Steps to reproduce

1.  Have a slow Internet connection
2.  Try to install something.
~~~





### Contributor mcatanzaro on Jul 2, 2018

> Have a slow Internet connection

That is not required to reproduce. I have a 200 Mbps connection and I just hit this with flatpak 0.99.2.



### Member alexlarsson on Jul 3, 2018

When this happens, does the flatpak-system-helper exit or die/crash or something?



### GongT on Jul 5, 2018

Fedora 28 & 0.99.2 from dnf

```shell
Required runtime for com.github.wwmm.pulseeffects/x86_64/stable (runtime/org.gnome.Platform/x86_64/3.28) found in remote flathub
Installing in system:
org.gnome.Platform/x86_64/3.28                    flathub e676fe7f9420
org.freedesktop.Platform.ffmpeg/x86_64/1.6        flathub d757f762489e
org.gnome.Platform.Locale/x86_64/3.28             flathub 0296024b5e1d
com.github.wwmm.pulseeffects/x86_64/stable        flathub ea4a15b3eaf9
  permissions: ipc, pulseaudio, wayland, x11
  file access: xdg-run/dconf, ~/.config/dconf:ro
  dbus access: ca.desrt.dconf
com.github.wwmm.pulseeffects.Locale/x86_64/stable flathub 7c6f7edcb09a
Installing: org.gnome.Platform/x86_64/3.28 from flathub
[####################] 10 delta parts, 80 loose fetched; 261229 KiB transferred 
Warning: Failed to install org.gnome.Platform/x86_64/3.28: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name :1.176 was not provided by any .service files
Installing: org.freedesktop.Platform.ffmpeg/x86_64/1.6 from flathub
[####################] 1 metadata, 0 content objects fetched; 569 B transferred in 4 seconds
error: Failed to install org.freedesktop.Platform.ffmpeg/x86_64/1.6: GDBus.Error:org.freedesktop.DBus.Error.ServiceUnknown: The name :1.176 was not provided by any .service files
```

what is `:1.176`?



### dsboger-zz on Jul 5, 2018

In my case, <mark style="background: #FF5582A6;">updating com.google.AndroidStudio seems to be causing this same issue. Whenever I try updating it, that "ServiceUnknown" problem</mark> occurs, and <mark style="background: #00FF70;">every update</mark> queued after AndroidStudio also fails with the same message. Leaving AndroidStudio out and updating everything else worked.





###  smcv on Jul 7, 2018

This is the Flatpak system helper service crashing. I get this while smoke-testing a backport of Flatpak 0.99.2 to Debian 9 'stretch'.

```shell
Jul 06 17:54:16 d9gnome dbus[411]: [system] Successfully activated service 'org.freedesktop.Flatpak.SystemHelper'
Jul 06 17:54:16 d9gnome systemd[1]: Started flatpak system helper.
Jul 06 17:54:16 d9gnome kernel: pool[2703]: segfault at 8 ip 000055e6f445e813 sp 00007fa0e8dee560 error 4 in flatpak
-system-helper[55e6f4447000+81000]
Jul 06 17:54:16 d9gnome systemd[1]: flatpak-system-helper.service: Main process exited, code=killed, status=11/SEGV
Jul 06 17:54:16 d9gnome systemd[1]: flatpak-system-helper.service: Unit entered failed state.
```

I'll try to get a backtrace.



###  smcv on Jul 7, 2018

Looks like [#1854](https://github.com/flatpak/flatpak/pull/1854)?

```
                Stack trace of thread 4231:
                #0  0x00005569ac20f813 handle_deploy_appstream (flatpak-system-helper)
                #1  0x00007fd535965038 ffi_call_unix64 (libffi.so.6)
                #2  0x00007fd535964a9a ffi_call (libffi.so.6)
                #3  0x00007fd53b96e7ae g_cclosure_marshal_generic (libgobject-2.0.so.0)
                #4  0x00007fd53b96df75 g_closure_invoke (libgobject-2.0.so.0)
                #5  0x00007fd53b97ff82 n/a (libgobject-2.0.so.0)
                #6  0x00007fd53b987d41 g_signal_emitv (libgobject-2.0.so.0)
                #7  0x00005569ac253499 _flatpak_system_helper_skeleton_handle_method_call (flatpak-system-helper)
```



###  smcv on Jul 7, 2018

No, looking at this in gdb, I'm in the `/* empty path == local pull */` case in `handle_deploy_appstream()`, specifically this statement:

```c
              g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR, G_DBUS_ERROR_FAILED,
                                                     "Error pulling from repo: %s", second_error->message);
```

and `second_error` is NULL, because the second `flatpak_dir_pull()` call passed a NULL `GError **` instead of the `&second_error` that was presumably intended.

[smcv](https://github.com/smcv) added a commit to smcv/flatpak that referenced this issue [on Jul 7, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-commit-47c8a73)



[Fix error handling while deploying AppStream](https://github.com/smcv/flatpak/commit/47c8a73e68bafc5b1d6b7064f9a305d053395450) "Fix error handling while deploying AppStream Setting an error with second_error->message is going to work poorly when second_error has never been set non-NULL. Related to #1845, although not necessarily the full solution. Signed-off-by: Simon McVittie <smcv@collabora.com> … [47c8a73](https://github.com/smcv/flatpak/commit/47c8a73e68bafc5b1d6b7064f9a305d053395450) [smcv](https://github.com/smcv) mentioned this issue [on Jul 7, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-pullrequest-339139379)

[Fix error handling while deploying AppStream #1867](https://github.com/flatpak/flatpak/pull/1867) Closed



###  smcv on Jul 7, 2018

I haven't been able to reproduce this since applying [#1867](https://github.com/flatpak/flatpak/pull/1867).

[rh-atomic-bot](https://github.com/rh-atomic-bot) pushed a commit that referenced this issue [on Jul 8, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-commit-7179c12)
[Fix error handling while deploying AppStream](https://github.com/flatpak/flatpak/commit/7179c12e22f56c0eee41a0d1ac84e3292580dada) "Fix error handling while deploying AppStream Setting an error with second_error->message is going to work poorly when second_error has never been set non-NULL. Related to #1845, although not necessarily the full solution. Signed-off-by: Simon McVittie <smcv@collabora.com> Closes: #1867 Approved by: alexlarsson" …[7179c12](https://github.com/flatpak/flatpak/commit/7179c12e22f56c0eee41a0d1ac84e3292580dada)



### Member alexlarsson on Jul 8, 2018

I think what happens is that flatpak-system-helper idle times out 10 minutes after the last operation, and we keep a single dbus proxy object around for the lifetime of the FlatpakDir. If we create the SystemHelper early on we resolve the name and keep the unique id, and then it dies we will get this error, because the proxy only resolves the name once.

[alexlarsson](https://github.com/alexlarsson) added a commit to alexlarsson/flatpak that referenced this issue [on Jul 8, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-commit-6ed2ea2)
[system-helper: Fix race condition with idle exit](https://github.com/alexlarsson/flatpak/commit/6ed2ea22765769b3d7aa7d12acf1003c5f6145d1) "system-helper: Fix race condition with idle exit If the system helper process exits due to being idle the FlatpakSystemHelper dbus proxy will return errors on all calls, because it resolves the well known name to a unique name on creation, which means it will try to talk to the old idle-exited instance. The fix is to not use GDBusProxy, but instead manually call g_dbus_connection_call_sync(). Fixes #1845") …
[6ed2ea2](https://github.com/alexlarsson/flatpak/commit/6ed2ea22765769b3d7aa7d12acf1003c5f6145d1)  



[alexlarsson](https://github.com/alexlarsson) added a commit to alexlarsson/flatpak that referenced this issue [on Jul 8, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-commit-dd44455)
[system-helper: Fix race condition with idle exit](https://github.com/alexlarsson/flatpak/commit/dd4445517d132f55edc2c8838bb61c599689930c) "system-helper: Fix race condition with idle exit If the system helper process exits due to being idle the FlatpakSystemHelper dbus proxy will return errors on all calls, because it resolves the well known name to a unique name on creation, which means it will try to talk to the old idle-exited instance. The fix is to not use GDBusProxy, but instead manually call g_dbus_connection_call_sync(). Fixes #1845")…[dd44455](https://github.com/alexlarsson/flatpak/commit/dd4445517d132f55edc2c8838bb61c599689930c)


[alexlarsson](https://github.com/alexlarsson) mentioned this issue [on Jul 8, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-pullrequest-339233845)
[system-helper: Fix race condition with idle exit #1873](https://github.com/flatpak/flatpak/pull/1873) Closed

 [rh-atomic-bot](https://github.com/rh-atomic-bot) closed this as [completed](https://github.com/flatpak/flatpak/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) in [e4dbd10](https://github.com/flatpak/flatpak/commit/e4dbd107b3dc6915088d748ce72c405b61ccbbc0)[on Jul 10, 2018](https://github.com/flatpak/flatpak/issues/1845#event-1723685068)

[mwleeds](https://github.com/mwleeds) mentioned this issue [on Jul 18, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-issue-342007732)
[Fail to update in the first time #1911](https://github.com/flatpak/flatpak/issues/1911)  Closed

[velurimithun](https://github.com/velurimithun) mentioned this issue [on Sep 6, 2018](https://github.com/flatpak/flatpak/issues/1845#ref-pullrequest-356651052)
[Add br.com.jeanhertel.adriconf flathub/flathub#605](https://github.com/flathub/flathub/pull/605) Closed

[dw](https://github.com/dw) mentioned this issue [on Mar 22, 2019](https://github.com/flatpak/flatpak/issues/1845#ref-issue-423792204)
[DBusException: org.freedesktop.DBus.Error.ServiceUnknown: The name :1.327 was not provided by any .service files mitogen-hq/mitogen#570](https://github.com/mitogen-hq/mitogen/issues/570) Closed



### strongholdmedia on Apr 12, 2019

> In my case, updating com.google.AndroidStudio seems to be causing this same issue. Whenever I try updating it, that "ServiceUnknown" problem occurs, and every update queued after AndroidStudio also fails with the same message. Leaving AndroidStudio out and updating everything else worked.

This.  
Since some update (I am on Debian 10, but same happens on Debian 9), with Android Studio installed, I receive this error every now and then, including with flatpak.  
But for me, _it only seems to cause problems when I have an Android device connected_.  
(Autostarting the emulator may also count as one, for whoever configured it to always run.)



###  smcv on Apr 12, 2019

[@strongholdmedia](https://github.com/strongholdmedia): Please open a separate bug report with details and steps to reproduce the problem. The high-level symptom you see being the same does not mean that the root cause is the same. If it turns out to be the same root cause then it's easy to close a separate bug as a duplicate, but if the root cause is different and two causes get mixed up on the same bug number, it's really confusing for everyone and will make it slower and more difficult to solve the bug.



### strongholdmedia on Apr 13, 2019

I absolutely didn't want to file a bug report whatsoever.  
I just suggested that anyone who can actually reproduce this issue try with any Android devices, including any possible emulators, disconnected, so that it may help them to narrow down any set of causes.

_I also have the personal opinion that the primary issue is _not_ with Flatpak, and if you indeed want to make the update "bulletproof" should any lower level script fail (that I suppose the case is currently), then you may try to rename the issue as such. But this is only a suggestion._

(I find it sad to see that more and more people are in this continuous-integration-flow-metrics-BS-obsessed mindset recently instead of actually being focused on rectifying issues, like in my opinion collaboration should.)



### Gameransari on Jul 2, 2020

The solution is here... :)  
[https://youtu.be/34iXHWuhsss](https://youtu.be/34iXHWuhsss)





### Gameransari on Jul 2, 2020

This is the Solution  
[https://youtu.be/34iXHWuhsss](https://youtu.be/34iXHWuhsss)



[marcelovbcfilho](https://github.com/marcelovbcfilho) mentioned this issue [on Aug 22, 2021](https://github.com/flatpak/flatpak/issues/1845#ref-issue-976176985)
[Missing socket declaration in developer tutorial elementary/docs#120](https://github.com/elementary/docs/issues/120) Open
