---
layout: post
tags:
- firejail
- ibus
- firefox
title: "Keyboard doesn''t work with Chrome browser Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) NO.1204"

---

# Keyboard doesn''t work with Chrome browser Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) NO.1204

[netblue30](https://github.com/netblue30)/[firejail](https://github.com/netblue30/firejail)Public

[Keyboard doesn't work with Chrome browser/Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) #1204](https://github.com/netblue30/firejail/issues/1204#start-of-content) Closed

[kensfc-23](https://github.com/kensfc-23) opened this issue on 9 Apr 2017 · 18 comments




### kensfc-23 on 9 Apr 2017

Hello again,

I get this with chrome browser and I can't type:

---

~~~
casicasi123@dheckjue-cokeiw:~$ firejail google-chrome-stable  
Reading profile /etc/firejail/google-chrome-stable.profile  
Reading profile /etc/firejail/google-chrome.profile  
Reading profile /etc/firejail/disable-common.inc  
Reading profile /etc/firejail/disable-programs.inc  
Reading profile /etc/firejail/whitelist-common.inc  
Parent pid 9615, child pid 9616  
Child process initialized

(google-chrome-stable:4): IBUS-WARNING **: Unable to connect to ibus: No se pudo conectar: Conexión rehusada  
[78:78:0409/151059.797639:ERROR:child_thread_impl.cc(762)] Request for unknown Channel-associated interface: ui::mojom::GpuMain

(google-chrome-stable:4): IBUS-WARNING **: Events queue growing too big, will start to drop.

....
~~~

---

Greetings

 [kensfc-23](https://github.com/kensfc-23) changed the title ~~Keyboard doesn't work with Chrome browser~~ Keyboard doesn't work with Chrome browser (firejail version 0.9.46~rc1) [on 9 Apr 2017](https://github.com/netblue30/firejail/issues/1204#event-1035469448)



###  chiraag-nataraj on 9 Apr 2017

So you're not able to type at all? Are you using the default profile or did you create your own?



### Author kensfc-23 on 9 Apr 2017

I'm Spaniard. Sorry for my English.

No ... I can't type at all and I didn't create any new profile.

I use Ubuntu Gnome 17.04. Kernel 4.10.9. Chrome version is 57.

 [netblue30](https://github.com/netblue30) added the [bug](https://github.com/netblue30/firejail/labels/bug) Something isn't workinglabel [on 9 Apr 2017](https://github.com/netblue30/firejail/issues/1204#event-1035483455)



### Owner netblue30 on 9 Apr 2017

Is "firejail --noprofile google-chrome-stable" working?

 [netblue30](https://github.com/netblue30) mentioned this issue [on 9 Apr 2017](https://github.com/netblue30/firejail/issues/1204#ref-issue-220455962)

[Error in Ubuntu Gnome 17.04 (firejail version 0.9.46~rc1) #1202](https://github.com/netblue30/firejail/issues/1202)

 Closed

 [netblue30](https://github.com/netblue30) changed the title ~~Keyboard doesn't work with Chrome browser (firejail version 0.9.46~rc1)~~ Keyboard doesn't work with Chrome browser (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04) [on 9 Apr 2017](https://github.com/netblue30/firejail/issues/1204#event-1035483946)



### Author kensfc-23 on 9 Apr 2017

Keyboard doesn't work with "firejail --noprofile google-chrome-stable" either



### Owner netblue30 on 9 Apr 2017

Question: how is the regular Chromium from Ubuntu 17.04 working?



### Author kensfc-23 on 9 Apr 2017

I don't use Chromium. Should I install it?

Edit: I have installed Chromium browser using Synaptic. Keyboard doesn't work either. I get the same message.

Chrome browser and Chromium work both well without firejail



### Owner netblue30 on 9 Apr 2017

Yes please, give it a try - with and without sandbox.



### Author kensfc-23 on 9 Apr 2017

Both (Chrome and Chromium) work well without firejail

I'm using Chrome browser right now without firejail.

 [kensfc-23](https://github.com/kensfc-23) changed the title ~~Keyboard doesn't work with Chrome browser (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04)~~ Keyboard doesn't work with Chrome browser/Firefox (firejail version 0.9.46~rc1, Ubuntu Gnome 17.04)[on 10 Apr 2017](https://github.com/netblue30/firejail/issues/1204#event-1035547846)



### Author kensfc-23 on 10 Apr 2017

Keyboard doesn't work with Firefox ... Title edited



### Owner netblue30 on 26 Apr 2017

I installed Ubuntu Gnome 17.04 and it seems to be working. I am able to switch the keyboard between English and Japanese without problems in firefox. I am running firejail on the latest version in mainline git. Closing for now, if you still have problems let me know

 [netblue30](https://github.com/netblue30) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 26 Apr 2017](https://github.com/netblue30/firejail/issues/1204#event-1058901485)



### Author  kensfc-23 on 30 Apr 2017

It doesn't work here. I use master. I do "sudo git pull" twice a day ;-) Greetings 2017-04-26 17:59 GMT+02:00 netblue30 <notifications@github.com>:

[…](https://github.com/netblue30/firejail/issues/1204#)



### Owner netblue30 on 2 May 2017

reopened!

 [netblue30](https://github.com/netblue30) reopened this [on 2 May 2017](https://github.com/netblue30/firejail/issues/1204#event-1064240851)

 [kensfc-23](https://github.com/kensfc-23) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 16 May 2017](https://github.com/netblue30/firejail/issues/1204#event-1082974104)



### fuelflo on 25 Sep 2017

I still have the same Problem. Keyboard not working. It worked before, but with some update in firejail or Ubuntu it stopped working.

Ubuntu Gnome 17.04  
Kernel 4.13.3  
firejail 0.9.50 (also didn't work on 0.9.48)  
Firefox 56.0  
Chrome 61.0.xxxxx

```
(firefox:5): IBUS-WARNING **: Unable to connect to ibus: Could not connect: Connection refused
1506293880732
addons.webextension.{b3e677f4-1150-4387-8629-da738260a48e}	WARN	Please specify whether you want browser_style or not in your browser_action options.
```

Any ideas how I could find the problem?

**P.S. actually this applies to all programs. I also can't write on gedit anymore, etc.**

 [netblue30](https://github.com/netblue30) reopened this [on 6 Oct 2017](https://github.com/netblue30/firejail/issues/1204#event-1281748953)



### Owner netblue30 on 6 Oct 2017

As root user, open /etc/firejail/globals.local, add "ignore private-tmp" to that file, and give it a try.



### Owner netblue30 on 6 Oct 2017

Also add a "ignore net" to that file.



### fuelflo on 7 Oct 2017

Unfortunately I can't verify that anymore. I upgraded to Ubuntu 17.10 and for now it's working.  
sry :(

 [SkewedZeppelin](https://github.com/SkewedZeppelin) closed this as [completed](https://github.com/netblue30/firejail/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 7 Oct 2017](https://github.com/netblue30/firejail/issues/1204#event-1282894168)



### BugShooter on 24 Oct 2017

Hello.  
I have same issue in Ubuntu 16.04  
firejail version 0.9.38.10  
When I try to type in chromium or in firefox then nothing happens.  
Mouse work correctly.  
Also I found that I can type in inputs but if I have selected text on a page and then press shift+left or shift +right then I can change selection size.

I have tried with --noprofile but any difference.

I have tied  
~~~
ignore private-tmp  
ignore net  
~~~
in /etc/firejail/chromium.profile  
in /etc/firejail/chromium-browser.profile  
in /etc/firejail/firefox.profile  
but any difference.

I think it can be related to this  
[https://firejail.wordpress.com/support/known-problems/#ibus](https://firejail.wordpress.com/support/known-problems/#ibus)

> Cannot connect to ibus-daemon in a new network namespace
> 
> ibus-daemon is used to change the system language, for example to switch between English (US) input and Japanese inputs. In a sandbox using a new network namespace ibus-daemon socket is disabled and keyboard switching capability is lost.

When I try to type text in browser then I see this message in console

(chromium-browser:2): IBUS-WARNING **: Events queue growing too big, will start to drop.

Full console log

 [SkewedZeppelin](https://github.com/SkewedZeppelin) mentioned this issue [on 13 Mar 2018](https://github.com/netblue30/firejail/issues/1204#ref-issue-304305476)

[Firefox and Chromiun not taking input and not appearning in `firejail --list` #1810](https://github.com/netblue30/firejail/issues/1810)

 Closed

 [SkewedZeppelin](https://github.com/SkewedZeppelin) mentioned this issue [on 26 Mar 2018](https://github.com/netblue30/firejail/issues/1204#ref-issue-308388982)

[Keyboard input doesn't work #1836](https://github.com/netblue30/firejail/issues/1836)

 Closed



### shemgp on 15 Dec 2020

Fixed for me: [#116 (comment)](https://github.com/netblue30/firejail/issues/116#issuecomment-271516261)


