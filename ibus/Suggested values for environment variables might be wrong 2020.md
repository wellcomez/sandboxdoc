---
layout: post
tags:
- ibus
- firejail
- firefox
- chrome
title: "Suggested values for environment variables might be wrong 2020"

---

# Suggested values for environment variables might be wrong 2020
[https://github.com/ibus/ibus](https://github.com/ibus/ibus)

[Suggested values for environment variables might be wrong #2020](https://github.com/ibus/ibus/issues/2020#start-of-content) Closed

### graywolf on 4 Jul 2018

Please fill in the following items if you don't know the root cause.

#### Which distribution and version?:  
archlinux

#### Which desktop environment and version?:  
i3-wm 4.15-1

#### Which session type?:  
X11

#### Which application and version?:  
chromium 67.0.3396.87-2  
firefox 61.0-1

#### IBus version?: IBus 1.5.18

#### Issue description:  
<mark class="hltr-pink">When ibus-setup finishes, it prints</mark>

```shell
IBus has been started! If you cannot use IBus, please add below lines in ~/.bashrc, and relogin your desktop.
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
```

<mark class="hltr-pink">however, with these valus neither chromium nor firefox accepts ibus input. Changing the values to</mark>

```shell
export GTK_IM_MODULE=xim
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=xim
```

Does fix the issue and everything behaves as expected now. (See? I can input 日本語 now).

I'm not however sure if this has other implications I'm missing. I've tried few programs and they all seem to work fine.

#### Do you see any errors when you run ibus-daemon with the verbose option?: 
no

#### Can you reproduce your problem with a new user account instead of the current your account? 
yes



Member

### fujiwarat on 5 Jul 2018

Your firefox does not work with ibus but gedit works with ibus?

I guess you might use gtk2 firefox and do not install ibus-gtk2.  
What is the output of `env GTK_IM_MODULE=ibus strace /usr/lib*/firefox/firefox |& grep immodule` ?

(Note the path of the binary firefox depends on your distribution but it's not the path of the script /usr/bin/firefox what's why I added '*')



### Author graywolf on 8 Jul 2018

Hm, after some digging, this is probably caused by firejail. But weird is that I run everything in it anyway, so I'm not sure why only firefox and chromium are affected. :/

<mark class="hltr-red">What actual advantages does `GTK_IM_MODULE=ibus` have over `GTK_IM_MODULE=xim`? Do I miss on anything by using `xim`? I did not notice any difference so far.</mark>



Member

### fujiwarat on 9 Jul 2018

> What actual advantages does GTK_IM_MODULE=ibus have over GTK_IM_MODULE=xim ?

Anyway try to get the output and paste it on this issue.



Member

### fujiwarat on 17 Jul 2018

ping

 [fujiwarat](https://github.com/fujiwarat) closed this as [completed](https://github.com/ibus/ibus/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 17 Jul 2018](https://github.com/ibus/ibus/issues/2020#event-1736827139)



### Author graywolf on 21 Jul 2018

```shell
$ env GTK_IM_MODULE=ibus strace /usr/lib*/firefox/firefox |& grep immodule
openat(AT_FDCWD, "/usr/lib/gtk-3.0/3.0.0/immodules.cache", O_RDONLY) = 3
stat("/usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so", {st_mode=S_IFREG|0755, st_size=30512, ...}) = 0
openat(AT_FDCWD, "/usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so", O_RDONLY|O_CLOEXEC) = 3
```



Member

### fujiwarat on 23 Jul 2018

<mark class="hltr-red">Seems your firefox loads im-ibus.so.  </mark>
When you run `env GTK_IM_MODULE=ibus firefox`, can you reproduce your problem?

<mark class="hltr-blue">Also can you reproduce your problem with gedit?</mark>



### Author graywolf on 30 Jul 2018

<mark class="hltr-orange">Yep I can, `env GTK_IM_MODULE=ibus firefox` fails (ibus seems to be ignored). `env GTK_IM_MODULE=ibus gedit` works fine (I can input japanese).</mark>

<mark class="hltr-pink">Since `xim` just works for me, I' tempted to just use that, however I'm wonders on what I'm missing compared to `ibus`.</mark>




### fujiwarat on 30 Jul 2018

Hmm..., strange.  
Can you check the results of the following commands?

```shell
% file /usr/lib*/firefox/firefox 
% file /usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so
% ldd /usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so
```




### fujiwarat on 30 Jul 2018

Also which ibus Japanese engine do you use?  
You can check with `ibus engine` command.



### graywolf on 31 Jul 2018

Hm it's definitely a firejail issue, `env GTK_IM_MODULE=ibus firefox` does not work while `env GTK_IM_MODULE=ibus /usr/bin/firefox` does. I forgot that I have symlink to firejail as `/usr/local/bin/firefox`.

```shell
[ wolf@ws ] :: ~
Load: 0.52 0.46 0.64 , Memory: 9.19 %, Disk: 71.45 %
$ file /usr/lib*/firefox/firefox 
/usr/lib64/firefox/firefox: ELF 64-bit LSB pie executable x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5431947d8babb8231519d0caafe65e5471635e12, stripped
/usr/lib/firefox/firefox:   ELF 64-bit LSB pie executable x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5431947d8babb8231519d0caafe65e5471635e12, stripped

[ wolf@ws ] :: ~
Load: 0.52 0.46 0.64 , Memory: 9.19 %, Disk: 71.45 %
$ file /usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so
/usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so: ELF 64-bit LSB pie executable x86-64, version 1 (SYSV), dynamically linked, BuildID[sha1]=8742b341f3eec823cb205221929ec19c04e5db9e, stripped

[ wolf@ws ] :: ~
Load: 0.47 0.45 0.64 , Memory: 9.19 %, Disk: 71.45 %
$ ldd /usr/lib/gtk-3.0/3.0.0/immodules/im-ibus.so
linux-vdso.so.1 (0x00007ffc1efaf000)
libgtk-3.so.0 => /usr/lib/libgtk-3.so.0 (0x00007f55bf88e000)
libgdk-3.so.0 => /usr/lib/libgdk-3.so.0 (0x00007f55bf599000)
libpangocairo-1.0.so.0 => /usr/lib/libpangocairo-1.0.so.0 (0x00007f55bf38c000)
libpango-1.0.so.0 => /usr/lib/libpango-1.0.so.0 (0x00007f55bf145000)
libfribidi.so.0 => /usr/lib/libfribidi.so.0 (0x00007f55bef29000)
libatk-1.0.so.0 => /usr/lib/libatk-1.0.so.0 (0x00007f55bed03000)
libcairo-gobject.so.2 => /usr/lib/libcairo-gobject.so.2 (0x00007f55beafa000)
libcairo.so.2 => /usr/lib/libcairo.so.2 (0x00007f55be7d7000)
libgdk_pixbuf-2.0.so.0 => /usr/lib/libgdk_pixbuf-2.0.so.0 (0x00007f55be5b3000)
libdbus-1.so.3 => /usr/lib/libdbus-1.so.3 (0x00007f55be362000)
libibus-1.0.so.5 => /usr/lib/libibus-1.0.so.5 (0x00007f55be0f1000)
libgio-2.0.so.0 => /usr/lib/libgio-2.0.so.0 (0x00007f55bdd40000)
libgobject-2.0.so.0 => /usr/lib/libgobject-2.0.so.0 (0x00007f55bdaec000)
libglib-2.0.so.0 => /usr/lib/libglib-2.0.so.0 (0x00007f55bd7d5000)
libpthread.so.0 => /usr/lib/libpthread.so.0 (0x00007f55bd5b7000)
libc.so.6 => /usr/lib/libc.so.6 (0x00007f55bd1fb000)
libgmodule-2.0.so.0 => /usr/lib/libgmodule-2.0.so.0 (0x00007f55bcff7000)
libX11.so.6 => /usr/lib/libX11.so.6 (0x00007f55bccb8000)
libXi.so.6 => /usr/lib/libXi.so.6 (0x00007f55bcaa7000)
libXfixes.so.3 => /usr/lib/libXfixes.so.3 (0x00007f55bc8a1000)
libatk-bridge-2.0.so.0 => /usr/lib/libatk-bridge-2.0.so.0 (0x00007f55bc66f000)
libepoxy.so.0 => /usr/lib/libepoxy.so.0 (0x00007f55bc342000)
libpangoft2-1.0.so.0 => /usr/lib/libpangoft2-1.0.so.0 (0x00007f55bc12d000)
libfontconfig.so.1 => /usr/lib/libfontconfig.so.1 (0x00007f55bbeea000)
libm.so.6 => /usr/lib/libm.so.6 (0x00007f55bbb55000)
libXinerama.so.1 => /usr/lib/libXinerama.so.1 (0x00007f55bb952000)
libXrandr.so.2 => /usr/lib/libXrandr.so.2 (0x00007f55bb747000)
libXcursor.so.1 => /usr/lib/libXcursor.so.1 (0x00007f55bb53d000)
libXcomposite.so.1 => /usr/lib/libXcomposite.so.1 (0x00007f55bb33a000)
libXdamage.so.1 => /usr/lib/libXdamage.so.1 (0x00007f55bb137000)
libxkbcommon.so.0 => /usr/lib/libxkbcommon.so.0 (0x00007f55baef8000)
libwayland-cursor.so.0 => /usr/lib/libwayland-cursor.so.0 (0x00007f55bacf0000)
libwayland-egl.so.1 => /usr/lib/libwayland-egl.so.1 (0x00007f55baaee000)
libwayland-client.so.0 => /usr/lib/libwayland-client.so.0 (0x00007f55ba8df000)
libXext.so.6 => /usr/lib/libXext.so.6 (0x00007f55ba6cd000)
librt.so.1 => /usr/lib/librt.so.1 (0x00007f55ba4c5000)
libfreetype.so.6 => /usr/lib/libfreetype.so.6 (0x00007f55ba1fc000)
libthai.so.0 => /usr/lib/libthai.so.0 (0x00007f55b9ff2000)
libpixman-1.so.0 => /usr/lib/libpixman-1.so.0 (0x00007f55b9d4a000)
libpng16.so.16 => /usr/lib/libpng16.so.16 (0x00007f55b9b14000)
libxcb-shm.so.0 => /usr/lib/libxcb-shm.so.0 (0x00007f55b9910000)
libxcb.so.1 => /usr/lib/libxcb.so.1 (0x00007f55b96e7000)
libxcb-render.so.0 => /usr/lib/libxcb-render.so.0 (0x00007f55b94d9000)
libXrender.so.1 => /usr/lib/libXrender.so.1 (0x00007f55b92ce000)
libz.so.1 => /usr/lib/libz.so.1 (0x00007f55b90b7000)
libsystemd.so.0 => /usr/lib/libsystemd.so.0 (0x00007f55b8e2d000)
libresolv.so.2 => /usr/lib/libresolv.so.2 (0x00007f55b8c16000)
libmount.so.1 => /usr/lib/libmount.so.1 (0x00007f55b89be000)
libffi.so.6 => /usr/lib/libffi.so.6 (0x00007f55b87b5000)
libpcre.so.1 => /usr/lib/libpcre.so.1 (0x00007f55b8543000)
/usr/lib64/ld-linux-x86-64.so.2 (0x00007f55c03aa000)
libdl.so.2 => /usr/lib/libdl.so.2 (0x00007f55b833f000)
libatspi.so.0 => /usr/lib/libatspi.so.0 (0x00007f55b810d000)
libharfbuzz.so.0 => /usr/lib/libharfbuzz.so.0 (0x00007f55b7e61000)
libexpat.so.1 => /usr/lib/libexpat.so.1 (0x00007f55b7c2f000)
libuuid.so.1 => /usr/lib/libuuid.so.1 (0x00007f55b7a28000)
libbz2.so.1.0 => /usr/lib/libbz2.so.1.0 (0x00007f55b7818000)
libdatrie.so.1 => /usr/lib/libdatrie.so.1 (0x00007f55b7611000)
libXau.so.6 => /usr/lib/libXau.so.6 (0x00007f55b740d000)
libXdmcp.so.6 => /usr/lib/libXdmcp.so.6 (0x00007f55b7207000)
liblzma.so.5 => /usr/lib/liblzma.so.5 (0x00007f55b6fe1000)
liblz4.so.1 => /usr/lib/liblz4.so.1 (0x00007f55b6dc4000)
libgcrypt.so.20 => /usr/lib/libgcrypt.so.20 (0x00007f55b6aa8000)
libblkid.so.1 => /usr/lib/libblkid.so.1 (0x00007f55b6858000)
libgraphite2.so.3 => /usr/lib/libgraphite2.so.3 (0x00007f55b662c000)
libgpg-error.so.0 => /usr/lib/libgpg-error.so.0 (0x00007f55b640c000)

```

```
$ ibus engine
xkb:us::eng
```




### fujiwarat on 31 Jul 2018

So you should check ibus would be available in issue 2020#issuecomment-406801282 .

```shell
$ file /usr/lib*/firefox/firefox 
/usr/lib64/firefox/firefox: ELF 64-bit LSB pie executable x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5431947d8babb8231519d0caafe65e5471635e12, stripped
/usr/lib/firefox/firefox:   ELF 64-bit LSB pie executable x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=5431947d8babb8231519d0caafe65e5471635e12, stripped
```

Why do you have /usr/lib64/firefox/firefox and /usr/lib/firefox/firefox ?  
/usr/lib64/firefox or /usr/lib/firefox, /usr/lib or /usr/lib64 is a symlink?

Do you wish to fix the firejail issue? or do you resolve your issue to use firefox?



### Author graywolf on 31 Jul 2018

Well my issue is "fixed" by using `GTK_IM_MODULE=xim` instead of `GTK_IM_MODULE=ibus`, <mark style="background: #FF5582A6;">so I guess at the moment my question is what is a downside of this? Do I lose any features? I did not notice anythig so far so I'm curious.</mark>

As for your other questions, I'm not sure what to tell you, this is how archlinux installs it from system repository..




### fujiwarat on 31 Jul 2018

> so I guess at the moment my question is what is a downside of this?

<mark class="hltr-red">Your setting would be wrong and using ibus with GTK_IM_MODULE=xim is also wrong. You should set XMODIFIERS=[@im](https://github.com/im)=none</mark>


### graywolf mentioned this issue  on 14 Aug 2018  [[Unable to use ibus-daemon in firejail NO.116]] Closed
- <mark class="hltr-yellow">graywolf mentioned this issue  on 14 Aug 2018</mark>  [[Unable to use ibus-daemon in firejail NO.116]] Closed



### Apteryks on 15 Aug 2018

I had the same problem, using GuixSD with a minimal desktop (ratpoison WM).

I tried setting these:  
~~~bash
GTK_DATA_PREFIX=/run/current-system/profile  
GUIX_GTK2_IM_MODULE_FILE=$HOME/.guix-profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache  
GUIX_GTK3_IM_MODULE_FILE=$HOME/.guix-profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache  
BUS_COMPONENT_PATH=$HOME/.guix-profile/share/ibus/component  
GTK_IM_MODULE=ibus  
QT_IM_MODULE=ibus  
XMODIFIERS=[@im](https://github.com/im)=ibus
~~~

<mark class="hltr-orange">But it wouldn't work anywhere. I also tried using "XMODIFIERS=[@im](https://github.com/im)=none" as suggested above, but it wouldn't help.</mark>

I can only use ibus successfully using the OP's solution:  
~~~bash
export GTK_IM_MODULE=xim  
export XMODIFIERS=[@im](https://github.com/im)=ibus  
export QT_IM_MODULE=xim
~~~



Member

### fujiwarat on 16 Aug 2018

> I had the same problem, using GuixSD with a minimal desktop (ratpoison WM).

I don't think it's the same problem.



### ghost on 27 Aug 2018

Same setup and issue as graywolf, but I'm not using firejail.  
Also fixed (?) with GTK_IM_MODULE=xim



Member

### fujiwarat on 27 Aug 2018

> Same setup and issue as graywolf, but I'm not using firejail.

I don't think your problem is same and if you need helps to resolve your configurations around input methods, please open a new issue.