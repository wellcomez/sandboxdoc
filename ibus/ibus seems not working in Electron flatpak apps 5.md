---
layout: post
tags:
- flathub
title: "ibus seems not working in Electron flatpak apps 5"

---
# ibus seems not working in Electron flatpak apps 5

[juhp](https://github.com/juhp) opened this issue on 8 Nov 2018 · 1 comment
[ibus seems not working in Electron flatpak apps](https://github.com/flathub/org.electronjs.Electron2.BaseApp/issues/5#top)#5 Closed

[juhp](https://github.com/juhp) opened this issue on 8 Nov 2018 · 1 comment 


### juhp commented on 8 Nov 2018

See [flatpak/flatpak#1671](electron%20apps%20do%20not%20recognize%20ibus%201671.md).  
If there is a way to enable this in Electron2 BaseApp that would be highly desirable.

cc [@fujiwarat](https://github.com/fujiwarat)



### fujiwarat commented on 8 Jan 2019

The pure GTK3 application on Electron is no problem.

 
[TingPing](https://github.com/TingPing) closed this as [completed](https://github.com/flathub/org.electronjs.Electron2.BaseApp/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 9 Jan 2019](https://github.com/flathub/org.electronjs.Electron2.BaseApp/issues/5#event-2061092004)