---
layout: post
tags:
- flatpak
- snapd
- fontconfig
title: "CJK fonts are all tofu in electron apps 1556"

---

# CJK fonts are all tofu in electron apps 1556

[CJK fonts are all tofu in electron apps #1556](https://github.com/flatpak/flatpak/issues/1556) Closed

[libertylocked](https://github.com/libertylocked) opened this issue on 6 Apr 2018 · 8 comments



[libertylocked](https://github.com/libertylocked)

### libertylocked [on 6 Apr 2018]

## Linux distribution and version

Antergos Linux, 4.15.15-1-ARCH

## Flatpak version

Flatpak 0.11.3

## Description of the problem

-   Host system has `adobe-source-han-sans-otc-fonts 1.004-2` installed
-   CJK fonts display correctly on non-flatpak applications
    -   Even snappy apps can display them correctly
-   CJK characters are all tofu in flatpak electron apps
    -   I tried Discord and Signal

## Steps to reproduce

-   Install adobe source CJK fonts with `pacman -S adobe-source-han-sans-otc-fonts`
-   Install flatpak with `pacman -S flatpak`
-   Install signal with `flatpak install flathub org.signal.Signal`
-   Open signal and send yourself a message with CJK chars
-   Tofu!

[![screenshot from 2018-04-05 22-11-01](https://user-images.githubusercontent.com/1425794/38400493-4049cf4c-391e-11e8-923b-6bc5d22037d4.png)](https://user-images.githubusercontent.com/1425794/38400493-4049cf4c-391e-11e8-923b-6bc5d22037d4.png)

Similar case with Discord  
[![screenshot from 2018-04-05 22-49-14](https://user-images.githubusercontent.com/1425794/38401412-9cc72742-3923-11e8-8386-b91c1b67ceb3.png)](https://user-images.githubusercontent.com/1425794/38401412-9cc72742-3923-11e8-8386-b91c1b67ceb3.png)

## Other info that might be useful

Running `fc-list`

```shell
[liberty@gumo ~]$ flatpak run --command=fc-list org.signal.Signal | grep adobe
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Heavy.ttc: Source Han Sans SC Heavy,思源黑体,Source Han Sans SC:style=Regular,Heavy
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Light.ttc: Source Han Sans Light,源ノ角ゴシック,Source Han Sans:style=Regular,Light
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Normal.ttc: Source Han Sans Normal,源ノ角ゴシック,Source Han Sans:style=Regular,Normal
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans,Source Han Sans Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans HW TC,思源黑體 HW Regular,Source Han Sans HW TC Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Light.ttc: Source Han Sans TC,思源黑體 Light,Source Han Sans TC Light:style=Light,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Medium.ttc: Source Han Sans K Medium,본고딕,Source Han Sans K:style=Regular,Medium
/run/host/fonts/adobe-source-han-sans/SourceHanSans-ExtraLight.ttc: Source Han Sans TC,思源黑體 ExtraLight,Source Han Sans TC ExtraLight:style=ExtraLight,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Heavy.ttc: Source Han Sans TC,思源黑體 Heavy,Source Han Sans TC Heavy:style=Heavy,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Normal.ttc: Source Han Sans TC,思源黑體 Normal,Source Han Sans TC Normal:style=Normal,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-ExtraLight.ttc: Source Han Sans ExtraLight,源ノ角ゴシック,Source Han Sans:style=Regular,ExtraLight
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans K,Source Han Sans K Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans TC,思源黑體 Bold,Source Han Sans TC Bold:style=Bold,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans HW K Bold,본고딕 HW,Source Han Sans HW K:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans HW SC,Source Han Sans HW SC Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans Bold,源ノ角ゴシック,Source Han Sans:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Light.ttc: Source Han Sans K Light,본고딕,Source Han Sans K:style=Regular,Light
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Medium.ttc: Source Han Sans Medium,源ノ角ゴシック,Source Han Sans:style=Regular,Medium
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans TC,思源黑體 Regular,Source Han Sans TC Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Heavy.ttc: Source Han Sans Heavy,源ノ角ゴシック,Source Han Sans:style=Regular,Heavy
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Light.ttc: Source Han Sans SC Light,思源黑体,Source Han Sans SC:style=Regular,Light
/run/host/fonts/adobe-source-han-sans/SourceHanSans-ExtraLight.ttc: Source Han Sans SC ExtraLight,思源黑体,Source Han Sans SC:style=Regular,ExtraLight
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans SC,Source Han Sans SC Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Medium.ttc: Source Han Sans SC Medium,思源黑体,Source Han Sans SC:style=Regular,Medium
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans SC Bold,思源黑体,Source Han Sans SC:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans K Bold,본고딕,Source Han Sans K:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans HW TC,思源黑體 HW Bold,Source Han Sans HW TC Bold:style=Bold,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans HW SC Bold,思源黑体 HW,Source Han Sans HW SC:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Heavy.ttc: Source Han Sans K Heavy,본고딕,Source Han Sans K:style=Regular,Heavy
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Normal.ttc: Source Han Sans SC Normal,思源黑体,Source Han Sans SC:style=Regular,Normal
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans HW,Source Han Sans HW Regular:style=Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Medium.ttc: Source Han Sans TC,思源黑體 Medium,Source Han Sans TC Medium:style=Medium,Regular
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Bold.ttc: Source Han Sans HW Bold,源ノ角ゴシック HW,Source Han Sans HW:style=Regular,Bold
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Normal.ttc: Source Han Sans K Normal,본고딕,Source Han Sans K:style=Regular,Normal
/run/host/fonts/adobe-source-han-sans/SourceHanSans-ExtraLight.ttc: Source Han Sans K ExtraLight,본고딕,Source Han Sans K:style=Regular,ExtraLight
/run/host/fonts/adobe-source-han-sans/SourceHanSans-Regular.ttc: Source Han Sans HW K,Source Han Sans HW K Regular:style=Regular
```

Running Signal from terminal

```shell
[liberty@gumo flatpak]$ flatpak run org.signal.Signal
Gtk-Message: Failed to load module "canberra-gtk-module"
NODE_ENV production
NODE_CONFIG_DIR /app/Signal/resources/app.asar/config
NODE_CONFIG {}
ALLOW_CONFIG_MUTATIONS undefined
HOSTNAME undefined
NODE_APP_INSTANCE undefined
SUPPRESS_NO_CONFIG_WARNING undefined
Set Windows Application User Model ID (AUMID) { appUserModelId: 'org.whispersystems.signal-desktop' }
userData: /home/liberty/.var/app/org.signal.Signal/config/Signal
making app single instance
OS Release: 4.15.15-1-ARCH - notifications polyfill? false
{"name":"log","hostname":"gumo","pid":3,"level":30,"msg":"app ready","time":"2018-04-06T02:14:57.838Z","v":0}
......
```

fontconfig version is 2.13.0




### alexlarsson [on 9 Apr 2018]

I wonder if this is due to the fontconfig /etc/fonts/conf.d/ files not being exported to the app?


### libertylocked [on 15 Apr 2018]

If it helps, I noticed that the issue only started occuring after fontconfig is updated to 2.13.0.


### ghost [on 17 Apr 2018]

I have the same problem. (I am Korean.)

I have the same problem with the steam app and the spotify app. The default setting, sans-serif did not seem to support Korean Font(Hangul).  
In the following location, the fonts are as shown below.

```shell
$ pwd /var/lib/flatpak/runtime/org.freedesktop.Platform/x86_64/1.6/active/files/share/fonts 

$ ls dejavu eosrei-emojione gnu-free google-crosextra-caladea google-crosextra-carlito liberation-fonts
```

I copied the naver-nanum font that supports CJK fonts.


```
$ sudo cp -a /usr/share/fonts/naver-nanum /var/lib/flatpak/runtime/org.freedesktop.Platform/x86_64/1.6/active/files/share/fonts/
```

The CJK font was output normally after copying the font. It recognizes the font cache information of the host OS, but seems to be limited to the runtime to display actual fonts.

My OS is fedora rawhide, so I do not know exactly what the effect is.

The version of the fontconfig package I use is also 2.13.0.

```
$ rpm -qv fontconfig  
fontconfig-2.13.0-3.fc29.x86_64  
fontconfig-2.13.0-3.fc29.i686
```

I am sorry that I can not speak English and can not give much information.


###  J-J-Chiarella  on 14 May 2018

@kbjoon79 영어가 불편하시다고 하셨어요? ("I am sorry that I can not speak English and can not give much information."이라고) 하고 싶으신 말씀이 있으시면 제가 번역해 드릴게요.

어쨌든 저는 글꼴이 잘 되지만 flatpak의 electron으로 ibus는 작동하지 않아요.

<mark style="background: #FF5582A6;">I am having a script problem as well, but it is different. Maybe still, it is related.
</mark>
Fonts work fine for me (flatpak on Ubuntu 18.04 0.11.3+ work).

<mark class="hltr-red">The problem is that electron apps do not recognize ibus. I have to type in a normal window (e.g., Mousepad) and copy-paste the contents into the window.
</mark>
I also just wrote a note to kbjoon letting him know I can translate any info he wanted to give but could not. Despite his protestations (inability to speak English). His command is fine unless he considers a missing semi-colon and a comma to be crippling, which most speakers do not.


### libertylocked on 4 Jun 2018

The issue seems to be gone after a few updates.  
Flatpak version is 0.11.7 and fontconfig version is 2.13.0

```
[liberty@gumo ~]$ flatpak list
com.discordapp.Discord/x86_64/stable            system,current
org.freedesktop.Platform.VAAPI.Intel/x86_64/1.6 system,runtime
org.freedesktop.Platform.ffmpeg/x86_64/1.6      system,runtime
org.freedesktop.Platform/x86_64/1.6             system,runtime
org.gtk.Gtk3theme.Arc-Darker/x86_64/3.22        system,runtime
```



### alexlarsson on 4 Jun 2018

Yeah, this was a fontconfig 2.13.0 on-host regression that is now fixed in the runtime.

[alexlarsson](https://github.com/alexlarsson) [alexlarsson](https://github.com/alexlarsson) closed this as [completed](https://github.com/flatpak/flatpak/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 4 Jun 2018](https://github.com/flatpak/flatpak/issues/1556#event-1660539188)


### kode54 on 2 Apr 2019

Now on fontconfig 2.13.1, on Arch, and experiencing this issue again.


### matthiasclasen on 4 Apr 2019

please reopen issues or file new ones




