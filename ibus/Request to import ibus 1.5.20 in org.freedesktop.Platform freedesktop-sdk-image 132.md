
---
layout: post
tags:
- flatpak-sdk
- flatpak
- electron
- 1.5.20
title: "Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image
  132"

---

# Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image

[Request to import ibus 1.5.20 in org.freedesktop.Platform](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681)

### Context

The old IBus GTK IM module has a bug with Atom and Slack in Flatpak.

[https://github.com/flatpak/flatpak/issues/1671](https://github.com/flatpak/flatpak/issues/1671)

### Description

Could you please import ibus 1.5.20 in org.freedesktop?

The IBus version includes the fixes to work with Atom and Slack applications:

[https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1](https://github.com/ibus/ibus/commit/be7fb813e530442897a9f9130b8a26380e5a12a1)

I think to update the version in ibus.bst:

[https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/18.08/elements/desktop/ibus.bst](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/blob/18.08/elements/desktop/ibus.bst)

### Acceptance Criteria

IBus 1.5.20 is now released officially.


## Activity

Sort or filter

-   [Javier Jardón](https://gitlab.com/jjardon) mentioned in merge request [!1075 (merged)](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/1075 "elements/desktop/ibus.bst: Update to 1.5.20") [3 years ago](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681#note_146861023)
    
-   [![Javier Jardón](https://secure.gravatar.com/avatar/d7cccb4d879ffcbab0533b459dfc370b?s=80&d=identicon)](https://gitlab.com/jjardon)
    
    [Javier Jardón](https://gitlab.com/jjardon)[@jjardon](https://gitlab.com/jjardon)· [3 years ago](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681#note_146861127)
    
    Owner
    
    [@fujiwarat](https://gitlab.com/fujiwarat "Takao Fujiwara") Thanks for raising this; MR on its way
    
-   [![Takao Fujiwara](https://secure.gravatar.com/avatar/e6b114bf8809395d19722b6be68fd109?s=80&d=identicon)](https://gitlab.com/fujiwarat)
    
    [Takao Fujiwara](https://gitlab.com/fujiwarat)[@fujiwarat](https://gitlab.com/fujiwarat)· [3 years ago](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681#note_146878698)
    
    Author
    
    [@jjardon](https://gitlab.com/jjardon "Javier Jardón") Thank you!
    
-   [Freedesktop SDK Merge Bot](https://gitlab.com/fsdk-marge-bot) closed via merge request [!1075 (merged)](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/merge_requests/1075 "elements/desktop/ibus.bst: Update to 1.5.20") [3 years ago](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681#note_148616966)
    
-   [Freedesktop SDK Merge Bot](https://gitlab.com/fsdk-marge-bot) mentioned in commit [cb382ed1](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/commit/cb382ed18cc1d8783d1eafadf7040f62a9a465ba "Merge branch 'jjardon/ibus_1_5_20' into '18.08'") [3 years ago](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/issues/681#note_148616976)