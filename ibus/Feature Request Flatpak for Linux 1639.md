---
layout: post
tags:
- flatpak
- flathub
- electron
- "app\u96C6\u6210"
title: "Feature Request Flatpak for Linux 1639"

---

# Feature Request Flatpak for Linux 1639
[signalapp](https://github.com/signalapp)/[Signal-Desktop](https://github.com/signalapp/Signal-Desktop)Public

[Feature Request: Flatpak for Linux #1639](https://github.com/signalapp/Signal-Desktop/issues/1639) Closed

[comzeradd](https://github.com/comzeradd) opened this issue on 1 Nov 2017 · 71 comments  Closed





### comzeradd on 1 Nov 2017

It would be nice to have the app distributed as a [Flatpak](http://flatpak.org/).

It would make installation so much easier (and more secure), even for APT distributions.



 [exploide](https://github.com/exploide) mentioned this issue [on 1 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-255461187) [Electron - No Stretch Repo #1431](https://github.com/signalapp/Signal-Desktop/issues/1431) Closed

1 task

 [janvlug](https://github.com/janvlug) mentioned this issue [on 1 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-270257950) [Warn non-APT based Linux distribution users before migrating #1643](https://github.com/signalapp/Signal-Desktop/issues/1643)Closed

 [breznak](https://github.com/breznak) mentioned this issue [on 1 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-270189871)[The Signal Electron App is not secure #1635](https://github.com/signalapp/Signal-Desktop/issues/1635)
 Open

 [scottnonnenberg](https://github.com/scottnonnenberg) added the [Feature Request](https://github.com/signalapp/Signal-Desktop/labels/Feature%20Request) label [on 3 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#event-1323793382)[diazbastian](https://github.com/diazbastian) mentioned this issue [on 3 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-270157275)

 [Packages for rpm-based linux distributions #1630](https://github.com/signalapp/Signal-Desktop/issues/1630) Closed




### Contributor scottnonnenberg on 3 Nov 2017

What do you know about update mechanisms for flatpak? Automatic updates? Or perhaps just notifications of new versions and easy manual upgrade?





### vrutkovs on 3 Nov 2017

Flatpak apps can be updated automatically, rolled back and checked for updated, very similarly to git. Every application can be installed from a remote repo, so every time the app is updated remotely the users would pull the changes and can update the app easily. Each change has its own commit hash, so it can also be easily rolled back. Flatpak repo can have multiple branches (i.e. "master" and "stable"). The app can be packed in a bundle and distributed without a repo.

I'm running Signal in flatpak using released .debs, see [https://github.com/vrutkovs/flatpak-signal](https://github.com/vrutkovs/flatpak-signal)





### ghost on 5 Nov 2017

If a tarball distribution is provided, I'd be willing to put the time in to have it included in the OpenBSD ports system as well.





### Contributor scottnonnenberg on 7 Nov 2017

Can someone point me to some documentation on flatpak auto-update mechanisms?





### vrutkovs on 7 Nov 2017

Flatpak is using [ostree](https://ostree.readthedocs.io/en/latest/manual/introduction/) to distribute updates, its model is very similar to git. [Here](https://blogs.gnome.org/alexl/2017/10/02/on-application-sizes-and-bloat-in-flatpak/) Flatpak maintainer Alex Larsson wrote a great blog post about the contents of the repo.

From user perspective it looks like this:

-   Install the app:

```
flatpak --user install https://vrutkovs.github.io/flatpak-signal/signal.flatpakref
```

-   Check app info:

```
$ flatpak --user info org.signal.Desktop
Ref: app/org.signal.Desktop/x86_64/master
ID: org.signal.Desktop
Arch: x86_64
Branch: master
Origin: org.signal.Desktop-origin
Commit: a24ca2533d206e192e6e4a99fbc32bc1b075164826d0d725062343ca5d6ef6e8
Location: /home/vrutkovs/.local/share/flatpak/app/org.signal.Desktop/x86_64/master/a24ca2533d206e192e6e4a99fbc32bc1b075164826d0d725062343ca5d6ef6e8
Installed size: 256.6 MB
Runtime: org.freedesktop.Platform/x86_64/1.6
```

It shows that commit `a24ca2533d206e192e6e4a99fbc32bc1b075164826d0d725062343ca5d6ef6e8` is installed.

-   Roll back to previous version (its currently not part of flatpak CLI though):

```
$ ostree log --repo=repo app/org.signal.Desktop/x86_64/master      
commit 2e9193e66e18b55e7222ed0c34e5f565536df857a8f7e6ab28978089463405ee
Date:  2017-11-04 18:29:23 +0000

	Signal Desktop Sat  4 Nov 19:28:23 CET 2017

	Name: org.signal.Desktop
	Arch: x86_64
	Branch: master
	Built with: Flatpak 0.9.99

commit 6c51a0f75e5402cd3cd6af7b48758d2bc1ed5ed961cec25145de23fce5ed11a2
Date:  2017-11-01 22:00:16 +0000

	Nightly build of Signal, Wed  1 Nov 22:59:37 CET 2017

	Name: org.signal.Desktop
	Arch: x86_64
	Branch: master
	Built with: Flatpak 0.9.99

...
$ flatpak --user update --commit=6c51a0f75e5402cd3cd6af7b48758d2bc1ed5ed961cec25145de23fce5ed11a2
```

-   Pull the latest commit from the repo:

```
$ flatpak --user update org.signal.Desktop
```

(or `flatpak --user update` to pull updates from all remotes)

Feel free to jump in at #flatpak on freenode and ask questions





### garrett on 7 Nov 2017

There's already been a lot of discussion of Flatpak above. One thing that's lacking is the experience from using Flatpak on the desktop.

## Advantages of Flatpak

-   [Works on all the major Linux distros](http://flatpak.org/getting): Alpine, Arch, Debian, Endless OS, Fedora, Gentoo, Mageia, openSUSE, Ubuntu, Solus
-   Easy to install
-   Sandboxed, so it's much better for security than traditional package formats
-   Updates are quick and lightweight compared to RPMs, DEBs, etc. (do to deltas being handled similar to git)
-   Can install via clicking a link on a website (see below)
-   If you publish Signal in Flathub and someone has already subscribed to Flathub for another app, then they can even search for Signal in their desktop's native app store

## A desktop centric view of installing a Flatpak app

Let's use the Blender app on [Flathub](https://flathub.org/) as an example.

On [the Flathub website](https://flathub.org/apps.html), the app is styled like a tile, like this:  
[![screenshot-2017-11-7 applications hosted on flathub 1](https://user-images.githubusercontent.com/10246/32484768-525b267e-c3a1-11e7-83ef-f2e7b74b0e31.png)](https://user-images.githubusercontent.com/10246/32484768-525b267e-c3a1-11e7-83ef-f2e7b74b0e31.png)

...It's just a link to [https://flathub.org/repo/appstream/org.blender.Blender.flatpakref](https://flathub.org/repo/appstream/org.blender.Blender.flatpakref) — and you can link it from other places (like here on GitHub), even if it's on Flathub. In the case of Signal, you _could_ publish on Flathub (or elsewhere) and link from Signal.org.

After clicking in the link, the `flatpakref` is immediately loaded by the app store / software center (GNOME Software in this case). It's basically just a friendlier version of the metadata provided in the `flatpakref`. It looks like this:

[![gnome-software-blender-flatpak](https://user-images.githubusercontent.com/10246/32484892-ad046fd6-c3a1-11e7-8d84-f96778ec0fc5.png)](https://user-images.githubusercontent.com/10246/32484892-ad046fd6-c3a1-11e7-8d84-f96778ec0fc5.png)

And it does have details on the same page, like the source of the Flatpak, the license, version, a link to the project's website, and place to leave reviews.

[![screenshot from 2017-11-07 10-06-31](https://user-images.githubusercontent.com/10246/32485419-a92e8944-c3a3-11e7-92ae-d651d0d576c7.png)](https://user-images.githubusercontent.com/10246/32485419-a92e8944-c3a3-11e7-92ae-d651d0d576c7.png)

After clicking install, it installs with a progress bar, and then same page has a "Launch" and "Remove" button.

### Installation summary

It's basically a 2 click install process (click on website, click install to confirm in the software app).

And updating happens automatically in the same place with all the other app & system updates happen (which means there are also notifications when updates are available), at least in the case of GNOME and KDE based distros.



 [exploide](https://github.com/exploide) mentioned this issue [on 9 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-pullrequest-270139852)

[Add rpm package creation to package.json scripts #1627](https://github.com/signalapp/Signal-Desktop/pull/1627)

 Closed

9 tasks



### charlesjt on 10 Nov 2017

Signal-desktop is now available on [flathub](https://flathub.org/apps.html). By the directives of the store, OWS can request authorship and maintain the package.

[![signal-flathub](https://user-images.githubusercontent.com/28692681/32621756-9757d780-c55f-11e7-899b-7957514e6a04.png)](https://user-images.githubusercontent.com/28692681/32621756-9757d780-c55f-11e7-899b-7957514e6a04.png)





### breznak on 15 Nov 2017

Is also `snap` "the other" format considered? But I think tarballs should be enough and leave rest to packagers





### bermeitinger-b on 16 Nov 2017

The flatpak version runs fine but who is maintaining it? The deployed version is 1.0.35 whereas the newest version currently is 1.0.38. Is the code for building the package somewhere?

Also, from my understanding, flatpak supports branches, so one could use the development branch to always be bleeding-edge, if one wishes.





### tvannahl on 23 Nov 2017

[@bermeitinger-b](https://github.com/bermeitinger-b) The flathub release of Signal is maintained [here](https://github.com/flathub/org.signal.Signal).



### lpar on 27 Nov 2017

Decided to give the Flatpak version of Signal a go. (Fedora 26.)

First tried installing from command line (`flatpak --local` ). That worked, and I could run Signal, but I couldn't access any files to import my exported data from the old web app. I got an empty home directory instead. I tried creating a folder and then went looking for it, but couldn't find it anywhere on the filesystem using locate or find. Checked documentation and FAQs, no information I can find about where it actually puts your files. So, uninstalled all that and made sure `flatpak list`showed nothing installed.

Next tried Flathub. Clicked on Signal, went through the GUI installer. It installed an icon for Signal, but it wouldn't start. Investigated via the command line:

```
flatpak update
Looking for updates...
Required runtime for org.signal.Signal/x86_64/stable (org.freedesktop.Platform/x86_64/1.6) is not installed, searching...
Found in remote flathub, do you want to install it? [y/n]:  
```

Told it to install the missing runtimes which the GUI installer hadn't bothered with. After that, I had a working Signal that could actually see my files.

So I think Flatpak isn't quite there yet as far as ordinary end users are concerned.



### Author comzeradd on 28 Nov 2017

> So I think Flatpak isn't quite there yet as far as ordinary end users are concerned.

I'm not a Flatpak expert, so I can't guess what went wrong with your installation but it worked really smoothly for me (Fedora 27). I just installed Flathub repo and all the apps, including Signal, are available through the Software app. I didn't have to open the terminal at all.



 [mark0n](https://github.com/mark0n) mentioned this issue [on 28 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-275965065)

[Missing package for Ubuntu 17.10 #1809](https://github.com/signalapp/Signal-Desktop/issues/1809)

 Closed

1 task



### dephekt on 28 Nov 2017

What happens right now if Flathub's repo or build tools are compromised? Is Flatpak downloading the .deb package to my PC from signal.org and building it all locally? Is Flatpak checking for a valid OWS GPG signature on the download before doing its work? I'm just trying to understand how the trust changes in this scheme without knowing a lot about how Flatpak handles this. I've been using the Flathub distribution for the last few weeks and I've been operating under the assumption that trust with OWS is broken this way; I'd just like to know if that's right or wrong in a (mathematically) provable way.

This isn't intended to be an indictment on Flathub or Flatpak, but I trust the build environment and key holders of OWS more so than Flathub (primarily because I'm ignorant of how Flathub does their PKI and less so inre OWS) or my own machine, so if there's a way to maintain the developer trust still while using Flathub then I'm happy. If there is not, then I think it should be made very clear on the Flathub release that it is unofficial and not signed by OWS, since anyone using Flathub already but is not privy to this thread may not be aware of the security implications that what they're installing isn't bit-for-bit the same thing Moxie signed on their offline build system using his secure keys.





### LivInTheLookingGlass on 29 Nov 2017

I have a strong preference against this. I don't want to have to install an entirely different distribution method just for one application.





### Author comzeradd on 29 Nov 2017

> I have a strong preference against this. I don't want to have to install an entirely different distribution method just for one application.

You don't _have to_. It's just another option for people who _want to_.





### lpar on 29 Nov 2017

I've no concerns regarding FlatPak being provided as well as Fedora RPMs, but I thought it was being suggested as an alternative to providing native distro packages for all the distros.





### deutrino on 11 Dec 2017

Linux Mint 18.3 now ships with native flatpak support, including full integration to their software installation manager (their equivalent of an app store) - which as a Linux user for over 20 years, even I use on occasion, if that's any indication of its usefulness to a broad spectrum of users.



### tvannahl on 11 Dec 2017

There are many reason for me that are good reasons to provide a flatpak build. In the following I do try to compare the way a software developer can provide its software for various distributions. For that I will categorize linux distributions with the following type system which only takes the desktop in consideration:

-   Packaged 1.0: A distribution which does provide only its own repositories. It's some social work to provide you're _new_software to this distribution or you would need to provide your users with an additional repository provided by you. But it has the advantage of a clearly defined trust architecture. You mainly need to trust your distribution and thats about it. An example would be _debian_.
	
-   Packaged 2.0: A distribution which does provide aside from it's own packages a platform for users to create and distribute packages. I do mean platforms like _PPA_ (Ubuntu), _Copr_ (Fedora), _AUR_ (Arch Linux), _openSuse build service_(openSuse) - in the following text I will make use of the term _PPA_ to wrap all those platforms together. This does lower the bar for users or software developers about delivering a software. I have to mention here that the _openSuse build service_ is a platform that can provide packages for multiple distributions - not just openSuse. This level has a trust issue: For every _PPA_ I - the user - have to check whether this software is provided by a cool guy in the community, the software developer itself or some mad man with a love for destruction. The _AUR_ tries to solve the this security complaint by not distributing binary packages but easy to read package build instructions. In the _PPA_ matter I do have to confront myself additionally with the question if this _PPA_ is going to be cared for in the future to provide (security) updates. And if I switch to the new distribution version I have to question myself if the software is provided for this version already via this channel. Both of my complaints about the model do have some relevance for Arch Linux as well - I don't know a Arch Linux user who is quick in updating the AUR _and_ is reading every PKGBUILD every time he updates the software or a user who has an eye on every AUR-package if it's actively maintained.
	
-   Packaged 3.0: A distribution which provides flatpak. The distribution does it thing like they used to (1.0 or 2.0) but does provide the user with the option of adding distribution independent packages. Everyone (developer, user and mad man) can provide their own repositories they can spread across the distribution boundary without caring for the users environment, rpm, deb, and the others. Especially users of smaller distributions can benefit from this model since they are not dependend from the special care by their developer, user or mad man. As mentioned the software developer can directly provide packages for multiple distributions! That is a good thing when thinking about the trust level: Flathub does provide a trustlevel similar to _Packaged 1.0_ and a Software Vendor can skip this level entirely by providing the software directly to the user. As a bonus to the user the software can be isolated[1], so the trust does not have to be absolute. But to support Ubuntu users I would say that a flatpak is currently no alternative to a native deb file (see [3]).
	
-   What about snap, it is 3.0 or not? I cannot really tell. From what I read it does not provide the distribution independence [2] of flatpak [3] nor does it seem to provide a manual about how to host you're own snap server. On the ubuntu insights blog you can find a post from 2016 [4] that does provide you with that information. But the _snapstore_ implementation referenced within that blog is not compatible with the current snapd and I have not been able to find a currently working way. I would be more than happy to extend/correct this post if I missed something. This in general would be related to issue [Please provide a distribution-neutral snap package for GNU/Linux #1798](https://github.com/signalapp/Signal-Desktop/issues/1798).
	

[@iPar](https://github.com/iPar) wrote:

> [@comzeradd](https://github.com/comzeradd) wrote:
> 
> > @gappleto97 wrote:
> > 
> > > I have a strong preference against this. I don't want to have to install an entirely different distribution method just for one application.
> > 
> > You don't have to. It's just another option for people who want to.
> 
> I've no concerns regarding FlatPak being provided as well as Fedora RPMs, but I thought it was being suggested as an alternative to providing native distro packages for all the distros.

To support distributions like Fedora, Linux Mint >= 18.3, Endless OS I do see no need to provide extra native packages that are bound to the local environment (including dependencies). You don't have the need to install _a distribution method for just one application_, you do have it already on you're system. On Ubuntu that seems to be another story as long as flatpak.org recommends installing a ppa to install flatpak, I myself would consider that overkill. In short: I do see reason in both arguments without contradiction to the opening enhancement wish.

Post scriptum: While drafting this I send out a preliminary version. I want to apologise to everyone who did receive an additional mail due to my error.

[1] SELinux and Namespace Isolation is used and may be not provided by every distribution.  
[2] [https://docs.snapcraft.io/core/install](https://docs.snapcraft.io/core/install)  
[3] [https://www.flatpak.org/getting](https://www.flatpak.org/getting)  
[4] [https://insights.ubuntu.com/2016/06/24/howto-host-your-own-snap-store/](https://insights.ubuntu.com/2016/06/24/howto-host-your-own-snap-store/)





### PorcelainMouse on 11 Dec 2017

I like the discussion here, but isn't this issue closed? I'm using Signal Desktop installed via flatpak now. (Thanks! I love it.) This enhancement request has been granted and achieved already.





### exploide on 11 Dec 2017

> I like the discussion here, but isn't this issue closed? I'm using Signal Desktop installed via flatpak now. (Thanks! I love it.) This enhancement request has been granted and achieved already.

The Signal release currently available via Flathub is not maintained by OpenWhisperSystems. There is no statement yet, if they encourage using this. In the past, OWS tended to prefer own distribution mechanisms. So this issue is probably waiting for OWS encouraging the Flathub release which they will continue to maintain or hosting their own Flatpak repository.

[@scottnonnenberg](https://github.com/scottnonnenberg) said in another issue that they probably come up with rpm support first, then investigate distribution independent mechanisms later on. However, proposed rpm instructions were not implemented so far and Chrome Apps support is removed soon. :/ So likely I will switch to the Flathub version too, if this situation persists.

(btw, I'm wondering why offering distribution specific repositories _and_ providing an independent mechanism is considered the way to go, as this increases the effort needed to release new versions)





### bermeitinger-b on 11 Dec 2017

[@dephekt](https://github.com/dephekt)

> What happens right now if Flathub's repo or build tools are compromised? Is Flatpak downloading the .deb package to my PC from signal.org and building it all locally? Is Flatpak checking for a valid OWS GPG signature on the download before doing its work? I'm just trying to understand how the trust changes in this scheme without knowing a lot about how Flatpak handles this. I've been using the Flathub distribution for the last few weeks and I've been operating under the assumption that trust with OWS is broken this way; I'd just like to know if that's right or wrong in a (mathematically) provable way.

I'm currently updating flathub's repo with new versions when they appear.  
The only security in charge, currently that I know of, is a predefined SHA256 sum of the deb package on OWS's servers. It is build on flathub's build servers and then deployed once an official maintainer accepts the update.  
From my understanding there is no signature verification. The build bot downloads the deb file from the https-source, checks the SHA256 (at least I hope), extracts the files and creates a flatpak supported filesystem.





### dephekt on 11 Dec 2017

[@bermeitinger-b](https://github.com/bermeitinger-b)

> It is build on flathub's build servers and then deployed once an official maintainer accepts the update.  
> From my understanding there is no signature verification. The build bot downloads the deb file from the https-source, checks the SHA256 (at least I hope), extracts the files and creates a flatpak supported filesystem.

This is how I believe it to work as well. I doubt OWS would endorse such a solution based on past responses they have made, to wit it's my understanding that using Flathub would introduce security vulnerabilities where an end-user cannot be sure the package they just installed is honest and byte-for-byte what OWS signed on their offline build system, even if OWS is maintaining the Flathub source manifest. You would still need to trust Flathub's build system to be secure, which personally, I would not at the present time (and if I did trust it, I would still trust it less than OWS' build system).

If the build from Flathub was deterministic, then perhaps this would be OK, but if there's no logic somewhere to easily verify this during the installation, I would think that would not be a valid solution for the non-technical user. That is to say, if the user has to do a lot of work to confirm this themselves, it would not be reliable for _the masses,_ in my opinion.

I believe Flatpak, in general, supports deterministic builds in that if you have the specific manifest and Flatpak SDK version it was built with and the input was deterministic, you should be able to get a deterministic output. [[1]](http://lists.reproducible-builds.org/pipermail/rb-general/2016-September/000062.html) That being said, I have _no idea_ how this works with Electron apps after reading [this](https://github.com/flathub/flathub/issues/59). It seems like it could be a lot of work and hacking to achieve deterministic builds of Signal Desktop through Flathub (possibly including changes to packages upstream from Flatpak), but I could be wrong.





### bermeitinger-b on 11 Dec 2017

Pardon me for a probably naive question, but what's the advantage of building the Electron package (so running npm or yarn downloading roughly a million dependencies) instead of the current solution like downloading the already build deb file?  
I guess that it should be somehow possible to verify the state of the downloaded file with the public key from [https://updates.signal.org/desktop/apt/keys.asc](https://updates.signal.org/desktop/apt/keys.asc) .

28 hidden itemsLoad more…



### heyakyra on 30 Mar 2020

Isn't the Flatpak unofficial? That's dangerous for software specifically for encryption, not to mention Signal developers have made it clear that a priority is discouraging unofficial distributions that may contain or introduce backdoors.





### yithian on 30 Mar 2020

The flathub build does not appear to be official - it's not mentioned on [https://www.signal.org/download/](https://www.signal.org/download/) and [#1639 (comment)](https://github.com/signalapp/Signal-Desktop/issues/1639#issuecomment-343240526) suggests that the flathub package is not owned or maintained by OWS.





### Contributor kenpowers-signal on 30 Mar 2020

Our official distribution channels are documented on our website: [https://www.signal.org/download/](https://www.signal.org/download/)





### compuguy on 31 Mar 2020

> Our official distribution channels are documented on our website: [https://www.signal.org/download/](https://www.signal.org/download/)

Ok....why isn't there a RPM officially provided then?



### kaushalyap on 9 Apr 2020

Is not signal already available in FlatHub? [https://flathub.org/apps/details/org.signal.Signal](https://flathub.org/apps/details/org.signal.Signal)





### ptman on 9 Apr 2020

> Is not signal already available in FlatHub? [https://flathub.org/apps/details/org.signal.Signal](https://flathub.org/apps/details/org.signal.Signal)

Please read the previous messages more carefully.





### compuguy on 9 Apr 2020

> Is not signal already available in FlatHub? [https://flathub.org/apps/details/org.signal.Signal](https://flathub.org/apps/details/org.signal.Signal)

Yes but that is packaged by a third party and not by Signal....





### kaushalyap on 10 Apr 2020

Hmm, but is not flatpak packages built from source by Flathub?  
Anyway I tried flatpak version but it seems buggy.(was not able to import conversations and contacts properly)

May be its time for us to move to alternative like [Session](https://github.com/loki-project/session-desktop/), which is a signal fork and it have `.AppImage` which can be used for Fedora

I am not affiliated with Session in anyway, since both this issue and [rpm issue](https://github.com/signalapp/Signal-Desktop/issues/1630) from Nov 2017, it seems supporting things on Fedora seems not a priority to them.

> Session Desktop - Onion routing based messenger

-   No footprints - Onion routing based
-   No phone numbers needed -
-   Censorship resistant - decentralized (resistant to [EARN IT act in US](https://www.wired.com/story/earn-it-act-sneak-attack-on-encryption/))





### ykne on 11 Apr 2020

For Fedora users It is reasonably trivial to build rpm from source or use one from luminoso copr, see my note from 2018 above. But [@kaushalyap](https://github.com/kaushalyap) is right, if Signal continues neglecting Fedora users then Fedora users will neglect Signal...





### rmader on 11 Apr 2020

It should maybe be noted that Firefox [just became](https://flathub.org/apps/details/org.mozilla.firefox) the first major project to ship official builds via flathup and [Martin Stránský of Fedora said](https://bugzilla.mozilla.org/show_bug.cgi?id=1441922#c68) they are looking at using that builds directly for Fedora Silverblue. So while it would be awesome to simply have a repo at signal.org, it might be worth to have an eye on how they solve that issue.





### lakano on 11 Apr 2020

> For Fedora users It is reasonably trivial to build rpm from source or use one from luminoso copr, see my note from 2018 above.

It's may be trivial when you do it just one time, but this also mean we need to watch Signal repository to get new versions / security fix, and rebuild each time, it's a big waste of time.

When we read / follow the blog of Open Whisper System / Moxie, they have solved so many attacks vectors in Signal, it's impressive, but they don't understand that they keep an MITM attack if they don't build / do not host flatpak themselves...





### janvlug on 11 Apr 2020

> Anyway I tried flatpak version but it seems buggy.(was not able to import conversations and contacts properly)

I've been using the Flathub Signal flatpak on Fedora since it exists without significant issues. But I agree that I would prefer a Signal authorized rpm or flatpak.



### compuguy on 12 Apr 2020

> > For Fedora users It is reasonably trivial to build rpm from source or use one from luminoso copr, see my note from 2018 above.
> 
> It's may be trivial when you do it just one time, but this also mean we need to watch Signal repository to get new versions / security fix, and rebuild each time, it's a big waste of time.
> 
> When we read / follow the blog of Open Whisper System / Moxie, they have solved so many attacks vectors in Signal, it's impressive, but they don't understand that they keep an MITM attack if they don't build / do not host flatpak themselves...

This isn't going to work in the long term for anyone who needs a RPM package for signal (Fedora, CentOS, and RHEL).



### RobDean69 on 16 Apr 2020

I'll jump on this one as well. I'm an OpenSUSE tumbleweed user as well as a Signal user.  
The need for either an official .RPM or (in my opinion, preferably) an official Flatpak build seems like a no brainer to me. Want people to use your messaging service? Provide clients that can run on as many different platforms as possible. Want to limit your user base? Provide clients that can be easily installed only on selected platforms. Given there is an unofficial Flatpak build it seems to me there shouldn't be too many problems providing an official one?



 [Cerberus0](https://github.com/Cerberus0) mentioned this issue [on 7 Sep 2020](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-692412447)

[No camera detected for video calls (on Pop OS / Ubuntu 20.04 ) #4490](https://github.com/signalapp/Signal-Desktop/issues/4490)

 Closed

1 task



### cmpunches on 3 Oct 2020

Please close this request, and then re-open issue [#1630](https://github.com/signalapp/Signal-Desktop/issues/1630)

this is not a best practice





### compuguy on 10 Oct 2020

[@cmpunches](https://github.com/cmpunches) The above issue you mentioned is still open. ⬆





### cardassian-tailor on 5 Nov 2020

> @justinkterry Realistically, with limited resources—two people full-time on desktop app—there are other features that have higher priority and officially supporting more platforms might also introduce extra support needs which we might not be able to meet to people’s satisfaction with the current team size.
> 
> We’d appreciate community help on improving the documentation on how to install the app from source for people on non-Debian systems in the meantime. Hopefully, as we grow our team, we can make the app more easily available on more platforms.

I'm just going to continue to mention this in passing because I feel its extremely important.

The time to **monetize** signal was three years ago. We as a community need to button up how we monetize and pay for the extremely necessary development, support, and research. There is no reason why we cant structure the product/service to meet the foundations current goals while offering a service at a reasonable cost.

Just keeping the lights on, servers running, salary's for devs ... all this costs money and the sooner we figure that out the quicker we can look to expand the team and have a product that continues to operate in the space competitively .. and with adequate developers.

Right now - we aren't doing that.

Having these expenditures paid for .... without the need to take large donations from a small group of people ... is detrimental to the longevity of the product/service. One of the best ways we ensure the product has staying power, remains community focused is by having its revenue streams be directly tied to its users and not external sources.





### kaimast on 16 May 2021

There's also a major issue with the unofficial Flatpak builds. It introduces another potential attack vector when distributing the binary, as a third-party is involved in converting your releases to a flatpak package.

In fact, there is a discussion [here](https://github.com/flathub/org.signal.Signal/issues/171) about the trustworthiness of the current Flatpak build.





###  **[stale] bot** on 23 Sep 2021

This issue has been automatically marked as stale because it has not had recent activity. It will be closed if no further activity occurs. Thank you for your contributions.

 [stale](https://github.com/apps/stale) bot added the [stale](https://github.com/signalapp/Signal-Desktop/labels/stale) label [on 23 Sep 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#event-5347717853)



### rmader on 23 Sep 2021

Bump to keep the feature request open - Flatpak becomes ever more popular on non-Ubuntu Linux desktops.



 [stale](https://github.com/apps/stale) bot removed the [stale](https://github.com/signalapp/Signal-Desktop/labels/stale) label [on 23 Sep 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#event-5348067697)

 [josh-signal](https://github.com/josh-signal) mentioned this issue [on 30 Sep 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-1011501726)

[Please discuss feature requests in community forums #5541](https://github.com/signalapp/Signal-Desktop/issues/5541)

 Closed

 [automated-signal](https://github.com/automated-signal) closed this as [completed](https://github.com/signalapp/Signal-Desktop/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 30 Sep 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#event-5383856530)



### heyakyra on 30 Sep 2021

no close plz



 [garrett](https://github.com/garrett) mentioned this issue [on 30 Sep 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-860542296)

[Electron runs with sandbox disabled #5195](https://github.com/signalapp/Signal-Desktop/issues/5195)

 Open



### garrett on 30 Sep 2021

Oddly, the AppImage ([#1758](https://github.com/signalapp/Signal-Desktop/issues/1758)) request is still open, even though this one has been closed. Unlike Flatpak, AppImage is inherently insecure, doesn't integrate with a desktop, doesn't have checksumming for verification, and doesn't get updates. (Flatpak has all of these features and more.)

And since there's currently no sandbox at all for the Signal app ([#5195](https://github.com/signalapp/Signal-Desktop/issues/5195)), it would be wise to use _some kind of sandbox_, which Flatpak does also provide.

Additionally, a native Flatpak build would handle large updates a lot better, sending only the diffs instead of everything, so it'd solve [#5365](https://github.com/signalapp/Signal-Desktop/issues/5365) too.

And, most importantly, the Linux version of Signal _isn't officially available for Linux_ — it's only officially made _for Debian as a .deb_ file. Having a Flatpak build would mean officially supporting all variants of Linux desktops. And as Flatpak uses common runtimes, it ensures that it has everything it needs to run without having to worry about dependencies or various package sets across distributions.

Also, I should point out that there's an unofficial Flatpak @ [https://github.com/flathub/org.signal.Signal](https://github.com/flathub/org.signal.Signal), which repackages the official .deb package. I'm sure the people who have been working on that would be overjoyed to help Signal make an official package available. (This has happened for Firefox, for example, which even hosts their official Firefox Flatpak build on Flathub.)

Basically: Making an official Flatpak package would solve a ton of open issues, make Signal much more secure, and make almost everyone on Linux much happier.





### shvchk on 1 Oct 2021

According to [#5541](https://github.com/signalapp/Signal-Desktop/issues/5541) we should discuss feature requests in community forums. There is a discussion on [Packaging for non-APT Linux distributions](https://community.signalusers.org/t/signal-desktop-for-non-apt-linux-distributions/1356).

 [vchernin](https://github.com/vchernin) mentioned this issue [on 10 Nov 2021](https://github.com/signalapp/Signal-Desktop/issues/1639#ref-issue-946703455)

[Replace deb package with source build flathub/org.signal.Signal#208](https://github.com/flathub/org.signal.Signal/issues/208) Open


