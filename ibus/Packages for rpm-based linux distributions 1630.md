---
layout: post
tags:
- electron
- fcitx
- snapd
title: "Packages for rpm-based linux distributions 1630"

---

[signalapp](https://github.com/signalapp)/[Signal-Desktop](https://github.com/signalapp/Signal-Desktop)Public
[Packages for rpm-based linux distributions #1630](https://github.com/signalapp/Signal-Desktop/issues/1630) Closed

1 task done

[nikwen](https://github.com/nikwen) opened this issue on 1 Nov 2017 · 171 comments




### nikwen on 1 Nov 2017

-    I have searched open and closed issues for duplicates

---

Currently, Signal-Desktop is only available for Debian-based distros with the APT package manager. Please provide pre-built packages for rpm-based distributions such as Fedora or RHEL.





### janvlug on 1 Nov 2017

It would also be good to warn before the migration that there are only .deb packages. I did the migration to learn only after that the Chrome Desktop is disabled that there are is no RPM installer available.





### kees-closed on 1 Nov 2017

Flatpak would also do the trick, no need for deb or rpm packaging.



 [exploide](https://github.com/exploide) mentioned this issue [on 1 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1630#ref-issue-255461187)

[Electron - No Stretch Repo #1431](https://github.com/signalapp/Signal-Desktop/issues/1431)

 Closed

1 task

 [janvlug](https://github.com/janvlug) mentioned this issue [on 1 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1630#ref-issue-270257950)

[Warn non-APT based Linux distribution users before migrating #1643](https://github.com/signalapp/Signal-Desktop/issues/1643)

 Closed



### euneuber on 2 Nov 2017

Please add openSuse to the list of supported Linux distributions!





### zimmski on 2 Nov 2017

Please use the open build service [http://openbuildservice.org/](http://openbuildservice.org/) to build packages. I do not know if the public instance [https://build.opensuse.org](https://build.opensuse.org/) enabled building packages for all major distributions, but the service application itself has the functionality [https://en.opensuse.org/openSUSE:Build_Service_cross_distribution_howto](https://en.opensuse.org/openSUSE:Build_Service_cross_distribution_howto), [https://en.opensuse.org/openSUSE:Build_Service_Debian_builds](https://en.opensuse.org/openSUSE:Build_Service_Debian_builds). I am sure that the build service guys will support you with the configuration of your projects.





###  sebix on 2 Nov 2017

On 11/02/2017 10:52 AM, Markus Zimmermann wrote: I do not know if the public instance [[https://build.opensuse.org](https://build.opensuse.org/)] enabled building packages for all major distributions,

Yes, it does. I can help with OBS for deb- and rpm packages.





### Author nikwen on 2 Nov 2017

For everyone in need of a transitional solution: Check out [#1627](https://github.com/signalapp/Signal-Desktop/pull/1627).



### diazbastian on 3 Nov 2017

I think it's [#1639](https://github.com/signalapp/Signal-Desktop/issues/1639) is a better solution. Besides being a more "universal" option is the path where fedora is headed anyway.



 [scottnonnenberg](https://github.com/scottnonnenberg) added the [Feature Request](https://github.com/signalapp/Signal-Desktop/labels/Feature%20Request) label [on 3 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1630#event-1324041321)

 [jnaulty](https://github.com/jnaulty) mentioned this issue [on 23 Nov 2017](https://github.com/signalapp/Signal-Desktop/issues/1630#ref-pullrequest-270139852)

[Add rpm package creation to package.json scripts #1627](https://github.com/signalapp/Signal-Desktop/pull/1627)

 Closed

9 tasks



### cryptomilk on 5 Jan 2018

I don't see why [#1639](https://github.com/signalapp/Signal-Desktop/issues/1639) is a better solution. Especially from a security point of view ...





### SISheogorath on 21 Jan 2018

There is one complete deal breaker for [#1639](https://github.com/signalapp/Signal-Desktop/issues/1639): Missing ibus support on Flatpaks.





### exploide on 22 Jan 2018

> There is one complete deal breaker for [#1639](https://github.com/signalapp/Signal-Desktop/issues/1639): Missing ibus support on Flatpaks.

I don't know what exactly you miss, but according to [flatpak/flatpak#675](https://github.com/flatpak/flatpak/issues/675), flatpak does support ibus now.





### wrender on 23 Jan 2018

An RPM would be nice. I think the idea of these other package types are good, but they need to be built into base linux OS's by default before they are actually useful.





### wrender on 4 Feb 2018

So does anyone have a way of building an rpm yet?



### rriemann on 4 Feb 2018

There is an rpm for suse. Note that it is a user contribution and may not be trustworthy.

[https://software.opensuse.org/package/signal-desktop](https://software.opensuse.org/package/signal-desktop)

spec file: [https://build.opensuse.org/package/view_file/home:ithod:signal/signal-desktop/signal-desktop.spec](https://build.opensuse.org/package/view_file/home:ithod:signal/signal-desktop/signal-desktop.spec)





### cryptomilk on 4 Feb 2018

I dicussed that with some friend yesterday. Packaging Signal is probably a huge task. However it could be done in the build service on a collaborative effort. The build services can also grab signed git tags and verify the signature. So you can rely on the integrity of the source code that way.

However, the biggest effort would be to package all the nodejs modules and other dependencies not available yet. They are not available and that would need to be done first. The spec [@rriemann](https://github.com/rriemann) mentions is a bad packaging example. It includes just everything instead of packaging the dependencies.

Is 'npm install' actually checking signatures, is it trustworthy?



### rriemann on 4 Feb 2018

On the security issues introduced by npm dependencies: [https://hackernoon.com/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5](https://hackernoon.com/im-harvesting-credit-card-numbers-and-passwords-from-your-site-here-s-how-9a8cb347c5b5)

I imagine that Signal is carefully reviewing all packages and then all updates of them and their dependencies and guess that like ruby packages, npm package versions are locked.

Opensuse has some tool to package npms: [https://github.com/marguerite/nodejs-packaging](https://github.com/marguerite/nodejs-packaging)

Please read the ideas in the repo readme on some general remarks on node module packaging. In essence: packaging dependencies of node modules and their node modules to a global namespace does not make sense from their point of view.



### steelstring94 on 8 Feb 2018

Just here to agree that an RPM version would be much appreciated, as a Fedora user.





### diazbastian on 9 Feb 2018

I don't know if the way is to duplicate the efforts in maintaining different options, but as I mentioned before, the [path of fedora is towards the use of flatpak](http://www.projectatomic.io/blog/2018/02/fedora-atomic-workstation/), on the other hand this alternative works in a wide range of other distributions.

[@cryptomilk](https://github.com/cryptomilk) Could you be more specific?





### cryptomilk on 9 Feb 2018

Distributions and package managers have been invented so that packages stay up to date and share resources.

flatpak doesn't share resources with my system it loads another Linux system into RAM (libc, xorg, gtk, openssl etc.) I don't really know if this system keeps up with security updates. It is a waste of resources just running a simple messenger.





### exploide on 9 Feb 2018

While I totally agree that package maintainers of classical package repositories are important and do a good job, there are also reasonable usecases for Flatpak.

Most importantly to bundle proprietary software. Not that I like this kind of software but sometimes some people need it and classically it comes with an obscure `install.sh` and does horrible thing to your system. Flatpak is really a better bet there.

Additionally, even for FLOSS software, if a program is hard to package and the responsibility remains at the software authors, not the distribution's package maintainers, then the use of Flatpak can decrease their effort needed to serve to more Linux users. This is the case here. You say "simple messenger" but this is not what Signal Desktop is. It is based on Electron, and Fedora packagers didn't manage to include the closely associated Atom editor in the repositories, yet. The Electron/Chromium/Node stuff is simply too much effort. And here for OWS it is simpler to provide a distribution independent Flatpak than to manage multiple distribution specific build systems.





### cryptomilk on 9 Feb 2018

I've just updated flatpak and Signal doesn't work anymore:

```
flatpak run org.signal.Signal
Gtk-Message: Failed to load module "canberra-gtk-module"
No protocol specified
```

My distro is better maintained ...



### diazbastian on 9 Feb 2018

[@cryptomilk](https://github.com/cryptomilk) I thought you had technical arguments against it, but I see that simply "the new model is not to your liking". With Flatpak, applications can be distributed directly from their providers, so in theory security updates will be obtained more quickly by its users than through an intermediary (maintainer). It also offers isolation and its runtime system allows resource sharing and deduplication, etc.

My Signal installation works properly for me, maybe you should update the runtime.





### sevagh on 9 Feb 2018

Why does the path to Fedora **necessarily** involve endless circular discussions about Flatpak, while Debian-based distros are sitting happy with their debs? Whatever arguments you provide for not providing RPMs should be applied to block the release of debs as well.





### rriemann on 9 Feb 2018

Can we please have the flatpak discussion somewhere else? I use opensuse and have never installed an app like this. However, I know how to deal with rpms. That's a widely used standard and if it is imperfect, than maybe the distros should invest into flatpak before individual applications do so.

My observations:

-   If I cannot trust my distro and their build system, I cannot use my computer. I would need to assume keyloggers that take my data even before it is encrypted on the app level by e.g. Signal.
-   I have to trust Signal.
-   Both my distro and Signal are opensource which makes trust easier.
-   So after all these assumptions, I do not understand the problem to also trust opensuse to checkout the code from signal, check the signatures and build using a transparent tool chain a trustworthy rpm.
-   Same argument goes for Fedora.

I agree that packaging of npm deps is difficult, but I do not see how flatpak can provide an advantage here.





### ErikLentz on 21 Feb 2018

If they're going to provide a deb, they need to provide an rpm.





### kees-closed on 27 Feb 2018

I just noticed that in GNOME Software (on Fedora) a Signal Flatpak is available via FlatHub. It installed and works like a charm, it's the latest Signal Desktop edition. I consider the issue closed.

[![screenshot from 2018-02-27 10-56-23](https://user-images.githubusercontent.com/6113099/36723397-6c385384-1bb0-11e8-889a-901ba1c139c1.png)](https://user-images.githubusercontent.com/6113099/36723397-6c385384-1bb0-11e8-889a-901ba1c139c1.png)  
[![screenshot from 2018-02-27 10-56-30](https://user-images.githubusercontent.com/6113099/36723398-6c5d46c6-1bb0-11e8-94eb-db32968193f1.png)](https://user-images.githubusercontent.com/6113099/36723398-6c5d46c6-1bb0-11e8-94eb-db32968193f1.png)  
[![screenshot from 2018-02-27 10-58-18](https://user-images.githubusercontent.com/6113099/36723399-6c81d9b4-1bb0-11e8-9808-ea6d66ef0106.png)](https://user-images.githubusercontent.com/6113099/36723399-6c81d9b4-1bb0-11e8-9808-ea6d66ef0106.png)





### SISheogorath on 27 Feb 2018

@AquaL1te no, it's not. Neither does this flatpak support ibus, nor is it build against Fedora libraries.

Apart from this there is a noticeable difference when you install an application as flatpak or install it as a native application. It starts at CLI integration, differences in the security setup, cache-ability, which is actually more complicated with Flatpaks (apart from the point that a lot of people have RPM caches in place, while no flatpak cache right now), …

So it's far from being solved, even when the flatpak exists and is for a lot of people a valid alternative. (yes, I use it myself for month, but for all communication that needs some more special letters, I have to use my phone)



126 hidden itemsLoad more…



### melonmouse on 25 Feb

cryptomilk, thanks for the updates and the great work! I tried to install signal-desktop from the fedora rawhide repo to help testing, but it is not there yet. Any idea when the package could appear?



### cryptomilk on 25 Feb

I can see it:

[https://download.opensuse.org/repositories/network:/im:/signal/Fedora_Rawhide/x86_64/signal-desktop-5.33.0-4.1.x86_64.rpm](https://download.opensuse.org/repositories/network:/im:/signal/Fedora_Rawhide/x86_64/signal-desktop-5.33.0-4.1.x86_64.rpm)



### piotr-dobrogost on 25 Feb

[@cryptomilk](https://github.com/cryptomilk)  
I guess opensuse.org is not the official source of Fedora's packages.  
Is Fedora using some services provided at opensuse.org?  
Is there any process in motion to have this package available through official Fedora repository?





### cryptomilk on 25 Feb

If you want to maintain the packages in Fedora, that would be great :-)



### aklapper on 25 Feb

As written before, [https://github.com/luminoso/fedora-copr-signal-desktop/](https://github.com/luminoso/fedora-copr-signal-desktop/) exists and maybe folks could sync with luminoso to avoid duplicating efforts for no good reasons.



### cryptomilk on 25 Feb

> As written before, [https://github.com/luminoso/fedora-copr-signal-desktop/](https://github.com/luminoso/fedora-copr-signal-desktop/) exists and maybe folks could sync with luminoso to avoid duplicating efforts for no good reasons.

This package is not allowed to build in COPR for legal reasons.



### dreua on 25 Feb

In copr? Can you cite a source for that? I could imagine it's a problem in Fedora Main repsoitories but Rpm fusion and copr should be way more relaxed.





### cryptomilk on 25 Feb

[https://docs.pagure.org/copr.copr/user_documentation.html#what-i-can-build-in-copr](https://docs.pagure.org/copr.copr/user_documentation.html#what-i-can-build-in-copr)



### cryptomilk on 25 Feb

Btw. if you go to:

[https://copr.fedorainfracloud.org/coprs/luminoso/Signal-Desktop/](https://copr.fedorainfracloud.org/coprs/luminoso/Signal-Desktop/)

Installation Instructions

⚠️ Before proceeding

-   If you're on OpenSUSE, please migrate to [obs network:im:signal](https://build.opensuse.org/project/show/network:im:signal) instead of using this Copr
-   Consider using [Signal's Flathub release](https://flathub.org/apps/details/org.signal.Signal)





### cryptomilk on 25 Feb

I think 1. is true for Fedora as soon as we have Fedora 36 beta out. Another option is that people start maintaining the required packages in Fedora. I already did all the work getting ffmpeg into Fedora.

[https://src.fedoraproject.org/rpms/ffmpeg/](https://src.fedoraproject.org/rpms/ffmpeg/)  
[https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/Q4ZYYBTEP2ODUI6KV77JUOB4QZG2BTPD/](https://lists.fedoraproject.org/archives/list/devel@lists.fedoraproject.org/thread/Q4ZYYBTEP2ODUI6KV77JUOB4QZG2BTPD/)





### dreua on 25 Feb

I stand corrected about what's allowed to build in copr, thanks. In practice it seems that those rules are not always enforced 🤔



### Sorinaki on 27 Feb

Even an installable .tar would be a solution - heck, even providing the .deb that can be alien-ated is better than saying "add this Ubuntu repo and gtfo"





### BarbossHack on 28 Mar

If you want to build your own Signal RPM package, I made a Dockerfile for that

[https://github.com/BarbossHack/Signal-Desktop-Fedora](https://github.com/BarbossHack/Signal-Desktop-Fedora)



### maitra on 28 Mar

As long as there is no official Fedora version, there is always the copr one available. To install, look at:

[https://copr.fedorainfracloud.org/coprs/luminoso/Signal-Desktop/](https://github.com/signalapp/Signal-Desktop/issues/url)

But it really would be awesome to have official support on Fedora.



### BarbossHack on 28 Mar

Yes indeed, but as long as there is no official Fedora version (from [@signalapp](https://github.com/signalapp)) , I prefer to build my own RPM package, for security reasons



### cryptomilk on 28 Mar

> As long as there is no official Fedora version, there is always the copr one available. To install, look at:
> 
> [https://copr.fedorainfracloud.org/coprs/luminoso/Signal-Desktop/](https://github.com/signalapp/Signal-Desktop/issues/url)
> 
> But it really would be awesome to have official support on Fedora.

As a Fedora proven packager, I'm more trustworthy if I package Signal in Fedora than outside?



### dreua on 1 Apr

This is about the trustworthyness of a package, not you as a person. You can't expect people to memorize the list of proven packagers, they shouldn't even need to know what that title means. (Congratulations, by the way.) A package in the Fedora repositories or rpmfusion will always be more trustworthy than copr or other sources and I believe this is a good thing.





### cryptomilk on 1 Apr

I would argue that nodejs apps per default are not trustworthy. You should be careful how you run them. I have signal-desktop still secured with apparmor.

Here is a `yarn audit` of Signal-Desktop 5.37.0:

```
207 vulnerabilities found - Packages audited: 2842
Severity: 19 Low | 80 Moderate | 85 High | 23 Critical
```



### piotr-dobrogost on 3 Apr

[@cryptomilk](https://github.com/cryptomilk)  
At [https://build.opensuse.org/package/show/network:im:signal/signal-desktop](https://build.opensuse.org/package/show/network:im:signal/signal-desktop) there is build for Fedora Rawhide.  
Could please add build for current version of Fedora (35) and if not describe steps needed to get one?



### cryptomilk on 3 Apr

That's not possible as Fedora 35 doesn't provide ffmpeg. I've just recently added ffmpeg to Fedora, it will be available with Fedora 36. You have to wait till f36 is out ...





### BarbossHack on 3 Apr

> describe steps needed to get one

You can build your own Signal-Desktop rpm for Fedora 35 with this project [@piotr-dobrogost](https://github.com/piotr-dobrogost)[https://github.com/BarbossHack/Signal-Desktop-Fedora](https://github.com/BarbossHack/Signal-Desktop-Fedora)

That's what I'm using, and no problem so far on Fedora 35





### leukimi on 29 Apr

[@cryptomilk](https://github.com/cryptomilk)  
[@ALL](https://github.com/ALL)

> I have signal-desktop still secured with apparmor.

It would be very very interesting to know how exactly you secured it with AppArmour. Would you mind sharing it? Thank you in advance.



### cryptomilk on 29 Apr

You can find the apparmor files here:  
[https://build.opensuse.org/package/show/home:darix:apps/apparmor-profiles-nordisch](https://build.opensuse.org/package/show/home:darix:apps/apparmor-profiles-nordisch)



### leukimi on 29 Apr

The Docker build by BarbossHack works great.  
[OpenSUSE repository](https://build.opensuse.org/package/show/home:ithod:signal/signal-desktop-bin) does not seem to build RPM for Fedora 35 after version 33.

The following recipe could serve as a plan B.

---

# An approach to building `signal-desktop` branch `5.49.x`on Fedora 35

In case someone would benefit from it, the following approach did prove successful on a fresh Fedora 35 install.

All you need is a "bit of patience", since it can take around ten minutes before you are a happy Fedora (signal-desktop) user.

Just in case someone doesn't know what the following code is doing, it is a bash script that will help you to build and install `signal-desktop` branch `$LATEST_BRANCH`. You can adapt the script to your needs and copy the bash script to a file which you can give the file name:

`build_signal-desktop.sh`

Make this file `build_signal-desktop.sh` executable in some way. This is how to do it using a terminal:

`chmod +x build_signal-desktop.sh`

Run:

`./build_signal-desktop.sh`

Watch the magic take place.

---

## 1. First step after the fresh install is to update Fedora 35.

~~~
# Update the newly installed Fedora 35.
sudo dnf update

# Reboot Fedora 35 to be on the safe side, since you probably installed a new kernel.
reboot
~~~

## 2. Second step is to install dependencies.

```shell
Fedora 35 already has most of the tools you will need. You can type this in the terminal to check:

echo "--------------------------------"
gcc --version
echo "--------------------------------"
git --version
echo "--------------------------------"
curl --version
echo "--------------------------------"
```

### `nvm`

`nvm` does not come preinstalled on Fedora 35. `nvm` is the Node Version Manager used to manage multiple Node.js versions on a single system. `nvm` can install any version of Node.js. `nvm` tool installs on your system by typing this into terminal, so we will include it in the build script further down to make sure it gets installed automatically if it is not present:

```shell
curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
source ~/.bashrc
```

## 3. Putting it all together in a script `build_signal-desktop.sh`

```shell
#!/bin/bash
##
## Script: build_signal-desktop.sh
##
## Description:
## Downloads the selected branch to $HOME/src/Signal-Desktop
## from git source.
## Compiles signal-desktop.
## Creates an RPM package.
##

VOICE_SOUND=true
REQUIRE_USER_CHECK=false
INSTALL_RPM=false
RUN_APPIMAGE=false

# Fedora 35 dependencies
if hash nvm 2>/dev/null; then
	nvm --version
else
	curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
	source ~/.bashrc 
fi
if hash git 2>/dev/null; then
	git --version
else
	sudo dnf -y install git 
fi
if hash git-lfs 2>/dev/null; then
	git-lfs --version
else
	sudo dnf -y install git-lfs 
fi

# Edit this to select the branch you want to compile
#
# Previous branches:
# "5.48.x"
# ...
# "5.1.x"
LATEST_BRANCH="5.49.x"
echo
echo "You have selected to use branch:"
echo \"$LATEST_BRANCH\"
echo
echo LATEST_BRANCH=$LATEST_BRANCH
echo
sleep 2

# Building Signal Desktop (5.v.x) version on Fedora 34
# Edit the branch names when new releases are made available

mkdir -p $HOME/src
cd $HOME/src
echo "Changed directory to:"
pwd
echo

# Check latest branch
if [ -d Signal-Desktop ]; then
	# Check if you have the latest branch
	cd Signal-Desktop
	echo "Changed directory to:"
	pwd
	echo
	echo "Checking the list of branches that are available remotely:"
	
	git ls-remote | grep heads
	sleep 3
	echo
	echo "Your current (local) branches are:"
	git branch -a

	echo
	echo "If you want to delete a branch (I will do it for you):"
	echo "   git branch -D 5.yy.x"
	echo "   git branch -D 5.46.x"
	echo "   git branch -D 5.47.x"
	echo "   git branch -D 5.48.x"
	echo
	echo

	git branch -D 5.1.x
	git branch -D 5.2.x
	git branch -D 5.3.x
	git branch -D 5.4.x
	git branch -D 5.5.x
	git branch -D 5.6.x
	git branch -D 5.7.x
	git branch -D 5.8.x
	git branch -D 5.9.x
	git branch -D 5.10.x
	git branch -D 5.11.x
	git branch -D 5.12.x
	git branch -D 5.13.x
	git branch -D 5.14.x
	git branch -D 5.15.x
	git branch -D 5.16.x
	git branch -D 5.17.x
	git branch -D 5.18.x
	git branch -D 5.19.x
	git branch -D 5.20.x
	git branch -D 5.21.x
	git branch -D 5.22.x
	git branch -D 5.23.x 
	git branch -D 5.24.x
	git branch -D 5.25.x
	git branch -D 5.26.x
	git branch -D 5.27.x
	git branch -D 5.28.x
	git branch -D 5.29.x
	git branch -D 5.30.x
	git branch -D 5.31.x
	git branch -D 5.32.x
	git branch -D 5.33.x
	git branch -D 5.34.x
	git branch -D 5.35.x
	git branch -D 5.36.x
	git branch -D 5.37.x
	git branch -D 5.38.x
	git branch -D 5.39.x
	git branch -D 5.40.x
	git branch -D 5.41.x
	git branch -D 5.42.x
	git branch -D 5.43.x
	git branch -D 5.44.x
	git branch -D 5.45.x
	git branch -D 5.46.x
	git branch -D 5.47.x
	git branch -D 5.48.x

	echo
	echo "If the latest branch you have"
	sleep 2
	echo "is not the latest branch on the remote,"
	sleep 2
	echo "consider hitting Ctrl + C to stop this script now."
	sleep 3
	echo
	echo "I will wait ten seconds before I start building."
	echo "..."
	sleep 10

	# Add signal-desktop branch $LATEST_BRANCH
	git fetch https://github.com/signalapp/Signal-Desktop.git $LATEST_BRANCH:$LATEST_BRANCH

	# If you get a complaint about that you have unsaved work:
	# error: Your local changes to the following files would be overwritten by checkout:
	#	package.json
	# Please commit your changes or stash them before you switch branches.
	# Aborting
	# Do the following:
	git stash
	# To delete all stashes in git, we need to run the git stash command followed by the clear option.
	git stash clear
	# Now it should work to switch to the new branch

	# Check out $LATEST_BRANCH
	git checkout $LATEST_BRANCH

	# Update $LATEST_BRANCH
	git fetch --all
	git pull --rebase
	# git rebase $LATEST_BRANCH
	git reset --hard $LATEST_BRANCH
	# Now you should be sure to have the latest files in $LATEST_BRANCH

	# Update existing branch
	# git fetch origin
	# git fetch main
	# git rebase
	# git reset --hard
	# git reset --hard origin
	# git reset --hard main

	# If you made any changes, git will ask you to stash them:
	# git stash
	# git stash clear
	# git pull --rebase
	# git reset --hard

	# Delete all files that are not version controled
	git clean -dfx
	# git submodule foreach --recursive "git clean -dfx"
	git branch -a
else
	# Download signal-desktop branch $LATEST_BRANCH
	echo
	echo "Downloading Signal-Desktop source code from git."
	echo "If you already have the software, you might see"
	echo "error messages. Don't worry, that's okay."
	sleep 5
	echo
	git clone -b $LATEST_BRANCH --single-branch https://github.com/signalapp/Signal-Desktop.git

	# Change directory
	cd Signal-Desktop
	echo "Changed directory to:"
	pwd
	echo
fi

# Remove prior packages
# rm -rf release

# Show which version of nvm is required:
echo
echo "Required nvm version:"
cat .nvmrc
sleep 1

# Source nvm
source $HOME/.nvm/nvm.sh

# nvm
# nvm does not come preinstalled on Fedora 35. nvm is the Node Version Manager used to manage multiple Node.js versions on a single system. nvm can install any version of Node.js. nvm tool installs on your system by typing this into terminal, so we will include it in the build script further down to make sure it gets installed automatically if it is not present:
#
# Installation of nvm:
# curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
# source ~/.bashrc

if hash nvm 2>/dev/null; then
	nvm --version
else
	curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
	source ~/.bashrc 
fi

# Sometimes nvm is not found, run the command in terminal in such case
# nvm install v18.1.0
# nvm install v16.13.2
# or generic
cat .nvmrc | nvm install 
# Sometimes nvm is not found, run the command in terminal in such case
# nvm use v18.1.0
# nvm use v16.13.2
# or generic
cat .nvmrc | nvm use

# Verify which nvm is used
nvm list
sleep 2

# Check that npm is available
type npm >/dev/null 2>&1 || { echo >&2 echo; echo "Run this script again to start building signal-desktop."; exit 0; }

# To confirm that you have npm installed you can run this command in your terminal:
echo
echo "Confirm that you have npm installed:"
echo

npm -v
sleep 1

# Update npm
echo
echo "Update npm:"
echo

npm install -g npm
npm update -g

# New step in build 2022:
git-lfs install

npm install --global yarn

# Edit package.json to add the options 'rpm' and 'AppImage'
python3 <<EOF
query = '''
	  "target": [
		"deb"
	  ],
'''
replacement = '''
	  "target": [
		"rpm",
		"AppImage"
	  ],
'''
data = None
with open("package.json",'r') as file:
	data = file.read()
	data = data.replace(query,replacement)
with open("package.json",'w') as file:
	file.write(data)
EOF

echo
echo "[OK] Source code downloaded."
echo "----------------------------"
echo
echo "Now you should have everything you need to build."
sleep 4
echo "The compilation will take a little while."
sleep 3
if hash espeak-ng 2>/dev/null; then
	# You have espeak-ng installed
	true
elif hash festival 2>/dev/null; then
	# You have festival installed
	true
else
	echo "If you want your computer to tell you with a sound when it is ready:"
	sleep 3
	echo "Install the packages:"
	sleep 2
	echo
	echo "+ espeak-ng"
	sleep 1
	echo "and/or"
	echo "+ festival festvox-slt-arctic-hts"
	sleep 2
	echo
	echo "You may have to remove:"
	echo "- festvox-clb-arctic-hts"
	sleep 3
	echo
	echo "Type this in the terminal if you wish:"
	echo
	echo "sudo dnf install espeak-ng festival festvox-slt-arctic-hts"
	echo "sudo dnf remove festvox-clb-arctic-hts"
	echo
	echo
	sleep 5
fi

echo
echo "The building of Signal-Desktop starts now."
sleep 2

yarn install --frozen-lockfile

yarn grunt

# The following is not working although it is mentioned in the install instructions to execute this command.
yarn icon-gen

yarn generate

if $REQUIRE_USER_CHECK; then
	# Help user to decide what to build:
	zenity --width 400 --warning --title "Do you want AppImage?" --text "Dont forget to alter the 'package.json' file to tell yarn to build deb, rpm, snap, AppImage in the \"linux\" section.\n\n \"target\": [\n        \"deb\",\n        \"rpm\",\n        \"snap\",\n        \"AppImage\"\n      ],"

	# The line where user has to edit:
	gedit package.json +362

	# Help user to decide what to build:
	zenity --width 400 --info --title "Do you want AppImage?" --text "Click OK when you are done editing package.json\n\n \"target\": [\n        \"deb\",\n        \"rpm\",\n        \"snap\",\n        \"AppImage\"\n      ],"
fi

yarn build-release

# Install rpm

if $INSTALL_RPM; then
	sudo dnf install release/signal-desktop*.rpm
else
	xdg-open release
fi

# Make AppImage executable

chmod +x release/*.AppImage

if $RUN_APPIMAGE; then
	# Run AppImage
	release/*.AppImage
else
	# Run linux-unpacked
	release/linux-unpacked/signal-desktop
fi
# Uninstall
# sudo dnf remove signal-desktop

# Tell compilation is done

if $VOICE_SOUND; then
	if hash espeak-ng 2>/dev/null; then
		espeak-ng -ven-us+f5 -s2 -s160 "Your signal desktop software package in format R P M is ready."
		espeak-ng -ven-us+f5 -s2 -s160 "This message! Will self destruct! In 5 seconds. ... five ... four ... threee ... two ... one ... BOOM."
	elif hash festival 2>/dev/null; then
		echo "Your signal desktop software package in format R P M is ready." | festival --tts
		echo "This message will self destruct in 5 seconds. ... five ... four ... threee ... two ... one ... BOOM."
	fi
fi
```

## Handle errors:

```shell
Clean yarn cache:  
`yarn cache clean`  
Delete node_modules:  
`rm -rf node_modules`
```

## Delete active nodejs:
```shell

nvm deactivate
nvm uninstall <TAB><TAB> 
nvm uninstall v16.13.2

Sometimes you may want to get rid of all nvm stuff and start new. `nvm` stores things here:  
`rm -rf ~./nvm`
```



### cryptomilk on 14 Jun

Now Fedora 36 package are available at:

[https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop)





### piotr-dobrogost on 14 Jun

> Now Fedora 36 package are available at:
> 
> [https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop)

[@cryptomilk](https://github.com/cryptomilk)  
Is there any way to send you money for your work on this?



### cryptomilk on 14 Jun

Donate the money to either [https://signal.org/donate/](https://signal.org/donate/) or [https://sfconservancy.org/donate/](https://sfconservancy.org/donate/) or both :-)



### maitra on 14 Jun

> Now Fedora 36 package are available at:
> 
> [https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop)

Thanks, wonderful! Thank you for your hard work on this.

Will this be in the official repo soon? There is also this package from the one in the copr by luminoso: [https://github.com/luminoso/fedora-copr-signal-desktop](https://github.com/luminoso/fedora-copr-signal-desktop) but it would be great to have an official package. Don't know if that is your plan.





### luminoso on 14 Jun

> > Now Fedora 36 package are available at:  
> > [https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop)
> 
> Thanks, wonderful! Thank you for your hard work on this.
> 
> Will this be in the official repo soon? There is also this package from the one in the copr by luminoso: [https://github.com/luminoso/fedora-copr-signal-desktop](https://github.com/luminoso/fedora-copr-signal-desktop) but it would be great to have an official package. Don't know if that is your plan.

[network:im:signal](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop) is way superior to mine. My understanding is that it builds signal and its dependencies offline and without pre-built binaries shipped along with the repo.

On the other hand, my copr repo is way simpler. Just grabs signal-desktop repo and builds it. It requires internet connection and uses pre-built binaries from it. Wishful thinking that those binaries present in this signal-desktop repository are trustable 🤷





### cryptomilk on 14 Jun

[network:im:signal](https://software.opensuse.org/download.html?project=network:im:signal&package=signal-desktop) builds everything from sources and uses system components like ffmpeg, system fonts etc. Also it has a nodejs-electron package.

To bring everything into Fedora is quite some work, the first step would be to bring nodejs-electron into Fedora.
