---
layout: post
tags:
- flatpak
- session-bus
title: "Add missing socket to flatpak manifest to allow app to start NO.135"

---

# Add missing socket to flatpak manifest to allow app to start NO.135

[elementary](https://github.com/elementary)/[docs](https://github.com/elementary/docs)


Add missing socket to flatpak manifest to allow app to start #135  Closed
https://github.com/elementary/docs/pull/135


[iancleary](https://github.com/iancleary) wants to merge 1 commit into [elementary:master](https://github.com/elementary/docs "elementary/docs:master") from [iancleary:add-missing-flatpak-socket](https://github.com/iancleary/docs/tree/add-missing-flatpak-socket "iancleary/docs:add-missing-flatpak-socket")

+1 −0 

 [Conversation 4](https://github.com/elementary/docs/pull/135) [Commits 1](https://github.com/elementary/docs/pull/135/commits) [Checks 0](https://github.com/elementary/docs/pull/135/checks) [Files changed 1](https://github.com/elementary/docs/pull/135/files)

## Conversation

### iancleary on Sep 12, 2021

This PR adds a missing socket to address the issue in [#120](https://github.com/elementary/docs/issues/120) where the flatpak application installs (builds, installs, creates desktop entry), but doesn't start.

The missing line is `- '--socket=session-bus'`.



[Add missing socket per](https://github.com/elementary/docs/pull/135/commits/80578db45a97bf0ff40d76c3ff800fc99aecd35f) "Add missing socket per #120"[elementary#120](https://github.com/elementary/docs/issues/120)`

Verified

[80578db](https://github.com/elementary/docs/pull/135/commits/80578db45a97bf0ff40d76c3ff800fc99aecd35f)

 [iancleary](https://github.com/iancleary) changed the title ~~Add missing socket per #120~~ Add missing socket to flatpak manifest to allow app to start [on Sep 12, 2021](https://github.com/elementary/docs/pull/135#event-5288068542)



### Member davidmhewitt on Sep 12, 2021

[https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access](https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access)
[[/flatpak/Sandbox Permissions]]
According to the docs, this should be avoided so I think there's a different issue here.





### Author iancleary on Sep 12, 2021

> [https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access](https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access)
> 
> According to the docs, this should be avoided so I think there's a different issue here.

Understood, I agree my proposed change is in conflict with the [flatpak docs'](https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access) best practices.



### Author iancleary on Sep 14, 2021

I'm not sure why, but I've been able to build and run without issue in the absence of the additional socket.

I do remember encountering the issue in [#120](https://github.com/elementary/docs/issues/120) though, so I'm not sure if there is an issue as written in the docs that I've since gotten past.



### Author iancleary on Sep 14, 2021

As the proposed change goes against Flatpak's documentation, I'm closing my own pull request, but recommend we keep the original poster's issue open.

> > [https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access](https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access)  
> > According to the docs, this should be avoided so I think there's a different issue here.
> 
> Understood, I agree my proposed change is in conflict with the [flatpak docs'](https://docs.flatpak.org/en/latest/sandbox-permissions.html#d-bus-access) best practices.

 [iancleary](https://github.com/iancleary) closed this [on Sep 14, 2021](https://github.com/elementary/docs/pull/135#event-5296967158)

 [iancleary](https://github.com/iancleary) deleted the add-missing-flatpak-socket branch [14 months ago](https://github.com/elementary/docs/pull/135#event-5296967463)
