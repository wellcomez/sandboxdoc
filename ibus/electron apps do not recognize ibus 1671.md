---
layout: post
tags:
- flatpak
- gtk2
title: "electron apps do not recognize ibus 1671"

---

# electron apps do not recognize ibus 1671
[electron apps do not recognize ibus #1671](https://github.com/flatpak/flatpak/issues/1671) Closed

[J-J-Chiarella](https://github.com/J-J-Chiarella) opened this issue on 14 May 2018 · 12 comments

### J-J-Chiarella commented on 14 May 2018

#### Linux distribution and version

Ubuntu 18.04

#### Flatpak version

0.11.3 to .6

#### Description of the problem

Electron apps do not recognize ibus

#### Steps to reproduce

use ibus in an electron-using flatpak like Signal or Skype. Not recognized. Just uses bog standard US English layout.

others experienced this, too: [flathub/org.signal.Signal#17](Missing%20ibus%20support%2017.md) but this is due to how electron is used. <mark class="hltr-red">Rather than fixing every packaging of a flatpak or every implementation of electron, is there some way for flatpak the platform to ease the issue for electron apps?
</mark>

Just thought I'd bring it to everyone's attention. I don't see it on bug reports here. At these early stages the venn diagram of flatpak users, electron app users, and ibus users is probably small. I also don't expect Microsoft to fix it if it must be fixed on a per-flatpak basis.



### J-J-Chiarella commented on 14 May 2018

[#675](Unable%20to%20use%20iBus%20(input%20method)%20675.md)

i saw this but it seems separate to the electron issue

[alexlarsson](https://github.com/alexlarsson) [alexlarsson](https://github.com/alexlarsson) added the [packaging](https://github.com/flatpak/flatpak/labels/packaging) Issue related to packaging of a specific app/runtimelabel [on 14 Aug 2018](https://github.com/flatpak/flatpak/issues/1671#event-1787799139)


### juhp commented on 8 Nov 2018 • edited 

(Just to clarify 675 was the issue requesting the original ibus support in Flatpaks.)

[juhp](https://github.com/juhp) [juhp](https://github.com/juhp) mentioned this issue [on 8 Nov 2018](https://github.com/flatpak/flatpak/issues/1671#ref-issue-218283834)

[Unable to use iBus (input method) #675](Unable%20to%20use%20iBus%20(input%20method)%20675.md) Closed


### juhp commented on 8 Nov 2018• edited 

Atom, Slack and Skype are using org.electronjs.Electron2.BaseApp (gtk3)

vscode, RocketChat, Signal are using io.atom.electron.BaseApp (gtk2)

I suggest it would be good to get this fixed first in [https://github.com/flathub/org.electronjs.Electron2.BaseApp](https://github.com/flathub/org.electronjs.Electron2.BaseApp) if possible, so an issue should be filed there.

[juhp](https://github.com/juhp) mentioned this issue [on 8 Nov 2018](https://github.com/flatpak/flatpak/issues/1671#ref-issue-378579724)

[ibus seems not working in Electron flatpak apps flathub/org.electronjs.Electron2.BaseApp#5](https://github.com/flathub/org.electronjs.Electron2.BaseApp/issues/5)Closed

### juhp commented on 8 Nov 2018 • edited 

ibus seems to work in Discord (io.atom.electron.BaseApp (gtk2)) for me, though.


### jeffshee commented on 8 Nov 2018

> ibus seems to work in Discord (io.atom.electron.BaseApp (gtk2)) for me.

Hi, got directed to this issue, from [#675](Unable%20to%20use%20iBus%20(input%20method)%20675.md). Thanks.  
iBus doesn't seem working on my Discord though, is there any log files that I can provide?




### juhp commented on 9 Nov 2018 • edited 

I am using Fedora 29 and tested Japanese with ibus-kkc:

```shell
$ flatpak list -d
Ref                                                Origin  Active commit Latest commit Installed size Options       
com.discordapp.Discord/x86_64/stable               flathub 3ac6f4539e8d  -              39.9 MB       system,current
org.freedesktop.Platform/x86_64/18.08              flathub 95881b6e72a7  -             757.1 MB       system,runtime
$ rpm -q ibus flatpak
ibus-1.5.19-7.fc29.x86_64
flatpak-1.0.4-1.fc29.x86_64
```

Which ibus engine (IME or input method) are you using?

 [juhp](https://github.com/juhp) mentioned this issue [on 9 Nov 2018](https://github.com/flatpak/flatpak/issues/1671#ref-issue-372161172)

[IBus IME not working with electron app flathub/io.atom.electron.BaseApp#4](IBus%20IME%20not%20working%20with%20electron%20app%20io.atom.electron.BaseApp-4.md) `Closed`


### juhp commented on 9 Nov 2018

As noted in some other related issues sometimes 

`flatpak run --env=GTK_IM_MODULE=xim ...` works.




### jeffshee commented on 10 Nov 2018

[@juhp](https://github.com/juhp)  
HI, I'm using ibus-libpinyin for Chinese and ibus-mozc for Japanese. I have the same version of flatpak, ibus, discord, ... everything as you listed previously.  
And thanks for `flatpak run --env=GTK_IM_MODULE=xim ...`, it actually works (discord), nice workaround! Not quite sure with other apps tho haven't tested.

[SISheogorath](https://github.com/SISheogorath) [SISheogorath](https://github.com/SISheogorath) mentioned this issue [on 24 Nov 2018](https://github.com/flatpak/flatpak/issues/1671#ref-issue-300583386)

[Missing ibus support flathub/org.signal.Signal#17](Missing%20ibus%20support%2017.md) Closed


### fujiwarat commented on 8 Jan 2019

I evaluated Atom and visualstudio in [[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]]   .  
The pure electron application works fine with ibus as I commented the issue.  
<mark class="hltr-red">Note: they uses ibus in org.freedesktop.Sdk instead of org.freedesktop.Platform and need to port the ibus fix to the Flatpak.  </mark>
So I think this can be closed.

[matthiasclasen](https://github.com/matthiasclasen) [matthiasclasen](https://github.com/matthiasclasen) closed this as [completed](https://github.com/flatpak/flatpak/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 8 Jan 2019](https://github.com/flatpak/flatpak/issues/1671#event-2059287657)


### jeffshee commented on 2 Mar 2019

Sorry for asking but what is the status of this bug? Still waiting for the fix to be ported into Flatpak?

[fujiwarat](https://github.com/fujiwarat) mentioned this issue [on 4 Mar 2019](https://github.com/flatpak/flatpak/issues/1671#ref-issue-416614801)

[[Request to import ibus 1.5.20 in org.freedesktop.Platform freedesktop-sdk-image 132]]  Request to import ibus 1.5.20 in org.freedesktop.Platform flatpak/freedesktop-sdk-images#132 Closed


### fujiwara commented on 4 Mar 2019

I'm not sure the right category but I filed a request to update ibus in freedesktop-sdk-images now.




### fujiwarat commented on 4 Mar 2019

[https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues/681](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/issues/681)

[abnerlee](https://github.com/abnerlee) mentioned this issue [on 7 Nov 2019](https://github.com/flatpak/flatpak/issues/1671#ref-issue-515923681)

[Cant use ibus on typora typora/typora-issues#2976](Cant%20use%20ibus%20on%20typora%202976.md)