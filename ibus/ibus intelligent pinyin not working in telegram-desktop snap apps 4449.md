---
layout: post
tags:
- electron
- snapd
- ibus
title: "ibus intelligent pinyin not working in telegram-desktop snap apps 4449"

---

# ibus intelligent pinyin not working in telegram-desktop snap apps 4449
[Ibus intelligent pinyin not working in telegram-desktop snap apps](https://github.com/telegramdesktop/tdesktop/issues/4449#top)#4449

[wolfsonliu](https://github.com/wolfsonliu) opened this issue on 26 Feb 2018 · 22 comments 





### wolfsonliu on 26 Feb 2018

### Steps to reproduce

1.  install the snap telegram desktop
2.  open the telegram program
3.  switch the input method to ibus intelligent pinyin
4.  input words.

### Expected behaviour

Chinese character should be inputed.

### Actual behaviour

You could only input English character, and the Chinese input method was not working at all.

### Configuration

**Operating system:** Ubuntu 16.04

**Version of Telegram Desktop:** 1.2.6

**Used theme**:

**Logs**:



###  stek29 on 26 Feb 2018

> telegram-desktop snap apps

Does it work in official binary?



### Author wolfsonliu on 28 Feb 2018

I had tried the official one, and it worked. So, I guess it might be the problem of snap sandbox?



###  stek29 on 28 Feb 2018

I guess so, and the snap package isn't official, so you'd have to contact package maintainers.

 [stek29](https://github.com/stek29) closed this as [completed](https://github.com/telegramdesktop/tdesktop/issues?q=is%3Aissue+is%3Aclosed+archived%3Afalse+reason%3Acompleted) [on 28 Feb 2018](https://github.com/telegramdesktop/tdesktop/issues/4449#event-1496723336)

 [stek29](https://github.com/stek29) added the [invalid](https://github.com/telegramdesktop/tdesktop/labels/invalid) label [on 28 Feb 2018](https://github.com/telegramdesktop/tdesktop/issues/4449#event-1496723926)



### chenzhiwei on 4 Oct 2018

[@stek29](https://github.com/stek29) Hi, I think the telegram-desktop snap app is the official one now, the PR was merged in [#4505](https://github.com/telegramdesktop/tdesktop/pull/4505) and it is displayed in supported systems: [https://github.com/telegramdesktop/tdesktop#supported-systems](https://github.com/telegramdesktop/tdesktop#supported-systems)

[@3v1n0](https://github.com/3v1n0) Hi, I also mentioned this issue in [https://forum.snapcraft.io/t/call-for-testing-telegram-desktop/3954](https://forum.snapcraft.io/t/call-for-testing-telegram-desktop/3954), could you help take a look? I can use the ibus IME in the binary client.



### chenzhiwei on 20 Oct 2018

There is a workaround provided here: [https://forum.snapcraft.io/t/call-for-testing-telegram-desktop/3954/4](https://forum.snapcraft.io/t/call-for-testing-telegram-desktop/3954/4)

```
env QT_IM_MODULE=xim \
   QT_XKB_CONFIG_ROOT=/snap/telegram-desktop/current/usr/share/X11/xkb \
   QTCOMPOSE=/snap/telegram-desktop/current/usr/share/X11/locale \
   snap run telegram-desktop
```

cc [@fujiwarat](https://github.com/fujiwarat) , another ibus IME issue with snap app which can be solved by setting `xim` as the IM module.



### fujiwarat on 23 Oct 2018

[@chenzhiwei](https://github.com/chenzhiwei) The ibus qt module was recently implemented for qt-base 5.11.



### fujiwarat on 23 Oct 2018

[http://code.qt.io/cgit/qt/qtbase.git/commit/?id=2adc06f9406ec2f8c02d1619c73f384eb2947498](http://code.qt.io/cgit/qt/qtbase.git/commit/?id=2adc06f9406ec2f8c02d1619c73f384eb2947498)  
I confirmed qtbase 5.11.2 includes the fix.



### chenzhiwei on 24 Oct 2018

[@fujiwarat](https://github.com/fujiwarat) Thank you for letting me know this change.

Can I enable ibus by upgrading the qtbase to 5.11.2? or the snap/flatpak package should be re-built with qbase 5.11.2?

I tried to upgrade qtbase and a lot of dependencies to [5.11.2](http://ftp.cn.debian.org/debian/pool/main/q/qtbase-opensource-src/) but still can't use ibus in flatpck app.



### fujiwarat on 24 Oct 2018

[@chenzhiwei](https://github.com/chenzhiwei) Did you try Qt applications on org.kde.Platform//5.11 ?



### chenzhiwei on 24 Oct 2018

[@fujiwarat](https://github.com/fujiwarat) No, I tried Slack and it uses org.freedesktop.Platform//18.08.



### fujiwarat on 24 Oct 2018

[@chenzhiwei](https://github.com/chenzhiwei) I don't think that platform has the ibus qt module for flatpak.



### chenzhiwei on 24 Oct 2018

[@fujiwarat](https://github.com/fujiwarat) So you mean if I can update the runtime(`org.freedesktop.Platform//18.08`) to include the new qt module, then the Slack flatpak app can use ibus without any changes?



### fujiwarat on 24 Oct 2018

Yes in case that Slack is a QT application.



Member

###  **[john-preston]** on 24 Oct 2018

[@fujiwarat](https://github.com/fujiwarat) AFAIK Slack is an Electron app.



### fujiwarat on 24 Oct 2018

And then probably Electron needs to implement Flatpak IBus. [@john-preston](https://github.com/john-preston) Thank you.



### chenzhiwei on 24 Oct 2018

Already created an issue to Electron base App: [flathub/io.atom.electron.BaseApp#4](https://github.com/flathub/io.atom.electron.BaseApp/issues/4)

[@fujiwarat](https://github.com/fujiwarat) [@john-preston](https://github.com/john-preston) Thank you guys.

 [chenzhiwei](https://github.com/chenzhiwei) mentioned this issue [on 24 Oct 2018](https://github.com/telegramdesktop/tdesktop/issues/4449#ref-issue-373517602)

[[Please update the qtbase to 5.11.2 or later in flatpak runtimes 2273]]

 Closed



### Contributor 3v1n0 on 6 Nov 2018

[@chenzhiwei](https://github.com/chenzhiwei) having `QTCOMPOSE` and `QT_XKB_CONFIG_ROOT` fixes you the problem?  
As the first one is already set for everyone, what's not set is `QT_XKB_CONFIG_ROOT`, but is it needed here? Also `xim` is used by default if there's not anything else set.

For ibus, I've blacklisted it because it was breaking the XComposition otherwise in most of setups, you could actually force another IM system by using the environment variable `TELEGRAM_QT_IM_MODULE` that will be used as the `QT_IM_MODULE`whatever you set.

Problem is that inside the snap we might not have access to the proper component.

Logs from `snap run --strace telegram-desktop` or `dmesg` might help in understanding.



### juhp on 9 Nov 2018

Perhaps I should open another ticket but for the record I can't get ibus working with the Telegram flatpak either.



Member

###  **[john-preston]** on 9 Nov 2018

[@juhp](https://github.com/juhp) Does ibus work with the binary from desktop.telegram.org?



### juhp on 9 Nov 2018

Yes (though I could only get it to run via strace so far on recent Fedora).



### fujiwarat on 9 Jan 2019

I don't know why you refer Slack in Flatpak on this telegram-desktop issue but recently I fixed Atom using Electron in Flatpak and hope Slack is also fixed;  
[[Atom flatpak failed to trigger ibus, after adding and loading im-ibus.so 1991]]


If this issue in Flatpak, probably I'd like to know how to install with flatpak-builder.



###  **[github-actions] bot** on 7 Mar 2021

This issue has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.



