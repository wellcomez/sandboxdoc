---
title: "如何在 Linux 中使用 UnionFS 透明覆盖两个目录？ - 芒果文档"
layout: post
---


# 如何在 Linux 中使用 UnionFS 透明覆盖两个目录？ - 芒果文档
<a style="text-decoration:underline" href="https://www.imangodoc.com/188628.html">如何在 Linux 中使用 UnionFS 透明覆盖两个目录？ - 芒果文档</a><br>

要使用 UnionFS 透明地覆盖两个目录，必须遵循以下步骤：

![](https://mangodoc.oss-cn-beijing.aliyuncs.com/geek8geeks/How_to_Transparently_Overlaid_Two_Directories_Using_UnionFS_in_Linux?_0.jpg)

**第一步：**进入超级用户模式，安装_unionfs-fuse 包_。

```
$ apt install unionfs-fuse
```

![](https://mangodoc.oss-cn-beijing.aliyuncs.com/geek8geeks/How_to_Transparently_Overlaid_Two_Directories_Using_UnionFS_in_Linux?_1.jpg)

**第 2 步：**现在创建一个_dir1_目录，然后在该目录中_创建两个文件 f1_和_f2。_

```
$ mkdir /root/dir1
$ touch /root/dir1/f1
$ touch /root/dir1/f2
```

![](https://mangodoc.oss-cn-beijing.aliyuncs.com/geek8geeks/How_to_Transparently_Overlaid_Two_Directories_Using_UnionFS_in_Linux?_2.jpg)

**第 3 步：**创建一个_dir2_目录，然后在该目录中_创建两个文件 f3_和_f4。_

```
$ mkdir /root/dir2
$ touch /root/dir2/f3
$ touch /root/dir2/f4
```

![](https://mangodoc.oss-cn-beijing.aliyuncs.com/geek8geeks/How_to_Transparently_Overlaid_Two_Directories_Using_UnionFS_in_Linux?_3.jpg)

**第四步：**创建一个名为_union的目录_

**第 5 步：**现在_使用 unionfs-fuse__将 dir1_和_dir2_挂载到 union 目录，然后列出文件。

```
$ unionfs /root/dir1:/root/dir2  /root/union/
$ ls /root/union/
f1 f2 f3 f4
```

![](https://mangodoc.oss-cn-beijing.aliyuncs.com/geek8geeks/How_to_Transparently_Overlaid_Two_Directories_Using_UnionFS_in_Linux?_4.jpg)

因此我们可以看到所有四个文件 f1、f2、f3、f4 都在目录_union_中可用。

**第 6 步：**要删除 unionfs-fuse 包，请使用该命令。

```
sudo apt-get remove unionfs-fuse 
```

**第 7 步：**要删除 unionfs-fuse 包及其依赖包，请使用该命令。

```
sudo apt-get remove --auto-remove unionfs-fuse
```