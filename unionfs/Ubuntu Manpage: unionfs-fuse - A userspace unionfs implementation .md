---
title: "Ubuntu Manpage: unionfs-fuse - A userspace unionfs implementation "
layout: post
---


# Ubuntu Manpage: unionfs-fuse - A userspace unionfs implementation 
<a style="text-decoration:underline" href="https://manpages.ubuntu.com/manpages/trusty/man8/unionfs-fuse.8.html">Ubuntu Manpage: unionfs-fuse - A userspace unionfs implementation </a><br>
< center>Canonical</center>
Provided by: [unionfs-fuse_0.24-2.2ubuntu1_amd64](https://launchpad.net/ubuntu/trusty/+package/unionfs-fuse) [![bug](https://manpages.ubuntu.com/img/bug.png)](https://bugs.launchpad.net/ubuntu/+source/unionfs-fuse/+filebug-advanced "Report a bug in the content of this documentation")

#### **NAME**

```
       unionfs-fuse - A userspace unionfs implementation


```

#### **SYNOPSIS**

```
       unionfs-fuse [-o option1 \-o option2 ... \-o optionN ]
                    top_branch:lower_branch:...:lowest_branch
                    mount_point


```

#### **DESCRIPTION**

```
       unionfs-fuse overlays several directory into one single mount point.

       It  first tries to access the file on the top branch and if the file does not exist there,
       it continues on lower level branches.  If the user tries to modify a file on a lower level
       read-only  branch  the  file  is  copied  to  to  a  higher level read-write branch if the
       copy-on-write (cow)  mode was enabled.


```

#### **OPTIONS**

```
       Below is a summary of unionfs-fuse options

       \-o cow Enable copy-on-write

       \-o stats
              Show statistics in the file 'stats' under the mountpoint.

       \-o statfs_omit_ro
              By default blocks of all branches are counted in statfs() calls (e.g. by 'df').  On
              setting  this  option read-only branches will be omitted for the summary of blocks.
              This may sound weird but it actually fixes "wrong" percentage of free space.

       \-o noinitgroups
              Since version 0.23 without any effect, just left over for compatibility.  Might  be
              removed in future versions.

       \-o chroot=path
              Path  to chroot into. By using this option unionfs-fuse may be used for live CDs or
              live USB sticks, etc. So it can serve "/" as filesystem. If you do not specify this
              option  and try to use it for "/" it will deadlock on calling 'pivot_root'.  If you
              do set this option, you also need to specify the branches relativly  to  the  given
              chroot directory. See examples/S01a-unionfs-fuse-live-cd.sh for an example.

       \-o max_files=number
              Maximum  number  of  open  files. Most system have a default of 1024 open files per
              process. For example if unionfs-fuse servs "/" applications like KDE or GNOME might
              have  much more open files, which will make the unionfs-fuse process to exceed this
              limit. Suggested for "/" is >16000 or even >32000 files.   If  this  limit  exceeds
              unionfs-fuse will not be able to open further files.


```

#### **Options** **to** **libfuse**

```
       There are several further options available, which don't directly apply to unionfs, but to
       libfuse. Please run "unionfs-fuse --help" to see these.  We already set the  "-o  default-
       permissions" options on our own.


```

#### **EXAMPLES**

```
        unionfs-fuse -o cow,max_files=32768 \\
                     -o allow_other,use_ino,suid,dev,nonempty \\
                     /u/host/etc=RW:/u/group/etc=RO:/u/common/etc=RO \\
                     /u/union/etc


```

#### **Meta** **data**

```
       Like  other  filesystems unionfs-fuse also needs to store meta data.  Well, presently only
       information about deleted files and directories need to be stored, but in future  releases
       more  information might be required, e.g.  inode-numbers for persistent inode information.
       Meta data information are saved and looked  for  in  the  .unionfs/  directories  of  each
       branch-root.    So    in    the    example    above,   these   are   /u/host/etc/.unionfs,
       /u/group/etc/.unionfs and /u/common/etc/.unionfs.  Within  these  directories  a  complete
       directory  structure  may  be  found.   Example:  If  the admin decides to delete the file
       /etc/test/testfile, which only exists in /u/unionfs/etc/test/testfile, unionfs-fuse  can't
       delete  this  file,  since  it  is  on  a  read-only  branch. So instead the whiteout file
       /u/host/etc/.unionfs/test/testfile_HIDDEN~ will be created.  So  on  accessing  the  union
       filesystem,   test/testfile   will  not  be  visible.   Please  also  note  that  whiteout
       files/directories will only hide the  files  in  lower  level  branches.  So  for  example
       whiteouts  in  the  group directory (/u/group/etc/.unionfs of the example above) will only
       hide file of the common branch (/u/common/etc), but  not  these  of  the  group  and  host
       branches.   Especially  for diskless-booted environments it is rather useful for the admin
       to create whiteout files him/her-self.  For  example  one  should  blacklist  network  re-
       initializations,  /etc/mtab, /etc/nologin of the server and several cron-scripts. This can
       be easily achieved by creating  whiteout  files  for  these  scripts  in  the  group  meta
       directory.


```

#### **KNOWN** **ISSUES**

```
       1) Another issue is that presently there is no support for read-only branches
       when copy-on-write is disabled, thus, -ocow is NOT specified! Support for
       that might be added in later releases.


```

#### **AUTHORS**

```
       unionfs-fuse Original implemention by Radek Podgorny <[radek@podgorny.cz](mailto:radek@podgorny.cz)\>


```

#### **COPYRIGHT**

```
       Radek Podgorny <[radek@podgorny.cz](mailto:radek@podgorny.cz)\>, Bernd Schubert <[bernd-schubert@gmx.de](mailto:bernd-schubert@gmx.de)\>


```

#### **THANKS**

```
       Many thanks to the author of the FUSE filesystem Miklos Szeredi.

```