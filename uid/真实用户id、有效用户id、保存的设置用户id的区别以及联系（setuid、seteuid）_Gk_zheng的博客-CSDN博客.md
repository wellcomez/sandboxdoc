---
title: "真实用户id、有效用户id、保存的设置用户id的区别以及联系（setuid、seteuid）_Gk_zheng的博客-CSDN博客"
layout: post
---


# 真实用户id、有效用户id、保存的设置用户id的区别以及联系（setuid、seteuid）_Gk_zheng的博客-CSDN博客
<a style="text-decoration:underline" href="https://blog.csdn.net/a823837282/article/details/106737352/">真实用户id、有效用户id、保存的设置用户id的区别以及联系（setuid、seteuid）_Gk_zheng的博客-CSDN博客</a><br>

文章转载自：[https://blog.csdn.net/taiyang1987912/article/details/40651623](https://blog.csdn.net/taiyang1987912/article/details/40651623)

在使用 setuid() 函数时会遇到 3 个关于 ID 的概念：  
real user ID -- 真实用户 ID  
effective user ID -- 有效用户 ID  
saved set-user-ID -- 保存了的设置用户 ID。

真实用户 ID (real user ID) 就是通常所说的 UID，在 /etc/passwd 中能看到它的身影，如：

> beyes:x: **1000**:1000:beyes,206,26563288,63230688:/home/beyes:/bin/bash

它用来标识系统中各个不同的用户。普通用户无法改变这个 ID 值。

有效用户 ID (effective) 表明，在运行一个程序时，你是以哪种有效身份运行它的。一般而言，有效用户ID 等于 真实用户 ID。这两个 ID 值可以用 [geteuid()](http://www.groad.net/bbs/read.php?tid-868.html) 和 [getuid()](http://www.groad.net/bbs/read.php?tid-868.html) 函数获取。

```
#include <sys/types.h>

#include <unistd.h>

#include <stdio.h>

#include <stdlib.h>


int main(void)

{

printf ("The real user ID is: %d\n", getuid());

printf ("The effective user ID is :%d\n", geteuid());

return (0);

}
```

编译程序后，查看生成的可执行文件权限：

> $ls -l getuid.exe  
> \-rwxr-xr-x 1 beyes beyes 4775 Jun  9 15:38 getuid.exe

**普通用户运行**：

> $ ./getuid.exe  
> The real user ID is: 1000  
> The effective user ID is :1000

**root 用户运行**：

> \# ./getuid.exe  
> The real user ID is: 0  
> The effective user ID is :0

这就是所说的一般情况：实际用户ID **\==** 有效用户 ID

**下面看不一致的情况**：  
使用 chmod 改变该程序的有效权限位(suid)：

> $ chmod u+s getuid.exe  
> $ ls -l getuid.exe  
> \-rwsr-xr-x 1 beyes beyes 4775 Jun  9 15:38 getuid.exe

  
再使用普通用户运行：

> $ ./getuid.exe  
> The real user ID is: 1000  
> The effective user ID is :1000

切换到 root 用户运行：

> \# ./getuid.exe  
> The real user ID is: 0  
> The effective user ID is :1000

从 root 运行输出来看，有效用户 ID 的值为 1000 。也就是说，root 运行这个程序时，是没有 root 的权限的，它只有 beyes 这个普通用户(实际用户 ID 为 1000)的权限。关于设置有效权限位可以参考：[http://www.groad.net/bbs/read.php?tid-367.html](http://www.groad.net/bbs/read.php?tid-367.html)

下面用一个实例来验证 root 运行此程序时只有 beyes 这个普通用户的权限，按下面步骤来进行：

1\. 现在 /root 目录下创建一个普通的文本文件 rootfile.txt ：

> \# echo "hello world" > /root/rootfile.txt  
> \# ls -l /root/rootfile.txt  
> \-rw-r--r-- 1 root root 12 Jun  9 15:48 /root/rootfile.txt

  
2\. 修改上面的程序代码为：

```
#include <sys/types.h>

#include <unistd.h>

#include <stdio.h>

#include <stdlib.h>


int main(void)

{

    const char *file = "/root/rootfile.txt";


    printf ("The real user ID is: %d\n", getuid());

    printf ("The effective user ID is :%d\n", geteuid());


    if (!unlink (file))

        printf ("Ok, I am root, and I can delete the file which in /root directory.\n");

    else

        perror ("unlink error");


    return (0);

}
```

在上面的代码中所要做的事情很简单，就是要尝试删除 /root/rootfile.txt 这个文件。使用普通用户来编译该程序。

现在先使用普通用户来运行该程序：

> $ ./getuid.exe  
> The real user ID is: 1000  
> The effective user ID is :1000  
> unlink error: Permission denied

很自然，普通用户没有权限删除 /root 下的文件。

下面使用 root 来运行该程序：

> \# ./getuid.exe  
> The real user ID is: 0  
> The effective user ID is :0  
> Ok, I am root, and I can delete the file which in /root directory.

也很正常，root 有权限删除它目录下的文件。

现在为这个程序添加有效权限位，注意这里是用普通用户运行的 chmod 命令：

> beyes@debian:~$ chmod u+s getuid.exe  
> beyes@debian:~$ ls -l getuid.exe  
> \-rwsr-xr-x 1 beyes beyes 5211 Jun 10 10:45 getuid.exe

再次分别以 普通用户 和 root 用户运行上面的程序：

普通用户：

> $ ./getuid.exe  
> The real user ID is: 1000  
> The effective user ID is :1000  
> unlink error: Permission denied

还是一样，普通用户并没有权限删除 /root 下的文件。

root 用户：

> \# ./getuid.exe  
> The real user ID is: 0  
> The effective user ID is :1000  
> unlink error: Permission denied

由输出可见，root 用户也没法删除该文件！同时我们看到，此时的 有效用户 ID 值为 1000，所以我们知道，这个程序所赋予操作文件的权限不但要检查实际用户ID，更重要的是要考虑 有效用户ID ；如果 有效用户 没有权限删除，那么换成 root 用户它也相当于被降权了，当然这个降权仅限于在这个程序的空间中。

* * *

下面通过 setuid() 和 seteuid() 这两个函数来考察一下 saved set-user-ID (保存的设置用户ID)这个概念。

在使用 setuid() 时会遇到如下情况：  
1\. 若进程有 root 权限，则函数将实际用户 ID、有效用户 ID 设置为参数 uid  。见如下代码：

```
#include <stdio.h>

#include <stdlib.h>

#include <sys/types.h>

#include <unistd.h>


void show_ids(void)

{

    printf ("real uid: %d\n", getuid());

    printf ("effective uid: %d\n", geteuid());

}


int main(int argc, char *argv[])

{


    int uid;

    show_ids();

    uid = atoi(argv[1]);


    if (setuid (uid) < 0)

        perror ("setuid error");


    show_ids();


    return (0);

}
```

下面使用 root 用户来运行上面的程序：

> \# ./setuid.exe 1001  
> real uid: 0  
> effective uid: 0  
> real uid: 1001  
> effective uid: 1001

由此可见，在 root 下，实际用户 ID 和有效用户 ID 均被设为 setuid() 的参数 uid 的值。

2\. 若进程不具有 root 权限，那么普通用户使用 setuid() 时参数 uid 只能是自己的，没有权限设置别的数值，否则返回失败：  
使用普通用户来运行：

> $ ./setuid.exe 1001  
> real uid: 1000  
> effective uid: 1000  
> setuid error: Operation not permitted  
> real uid: 1000  
> effective uid: 1000

由以上可以看出，只有超级用户进程才能更改实际用户 ID 。所以一个非特权用户进程不能通过 setuid 或 seteuid 得到特权用户权限。

这里考虑 su 这个程序，su 可以将普通用户切换到 root 用户。这是因为，su 被设置了 有效权限位：

> \# ll /bin/su  
> \-rwsr-xr-x 1 root root 29152 Feb 16 04:50 /bin/su

如上面所做实验描述的一样，普通用户在运行 su 时，它也就拥有了 root 的权限。

对于调用了 setuid() 函数的程序要格外小心，当进程的有效用户 ID 即 euid 是 root 用户时(设置了有效权限位)，如果调用了 setuid() ，那么它会与它相关的所有 ID (real user ID, effective user ID,saved set-user-ID) 都变成它参数里所设的 ID，这样该进程就变成了普通用户进程，也就再也恢复不了 root 权限了。看下面代码：

```
#include <stdio.h>

#include <stdlib.h>


void show_ids (void)

{

    printf ("The real user ID is: %d\n", getuid());

    printf ("The effective user ID is :%d\n", geteuid());

}


int main(void)

{

    const char *file = "/root/rootfile3.txt";  

        setuid (0)

    show_ids();

    if (!unlink (file)) {

        printf ("Ok, I am root, and I can delete the file which in /root directory.\n");

    system ("echo hello world > /root/rootfile3.txt");

    printf ("Now, drop the root privileges.\n");
    
    if (setuid (1000) < 0) {

        perror ("setuid");

    exit (EXIT_FAILURE);

    }

    show_ids();

    if (unlink (file) < 0) {

        printf ("Ok, we have no privilege to delete rootfile.txt.\n");

    }

    printf ("try to regain root power again...\n");

    if (seteuid (0)) {

        perror ("seteuid");

    show_ids();

    exit (EXIT_FAILURE);

    }

}
```

我们使用 root 编译上面的程序，并运行 chmod u+s 给程序添加 suid 位，然后以普通用户来运行它：

> \# ./getuid3 The real user ID is: 0The effective user ID is :0Ok, I am root, and I can delete the file which in /root directory.Now, drop the root privileges.The real user ID is: 1000The effective user ID is :1000Ok, we have no privilege to delete rootfile.txt.try to regain root power again... **seteuid: Operation not permitted**The real user ID is: 1000The effective user ID is :1000

由输出可见，在运行 setuid (1000) 函数时，我们还是具有 root 权限的，所以该函数会设置成功。正是因为有了 root 权限，所以 3 个 ID (真实用户ID，已保存用户ID，有效用户ID)都会被设置为 1000。所以在运行完 setuid(1000) 后，进程已经被降权为普通用户，此时想再  seteuid (0) 提高权限已经不可能。这里需要提到一点，对于 show_ids() 函数里，我们无法获取 保存的设置用户ID(saved set-user-ID)，这是因为没有这种 API 。但是我们知道这个约定：当用户是 root 时，使用 setuid() 来修改 uid，这 3 个 ID 是会被同时都修改的。但是有没有办法，先使进程降权后在某些时候再恢复 root 权力呢？办法是使用 seteuid() 而不是 setuid() 。那 setuid() 和 seteuid() 有什么不同么？在 seteuid() 的 man 手册里提到：

> seteuid() sets the effective user ID of the calling process.  Unprivileged user processes may only set the effective user ID to the real user ID, the effec‐tive user ID or the saved set-user-ID.

setedui() 用来设置调用进程的有效用户 ID。普通用户进程只能将 有效用户ID 设置为 实际用户ID，有效用户ID，保存的设置用户ID。这里比较重要的是，**seteuid() 中的参数可以被设置为 保存的设置用户 ID** 。保存的设置用户 ID 是这样的一种概念：它是从 exec 复制有效用户 ID 而得来的。具体的说，当我们从一个 shell 里执行一个外部命令时(这里就当做是执行上面的 getuid3 这个)，如果该程序设置了用户ID位(有效权限位)，那么在 exec 根据文件的用户ID设置了进程的有效用户 ID 后，就会将这个副本保存起来。简单的说，saved set-user-ID 保存了 有效用户ID 的值。比如对于 getuid3 这个程序，saved set-user-ID 保存的值就是 0 。据此，我们修改上面的 getuid3 程序代码为：

```
#include <sys/types.h>

#include <unistd.h>

#include <stdio.h>

#include <stdlib.h>


void show_ids (void)

{

    printf ("The real user ID is: %d\n", getuid());

    printf ("The effective user ID is :%d\n", geteuid());

}


int main(void)

{

    const char *file = "/root/rootfile3.txt";


    show_ids();

    if (!unlink (file)) {

        printf ("Ok, I am root, and I can delete the file which in /root directory.\n");

        system ("echo hello world > /root/rootfile3.txt");

        printf ("Now, drop the root privileges.\n");

        if (seteuid (1000) < 0) {

            perror ("setuid");

            exit (EXIT_FAILURE);

        }

        show_ids();

        if (unlink (file) < 0) {

            printf ("Ok, we have no privilege to delete rootfile3.txt.\n");

           }

    printf ("try to regain root power again...\n");

    if (seteuid (0)) {

        perror ("seteuid");

        show_ids();

        exit (EXIT_FAILURE);

        }

    }

    show_ids();


    printf ("try to delete rootfile3.txt again\n");

    if (!unlink(file)) {

        printf ("Ok, regain root power successful!\n");

        system ("echo hello world > /root/rootfile3.txt");

        return (0);

        }


    return (0);

}
```

在上面的代码中，我们将原来的 setuid(1000) 替换为 seteuid(1000); 。并且在此后，再次尝试删除 /root/rootfile3.txt 这个文件。下面在普通用户下运行该程序：

> beyes@debian:~/C/syscall/getuid$ ./getuid3  
> The real user ID is: 1000  
> The effective user ID is :0  
> Ok, I am root, and I can delete the file which in /root directory.  
> Now, drop the root privileges.  
> The real user ID is: 1000  
> The effective user ID is :1000  
> Ok, we have no privilege to delete rootfile3.txt.  
> try to regain root power again...  
> The real user ID is: 1000  
> The effective user ID is :0  
> try to delete rootfile.txt again  
> Ok, regain root power successful!

此时我们看到整个过程：  
先是普通用户执行了具有 root 有效权限位设置的程序，它成功的删除了 /root 下面的一个文本文件；然后使用 system() 系统调用恢复了该文件，目的是方便下面继续实验。接着，它使用 seteuid() 函数时该进程降权为普通用户权限进程。此后，正是因为有了 saved set-user-ID 的保存，所以当再次使用 seteuid() 恢复 进程的 root 权限时可以恢复成功！

所以再次看到，setuid() 会改变 saved set-user-ID 的值而不能恢复权限；而 seteuid() 不会改变 saved set-user-ID 这个值，所以它能够恢复。