#!/usr/bin/env python3
import re
import sys
try:
    file = sys.argv[1]
except:
    file = None
test = "[![@shemgp](https://avatars.githubusercontent.com/u/1734126?s=52&v=4)](https://github.com/shemgp)[![@SkewedZeppelin](https://avatars.githubusercontent.com/u/8296104?s=52&v=4)](https://github.com/SkewedZeppelin)[![@fuelflo](https://avatars.githubusercontent.com/u/8934762?s=52&v=4)](https://github.com/fuelflo)[![@BugShooter](https://avatars.githubusercontent.com/u/10827226?s=52&v=4)](https://github.com/BugShooter)[![@netblue30](https://avatars.githubusercontent.com/u/13711990?s=52&v=4)](https://github.com/netblue30)[![@chiraag-nataraj](https://avatars.githubusercontent.com/u/14947779?s=52&v=4)](https://github.com/chiraag-nataraj)[![@kensfc-23](https://avatars.githubusercontent.com/u/22351156?s=52&v=4)](https://github.com/kensfc-23)"
test_bold = "### **[chiraag-nataraj](https://github.com/chiraag-nataraj)** commented [on 9 Apr 2017](https://github.com/netblue30/firejail/issues/1204#issuecomment-292787088)"
test_remove_author = '''

Author

###  kensfc-23 on 30 Apr 2017

It doesn't work here. I use master. I do "sudo git pull" twice a day ;-) Greetings 2017-04-26 17:59 GMT+02:00 netblue30 <notifications@github.com>:

[…](https://github.com/netblue30/firejail/issues/1204#)
Author

### kensfc-23 on 9 Apr 2017
Author


### 111kensfc-23 on 9 Apr 2017
'''
http_ptn = 'https://[\d\w./\-\?=&]+'
re_remove_bold = re.compile(r'\*{2}\[(.*)\]\(%s\)\*{2}' % (http_ptn))
re_remove_comment = re.compile(r'commented\s\[(.*)\].*')
re_remove_image = re.compile(
    r'\[!\[@(.*)\]\(%s\)\]\(%s\)' % (http_ptn, http_ptn))
re_remove_emji = re.compile(r'.*emoji')


def replace_with_except(test, expr, replace=[]):
    try:
        sub = ''
        finds = expr.findall(test)
        if len(finds) == 0:
            return None
        if type(replace) == type([]):
            if len(replace):
                sub = finds[0]
        elif type(replace) == type(""):
            sub = replace
        return re.sub(expr, sub, test)
    except:
        return None


def replace_with(test, expr, replace=[]):
    a = replace_with_except(test, expr, replace)
    if a is None:
        return test
    while True:
        b = replace_with_except(a, expr, replace)
        if b == None:
            return a
        else:
            a = b


def replace_with_author(test):
    for key in ["Author", "Owner", "Collaborator", "Contributor", "Member"]:
        re_remove_author = re.compile(r'%s\s*\n+(\#+)' % (key))
        finds = re_remove_author.findall(test)
        print("len of (%d) %s" % (len(finds), re_remove_author))
        if len(finds):
            test = re.sub(re_remove_author, "%s %s" % (finds[0], key), test)
    return test


print("re_remove_image", replace_with(test, re_remove_image))
print("re_remove_bold", replace_with(test_bold, re_remove_bold, [0]))
print("re_remove_author", replace_with_author(test_remove_author))
fp = open(file)
lines = fp.readlines()
fp.close()


def replace_line(a):
    a = replace_with(a, re_remove_bold, [0])
    a = replace_with(a, re_remove_comment, [0])
    a = replace_with(a, re_remove_image)
    a = replace_with(a, re_remove_emji)
    a = replace_with(a, re.compile(r"\n+\s*(Closed)\n"),
                     '<span stycle="background-color:orange;">Closed</span>')
    return a


aaa = list(map(replace_line, lines))
aaa = "".join(aaa)
aaa = replace_with_author(aaa)
open(file, "w").write(aaa)
