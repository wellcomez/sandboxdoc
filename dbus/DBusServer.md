---
title: "DBusServer"
layout: post
---
### Class

# Gio DBusServer

[−]

#### Description

```
final class Gio.DBusServer : GObject.Object {
  /* No available fields */
}
```

`GDBusServer` is a helper for listening to and accepting D-Bus connections. This can be used to create a new D-Bus server, allowing two peers to use the D-Bus protocol for their own specialized communication. A server instance provided in this way will not perform message routing or implement the org.freedesktop.DBus interface.

To just export an object on a well-known name on a message bus, such as the session or system bus, you should instead use g_bus_own_name().

An example of peer-to-peer communication with GDBus can be found in [gdbus-example-peer.c](https://gitlab.gnome.org/GNOME/glib/-/blob/HEAD/gio/tests/gdbus-example-peer.c).

Note that a minimal `GDBusServer` will accept connections from any peer. In many use-cases it will be necessary to add a `GDBusAuthObserver` that only accepts connections that have successfully authenticated as the same user that is running the `GDBusServer`. Since GLib 2.68 this can be achieved more simply by passing the `G_DBUS_SERVER_FLAGS_AUTHENTICATION_REQUIRE_SAME_USER` flag to the server.

Available since:

2.26

[−]

#### Hierarchy

GDBusServerGInitableGObject

[−]

#### Ancestors

-   [GObject](https://docs.gtk.org/gobject/class.Object.html)

[−]

#### Implements

-   [GInitable](https://docs.gtk.org/gio/iface.Initable.html "Initable")

[−]

#### Constructors

###### g_dbus_server_new_sync

Creates a new D-Bus server that listens on the first address in `address` that works.

Available since: 2.26

[−]

#### Instance methods

###### g_dbus_server_get_client_address

Gets a [D-Bus address](https://dbus.freedesktop.org/doc/dbus-specification.html#addresses) string that can be used by clients to connect to `server`.

Available since: 2.26

###### g_dbus_server_get_flags

Gets the flags for `server`.

Available since: 2.26

###### g_dbus_server_get_guid

Gets the GUID for `server`, as provided to g_dbus_server_new_sync().

Available since: 2.26

###### g_dbus_server_is_active

Gets whether `server` is active.

Available since: 2.26

###### g_dbus_server_start

Starts `server`.

Available since: 2.26

###### g_dbus_server_stop

Stops `server`.

Available since: 2.26

[+]

#####  Methods inherited from [GObject] (43)

[+]

#####  Methods inherited from [GInitable] (1)

[−]

#### Properties

###### Gio.DBusServer:active

Whether the server is currently active.

Available since: 2.26

###### Gio.DBusServer:address

The D-Bus address to listen on.

Available since: 2.26

###### Gio.DBusServer:authentication-observer

A `GDBusAuthObserver` object to assist in the authentication process or `NULL`.

Available since: 2.26

###### Gio.DBusServer:client-address

The D-Bus address that clients can use.

Available since: 2.26

###### Gio.DBusServer:flags

Flags from the `GDBusServerFlags` enumeration.

Available since: 2.26

###### Gio.DBusServer:guid

The GUID of the server.

Available since: 2.26

[−]

#### Signals

###### Gio.DBusServer::new-connection

Emitted when a new authenticated connection has been made. Use `g_dbus_connection_get_peer_credentials()` to figure out what identity (if any), was authenticated.

Available since: 2.26

[−]

#####  Signals inherited from [GObject] (1)

###### GObject.Object::notify

The notify signal is emitted on an object when one of its properties has its value set through g_object_set_property(), g_object_set(), et al.


### Function

# Gio bus_own_name

[−]

#### Declaration

```
guint
g_bus_own_name (
  GBusType bus_type,
  const gchar* name,
  GBusNameOwnerFlags flags,
  GBusAcquiredCallback bus_acquired_handler,
  GBusNameAcquiredCallback name_acquired_handler,
  GBusNameLostCallback name_lost_handler,
  gpointer user_data,
  GDestroyNotify user_data_free_func
)

```

[−]

#### Description

Starts acquiring `name` on the bus specified by `bus_type` and calls `name_acquired_handler` and `name_lost_handler` when the name is acquired respectively lost. Callbacks will be invoked in the [thread-default main context][g-main-context-push-thread-default] of the thread you are calling this function from.

You are guaranteed that one of the `name_acquired_handler` and `name_lost_handler` callbacks will be invoked after calling this function - there are three possible cases:

-   `name_lost_handler` with a `NULL` connection (if a connection to the bus can’t be made).
    
-   `bus_acquired_handler` then `name_lost_handler` (if the name can’t be obtained)
    
-   `bus_acquired_handler` then `name_acquired_handler` (if the name was obtained).
    

When you are done owning the name, just call `g_bus_unown_name()` with the owner id this function returns.

If the name is acquired or lost (for example another application could acquire the name if you allow replacement or the application currently owning the name exits), the handlers are also invoked. If the `GDBusConnection` that is used for attempting to own the name closes, then `name_lost_handler` is invoked since it is no longer possible for other processes to access the process.

You cannot use `g_bus_own_name()` several times for the same name (unless interleaved with calls to g_bus_unown_name()) - only the first call will work.

Another guarantee is that invocations of `name_acquired_handler` and `name_lost_handler` are guaranteed to alternate; that is, if `name_acquired_handler` is invoked then you are guaranteed that the next time one of the handlers is invoked, it will be `name_lost_handler`. The reverse is also true.

If you plan on exporting objects (using e.g. g_dbus_connection_register_object()), note that it is generally too late to export the objects in `name_acquired_handler`. Instead, you can do this in `bus_acquired_handler` since you are guaranteed that this will run before `name` is requested from the bus.

This behavior makes it very simple to write applications that wants to [own names][gdbus-owning-names] and export objects. Simply register objects to be exported in `bus_acquired_handler` and unregister the objects (if any) in `name_lost_handler`.

Available since:

2.26

This function is not directly available to language bindings

The implementation of this function is provided by [`g_bus_own_name_with_closures()`](https://docs.gtk.org/gio/func.bus_own_name_with_closures.html) in language bindings

[−]

#### Parameters

`bus_type`

_Type:_ [`GBusType`](https://docs.gtk.org/gio/enum.BusType.html)

The type of bus to own a name on.

`name`

_Type:_ `const gchar*`

The well-known name to own.

The data is owned by the caller of the function.

The value is a NUL terminated UTF-8 string.

`flags`

_Type:_ [`GBusNameOwnerFlags`](https://docs.gtk.org/gio/flags.BusNameOwnerFlags.html)

A set of flags from the `GBusNameOwnerFlags` enumeration.

`bus_acquired_handler`

_Type:_ [`GBusAcquiredCallback`](https://docs.gtk.org/gio/callback.BusAcquiredCallback.html)

Handler to invoke when connected to the bus of type `bus_type` or `NULL`.

The argument can be `NULL`.

`name_acquired_handler`

_Type:_ [`GBusNameAcquiredCallback`](https://docs.gtk.org/gio/callback.BusNameAcquiredCallback.html)

Handler to invoke when `name` is acquired or `NULL`.

The argument can be `NULL`.

`name_lost_handler`

_Type:_ [`GBusNameLostCallback`](https://docs.gtk.org/gio/callback.BusNameLostCallback.html)

Handler to invoke when `name` is lost or `NULL`.

The argument can be `NULL`.

`user_data`

_Type:_ `gpointer`

User data to pass to handlers.

The argument can be `NULL`.

The data is owned by the caller of the function.

`user_data_free_func`

_Type:_ [`GDestroyNotify`](https://docs.gtk.org/glib/callback.DestroyNotify.html)

Function for freeing `user_data` or `NULL`.

The argument can be `NULL`.

[−]

#### Return value

_Type:_ `guint`

An identifier (never 0) that can be used with `g_bus_unown_name()` to stop owning the name.