---
title: "D-Bus - Wikipedia"
layout: post
---
# D-Bus

From Wikipedia, the free encyclopedia

[Jump to navigation](https://en.wikipedia.org/wiki/D-Bus#mw-head)[Jump to search](https://en.wikipedia.org/wiki/D-Bus#searchInput)

"DBus" redirects here. For the Tibetan province, see [Dbus](https://en.wikipedia.org/wiki/%C3%9C_(region) "Ü (region)").

D-Bus
| | |
|---|---|
|[Developer(s)](https://en.wikipedia.org/wiki/Programmer "Programmer")s|[Red Hat](https://en.wikipedia.org/wiki/Red_Hat "Red Hat")|
|Initial release|November 2006; 16 years ago|
|[Stable release](https://en.wikipedia.org/wiki/Software_release_life_cycle "Software release life cycle")|1.14.0 / February 28, 2022; 9 months ago[[1]](https://en.wikipedia.org/wiki/D-Bus#cite_note-1)|
|[Preview release](https://en.wikipedia.org/wiki/Software_release_life_cycle#Beta "Software release life cycle")|1.15.0 / February 28, 2022; 9 months ago[[2]](https://en.wikipedia.org/wiki/D-Bus#cite_note-2)|
|[Repository](https://en.wikipedia.org/wiki/Repository_(version_control) "Repository (version control)")|-   [cgit.freedesktop.org/dbus/dbus/](https://cgit.freedesktop.org/dbus/dbus/) [![Edit this at Wikidata](https://upload.wikimedia.org/wikipedia/en/thumb/8/8a/OOjs_UI_icon_edit-ltr-progressive.svg/10px-OOjs_UI_icon_edit-ltr-progressive.svg.png)](https://www.wikidata.org/wiki/Q768923#P1324 "Edit this at Wikidata")|
|Written in|[C](https://en.wikipedia.org/wiki/C_(programming_language) "C (programming language)")|
|[Operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system")|[Cross-platform](https://en.wikipedia.org/wiki/Cross-platform "Cross-platform")|
|[Type](https://en.wikipedia.org/wiki/Software_categories#Categorization_approaches "Software categories")|-   [IPC daemon](https://en.wikipedia.org/wiki/Inter-process_communication "Inter-process communication") -   [Linux on the desktop](https://en.wikipedia.org/wiki/Linux_on_the_desktop "Linux on the desktop")|
|[License](https://en.wikipedia.org/wiki/Software_license "Software license")|[GPLv2+](https://en.wikipedia.org/wiki/GNU_General_Public_License "GNU General Public License") or [AFL](https://en.wikipedia.org/wiki/Academic_Free_License "Academic Free License") 2.1[[3]](https://en.wikipedia.org/wiki/D-Bus#cite_note-3)|
|Website|[www.freedesktop.org/wiki/Software/dbus](http://www.freedesktop.org/wiki/Software/dbus)


In [computing](https://en.wikipedia.org/wiki/Computing "Computing"), **D-Bus** (short for "**Desktop Bus**"[[4]](https://en.wikipedia.org/wiki/D-Bus#cite_note-4)) is a [message-oriented middleware](https://en.wikipedia.org/wiki/Message-oriented_middleware "Message-oriented middleware") mechanism that allows communication between multiple [processes](https://en.wikipedia.org/wiki/Process_(computing) "Process (computing)") running concurrently on the same machine.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) D-Bus was developed as part of the [freedesktop.org](https://en.wikipedia.org/wiki/Freedesktop.org "Freedesktop.org") project, initiated by [Havoc Pennington](https://en.wikipedia.org/wiki/Havoc_Pennington "Havoc Pennington") from [Red Hat](https://en.wikipedia.org/wiki/Red_Hat "Red Hat") to standardize services provided by [Linux](https://en.wikipedia.org/wiki/Linux "Linux") [desktop environments](https://en.wikipedia.org/wiki/Desktop_environment "Desktop environment") such as [GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME") and [KDE](https://en.wikipedia.org/wiki/KDE "KDE").[[7]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus_q1-7)[[8]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Palmieri_2005-8)[_[dead link](https://en.wikipedia.org/wiki/Wikipedia:Link_rot "Wikipedia:Link rot")_]

The freedesktop.org project also developed a [free and open-source](https://en.wikipedia.org/wiki/Free_and_open-source "Free and open-source") software library called libdbus, as a [reference implementation](https://en.wikipedia.org/wiki/Reference_implementation "Reference implementation") of the specification. This library should not be confused with D-Bus itself, as other implementations of the D-Bus specification also exist, such as GDBus (GNOME),[[9]](https://en.wikipedia.org/wiki/D-Bus#cite_note-gdbus-9) QtDBus ([Qt](https://en.wikipedia.org/wiki/Qt_(software) "Qt (software)")/KDE),[[10]](https://en.wikipedia.org/wiki/D-Bus#cite_note-qtdbus-10) dbus-java[[11]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus-java-11) and sd-bus (part of [systemd](https://en.wikipedia.org/wiki/Systemd "Systemd")).[[12]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Poettering_2015-12)

## Contents

-   [1Overview](https://en.wikipedia.org/wiki/D-Bus#Overview)
-   [2D-Bus specification](https://en.wikipedia.org/wiki/D-Bus#D-Bus_specification)
    -   [2.1Bus model](https://en.wikipedia.org/wiki/D-Bus#Bus_model)
    -   [2.2Object model](https://en.wikipedia.org/wiki/D-Bus#Object_model)
    -   [2.3Communications model](https://en.wikipedia.org/wiki/D-Bus#Communications_model)
-   [3Internals](https://en.wikipedia.org/wiki/D-Bus#Internals)
-   [4History and adoption](https://en.wikipedia.org/wiki/D-Bus#History_and_adoption)
-   [5Implementations](https://en.wikipedia.org/wiki/D-Bus#Implementations)
    -   [5.1libdbus](https://en.wikipedia.org/wiki/D-Bus#libdbus)
    -   [5.2GDBus](https://en.wikipedia.org/wiki/D-Bus#GDBus)
    -   [5.3QtDBus](https://en.wikipedia.org/wiki/D-Bus#QtDBus)
    -   [5.4sd-bus](https://en.wikipedia.org/wiki/D-Bus#sd-bus)
    -   [5.5libnih-dbus](https://en.wikipedia.org/wiki/D-Bus#libnih-dbus)
    -   [5.6kdbus](https://en.wikipedia.org/wiki/D-Bus#kdbus)
    -   [5.7zbus](https://en.wikipedia.org/wiki/D-Bus#zbus)
    -   [5.8Protocol::DBus](https://en.wikipedia.org/wiki/D-Bus#Protocol::DBus)
    -   [5.9Language bindings](https://en.wikipedia.org/wiki/D-Bus#Language_bindings)
-   [6See also](https://en.wikipedia.org/wiki/D-Bus#See_also)
-   [7References](https://en.wikipedia.org/wiki/D-Bus#References)
-   [8External links](https://en.wikipedia.org/wiki/D-Bus#External_links)

## Overview

D-Bus is an [inter-process communication](https://en.wikipedia.org/wiki/Inter-process_communication "Inter-process communication") (IPC) mechanism initially designed to replace the [software component](https://en.wikipedia.org/wiki/Software_component "Software component") communications systems used by the [GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME") and [KDE](https://en.wikipedia.org/wiki/KDE "KDE") Linux [desktop environments](https://en.wikipedia.org/wiki/Desktop_environment "Desktop environment") ([CORBA](https://en.wikipedia.org/wiki/CORBA "CORBA") and [DCOP](https://en.wikipedia.org/wiki/Desktop_communication_protocol "Desktop communication protocol") respectively).[[13]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut_q1-13)[[14]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus_q2-14) The components of these desktop environments are normally distributed in many processes, each one providing only a few—usually one—_services_. These services may be used by regular client [applications](https://en.wikipedia.org/wiki/Application_software "Application software") or by other components of the desktop environment to perform their tasks.

[![Processes without D-Bus](https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Processes_without_D-Bus.svg/200px-Processes_without_D-Bus.svg.png)](https://en.wikipedia.org/wiki/File:Processes_without_D-Bus.svg)

Processes without D-Bus

[![Processes with D-Bus](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Processes_with_D-Bus.svg/200px-Processes_with_D-Bus.svg.png)](https://en.wikipedia.org/wiki/File:Processes_with_D-Bus.svg)

The same processes with D-Bus

Large groups of cooperating processes demand a dense mesh of individual communication channels (using one-to-one IPC methods) between them. D-Bus simplifies the IPC requirements with one single shared channel.

Due to the large number of processes involved—adding up processes providing the services and clients accessing them—establishing one-to-one IPC between all of them becomes an inefficient and quite unreliable[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_]approach. Instead, D-Bus provides a [software-bus](https://en.wikipedia.org/wiki/Software_bus "Software bus") [abstraction](https://en.wikipedia.org/wiki/Abstraction_(software_engineering) "Abstraction (software engineering)") that gathers all the communications between a group of processes over a single shared virtual channel.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) Processes connected to a bus do not know how it is internally implemented, but D-Bus specification guarantees that all processes connected to the bus can communicate with each other through it.

Linux desktop environments take advantage of the D-Bus facilities by instantiating multiple buses, notably:[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

-   a single **system bus**, available to all users and processes of the system, that provides access to system services (i.e. services provided by the [operating system](https://en.wikipedia.org/wiki/Operating_system "Operating system") and also by any system [daemons](https://en.wikipedia.org/wiki/Daemon_(computer_software) "Daemon (computer software)"))
-   a **session bus** for each user login session, that provides desktop services to user applications in the same desktop session, and allows the integration of the desktop session as a whole

A process can connect to any number of buses, provided that it has been granted access to them. In practice, this means that any user process can connect to the system bus and to its current session bus, but not to another user's session buses, or even to a different session bus owned by the same user. The latter restriction may change in the future if all user sessions are combined into a single user bus.[[17]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Poettering_2015_q1-17)

D-Bus provides additional or simplifies existing functionality to the applications, including information-sharing, modularity and [privilege separation](https://en.wikipedia.org/wiki/Privilege_separation "Privilege separation"). For example, information on an incoming voice-call received through [Bluetooth](https://en.wikipedia.org/wiki/Bluetooth "Bluetooth") or [Skype](https://en.wikipedia.org/wiki/Skype "Skype") can be propagated and interpreted by any currently-running music player, which can react by muting the volume or by pausing playback until the call is finished.[[18]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Love_2005-18)

D-Bus can also be used as a [framework](https://en.wikipedia.org/wiki/Software_framework "Software framework") to integrate different components of a user application. For instance, an [office suite](https://en.wikipedia.org/wiki/Office_suite "Office suite") can communicate through the session bus to share data between a [word processor](https://en.wikipedia.org/wiki/Word_processor "Word processor") and a [spreadsheet](https://en.wikipedia.org/wiki/Spreadsheet "Spreadsheet").

## D-Bus specification

### Bus model

Every connection to a bus is identified in the context of D-Bus by what is called a _bus name_.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5) A bus name consists of two or more dot-separated strings of letters, digits, dashes, and underscores. An example of a valid bus name is `org.freedesktop.NetworkManager`.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)

When a process sets up a connection to a bus, the bus assigns to the connection a special bus name called _unique connection name_.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) Bus names of this type are immutable—it is guaranteed they will not change as long as the connection exists—and, more importantly, they cannot be reused during the bus lifetime.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) This means that no other connection to that bus will ever have assigned such unique connection name, even if the same process closes down the connection to the bus and creates a new one. Unique connection names are easily recognizable because they start with the otherwise forbidden colon character.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) An example of a unique connection name is `:1.1553` (the characters after the colon have no particular meaning[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)).

A process can ask for additional bus names for its connection,[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) provided that any requested name is not already being used by another connection to the bus. In D-Bus parlance, when a bus name is assigned to a connection, it is said the connection _owns_ the bus name.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) In that sense, a bus name cannot be owned by two connections at the same time, but, unlike unique connection names, these names can be reused if they are available: a process may reclaim a bus name released—purposely or not—by another process.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)

The idea behind these additional bus names, commonly called _well-known names_, is to provide a way to refer to a service using a prearranged bus name.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) For instance, the service that reports the current time and date in the system bus lies in the process whose connection owns the org.freedesktop.timedate1 bus name, regardless of which process it is.

Bus names can be used as a simple way to implement single-instance applications (second instances detect that the bus name is already taken).[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) It can also be used to track a service process lifecycle, since the bus sends a notification when a bus name is released due to a process termination.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

### Object model

Because of its original conception as a replacement for several component oriented communications systems, D-Bus shares with its predecessors an object model in which to express the semantics of the communications between clients and services. The terms used in the D-Bus object model mimic those used by some [object oriented](https://en.wikipedia.org/wiki/Object_oriented "Object oriented") [programming languages](https://en.wikipedia.org/wiki/Programming_language "Programming language"). That does not mean that D-Bus is somehow limited to OOP languages—in fact, the most used implementation (libdbus) is written in [C](https://en.wikipedia.org/wiki/C_(programming_language) "C (programming language)"), a [procedural programming](https://en.wikipedia.org/wiki/Procedural_programming "Procedural programming") language.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/D-Feet.png/220px-D-Feet.png)](https://en.wikipedia.org/wiki/File:D-Feet.png)

[](https://en.wikipedia.org/wiki/File:D-Feet.png "Enlarge")

Browsing the existing bus names, objects, interfaces, methods and signals in a D-Bus bus using D-Feet

In D-Bus, a process offers its services by exposing _objects_. These objects have _methods_ that can be invoked, and _signals_ that the object can emit.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) Methods and signals are collectively referred to as the _members_ of the object.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5) Any client connected to the bus can interact with an object by using its methods, making requests or commanding the object to perform actions.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) For instance, an object representing a time service can be queried by a client using a method that returns the current date and time. A client can also listen to signals that an object emits when its state changes due to certain events, usually related to the underlying service. An example would be when a service that manages hardware devices—such as USB or network drivers—signals a "new hardware device added" event. Clients should instruct the bus that they are interested in receiving certain signals from a particular object, since a D-Bus bus only passes signals to those processes with a registered interest in them.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)

A process connected to a D-Bus bus can request it to _export_ as many D-Bus objects as it wants. Each object is identified by an _object path_, a string of numbers, letters and underscores separated and prefixed by the slash character, called that because of their resemblance to [Unix filesystem paths](https://en.wikipedia.org/wiki/Unix_path "Unix path").[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) The object path is selected by the requesting process, and must be unique in the context of that bus connection. An example of a valid object path is `/org/kde/kspread/sheets/3/cells/4/5`.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) However, it is not enforced—but also not discouraged—to form hierarchies within object paths.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) The particular naming convention for the objects of a service is entirely up to the developers of such service, but many developers choose to [namespace](https://en.wikipedia.org/wiki/Namespace "Namespace") them using the reserved [domain name](https://en.wikipedia.org/wiki/Domain_name "Domain name") of the project as a prefix (e.g. /org/kde).[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

Every object is inextricably associated to the particular bus connection where it was exported, and, from the D-Bus point of view, only lives in the context of such connection. Therefore, in order to be able to use a certain service, a client must indicate not only the object path providing the desired service, but also the bus name under which the service process is connected to the bus.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5) This in turn allows that several processes connected to the bus can export different objects with identical object paths unambiguously.

An _interface_ specifies members—methods and signals—that can be used with an object.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) It is a set of declarations of methods (including its passing and returning parameters) and signals (including its parameters) identified by a dot-separated name resembling the [Java language](https://en.wikipedia.org/wiki/Java_(programming_language) "Java (programming language)") interfaces notation.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) An example of a valid interface name is `org.freedesktop.Introspectable`.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) Despite their similarity, interface names and bus names should not be mistaken. A D-Bus object can _implement_ several interfaces, but at least must implement one, providing support for every method and signal defined by it. The combination of all interfaces implemented by an object is called the object _type_.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

When using an object, it is a good practice for the client process to provide the member's interface name besides the member's name, but is only mandatory when there is an ambiguity caused by duplicated member names available from different interfaces implemented by the object[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)—otherwise, the selected member is undefined or erroneous. An emitted signal, on the other hand, must always indicate to which interface it belongs.

The D-Bus specification also defines several standard interfaces that objects may want to implement in addition to its own interfaces.[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) Although technically optional, most D-Bus service developers choose to support them in their exported objects since they offer important additional features to D-Bus clients, such as [introspection](https://en.wikipedia.org/wiki/Type_introspection "Type introspection").[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) These standard interfaces are:[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)

-   org.freedesktop.DBus.Peer: provides a way to test if a D-Bus connection is alive.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)
-   org.freedesktop.DBus.Introspectable: provides an introspection mechanism by which a client process can, at run-time, get a description (in [XML](https://en.wikipedia.org/wiki/XML "XML") format) of the interfaces, methods and signals that the object implements.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)
-   org.freedesktop.DBus.Properties: allows a D-Bus object to expose the underlying native object [properties](https://en.wikipedia.org/wiki/Property_(programming) "Property (programming)") or attributes, or simulate them if it does not exist.[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)
-   org.freedesktop.DBus.ObjectManager: when a D-Bus service arranges its objects hierarchically, this interface provides a way to query an object about all sub-objects under its path, as well as their interfaces and properties, using a single method call.[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)

The D-Bus specification defines a number of administrative bus operations (called "bus services") to be performed using the /org/freedesktop/DBus object that resides in the org.freedesktop.DBus bus name.[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) Each bus reserves this special bus name for itself, and manages any requests made specifically to this combination of bus name and object path. The administrative operations provided by the bus are those defined by the object's interface org.freedesktop.DBus. These operations are used for example to provide information about the status of the bus,[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5) or to manage the request and release of additional _well-known_ bus names.[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15)[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)

### Communications model

D-Bus was conceived as a generic, high-level inter-process communication system. To accomplish such goals, D-Bus communications are based on the exchange of _messages_ between processes instead of "raw bytes".[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) D-Bus messages are high-level discrete items that a process can send through the bus to another connected process. Messages have a well-defined structure (even the types of the data carried in their payload are defined), allowing the bus to validate them and to reject any ill-formed message. In this regard, D-Bus is closer to an [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call "Remote procedure call") mechanism than to a classic IPC mechanism, with its own type definition system and its own [marshaling](https://en.wikipedia.org/wiki/Marshalling_(computer_science) "Marshalling (computer science)").[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/D-Bus_method_invocation.svg/220px-D-Bus_method_invocation.svg.png)](https://en.wikipedia.org/wiki/File:D-Bus_method_invocation.svg)

[](https://en.wikipedia.org/wiki/File:D-Bus_method_invocation.svg "Enlarge")

Example of one-to-one request-response message exchange to invoke a method over D-Bus. Here the client process invokes the SetFoo() method of the /org/example/object1 object from the service process named org.example.foo (or `:1.14`) in the bus.

The bus supports two modes of interchanging messages between a client and a service process[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5):

-   One-to-one [request-response](https://en.wikipedia.org/wiki/Request-response "Request-response"): This is the way for a client to invoke an object's method. The client sends a message to the service process exporting the object, and the service in turn replies with a message back to the client process.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) The message sent by the client must contain the object path, the name of the invoked method (and optionally the name of its interface), and the values of the input parameters (if any) as defined by the object's selected interface. The reply message carries the result of the request, including the values of the output parameters returned by the object's method invocation, or _exception_ information if there was an error.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)
-   [Publish/subscribe](https://en.wikipedia.org/wiki/Publish/subscribe "Publish/subscribe"): This is the way for an object to announce the occurrence of a signal to the interested parties. The object's service process broadcasts a message that the bus passes only to the connected clients subscribed to the object's signal.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) The message carries the object path, the name of the signal, the interface to which the signal belongs, and also the values of the signal's parameters (if any). The communication is one-way: there are no response messages to the original message from any client process, since the sender knows neither the identities nor the number of the recipients.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

Every D-Bus message consists of a header and a body.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) The header is formed by several fields that identify the type of message, the sender, as well as information required to deliver the message to its recipient (destination bus name, object path, method or signal name, interface name, etc.).[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) The body contains the data payload that the receiver process interprets—for instance the input or output arguments. All the data is encoded in a well known binary format called the _wire format_ which supports the [serialization](https://en.wikipedia.org/wiki/Serialization "Serialization") of various types, such as integers and floating-point numbers, strings, compound types, and so on,[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) also referred to as  [marshaling](https://en.wikipedia.org/wiki/Marshalling_(computer_science) "Marshalling (computer science)").

The D-Bus specification defines the [wire protocol](https://en.wikipedia.org/wiki/Wire_protocol "Wire protocol"): how to build the D-Bus messages to be exchanged between processes within a D-Bus connection. However, it does not define the underlying transport method for delivering these messages.

## Internals

Most existing D-Bus implementations follow the architecture of the reference implementation. This architecture consists of two main components:[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)

-   a point-to-point communications [library](https://en.wikipedia.org/wiki/Library_(computing) "Library (computing)") that implements the D-Bus [wire protocol](https://en.wikipedia.org/wiki/Wire_protocol "Wire protocol") in order to exchange messages between two processes. In the reference implementation this library is **libdbus**. In other implementations libdbus may be wrapped by another higher-level library, language binding, or entirely replaced by a different standalone implementation that serves the same purpose.[[19]](https://en.wikipedia.org/wiki/D-Bus#cite_note-what_is_dbus_q1-19) This library only supports one-to-one communications between two processes.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)
-   [![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Dbus-daemon.svg/220px-Dbus-daemon.svg.png)](https://en.wikipedia.org/wiki/File:Dbus-daemon.svg)
    
    [](https://en.wikipedia.org/wiki/File:Dbus-daemon.svg "Enlarge")
    
    A dbus-daemon process acting as a D-Bus message bus daemon. Every process connected to the bus keeps one D-Bus connection with it.
    
    a special [daemon process](https://en.wikipedia.org/wiki/Daemon_(computing) "Daemon (computing)") that plays the bus role and to which the rest of the processes connect using any D-Bus point-to-point communications library. This process is also known as the _message bus daemon_,[[18]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Love_2005-18) since it is responsible for routing messages from any process connected to the bus to another. In the reference implementation this role is performed by **dbus-daemon**, which itself is built on top of libdbus. Another implementation of the message bus daemon is **dbus-broker**, which is built on top of **sd-bus**.

[![Process A and B have a one-to-one D-Bus connection between them over a Unix domain socket](https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/D-Bus_communications_architecture_-_simple.svg/200px-D-Bus_communications_architecture_-_simple.svg.png)](https://en.wikipedia.org/wiki/File:D-Bus_communications_architecture_-_simple.svg)

Process A and B have a one-to-one D-Bus connection using libdbusover a Unix domain socket. They can use it to exchange messages directly.[[20]](https://en.wikipedia.org/wiki/D-Bus#cite_note-what_is_dbus_q2-20) In this scenario bus names are not required.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

[![Process A and B have both a one-to-one D-Bus connection with a dbus-daemon process over a Unix domain socket](https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/D-Bus_communications_architecture_-_message_bus.svg/200px-D-Bus_communications_architecture_-_message_bus.svg.png)](https://en.wikipedia.org/wiki/File:D-Bus_communications_architecture_-_message_bus.svg)

Process A and B both connected to a dbus-daemon using libdbusover a Unix domain socket. They can exchange messages sending them to the message bus process, which in turn will deliver the messages to the appropriate process. In this scenario bus names are mandatory to identify the destination process.

The libdbus library (or its equivalent) internally uses a native lower-level IPC mechanism to transport the required D-Bus messages between the two processes in both ends of the D-Bus connection. D-Bus specification does not mandate which particular IPC transport mechanisms should be available to use, as it is the communications library that decides what transport methods it supports. For instance, in Unix-like operating systems such as Linux libdbus typically uses [Unix domain sockets](https://en.wikipedia.org/wiki/Unix_domain_socket "Unix domain socket") as the underlying transport method, but it also supports [TCP sockets](https://en.wikipedia.org/wiki/Transmission_Control_Protocol "Transmission Control Protocol").[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

The communications libraries of both processes must agree on the selected transport method and also on the particular channel used for their communication. This information is defined by what D-Bus calls an _address_.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) Unix-domain sockets are [filesystem](https://en.wikipedia.org/wiki/Filesystem "Filesystem") objects, and therefore they can be identified by a filename, so a valid address would be `unix:path=/tmp/.hiddensocket`.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) Both processes must pass the same address to their respective communications libraries to establish the D-Bus connection between them. An address can also provide additional data to the communications library in the form of comma-separated `key=value` pairs.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6)[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) This way, for example, it can provide authentication information to a specific type of connection that supports it.

When a message bus daemon like dbus-daemon is used to implement a D-Bus bus, all processes that want to connect to the bus must know the _bus address_, the address by which a process can establish a D-Bus connection to the central message bus process.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5)[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) In this scenario, the message bus daemon selects the bus address and the remainder processes must pass that value to their corresponding libdbus or equivalent libraries. dbus-daemondefines a different bus address for every bus instance it provides. These addresses are defined in the daemon's configuration files.

Two processes can use a D-Bus connection to exchange messages directly between them,[[20]](https://en.wikipedia.org/wiki/D-Bus#cite_note-what_is_dbus_q2-20) but this is not the way in which D-Bus is normally intended to be used. The usual way is to always use a message bus daemon (i.e. dbus-daemon) as a communications central point to which each process should establish its point-to-point D-Bus connection. When a process—client or service—sends a D-Bus message, the message bus process receives it in the first instance and delivers it to the appropriate recipient. The message bus daemon may be seen as a hub or router in charge of getting each message to its destination by repeating it through the D-Bus connection to the recipient process.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16) The recipient process is determined by the destination bus name in the message's header field,[[15]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_spec-15) or by the subscription information to signals maintained by the message bus daemon in the case of signal propagation messages.[[6]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Cocagne_2012-6) The message bus daemon can also produce its own messages as a response to certain conditions, such as an error message to a process that sent a message to a nonexistent bus name.[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

dbus-daemon improves the feature set already provided by D-Bus itself with additional functionality. For example, _service activation_ allows automatic starting of services when needed—when the first request to any bus name of such service arrives at the message bus daemon.[[5]](https://en.wikipedia.org/wiki/D-Bus#cite_note-intro_dbus-5) This way, service processes neither need to be launched during the [system initialization](https://en.wikipedia.org/wiki/Init "Init") or user initialization stage nor need they consume memory or other resources when not being used. This feature was originally implemented using [setuid](https://en.wikipedia.org/wiki/Setuid "Setuid") helpers,[[21]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_service_activation-21) but nowadays it can also be provided by [systemd](https://en.wikipedia.org/wiki/Systemd "Systemd")'s service activation framework.[_[citation needed](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed "Wikipedia:Citation needed")_] Service activation is an important feature that facilitates the management of the process lifecycle of services (for example when a desktop component should start or stop).[[16]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_tut-16)

## History and adoption

D-Bus was started in 2002 by Havoc Pennington, Alex Larsson ([Red Hat](https://en.wikipedia.org/wiki/Red_Hat "Red Hat")) and Anders Carlsson.[[8]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Palmieri_2005-8) The version 1.0—considered [API](https://en.wikipedia.org/wiki/Application_Program_Interface "Application Program Interface") stable—was released in November 2006.[[22]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_1.0-22)[[23]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Molkentin_2006-23)

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Free_and_open-source-software_display_servers_and_UI_toolkits.svg/350px-Free_and_open-source-software_display_servers_and_UI_toolkits.svg.png)](https://en.wikipedia.org/wiki/File:Free_and_open-source-software_display_servers_and_UI_toolkits.svg)

[](https://en.wikipedia.org/wiki/File:Free_and_open-source-software_display_servers_and_UI_toolkits.svg "Enlarge")

The dbus-daemon plays a significant role in modern Linux [graphical desktop environments](https://en.wikipedia.org/wiki/Graphical_desktop_environment "Graphical desktop environment").

Heavily influenced by the [DCOP](https://en.wikipedia.org/wiki/Desktop_communication_protocol "Desktop communication protocol") system used by versions 2 and 3 of [KDE](https://en.wikipedia.org/wiki/KDE "KDE"), D-Bus has replaced DCOP in the [KDE 4](https://en.wikipedia.org/wiki/KDE_4 "KDE 4") release.[[23]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Molkentin_2006-23)[[24]](https://en.wikipedia.org/wiki/D-Bus#cite_note-KDE_dbus_intro-24) An implementation of D-Bus supports most [POSIX](https://en.wikipedia.org/wiki/POSIX "POSIX") operating systems, and a port for [Windows](https://en.wikipedia.org/wiki/Windows "Windows") exists. It is used by [Qt](https://en.wikipedia.org/wiki/Qt_(toolkit) "Qt (toolkit)") 4 and later by [GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME"). In GNOME it has gradually replaced most parts of the earlier [Bonobo](https://en.wikipedia.org/wiki/Bonobo_(GNOME) "Bonobo (GNOME)") mechanism. It is also used by [Xfce](https://en.wikipedia.org/wiki/Xfce "Xfce").

One of the earlier adopters was the (nowadays deprecated) [Hardware Abstraction Layer](https://en.wikipedia.org/wiki/HAL_(software) "HAL (software)"). HAL used D-Bus to export information about hardware that has been added to or removed from the computer.[[8]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Palmieri_2005-8)

The usage of D-Bus is steadily expanding beyond the initial scope of desktop environments to cover an increasing amount of system services. For instance, the [NetworkManager](https://en.wikipedia.org/wiki/NetworkManager "NetworkManager") network daemon, [BlueZ](https://en.wikipedia.org/wiki/BlueZ "BlueZ") bluetooth stack and [PulseAudio](https://en.wikipedia.org/wiki/PulseAudio "PulseAudio") sound server use D-Bus to provide part or all of their services. [systemd](https://en.wikipedia.org/wiki/Systemd "Systemd") uses the D-Bus wire protocol for communication between [systemctl](https://en.wikipedia.org/wiki/Systemd#Core_components_and_libraries "Systemd") and systemd, and is also promoting traditional system daemons to D-Bus services, such as [logind](https://en.wikipedia.org/wiki/Logind "Logind").[[25]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Poettering_2015_q2-25) Another heavy user of D-Bus is [Polkit](https://en.wikipedia.org/wiki/Polkit "Polkit"), whose policy authority daemon is implemented as a service connected to the system bus.[[26]](https://en.wikipedia.org/wiki/D-Bus#cite_note-polkit_doc-26)

## Implementations

### libdbus

Although there are several implementations of D-Bus, the most widely used is the reference implementation _libdbus_, developed by the same freedesktop.org project that designed the specification. However, libdbus is a low-level implementation that was never meant to be used directly by application developers, but as a reference guide for other reimplementations of D-Bus (such as those included in standard libraries of desktop environments, or in [programming language](https://en.wikipedia.org/wiki/Programming_language "Programming language") bindings). The freedesktop.org project itself recommends applications authors to "use one of the higher level bindings or implementations" instead.[[27]](https://en.wikipedia.org/wiki/D-Bus#cite_note-what_is_dbus_q3-27) The predominance of libdbus as the most used D-Bus implementation caused the terms "D-Bus" and "libdbus" to be often used interchangeably, leading to confusion.

### GDBus

GDBus[[9]](https://en.wikipedia.org/wiki/D-Bus#cite_note-gdbus-9) is an implementation of D-Bus based on [GIO streams](https://en.wikipedia.org/wiki/GIO_(software) "GIO (software)") included in [GLib](https://en.wikipedia.org/wiki/GLib "GLib"), aiming to be used by [GTK+](https://en.wikipedia.org/wiki/GTK%2B "GTK+") and [GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME"). GDBus is not a wrapper of libdbus, but a complete and independent reimplementation of the D-Bus specification and protocol.[[28]](https://en.wikipedia.org/wiki/D-Bus#cite_note-gdbus2-28) [MATE Desktop](https://en.wikipedia.org/wiki/MATE_(software) "MATE (software)")[[29]](https://en.wikipedia.org/wiki/D-Bus#cite_note-29) and [Xfce](https://en.wikipedia.org/wiki/Xfce "Xfce") (version 4.14), which are also based on GTK+ 3, also use GDBus.

### QtDBus

QtDBus[[10]](https://en.wikipedia.org/wiki/D-Bus#cite_note-qtdbus-10) is an implementation of D-Bus included in the [Qt library](https://en.wikipedia.org/wiki/Qt_(software) "Qt (software)") since its version 4.2. This component is used by [KDE](https://en.wikipedia.org/wiki/KDE "KDE") applications, libraries and components to access the D-Bus services available in a system.

### sd-bus

In 2013, the [systemd](https://en.wikipedia.org/wiki/Systemd "Systemd") project rewrote libdbus in an effort to simplify the code,[[30]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Poettering_2013-30) but it also resulted in a significant increase of the overall D-Bus performance. In preliminary benchmarks, [BMW](https://en.wikipedia.org/wiki/BMW "BMW") found that the systemd's D-Bus library increased performance by 360%.[[31]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Edge_2013-31) By version 221 of [systemd](https://en.wikipedia.org/wiki/Systemd "Systemd"), the sd-bus [API](https://en.wikipedia.org/wiki/Application_programming_interface "Application programming interface") was declared stable.[[32]](https://en.wikipedia.org/wiki/D-Bus#cite_note-systemd_221-32)

### libnih-dbus

The libnih project provides a light-weight "standard library" of C support for D-Bus. Additionally, it has good support for cross compiling.

### kdbus

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Simplified_Structure_of_the_Linux_Kernel.svg/220px-Simplified_Structure_of_the_Linux_Kernel.svg.png)](https://en.wikipedia.org/wiki/File:Simplified_Structure_of_the_Linux_Kernel.svg)

[](https://en.wikipedia.org/wiki/File:Simplified_Structure_of_the_Linux_Kernel.svg "Enlarge")

kdbus is implemented as a character device driver.[[33]](https://en.wikipedia.org/wiki/D-Bus#cite_note-33)[[34]](https://en.wikipedia.org/wiki/D-Bus#cite_note-34) All communication between processes take place over special character device nodes in `/dev/kdbus` (cf. [devfs](https://en.wikipedia.org/wiki/Devfs "Devfs")).

_kdbus_ was a project that aimed to reimplement D-Bus as a kernel-mediated peer-to-peer [inter-process communication](https://en.wikipedia.org/wiki/Inter-process_communication "Inter-process communication") mechanism. Beside performance improvements, kdbus would have advantages arising from other [Linux kernel](https://en.wikipedia.org/wiki/Linux_kernel "Linux kernel") features such as [namespaces](https://en.wikipedia.org/wiki/Linux_namespaces "Linux namespaces") and auditing,[[31]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Edge_2013-31)[[35]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Corbet_2014-35)security from the kernel mediating, closing race conditions, and allowing D-Bus to be used during boot and shutdown (as needed by systemd).[[36]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Kroah-Hartman_2015-36)kdbus inclusion in the Linux kernel proved controversial,[[37]](https://en.wikipedia.org/wiki/D-Bus#cite_note-Corbet_2015-37) and was dropped in favor of [BUS1](https://bus1.org/), as a more generic [inter-process communication](https://en.wikipedia.org/wiki/Inter-process_communication "Inter-process communication").[[38]](https://en.wikipedia.org/wiki/D-Bus#cite_note-38)

### zbus

[zbus](https://gitlab.freedesktop.org/dbus/zbus) is a native Rust library for D-Bus. Its main strengths are its [macros](https://docs.rs/zbus_macros/latest/zbus_macros/) that make communication with services and implementation of services, extremely easy and simple.

### Protocol::DBus

[Protocol::DBus](https://metacpan.org/pod/Protocol::DBus) is a native Perl D-Bus client.

### Language bindings

Several programming language [bindings](https://en.wikipedia.org/wiki/Language_binding "Language binding") for D-Bus have been developed,[[39]](https://en.wikipedia.org/wiki/D-Bus#cite_note-dbus_bindings-39) such as those for [Java](https://en.wikipedia.org/wiki/Java_(programming_language) "Java (programming language)"), [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language) "C Sharp (programming language)"), [Ruby](https://en.wikipedia.org/wiki/Ruby_(programming_language) "Ruby (programming language)"), and [Perl](https://en.wikipedia.org/wiki/Perl_(programming_language) "Perl (programming language)").

## See also

-   ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Free_and_open-source_software_logo_%282009%29.svg/28px-Free_and_open-source_software_logo_%282009%29.svg.png)[Free and open-source software portal](https://en.wikipedia.org/wiki/Portal:Free_and_open-source_software "Portal:Free and open-source software")

-   [Linux on the desktop](https://en.wikipedia.org/wiki/Linux_on_the_desktop "Linux on the desktop")
-   [Common Language Infrastructure](https://en.wikipedia.org/wiki/Common_Language_Infrastructure "Common Language Infrastructure")
-   [Common Object Request Broker Architecture](https://en.wikipedia.org/wiki/Common_Object_Request_Broker_Architecture "Common Object Request Broker Architecture")
-   [Component Object Model](https://en.wikipedia.org/wiki/Component_Object_Model "Component Object Model")
-   [Distributed Component Object Model](https://en.wikipedia.org/wiki/Distributed_Component_Object_Model "Distributed Component Object Model")
-   [Foreign function interface](https://en.wikipedia.org/wiki/Foreign_function_interface "Foreign function interface")
-   [Java remote method invocation](https://en.wikipedia.org/wiki/Java_remote_method_invocation "Java remote method invocation")
-   [Remote procedure call](https://en.wikipedia.org/wiki/Remote_procedure_call "Remote procedure call")
-   [XPCOM](https://en.wikipedia.org/wiki/XPCOM "XPCOM")