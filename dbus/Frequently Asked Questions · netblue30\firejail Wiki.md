---
title: "Frequently Asked Questions · netblue30firejail Wiki"
layout: post
---


# Frequently Asked Questions · netblue30\firejail Wiki
<a style="text-decoration:underline" href="https://github.com/netblue30/firejail">Frequently Asked Questions · netblue30\firejail Wiki</a><br>

Technology
----------

-   [Why on earth should I use Firejail?](https://github.com/netblue30/firejail#why-on-earth-should-i-use-firejail)
-   [How does it compare with AppArmor?](https://github.com/netblue30/firejail#how-does-it-compare-with-apparmor)
-   [How does it compare with Docker, LXC, nspawn, bubblewrap ?](https://github.com/netblue30/firejail#how-does-it-compare-with-docker-lxc-nspawn-bubblewrap)
-   [What is the overhead of the sandbox?](https://github.com/netblue30/firejail#what-is-the-overhead-of-the-sandbox)
-   [Can I sandbox a full OS?](https://github.com/netblue30/firejail#can-i-sandbox-a-full-os)

Applications
------------

-   [Firefox doesn’t open in a new sandbox.](https://github.com/netblue30/firejail#firefox-doesnt-open-in-a-new-sandbox-instead-it-opens-a-new-tab-in-an-existing-firefox-instance)
-   [How do I run two instances of Firefox?](https://github.com/netblue30/firejail#how-do-i-run-two-instances-of-firefox)
-   [How do I run Tor browser?](https://github.com/netblue30/firejail#how-do-i-run-tor-browser)
-   [How do I run VLC in a sandbox without network access?](https://github.com/netblue30/firejail#how-do-i-run-vlc-in-a-sandbox-without-network-access)
-   [Can you sandbox Steam games and Skype?](https://github.com/netblue30/firejail#can-you-sandbox-steam-games-and-skype)
-   [How do I enable plasma browser integration in firefox?](https://github.com/netblue30/firejail#how-do-i-enable-plasma-browser-integration-in-firefox)
-   [How do I integrate firecfg into my package manager?](https://github.com/netblue30/firejail#how-do-i-integrate-firecfg-in-my-packagemanager)
-   [How can I enable fcitx?](https://github.com/netblue30/firejail#how-can-i-enable-fcitx)

Usage
-----

-   [How do I undo `firecfg`?](https://github.com/netblue30/firejail#how-do-i-undo-firecfg)
-   [How do I bypass firejail on a one-off basis?](https://github.com/netblue30/firejail#how-do-i-bypass-firejail-on-a-one-off-basis)

Known Problems
--------------

-   [OverlayFS features disabled for Linux kernel 4.19 and newer](https://github.com/netblue30/firejail#overlayfs-features-disabled-for-linux-kernel-419-and-newer)
-   [A program isn't firejailed](https://github.com/netblue30/firejail#a-program-isnt-firejailed)
-   [RTNETLINK error using "--net" option](https://github.com/netblue30/firejail#RTNETLINK-error)
-   [PulseAudio 7.0/8.0 issue](https://github.com/netblue30/firejail#pulseaudio-7080-issue)
-   [Browser mailto and mail programs attachments do not work](https://github.com/netblue30/firejail#Browser-mailto-and-mail-programs-attachments-do-not-work)
-   [Cannot open hyperlink with web browser using another application](https://github.com/netblue30/firejail#cannot-open-hyperlink-with-web-browser-using-another-application)
-   [Firefox 60 problems](https://github.com/netblue30/firejail#firefox-60-problems)
-   [LibreOffice on Ubuntu 18.04](https://github.com/netblue30/firejail#libreoffice-on-ubuntu-1804)
-   [Cannot install new software while Firejail is running](https://github.com/netblue30/firejail#cannot-install-new-software-while-firejail-is-running)
-   [Cannot connect to ibus-daemon in a new network namespace](https://github.com/netblue30/firejail#cannot-connect-to-ibus-daemon-in-a-new-network-namespace)
-   [Cannot kill firejailed program](https://github.com/netblue30/firejail#cannot-kill-firejailed-program)
-   [Firefox crashing on Netflix, AMDGPU PRO, Nvidia closed source drivers](https://github.com/netblue30/firejail#firefox-crashing-on-netflix-amdgpu-pro-nvidia-closed-source-drivers)
-   [I’ve noticed the title bar in Firefox shows “(as superuser)”, is this normal?](https://github.com/netblue30/firejail#ive-noticed-the-title-bar-in-firefox-shows-as-superuser-is-this-normal)
-   [Media issues with firejail 0.9.62 under Arch](https://github.com/netblue30/firejail#media-issues-with-firejail-0962-under-arch)

Technology
----------

Why on earth should I use Firejail?
-----------------------------------

Some existing Linux security solutions are easily defeated from internal and/or external threats. Other solutions are just too difficult to put in place. Firejail’s approach is radically different.

For us, the user always comes first. We manage to keep the learning curve down. Actually, most of the time you don’t need to learn anything, just prefix your application with “firejail” and run it. This makes Firejail ideal for the regular, not-so-skilled home user.

We use the latest Linux kernel security features, such as namespaces and seccomp-bpf. In our view these features are mature, and have been extensively tested in the market place by products such as Google Chrome or Docker.

How does it compare with AppArmor?
----------------------------------

Firejail uses private mount namespaces to achieve similar access controls compared to AppArmor and capability restrictions are also similar. In addition to those, Firejail can set up system call filtering with seccomp and restrict networking. Unlike Firejail, AppArmor can restrict mapping of files to memory. AppArmor's available features can vary widely across kernel versions and distros. Firejail also is executing SUID, elevating user's privileges temporarily, e.g. for setting up mounts, which can be an attack vector.

It is recommended to only use AppArmor OR Firejail but not both at the same time for the same app. If you set a tight AppArmor profile already then you may want to stick with that.

In general to use both for a particular app, you will need to modify the pre-packaged app profiles for both AppArmor and Firejail to get them to work together. You need to add more privileges in AppArmor than is needed without Firejail, and you need to use less Firejail features otherwise it would be broken with AppArmor. Many times they will cancel eachother out rather than be complementary. We have multiple reports about broken apps when AppArmor and Firejail are used at the same time.

Keep in mind that AppArmor is mandatory when enabled while Firejail can be easily circumvented (intentionally or not). It should be possible to use Firejail just for seccomp and network control, those can not conflict with prepackaged AppArmor. See [#2248](https://github.com/netblue30/firejail/issues/2248) for a discussion on this.

How does it compare with Docker, LXC, nspawn, bubblewrap?
---------------------------------------------------------

Docker, LXC and nspawn are container managers. A container is a separate root filesystem. The software runs in this new filesystem. Firejail and bubblewrap are security sandboxes. Firejail works on your existing filesystem. It is modeled after the security sandbox distributed with Google Chrome.

Containers and sandboxes use the same Linux kernel technology, Linux namespaces. The developer focus is different. Containers target the virtualization market, while sandboxes focus on application security.

(2018-05-28) Docker and Firejail use similar features like namespaces, seccomp, capabilities. This means Firejail is unable to create its own sandbox when run inside docker. See [#1956](https://github.com/netblue30/firejail/issues/1956#issuecomment-391660354).

The closest alternative to Firejail is bubblewrap. The technologies and capabilities used are roughly the same (Linux namespaces, bind mounts, capabilities, seccomp filters) but the design philosophies are different: simplicity over features. Bubblewrap chooses to provide only a set of basic tools to construct the sandbox, while Firejail has dedicated support for many applications with its application profiles and tailored exceptions in Firejail itself. One example is that bubblewrap always enforces `NO_NEW_PRIVS` `prctl(2)`. This ensures that child processes cannot acquire new privileges using `execve(2)` and in particular, this means that calling a set-uid binary (or one with file capabilities) does not result in an increase of privileges. With Firejail this can be chosen, so it's actually possible to weaken security when set-uid functionality is preferred, though this also means that set-uid programs can be sandboxed.

Comparison of Firejail features vs. bubblewrap:

-   very detailed and easy to use profiles for hundreds of applications, especially highly important browsers, messaging apps and media players
-   automatic profile generation with `firejail --build`
-   automatic desktop integration with `firecfg`
-   more extensive network support including firewalling, protocol filtering, tunneling, DNS
-   AppArmor and SELinux support
-   D-bus filtering
-   AppImage support
-   resource limits (`rlimit-*=`)
-   X11 sandboxing
-   construct new file systems easily with `private-bin`, `private-etc`, `private-lib` etc.
-   easy to use flags like `novideo`, `no3d`, `nodvd`, `notv`, `nogroups`, `noroot`, `nou2f`, `noexec` mount flag
-   easier seccomp filtering (compared to loading cBPF programs) with deny-listing and allow-listing, 32-bit support, `memory-deny-write-execute`, `protocol`

What is the overhead of the sandbox?
------------------------------------

The sandbox itself is a very small process. The setup is fast, typically several milliseconds. After the application is started, the sandbox process goes to sleep and doesn’t consume any resources. All the security features are implemented inside the kernel, and run at kernel speed.

Can I sandbox a full OS?
------------------------

The idea so far was to target specific applications, such as Firefox and Chromium, or closed source apps like Steam and Skype. We are moving in the direction of sandboxing a full OS, but it will take some time to get there.

Usage
-----

How do I undo firecfg?
----------------------

Run `sudo firecfg --clean`.

How do I bypass firejail on a one-off basis?
--------------------------------------------

`firecfg` works by simply creating symlinks in '/usr/local/bin' to the `/usr/bin/firejail` binary, which will then search for the app's binary (usually in `/usr/bin`). Because `/usr/local/bin/` comes before `/usr/bin` in `$PATH`, running `<app>` (e.g. `vlc`) will run `firejail`, so the app will start jailed by default. If you want to start it without a jail, run `/usr/bin/vlc`.

For programs you never want jailed, delete the program link from `/usr/local/bin` (`sudo rm /usr/local/bin/vlc`).

See also:

-   [https://github.com/netblue30/firejail/issues/2097](https://github.com/netblue30/firejail/issues/2097)
-   [https://github.com/netblue30/firejail/issues/3665#issuecomment-707689049](https://github.com/netblue30/firejail/issues/3665#issuecomment-707689049)

Applications
------------

Firefox doesn’t open in a new sandbox. Instead, it opens a new tab in an existing Firefox instance
--------------------------------------------------------------------------------------------------

By default, Firefox browser uses a single process to handle multiple windows. When you start the browser, if another Firefox process is already running, the existing process opens a new tab or a new window. Make sure Firefox is not already running when you start it in Firejail sandbox.

How do I run two instances of Firefox?
--------------------------------------

Open `about:profiles` and create the new profile NAME.

Start the first Firefox instance as usual:

Then, start the second sandbox:

```
    $ firejail firefox -P "NAME" --no-remote

```

How do I run tor browser?
-------------------------

See [Tor Browser home install](https://github.com/netblue30/firejail/wiki/Sandboxing-Binary-Software#tor-browser-home-install).

How do I run VLC in a sandbox without network access?
-----------------------------------------------------

`--net=none` command line switch installs a new TCP/IP stack in your sandbox. The stack is not connected to any external interface. For the programs running inside, the sandbox looks like a computer without any Ethernet interface.

```
    $ firejail --net=none vlc

```

The best way to handle the command line switch is to place it in a `.local` file in `~/.config/firejail`. Create a `vlc.local` text file in this directory, with the following content:

```
    $ cat ~/.config/firejail/vlc.local
    net none

```

See also: [https://github.com/netblue30/firejail/wiki/Creating-overrides](https://github.com/netblue30/firejail/wiki/Creating-overrides)

Can you sandbox Steam games and Skype?
--------------------------------------

Support for Steam, Wine and Skype has been around since version 0.9.34. Quite a number of other closed-source programs are supported.

Running `ls /etc/firejail/*.profile` will list all the security profiles distributed with Firejail. Applications that do not have a profile will use the default profile (`/etc/firejail/default.profile`).

How do I enable plasma browser integration in Firefox?
------------------------------------------------------

Create a new file `~/.config/firejail/firefox.local` and add `ignore nodbus`. If you have `private-bin` enabled, you must also add `private-bin plasma-browser-integration-host`.

How do I integrate firecfg into my package manager?
---------------------------------------------------

Arch Linux: [https://wiki.archlinux.org/index.php/Firejail#Using_Firejail_by_default](https://wiki.archlinux.org/index.php/Firejail#Using_Firejail_by_default)

Debian: [https://github.com/netblue30/firejail/issues/2226#issuecomment-434249669](https://github.com/netblue30/firejail/issues/2226#issuecomment-434249669)

Fedora: Install `python3-dnf-plugin-post-transaction-actions` and create `/etc/dnf/plugins/post-transaction-actions.d/firecfg.action` with this content:

```
/usr/bin/*:any:firecfg
/usr/sbin/*:any:firecfg
/usr/share/applications/*:any:firecfg

```

How can I enable fcitx?
-----------------------

Depending on the dbus-policy of the profile you need to add different command to its local. If the dbus-policy is set to `filter`, it is enough to add `dbus-user.talk org.freedesktop.portal.Fcitx`. If it is set to none, you need to add

```
dbus-user filter
dbus-user.talk org.freedesktop.portal.Fcitx
ignore dbus-user none

```

Known Problems
--------------

OverlayFS features disabled for Linux kernel 4.19 and newer
-----------------------------------------------------------

Something changed in the kernel code, and we are not able to mount / filesystem in overlay. We are working on a fix.

A program isn't firejailed
--------------------------

`firejail --list` does not show the running program to be inside a Firejail sandbox. First make sure you have run `firecfg` with `sudo`. If you run this as root without sudo, it will not fix your `.desktop` files. If this did not work, create a symlink manually (`ln -s /usr/bin/firejail /usr/local/bin/PROGRAM`). Additionally, adding an alias in your shell or modifying the `Exec` line in your `.desktop` file will work, too. Only edit the `.desktop` file if you know what you are doing. If your program is installed under `/opt` you need to use `firejail /opt/foo/bar` in the terminal/`.desktop` file for example.

RTNETLINK error
---------------

`firejail --net=eth1 firefox` yields in `RTNETLINK answers: Operation not supported`. Missing modules, kernel update without reboot are cause. Look at issue [#2046](https://github.com/netblue30/firejail/issues/2046) or [#2387](https://github.com/netblue30/firejail/issues/2387).

PulseAudio 7.0/8.0 issue
------------------------

The srbchannel IPC mechanism, introduced in PulseAudio 6.0, was enabled by default in release 7.0. Many Linux users are reporting sound problems when running applications in Firejail sandbox. It affects among others Ubuntu 16.04 and Mint users. This problem was fixed PulseAudio version 9.0. Run `firecfg --fix` in a terminal or apply the following configuration to mask the problem:

```
    $ mkdir -p ~/.config/pulse
    $ cd ~/.config/pulse
    $ cp /etc/pulse/client.conf .
    $ echo "enable-shm = no" >> client.conf

```

A logout/login is required for the changes to take effect.

If you have problems with PulseAudio 9.x use the previous fix, or configure `enable-memfd = yes` in `/etc/pulse/daemon.conf`.

Browser mailto and mail programs attachments do not work
--------------------------------------------------------

Mailto usually uses dbus and is thus disabled by default. [Create a local override](https://github.com/netblue30/firejail/wiki/Creating-overrides#ignore-a-specific-directive) and add `ignore nodbus` to the Firefox or Chromium override file as in [#2795](https://github.com/netblue30/firejail/issues/2795) and [#1718](https://github.com/netblue30/firejail/issues/1718).

Mail programs do not need to interact with the entirety of the filesystem. You may want to [allow access](https://github.com/netblue30/firejail/wiki/Creating-overrides#allow-access-to-a-file-or-directory) to a single directory for attachments and other downloads.

Cannot open hyperlink with web browser using another application
----------------------------------------------------------------

It is recommended to copy-paste links from with application with the hyperlink into an already running web browser. This will always be the safest bet, albeit not very user-friendly. See [#2228](https://github.com/netblue30/firejail/issues/2228) and [#2047](https://github.com/netblue30/firejail/issues/2047)

Firefox 60 problems
-------------------

Firefox 60 doesn’t work with Firejail version 0.9.52 or older. Patched security profiles for are available for Firejail versions 0.9.38.x (LST) and 0.9.52. You can find them in our [profile fixes section](https://github.com/netblue30/firejail/tree/master/etc-fixes). Another option is to install a [newer version of Firejail](https://github.com/netblue30/firejail#installing).

LibreOffice on Ubuntu 18.04
---------------------------

LibreOffice crashes when sandboxed with Firejail version 0.9.52 in Ubuntu 18.04. A patched security profile for Firejail 0.9.52 is available in our [profile fixes section](https://github.com/netblue30/firejail/tree/master/etc-fixes). Another option is to install a [newer version of Firejail](https://github.com/netblue30/firejail#installing).

Cannot install new software while Firejail is running
-----------------------------------------------------

Files blacklisted in a running jail cannot be removed from outside of jail. This causes a serious inconvenience when using Firejail with long time running processes. For example, preventing user from updating system normally, as files like `/bin/su`, `/bin/mount`, `/usr/bin/sudo` are blacklisted by default. Also, admin commands for adding users and groups will fail.

Firejail implements blacklisting by mounting an empty, read-only file or directory on top of the original file. The kernel, at least the older kernels, will refuse to delete the file because it is a mount point in some other place in the system.

The problem is fixed in Linux kernels 3.18 or newer.

Cannot connect to ibus-daemon in a new network namespace
--------------------------------------------------------

`ibus-daemon` is used to change the system language, for example to switch between English (US) input and Japanese inputs. In a sandbox using a new network namespace ibus-daemon socket is disabled and keyboard switching capability is lost.

Cannot kill firejailed program
------------------------------

Check namespace support like for killall (command option `--ns`) and adapt the command for `firejail --tree` output.

Firefox crashing on Netflix, AMDGPU PRO, Nvidia closed source drivers
---------------------------------------------------------------------

You should first set `browser-allow-drm` to `yes` in `/etc/firejail/firejail.config` or add `ignore noexec ${HOME}` to your `firefox.local`. If you are using a firejail version older than 0.9.68 and NVIDIA proprietary drivers, you must also set `ignore noroot` in your `firefox.local`.

If this didn't work, try the old solution.

old solution

We are still working on these problems. From what we’ve seen so far, these programs make liberal use of system calls such as chroot and ptrace. These syscalls have no place in regular, well behaved programs, and seccomp kills the application immediately. Workarounds involve disabling seccomp and allowing ptrace utility. Example:

```
    $ firejail --allow-debuggers --ignore=seccomp --ignore=protocol --ignore=noroot --ignore=nogroups --ignore=nonewprivs firefox --no-remote

```

I’ve noticed the title bar in Firefox shows “(as superuser)”, is this normal?
-----------------------------------------------------------------------------

The sandbox process itself runs as root. The application inside the sandbox runs as a regular user. `ps aux | grep firefox` reports Firefox process running as a regular user.

The same problem was seen on other programs as well (VLC, Audacious, Transmission), and it is believed to be a bug in the window manager. You can find a very long discussion on the development site: [https://github.com/netblue30/firejail/issues/258](https://github.com/netblue30/firejail/issues/258)

Media issues with firejail 0.9.62 under Arch
--------------------------------------------

Fix: Add `ld.so.conf,ld.so.conf.d,ld.so.preload` to the `private-etc` line of the profile, you can do this by creating a `.local` file in `~/.config/firejail/PROFILE_NAME.local` with the following content:

```
private-etc ld.so.conf,ld.so.conf.d,ld.so.preload

```

References: [#3147](https://github.com/netblue30/firejail/issues/3147) [#3157](https://github.com/netblue30/firejail/issues/3157) [#3158](https://github.com/netblue30/firejail/issues/3158) [#3150](https://github.com/netblue30/firejail/pull/3150) [`31772d81`](https://github.com/netblue30/firejail/commit/31772d81f534d1537736dbec02098a80544182d9) [`bc337e23`](https://github.com/netblue30/firejail/commit/bc337e2330730e8ed8f2673398b11f41b50ee04f)