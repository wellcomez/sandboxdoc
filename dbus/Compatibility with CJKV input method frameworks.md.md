---
title: "Compatibility with CJKV input method frameworks.md"
layout: post
---


# Compatibility with CJKV input method frameworks
[Compatibility with CJKV input method frameworks](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)

[doc](https://forum.snapcraft.io/c/doc/15)

[Oct 2018](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788 "Oct 2018")

1 / 4 Oct 2018

[(https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)

[Lin-Buo-Ren](https://forum.snapcraft.io/u/Lin-Buo-Ren)


This topic discusses the compatibility of a graphical user interface snaps with the input method frameworks broadly used by the Chinese, Japanese, Korean and Vietnamese etc.(CJKV) users. _If your snap has a graphical interface and is expected to receive text input from these users, you should check it out._

## TL; DR

### User-oriented

-   Currently support for IBus and Fcitx input method frameworks are implemented in snapd and the `desktop-*` parts.
-   The support of the input method framework must be done by the packager, **if a certain snap isn’t compatible with the supported input methods contact the snap maintainer to do an update or package rebuild.**

### Packager-oriented

To make the snap compatible with the input method frameworks, you must:

-   Connect the snap to [the `desktop-legacy` interface](https://forum.snapcraft.io/t/the-desktop-legacy-interface/7782/1)
-   Incorporate one of the [desktop helpers remote part 51](https://github.com/ubuntu/snapcraft-desktop-helpers) and launch the app using the `desktop-launch` launcher

## Preface

Unlike most European text input which only requires the keys on the keyboard, CJKV text input requires _composing_ a character via a series of US layout keyboard keystrokes, for example, in order to input “中文” string using the phonetic, or zhu-yin(注音) input method with a 大千style layout keyboard one must type in `5`, `j`, `/`, and `<space>` sequentially to compose “中”(and every Chinese character that has the same pronunciation with “中”, which is usually selected via a number keystroke), and `j`, `p`, and `6` for “文”.

[![Example Footage of Inputting "中文" Using Zhu-yin Input Method.gif](https://forum-snapcraft-io.s3.dualstack.us-east-1.amazonaws.com/optimized/2X/5/50ad0724696f8c6777e92ca075df636308b2f2c0_2_690x319.gif)

Example Footage of Inputting &quot;中文&quot; Using Zhu-yin Input Method.gif699×324 1.56 MB

](https://forum-snapcraft-io.s3.amazonaws.com/original/2X/5/50ad0724696f8c6777e92ca075df636308b2f2c0.gif "Example Footage of Inputting &quot;中文&quot; Using Zhu-yin Input Method.gif")

This is accomplished via the help of the input method frameworks, like the [Intelligent Input Bus for Linux/Unix(IBus) 13](https://github.com/ibus/ibus/wiki), [Fcitx 11](https://fcitx-im.org/wiki/Fcitx), and [many 7](http://hime-ime.github.io/), [many 6](http://hyperrate.com/dir.php?eid=67) others. Due to the security confinement of snapd additional effort must be made to allow snapped applications communicate with the input method framework running on the host to allow proper text input from CJKV users. **Failure of doing so will cause great frustration to these users.**

## Currently supported input method frameworks

-   IBus
-   Fcitx

## Known issues

### Qt5 application can’t communicate with IBus input method framework

![](https://forum.snapcraft.io/user_avatar/forum.snapcraft.io/lin-buo-ren/40/4612_2.png) [IBus input method not available in Qt5 applications](https://forum.snapcraft.io/t/ibus-input-method-not-available-in-qt5-applications/11712) [snapd](https://forum.snapcraft.io/c/snapd/5)

> In Qt5 application communicate with IBus via DBus calls, which is currently not implemented in [the desktop-legacy security confinement interface](https://github.com/snapcore/snapd/blob/master/interfaces/builtin/desktop_legacy.go). Refer [https://github.com/qt/qtbase/tree/HEAD/src/plugins/platforminputcontexts/ibus](https://github.com/qt/qtbase/tree/HEAD/src/plugins/platforminputcontexts/ibus) for the current implementation.

### Electron-based applications

Currently known has problems using the input methods:

-   VisualStudio Code

### Binary compatibility issues

Currently it’s been known that snaps shipping Gtk immodules may not work in hosts that using newer Gtk libraries.

## How to Help

-   Hack [the AppArmor policy of the `desktop-legacy` interface 51](https://github.com/snapcore/snapd/blob/master/interfaces/builtin/desktop_legacy.go) to expose more input method frameworks to the snap
-   Hack [the `desktop-launch` launcher script 59](https://github.com/ubuntu/snapcraft-desktop-helpers/blob/master/common/desktop-exports) to make necessary runtime changes to make input method frameworks happy

## References

-   [Stage Fcitx frontend by Lin-Buo-Ren · Pull Request #156 · ubuntu/snapcraft-desktop-helpers 47](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156)
-   [Can't use input method in snap apps11](https://forum.snapcraft.io/t/cant-use-input-method-in-snap-apps/4712/33)
-   [Fcitx does not works on some snap apps3](https://forum.snapcraft.io/t/fcitx-does-not-works-on-some-snap-apps/15995/2)
-   [UIM input method support on snap3](https://forum.snapcraft.io/t/uim-input-method-support-on-snap/15913)
-   [Locale/input method issue with veloren snap2](https://forum.snapcraft.io/t/locale-input-method-issue-with-veloren-snap/21345/6)
-   [Slack doesn't support CJKV text input (IBus)2](https://forum.snapcraft.io/t/slack-doesnt-support-cjkv-text-input-ibus/8538)
-   [1 more](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788 "expand more links for this post")
 #### created
    
    ![](https://forum.snapcraft.io/user_avatar/forum.snapcraft.io/lin-buo-ren/40/4612_2.png "林博仁(Buo-ren, Lin)")Oct '18
    
-   [last reply](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788/4)


Fcitx support [are now fixed 188](https://github.com/ubuntu/snapcraft-desktop-helpers/pull/156). 17 DAYS LATER

[](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)

[![](https://forum.snapcraft.io/user_avatar/forum.snapcraft.io/zixia/90/4907_2.png)](https://forum.snapcraft.io/u/zixia)

[zixia](https://forum.snapcraft.io/u/zixia)

[Nov '18](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788/3)

Please rebuild vscode snap package to support this awesome fix!

[](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)

[![](https://forum.snapcraft.io/user_avatar/forum.snapcraft.io/lin-buo-ren/90/4612_2.png)](https://forum.snapcraft.io/u/Lin-Buo-Ren)

[Lin-Buo-Ren](https://forum.snapcraft.io/u/Lin-Buo-Ren)

[Nov '18](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788/4)

Snap package are directly managed by its maintainers, please refer the [issue tracker 102](https://github.com/snapcrafters/vscode/issues) of the vscode snap.

Hello! Looks like you’re enjoying the discussion, but you haven’t signed up for an account yet.

When you create an account, we remember exactly what you’ve read, so you always come right back where you left off. You also get notifications, here and via email, whenever someone replies to you. And you can like posts to share the love. ![heartpulse](https://forum.snapcraft.io/images/emoji/emoji_one/heartpulse.png?v=9 "heartpulse")

Sign Up Remind me tomorrow[no thanks](https://forum.snapcraft.io/t/compatibility-with-cjkv-input-method-frameworks/7788)

