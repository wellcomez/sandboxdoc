---
date: 2023-04-13 13:50
title: xdg-dbus-proxy
tags:
- patch
---

# xdg-dbus-proxy


~~~c

common/flatpak-context-private.h

32, 3: FLATPAK_POLICY_OWN

  

common/flatpak-context.c

239, 17: if (policy == FLATPAK_POLICY_OWN)

444, 36: GPOINTER_TO_INT (value) == FLATPAK_POLICY_OWN)

1345, 59: flatpak_context_set_session_bus_policy (context, value, FLATPAK_POLICY_OWN);

1390, 58: flatpak_context_set_system_bus_policy (context, value, FLATPAK_POLICY_OWN);

  

subprojects/dbus-proxy/dbus-proxy.c

279, 22: policy = FLATPAK_POLICY_OWN;

  

subprojects/dbus-proxy/flatpak-proxy.c

1443, 26
mount_option("PRIV_SEP_OP_BIND_MOUNT");: static Filter *match_all[FLATPAK_POLICY_OWN + 1] = { NULL };

1462, 17: match_all[FLATPAK_POLICY_OWN] = filter_new ("", FALSE, FLATPAK_POLICY_OWN);

1462, 62: filter_new ("", FALSE, FLATPAK_POLICY_OWN);

1731, 21: if (policy == FLATPAK_POLICY_OWN ||

1805, 14: return FLATPAK_POLICY_OWN;

2526, 25: if (policy == FLATPAK_POLICY_OWN ||

  

subprojects/dbus-proxy/flatpak-proxy.h

32, 3: FLATPAK_POLICY_OWN

~~~

  

### filter_matches (Filter *filter,

~~~c

static gboolean

filter_matches (Filter *filter,

FilterTypeMask type,

const char *path,

const char *interface,

const char *member)

~~~

- 针对`own`和`talk` 和filter作比较，path，interface,member 一样,

- 如果`path`，`interface`,`member`为空认为是match

  

## flatpak_proxy_client_get_max_policy_and_matched

- `flatpak_proxy_client_get_max_policy`
	1531, 10: return flatpak_proxy_client_get_max_policy_and_matched (client, source, NULL);
	1. `org.freedesktop.DBus.ListNames`,`org.freedesktop.DBus.ListActivatableNames`
	~~~c
		EXPECTED_REPLY_LIST_NAMES
		1933, 11: if (flatpak_proxy_client_get_max_policy (client, names[i]) >= FLATPAK_POLICY_SEE)
	~~~
	2. `org.freedesktop.DBus.NameHasOwner`,`org.freedesktop.DBus.GetNameOwner`
	~~~c
		1896, 21: name_policy = flatpak_proxy_client_get_max_policy (client, name);
		//validate_arg0_name
	~~~
	3. should_filter_name_owner_changed, `broadcast`
	~~~c
		1980, 7: if (flatpak_proxy_client_get_max_policy (client, name) >= FLATPAK_POLICY_SEE ||
		// should_filter_name_owner_changed
		//broadcast
		//的第一个参数
	~~~
	4. queue_wildcard_initial_name_ops `EXPECTED_REPLY_FAKE_LIST_NAMES`
	~~~c
    2181, 15: flatpak_proxy_client_get_max_policy (client, name) != FLATPAK_POLICY_NONE)
		//queue_wildcard_initial_name_ops
	~~~

- `get_dbus_method_handler`  检查busname `header->destination`
~~~c
1726, 12: policy = flatpak_proxy_client_get_max_policy_and_matched (client, header->destination, &filters);
~~~

- `got_buffer_from_bus` header->sender
~~~c
2527, 20: policy = flatpak_proxy_client_get_max_policy_and_matched (client, header->sender, &filters);
~~~
