---
date: 2023-04-13 13:56
title: flatpak_node
tags:
- flatpak
---


# flatpak_node



## 1. any_filter_matches
~~~c
static gboolean
any_filter_matches (GList         *filters,
                    FilterTypeMask type,
                    const char    *path,
                    const char    *interface,
                    const char    *member)
{
  fnentry
  GList *l;

  for (l = filters; l != NULL; l = l->next)
    {
      Filter *filter = l->data;
      if (filter_matches (filter, type, path, interface, member))
        return TRUE;
    }

  return FALSE;
}
~~~

  

## 2. filter_matches

~~~c
static gboolean
filter_matches (Filter        *filter,
                FilterTypeMask type,
                const char    *path,
                const char    *interface,
                const char    *member)
{
  if (filter->policy < FLATPAK_POLICY_TALK ||
      (filter->types & type) == 0)
    return FALSE;

  if (filter->path)
    {
      if (path == NULL)
        return FALSE;

      if (filter->path_is_subtree)
        {
          gsize filter_path_len = strlen (filter->path);
          if (strncmp (path, filter->path, filter_path_len) != 0 ||
              (path[filter_path_len] != 0 && path[filter_path_len] != '/'))
            return FALSE;
        }
      else if (strcmp (filter->path, path) != 0)
        return FALSE;
    }

  if (filter->interface && g_strcmp0 (filter->interface, interface) != 0)
    return FALSE;

  if (filter->member && g_strcmp0 (filter->member, member) != 0)
    return FALSE;

  return TRUE;
}
~~~
- 对于filter的policy在policy see，none 必须call和broadcast对应
- path，interface,member 一样,
- 如果`path`，`interface`,`member`为空认为是match

  

## 3. flatpak_proxy_client_get_max_policy_and_matched
### 3.1. flatpak_proxy_client_get_max_policy
1531, 10: return flatpak_proxy_client_get_max_policy_and_matched (client, source, NULL);
1. `org.freedesktop.DBus.ListNames`,`org.freedesktop.DBus.ListActivatableNames`
	<mark style="background: #FF5582A6;">EXPECTED_REPLY_LIST_NAMES</mark>
~~~c
	EXPECTED_REPLY_LIST_NAMES
		1933, 11: if (flatpak_proxy_client_get_max_policy (client, names[i]) >= FLATPAK_POLICY_SEE)
	~~~
2. `org.freedesktop.DBus.NameHasOwner`,`org.freedesktop.DBus.GetNameOwner`
	~~~c
	1896, 21: name_policy = flatpak_proxy_client_get_max_policy (client, name);
		//validate_arg0_name
	~~~
3. `org.freedesktop.DBus`->`org.freedesktop.DBus.NameOwnerChanged` should_filter_name_owner_changed, `broadcast`
	**处理ownerchange 消息**
   -  message_is_name_owner_changed
~~~c
static gboolean
      message_is_name_owner_changed (FlatpakProxyClient *client, Header *header)
      {
        if (header->type == G_DBUS_MESSAGE_TYPE_SIGNAL &&
          g_strcmp0 (header->sender, "org.freedesktop.DBus") == 0 &&
          g_strcmp0 (header->interface, "org.freedesktop.DBus") == 0 &&
          g_strcmp0 (header->member, "NameOwnerChanged") == 0)
        return TRUE;
      return FALSE;
}
~~~
   - **got_buffer_from_bus**
~~~c
got_buffer_from_bus{
            /* We filter all NameOwnerChanged signal according to the policy */
            if (message_is_name_owner_changed (client, header))
              {
                if (should_filter_name_owner_changed (client, buffer))
                  g_clear_pointer (&buffer, buffer_unref);
              }
}
~~~
   - should_filter_name_owner_changed
~~~c
1980, 7: if (flatpak_proxy_client_get_max_policy (client, name) >= FLATPAK_POLICY_SEE ||
// should_filter_name_owner_changed
//broadcast
		//的第一个参数
~~~
4. queue_wildcard_initial_name_ops `EXPECTED_REPLY_FAKE_LIST_NAMES`
~~~c
2181, 15: flatpak_proxy_client_get_max_policy (client, name) != FLATPAK_POLICY_NONE)
		//queue_wildcard_initial_name_ops
~~~
###  3.2. 方法调用 get_dbus_method_handler  busname header->destination 
~~~c
  policy = flatpak_proxy_client_get_max_policy_and_matched (client, header->destination, &filters);
  if (policy < FLATPAK_POLICY_SEE)//none
    return HANDLE_HIDE;
  if (policy < FLATPAK_POLICY_TALK)//seee
    return HANDLE_DENY;
  // talk,owner 
  if (!is_for_bus (header))//not freedesktop.dbus
    {
      if (policy == FLATPAK_POLICY_OWN ||
          any_filter_matches (filters, FILTER_TYPE_CALL,
                              header->path,
                              header->interface,
                              header->member))
                              //talk必须是方法匹配
        return HANDLE_PASS;

      return HANDLE_DENY;
    }
    //对于是 freedesktop.dbus 方法返回针对值
~~~
### 3.3. 信号广播 got_buffer_from_bus` `header->sender 
**广播**
if (header->type == G_DBUS_MESSAGE_TYPE_SIGNAL && header->destination == NULL)

~~~c
/* All incoming broadcast signals are filtered according to policy */
if (header->type == G_DBUS_MESSAGE_TYPE_SIGNAL && header->destination == NULL)
...........................
...............
policy = flatpak_proxy_client_get_max_policy_and_matched (client, header->sender, &filters);

if (policy == FLATPAK_POLICY_OWN ||
              (policy == FLATPAK_POLICY_TALK &&
               any_filter_matches (filters, FILTER_TYPE_BROADCAST,
                                   header->path,
                                   header->interface,
                                   header->member)))
            filtered = FALSE;

if (filtered)
{
              if (client->proxy->log_messages)
                printline;g_print ("*FILTERED IN*;\n");
              g_clear_pointer (&buffer, buffer_unref);
}
~~~

