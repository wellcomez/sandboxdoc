---
title: "A deep dive into Linux namespaces, part 3"
layout: post
---
页码：1/2



http://ifeanyi.co/posts/linux-namespaces-part-3/

# A deep dive into Linux namespaces, part 3

**A deep dive into Linux namespaces, part 3** was published on <time>July 18, 2019</time>.

[Mount namespaces](http://man7.org/linux/man-pages/man7/mount_namespaces.7.html) isolate filesystem resources. This pretty much covers everything that has to do with files on the system. Among the encapsulated resources is a file containing the list of [mount points](http://www.linfo.org/mount_point.html) that are visible to a process and as we hinted at in the [intro post](/posts/linux-namespaces-part-1/), isolation can enforce that changing the list (or any other file) within some mount namespace instance `M` does not affect that list in a different instance (so that only the processes in `M` observe the changes).

## Mount Points

You might be wondering why we just zoomed in on a seemingly random file that contains a list - what’s so special about it? The list of mount points determines a process’ entire view of available [filesystems](https://www.tldp.org/LDP/sag/html/filesystems.html) on the system and since we’re in Linux land with the *everything is a file* mantra, the visibility of pretty much every resource is dictated by this view - from actual files and devices to information about which other processes are also running in the system. So it’s a huge security win for `isolate` to be able to dictate exactly what parts of the system we want commands that we run to be aware of. Mount namespaces combined with mount points are a very powerful tool that lets us acheive this.  

We can see mount points visible to a process with id `$pid` via the `/proc/$pid/mounts` file - its contents is the same for all processes that belong to the same mount namespace as `$pid`:

```shell
$ cat /proc/$$/mounts
...
/dev/sda1 / ext4 rw,relatime,errors=remount-ro,data=ordered 0 0
...
```

Spotted somewhere in the list returned on my system is the `/dev/sda1` device mounted at `/` (yours might differ). This is the disk device hosting the [root filesystem](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03.html) that contains all the good stuff needed for the system to start and run properly so it would be great if `isolate` runs commands without them knowing about filesystems like these.  

Let’s start by running a terminal in its own mount namespace:

> Strictly speaking, we don’t need superuser access to work with new mount namespaces as long as we include the user namespace setup procedures of the previous post. As a result, in this post we will only assume that `unshare` commands within the terminal are running as superuser. `isolate` doesn’t need this assumption.

```shell
# The -m flag creates a new mount namespace.
$ unshare -m bash
$ cat /proc/$$/mounts
...
/dev/sda1 / ext4 rw,relatime,errors=remount-ro,data=ordered 0 0
...
```

Hmmm, we can still see the same list as in the root mount namespace. Especially after witnessing in the [previous post](/posts/linux-namespaces-part-2/) that a new user namespace begins with a clean slate, it may seem that the `-m` flag we passed to `unshare` didn’t have any effect.

The shell process is in fact running in a different mount namespace (we can verify this by comparing the symlinked file `ls -l /proc/$$/ns/mnt` to that of another shell running in the root mount namespace). The reason we still see the same list is that whenever we create a new mount namespace (child), a copy of the mount points of the mount namespace where the creation took place (parent) is used as the child’s list. Now any changes we make to this file (e.g by mounting a filesystem) will be invisible to all other processes.

However, changing pretty much any other file at this point *will* affect other processes because we are still referencing the exact same files (Linux only makes copies of special files like the mount points list). This means that we currently have minimal isolation. If we want to limit what our command process will see, we must update this list ourselves.  

Now, on one extreme, since we’re trying to be security conscious, we could just say F\* it and have `isolate` clear the entire list before executing the command but that will render the command useless since every program at least has dependencies on resources like operating system files, which in turn, are backed by *some* filesystem. On the other extreme, we could also just execute the command as is, sharing with it, the same filesystems that contain the necessary system files that it requires but this obviously defeats the purpose of this isolation thing that we have going on.  

The sweet spot would provide the program with its very own copy of dependencies and system files that it requires to run, all sandboxed so that it can make any changes to them without affecting other programs on the system. In the best case scenario, we would wrap these files in a filesystem and mount it as the root filesystem (at the root directory `/`) before executing the un-suspecting program. The idea is, because everything reachable by a process must go via the root filesystem and because we will know exactly what files we put in there for the command process, we will rest easy knowing that it is properly isolated from the rest of the system.  

Alright, this sounds good in theory and in order to pull it off, we will do the following:

1. Create a copy of the dependencies and system files needed by the command.
2. Create a new mount namespace.
3. Replace the root filesystem in the new mount namespace with one that is made up of our system files copy.
4. Execute the program inside the new mount namespace.

## Root Filesystems

A question that arises already at step `1` is *which system files are even needed by the command we want to run?* We could rummage in our own root filesystem and ask this question for every file that we encounter and only include the ones where the answer is *yes* but that sounds painful and unnecessary. Also, we don’t even know what command `isolate` will be executing to begin with.  

If only people have had this same issue and gathered a set of system files, generic enough to serve as a base right out of the box for a majority of programs out there? Luckily there are many projects that do this! One of which is the [Alpine Linux project](https://alpinelinux.org/) (this is its main function when you start `FROM alpine:xxx` in your `Dockerfile`). Alpine provides [root filesystems](https://alpinelinux.org/downloads/) that we can use for our purposes. If you are following along, you can get a copy of their minimal root filesystem (`MINI ROOT FILESYSTEM`) for `x86_64` [here](http://dl-cdn.alpinelinux.org/alpine/v3.10/releases/x86_64/alpine-minirootfs-3.10.1-x86_64.tar.gz). The latest version at the time of writing and that we will use in this post is `v3.10.1`.

```shell
$ wget http://dl-cdn.alpinelinux.org/alpine/v3.10/releases/x86_64/alpine-minirootfs-3.10.1-x86_64.tar.gz
$ mkdir rootfs
$ tar -xzf alpine-minirootfs-3.10.1-x86_64.tar.gz -C rootfs
$ ls rootfs
bin  dev  etc  home  lib  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
```

The `rootfs` directory has familiar files just like our own root filesystem at `/` but checkout how minimal it is - quite a few of these directories are empty:

```shell
$ ls rootfs/{mnt,dev,proc,home,sys}
# empty
```

This is great! we can give the command that we launch a copy of this and it could `sudo rm -rf /` for all we care, no one else will be bothered.

## Pivot root

Given our new mount namespace and a copy of system files, we would like to mount those files on the root directory of the new mount namespace without pulling the rug from under our feet. Linux has us covered here with the `pivot_root` system call (there is an associated command) that allows us to control what a processes sees as the root filesystem.

The command takes two arguments `pivot_root new_root put_old` where `new_root` is the path to the filesystem containing the soon-to-be root filesystem and `put_old` is a path to a directory. It works by:

1. Mounting the root filesystem of the calling process on `put_old`.
2. Mounting the filesystem pointed to by `new_root` as the current root filesystem at `/`.

Let’s see this in action. In our new mount namespace, we start by creating a filesystem out of our alpine files:

```shell
$ unshare -m bash
$ mount --bind rootfs rootfs
```

Next we pivot root:

```shell
$ cd rootfs
$ mkdir put_old
$ pivot_root . put_old
$ cd /
# We should now have our new root. e.g if we:
$ ls proc
# proc is empty
# And the old root is now in put_old
$ ls put_old
bin   dev  home        lib    lost+found  mnt  proc  run   srv  tmp  var
boot  etc  initrd.img  lib64  media       opt  root  sbin  sys  usr  vmlinuz
```

Finally, we unmount the old filesystem from `put_old` so that the nested shell cannot access it.

```shell
$ umount -l put_old
```

With that, we can run any command in our shell and they will run using our custom alpine root filesystem, unaware of the orchestration that led up to their execution. And our precious files on the old filesystem are safe beyond their reach.

## Implementation

> The source code for this post [can be found here](https://github.com/iffyio/isolate/tree/part-3).

We can replicate what we just accomplished in code, swapping the `pivot_root` command for the corresponding system call. First, we create our command process in a new mount namespace by adding the `CLONE_NEWNS` flag to `clone`.

```c
int clone_flags = SIGCHLD | CLONE_NEWUTS | CLONE_NEWUSER | CLONE_NEWNS;
```

Next, we create a function `prepare_mntns` that, given a path to a directory containing system files (`rootfs`), sets up the current mount namespace by pivoting the root of the current process to `rootfs` as we did earlier.

```c
static void prepare_mntns(char *rootfs)
{
    const char *mnt = rootfs;

    if (mount(rootfs, mnt, "ext4", MS_BIND, ""))
        die("Failed to mount %s at %s: %m\n", rootfs, mnt);

    if (chdir(mnt))
        die("Failed to chdir to rootfs mounted at %s: %m\n", mnt);

    const char *put_old = ".put_old";
    if (mkdir(put_old, 0777) && errno != EEXIST)
        die("Failed to mkdir put_old %s: %m\n", put_old);

    if (syscall(SYS_pivot_root, ".", put_old))
        die("Failed to pivot_root from %s to %s: %m\n", rootfs, put_old);

    if (chdir("/"))
        die("Failed to chdir to new root: %m\n");

    if (umount2(put_old, MNT_DETACH))
        die("Failed to umount put_old %s: %m\n", put_old);
}
```

We need to call this function from our code and it must be done by our command process in `cmd_exec` (since its the one running within the new mount namespace), before the actual command begins execution.

```c
    ...
    // Wait for 'setup done' signal from the main process.
    await_setup(params->fd[0]);

    prepare_mntns("rootfs");
    ...
```

Let’s try it out:

```shell
$ ./isolate sh
===========sh============
$ ls put_old
# put_old is empty. Hurray!
# What does our new mount list look like?
$ cat /proc/$$/mounts
cat: cant open '/proc/1431/mounts': No such file or directory
# Hmmm, what other processes are running?
$ ps aux
PID   USER     TIME  COMMAND
# Empty! eh?
```

This output shows something strange - we’re unable to verify the mount list that we have fought so hard for, and `ps` tells us that there are no processes running on the system (not even the current process or `ps` itself?). Its more likely that we broke something while setting up the mount namespace.

## PID Namespaces

We’ve mentioned the `/proc` directory a few times so far in this series and if you were familiar with it, then you’re probably not surprised that `ps` came up empty since we saw earlier that the directory was empty within this mount namespace (when we got it from the alpine root filesystem).  

The `/proc` directory in Linux is usually used to expose a [special filesystem](https://www.tldp.org/LDP/Linux-Filesystem-Hierarchy/html/proc.html) (called the proc filesystem) that is managed by Linux itself. Linux uses it to expose information about all processes running in the system as well as other system information with regards to devices, interrupts etc. Whenever we run a command like `ps` which accesses information about processes in the system, it looks to this filesystem to fetch information.

In other words, we need to spin up a `proc` filesystem. Luckily, this basically involves telling Linux that we need one, preferably mounted at `/proc`. But we can’t do so just yet since our command process is still dependent on the same `proc` filesystem as `isolate` and every other regular process in the system - to cut this dependency, we need to run it inside its own `PID` namespace.  

The [PID namespace](http://man7.org/linux/man-pages/man7/pid_namespaces.7.html) isolates process IDs in the system. One effect is that processes running in different PID namespaces can have the same process ID without conflicting with each other. Granted that we’re isolating this namespace because we want to give as much isolation as we can to our running command, a more interesting reason we show it here is that mounting the `proc` filesystem requires root privileges and the current PID namespace is owned by the root user namespace where we do not have sufficient permissions (if you remember from the [previous post](/posts/linux-namespaces-part-2/), `root` to the command process isn’t really root). So, we must be running within a PID namespace owned by the user namespace that recognizes our command process as root.  

We can create a new PID namespace by passing the `CLONE_NEWPID` to `clone`:

```c
int clone_flags = SIGCHLD | CLONE_NEWUTS | CLONE_NEWUSER | CLONE_NEWNS | CLONE_NEWPID;
```

Next, we add a function `prepare_procfs` that sets up the proc filesystem by mounting one within the currently mount and pid namespace.

```c
static void prepare_procfs()
{
    if (mkdir("/proc", 0555) && errno != EEXIST)
        die("Failed to mkdir /proc: %m\n");

    if (mount("proc", "/proc", "proc", 0, ""))
        die("Failed to mount proc: %m\n");
}
```

Finally, we call the function right before unmounting `put_old` in our `prepare_mntns` function, after we have setup the mount namespace and changed to the root directory.

```c
static void prepare_mntns(char *rootfs)
{
  ...

    prepare_procfs();

    if (umount2(put_old, MNT_DETACH))
        die("Failed to umount put_old %s: %m\n", put_old);
  ...
}
```

We can take `isolate` for another spin:

```shell
$ ./isolate sh
===========sh============
$ ps
PID   USER     TIME  COMMAND
    1 root      0:00 sh
    2 root      0:00 ps
```

This looks much better! The shell sees itself as the only process running on the system and running as PID 1 (since it was the first process to start in this new PID namespace).  

This post covered two namespaces and `isolate` racked up two new features as a result. In the [next post](/posts/linux-namespaces-part-4/), we will be looking at isolation via `Network` namespaces. There, we will have to deal with some intricate, low-level network configuration in an attempt to enable network communication between processes in different network namespaces.

---

In this final post of the series we take a look at [Network namespaces](http://man7.org/linux/man-pages/man8/ip-netns.8.html). As we hinted at during the [intro post](/posts/linux-namespaces-part-1/), a network namespace isolates network related resources - a process running in a distinct network namespace has its own networking devices, routing tables, firewall rules etc. We can see this in action immediately by inspecting our current network environment.

## The ip Command

> Since we will be interacting with network devices in this post, we will re-enforce the superuser requirements that we relaxed in the previous posts. From now on, we will assume that both `ip` and `isolate` are being run with `sudo`.

```shell
$ ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 00:0c:29:96:2e:3b brd ff:ff:ff:ff:ff:ff
```

Star of the show here is the `ip` command - the [Swiss Army Knife](https://access.redhat.com/sites/default/files/attachments/rh_ip_command_cheatsheet_1214_jcs_print.pdf) for networking in Linux - and we will use it extensively in this post. Right now we have just run the `link list` subcommand to show us what networking devices are currently available in the system (here we have `lo`, the loopback interface and `ens33` an ethernet LAN interface).  

As with all other namespaces, the system starts with an initial network namespace within to which all processes belong unless specified otherwise. Running this `ip link list` command as-is gives us the networking devices owned by the initial namespace (since our shell and the `ip` command belong to this namespace).

## Named Network Namespaces

Let’s create a new network namespace:

```shell
$ ip netns add coke
$ ip netns list
coke
```

Again, we’ve used the `ip` command. Its `netns` subcommand allows us to play with network namespaces - for example we can create new network namespaces using the `add` subcommand of `netns` and use `list` to, well, list them.

You might notice that `list` only returned our newly created namespace - shouldn’t it return at least two, the other one being the initial namespace that we mentioned earlier? The reason for this is that `ip` creates what is called a *named network namespace*, which simply is a network namespace that is identifiable by a unique name (in our case `coke`). Only named network namespaces are shown via `list` and the initial network namespace isn’t named.  

Named network namespaces are easier to get a hold of. For example, a file is created for each named network namespace under the `/var/run/netns` folder and can be used by a process that wants to switch to its namespace. Another property of named network namespaces is that they can exist without having any process as a member - unlike non-named ones that will be deleted once all member processes exit.  

Now that we have a child network namespace, we can see networking from its perspective.

> We will be using the command prompt `C$` to emphasize a shell running inside a child network namespace.

```shell
$ ip netns exec coke bash
C$ ip link list
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
```

The `exec $namespace $command` subcommand executes `$command` in the named network namespace `$namespace`. Here we ran a shell inside the `coke` namespace and listed the network devices available. We can see that at least our `ens33` device has disappeared. The only device that shows up is loopback and even that interface is down.

```shell
C$ ping 127.0.0.1
connect: Network is unreachable
```

We should be used to this by now, the default setup for namespaces are usually very strict. For network namespaces as we can see, no devices except `loopback` will be present. We can bring the `loopback` interface up without any paperwork though:

```shell
C$ ip link set dev lo up
C$ ping 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.034 ms
...
```

## Network Isolation

We’re already starting to see that by running a process in a nested network namespace like `coke`, we can be sure that it is isolated from the rest of the system as far as networking is concerned. Our shell process running in `coke` can only communicate via `loopback` - this means that it can only communicate with processes that are also members of the `coke` namespace but currently there are no other member processes (and in the name of isolation, we would like that it remains that way) so it’s a bit lonely. Let’s try to relax this isolation a bit, we will create a *tunnel* through which processes in `coke` can communicate with processes in our initial namespace.  

Now, any network communication has to go via some network device and a device can exist in exactly one network namespace at any given time so communication between any two processes in different namespaces must go via at least two network devices - one in each network namespace.

#### Veth Devices

We will use a [**v**irtual **eth**ernet](http://man7.org/linux/man-pages/man4/veth.4.html) network device (or `veth` for short) to fulfill our need. Veth devices are always created as a pair of devices in a tunnel-like fashion so that messages written to the device on one end comes out of the device on the other end. You might guess that we could easily have one end in the initial network namespace and the other in our child network namespace and have all inter-network-namespace communication go via the respective veth end device (and you would be correct).

```shell
# Create a veth pair (veth0 <=> veth1)
$ ip link add veth0 type veth peer name veth1
# Move the veth1 end to the new namespace
$ ip link set veth1 netns coke
# List the network devices from inside the new namespace
C$ ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
7: veth1@if8: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether ee:16:0c:23:f3:af brd ff:ff:ff:ff:ff:ff link-netnsid 0
```

Our `veth1` device now shows up in the `coke` namespace. But to make the veth pair functional, we need to give them both IP addresses and bring the interfaces up. We will do this in their respective network namespace.

```shell
# In the initial namespace
$ ip addr add 10.1.1.1/24 dev veth0
$ ip link set dev veth0 up

# In the coke namespace
C$ ip addr add 10.1.1.2/24 dev veth1
C$ ip link set dev veth1 up

C$ ip addr show veth1
7: veth1@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether ee:16:0c:23:f3:af brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 10.1.1.2/24 scope global veth1
       valid_lft forever preferred_lft forever
    inet6 fe80::ec16:cff:fe23:f3af/64 scope link
       valid_lft forever preferred_lft forever
```

We should see that `veth1` is up and has our assigned address `10.1.1.2` - the same should happen for `veth0` in the initial namespace. Now we should be able to do an inter-namespace ping between two processes running in both namespaces.

```shell
$ ping -I veth0 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=64 time=0.041 ms
...
C$ ping 10.1.1.1
PING 10.1.1.1 (10.1.1.1) 56(84) bytes of data.
64 bytes from 10.1.1.1: icmp_seq=1 ttl=64 time=0.067 ms
...
```

## Implementation

> The source code for this post [can be found here](https://github.com/iffyio/isolate/tree/part-4).

As usual, we will now try to replicate what we’ve seen so far in code. Specifically, we will need to do the following:

1. Execute the command within a new network namespace.
2. Create a veth pair (veth0 <=> veth1).
3. Move the veth1 device to the new namespace.
4. Assign IP addresses to both devices and bring them up.

Step `1` is straight-forward, we create our command process in a new network namespace by adding the `CLONE_NEWNET` flag to `clone`:

```c
int clone_flags = SIGCHLD | CLONE_NEWUTS | CLONE_NEWUSER | CLONE_NEWNS | CLONE_NEWNET;
```

#### Netlink

For the remaining steps, we will primarily be using the [Netlink](http://www.infradead.org/~tgr/libnl/doc/core.html#_introduction) [interface](http://man7.org/linux/man-pages/man7/netlink.7.html) to communicate with Linux. Netlink is primarily used for communication between regular applications (like `isolate`) and the Linux kernel. It exposes an API on top of sockets, based on a [protocol](http://www.infradead.org/~tgr/libnl/doc/core.html#core_netlink_fundamentals) that determines message structure and content. Using this protocol we can send messages that Linux receives and translates to requests - like *create a veth pair with names veth0 and veth1*.  

Let’s start by creating our netlink socket. In it, we specify that we want to use the `NETLINK_ROUTE` protocol - this protocol covers implementations for network routing and device management.

```c
int create_socket(int domain, int type, int protocol)
{
    int sock_fd = socket(domain, type, protocol);
    if (sock_fd < 0)
        die("cannot open socket: %m\n");

    return sock_fd;
}

int sock_fd = create_socket(
  PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);
```

#### Netlink Message Format

A Netlink [message](http://www.infradead.org/~tgr/libnl/doc/core.html#core_msg_format) is a 4-byte aligned block of data containing a header (`struct nlmsghdr`) and a payload. The header format is described [here](https://tools.ietf.org/html/rfc3549#section-2.3.2). The [Network Interface Service (NIS) Module](https://tools.ietf.org/html/rfc3549#section-2.3.1) specifies the format (`struct ifinfomsg`) that payload related to network interface administration must begin with.  

Our request will be represented by the following `C` struct:

```c
#define MAX_PAYLOAD 1024

struct nl_req {
    struct nlmsghdr n;     // Netlink message header
    struct ifinfomsg i;    // Payload starting with NIS module info
    char buf[MAX_PAYLOAD]; // Remaining payload
};
```

#### Netlink Attributes

The NIS module requires the payload to be encoded as [Netlink attributes](http://www.infradead.org/~tgr/libnl/doc/core.html#core_attr). Attributes provide a way to segment the payload into subsections. An attribute has a type and a length in addition to a payload containing its actual data.  

The Netlink message payload will be encoded as a list of attributes (where any such attribute can in turn have nested attributes) and we will have some helper functions to populate it with attributes. In code, an attribute is represented by the `rtattr` struct in the `linux/rtnetlink.h` header file as:

```c
struct rtattr {
  unsigned short  rta_len;
  unsigned short  rta_type;
};
```

`rta_len` is the length of the attribute’s payload which immediately follows the `rt_attr` struct in memory (i.e the next `rta_len` bytes). How the content of this payload is interpreted is dictated by `rta_type` and possible values are entirely dependent on the receiver implementation and the request being sent.  

In an attempt to put this all together, let’s see how `isolate` makes a netlink request to create veth pair with the following function `create_veth` that fulfills step `2`:

```c
// ip link add ifname type veth ifname name peername
void create_veth(int sock_fd, char *ifname, char *peername)
{
    __u16 flags =
            NLM_F_REQUEST  // This is a request message
            | NLM_F_CREATE // Create the device if it doesn't exist
            | NLM_F_EXCL   // If it already exists, do nothing
            | NLM_F_ACK;   // Reply with an acknowledgement or error

    // Initialise request message.
    struct nl_req req = {
            .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg)),
            .n.nlmsg_flags = flags,
            .n.nlmsg_type = RTM_NEWLINK, // This is a netlink message
            .i.ifi_family = PF_NETLINK,
    };
    struct nlmsghdr *n = &req.n;
    int maxlen = sizeof(req);

    /*
     * Create an attribute r0 with the veth info. e.g if ifname is veth0
     * then the following will be appended to the message
     * {
     *   rta_type: IFLA_IFNAME
     *   rta_len: 5 (len(veth0) + 1)
     *   data: veth0\0
     * }
     */
    addattr_l(n, maxlen, IFLA_IFNAME, ifname, strlen(ifname) + 1);

    // Add a nested attribute r1 within r0 containing iface info
    struct rtattr *linfo =
            addattr_nest(n, maxlen, IFLA_LINKINFO);
    // Specify the device type is veth
    addattr_l(&req.n, sizeof(req), IFLA_INFO_KIND, "veth", 5);

    // Add another nested attribute r2
    struct rtattr *linfodata =
            addattr_nest(n, maxlen, IFLA_INFO_DATA);

    // This next nested attribute r3 one contains the peer name e.g veth1
    struct rtattr *peerinfo =
            addattr_nest(n, maxlen, VETH_INFO_PEER);
    n->nlmsg_len += sizeof(struct ifinfomsg);
    addattr_l(n, maxlen, IFLA_IFNAME, peername, strlen(peername) + 1);
    addattr_nest_end(n, peerinfo); // end r3 nest

    addattr_nest_end(n, linfodata); // end r2 nest
    addattr_nest_end(n, linfo); // end r1 nest

    // Send the message
    send_nlmsg(sock_fd, n);
}
```

As we can see, we need to be precise about what we send here - we had to encode the message in the exact way it will be interpreted by the kernel implementation and here it took us 3 nested attributes to do so. I’m sure this is documented somewhere even though I was unable to find it after some googling - I mostly figured this out via [strace](https://linux.die.net/man/1/strace) and the `ip` command [source code](https://github.com/shemminger/iproute2/tree/master/ip).  

Next, for step `3`, is a method that, given an interface name `ifname` and a network namespace file descriptor `netns`, moves the device associated with that interface to the specified network namespace.

```c
// $ ip link set veth1 netns coke
void move_if_to_pid_netns(int sock_fd, char *ifname, int netns)
{
    struct nl_req req = {
            .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg)),
            .n.nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK,
            .n.nlmsg_type = RTM_NEWLINK,
            .i.ifi_family = PF_NETLINK,
    };

    addattr_l(&req.n, sizeof(req), IFLA_NET_NS_FD, &netns, 4);
    addattr_l(&req.n, sizeof(req), IFLA_IFNAME,
              ifname, strlen(ifname) + 1);
    send_nlmsg(sock_fd, &req.n);
}
```

After creating the veth pair and moving one end to our target network namespace, step `4` has us assigning both end devices IP addresses and bringing their interfaces up. For that we have a helper function `if_up` which, given an interface name `ifname` and ip address `ip`, assigns `ip` to the device `ifname` and brings it up. For brevity we do not show those here but they can be found [here instead](https://github.com/iffyio/isolate/blob/part-4/netns.c#L155).  

Finally, we bring these methods together to prepare our network namespace for our command process.

```c
static void prepare_netns(int child_pid)
{
    char *veth = "veth0";
    char *vpeer = "veth1";
    char *veth_addr = "10.1.1.1";
    char *vpeer_addr = "10.1.1.2";
    char *netmask = "255.255.255.0";

    // Create our netlink socket
    int sock_fd = create_socket(
            PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);

    // ... and our veth pair veth0 <=> veth1.
    create_veth(sock_fd, veth, vpeer);

    // veth0 is in our current (initial) namespace
    // so we can bring it up immediately.
    if_up(veth, veth_addr, netmask);

    // ... veth1 will be moved to the command namespace.
    // To do that though we need to grab a file descriptor
    // to and enter the commands namespace but first we must
    // remember our current namespace so we can get back to it
    // when we're done.
    int mynetns = get_netns_fd(getpid());
    int child_netns = get_netns_fd(child_pid);

    // Move veth1 to the command network namespace.
    move_if_to_pid_netns(sock_fd, vpeer, child_netns);

    // ... then enter it
    if (setns(child_netns, CLONE_NEWNET)) {
        die("cannot setns for child at pid %d: %m\n", child_pid);
    }

    // ... and bring veth1 up
    if_up(vpeer, vpeer_addr, netmask);

    // ... before moving back to our initial network namespace.
    if (setns(mynetns, CLONE_NEWNET)) {
        die("cannot restore previous netns: %m\n");
    }

    close(sock_fd);
}
```

Then we can call `prepare_netns` right after we’re done setting up the user namespace.

```c
    ...
    // Get the writable end of the pipe.
    int pipe = params.fd[1];

    prepare_userns(cmd_pid);
    prepare_netns(cmd_pid);

    // Signal to the command process we're done with setup.
    ...
```

Let’s try it out!

```shell
$ sudo ./isolate sh
===========sh============
$ ip link list
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
31: veth1@if32: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP qlen 1000
    link/ether 2a:e8:d9:df:b4:3d brd ff:ff:ff:ff:ff:ff
# Verify inter-namespace connectivity
$ ping 10.1.1.1
PING 10.1.1.1 (10.1.1.1): 56 data bytes
64 bytes from 10.1.1.1: seq=0 ttl=64 time=0.145 ms
```

---
