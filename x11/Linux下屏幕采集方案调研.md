---
date: 2023-04-11 23:28
title: Linux下屏幕采集方案调研
tags:
- watermark
---

http://www.lenky.info/archives/2022/11/3213

# Linux下屏幕采集方案调研

2022年11月5日[lenky](http://www.lenky.info/archives/author/lenky "由lenky发布")[发表评论](http://www.lenky.info/archives/2022/11/3213#respond)[阅读评论](http://www.lenky.info/archives/2022/11/3213#comments)522 次浏览

Linux下如何采集桌面屏幕？

对这块完全不懂，只能先看看类似x11vnc-0.9.16的源码。

https://github.com/LibVNC/x11vnc

然后再找一些录屏软件，也得看看源码。

https://github.com/obsproject/obs-studio

一，x11vnc-0.9.16源码  
核心函数：watch_loop  
找屏幕相关：watch_loop -> scan_for_updates -> scan_display -> copy_image  
然后是：  
copy_raw_fb  
XShmGetImage_wr  
XGetSubImage_wr

其中：  
copy_raw_fb直接读取/dev/video设备文件？具体见函数：initialize_raw_fb  
XShmGetImage_wr封装XShmGetImage，XGetSubImage_wr封装XGetSubImage，这两个函数貌似是Xlib编程的API接口。  
搜到一篇利用XShmGetImage捕获屏幕数据的博文，可用参考，不错。

https://blog.csdn.net/qq_36383272/article/details/114978583

目前看，x11vnc主要就是利用XShmGetImage来获取桌面屏幕了？

二，obs-studio-28.1.1源码  
关键代码比较好找，在目录：obs-studio-28.1.1\plugins\linux-capture  
里面的README说明是通过MIT-SHM扩展来捕获桌面图像。  
并且给出了参考地址：http://www.x.org/releases/current/doc/xextproto/shm.html  
但obs这里没有直接使用XShmGetImage接口来获取图像，而是用的xcb_类似接口？  
关键函数是：xcompcap_video_tick，该函数实现定时抓屏？  
与XShmGetImage相对应的应该是xcb_shm_get_image或xcb_get_image接口？但obs源码里也没有找到这几个类似接口。  
看来obs是另外的做法？

https://www.mankier.com/3/xcb_shm_get_image#

https://cpp.hotexamples.com/examples/-/-/xcb_get_image/cpp-xcb_get_image-function-examples.html

Google下Xlib vs libxcb  
看起来libxcb的目标是替换Xlib，Xlib相对较久，而libxcb更底层也更复杂，当然性能也更好。libxcb文档也好少。  
好吧，需要更深一步了解相关知识。

https://venam.nixers.net/blog/unix/2016/10/25/x11-blah-blah.html

https://www.51cto.com/article/650435.html?pc

检查当前桌面使用的是wayland还是X11：echo $XDG_SESSION_TYPE

https://bbs.archlinux.org/viewtopic.php?id=59840

https://xcb.freedesktop.org/XcbApi/

继续找：  
这个是Windows的分析，对我帮助不大：  
【obs-studio开源项目从入门到放弃】windows 窗口采集和桌面采集的解决方案

https://blog.csdn.net/qq_33844311/article/details/122560396

找到个Linux的分析：  
Linux X11获取屏幕截图和程序窗口截图，并通过QPixmap显示

https://blog.51cto.com/u_15127661/4277766

文章用的XGetImage来获取图像，但我搜索obs-studio-28.1.1源码，并没有看到obs使用这个接口。  
根据文章里图片上的显示，下载老版本的代码看看（2018年的代码）：https://github.com/obsproject/obs-studio/releases/tag/22.0.3  
同样没有直接调用XGetImage，估计上面博文的抓屏逻辑并没有参考obs源码。

继续找：  
C windows和linux截屏

https://blog.csdn.net/weixin_46840784/article/details/125424113

一种基于虚拟化技术的Linux视频编码方法与流程

https://www.xjishu.com/zhuanli/62/202111002790.html

https://blog.csdn.net/zw1996/article/details/118633573

以上博文均有明确的调用xcb_get_image接口来获取桌面图像，所以还是没明白obs是怎么截图的。

三，抓屏性能问题  
通过ffmpeg -f x11grab进行抓包有性能问题，可能会出现卡顿现象。

https://www.ffmpeg.org/ffmpeg-devices.html#x11grab

https://superuser.com/questions/709866/ffmpeg-x11grab-a-single-window

Xcomposite和OpenGL capture性能还行？

https://wiki.tonytascioglu.com/scripts/ffmpeg/kmsgrab_screen_capture

dma零拷贝优化？

https://github.com/w23/obs-kmsgrab

https://hg.sr.ht/~scoopta/wlrobs

https://blaztinn.gitlab.io/post/dmabuf-texture-sharing/

四，obs抓屏分析继续  
搜素过程中，找到一些抓屏性能优化的问题。根据这个思路，猜测obs可能不是采用普通的抓屏方式？  
而是采用性能更优的其他方式？  
在函数xcompcap_video_tick里找到几个关键逻辑：  
xcompcap_video_tick -> watcher_register -> xcb_composite_redirect_window  
xcompcap_video_tick -> xcomp_create_pixmap -> xcb_get_geometry/xcb_composite_name_window_pixmap_checked  
可能上面两个逻辑就是获取屏幕图像数据的操作，猜测这样做的性能更高？

https://xcb.freedesktop.org/tutorial/

https://www.x.org/releases/X11R7.7/doc/

https://blog.csdn.net/weixin_35715402/article/details/116726184

over~

——–  
更新：

一，分析了Linux下另外一个vnc软件turbovnc-3.0.1，看其是如何截屏的？

核心函数：rfbProcessClientNormalMessage()  
处理消息：rfbFramebufferUpdateRequest  
拷屏相关：rfbSendFramebufferUpdate()

初步结论（完全不保证正确）：  
turbovnc自身管理display，自身就拥有屏幕数据，因此没有拷贝操作，直接访问全部变量rfbFB.pfbMemory拿到屏幕图像，然后进行截取操作？？

二，obs抓屏分析继续  
前面分析obs抓屏的关键函数如下：  
xcompcap_video_tick -> watcher_register -> xcb_composite_redirect_window  
xcompcap_video_tick -> xcomp_create_pixmap -> xcb_get_geometry/xcb_composite_name_window_pixmap_checked

里面涉及到一些xcb接口，需要明确下各个xcb接口的具体含义。

1，xcb_get_window_attributes：  
Gets window attributes，获取窗口属性，异步操作，因此需要使用xcb_get_window_attributes_reply()获取结果。

2，xcb_change_window_attributes：  
change window attributes，修改窗口属性  
XCB_CW_EVENT_MASK：事件掩码定义客户端对此窗口感兴趣的事件。

3，xcb_composite_redirect_window：

https://www.x.org/releases/current/doc/man/man3/xcb_composite_redirect_window.3.xhtml

man手册上啥也没有，不知道什么意思。

4，xcb_get_geometry：  
Get current window geometry，获取当前窗口几何图形。

5，xcb_generate_id：  
创建新X对象之前分配XID。

6，xcb_composite_name_window_pixmap_checked：

https://www.x.org/releases/current/doc/man/man3/xcb_composite_name_window_pixmap.3.xhtml

man手册上啥也没有，不知道什么意思。

参考：https://www.x.org/releases/current/doc/man/man3/

其中的3和6应该就是截屏的关键API函数。man手册上没有过多解释。  
但在这个地址有Xlib的解释，应该是一致的：

https://linux.die.net/man/3/xcompositenamewindowpixmap

XCompositeRedirectWindow  
XCompositeRedirectWindow requests the X server to direct the hierarchy starting at window to off-screen storage. The update argument specifies whether the contents are mirrored to the parent window automatically or not. Only one client at a time may specify an update type of CompositeRedirectManual, another attempt will result in a BadAccess error. When all clients enabling redirection terminate, the redirection will automatically be disabled.  
The root window may not be redirected. Doing so results in a BadMatch error. Specifying an invalid window id will result in a BadWindow error.

XCompositeNameWindowPixmap  
XCompositeNameWindowPixmap creates and returns a pixmap id that serves as a reference to the off-screen storage for window. This pixmap will remain allocated until freed, even if the window is unmapped, reconfigured or destroyed. However, the window will get a new pixmap allocated each time it is mapped or resized, so this function will need to be reinvoked for the client to continue to refer to the storage holding the current window contents. Generates a BadMatch error if window is not redirected or is not visible.  
The X server must support at least version 0.2 of the Composite Extension for XCompositeNameWindowPixmap.

应该就是获取窗口的pixman像素图，然后调用gs_texture_create_from_pixmap转换为texture纹理图？所以，支持3D？  
参考：https://www.jianshu.com/p/c49fc6c1b03e

参考：

https://xcb.freedesktop.org/tutorial/

https://cloud.tencent.com/developer/ask/sof/714425

https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-i/

https://jichu4n.com/posts/how-x-window-managers-work-and-how-to-write-one-part-ii/

附录：Ubuntu 20.04.3 LTS下试用obs-studio  
1，安装  
sudo add-apt-repository ppa:obsproject/obs-studio  
sudo apt update  
sudo apt install obs-studio

2，运行

参考：

https://github.com/obsproject/obs-studio

https://github.com/obsproject/obs-studio/wiki/Install-Instructions

https://obsproject.com/wiki/install-instructions#linux

https://zhuanlan.zhihu.com/p/412270603

其他：  
linux xcb

https://blog.csdn.net/weixin_35715402/article/details/116726184

linux frame

https://www.kernel.org/doc/html/latest/driver-api/frame-buffer.html

https://github.com/roddehugo/linuxfb

https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842128/Video+Framebuffer+Read

https://wiki.linuxquestions.org/wiki/Framebuffer

DirectFB

https://elinux.org/DirectFB

https://platform.uno/docs/articles/features/using-linux-framebuffer.html

抓屏软件：

https://linuxhint.com/60_fps_screen_recording_apps_linux/

https://itsfoss.com/best-linux-screen-recorders/

https://launchpad.net/kazam

https://github.com/vkohaupt/vokoscreenNG

https://www.maartenbaert.be/simplescreenrecorder/

https://github.com/MaartenBaert/ssr

https://obsproject.com/download#linux

https://github.com/obsproject/obs-studio

屏幕共享：

http://displaycast.github.io/

VNC (and variants such as Apple Remote Desktop)  
Microsoft Remote Desktop  
Google Chrome Desktop  
Cisco WebEx

vnc：

https://blog.csdn.net/tennychen/article/details/126799469

vino

https://www.lategege.com/?p=691

vnc4server

https://tigervnc.org/

https://github.com/TigerVNC/tigervnc

remote display system：

https://www.tecmint.com/best-remote-linux-desktop-sharing-software/

转载请保留地址：[http://www.lenky.info/archives/2022/11/3213](http://www.lenky.info/archives/2022/11/3213) 或 [http://lenky.info/?p=3213](http://lenky.info/?p=3213)

---

备注：如无特殊说明，文章内容均出自[Lenky](http://lenky.info/)个人的真实理解而并非存心妄自揣测来故意愚人耳目。由于个人水平有限，虽力求内容正确无误，但仍然难免出错，请勿见怪，如果可以则请留言告之，并欢迎来[信](mailto:lenky0401#gmail.com "改 # 为 @")讨论。另外值得说明的是，[Lenky](http://lenky.info/)的部分文章以及部分内容参考借鉴了网络上各位网友的热心分享，特别是一些带有完全参考的文章，其后附带的链接内容也许更直接、更丰富，而我只是做了一下归纳&转述，在此也一并表示感谢。关于本站的所有技术文章，欢迎转载，但请遵从[CC创作共享协议](http://creativecommons.org/licenses/by-nc-nd/3.0/us/)，而一些私人性质较强的心情随笔，建议不要转载。

法律：根据最新颁布的《信息网络传播权保护条例》，如果您认为本文章的任何内容侵犯了您的权利，请以[Email](mailto:lenky0401#gmail.com "改 # 为 @")或书面等方式告知，本站将及时删除相关内容或链接。