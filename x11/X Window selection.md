---
date: 2023-05-04 21:38
title: X Window selection
tags:
- x11
- clipboard
- x11-selection
---



# X Window selection

From Wikipedia, the free encyclopedia

**Selections**, **cut buffers**, and **drag-and-drop** are the mechanisms used in the [X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System") to allow a [user](https://en.wikipedia.org/wiki/User_(computing) "User (computing)") to transfer data from one [window](https://en.wikipedia.org/wiki/Window_(computing) "Window (computing)") to another. [Selections](https://en.wikipedia.org/wiki/Selection_(user_interface) "Selection (user interface)") and cut buffer are typically used when a user selects text or some other data in a window and pastes in another one. Drag-and-drop is used when a user selects something in a window, then clicks on the selection and drags it into another window.

Since the two windows may be handled by two different applications, these mechanisms require two different clients connected with the same X server to exchange data. The [X Window System core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol") includes some requests and events that are specific to selection exchange, but the transfer is mainly done using event sending and window properties, which are not specific to selection transfer.

Different types of data can be transferred: it is usually text, but can also be an image, a number, a list of objects, etc. In the following, only the case of text is considered.

## Active and passive selections

The methods for transferring data can be classified into active and passive, depending on whether the client handling the selected data has to actively participate in the transfer to a client requesting it:

- Passive

When some data is selected, the client handling the window where this selection is done transfers it somewhere, and no longer needs to care about it;

- Active

Transfer of data to a client requires the client "holding" the selection to actively participate in the exchange.

Selections and drag-and-drop are active mechanisms: after some text is selected in a window, the client handling the window must actively support a protocol for transferring the data to the application requesting it. By contrast, cut buffers are a passive mechanism: after some text is selected, it is transferred to a cut buffer and remains there even if the application handling the window terminates and the window is destroyed. The X clipboard is a passive mechanism as perceived by the client holding the selection, but requires the `xclipboard` client to actively support any subsequent data transfer.

An advantage of active mechanisms is that the data can be converted to a different format before the transfer. In particular, the client receiving the data can request the selection data to be converted into a suitable form. If the sending client refuses to do so, the receiver can request a different format. For example, a piece of text rendering [HTML](https://en.wikipedia.org/wiki/HTML "HTML") code can be transferred as text to a requester that can only handle text, but can also be transferred as HTML code if the requester can handle it. Such negotiation of format cannot be done by passive mechanisms, in which the client holding the selection (and giving it semantics) transfers the selection and is not involved in the further transfer into a client requesting it.

Another advantage of the active mechanisms is that large pieces of data can be transferred in a sequence of transfers rather than a single one. Passive mechanisms instead require all data to be transferred somewhere from the selection owner, then transferred again to the client requesting it.

The advantage of the passive mechanisms is that the transfer can be done even after the client holding the data terminates. This is not possible in the active mechanisms, which require the client holding the data to actively participate in the transfer.

## Selections

The X Window System supports an arbitrary number of selections; every selection is identified by a string (more precisely, an `atom`). The most used selection is the `PRIMARY` selection.

The following requests are specific to selection transfer, although transfer also involves other requests:

1.  request to know which window owns the selection
2.  request to set the window that owns the selection
3.  request to convert the selection

The owner of the selection is typically the window in which the selected text is located, if any. When the user selects some text in a window, the client handling the window must tell the server that the window is the owner of the selection.

When the user tries to paste the selection into another window, that window's handler initiates a protocol for getting the selected text from the other client. This protocol involves the second and third requests of the list above, and is not specified by the X protocol but as a convention in the [Inter-Client Communication Convention Manual](https://en.wikipedia.org/wiki/Inter-Client_Communication_Convention_Manual "Inter-Client Communication Convention Manual") (ICCCM).

In particular, the destination client begins by asking the server which window owns the selection. Then the two clients transfer the selection via the server. This exchange involves a property of a window, and an arbitrary piece of data attached to the window. If the content of the selection is considered small enough to be transferred all at once, the steps that take place are:

1.  the recipient of the selection requests the selection to be converted, specifying a property of a window (this may be the window where the text has to be pasted)
2.  in response, the server sends to the current owner of the selection a `SelectionRequest` event;
3.  the owner places the selected text in the property of the window that the requestor has specified by sending a `ChangeProperty`; request to the server
4.  the owner sends a request to the server to send the requester a `SelectionNotify` to notify that the selection has been transferred
5.  the requester can now read the selection in the property of the window by sending one or more `GetProperty` requests to the server;
6.  the requester destroys the property; if the owner has requested to be informed of this, it is sent a `PropertyNotify` event.

If the content is large, it should be transferred in chunks. In this case, both clients express interest in `PropertyNotify` events: this way, the selection owner knows when the selection has been read, and the requester knows when another chunk has been placed in the property.

The [XFixes](https://en.wikipedia.org/wiki/XFixes "XFixes") extension allow clients to listen for selection changes.[[1]](https://en.wikipedia.org/wiki/X_Window_selection#cite_note-1)

## Clipboard

The most used selection is the `PRIMARY` selection, and is used when the user selects some data. The `CLIPBOARD` selection is used when the user selects some data and explicitly requests it to be "copied" to the clipboard, such as by invoking "Copy" under the "Edit" menu of an application. An associated request of "Paste" results in the data of the `CLIPBOARD` selection being used.

At the level of the core protocol, the `PRIMARY` and `CLIPBOARD` selections do not differ. But the `xclipboard` client makes them behave differently. In particular, when another client asserts the ownership of the `CLIPBOARD` selection, this program requests and displays it in a window. Any further request for this selection are handled by `xclipboard`. This way, the content of the selection survives the client having copied it.

## Cut buffers

Cut buffers are another mechanism to transfer data, in particular selected text. They are [window properties](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#Properties "X Window System core protocol") of the [root window](https://en.wikipedia.org/wiki/Root_window "Root window"), named `CUT_BUFFER1`, etc. Unlike selections, cut buffers do not involve a direct interaction between clients. Rather, when text is selected in a window, the window owner copies this text into the property of the root window called `CUT_BUFFER1`. When the user pastes the text in another window, the window owner reads this property of the root window.

The `xcutsel` program transfers data between selections and cut buffers, and the `xcb` program allows various kinds of access to the cut buffers.

Cut buffers are considered obsolete.[[2]](https://en.wikipedia.org/wiki/X_Window_selection#cite_note-Zawinski2002-2)

## XDND
[[Drag-and-Drop Protocol for the X Window System]]

[Drag-and-drop](https://en.wikipedia.org/wiki/Drag-and-drop "Drag-and-drop") in the X Window System is regulated by the Xdnd convention.[[3]](https://en.wikipedia.org/wiki/X_Window_selection#cite_note-XDND-3) When the user drags the selected text into a window and releases the mouse button, the exchange of data is done as for the primary selection. Drag-and-drop is complicated by what happens during the drag. Namely, when the user drags the selection to different parts of the desktop or a window, the user expects to be able to tell whether text can be dropped or not. In particular, the target should display visual feedback on whether or not it will accept the drop, and the cursor should change to indicate the action that will be taken; e.g., copy or move.

In the Xdnd protocol, the window where the text is selected and the drag begins is called the _source_; the window over which the cursor hovers is called the _target_. The communication between the source and the target is driven by the source because the source "grabs" the cursor. An exchange between source and target is therefore necessary in order for the target to even know that drag-and-drop is happening. Since the source decides the shape of the cursor, the source must receive a response from the target in order to update the cursor. In addition, since the target may need to draw a bombsight to indicate where the drop will occur, and since acceptance of the drop may depend on the exact location of the cursor, this exchange must happen repeatedly as the cursor moves. In fact, even if the cursor does not move, messages must be exchanged to allow the target to scroll when the cursor is near an edge of the viewing area. Otherwise, the user will only be able to drop on the visible portion of the target.

A program can state that a window can be the target of a drop by creating a property named `XdndAware` which contains the highest version of the protocol that the program supports. This way, applications which support newer versions can fall back to older versions in order to interoperate correctly. In addition, all applications that are written without support for Xdnd will be ignored.

When the cursor enters the target window, the source checks the presence of the `XdndAware` property on that window. If this property is present, an exchange begins:

-   the source tells the target that the cursor has entered the target while dragging some data by sending an event `XdndEnter`
-   the target can find out which kind of data is dragged (text, image, etc.) by looking at this event and possibly by further interaction with the source

While the cursor is inside the target window:

-   the source sends `XdndPosition` events to tell the target where the cursor currently is
-   the target replies with `XdndStatus` events to tell the source whether the data can be dropped in the current position
-   the source sends a message `XdndLeave` or `XdndDrop` when the cursor has left the window or the button has been released, respectively

If the user drops, the target requests the selection from the source as usual. When the transfer of the selection is concluded, the target sends an `XdndFinish` event to tell the source that the transfer has been successful.

In summary, the protocol is driven by the source, which keeps the target informed of what is happening with the cursor. In reply, the target tells the source whether a drop would be accepted or not. The target must also be informed when the user releases the mouse button, as this event starts a regular request for a selection, which is a protocol driven by the target.

The above is the description of the Xdnd convention for drag-and-drop. Different conventions for drag-and-drop are used in Motif, OffiX, and Amulet.

### XDS

The _Direct Save Protocol_, abbreviated _XDS_ (for _X_ Window _D_irect _S_ave Protocol), is a [software protocol](https://en.wikipedia.org/wiki/Protocol_(computing) "Protocol (computing)") that supports saving [files](https://en.wikipedia.org/wiki/Computer_file "Computer file") by [dragging](https://en.wikipedia.org/wiki/Drag-and-drop "Drag-and-drop") them to [file manager](https://en.wikipedia.org/wiki/File_manager "File manager") [windows](https://en.wikipedia.org/wiki/Window_(computing) "Window (computing)"). XDS is built on top of the [XDND](https://en.wikipedia.org/wiki/XDND "XDND") protocol.[[4]](https://en.wikipedia.org/wiki/X_Window_selection#cite_note-XDS-JL-4)[[5]](https://en.wikipedia.org/wiki/X_Window_selection#cite_note-XDS-FdeskT-5)

## Programs

The following programs specifically operate on data transfer mechanisms:

-   xcutsel transfers data from selections to cut buffers or vice versa
-   xclipboard, [glipper](https://en.wikipedia.org/wiki/Glipper "Glipper") ([Gnome](https://en.wikipedia.org/wiki/Gnome "Gnome")), [parcellite](https://en.wikipedia.org/wiki/Parcellite "Parcellite") ([LXDE](https://en.wikipedia.org/wiki/LXDE "LXDE")), and [klipper](https://en.wikipedia.org/wiki/Klipper "Klipper") ([KDE](https://en.wikipedia.org/wiki/KDE "KDE")) are [clipboard managers](https://en.wikipedia.org/wiki/Clipboard_manager "Clipboard manager"), maybe wmcliphist as well
-   [xcb](http://oldhome.schmorp.de/marc/xcb.html) shows the content of the cut buffers and allows the user to manipulate them
-   [xselection](https://web.archive.org/web/20121106221515/http://www.novell.com/products/linuxpackages/opensuse/xselection.html), [xclip](https://github.com/astrand/xclip), [xsel](https://en.wikipedia.org/wiki/Clipboard_(computing)#X_Window_System "Clipboard (computing)") and [xcopy](http://www.chiark.greenend.org.uk/~sgtatham/utils/) are command line programs that copy data to or from the X selection. xcopy has a verbosity option that helps debug X selection issues. parcellite also has the ability to read from and write to specific X selections from the command line.
-   synergy is a cross platform tool that allows you to share a clipboard across multiple computers running multiple operating systems
-   xfce4-clipman-plugin is a "clipboard history plugin for the Xfce4 panel" and also a clipboard manager
-   xtranslate looks up words in the Xselection in a multi-lingual dictionary
-   [autocutsel](http://www.nongnu.org/autocutsel/) syncs cut buffer and selection buffer

## See also

-   [X Window System protocols and architecture](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture "X Window System protocols and architecture")
-   [X Window System core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol")
-   [ICCCM](https://en.wikipedia.org/wiki/ICCCM "ICCCM")

## References

1.  **[^](https://en.wikipedia.org/wiki/X_Window_selection#cite_ref-1 "Jump up")** ["c - X11 Wait for and Get Clipboard Text"](https://stackoverflow.com/a/44992967/1517969). _Stack Overflow_. Retrieved 2021-07-27.
2.  **[^](https://en.wikipedia.org/wiki/X_Window_selection#cite_ref-Zawinski2002_2-0 "Jump up")** Zawinski, J. W. (2002). _X selections, cut buffers, and kill rings._ Retrieved July 13, 2010, from [http://www.jwz.org/doc/x-cut-and-paste.html](https://www.jwz.org/doc/x-cut-and-paste.html)
3.  **[^](https://en.wikipedia.org/wiki/X_Window_selection#cite_ref-XDND_3-0 "Jump up")** Drag-and-Drop Protocol for the X Window System, from [http://johnlindal.wix.com/xdnd](http://johnlindal.wix.com/xdnd)
4.  **[^](https://en.wikipedia.org/wiki/X_Window_selection#cite_ref-XDS-JL_4-0 "Jump up")** Lindal, John. ["Saving Files Via Drag-and-Drop: The Direct Save Protocol for the X Window System"](https://web.archive.org/web/20160305162815/http://johnlindal.wix.com/xdsave). Archived from [the original](http://johnlindal.wix.com/xdsave) on 5 March 2016.
5.  **[^](https://en.wikipedia.org/wiki/X_Window_selection#cite_ref-XDS-FdeskT_5-0 "Jump up")** ["Saving Files Via Drag-and-Drop: The Direct Save Protocol for the X Window System"](https://web.archive.org/web/20150322085936/http://freedesktop.org:80/wiki/Specifications/XDS/). _freedesktop.org_. Archived from [the original](http://freedesktop.org/wiki/Specifications/XDS) on 22 March 2015.

## External links

-   [ICCCM: Peer-to-Peer Communication by Means of Selections](http://tronche.com/gui/x/icccm/sec-2.html)
-   [ICCCM: Peer-to-Peer Communication by Means of Cut Buffers](http://tronche.com/gui/x/icccm/sec-3.html)
-   [Xdnd specification](http://johnlindal.wix.com/xdnd)
-   [A paper by Keith Packard](http://keithp.com/~keithp/talks/selection.ps)
-   [Selections in general and in Emacs](https://www.jwz.org/doc/x-cut-and-paste.html)