---
date: 2023-04-13 23:33
title: How to get the window id of the toplevel window? was Re how to convert a window ID to a linux process ID?
tags:
- x11
---

# How to get the window id of the toplevel window? was Re how to convert a window ID to a linux process ID?

https://www.mail-archive.com/search?l=devel@xfree86.org&q=subject:%22how+to+convert+a+window+ID+to+a+linux+process+ID%5C%3F%22&o=newest&f=1

## 2004-03-24  dave giffin  how to convert a window ID to a linux process ID? 

 I need to be able to tell which process a given window was created by. (Window ID=Process ID)
 
 Apparently, X servers don't know which process a client is, they just get a socket (b/c of X's network transparency).
 
 It has been suggested that I could run a program like netstat on the client machine(which in my case is the same machine running the server) to get a list of which sockets belong to which processes.
 
 Then, I need to get the ID of the foreign socket
 (socket ID on client end) from which a window was created.
 
 What would work well is if the socket ID and the client/window ID could be written to STDOUT or STDIN when the window is created. Then I can have a script read the server's STDOUT or STDIN and compare it to the list of socket IDs and process IDs from netstat on the client machine (which for my purposes is on the same machine as the server).
 
 MY PROBLEM: I don't know how to get XFree86 to write the socket ID and client ID when a window is created!
 
 Today, I was looking at the XFree86(4.3.0) source code for the first time and trying some different hacks to get the info I wanted to print. 
 
 In xc/programs/Xserver/dix/dispatch.c, I found ProcCreateWindow and the ClientPtr structure. I was able to get X to print client-index and client-OsPrivate-fd when a new window was created.
 
 Which is what I want, except that the client index that is printed is always 2 and the socket ID is always 13. 13 is the ID of the socket that X listens on and I think 2 is the server client?
 
 What am I doing wrong?
 
 How can I print the Window ID and foreign/from socket ID of a client when a window is created?
 
 
 :) thanx
  PS: I'm thinking of making a sourceforge project to publish the XFree86 modification and some associated scripts so that others will be able to see which process created which window. How does the project name XTracer sound?
 
 __
 Do you Yahoo!?
 Yahoo! Finance Tax Center - File online. File on time.
 http://taxes.yahoo.com/filing.html
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  The Rasterman  Re: how to convert a window ID to a linux process ID? 

 On Wed, 24 Mar 2004 00:52:28 -0800 (PST) dave giffin [EMAIL PROTECTED]
>>babbled:
>>
>>/lurk
>>
>>much much much cleaner:
>>compile the attached 2 files into a .so shared library and use it as an
>>LD_PRELOAD when running ALL x applications (or make appropriate changes to Xlib
>>itself). this is a non invasive method that doesnt change the xserver or xlib -
>>it simply forces window creates (and reparents) to root to have that window
>>carry some extra properites - one of which is the process id (also machine name,
>>username etc. is done too).
>>
>>if you dont know about LD_PRELOAD i'd highly suggest reading up about it - its a
>>great way to do the most evil of hacks :)
>>
>>lurk
>>
>> I need to be able to tell which process a given window
>> was created by. (Window ID=Process ID)
>> 
>> Apparently, X servers don't know which process a
>> client is, they just get a socket (b/c of X's network
>> transparency).
>> 
>> It has been suggested that I could run a program like
>> netstat on the client machine(which in my case is the
>> same machine running the server) to get a list of
>> which sockets belong to which processes.
>> 
>> Then, I need to get the ID of the foreign socket
>> (socket ID on client end) from which a window was
>> created.
>> 
>> What would work well is if the socket ID and the
>> client/window ID could be written to STDOUT or STDIN
>> when the window is created. Then I can have a script
>> read the server's STDOUT or STDIN and compare it to
>> the list of socket IDs and process IDs from netstat on
>> the client machine (which for my purposes is on the
>> same machine as the server).
>> 
>> MY PROBLEM: I don't know how to get XFree86 to write
>> the socket ID and client ID when a window is created!
>> 
>> Today, I was looking at the XFree86(4.3.0) source code
>> for the first time and trying some different hacks to
>> get the info I wanted to print. 
>> 
>> In xc/programs/Xserver/dix/dispatch.c, I found
>> ProcCreateWindow and the ClientPtr structure. I was
>> able to get X to print client-index and
>> client-OsPrivate-fd when a new window was created.
>> 
>> Which is what I want, except that the client index
>> that is printed is always 2 and the socket ID is
>> always 13. 13 is the ID of the socket that X listens
>> on and I think 2 is the server client?
>> 
>> What am I doing wrong?
>> 
>> How can I print the Window ID and foreign/from socket
>> ID of a client when a window is created?
>> 
>> 
>> :) thanx
>> 
>> PS: I'm thinking of making a sourceforge project to
>> publish the XFree86 modification and some associated
>> scripts so that others will be able to see which
>> process created which window. How does the project
>> name XTracer sound?
>> 
>> __
>> Do you Yahoo!?
>> Yahoo! Finance Tax Center - File online. File on time.
>> http://taxes.yahoo.com/filing.html
>> \___
>> Devel mailing list
>> [EMAIL PROTECTED]
>> http://XFree86.Org/mailman/listinfo/devel
>>
>>
>>-- 
>>- Codito, ergo sum - "I code, therefore I am" --
>>The Rasterman (Carsten Haitzler)[EMAIL PROTECTED]
~~~c
 #include e_hack.h
 
 /* prototypes */
 static void __e_hack_set_properties(Display *display, Window window);
 
 /* dlopened xlib so we can find the symbols in the real xlib to call them */
 static void *lib_xlib = NULL;
 
 /* the function that actually sets the properties on toplevel window */
 static void
 __e_hack_set_properties(Display *display, Window window)
 {
    static Atom a_launch_id = 0;
    static Atom a_launch_path = 0;
    static Atom a_user_id = 0;
    static Atom a_process_id = 0;
    static Atom a_p_process_id = 0;
    static Atom a_machine_name = 0;
    static Atom a_user_name = 0;
    char *env = NULL;
 
    if (!a_launch_id)a_launch_id= XInternAtom(display, _E_HACK_LAUNCH_ID, False);
    if (!a_launch_path)  a_launch_path  = XInternAtom(display, _E_HACK_LAUNCH_PATH, False);
    if (!a_user_id)  a_user_id  = XInternAtom(display, _E_HACK_USER_ID, False);
    if (!a_process_id)   a_process_id   = XInternAtom(display, _E_HACK_PROCESS_ID, False);
    if (!a_p_process_id) a_p_process_id = XInternAtom(display, _E_HACK_PARENT_PROCESS_ID, False);
    if (!a_machine_name) a_machine_name = XInternAtom(display, _E_HACK_MACHINE_NAME, False);
    if (!a_user_name)a_user_name= XInternAtom(display, _E_HACK_USER_NAME, False);
 
    if ((env = getenv(E_HACK_LAUNCH_ID)))
   XChangeProperty(display, window, a_launch_id, XA_STRING, 8, PropModeReplace, env, strlen(env));
    if ((env = getenv(E_HACK_LAUNCH_PATH)))
   XChangeProperty(display, window, a_launch_path, XA_STRING, 8, PropModeReplace, env, strlen(env));
  {
 	uid_t uid;
 	pid_t pid, ppid;
 	struct utsname ubuf;
 	char buf[4096];
 	
 	uid = getuid();
 	pid = getpid();
 	ppid = getppid();
 
 	snprintf(buf, sizeof(buf), %i, uid);
~~~	

  


## 2004-03-24  Fernando Herrera  Re: how to convert a window ID to a linux process ID? 

 Wed, Mar 24, 2004 at 12:52:28AM -0800, dave giffin escribió:
 
 I need to be able to tell which process a given window was created by. (Window ID=Process ID)
 
 Apparently, X servers don't know which process a client is, they just get a socket (b/c of X's network transparency).
 
 If you are running a window manager setting the _NET_WM_PID property correctly (metacity does it, dunno about others) you can do the mapping easily. The next example get that prop from the Window ID, and then converts the PID to the UID of user running the process (it uses libgtop for doing that):
 
 Salu2
 
 ~~~c
 #include X11/Xlib.h
 #include X11/Xos.h
 #include X11/Xfuncs.h
 #include X11/Xutil.h
 #include stdio.h
 #include ctype.h
 
 #include X11/Xatom.h
 #include X11/Xmu/WinUtil.h
 
 #include libgtop-2.0/glibtop.h
 #include libgtop-2.0/glibtop/procuid.h
 
 
 int
 main (int argc, char*argv[])
 {
 Atom atom,actual_type;
 Display *dpy;
 int screen;
 char *atom_name;
 int actual_format;
 unsigned long nitems;
 unsigned long bytes_after;
 unsigned char *prop;
 int status;
 int pid;
 glibtop_proc_uid proc_uid;
 
 Window w=0;
 
 //dpy = XOpenDisplay(:0);
 dpy = XOpenDisplay(getenv(DISPLAY));
 screen = DefaultScreen(dpy);
 if (argc!=2) {
 printf(Usage %s window-id\n,argv[0]);
 exit(-1);
 }
 
 sscanf(argv[1],0x%x,w);
 
 
 atom = XInternAtom(dpy, _NET_WM_PID, True);
 atom_name = XGetAtomName (dpy,atom);
 
 
 status = XGetWindowProperty(dpy, w, atom, 0, 1024,
 False, AnyPropertyType,
 actual_type,
 actual_format, nitems,
 bytes_after,
 prop);
 if (status!=0) {
 printf(Cannot get _NET_WM_PID);
 exit(-2);
 }
 
 pid = prop[1] * 256; 
 pid += prop[0];
 printf(pid of window 0x%x = %d\n,w,pid);
 glibtop_get_proc_uid (proc_uid, pid);
 printf(uid = %d, euid = %d\n,proc_uid.uid, proc_uid.euid);
 return 0;
 }
~~~ 
 
 -- 
 Fernando Herrera de las Heras
 Onírica: análisis, diseño e implantación de soluciones informáticas
 http://www.onirica.com
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  Fernando Herrera  Re: how to convert a window ID to a linux process ID? 

 Wed, Mar 24, 2004 at 05:16:59PM +0100, Marcus Schaefer escribió:
 
 I tried your example because I don't know the _NET_WM_PID property and was interested in it. Unfortunately the call to XGetWindowProperty()
 never filled the prop data. Your code is checking against the return code of XGetWindowProperty() which is fine but prop equals to (nil) in all my tests. I was using metacity as suggested windowmanger.
 
 Do you have any idea why it fails ? 
 
 The window Id which is required of course is obtained from a xwininfo call. I passed the information from this command to
  Using this (removed the uid stuff) works here:
 
 [EMAIL PROTECTED] fer]$ gcc test.c -L/usr/X11R6/lib/ -I/usr/X11R6/include/ -lX11 -o 
 test
 [EMAIL PROTECTED] fer]$ xwininfo
  
 xwininfo: Please select the window about which you   would like information by clicking the   mouse in that window.
  
 xwininfo: Window id: 0x1e00012 [EMAIL PROTECTED]:~
 [...] 
 [EMAIL PROTECTED] fer]$ ./a.out 0x1e00012
 pid of window 0x1e00012 = 24109
 
 Salu2
 
 -- 
 Fernando Herrera de las Heras
 Onírica: análisis, diseño e implantación de soluciones informáticas
 http://www.onirica.com
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  Marcus Schaefer  Re: how to convert a window ID to a linux process ID? 

 Hi,
 
  I tried your example because I don't know the _NET_WM_PID property  and was interested in it. Unfortunately the call to XGetWindowProperty()
  never filled the prop data. Your code is checking against the return  code of XGetWindowProperty() which is fine but prop equals to (nil) in  all my tests. I was using metacity as suggested windowmanger.
  
  Do you have any idea why it fails ? 
  
  The window Id which is required of course is obtained from a  xwininfo call. I passed the information from this command to
      Using this (removed the uid stuff) works here:
  
  [EMAIL PROTECTED] fer]$ gcc test.c -L/usr/X11R6/lib/ -I/usr/X11R6/include/ -lX11 -o 
  test
  [EMAIL PROTECTED] fer]$ xwininfo
   
  xwininfo: Please select the window about which you
    would like information by clicking the
    mouse in that window.
   
  xwininfo: Window id: 0x1e00012 [EMAIL PROTECTED]:~
  [...] 
  [EMAIL PROTECTED] fer]$ ./a.out 0x1e00012
  pid of window 0x1e00012 = 24109
 
 Hmm, yes I see, I did it the same way but it doesn't work for me.
 The code I use is as follows:
 
 snip

~~~c
 #include X11/Xlib.h
 #include X11/Xos.h
 #include X11/Xfuncs.h
 #include X11/Xutil.h
 #include stdio.h
 #include ctype.h
 
 #include X11/Xatom.h
 #include X11/Xmu/WinUtil.h
 
 
 int main (int argc, char*argv[]) {
 Atom atom,actual_type;
 Display *dpy;
 int screen;
 char *atom_name;
 int actual_format;
 unsigned long nitems;
 unsigned long bytes_after;
 unsigned char *prop;
 int status;
 int pid;
 Window w=0;
 
 dpy = XOpenDisplay((char*)getenv(DISPLAY));
 screen = DefaultScreen(dpy);
 if (argc!=2) {
 printf(Usage %s window-id\n,argv[0]);
 exit(-1);
 }
 sscanf(argv[1],0x%x,(unsigned int*)(w));
 atom = XInternAtom(dpy, _NET_WM_PID, True);
 atom_name = XGetAtomName (dpy,atom);
 status = XGetWindowProperty(
 dpy, w, atom, 0, 1024,
 False, AnyPropertyType,
 actual_type,
 actual_format, nitems,
 bytes_after,
 prop
 );
 
 if (status!=0) {
 printf(Cannot get _NET_WM_PID);
 exit(-2);
 }
 if (! prop) {
 printf (No properties\n);
 exit (-3);
 }
 
 pid = prop[1] * 256;
 pid += prop[0];
 printf(pid of window 0x%x = %d\n,(unsigned int)w,pid);
 
 return 0;
 }
 snap---
 
 gcc id.c -L/usr/X11R6/lib/ -I/usr/X11R6/include/ -lX11 -o bla

~~~ 
I'm running XFree86 Version 4.3.99.902 (4.4.0 RC 2) on kernel
 2.6.4-6-default.
 2.6.4-6-default.
 
 any ideas ?
 
 Regards
 Marcus
 \-- 
  Public Key available
  
  Marcus Schäfer (Res.  Dev.)   SUSE LINUX AG
  Tel: 0911-740 53 0 Maxfeldstrasse 5
  FAX: 0911-741 77 55D-90409 Nürnberg
  http://www.suse.de Germany
  
 
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  Måns Rullgård  Re: how to convert a window ID to a linux process ID? 

 Fernando Herrera [EMAIL PROTECTED] writes:
 
  Wed, Mar 24, 2004 at 12:52:28AM -0800, dave giffin escribió:
 
 I need to be able to tell which process a given window was created by. (Window ID=Process ID)
 
 Apparently, X servers don't know which process a client is, they just get a socket (b/c of X's network transparency).
 
    If you are running a window manager setting the _NET_WM_PID  property correctly (metacity does it, dunno about others) you can do the  mapping easily.
 
 How does it handle windows owned by remote processes connected over the network?
 
 -- 
 Måns Rullgård
 [EMAIL PROTECTED]
 
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  Eamon Walsh  Re: how to convert a window ID to a linux process ID? 

 On Wed, 2004-03-24 at 03:52, dave giffin wrote:
  I need to be able to tell which process a given window  was created by. (Window ID=Process ID)
 
 On the server side, in os/access.c, is a function LocalClientCred() that you can use to get the UID and GID of a local client.  This function could be extended to return the PID as well since the structure returned by the SO_PEERCRED sockopt has the PID in it.  Take a look at it;
 there's already a bunch of _XTrans stuff dealing with this sort of thing.
 
 --Eamon
 
    Apparently, X servers don't know which process a  client is, they just get a socket (b/c of X's network  transparency).
  
  It has been suggested that I could run a program like  netstat on the client machine(which in my case is the  same machine running the server) to get a list of  which sockets belong to which processes.
  
  Then, I need to get the ID of the foreign socket
  (socket ID on client end) from which a window was  created.
  
  What would work well is if the socket ID and the  client/window ID could be written to STDOUT or STDIN  when the window is created. Then I can have a script  read the server's STDOUT or STDIN and compare it to  the list of socket IDs and process IDs from netstat on  the client machine (which for my purposes is on the  same machine as the server).
  
  MY PROBLEM: I don't know how to get XFree86 to write  the socket ID and client ID when a window is created!
  
  Today, I was looking at the XFree86(4.3.0) source code  for the first time and trying some different hacks to  get the info I wanted to print. 
  
  In xc/programs/Xserver/dix/dispatch.c, I found  ProcCreateWindow and the ClientPtr structure. I was  able to get X to print client-index and  client-OsPrivate-fd when a new window was created.
  
  Which is what I want, except that the client index  that is printed is always 2 and the socket ID is  always 13. 13 is the ID of the socket that X listens  on and I think 2 is the server client?
  
  What am I doing wrong?
  
  How can I print the Window ID and foreign/from socket  ID of a client when a window is created?
  
  
  :) thanx
  
  PS: I'm thinking of making a sourceforge project to  publish the XFree86 modification and some associated  scripts so that others will be able to see which  process created which window. How does the project  name XTracer sound?
  
  __
  Do you Yahoo!?
  Yahoo! Finance Tax Center - File online. File on time.
  http://taxes.yahoo.com/filing.html
  \___
  Devel mailing list
  [EMAIL PROTECTED]
  http://XFree86.Org/mailman/listinfo/devel
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  Andrew C Aitchison  Re: how to convert a window ID to a linux process ID? 

 On Wed, 24 Mar 2004, [iso-8859-1] Mns Rullgrd wrote:
 
  Fernando Herrera [EMAIL PROTECTED] writes:
  
   If you are running a window manager setting the _NET_WM_PID
   property correctly (metacity does it, dunno about others) you can do the   mapping easily.
  
  How does it handle windows owned by remote processes connected over  the network?
 
 And what happens to remote applications displaying over an ssh tunnel ?
 Do they all share the pid of the ssh client ?
 
 -- 
 Andrew C. Aitchison Cambridge
 [EMAIL PROTECTED]
 
 
 \___ Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-24  The Rasterman  Re: how to convert a window ID to a linux process ID? 

 On Wed, 24 Mar 2004 18:13:58 +0100 Marcus Schaefer [EMAIL PROTECTED] babbled:
>>
>> Hi,
>> 
>>  I tried your example because I don't know the _NET_WM_PID property
>>  and was interested in it. Unfortunately the call to XGetWindowProperty()
>>  never filled the prop data. Your code is checking against the return
>>  code of XGetWindowProperty() which is fine but prop equals to (nil) in
>>  all my tests. I was using metacity as suggested windowmanger.
>>  
>>  Do you have any idea why it fails ? 
>>  
>>  The window Id which is required of course is obtained from a
>>  "xwininfo" call. I passed the information from this command to
>>  
>>  Using this (removed the uid stuff) works here:
>>  
>>  [EMAIL PROTECTED] fer]$ gcc test.c -L/usr/X11R6/lib/ -I/usr/X11R6/include/ -lX11
>>  -o [EMAIL PROTECTED] fer]$ xwininfo
>>   
>>  xwininfo: Please select the window about which you
>>would like information by clicking the
>>mouse in that window.
>>   
>>  xwininfo: Window id: 0x1e00012 "[EMAIL PROTECTED]:~"
>>  [...] 
>>  [EMAIL PROTECTED] fer]$ ./a.out 0x1e00012
>>  pid of window 0x1e00012 = 24109
>> 
>> Hmm, yes I see, I did it the same way but it doesn't work for me.
>> The code I use is as follows:
>> 
>> snip
>> \#include X11/Xlib.h
>> \#include X11/Xos.h
>> \#include X11/Xfuncs.h
>> \#include X11/Xutil.h
>> \#include stdio.h
>> \#include ctype.h
>> 
>> \#include X11/Xatom.h
>> \#include X11/Xmu/WinUtil.h
>> 
>> 
>> int main (int argc, char*argv[]) {
>> Atom atom,actual_type;
>> Display *dpy;
>> int screen;
>> char *atom_name;
>> int actual_format;
>> unsigned long nitems;
>> unsigned long bytes_after;
>> unsigned char *prop;
>> int status;
>> int pid;
>> Window w=0;
>> 
>> dpy = XOpenDisplay((char*)getenv("DISPLAY"));
>> screen = DefaultScreen(dpy);
>> if (argc!=2) {
>> printf("Usage %s window-id\n",argv[0]);
>> exit(-1);
>> }
>> sscanf(argv[1],"0x%x",(unsigned int*)(w));
>> atom = XInternAtom(dpy, "_NET_WM_PID", True);
>> atom_name = XGetAtomName (dpy,atom);
>> status = XGetWindowProperty(
>> dpy, w, atom, 0, 1024,
>> False, AnyPropertyType,
>> actual_type,
>> actual_format, nitems,
>> bytes_after,
>> prop
>> );
>> 
>> if (status!=0) {
>> printf("Cannot get _NET_WM_PID");
>> exit(-2);
>> }
>> if (! prop) {
>> printf ("No properties\n");
>> exit (-3);
>> }
>> 
>> pid = prop[1] * 256;
>> pid += prop[0];
>> printf("pid of window 0x%x = %d\n",(unsigned int)w,pid);
>> 
>> return 0;
>> }
>> snap---
>> 
>>   gcc id.c -L/usr/X11R6/lib/ -I/usr/X11R6/include/ -lX11 -o bla
>>   
>> I'm running XFree86 Version 4.3.99.902 (4.4.0 RC 2) on kernel
>> 2.6.4-6-default.
>> 
>> any ideas ?
>>
>>_NET_WM_PID is a VOLUNTARY property set by some apps if they want to. not all x
>>apps will set this - so windows wont necessarily have this property. see my
>>LD_PRELOAD hack for forcing a process id property to be set on ALL toplevel
>>windows of apps run under the preload
>>
>>-- 
>>- Codito, ergo sum - "I code, therefore I am" --
>>The Rasterman (Carsten Haitzler)[EMAIL PROTECTED]
 $B7'<*(B - $Bhttp://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-25  Marcus Schaefer  Re: how to convert a window ID to a linux process ID? 

 Hi,
 
  _NET_WM_PID is a VOLUNTARY property set by some apps if they want to.
  not all x  apps will set this - so windows wont necessarily have this  property. see my LD_PRELOAD hack for forcing a process id property to  be set on ALL toplevel windows of apps run under the preload
  Yes now I undesrstand, the application I tried didn't implement the _NET_WM_PID property. Well most of the gnome apps did so I tried one of these and it works. Thanks for all the information
 
 Regards
 Marcus
 \-- 
  Public Key available
  
  Marcus Schäfer (Res.  Dev.)   SUSE LINUX AG
  Tel: 0911-740 53 0 Maxfeldstrasse 5
  FAX: 0911-741 77 55D-90409 Nürnberg
  http://www.suse.de Germany
  
 
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-25  Rick Beldin  Re: how to convert a window ID to a linux process ID? 

 Perhaps some thought should be given to modify Xlib in a manner similar to the way Xt is modified with the XtDebug resource.   It sets the _MIT_OBJ_CLASS property on the window with the widget instance name and the application class name.This is happening in Intrinsic.c in RealizeWidget().
 This approach would allow you to get pid information in the absence of a window manager, such as when running apps rootless on a PC or in some other constrained environment.
 Rick
 
 --
 +--+
 | Rick Beldin|  Hewlett-Packard Company|
 | email: [EMAIL PROTECTED]  |  Global Solutions Engineering   |
 ||  20 Perimeter Summit|
 +--+
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-28  Sheldon Lee-Wen  How to get the window id of the toplevel window? was Re: how to convert a window ID to a linux process ID? 

 Hi,
 
 So I'm wondering how do I get the window id of the toplevel window? Does 
 anyone know of a command or existing code that does this?
 
 Thanks
 Sheldon.
 
 On March 25, 2004 1:51 pm, Rick Beldin wrote:
  Perhaps some thought should be given to modify Xlib in a manner similar  to the way Xt is modified with the XtDebug resource.   It sets the  _MIT_OBJ_CLASS property on the window with the widget instance name and  the application class name.This is happening in Intrinsic.c in  RealizeWidget().
 
  This approach would allow you to get pid information in the absence of  a window manager, such as when running apps rootless on a PC or in some  other constrained environment.
 
  Rick
 
 -- 
 \==
 ... all thoughts of selfish desire, ill-will, hatred and
  violence are the result of a lack of wisdom ... 
  - Buddha
 
 For an awesome fantasy role playing game checkout:
 http://lycadican.sourceforge.net
 
 GPG KeyID=04B7F7F8
 GPG Fingerprint=4B0F 7202 FAFF D146 5F56  9E83 BE7F D7F7 04B7 F7F8
 \==
 \___
 Devel mailing list
 [EMAIL PROTECTED]
 http://XFree86.Org/mailman/listinfo/devel

  

## 2004-03-28  The Rasterman  Re: How to get the window id of the toplevel window? was Re: how to convert a window ID to a linux process ID? 

 On Sun, 28 Mar 2004 15:17:10 -0500 Sheldon Lee-Wen [EMAIL PROTECTED] babbled:
>>
>> Hi,
>> 
>> So I'm wondering how do I get the window id of the toplevel window? Does 
>> anyone know of a command or existing code that does this?
>>
>>start from root - work your way down the window tree querying each child of
>>root, then each child of that child, recursively until you find a window that
>>has WM_STATE set on it - if it does - chances are you have a toplevel window,
>>regardless how many levels down a WM may have decided to reparent it, or if the
>>wm uses virtual roots or not. just do this and you can build a list of toplevels
>>easily.
>>
>>NB: the xprop/xwininfo and xkill x utilities are broken in this respect and will
>>not find toplevel windows properly in all circumstances.
>>
>> Thanks
>> Sheldon.
>> 
>> On March 25, 2004 1:51 pm, Rick Beldin wrote:
>>  Perhaps some thought should be given to modify Xlib in a manner similar
>>  to the way Xt is modified with the XtDebug resource.   It sets the
>>  _MIT_OBJ_CLASS property on the window with the widget instance name and
>>  the application class name.This is happening in Intrinsic.c in
>>  RealizeWidget().
>> 
>>  This approach would allow you to get pid information in the absence of
>>  a window manager, such as when running apps rootless on a PC or in some
>>  other constrained environment.
>> 
>>  Rick
>> 
>> -- 
>> \==
>> "... all thoughts of selfish desire, ill-will, hatred and
>>  violence are the result of a lack of wisdom ... "
>>  - Buddha
>> 
>> For an awesome fantasy role playing game checkout:
>> http://lycadican.sourceforge.net
>> 
>> GPG KeyID=04B7F7F8
>> GPG Fingerprint=4B0F 7202 FAFF D146 5F56  9E83 BE7F D7F7 04B7 F7F8
>> \==
>> \___
>> Devel mailing list
>> [EMAIL PROTECTED]
>> http://XFree86.Org/mailman/listinfo/devel
>> 
>>
>>
>>-- 
>>- Codito, ergo sum - "I code, therefore I am" --
>>The Rasterman (Carsten Haitzler)[EMAIL PROTECTED]
 $B7'<*(B - $Bhttp://XFree86.Org/mailman/listinfo/devel

  
