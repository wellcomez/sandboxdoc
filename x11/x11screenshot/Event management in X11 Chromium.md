---
date: 2023-11-01 15:22
title: Event management in X11 Chromium
tags:
  - x11-programn
---



# Event management in X11 Chromium

Posted on [October 30, 2020](https://blogs.igalia.com/jaragunde/2020/10/event-management-in-x11-chromium/ "18:00")

This is a follow-up of [my previous post](https://blogs.igalia.com/jaragunde/2020/07/27/the-trip-of-a-key-press-event-in-chromium-accessibility/), where I was trying to fix the [bug #1042864](https://bugs.chromium.org/p/chromium/issues/detail?id=1042864) in Chromium: key strokes happening on native dialogs, like open and save dialogs, were not reported to the screen reader.

After learning how accessibility tools (ATs) register listeners for key events, I found out the problem was not actually there; I had to investigate how events arrive from the X11 server to the browser, and how they are forwarded to the ATs.

[![](https://blogs.igalia.com/jaragunde/files/2020/10/desenhocs-150x150.png)](https://blogs.igalia.com/jaragunde/files/2020/10/desenhocs.png)

Not this kind of event

### EVENTS ARRIVE FROM THE X SERVER

If you are running Chromium on Linux with the X11 backend (most likely, as it is the default), the Chromium browser process receives key press events from the X server. Then, it finds out if the target of those events is one of its browser windows, and sends it to the proper Window object to be processed.

These are the classes involved in the first part of this process:  
[![](https://blogs.igalia.com/jaragunde/files/2020/10/XEvents-classes-1024x474.png)](https://blogs.igalia.com/jaragunde/files/2020/10/XEvents-classes.png)

The interface [PlatformEventSource](https://source.chromium.org/chromium/chromium/src/+/master:ui/events/platform/platform_event_source.h;l=32;bpv=1;bpt=0;drc=a60b6a5add634cf45ee443f226a04e99b0f6af42) represents an undetermined source of events coming from the platform, and a [PlatformEventDispatcher](https://source.chromium.org/chromium/chromium/src/+/master:ui/events/platform/platform_event_dispatcher.h;l=25;drc=c730d60a8ae8ef351ef0150ab2b40afff79afa92;bpv=1;bpt=1) is any object in the browser capable of managing those events, dispatching them to the actual webpage or UI element. These two classes are related, the PlatformEventSource keeps a list of dispatchers it will forward the event to, if they can manage it (CanDispatchEvent).

The [X11EventSource](https://source.chromium.org/chromium/chromium/src/+/master:ui/events/platform/x11/x11_event_source.h;l=119;bpv=1;bpt=0;drc=b960daf4e690a064e952eda23bfd87c44580f31c) class implements PlatformEventSource; it has the code managing the events coming from an X11 server, in particular. It additionally keeps a list of [XEventDispatcher](https://source.chromium.org/chromium/chromium/src/+/master:ui/events/platform/x11/x11_event_source.h;drc=929e790ad17a54008b3dac86cbe30751c968c2ce;bpv=1;bpt=0;l=37) objects, which is a class to manage X11 Event objects independently, but it’s not an implementation of PlatformEventDispatcher.

The [X11Window](https://source.chromium.org/chromium/chromium/src/+/master:ui/platform_window/x11/x11_window.h;l=46;drc=929e790ad17a54008b3dac86cbe30751c968c2ce;bpv=1;bpt=0) class is the central piece, implementing both the PlatformEventDispatcher and the XEventDispatcher interfaces, in addition to the [XWindow](https://source.chromium.org/chromium/chromium/src/+/master:ui/base/x/x11_window.h;drc=929e790ad17a54008b3dac86cbe30751c968c2ce;bpv=1;bpt=0;l=56) class. It has all the means required to find out if it can dispatch an event, and do it.

The main event processing loop looks like this:

1. An event arrives to X11EventSource.
    
2. X11EventSource loops through its list of XEventDispatcher, and calls CheckCanDispatchNextPlatformEvent for each of them.
    
3. The X11Window implementing that function checks if the XWindow ID of the event target matches the ID of the XWindow represented by that object, and saves the XEvent object if affirmative.
    
4. X11EventSource calls DispatchEvent as implemented by its parent class PlatformEventSource.
    
5. The PlatformEventSource loops through its list of PlatformEventDispatchers and calls CanDispatchEvent on each one of them.
    
6. The X11Window object, which had previously run CheckCanDispatchNextPlatformEvent, just verifies if the XEvent object was saved then, and considers that a confirmation it can dispatch the event.
    
7. When one of the dispatchers answers positively, it receives the event for processing in a call to DispatchEvent; it is implemented at X11Window.
    
8. If it’s a keyboard event, it takes the steps required to send it to any ATs listening to it, which had been previously registered via ATK.
    
9. When X11Window ends processing the event, it returns POST_DISPATCH_STOP_PROPAGATION, telling PlatformEventSource to stop looping through the rest of dispatchers.
    

This is a sequence diagram summarizing this process:  
[![](https://blogs.igalia.com/jaragunde/files/2020/07/XEvents-sequence-1024x454.png)](https://blogs.igalia.com/jaragunde/files/2020/07/XEvents-sequence.png)

### EVENTS LEAVE TO THE ATS

As explained in the [previous post](https://blogs.igalia.com/jaragunde/2020/07/27/the-trip-of-a-key-press-event-in-chromium-accessibility/), ATs can register callbacks for key press events, which ultimately call [AtkUtilClass::add_key_event_listener](https://gitlab.gnome.org/GNOME/atk/-/blob/d84ddda9106ca78c815a91aa9e9524533245373f/atk/atkutil.h#L166). AtkUtilClass is a struct of function pointers, the actual implementation is provided by Chromium in the [AtkUtilAuraLinux](https://source.chromium.org/chromium/chromium/src/+/master:ui/accessibility/platform/atk_util_auralinux.h;l=36;bpv=1;bpt=0;drc=a13d2391989bcb21856eb2031eccbb46409ad9f7) class, which keeps a list of those callbacks.

When an X11Window class encounters an event that is targetting its own X Window, and it is a keyboard event, it calls [X11ExtensionDelegate::OnAtkEvent()](https://source.chromium.org/chromium/chromium/src/+/master:ui/platform_window/extensions/x11_extension_delegate.h;l=42;drc=f6b6118a6536a9a69774da0b36975c9fb031a595;bpv=1;bpt=0?q=x11extensiondelegate&ss=chromium%2Fchromium%2Fsrc) which is actually implemented by the class [DesktopWindowTreeHostLinux](https://source.chromium.org/chromium/chromium/src/+/master:ui/views/widget/desktop_aura/desktop_window_tree_host_linux.cc;l=275;bpv=1;bpt=0;drc=00b8c64bf3adb104719465faa46489321ad05b24); it ultimately hands the event to the AtkUtilAuraLinux class and runs HandleAtkEvent(). It will loop through, and run, any listeners that may have been registered.

[![](https://blogs.igalia.com/jaragunde/files/2020/07/X11Window-classes-1024x424.png)](https://blogs.igalia.com/jaragunde/files/2020/07/X11Window-classes.png)

### NATIVE DIALOGS ARE DIFFERENT

Native dialogs are stand-alone windows in the X server, different from the browser window that called them, and the browser process doesn’t wrap them in X11Window object. It is considered unnecessary, because the windows for native dialogs talk to the X server and receive events from it directly.

They do belong to the browser process, though, which means that the browser will still receive events targetting the dialog windows. They will go through all the steps mentioned above to eventually be dismissed, because there is no X11Window object in the browser matching the ID of the target window of the process.

Another consequence of dialog windows belonging to the browser process is that the AtkUtilClass struct points to Chromium’s own implementation, and here comes the problem… The dialog is expected to manage its own events through GTK+ code, including the GTK+ implementation of AtkUtilClass, but Chromium overrode it. The key press listeners that ATs registered are kept in Chromium code, so the dialog cannot notify them.

[![](https://blogs.igalia.com/jaragunde/files/2020/10/GtkDialog-classes-1024x396.png)](https://blogs.igalia.com/jaragunde/files/2020/10/GtkDialog-classes.png)

### FINALLY, FIXING THE PROBLEM

Chromium does receive the keyboard events targetted to the dialog windows, but it does nothing with them because the target of those events is not a browser window. It gives us, though, a leg towards building a solution.

To fix the problem, I made Chromium X Windows manage the keyboard events addressed to the native dialogs in addition to their own. For that, I took advantage of the “transient” property, which indicates a dependency of one window from the other: the dialog window had been [set as transient for](https://www.x.org/releases/X11R7.6/doc/man/man3/XGetTransientForHint.3.xhtml) the browser window. In my first approach, I modified X11Window::CheckCanDispatchNextPlatformEvent() to verify if the target of the event was a transient window of the browser X Window, and in that case it would hand the event to X11ExtensionDelegate to be sent to ATs, following the code patch previously explained. It stopped processing at this point, otherwise the browser window would have received key presses directed to the dialog.

The approach had one performance problem: I was calling the X server to check that property, for every keystroke, and that call implied using synchronous IPC. This was unacceptable! But it could be worked around: we could also notify the corresponding internal X11Window object about the existence of this transient window, when the dialog is created. This implies no IPC at all, we just store one new property in the X11Window object that can be checked locally when keyboard events are processed.

This is [a link to the review process of the patch](https://chromium-review.googlesource.com/c/chromium/src/+/2228151), if you are interested in its history. To sum up, in the final solution:

1. Chromium creates the native dialog and calls [XWindow::SetTransientWindow](https://source.chromium.org/chromium/chromium/src/+/master:ui/base/x/x11_window.h;l=121;bpv=1;bpt=0;drc=c0176d627c5cfe7f7ed097c73ad6f20a49869c15), setting that property in the corresponding browser X Window.
    
2. When Chromium receives a keyboard event, it is captured by the X11Window object whose transient window property has been set before.
    
3. X11ExtensionDelegate::OnAtkEvent() is called for that event, then no more processing of this event happens in Chromium.
    
4. The native dialog code will also receive the event and manage the keystroke accordingly.
    

I hope you enjoyed this trip through Chromium event processing code. If you want to use the diagrams in this post, you may find their Dia source files [in this link](https://people.igalia.com/jaragunde/chromium/X-events-management-diagrams.zip). Happy hacking!

This entry was posted in [Accessibility](https://blogs.igalia.com/jaragunde/category/igalia/accessibility/), [Browsers](https://blogs.igalia.com/jaragunde/category/igalia/browsers/), [Chromium](https://blogs.igalia.com/jaragunde/category/igalia/chromium/), [Igalia](https://blogs.igalia.com/jaragunde/category/igalia/) by [Jacobo Aragunde Pérez](https://blogs.igalia.com/jaragunde/author/jacobo-aragunde/). Bookmark the [permalink](https://blogs.igalia.com/jaragunde/2020/10/event-management-in-x11-chromium/ "Permalink to Event management in X11 Chromium").

## 2 THOUGHTS ON “EVENT MANAGEMENT IN X11 CHROMIUM”

1. Pingback: [A recap of Chromium dialog accessibility enhancements | Jacobo's home at Igalia](https://blogs.igalia.com/jaragunde/2021/04/08/a-recap-of-chromium-dialog-accessibility-enhancements/)
    
2. Pingback: [The trip of a key press event in Chromium accessibility | Jacobo's home at Igalia](https://blogs.igalia.com/jaragunde/2020/07/27/the-trip-of-a-key-press-event-in-chromium-accessibility/)
    

### Leave a Reply

Your email address will not be published. Required fields are marked *

Comment 

Name*

Email*

Website

[Proudly powered by WordPress](https://wordpress.org/ "Semantic Personal Publishing Platform")