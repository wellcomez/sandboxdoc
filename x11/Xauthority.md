---
date: 2023-04-17 16:52
title: Xauthority
tags:
-  XAUTHORITY
- x11
---



# Xauthority

 June 22, 2022 

 Contributed by:  

B

The Linux VDA supports environments that use X11 display functionality (including `xterm` and `gvim`) for interactive remoting. This feature provides a security mechanism necessary to ensure secure communication between XClient and XServer.

There are two methods to secure permission for this secure communication:

-   **Xhost**. By default, Xhost allows only the localhost XClient to communicate with XServer. If you choose to allow a remote XClient to access XServer, the Xhost command must be run to grant permission on the specific machine. Or, you can alternately use **xhost +** to allow any XClient to connect to XServer.
-   **Xauthority**. The `.Xauthority` file can be found in each user’s home directory. It is used to store credentials in cookies used by xauth for authentication of XServer. When an XServer instance (Xorg) is started, the cookie is used to authenticate connections to that specific display.

## How it works

When Xorg starts up, a `.Xauthority` file is passed to the Xorg. This `.Xauthority` file contains the following elements:

-   Display number
-   Remote request protocol
-   Cookie number

You can browse this file using the `xauth` command. For example:

`# xauth -f ~/.Xauthority  # > list  # > us01msip06:107  MIT-MAGIC-COOKIE-1  fb228d1b695729242616c5908f11624b <!--NeedCopy-->`

If **XClient** connects to the Xorg remotely, two prerequisites must be met:

-   Set the **DISPLAY** environment variable to the remote XServer.
-   Get the `.Xauthority` file which contains one of the cookie numbers in Xorg.

## Configure **Xauthority**

To enable **Xauthority** on the Linux VDA for remote X11 display, you must create the following two registry keys:

```plaintext
sudo /opt/Citrix/VDA/bin/ctxreg create -k "HKLM\System\CurrentControlSet\Control\Citrix\Xorg" -t "REG_DWORD" -v "XauthEnabled" -d "0x00000001" --force

sudo /opt/Citrix/VDA/bin/ctxreg create -k "HKLM\System\CurrentControlSet\Control\Citrix\Xorg" -t "REG_DWORD" -v "ListenTCP" -d "0x00000001" --force
<!--NeedCopy-->
```

After enabling **Xauthority**, pass the `.Xauthority` file to the **XClient** manually or by mounting a shared home directory:

-   Pass the `.Xauthority` file to the XClient manually
    
    After launching an ICA session, the Linux VDA generates the `.Xauthority` file for the XClient and stores the file in the logon user’s home directory. You can copy this `.Xauthority` file to the remote XClient machine, and set the **DISPLAY** and **XAUTHORITY** environment variables. `DISPLAY` is the display number stored in the `.Xauthority` file and `XAUTHORITY` is the file path of **Xauthority**. For an example, see the following command:
    
    ```plaintext
     export DISPLAY={Display number stored in the Xauthority file}
    
     export XAUTHORITY={the file path of .Xauthority}
     <!--NeedCopy-->
    ```
    
    > **NOTE:**
    > 
    > If the **XAUTHORITY** environment variable is not set, the `~/.Xauthority` file is used by default.
    
-   Pass the `.Xauthority` file to the XClient by mounting a shared home directory
    
    The convenient way is to mount a shared home directory for the logon user. When the Linux VDA starts an ICA session, the `.Xauthority` file is created under the logon user’s home directory. If this home directory is shared with the XClient, the user does not need to transmit this `.Xauthority` file to the XClient manually. After the **DISPLAY** and **XAUTHORITY** environment variables are set correctly, the GUI is displayed in the XServer desktop automatically.
    

## Troubleshooting

If **Xauthority** does not work, follow the troubleshooting steps:

1.  As an administrator with root privilege, retrieve all Xorg cookies:
    
    ```plaintext
    ps aux | grep -i xorg
    <!--NeedCopy-->
    ```
    
    This command displays the Xorg process and the parameters passed to Xorg while starting. Another parameter displays which `.Xauthority` file is used. For example:
    
    ```plaintext
    /var/xdl/xauth/.Xauthority110
    <!--NeedCopy-->
    ```
    
    Display the cookies using the **Xauth** command:
    
    ```plaintext
    Xauth -f /var/xdl/xauth/.Xauthority110
    <!--NeedCopy-->
    ```
    
2.  Use the `Xauth` command to show the cookies contained in `~/.Xauthority`. For the same display number, the displayed cookies must be the same in the `.Xauthority` files of Xorg and XClient.
    
3.  If the cookies are the same, check the remote display port accessibility by using the IP address of the Linux VDA and the published desktop display number.
    
    For example, run the following command on the XClient machine:
    
    ```plaintext
    telnet 10.158.11.11 6160
    <!--NeedCopy-->
    ```
    
    The port number is the sum of 6000 + <display number>.
    
    If this telnet operation fails, the firewall might be blocking the request.