---
date: 2023-04-17 16:41
title: xauth
tags:
-  XAUTHORITY
- x11
---



# XAUTH

[NAME](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading1)  
[SYNOPSIS](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading2)  
[DESCRIPTION](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading3)  
[OPTIONS](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading4)  
[COMMANDS](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading5)  
[DISPLAY NAMES](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading6)  
[EXAMPLE](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading7)  
[ENVIRONMENT](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading8)  
[FILES](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading9)  
[SEE ALSO](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading10)  
[BUGS](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading11)  
[AUTHOR](https://www.x.org/archive/X11R7.7/doc/man/man1/xauth.1.xhtml#heading12)  

---

## NAME

xauth − X authority file utility

## SYNOPSIS

**xauth** [ **−f** _authfile_ ] [ **−vqibn** ] [ _command arg ..._ ]

## DESCRIPTION

The _xauth_ program is used to edit and display the authorization information used in connecting to the X server. This program is usually used to extract authorization records from one machine and merge them in on another (as is the case when using remote logins or granting access to other users). Commands (described below) may be entered interactively, on the _xauth_ command line, or in scripts. Note that this program does **not** contact the X server except when the generate command is used. Normally _xauth_ is not used to create the authority file entry in the first place; the program that starts the X server (often _xdm_ or _startx_) does that.

## OPTIONS

The following options may be used with _xauth_. They may be given individually (e.g., _−q −i_) or may combined (e.g., _−qi_).**  
−f** _authfile_

This option specifies the name of the authority file to use. By default, _xauth_ will use the file specified by the XAUTHORITY environment variable or _.Xauthority_ in the user’s home directory.

**−q**

This option indicates that _xauth_ should operate quietly and not print unsolicited status messages. This is the default if an _xauth_ command is given on the command line or if the standard output is not directed to a terminal.

**−v**

This option indicates that _xauth_ should operate verbosely and print status messages indicating the results of various operations (e.g., how many records have been read in or written out). This is the default if _xauth_ is reading commands from its standard input and its standard output is directed to a terminal.

**−i**

This option indicates that _xauth_ should ignore any authority file locks. Normally, _xauth_ will refuse to read or edit any authority files that have been locked by other programs (usually _xdm_ or another _xauth_).

**−b**

This option indicates that _xauth_ should attempt to break any authority file locks before proceeding. Use this option only to clean up stale locks.

**−n**

This option indicates that _xauth_ should not attempt to resolve any hostnames, but should simply always print the host address as stored in the authority file.

## COMMANDS

The following commands may be used to manipulate authority files:**  
add** _displayname protocolname hexkey_

An authorization entry for the indicated display using the given protocol and key data is added to the authorization file. The data is specified as an even-lengthed string of hexadecimal digits, each pair representing one octet. The first digit of each pair gives the most significant 4 bits of the octet, and the second digit of the pair gives the least significant 4 bits. For example, a 32 character hexkey would represent a 128-bit value. A protocol name consisting of just a single period is treated as an abbreviation for _MIT-MAGIC-COOKIE-1_.

**generate** _displayname protocolname_ [**trusted|untrusted**]

[**timeout** _seconds_] [**group** _group-id_] [**data** _hexdata_]

This command is similar to add. The main difference is that instead of requiring the user to supply the key data, it connects to the server specified in _displayname_ and uses the SECURITY extension in order to get the key data to store in the authorization file. If the server cannot be contacted or if it does not support the SECURITY extension, the command fails. Otherwise, an authorization entry for the indicated display using the given protocol is added to the authorization file. A protocol name consisting of just a single period is treated as an abbreviation for _MIT-MAGIC-COOKIE-1_.

If the **trusted** option is used, clients that connect using this authorization will have full run of the display, as usual. If **untrusted** is used, clients that connect using this authorization will be considered untrusted and prevented from stealing or tampering with data belonging to trusted clients. See the SECURITY extension specification for full details on the restrictions imposed on untrusted clients. The default is **untrusted**.

The **timeout** option specifies how long in seconds this authorization will be valid. If the authorization remains unused (no clients are connected with it) for longer than this time period, the server purges the authorization, and future attempts to connect using it will fail. Note that the purging done by the server does **not** delete the authorization entry from the authorization file. The default timeout is 60 seconds.

The **group** option specifies the application group that clients connecting with this authorization should belong to. See the application group extension specification for more details. The default is to not belong to an application group.

The **data** option specifies data that the server should use to generate the authorization. Note that this is **not** the same data that gets written to the authorization file. The interpretation of this data depends on the authorization protocol. The _hexdata_ is in the same format as the _hexkey_ described in the add command. The default is to send no data.

**[n]extract** _filename displayname..._

Authorization entries for each of the specified displays are written to the indicated file. If the _nextract_ command is used, the entries are written in a numeric format suitable for non-binary transmission (such as secure electronic mail). The extracted entries can be read back in using the _merge_ and _nmerge_ commands. If the filename consists of just a single dash, the entries will be written to the standard output.

**[n]list** [_displayname_...]

Authorization entries for each of the specified displays (or all if no displays are named) are printed on the standard output. If the _nlist_ command is used, entries will be shown in the numeric format used by the _nextract_ command; otherwise, they are shown in a textual format. Key data is always displayed in the hexadecimal format given in the description of the _add_ command.

**[n]merge** [_filename_...]

Authorization entries are read from the specified files and are merged into the authorization database, superseding any matching existing entries. If the _nmerge_ command is used, the numeric format given in the description of the _extract_ command is used. If a filename consists of just a single dash, the standard input will be read if it hasn’t been read before.

**remove** _displayname_...

Authorization entries matching the specified displays are removed from the authority file.

**source** _filename_

The specified file is treated as a script containing _xauth_ commands to execute. Blank lines and lines beginning with a sharp sign (#) are ignored. A single dash may be used to indicate the standard input, if it hasn’t already been read.

**info**

Information describing the authorization file, whether or not any changes have been made, and from where _xauth_ commands are being read is printed on the standard output.

**exit**

If any modifications have been made, the authority file is written out (if allowed), and the program exits. An end of file is treated as an implicit _exit_ command.

**quit**

The program exits, ignoring any modifications. This may also be accomplished by pressing the interrupt character.

**help [**_string_**]**

A description of all commands that begin with the given string (or all commands if no string is given) is printed on the standard output.

**?**

A short list of the valid commands is printed on the standard output.

## DISPLAY NAMES

Display names for the _add_, _[n]extract_, _[n]list_, _[n]merge_, and _remove_ commands use the same format as the DISPLAY environment variable and the common _−display_ command line argument. Display-specific information (such as the screen number) is unnecessary and will be ignored. Same-machine connections (such as local-host sockets, shared memory, and the Internet Protocol hostname _localhost_) are referred to as _hostname_/unix:_displaynumber_ so that local entries for different machines may be stored in one authority file.

## EXAMPLE

The most common use for _xauth_ is to extract the entry for the current display, copy it to another machine, and merge it into the user’s authority file on the remote machine:

% xauth extract − $DISPLAY | ssh otherhost xauth merge −

The following command contacts the server :0 to create an authorization using the MIT-MAGIC-COOKIE-1 protocol. Clients that connect with this authorization will be untrusted.  
% xauth generate :0 .

## ENVIRONMENT

This _xauth_ program uses the following environment variables:**  
XAUTHORITY**

to get the name of the authority file to use if the _−f_ option isn’t used.

**HOME**

to get the user’s home directory if XAUTHORITY isn’t defined.

## FILES

_$HOME/.Xauthority_

default authority file if XAUTHORITY isn’t defined.

## SEE ALSO

[_X_(7)](https://www.x.org/archive/X11R7.7/doc/man/man7/X.7.xhtml), [_Xsecurity_(7)](https://www.x.org/archive/X11R7.7/doc/man/man7/Xsecurity.7.xhtml), [_xhost_(1)](https://www.x.org/archive/X11R7.7/doc/man/man1/xhost.1.xhtml), [_Xserver_(1)](https://www.x.org/archive/X11R7.7/doc/man/man1/Xserver.1.xhtml), [_xdm_(1)](https://www.x.org/archive/X11R7.7/doc/man/man1/xdm.1.xhtml), [_startx_(1)](https://www.x.org/archive/X11R7.7/doc/man/man1/startx.1.xhtml), [_Xau_(3)](https://www.x.org/archive/X11R7.7/doc/man/man3/Xau.3.xhtml).

## BUGS

Users that have unsecure networks should take care to use encrypted file transfer mechanisms to copy authorization entries between machines. Similarly, the _MIT-MAGIC-COOKIE-1_ protocol is not very useful in unsecure environments. Sites that are interested in additional security may need to use encrypted authorization mechanisms such as Kerberos.

Spaces are currently not allowed in the protocol name. Quoting could be added for the truly perverse.

## AUTHOR

Jim Fulton, MIT X Consortium

---