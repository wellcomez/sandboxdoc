---
date: 2023-05-04 21:34
title: X Window System protocols and architecture
tags:
- x11
- x11protocol
---


  

# X Window System protocols and architecture


The X Window System logo

In [computing](https://en.wikipedia.org/wiki/Computing "Computing"), the [X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System") (commonly: X11, or X) is a [network-transparent](https://en.wikipedia.org/wiki/Network_transparency "Network transparency") [windowing system](https://en.wikipedia.org/wiki/Windowing_system "Windowing system") for [bitmap](https://en.wikipedia.org/wiki/Raster_graphics "Raster graphics") displays. This article details the protocols and technical structure of X11.

## Client–server model and network transparency

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/X_client_server_example.svg/250px-X_client_server_example.svg.png)](https://en.wikipedia.org/wiki/File:X_client_server_example.svg)

In this example, the X server takes input from a keyboard and mouse and displays to a screen. A [web browser](https://en.wikipedia.org/wiki/Web_browser "Web browser") and a [terminal emulator](https://en.wikipedia.org/wiki/Terminal_emulator "Terminal emulator") run on the user's workstation, and a terminal emulator runs on a remote server but under the control of the user's machine. Note that the remote application runs just as it would locally.

X uses a [client–server model](https://en.wikipedia.org/wiki/Client%E2%80%93server_model "Client–server model"). An _X server_ program runs on a computer with a graphical display and communicates with various _client programs_. The X server acts as a go-between for the user and the client programs, accepting requests on TCP port 6000 for graphical output (windows) from the client programs and displaying them to the user (display), and receiving user input (keyboard, mouse) and transmitting it to the client programs.

In X, the server runs on the user's computer, while the clients may run on remote machines. This terminology reverses the common notion of client–server systems, where the _client_ normally runs on the user's local computer and the server runs on the remote computer. The X Window terminology takes the perspective that the X Window program is at the centre of all activity, i.e. the X Window program accepts and responds to requests from applications, and from the user's mouse and keyboard input. Therefore, applications (on remote computers) are viewed as clients of the X Window server program.

The [communication protocol](https://en.wikipedia.org/wiki/Communication_protocol "Communication protocol") between server and client runs [network-transparently](https://en.wikipedia.org/wiki/Network_transparency "Network transparency"): the client and server may run on the same machine or on different ones, possibly with different [architectures](https://en.wikipedia.org/wiki/Computer_architecture "Computer architecture") and [operating systems](https://en.wikipedia.org/wiki/Operating_system "Operating system"). A client and server can communicate [securely](https://en.wikipedia.org/wiki/Computer_security "Computer security") over the [Internet](https://en.wikipedia.org/wiki/Internet "Internet") by [tunneling](https://en.wikipedia.org/wiki/Tunneling_protocol "Tunneling protocol") the connection over an encrypted connection.[[1]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-Client%E2%80%93server_model_1-1)

## Design principles

[Bob Scheifler](https://en.wikipedia.org/wiki/Bob_Scheifler "Bob Scheifler") and [Jim Gettys](https://en.wikipedia.org/wiki/Jim_Gettys "Jim Gettys") set out the early principles of X as follows (as listed in Scheifler/Gettys 1996):

-   _Do not add new functionality unless an implementor cannot complete a real application without it._
-   _It is as important to decide what a system is not as to decide what it is. Do not serve all the world's needs; rather, make the system extensible so that additional needs can be met in an upwardly compatible fashion._
-   _The only thing worse than [generalizing from one example](https://en.wikipedia.org/wiki/Casuistry "Casuistry") is generalizing from no examples at all._
-   _If a problem is not completely understood, it is probably best to provide no solution at all._
-   _If you can get 90 percent of the desired effect for 10 percent of the work, use the simpler solution._ (See also [Worse is better](https://en.wikipedia.org/wiki/Worse_is_better "Worse is better").)
-   _Isolate complexity as much as possible._
-   _Provide mechanism rather than policy. In particular, place user interface policy in the clients' hands._

The first principle was modified during the design of X11 to: _Do not add new functionality unless you know of some real application that will require it._

X has largely kept to these principles since. The [X.Org Foundation](https://en.wikipedia.org/wiki/X.Org_Foundation "X.Org Foundation") develops the [reference implementation](https://en.wikipedia.org/wiki/X.Org_Server "X.Org Server") with a view to extension and improvement of the implementation, whilst keeping it almost entirely compatible with the original 1987 protocol.

## Core protocol

Main article: [X Window System core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol")

Communication between server and clients is done by exchanging packets over a network [channel](https://en.wikipedia.org/wiki/Channel_(communications) "Channel (communications)"). The client establishes the connection, sending the first packet. The server answers by sending back a packet stating the acceptance or refusal of the connection, or with a request for a further authentication. If the connection is accepted, the acceptance packet contains data for the client to use in the subsequent interaction with the server.

After connection is established, the client and the server exchange four different types of packets over the channel:

1.  **Request:** the client requests information from the server or requests it to perform an action.
2.  **Reply:** the server responds to a request. Not all requests generate replies.
3.  **Event:** the server sends an event to the client, e.g., keyboard or mouse input, or a window being moved, resized or exposed.
4.  **Error:** the server sends an error packet if a request is invalid. Since requests are queued, error packets generated by a request may not be sent immediately.

The X server provides a set of basic services. The client programs realize more complex functionalities by interacting with the server.

### Windows

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Some_X_windows.svg/378px-Some_X_windows.svg.png)](https://en.wikipedia.org/wiki/File:Some_X_windows.svg)

A possible placement of some windows: 1 is the root window, which covers the whole screen; 2 and 3 are top-level windows; 4 and 5 are subwindows of 2. The parts of window that are outside its parent are not visible.

What other [graphical user interfaces](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface") usually call a _window_ is a _top-level window_ in the X Window System. The term _window_ is also used for windows that lie within another window, that is, the _subwindows_ of a _parent window_. Graphical elements such as buttons, menus, icons, etc. are all realized using windows.

A window can only be created as a subwindow of a parent window. This causes the windows to be arranged [hierarchically](https://en.wikipedia.org/wiki/Hierarchy "Hierarchy") in a [tree](https://en.wikipedia.org/wiki/Tree_(graph_theory) "Tree (graph theory)"). The X server automatically creates the root of the tree, called the [root window](https://en.wikipedia.org/wiki/Root_window "Root window"). The top-level windows are exactly the direct subwindows of the root window. Visibly, the root window is as large as the screen, and lies behind all other windows.

### Identifiers

The X server stores all data about windows, fonts, etc. The client knows identifiers of these objects – integers it can use as names for them when interacting with the server. For example, if a client wishes a window to be created, it requests the server to create one and (in case of success) gets in return an identifier the server associated with the newly created window. The identifier can be later used by the client to request, for example, a string to be drawn in the window.

Identifiers are unique to the server, not only to the client; for example, no two windows have the same identifier, even if created by two different clients. A client can access any object given its identifier, even if another client created the object.

### Attributes and properties

Every window has a predefined set of attributes and a set of properties, all stored in the X server and accessible to the clients via appropriate requests. Attributes are data about the window, such as its size, position, background color, etc. Properties are pieces of data that are attached to a window. Unlike attributes, properties have no meaning at the level of the X Window core protocol. A client can store arbitrary data in a property of a window.

A property is characterized by a name, a type, and a value. Properties resemble [variables](https://en.wikipedia.org/wiki/Variable_(programming) "Variable (programming)") in [imperative programming languages](https://en.wikipedia.org/wiki/Imperative_programming_language "Imperative programming language"), in that the application can create a new property with a given name and of a given type and store a value in it. Properties are associated to windows: two properties with the same name can exist on two different windows while having different types and values.

Properties are mostly used for inter-client communication. For example, the property named `WM_NAME` stores the name for the window; window managers typically read this property and display the name of the window at the top of it.

The `xprop` program can display the properties of a window. In particular, `xprop -root` shows the properties of the root window, which include the [X resources](https://en.wikipedia.org/wiki/X_resources "X resources") (parameters of programs).

### Events

Events are packets sent by the server to the client to communicate that something has happened which may interest the client. A client can request the server to send an event to another client; this is used for communication between clients. For example, when a client requests the text that is currently selected, an event is sent to the client that is currently handling the window that holds the selection.

The content of a window may be "destroyed" in some conditions (for example, if the window is covered). Whenever an area of destroyed content is made visible, the server generates an `Expose` event to notify the client that a part of the window has to be drawn.

Other events can serve to notify clients of keyboard or mouse input, of the creation of new windows, etc.

Some kinds of events are always sent to a client, but most kinds of event are sent only if the client previously stated an interest in them, because clients may only be interested in some kind of events. For example, a client may be interested in keyboard-related event but not in mouse-related events.

### Color modes

The way the X Window System handles colors can sometimes confuse users, and historically several different modes have been supported. Most modern applications use [full color](https://en.wikipedia.org/wiki/24-bit_color "24-bit color") (24-bit color, 8 bits for each of red, green and blue), but old or specialist applications may require a different color mode. Many commercial specialist applications use [PseudoColor](https://en.wikipedia.org/w/index.php?title=PseudoColor&action=edit&redlink=1 "PseudoColor (page does not exist)").

The X11 protocol actually uses a single 32-bit unsigned integer - called a _pixelvalue_ - for representing a single color in most graphic operations. When transferring the intensity of [primary colors](https://en.wikipedia.org/wiki/Primary_color "Primary color"), a 16 bit integer is used for each color component. The following representations of colors exist; not all of them may be supported on a specific device.

-   **DirectColor:** A pixel value is decomposed into separate red, green, and blue subfields. Each subfield indexes a separate colormap. Entries in all colormaps can be changed.
    -   **TrueColor:** Same as DirectColor, except that the colormap entries are predefined by the hardware and cannot be changed. Typically, each of the red, green, and blue colormaps provides a (near) linear ramp of intensity.
-   **GrayScale:** A pixel value indexes a single colormap that contains monochrome intensities. Colormap entries can be changed.
    -   **StaticGray:** Same as GrayScale, except that the colormap entries are predefined by the hardware and cannot be changed.
-   **PseudoColor** ([Chunky](https://en.wikipedia.org/wiki/Packed_pixel "Packed pixel")): A pixel value indexes a single colormap that contains color intensities. Colormap entries can be changed.
    -   **StaticColor:** Same as PseudoColor, except that the colormap entries are predefined by the hardware and cannot be changed.

See also: [X11 color names](https://en.wikipedia.org/wiki/X11_color_names "X11 color names")

## Xlib and other client libraries

Main article: [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")

Most client programs communicate with the server via the [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib") client library. Beside Xlib, the [XCB](https://en.wikipedia.org/wiki/XCB "XCB") library operates more closely to X protocol. In particular, most clients use libraries such as [Xaw](https://en.wikipedia.org/wiki/Xaw "Xaw"), [Motif](https://en.wikipedia.org/wiki/Motif_(software) "Motif (software)"), [GTK+](https://en.wikipedia.org/wiki/GTK%2B "GTK+"), or [Qt](https://en.wikipedia.org/wiki/Qt_(toolkit) "Qt (toolkit)") which in turn use [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib") for interacting with the server. [Qt](https://en.wikipedia.org/wiki/Qt_(toolkit) "Qt (toolkit)") switched from Xlib to [XCB](https://en.wikipedia.org/wiki/XCB "XCB") with the 5.0 release, but client programs were almost entirely unaffected by this change.

## Inter-client communication

The X Window core protocol provides mechanisms for communication between clients: window properties and events, in particular the client-to-client message events. However, it does not specify any protocol for such interactions. Instead, a separate set of inter-client communication conventions governs these protocols.

The [Inter-Client Communication Conventions Manual](https://en.wikipedia.org/wiki/Inter-Client_Communication_Conventions_Manual "Inter-Client Communication Conventions Manual") specifies the protocol for the exchange of data via selections and the interaction of applications with the window manager. Some have considered this specification difficult and confusing;[[2]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-The_X-Windows_Disaster-2)[[3]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-Armed_and_Dangerous-3) consistency of application [look and feel](https://en.wikipedia.org/wiki/Look_and_feel "Look and feel") and communication is typically addressed by programming to a given desktop environment.

The Inter-Client Exchange protocol (ICE) specifies a framework for building protocols for interaction between clients, so that programmers can build a specific protocol on top of it. In particular, the X Session Management protocol (XSMP) is a protocol based on ICE that governs the interaction between applications with the [session manager](https://en.wikipedia.org/wiki/Session_management "Session management"), which is the program that takes care of storing the status of the desktop at the end of an interactive session and recovering it when another session with the same user is started again.

The [freedesktop](https://en.wikipedia.org/wiki/Freedesktop "Freedesktop") specifications include newer conventions, including the drag-and-drop convention Xdnd (used for transferring data by selecting it and dragging in another window) and the embedded application convention Xembed (which details how an application can run in a subwindow of another application).

### Selections, cut buffers, and drag-and-drop

Main article: [X Window selection](https://en.wikipedia.org/wiki/X_Window_selection "X Window selection")[[X Window selection]]

The mechanisms of selections, cut buffers, and drag-and-drop in the X Window System allow a user to transfer data from one window to another. Selections and cut buffer are used (typically) when a user selects text or some other data in a window and pastes in a different window. Drag-and-drop is used when a user selects something in a window, then clicks on the selection and drags it into another window.

Since two different applications may handle the two windows, data transfer requires different clients connected with the same X server to interact. The X Window core protocol includes some types of requests and events that are specific to selection exchange, but the transfer is mainly done using the general client-to-client event sending and window properties, which are not specific to selection transfer.

Users can transfer data of different types between clients: it is usually text, but can also be a pixmap, a number, a list of objects, etc.

Selections and drag-and-drop are active mechanisms: after the user selects data in a window, the client handling the window must actively support a protocol for transferring that data to the application requesting it. [Cut buffers](https://en.wikipedia.org/wiki/Cut_buffer "Cut buffer"), by contrast, provide a passive mechanism: when the user selects some text, its content is transferred to a cut buffer, where it remains even if the application handling the window terminates and the window is destroyed.

### Window manager

Main article: [X window manager](https://en.wikipedia.org/wiki/X_window_manager "X window manager")

A window manager is a program that controls the general appearance of windows and other graphical elements of the [graphical user interface](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface"). Differences in the look of X Window System in different installations stem mainly from the use of different window managers or from different configurations of the window manager.

The window manager takes care of deciding the position of windows, placing the decorative border around them, handling icons, handling mouse clicks outside windows (on the “background”), handling certain keystrokes, etc.

From the point of view of the X server, the window manager operates as a client just like any other client. The initial position and the decorative borders around windows are handled by the window manager using the following requests:

1.  an application can request the server not to satisfy requests of mapping (showing) subwindows of a given window, and to be sent an event instead;
2.  an application can request changing the parent of a window.

The window manager uses the first request to intercept any request for mapping top-level windows (children of the root window). Whenever another application requests the mapping of a top-level window, the server does not do it but sends an event to the window manager instead. Most window managers [reparent](https://en.wikipedia.org/wiki/Re-parenting_window_manager "Re-parenting window manager") the window: they create a larger top-level window (called the frame window) and reparent the original window as a child of it. Graphically, this corresponds to placing the original window inside the frame window. The space of the frame window that is not taken by the original window is used for the decorative frame around the window (the “border” and the “title bar”).

The window manager manages mouse clicks in the frame window. This allows, for example, a user to move or resize the window by clicking and dragging on the border or on the title bar.

The window manager also handles icons and related visual elements of the graphical user interface. Icons do not exist at the level of the X Window core protocol. They are implemented by the window manager. For example, whenever a window has to be “iconified”, the [FVWM](https://en.wikipedia.org/wiki/FVWM "FVWM") window manager unmaps the window, and creates a window for the icon name and possibly another window for the icon image. The meaning and handling of icons is therefore completely decided by the window manager: some window managers such as [wm2](https://en.wikipedia.org/wiki/Wm2 "Wm2") do not implement icons at all.

### Session manager

Main article: [X session manager](https://en.wikipedia.org/wiki/X_session_manager "X session manager")

Roughly, the state of a [session](https://en.wikipedia.org/wiki/Session_management "Session management") is the “state of the desktop” at a given time: a set of windows with their current content. More precisely, it is the set of applications managing these windows and the information that allow these applications to restore the condition of their managed windows if required. A program known as the X session manager saves and restores the state of sessions.

Most recognizably, using a session manager permits a user to log out from an interactive session but to find exactly the same windows in the same state when logging in again. For this to work, the session manager program stores the names of the running applications at logout and starts them again at login. In order for the state of the applications to be restored as well (which is needed to restore the content of windows), the applications must be able to save their state of execution upon request from the session manager and load it back when they start again.

The X Window System includes a default session manager called `xsm`. Developers have written other session managers for specific desktop systems. Major examples include `ksmserver`, `xfce4-session`, and `gnome-session` for [KDE](https://en.wikipedia.org/wiki/KDE "KDE"), [Xfce](https://en.wikipedia.org/wiki/Xfce "Xfce"), and [GNOME](https://en.wikipedia.org/wiki/Gnome "Gnome") respectively.

## X display manager

Main article: [X display manager](https://en.wikipedia.org/wiki/X_display_manager "X display manager")

The program known as the [X display manager](https://en.wikipedia.org/wiki/X_display_manager "X display manager") shows the graphical login prompt in the X Window System. More generally, a display manager runs one or more X servers on the local computer or accepts incoming connections from X servers running on remote computers. The local servers are started by the display manager, which then connects to them to present the user the login screen. The remote servers are started independently from the display manager and connect to it. In this situation, the display manager works like a graphical [telnet](https://en.wikipedia.org/wiki/Telnet "Telnet") server: an X server can connect to the display manager, which starts a session; the applications which utilize this session run on the same computer of the display manager but have input and output on the computer where the X server runs (which may be the computer in front of the user or a remote one).

The X Window System ships with [XDM](https://en.wikipedia.org/wiki/XDM_(display_manager) "XDM (display manager)") as the basic supplied display manager. Other display managers include [GDM](https://en.wikipedia.org/wiki/GNOME_Display_Manager "GNOME Display Manager") ([GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME")), [KDM](https://en.wikipedia.org/wiki/KDE_Display_Manager "KDE Display Manager")/[SDDM](https://en.wikipedia.org/wiki/Simple_Desktop_Display_Manager "Simple Desktop Display Manager") ([KDE](https://en.wikipedia.org/wiki/KDE "KDE")), [WDM](https://en.wikipedia.org/wiki/WINGs_Display_Manager "WINGs Display Manager") (using the WINGs widget set used in [Window Maker](https://en.wikipedia.org/wiki/Window_Maker "Window Maker")) and [entrance](https://en.wikipedia.org/wiki/Entrance_(display_manager) "Entrance (display manager)") (using the architecture used in [Enlightenment](https://en.wikipedia.org/wiki/Enlightenment_(software) "Enlightenment (software)") v.17).

## User interface elements

Early [widget toolkits](https://en.wikipedia.org/wiki/Widget_toolkit "Widget toolkit") for X included [Xaw](https://en.wikipedia.org/wiki/X_Athena_Widgets "X Athena Widgets") (the [Athena](https://en.wikipedia.org/wiki/Project_Athena "Project Athena") Widget Set, 1983), [OLIT](https://en.wikipedia.org/wiki/OLIT "OLIT") ([OPEN LOOK](https://en.wikipedia.org/wiki/OPEN_LOOK "OPEN LOOK") Intrinsics Toolkit, 1988), [XView](https://en.wikipedia.org/wiki/XView "XView") (1988), [Motif](https://en.wikipedia.org/wiki/Motif_(software) "Motif (software)") (1980s) and [Tk](https://en.wikipedia.org/wiki/Tk_(software) "Tk (software)"). OLIT and XView function as the base toolkits for [Sun](https://en.wikipedia.org/wiki/Sun_Microsystems "Sun Microsystems")'s legacy [OpenWindows](https://en.wikipedia.org/wiki/OpenWindows "OpenWindows") desktop environment.

Motif provides the base toolkit for the [Common Desktop Environment](https://en.wikipedia.org/wiki/Common_Desktop_Environment "Common Desktop Environment") (CDE), the desktop environment used on commercial [Unix](https://en.wikipedia.org/wiki/Unix "Unix") systems such as [Solaris](https://en.wikipedia.org/wiki/Solaris_(operating_system) "Solaris (operating system)"), [AIX](https://en.wikipedia.org/wiki/IBM_AIX "IBM AIX") and [HP-UX](https://en.wikipedia.org/wiki/HP-UX "HP-UX"). (Solaris 10 includes both CDE and [GNOME](https://en.wikipedia.org/wiki/GNOME "GNOME"), with the latter the preferred desktop environment as of 2010.)

Toolkits developed more recently include [Qt](https://en.wikipedia.org/wiki/Qt_(software) "Qt (software)") (1991- , used by [KDE](https://en.wikipedia.org/wiki/KDE "KDE")), [GTK+](https://en.wikipedia.org/wiki/GTK%2B "GTK+") (1997- , used by GNOME), [wxWidgets](https://en.wikipedia.org/wiki/WxWidgets "WxWidgets") (1992- ), [FLTK](https://en.wikipedia.org/wiki/FLTK "FLTK") (1998- ), [FOX](https://en.wikipedia.org/wiki/Fox_toolkit "Fox toolkit") (1997- ) and [fpGUI](https://en.wikipedia.org/wiki/FpGUI "FpGUI") (2005-current).

## Extensions

Scheifler and Gettys designed the X server to be simple but extensible. As such, much functionality now resides in extensions to the protocol.

At the protocol level, every extension can be assigned new request/event/error packet types. Extension features are accessed by client applications through extension libraries. Adding extensions to current X server implementations is reportedly difficult due to a lack of modularity in the server design.[[4]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-The_X_Window_System-4) It is a long-term goal of the [XCB](https://en.wikipedia.org/wiki/XCB "XCB") project to automate generating both the client and server sides of extensions from XML protocol descriptions.

The following table provides a partial catalog of extensions that have been developed, sorted roughly by recency of introduction:

Extension

Description and notes

Composite

Off-screen rendering of entire window hierarchies, allowing applications and composition managers to do effects anywhere along the way. Required for things like alpha transparency for windows and drop shadows.

Damage

Tracks modified regions of windows, and minimizes bandwidth use required to keep the display up to date.

[XFixes](https://en.wikipedia.org/wiki/XFixes "XFixes")

Several protocol changes.

Extended-Visual-Information (EVIE)

Allows client to determine information about core X visuals beyond what the core protocol provides.

Distributed Multihead (DMX)

Communicates with DMX X server.

[X-Video Motion Compensation](https://en.wikipedia.org/wiki/X-Video_Motion_Compensation "X-Video Motion Compensation") (XvMC)

Offloading video motion compensation to a GPU that supports it.

[GLX](https://en.wikipedia.org/wiki/GLX "GLX")

Support for rendering [OpenGL](https://en.wikipedia.org/wiki/OpenGL "OpenGL") within windows.

[XRender](https://en.wikipedia.org/wiki/XRender "XRender")

Hardware accelerated image compositing with alpha blending.

[Resize and Rotate (RANDR)](https://en.wikipedia.org/wiki/XRandR "XRandR")

Dynamically change the size, reflection, rotation and refresh rate of an X screen.

[Xinerama](https://en.wikipedia.org/wiki/Xinerama "Xinerama")

Splitting the desktop across multiple monitors.

Display Power Management Signaling ([DPMS](https://en.wikipedia.org/wiki/VESA_Display_Power_Management_Signaling "VESA Display Power Management Signaling"))

Allows controlling monitor power saving modes.

[X keyboard extension](https://en.wikipedia.org/wiki/X_keyboard_extension "X keyboard extension")

Enhanced keyboard layout handling.

DOUBLE-BUFFER

Gives flicker-free animation.

RECORD

Used in server testing

[MIT-SHM](https://en.wikipedia.org/wiki/MIT-SHM "MIT-SHM")

Use of shared memory to improve performance.

SYNC

Provides timers and synchronizes clients (e.g. running on different hosts and operating systems) from within the X server. Created because of errors introduced by the network.

XTEST

For generating fake input. Use i.e. the `xte` application from the [XAutomation](http://hoopajoo.net/projects/xautomation.html) package.

XInputExtension

Support for input devices such as graphic tablets; implementable as libXi[[5]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-5)

BIG-REQUESTS

Enables requests exceeding 262,140 bytes in length.

XC-MISC

Fixes resource ID range overrun[[6]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-6)

[X video extension](https://en.wikipedia.org/wiki/X_video_extension "X video extension")

Support for hardware video overlays and hardware-based video scaling on playback. Also called Xv (not to be confused with the [xv](https://en.wikipedia.org/wiki/Xv_(software) "Xv (software)") program).

[Shape](https://en.wikipedia.org/wiki/Shape_extension "Shape extension")

Support for non-rectangular and partially transparent (binary, no alpha opacity) windows.

MIT-SCREEN-SAVER

Launches a program when the X server turns on the built-in screen saver

SECURITY

Provides enhanced server security[[7]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7)

X-Resource

Allows querying the X server about its resource usage

XFree86-Bigfont

XFree86-DGA

Provides direct linear framebuffer access (direct graphics access). To be mostly removed in X.Org 7.6[[8]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-8)

XFree86-VidModeExtension

Dynamically configures modelines and gamma.

### Obsolete extensions

Extension

Description and notes

[Low Bandwidth X](https://en.wikipedia.org/wiki/Low_Bandwidth_X "Low Bandwidth X") (LBX)

Replaced by [VNC](https://en.wikipedia.org/wiki/Virtual_Network_Computing "Virtual Network Computing") [tunneled](https://en.wikipedia.org/wiki/Tunneling_protocol "Tunneling protocol") over a secure shell connection, proved faster than LBX.

[PEX](https://en.wikipedia.org/wiki/PHIGS "PHIGS")

"PHIGS Extension to X"; support for PHIGS 3D scene graph API. GLX with OpenGL is frequently used instead.

XImage Extension

MIT-SHM is used instead.

XEvIE

X Event Interception Extension. Designed for users who need to intercept all keyboard and mouse events. Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

XFree86-Misc

Superseded by _input properties_. Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

DEC-XTRAP

XTrap allows a controlling X client to emulate a real user's interaction with an X workstation. Registering itself to monitor visual output, actual keyboard and mouse input, and to simulate device input. A typical use case would be for precise performance measurement of user-perceived interactive response time, in milliseconds. A user emulation script can be recorded and then played back, repeatedly. XTrap was released open-source by engineers at Digital Equipment Corporation around 1988. Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

TOG-CUP

Provides colormap utilization policy. Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

MIT-SUNDRY-NONSTANDARD

Support for various backwards compatibility features for clients which used early implementations of X11. For compatibility with Pre-X11R4 clients. Disabled since 2006. Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

XC-APPGROUP

Removed in X.Org 7.5.[[9]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-7.5release-9)

[XPRINT](https://en.wikipedia.org/wiki/XPRINT "XPRINT")

Deprecated. Allows an application to render output to a printer just as it would to any other display device. Removed in May 2008.[[10]](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_note-10)

## See also

-   [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")
-   [Intrinsics](https://en.wikipedia.org/wiki/X_Toolkit_Intrinsics "X Toolkit Intrinsics") (Xt)
-   [X Window System core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol")
-   [X logical font description](https://en.wikipedia.org/wiki/X_logical_font_description "X logical font description")

## Notes

1.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-Client%E2%80%93server_model_1_1-0 "Jump up")** Client–server model
    
    -   [IBM 1994](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#IBM1994), pp.2-11
    -   [Maguolo 2005](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#Maguolo2005)
    -   [Manrique 2001](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#Manrique2001)
    -   [Stevens 1994](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#Stevens1994), pp.430-433
    -   [Quercia & O'Reilly 1993](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#Quercia1993), pp.13-17
    
2.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-The_X-Windows_Disaster_2-0 "Jump up")** Hopkins, Don (uncredited) (May 1994). [Garfinkel, Simson](https://en.wikipedia.org/wiki/Simson_Garfinkel "Simson Garfinkel"); Weise, Daniel; Strassmann, Steven (eds.). [_The UNIX-Haters Handbook_](https://archive.org/details/unixhatershandbo0000unse/page/126) (PDF). San Mateo, CA, USA: [IDG Books](https://en.wikipedia.org/wiki/IDG_Books "IDG Books"). p. [126 The X-Windows Disaster](https://archive.org/details/unixhatershandbo0000unse/page/126). [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [978-1-56884-203-5](https://en.wikipedia.org/wiki/Special:BookSources/978-1-56884-203-5 "Special:BookSources/978-1-56884-203-5"). [OCLC](https://en.wikipedia.org/wiki/OCLC_(identifier) "OCLC (identifier)") [30681401](https://www.worldcat.org/oclc/30681401). Retrieved July 11, 2011. The ICCCM is unbelievably dense, it must be followed to the last letter, and it still doesn't work. ICCCM compliance is one of the most complex ordeals of implementing X toolkits, window managers, and even simple applications. It's so difficult, that many of the benefits just aren't worth the hassle of compliance.
3.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-Armed_and_Dangerous_3-0 "Jump up")** [Raymond, Eric S.](https://en.wikipedia.org/wiki/Eric_S._Raymond "Eric S. Raymond") (September 30, 2008). ["The Unix Hater's Handbook, Reconsidered"](http://esr.ibiblio.org/?p=538). _Armed and Dangerous_. Retrieved July 11, 2011. ICCCM is about as horrible as the [Unix Hater's Handbook's] authors describe, but that's hard to notice these days because modern toolkits and window managers do a pretty good job of hiding the ugliness from applications.
4.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-The_X_Window_System_4-0 "Jump up")** [Gettys, James](https://en.wikipedia.org/wiki/James_Gettys "James Gettys"); Karlton, Philip L.; [McGregor, Scott](https://en.wikipedia.org/wiki/Scott_A._McGregor "Scott A. McGregor") (10 December 1990). ["The X Window System, Version 11"](http://www.hpl.hp.com/techreports/Compaq-DEC/CRL-90-8.pdf) (PDF). [Digital Equipment Corporation](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation "Digital Equipment Corporation") and [Silicon Graphics Computer Systems](https://en.wikipedia.org/wiki/Silicon_Graphics "Silicon Graphics"). p. 36. Retrieved July 11, 2011. X11 does not permit the read back of all information that may have been stored in the server (for example, the X11 protocol does not permit querying the GC state). This makes modularity somewhat harder to accomplish.
5.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-5 "Jump up")** ["X.org libXi Client library for XInput"](http://cgit.freedesktop.org/xorg/lib/libXi/tree/README). Retrieved 2010-03-02. libXi - library for the X Input Extension
6.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-6 "Jump up")** ["XC-MISC Extension"](https://web.archive.org/web/20110927020019/http://www.xfree86.org/current/xc-misc.pdf) (PDF). Archived from [the original](http://www.xfree86.org/current/xc-misc.pdf) (PDF) on September 27, 2011. Retrieved 2010-08-02.
7.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7 "Jump up")** ["Security Extension Specification"](https://web.archive.org/web/20110927020037/http://www.xfree86.org/current/security.pdf) (PDF). Archived from [the original](http://www.xfree86.org/current/security.pdf) (PDF) on September 27, 2011. Retrieved 2010-08-02.
8.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-8 "Jump up")** _Disable XFree86-DGA requests except relative mouse motion, until Xinput 2 allows killing it all._ [X.Org Wiki - Releases/7.6](http://www.x.org/wiki/Releases/7.6)
9.  ^ [Jump up to:_**a**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-0) [_**b**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-1) [_**c**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-2) [_**d**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-3) [_**e**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-4) [_**f**_](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-7.5release_9-5) [7.5 release announcement](http://www.x.org/wiki/Releases/7.5)
10.  **[^](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#cite_ref-10 "Jump up")** [Commit removing XPrint](http://cgit.freedesktop.org/xorg/xserver/commit/?id=1c8bd318fbaf65890ef16fe26c76dd5e6f14dfde)

## References

1.  Manrique, Daniel (May 23, 2001). ["The X Window System Architecture: overview"](http://tldp.org/HOWTO/XWindow-Overview-HOWTO/arch-overview.html). _X Window System Architecture Overview HOWTO_. [The Linux Documentation Project](https://en.wikipedia.org/wiki/The_Linux_Documentation_Project "The Linux Documentation Project"). Retrieved July 13, 2011.
2.  Maguolo, Filippo (December 16, 2005). ["The Architecture of X-Window"](http://lions-wing.net/lessons/customize/The%20Architecture%20of%20X-Window.html). _Lessons in Linux_. Mount Kisco, NY, USA: John F. Moore. Retrieved July 13, 2011.
3.  [Stevens, W. Richard](https://en.wikipedia.org/wiki/W._Richard_Stevens "W. Richard Stevens") (1994). ["30.5 X Window System"](https://sites.google.com/site/baoshili/tcpipillv1.pdf#pagemode=none&view=fit&page=430) (PDF). [_TCP/IP Illustrated_](https://archive.org/details/tcpipillustrated00stev/page/30) (PDF). Addison-Wesley professional computing series. Vol. 1, TheProtocols (1 ed.). Boston, MA, USA: [Addison-Wesley](https://en.wikipedia.org/wiki/Addison-Wesley "Addison-Wesley"). [30.5 X Window System](https://archive.org/details/tcpipillustrated00stev/page/30). [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [978-0-201-63346-7](https://en.wikipedia.org/wiki/Special:BookSources/978-0-201-63346-7 "Special:BookSources/978-0-201-63346-7"). [OCLC](https://en.wikipedia.org/wiki/OCLC_(identifier) "OCLC (identifier)") [246049781](https://www.worldcat.org/oclc/246049781). Retrieved July 13, 2011.
4.  [IBM Corporation, International Technical Support Center](https://en.wikipedia.org/wiki/IBM "IBM") (July 1994). ["1.2 X Concepts"](http://www.redbooks.ibm.com/redbooks/pdfs/gg243911.pdf#pagemode=none&view=fit&page=24) (PDF). [_TCP/IP for MVS, VM, OS/2 and DOS: X Window System Guide_](http://www.redbooks.ibm.com/redbooks/pdfs/gg243911.pdf) (PDF). [IBM Redbooks](https://en.wikipedia.org/w/index.php?title=IBM_Redbooks&action=edit&redlink=1 "IBM Redbooks (page does not exist)") (Second ed.). Research Triangle Park, NC, USA: [IBM](https://en.wikipedia.org/wiki/IBM "IBM"). X Concepts. Retrieved July 13, 2011.
5.  Quercia, Valerie; [O'Reilly, Tim](https://en.wikipedia.org/wiki/Tim_O%27Reilly "Tim O'Reilly") (1993) [1988]. [_X Window System User's Guide : for X11 release 5_](https://archive.org/details/xwindowsystem03quermiss). Definitive guides to the X Window System. Vol. 3. Sebastopol, CA, USA: [O'Reilly & Assoc.](https://en.wikipedia.org/wiki/O%27Reilly_Media "O'Reilly Media") [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [978-1-56592-014-9](https://en.wikipedia.org/wiki/Special:BookSources/978-1-56592-014-9 "Special:BookSources/978-1-56592-014-9"). [OCLC](https://en.wikipedia.org/wiki/OCLC_(identifier) "OCLC (identifier)") [682229836](https://www.worldcat.org/oclc/682229836). [LCC](https://en.wikipedia.org/wiki/LCC_(identifier) "LCC (identifier)") [QA76.76.W56 Q47](https://catalog.loc.gov/vwebv/search?searchCode=CALL%2B&searchArg=QA76.76.W56+Q47&searchType=1&recCount=25). Retrieved July 14, 2011. archive.org has the 1990 edition.

## Further reading

-   Robert W. Scheifler and James Gettys: _X Window System: Core and extension protocols, X version 11, releases 6 and 6.1_, Digital Press 1996, [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [978-1-55558-148-0](https://en.wikipedia.org/wiki/Special:BookSources/978-1-55558-148-0 "Special:BookSources/978-1-55558-148-0")
-   ["An Introduction to X11 User Interfaces"](https://web.archive.org/web/20070103060448/http://www.visi.com/~grante/Xtut/). Archived from [the original](http://www.visi.com/~grante/Xtut/) on 3 January 2007.
-   [Introduction to X Windows](https://web.archive.org/web/20070103133417/http://www.its.strath.ac.uk/courses/x/)[sic]
-   Gettys, Jim (9 December 2003). ["Open Source Desktop Technology Road Map"](https://web.archive.org/web/20060102002339/http://freedesktop.org/~jg/roadmap.html). Archived from [the original](http://freedesktop.org/~jg/roadmap.html) on 2 January 2006.

## External links

-   [X.Org Foundation](http://www.x.org/) (official home page)
-   [X.Org Foundation wiki](http://xorg.freedesktop.org/wiki/)
-   [X Window System Internals](http://xwindow.angelfire.com/)
-   [Kenton Lee's pages on X Window and Motif](http://www.rahul.net/kenton/bib.html)
-   [X11 Extension tutorial](https://web.archive.org/web/20100712025419/http://www.x.org/wiki/X11_Extension_tutorial)

hide

-   [v](https://en.wikipedia.org/wiki/Template:XWinSys "Template:XWinSys")
-   [t](https://en.wikipedia.org/wiki/Template_talk:XWinSys "Template talk:XWinSys")
-   [e](https://en.wikipedia.org/w/index.php?title=Template:XWinSys&action=edit)

[X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System")

Architecture

-   **[Core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol")**
-   [X Window selection](https://en.wikipedia.org/wiki/X_Window_selection "X Window selection")
-   [X Window authorization](https://en.wikipedia.org/wiki/X_Window_authorization "X Window authorization")
-   [X11 color names](https://en.wikipedia.org/wiki/X11_color_names "X11 color names")
-   [X Input Method](https://en.wikipedia.org/wiki/X_Input_Method "X Input Method")
-   [Wayland](https://en.wikipedia.org/wiki/Wayland_(display_server_protocol) "Wayland (display server protocol)")

[![X11.svg](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/X11.svg/64px-X11.svg.png)](https://en.wikipedia.org/wiki/File:X11.svg)

Extensions

-   [X Image Extension](https://en.wikipedia.org/wiki/X_Image_Extension "X Image Extension")
-   [X keyboard extension](https://en.wikipedia.org/wiki/X_keyboard_extension "X keyboard extension")
-   [RandR](https://en.wikipedia.org/wiki/RandR "RandR")
-   [X Rendering Extension](https://en.wikipedia.org/wiki/X_Rendering_Extension "X Rendering Extension")
-   [Shape extension](https://en.wikipedia.org/wiki/Shape_extension "Shape extension")
-   [Shared memory extension](https://en.wikipedia.org/wiki/MIT-SHM "MIT-SHM")
-   [X Display Manager Control Protocol](https://en.wikipedia.org/wiki/XDM_(display_manager) "XDM (display manager)")
-   [X video extension](https://en.wikipedia.org/wiki/X_video_extension "X video extension")
-   [X-Video Motion Compensation](https://en.wikipedia.org/wiki/X-Video_Motion_Compensation "X-Video Motion Compensation")
-   [AIGLX](https://en.wikipedia.org/wiki/AIGLX "AIGLX")
-   [GLX](https://en.wikipedia.org/wiki/GLX "GLX")
-   [Multi-Pointer X](https://en.wikipedia.org/wiki/Multi-Pointer_X "Multi-Pointer X")
-   [Display PostScript](https://en.wikipedia.org/wiki/Display_PostScript "Display PostScript")
-   [Composite](https://en.wikipedia.org/wiki/Composite_(graphics) "Composite (graphics)")
-   [Xinerama](https://en.wikipedia.org/wiki/Xinerama "Xinerama")

Components  
and notable  
implementations

[Display servers](https://en.wikipedia.org/wiki/Display_server "Display server")

-   **[X.Org Server](https://en.wikipedia.org/wiki/X.Org_Server "X.Org Server")**
-   [Cygwin/X](https://en.wikipedia.org/wiki/Cygwin/X "Cygwin/X")
-   [X-Win32](https://en.wikipedia.org/wiki/X-Win32 "X-Win32")
-   [X386](https://en.wikipedia.org/wiki/X386 "X386")
-   [XFree86](https://en.wikipedia.org/wiki/XFree86 "XFree86")
-   [XDarwin](https://en.wikipedia.org/wiki/XDarwin "XDarwin")
-   [Xming](https://en.wikipedia.org/wiki/Xming "Xming")
-   [Xsgi](https://en.wikipedia.org/wiki/Xsgi "Xsgi")
-   [Xsun](https://en.wikipedia.org/wiki/Xsun "Xsun")
-   [Xgl](https://en.wikipedia.org/wiki/Xgl "Xgl")
-   [Xephyr](https://en.wikipedia.org/wiki/Xephyr "Xephyr")
-   [XQuartz](https://en.wikipedia.org/wiki/XQuartz "XQuartz")
-   [XWayland](https://en.wikipedia.org/wiki/XWayland "XWayland")

Client libraries

-   [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")
-   [XCB](https://en.wikipedia.org/wiki/XCB "XCB")
-   [X Toolkit Intrinsics](https://en.wikipedia.org/wiki/X_Toolkit_Intrinsics "X Toolkit Intrinsics")

[Display managers](https://en.wikipedia.org/wiki/X_display_manager "X display manager")  
[Session managers](https://en.wikipedia.org/wiki/X_session_manager "X session manager")

-   [GDM](https://en.wikipedia.org/wiki/GNOME_Display_Manager "GNOME Display Manager")
-   [KDM](https://en.wikipedia.org/wiki/KDE_Display_Manager "KDE Display Manager")
-   [SDDM](https://en.wikipedia.org/wiki/Simple_Desktop_Display_Manager "Simple Desktop Display Manager")
-   [XDM](https://en.wikipedia.org/wiki/XDM_(display_manager) "XDM (display manager)")
-   [LightDM](https://en.wikipedia.org/wiki/LightDM "LightDM")
-   [LXDM](https://en.wikipedia.org/wiki/LXDE "LXDE")
-   [SLiM](https://en.wikipedia.org/wiki/SLiM "SLiM")
-   [Entrance](https://en.wikipedia.org/wiki/Entrance_(display_manager) "Entrance (display manager)")
-   [WDM](https://en.wikipedia.org/wiki/WINGs_Display_Manager "WINGs Display Manager")

[Window managers](https://en.wikipedia.org/wiki/X_window_manager "X window manager")  
([comparison](https://en.wikipedia.org/wiki/Comparison_of_X_window_managers "Comparison of X window managers"))

[Compositing](https://en.wikipedia.org/wiki/Compositing_window_manager "Compositing window manager")

-   [Compiz](https://en.wikipedia.org/wiki/Compiz "Compiz")
-   [KWin](https://en.wikipedia.org/wiki/KWin "KWin")
-   [Metacity](https://en.wikipedia.org/wiki/Metacity "Metacity")
-   [Mutter](https://en.wikipedia.org/wiki/Mutter_(software) "Mutter (software)")
-   [Xfwm](https://en.wikipedia.org/wiki/Xfwm "Xfwm")
-   [Enlightenment](https://en.wikipedia.org/wiki/Enlightenment_(software) "Enlightenment (software)")

[Stacking](https://en.wikipedia.org/wiki/Stacking_window_manager "Stacking window manager")

-   [4Dwm](https://en.wikipedia.org/wiki/4Dwm "4Dwm")
-   [9wm](https://en.wikipedia.org/wiki/9wm "9wm")
-   [AfterStep](https://en.wikipedia.org/wiki/AfterStep "AfterStep")
-   [amiwm](https://en.wikipedia.org/wiki/Amiwm "Amiwm")
-   [Blackbox](https://en.wikipedia.org/wiki/Blackbox "Blackbox")
-   [CTWM](https://en.wikipedia.org/wiki/CTWM "CTWM")
-   [cwm](https://en.wikipedia.org/wiki/Cwm_(window_manager) "Cwm (window manager)")
-   [Fluxbox](https://en.wikipedia.org/wiki/Fluxbox "Fluxbox")
-   [FLWM](https://en.wikipedia.org/wiki/FLWM "FLWM")
-   [FVWM](https://en.wikipedia.org/wiki/FVWM "FVWM")
-   [IceWM](https://en.wikipedia.org/wiki/IceWM "IceWM")
-   [JWM](https://en.wikipedia.org/wiki/JWM "JWM")
-   [Matchbox](https://en.wikipedia.org/wiki/Matchbox_(window_manager) "Matchbox (window manager)")
-   [mwm](https://en.wikipedia.org/wiki/Motif_Window_Manager "Motif Window Manager")
-   [olwm](https://en.wikipedia.org/wiki/Olwm "Olwm")
-   [Openbox](https://en.wikipedia.org/wiki/Openbox "Openbox")
-   [Qvwm](https://en.wikipedia.org/wiki/Qvwm "Qvwm")
-   [Sawfish](https://en.wikipedia.org/wiki/Sawfish_(window_manager) "Sawfish (window manager)")
-   [swm](https://en.wikipedia.org/wiki/Swm "Swm")
-   [tvtwm](https://en.wikipedia.org/wiki/Tvtwm "Tvtwm")
-   [twm](https://en.wikipedia.org/wiki/Twm "Twm")
-   [vtwm](https://en.wikipedia.org/wiki/Vtwm "Vtwm")
-   [WindowLab](https://en.wikipedia.org/wiki/WindowLab "WindowLab")
-   [Window Maker](https://en.wikipedia.org/wiki/Window_Maker "Window Maker")
-   [wm2](https://en.wikipedia.org/wiki/Wm2 "Wm2")

[Tiling](https://en.wikipedia.org/wiki/Tiling_window_manager "Tiling window manager")

-   [awesome](https://en.wikipedia.org/wiki/Awesome_(window_manager) "Awesome (window manager)")
-   [dwm](https://en.wikipedia.org/wiki/Dwm "Dwm")
-   [i3](https://en.wikipedia.org/wiki/I3_(window_manager) "I3 (window manager)")
-   [Ion](https://en.wikipedia.org/wiki/Ion_(window_manager) "Ion (window manager)")
-   [ratpoison](https://en.wikipedia.org/wiki/Ratpoison "Ratpoison")
-   [wmii](https://en.wikipedia.org/wiki/Wmii "Wmii")
-   [xmonad](https://en.wikipedia.org/wiki/Xmonad "Xmonad")
-   [StumpWM](https://en.wikipedia.org/wiki/StumpWM "StumpWM")
-   [larswm](https://en.wikipedia.org/wiki/Larswm "Larswm")

Standards

-   [ICCCM](https://en.wikipedia.org/wiki/Inter-Client_Communication_Conventions_Manual "Inter-Client Communication Conventions Manual")
-   [EWMH](https://en.wikipedia.org/wiki/Extended_Window_Manager_Hints "Extended Window Manager Hints")
-   [XDS](https://en.wikipedia.org/wiki/Direct_Save_Protocol "Direct Save Protocol")
-   [freedesktop.org](https://en.wikipedia.org/wiki/Freedesktop.org "Freedesktop.org")

Applications

-   [xcalc](https://en.wikipedia.org/wiki/Xcalc "Xcalc")
-   [xclock](https://en.wikipedia.org/wiki/Xclock "Xclock")
-   [xedit](https://en.wikipedia.org/wiki/Xedit_(X11) "Xedit (X11)")
-   [xload](https://en.wikipedia.org/wiki/Xload "Xload")
-   [xterm](https://en.wikipedia.org/wiki/Xterm "Xterm")
-   [xeyes](https://en.wikipedia.org/wiki/Xeyes "Xeyes")
-   [Desktop environments (comparison)](https://en.wikipedia.org/wiki/Comparison_of_X_Window_System_desktop_environments "Comparison of X Window System desktop environments")

[Categories](https://en.wikipedia.org/wiki/Help:Category "Help:Category"): 

-   [X Window System](https://en.wikipedia.org/wiki/Category:X_Window_System "Category:X Window System")
-   [Application layer protocols](https://en.wikipedia.org/wiki/Category:Application_layer_protocols "Category:Application layer protocols")
-   [Remote desktop protocols](https://en.wikipedia.org/wiki/Category:Remote_desktop_protocols "Category:Remote desktop protocols")
-   [X Window extensions](https://en.wikipedia.org/wiki/Category:X_Window_extensions "Category:X Window extensions")

-   This page was last edited on 21 May 2022, at 19:39 (UTC).
-   Text is available under the [Creative Commons Attribution-ShareAlike License 3.0](https://en.wikipedia.org/wiki/Wikipedia:Text_of_the_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License); additional terms may apply. By using this site, you agree to the [Terms of Use](https://foundation.wikimedia.org/wiki/Terms_of_Use) and [Privacy Policy](https://foundation.wikimedia.org/wiki/Privacy_policy). Wikipedia® is a registered trademark of the [Wikimedia Foundation, Inc.](https://www.wikimediafoundation.org/), a non-profit organization.

-   [Privacy policy](https://foundation.wikimedia.org/wiki/Privacy_policy)
-   [About Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:About)
-   [Disclaimers](https://en.wikipedia.org/wiki/Wikipedia:General_disclaimer)
-   [Contact Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Contact_us)
-   [Mobile view](https://en.m.wikipedia.org/w/index.php?title=X_Window_System_protocols_and_architecture&mobileaction=toggle_view_mobile)
-   [Developers](https://developer.wikimedia.org/)
-   [Statistics](https://stats.wikimedia.org/#/en.wikipedia.org)
-   [Cookie statement](https://foundation.wikimedia.org/wiki/Cookie_statement)

-   [![Wikimedia Foundation](https://en.wikipedia.org/static/images/footer/wikimedia-button.png)](https://wikimediafoundation.org/)
-   [![Powered by MediaWiki](https://en.wikipedia.org/static/images/footer/poweredby_mediawiki_88x31.png)](https://www.mediawiki.org/)

Toggle limited content width