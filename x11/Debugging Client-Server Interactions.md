---
date: 2023-04-17 15:25
title: Debugging Client-Server Interactions
tags:
- x11
- x11-traffic
- xrestop
---



# Debugging Client-Server Interactions

_Alan Coopersmith_

Debugging X client applications presents additional challenges, as often problems are not found solely in the running program but involve the interaction with the X server, and may need monitoring of what the X server is doing or of the communications between the X client and server. There are several tools available for doing this.

1.  [Event reporting](https://www.x.org/wiki/guide/debugging/#index1h2)
2.  [The X-Resource extension](https://www.x.org/wiki/guide/debugging/#index2h2)
3.  [Protocol monitors](https://www.x.org/wiki/guide/debugging/#index3h2)
4.  [System level tracing](https://www.x.org/wiki/guide/debugging/#index4h2)

## Event reporting

Sometimes a problem is as simple as trying to figure out what event the X server sends for a certain key or button press, or other input. The xev program included in the X.Org sample applications provides a simple way to do this - simply run xev, give focus to its window, and cause the event to happen, and it will print out the details. For instance, a press and release of the "e" key on a keyboard generates this output from xev:

```
KeyPress event, serial 115, synthetic NO, window 0x4a00001,
root 0x15a, subw 0x0, time 170495410, (948,265), root:(966,365),
state 0x0, keycode 26 (keysym 0x65, e), same_screen YES,
XLookupString gives 1 bytes: (65) "e"
XmbLookupString gives 1 bytes: (65) "e"
XFilterEvent returns: False

KeyRelease event, serial 115, synthetic NO, window 0x4a00001,
root 0x15a, subw 0x0, time 170495531, (948,265), root:(966,365),
state 0x0, keycode 26 (keysym 0x65, e), same_screen YES,
XLookupString gives 1 bytes: (65) "e"
XFilterEvent returns: False
```

## The X-Resource extension

Clients can make several types of request to the X server to allocate memory in the X server on the clients behalf, such as creating windows or pixmaps. Often times when users complain of X server memory growth it is due to clients making these allocations and not cleaning them up. While the X server will free most resources associated with a client when the client exits, that doesn't stop long-lived clients like web browsers from growing the memory while the client is running.

To help resolve this, the X-Resource extension was created to the to allow clients to get insight into the allocations that have been performed for each client in the X server. Several clients are available that can query this information. In GNOME, the Performance Monitor application can display a total X server memory usage per client by enabling the X Server Memory column in the information fields shown in the Preferences.

More detailed information per client, breaking down usage by type of resource, is displayed by the xrestop program, available from [http://www.freedesktop.org/wiki/Software/xrestop](http://www.freedesktop.org/wiki/Software/xrestop).

## Protocol monitors

Several programs can monitor an X protocol connection and display decoded packets to show what requests clients are making and the replies, errors, and events the server is sending in response. These include:

-   xscope
    -   Sources in [http://www.x.org/releases/individual/app/](http://www.x.org/releases/individual/app/).
    -   Described in [http://www.x.org/releases/X11R7.6/doc/man/man1/xscope.1.xhtml](http://www.x.org/releases/X11R7.6/doc/man/man1/xscope.1.xhtml) and [http://jklp.org/public/profession/papers/xscope/paper.htm](http://jklp.org/public/profession/papers/xscope/paper.htm)
-   [x11vis](http://x11vis.org/)
-   [xmon](http://xmon.sourceforge.net/)
-   [xtrace](http://xtrace.alioth.debian.org/)
-   [wireshark](http://www.wireshark.org/)

The first four of these are X protocol proxy servers - before you start your client, you must first start the proxy and configure it as a new xserver that connects to your current X server. For instance a common configuration is the Xorg server running as :0, and xscope running on :1. The client then needs to be set to connect to the xscope server, which in turn prints the communications it passes back and forth between the real server and client.

One caveat with `xtrace` is that you may need the `-n` option in order to bypass authentication problems. For example:

```
jcomeau@aspire:~$ xtrace -o /tmp/skype.xtrace skype
No display name to create specified, trying :9
Error parsing xauth list data: less than three things in a line!
```

But with `-n`:

```
jcomeau@aspire:~$ xtrace -n -o /tmp/skype.xtrace skype
No display name to create specified, trying :9
Got connection from unknown(local)
...
[starts skype successfully]
```

[Can someone write about xmon & x11vis? I've never actually used them]

Wireshark is a network protocol monitor. Unlike the proxy-style monitors, it can be started at any time during the life of the client, but it only monitors connections going over tcp sockets, not local connections. It's actually a general purpose network protocol analyzer, supporting hundreds of protocols, of which X11 is just one.

## System level tracing

The DTrace facility on Solaris, MacOS, and FreeBSD, and the SystemTap facility on Linux, provide the ability to trace operations across programs and correlate them between the client and server.

Examples and more details can be found in the "Xserver provider for DTrace" document under [http://www.x.org/releases/current/doc/](http://www.x.org/releases/current/doc/) as well as at [https://blogs.oracle.com/alanc/tags/dtrace](https://blogs.oracle.com/alanc/tags/dtrace).