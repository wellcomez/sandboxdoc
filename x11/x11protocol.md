---
date: 2023-05-05 10:06
title: x11protocol
tags:
- x11
- x11protocol
---



# x11protocol

https://www.x.org/releases/current/doc/xproto/x11protocol.html

## Chapter 8. Connection Setup

**Table of Contents**

[Connection Initiation](https://www.x.org/releases/current/doc/xproto/x11protocol.html#connection_initiation)

[Server Response](https://www.x.org/releases/current/doc/xproto/x11protocol.html#server_response)

[Server Information](https://www.x.org/releases/current/doc/xproto/x11protocol.html#server_information)

[Screen Information](https://www.x.org/releases/current/doc/xproto/x11protocol.html#screen_information)

[Visual Information](https://www.x.org/releases/current/doc/xproto/x11protocol.html#visual_information)

For remote clients, the X protocol can be built on top of any reliable byte stream.

### Connection Initiation

The client must send an initial byte of data to identify the byte order to be employed. The value of the byte must be octal 102 or 154. The value 102 (ASCII uppercase B) means values are transmitted most significant byte first, and value 154 (ASCII lowercase l) means values are transmitted least significant byte first. Except where explicitly noted in the protocol, all 16-bit and 32-bit quantities sent by the client must be transmitted with this byte order, and all 16-bit and 32-bit quantities returned by the server will be transmitted with this byte order.

Following the byte-order byte, the client sends the following information at connection setup:

> protocol-major-version: CARD16
> 
> protocol-minor-version: CARD16
> 
> authorization-protocol-name: STRING8
> 
> authorization-protocol-data: STRING8

The version numbers indicate what version of the protocol the client expects the server to implement.

The authorization name indicates what authorization (and authentication) protocol the client expects the server to use, and the data is specific to that protocol. Specification of valid authorization mechanisms is not part of the core X protocol. A server that does not implement the protocol the client expects or that only implements the host-based mechanism may simply ignore this information. If both name and data strings are empty, this is to be interpreted as "no explicit authorization."

### Server Response

The client receives the following information at connection setup:

-   success: { **Failed**, **Success**, **Authenticate**}
    

The client receives the following additional data if the returned success value is **Failed**, and the connection is not successfully established:

> protocol-major-version: CARD16
> 
> protocol-minor-version: CARD16
> 
> reason: STRING8

The client receives the following additional data if the returned success value is **Authenticate**, and further authentication negotiation is required:

> reason: STRING8

The contents of the reason string are specific to the authorization protocol in use. The semantics of this authentication negotiation are not constrained, except that the negotiation must eventually terminate with a reply from the server containing a success value of **Failed** or **Success**.

The client receives the following additional data if the returned success value is **Success**, and the connection is successfully established:

> protocol-major-version: CARD16
> 
> protocol-minor-version: CARD16
> 
> vendor: STRING8
> 
> release-number: CARD32
> 
> resource-id-base, resource-id-mask: CARD32
> 
> image-byte-order: { **LSBFirst**, **MSBFirst** }
> 
> bitmap-scanline-unit: {8, 16, 32}
> 
> bitmap-scanline-pad: {8, 16, 32}
> 
> bitmap-bit-order: { **LeastSignificant**, **MostSignificant** }
> 
> pixmap-formats: LISTofFORMAT
> 
> roots: LISTofSCREEN
> 
> motion-buffer-size: CARD32
> 
> maximum-request-length: CARD16
> 
> min-keycode, max-keycode: KEYCODE
> 
> where:
> 
> > FORMAT:
> > 
> > [depth: CARD8,
> > 
> >  
> > 
> > bits-per-pixel: {1, 4, 8, 16, 24, 32}
> > 
> >  
> > 
> > scanline-pad: {8, 16, 32}]
> > 
> > SCREEN:
> > 
> > [root: WINDOW
> > 
> >  
> > 
> > width-in-pixels, height-in-pixels: CARD16
> > 
> >  
> > 
> > width-in-millimeters, height-in-millimeters: CARD16
> > 
> >  
> > 
> > allowed-depths: LISTofDEPTH
> > 
> >  
> > 
> > root-depth: CARD8
> > 
> >  
> > 
> > root-visual: VISUALID
> > 
> >  
> > 
> > default-colormap: COLORMAP
> > 
> >  
> > 
> > white-pixel, black-pixel: CARD32
> > 
> >  
> > 
> > min-installed-maps, max-installed-maps: CARD16
> > 
> >  
> > 
> > backing-stores: {Never, WhenMapped, Always}
> > 
> >  
> > 
> > save-unders: BOOL
> > 
> >  
> > 
> > current-input-masks: SETofEVENT]
> > 
> > DEPTH:
> > 
> > [depth: CARD8
> > 
> >  
> > 
> > visuals: LISTofVISUALTYPE]
> > 
> > VISUALTYPE:
> > 
> > [visual-id: VISUALID
> > 
> >  
> > 
> > class: {StaticGray, StaticColor, TrueColor, GrayScale, PseudoColor, DirectColor}
> > 
> >  
> > 
> > red-mask, green-mask, blue-mask: CARD32
> > 
> >  
> > 
> > bits-per-rgb-value: CARD8
> > 
> >  
> > 
> > colormap-entries: CARD16]

### Server Information

The information that is global to the server is:

The protocol version numbers are an escape hatch in case future revisions of the protocol are necessary. In general, the major version would increment for incompatible changes, and the minor version would increment for small upward compatible changes. Barring changes, the major version will be 11, and the minor version will be 0. The protocol version numbers returned indicate the protocol the server actually supports. This might not equal the version sent by the client. The server can (but need not) refuse connections from clients that offer a different version than the server supports. A server can (but need not) support more than one version simultaneously.

The vendor string gives some identification of the owner of the server implementation. The vendor controls the semantics of the release number.

The resource-id-mask contains a single contiguous set of bits (at least 18). The client allocates resource IDs for types WINDOW, PIXMAP, CURSOR, FONT, GCONTEXT, and COLORMAP by choosing a value with only some subset of these bits set and ORing it with resource-id-base. Only values constructed in this way can be used to name newly created resources over this connection. Resource IDs never have the top three bits set. The client is not restricted to linear or contiguous allocation of resource IDs. Once an ID has been freed, it can be reused. An ID must be unique with respect to the IDs of all other resources, not just other resources of the same type. However, note that the value spaces of resource identifiers, atoms, visualids, and keysyms are distinguished by context, and as such, are not required to be disjoint; for example, a given numeric value might be both a valid window ID, a valid atom, and a valid keysym.

Although the server is in general responsible for byte-swapping data to match the client, images are always transmitted and received in formats (including byte order) specified by the server. The byte order for images is given by image-byte-order and applies to each scanline unit in XY format (bitmap format) and to each pixel value in Z format.

A bitmap is represented in [_scanline order_](https://www.x.org/releases/current/doc/xproto/x11protocol.html#glossary:Scanline_order). Each [_scanline_](https://www.x.org/releases/current/doc/xproto/x11protocol.html#glossary:Scanline) is padded to a multiple of bits as given by bitmap-scanline-pad. The pad bits are of arbitrary value. The scanline is quantized in multiples of bits as given by bitmap-scanline-unit. The bitmap-scanline-unit is always less than or equal to the bitmap-scanline-pad. Within each unit, the leftmost bit in the bitmap is either the least significant or most significant bit in the unit, as given by bitmap-bit-order. If a pixmap is represented in XY format, each plane is represented as a bitmap, and the planes appear from most significant to least significant in bit order with no padding between planes.

Pixmap-formats contains one entry for each [_depth_](https://www.x.org/releases/current/doc/xproto/x11protocol.html#glossary:Depth) value. The entry describes the [_Z format_](https://www.x.org/releases/current/doc/xproto/x11protocol.html#glossary:ZFormat) used to represent images of that depth. An entry for a depth is included if any screen supports that depth, and all screens supporting that depth must support only that Z format for that depth. In Z format, the pixels are in scanline order, left to right within a scanline. The number of bits used to hold each pixel is given by bits-per-pixel. Bits-per-pixel may be larger than strictly required by the depth, in which case the least significant bits are used to hold the pixmap data, and the values of the unused high-order bits are undefined. When the bits-per-pixel is 4, the order of nibbles in the byte is the same as the image byte-order. When the bits-per-pixel is 1, the format is identical for bitmap format. Each scanline is padded to a multiple of bits as given by scanline-pad. When bits-per-pixel is 1, this will be identical to bitmap-scanline-pad.

How a pointing device roams the screens is up to the server implementation and is transparent to the protocol. No geometry is defined among screens.

The server may retain the recent history of pointer motion and do so to a finer granularity than is reported by [**MotionNotify**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#events:MotionNotify) events. The [**GetMotionEvents**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:GetMotionEvents "GetMotionEvents") request makes such history available. The motion-buffer-size gives the approximate maximum number of elements in the history buffer.

Maximum-request-length specifies the maximum length of a request accepted by the server, in 4-byte units. That is, length is the maximum value that can appear in the length field of a request. Requests larger than this maximum generate a **Length** error, and the server will read and simply discard the entire request. Maximum-request-length will always be at least 4096 (that is, requests of length up to and including 16384 bytes will be accepted by all servers).

Min-keycode and max-keycode specify the smallest and largest keycode values transmitted by the server. Min-keycode is never less than 8, and max-keycode is never greater than 255. Not all keycodes in this range are required to have corresponding keys.

### Screen Information

The information that applies per screen is:

The allowed-depths specifies what pixmap and window depths are supported. Pixmaps are supported for each depth listed, and windows of that depth are supported if at least one visual type is listed for the depth. A pixmap depth of one is always supported and listed, but windows of depth one might not be supported. A depth of zero is never listed, but zero-depth **InputOnly** windows are always supported.

Root-depth and root-visual specify the depth and visual type of the root window. Width-in-pixels and height-in-pixels specify the size of the root window (which cannot be changed). The class of the root window is always **InputOutput**. Width-in-millimeters and height-in-millimeters can be used to determine the physical size and the aspect ratio.

The default-colormap is the one initially associated with the root window. Clients with minimal color requirements creating windows of the same depth as the root may want to allocate from this map by default.

Black-pixel and white-pixel can be used in implementing a monochrome application. These pixel values are for permanently allocated entries in the default-colormap. The actual RGB values may be settable on some screens and, in any case, may not actually be black and white. The names are intended to convey the expected relative intensity of the colors.

The border of the root window is initially a pixmap filled with the black-pixel. The initial background of the root window is a pixmap filled with some unspecified two-color pattern using black-pixel and white-pixel.

Min-installed-maps specifies the number of maps that can be guaranteed to be installed simultaneously (with [**InstallColormap**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:InstallColormap "InstallColormap")), regardless of the number of entries allocated in each map. Max-installed-maps specifies the maximum number of maps that might possibly be installed simultaneously, depending on their allocations. Multiple static-visual colormaps with identical contents but differing in resource ID should be considered as a single map for the purposes of this number. For the typical case of a single hardware colormap, both values will be 1.

Backing-stores indicates when the server supports backing stores for this screen, although it may be storage limited in the number of windows it can support at once. If save-unders is **True**, the server can support the save-under mode in [**CreateWindow**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:CreateWindow "CreateWindow") and [**ChangeWindowAttributes**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:ChangeWindowAttributes "ChangeWindowAttributes"), although again it may be storage limited.

The current-input-events is what [**GetWindowAttributes**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:GetWindowAttributes "GetWindowAttributes") would return for the all-event-masks for the root window.

### Visual Information

The information that applies per visual-type is:

A given visual type might be listed for more than one depth or for more than one screen.

For **PseudoColor**, a pixel value indexes a colormap to produce independent RGB values; the RGB values can be changed dynamically. **GrayScale** is treated in the same way as **PseudoColor** except which primary drives the screen is undefined; thus, the client should always store the same value for red, green, and blue in colormaps. For **DirectColor**, a pixel value is decomposed into separate RGB subfields, and each subfield separately indexes the colormap for the corresponding value. The RGB values can be changed dynamically. **TrueColor** is treated in the same way as **DirectColor** except the colormap has predefined read-only RGB values. These values are server-dependent but provide linear or near-linear increasing ramps in each primary. **StaticColor** is treated in the same way as **PseudoColor** except the colormap has predefined read-only RGB values, which are server-dependent. **StaticGray** is treated in the same way as **StaticColor** except the red, green, and blue values are equal for any single pixel value, resulting in shades of gray. **StaticGray** with a two-entry colormap can be thought of as monochrome.

The red-mask, green-mask, and blue-mask are only defined for **DirectColor** and **TrueColor**. Each has one contiguous set of bits set to 1 with no intersections. Usually each mask has the same number of bits set to 1.

The bits-per-rgb-value specifies the log base 2 of the number of distinct color intensity values (individually) of red, green, and blue. This number need not bear any relation to the number of colormap entries. Actual RGB values are always passed in the protocol within a 16-bit spectrum, with 0 being minimum intensity and 65535 being the maximum intensity. On hardware that provides a linear zero-based intensity ramp, the following relationship exists:

       hw-intensity = protocol-intensity / (65536 / total-hw-intensities)

Colormap entries are indexed from 0. The colormap-entries defines the number of available colormap entries in a newly created colormap. For **DirectColor** and **TrueColor**, this will usually be 2 to the power of the maximum number of bits set to 1 in red-mask, green-mask, and blue-mask.


## Chapter 10. Connection Close

At connection close, all event selections made by the client are discarded. If the client has the pointer actively grabbed, an [**UngrabPointer**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:UngrabPointer "UngrabPointer") is performed. If the client has the keyboard actively grabbed, an [**UngrabKeyboard**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:UngrabKeyboard "UngrabKeyboard") is performed. All passive grabs by the client are released. If the client has the server grabbed, an [**UngrabServer**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:UngrabServer "UngrabServer") is performed. All selections (see [**SetSelectionOwner**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:SetSelectionOwner "SetSelectionOwner") request) owned by the client are disowned. If close-down mode (see [**SetCloseDownMode**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:SetCloseDownMode "SetCloseDownMode") request) is **RetainPermanent** or **RetainTemporary**, then all resources (including colormap entries) allocated by the client are marked as permanent or temporary, respectively (but this does not prevent other clients from explicitly destroying them). If the mode is **Destroy**, all of the client's resources are destroyed.

When a client's resources are destroyed, for each window in the client's save-set, if the window is an inferior of a window created by the client, the save-set window is reparented to the closest ancestor such that the save-set window is not an inferior of a window created by the client. If the save-set window is unmapped, a [**MapWindow**](https://www.x.org/releases/current/doc/xproto/x11protocol.html#requests:MapWindow "MapWindow") request is performed on it (even if it was not an inferior of a window created by the client). The reparenting leaves unchanged the absolute coordinates (with respect to the root window) of the upper-left outer corner of the save-set window. After save-set processing, all windows created by the client are destroyed. For each nonwindow resource created by the client, the appropriate **Free** request is performed. All colors and colormap entries allocated by the client are freed.

A server goes through a cycle of having no connections and having some connections. At every transition to the state of having no connections as a result of a connection closing with a **Destroy** close-down mode, the server resets its state as if it had just been started. This starts by destroying all lingering resources from clients that have terminated in **RetainPermanent** or **RetainTemporary** mode. It additionally includes deleting all but the predefined atom identifiers, deleting all properties on all root windows, resetting all device maps and attributes (key click, bell volume, acceleration), resetting the access control list, restoring the standard root tiles and cursors, restoring the default font path, and restoring the input focus to state **PointerRoot**.

Note that closing a connection with a close-down mode of **RetainPermanent** or **RetainTemporary** will not cause the server to reset.