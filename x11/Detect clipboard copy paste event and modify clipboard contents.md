---
date: 2023-04-12 00:27
title: Detect clipboard copy paste event and modify clipboard contents
tags:
- clipboard
---

[Detect clipboard copy/paste event and modify clipboard contents](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents)


# Detect clipboard copy/paste event and modify clipboard contents

Asked 3 years, 7 months ago



Viewed 6k times

14

[](https://askubuntu.com/posts/1167026/timeline)

After something is copied to clipboard (using ctrl+c) I want a script (bash, python or any other language) to automatically detect that new entry is added to clipboard, change it's content and put it back to clipboard so when I paste it I get the modified text. The script should constantly run in background and monitor the clipboard for changes.

The following script describes the modification that is needed :  
Source : [https://superuser.com/questions/796292/is-there-an-efficient-way-to-copy-text-from-a-pdf-without-the-line-breaks](https://superuser.com/questions/796292/is-there-an-efficient-way-to-copy-text-from-a-pdf-without-the-line-breaks)

```bash
#!/bin/bash

# title: copy_without_linebreaks
# author: Glutanimate (github.com/glutanimate)
# license: MIT license

# Parses currently selected text and removes 
# newlines that aren't preceded by a full stop

SelectedText="$(xsel)"

ModifiedText="$(echo "$SelectedText" | \
    sed 's/\.$/.|/g' | sed 's/^\s*$/|/g' | tr '\n' ' ' | tr '|' '\n')"

#   - first sed command: replace end-of-line full stops with '|' delimiter and keep original periods.
#   - second sed command: replace empty lines with same delimiter (e.g.
#     to separate text headings from text)
#   - subsequent tr commands: remove existing newlines; replace delimiter with
#     newlines
# This is less than elegant but it works.

echo "$ModifiedText" | xsel -bi
```

**I do not want to use shortcut key binding to run the script.**

-   [bash](https://askubuntu.com/questions/tagged/bash "show questions tagged 'bash'")
-   [scripts](https://askubuntu.com/questions/tagged/scripts "show questions tagged 'scripts'")
-   [clipboard](https://askubuntu.com/questions/tagged/clipboard "show questions tagged 'clipboard'")
-   [xclip](https://askubuntu.com/questions/tagged/xclip "show questions tagged 'xclip'")
-   [xsel](https://askubuntu.com/questions/tagged/xsel "show questions tagged 'xsel'")

[Improve this question](https://askubuntu.com/posts/1167026/edit)

Follow

[edited Aug 20, 2019 at 7:47](https://askubuntu.com/posts/1167026/revisions "show all edits to this post")

asked Aug 20, 2019 at 7:41

[![SidMan's user avatar](https://www.gravatar.com/avatar/a86dc7052dae920624d99a42278262cd?s=64&d=identicon&r=PG&f=1)](https://askubuntu.com/users/986837/sidman)

[SidMan](https://askubuntu.com/users/986837/sidman)

36122 silver badges99 bronze badges

-   I checked the following questions : 1. [Automatically modify clipboard content (regex pattern replacement) when copy-pasting](https://askubuntu.com/questions/873948/automatically-modify-clipboard-content-regex-pattern-replacement-when-copy-pas) but it is an Application Indicator 2. [Processing clipboard text through a script in between copy and paste](https://askubuntu.com/questions/399585/processing-clipboard-text-through-a-script-in-between-copy-and-paste) but it runs script manually. 
    
    – [SidMan](https://askubuntu.com/users/986837/sidman "361 reputation")
    
     [Aug 20, 2019 at 7:42](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment1946340_1167026) 
    
-   Take a look at this:[github.com/cdown/clipnotify](https://github.com/cdown/clipnotify) Run it like `while ./clipnotify; do echo "clipboard changed"; done` 
    
    – [kenn](https://askubuntu.com/users/81249/kenn "5,044 reputation")
    
     [Aug 20, 2019 at 11:40](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment1946418_1167026)
    
-   Works very well. Should I write it as an answer ? 
    
    – [SidMan](https://askubuntu.com/users/986837/sidman "361 reputation")
    
     [Aug 20, 2019 at 12:51](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment1946434_1167026) 
    
-   @kenn Do you know a clipnotify alternative for Wayland? 
    
    – [Pablo Bianchi](https://askubuntu.com/users/349837/pablo-bianchi "13,391 reputation")
    
     [Nov 12, 2020 at 4:44](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment2192944_1167026)
    

[Add a comment](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

---


## 1 Answer

Sorted by:

                                              Highest score (default)                                                                   Date modified (newest first)                                                                   Date created (oldest first)                              

12

[](https://askubuntu.com/posts/1167129/timeline)

Credit goes to [Kenn](https://askubuntu.com/users/81249/kenn).

I modified the script to my requirements and added the function to detect clipboard copy event and modify its contents.

[Source](https://github.com/SidMan2001/Scripts/tree/master/PDF-Copy-without-Linebreaks-Linux).

## Remove Line Breaks when copying text from PDF (Linux)

This bash script removes line breaks when copying text from PDF. It works for both Primary Selection and Clipboard of Linux.

```bash
#!/bin/bash

# title: copy_without_linebreaks
# author: Glutanimate (github.com/glutanimate)
# modifier: Siddharth (github.com/SidMan2001)
# license: MIT license

# Parses currently selected text and removes 
# newlines

while ./clipnotify;
do
  SelectedText="$(xsel)"
  CopiedText="$(xsel -b)"
  if [[ $SelectedText != *"file:///"* ]]; then
    ModifiedTextPrimary="$(echo "$SelectedText" | tr -s '\n' ' ')"
    echo -n "$ModifiedTextPrimary" | xsel -i
  fi
  if [[ $CopiedText != *"file:///"* ]]; then
    ModifiedTextClipboard="$(echo "$CopiedText" | tr -s '\n' ' '  )"
    echo -n "$ModifiedTextClipboard" | xsel -bi
  fi
done
```

## Dependencies

1.  **xsel**: `sudo apt install xsel`
2.  [**clipnotify**](https://github.com/cdown/clipnotify). You can use the pre-compiled clipnotify provided in the repository or compile yourself.  
    **To compile clipnotify yourself**
    
    ```bash
    sudo apt install git build-essential libx11-dev libxtst-dev
    git clone https://github.com/cdown/clipnotify.git
    cd clipnotify
    sudo make
    ```
    

## To USE

1.  Download this repository as zip or copy and paste the script in a text editor and save it as copy_without_linebreaks.sh.
2.  Make sure that script and clipnotify (downloaded or precompiled) are in the same folder.
3.  Open terminal in script's folder and set permission  
    `chmod +x "copy_without_linebreaks.sh"`
4.  Double-click the script or run by entering in terminal :  
    `.\copy_without_linebreaks.sh`
5.  Copy text in pdf and paste it anywhere. Lines breaks will be removed.

[Share](https://askubuntu.com/a/1167129 "Short permalink to this answer")

[Improve this answer](https://askubuntu.com/posts/1167129/edit)

Follow

[edited Nov 11, 2020 at 19:21](https://askubuntu.com/posts/1167129/revisions "show all edits to this post")

[![Pablo Bianchi's user avatar](https://www.gravatar.com/avatar/0ed9f7f093bc4915bfe51e794e846ea2?s=64&d=identicon&r=PG)](https://askubuntu.com/users/349837/pablo-bianchi)

[Pablo Bianchi](https://askubuntu.com/users/349837/pablo-bianchi)

13.4k44 gold badges7272 silver badges112112 bronze badges

answered Aug 20, 2019 at 16:39

[![SidMan's user avatar](https://www.gravatar.com/avatar/a86dc7052dae920624d99a42278262cd?s=64&d=identicon&r=PG&f=1)](https://askubuntu.com/users/986837/sidman)

[SidMan](https://askubuntu.com/users/986837/sidman)

36122 silver badges99 bronze badges

-   Please click grey check mark next to your answer in two days time. 
    
    – [WinEunuuchs2Unix](https://askubuntu.com/users/307523/wineunuuchs2unix "97,971 reputation")
    
     [Aug 20, 2019 at 17:22](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment1946570_1167129)
    
-   Does `clipnotify` support individual selection events? i.e. "primary", "secondary", "clipboard" or "buffer-cut". I commented a line in clipnotify.c to get events for only clipboard selection. i.e. [i.imgur.com/FcY3REn.png](https://i.imgur.com/FcY3REn.png) 
    
    – [Akhil](https://askubuntu.com/users/835941/akhil "516 reputation")
    
     [Jun 23, 2021 at 17:43](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents#comment2303571_1167129) 
    

[Add a comment](https://askubuntu.com/questions/1167026/detect-clipboard-copy-paste-event-and-modify-clipboard-contents# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---