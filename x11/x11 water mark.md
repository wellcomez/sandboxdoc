---
date: 2023-04-03 16:51
title: x11 water mark
tags:
- x11
- watermark
---



## watermark-for-vnc-displays
https://serverfault.com/questions/1111633/watermark-for-vnc-displays
Are there any feasible options for adding a watermark (i.e. diagonal transparent text) to VNC sessions?
I did not find such features provided out of the box for the existing open-source VNC servers. Gateways / proxies like Apache Guacamole don't offer such feature as well.

I was thinking about running a side-car process that uses Xlib / Cairo to modify X buffer on the server side before it gets exported by a VNC server. But that did not work as expected (no changes on the VNC client side).
Xorg

```sh
0:00 /usr/sbin/lightdm
0:23  \_ /usr/lib/xorg/Xorg -core :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
0:00  \_ lightdm --session-child 12 19
0:00      \_ /usr/libexec/gnome-session-binary --session=ubuntu
```

VNC

```sh
0:00 /usr/bin/x11vnc -display :0 -auth /run/lightdm/root/:0 -rfbauth /etc/x11vnc.pwd -rfbport 5900 -shared -noxdamage -repeat -forever -loop
0:06  \_ /usr/bin/x11vnc -display :0 -auth /run/lightdm/root/:0 -rfbauth /etc/x11vnc.pwd -rfbport 5900 -shared -noxdamage -repeat -forever -loop
```

Xlib

```python
import Xlib

display = Xlib.display.Display(':0')
screen = display.screen()
root = screen.root

gc = root.create_gc(foreground = screen.white_pixel, background = screen.black_pixel)

while True:
    root.draw_text(gc, 100, 100, b"Test")
    display.flush()
```

```sh
sudo XAUTHORITY=/var/run/lightdm/root/:0 python3 /tmp/watermark.py
```


<mark style="background: #E73308E3;">I solved the problem by switching from my python XLib client to adjusting code from [https://github.com/ftorkler/x11-overlay](https://github.com/ftorkler/x11-overlay) for my needs.</mark>

Apparently, using Xlib requires certain skills which I did not have:

-   [https://en.wikibooks.org/wiki/X_Window_Programming/Xlib](https://en.wikibooks.org/wiki/X_Window_Programming/Xlib)
-   [https://tronche.com/gui/x/xlib-tutorial/](https://tronche.com/gui/x/xlib-tutorial/)
-   [https://www.x.org/docs/X11/xlib.pdf](https://www.x.org/docs/X11/xlib.pdf)

[Share](https://serverfault.com/a/1112166 "Short permalink to this answer")

[Improve this answer](https://serverfault.com/posts/1112166/edit)
