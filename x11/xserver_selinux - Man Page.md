
---
date: 2023-04-14 17:45
title: xserver_selinux - Man Page
tags:
- selinux
- x11
- xserver
---



# xserver_selinux - Man Page
[xserver_selinux - Man Page](https://www.mankier.com/8/xserver_selinux#)

Security Enhanced Linux Policy for the xserver processes


## Description

Security-Enhanced Linux secures the xserver processes via flexible mandatory access control.

The xserver processes execute with the xserver_t SELinux type. You can check if you have these processes running by executing the **ps** command with the **-Z** qualifier.

For example:

**ps -eZ | grep xserver_t**


## Entrypoints

The xserver_t SELinux type can be entered via the **xserver_exec_t** file type.

The default entrypoint paths for the xserver_t domain are the following:
~~~
/usr/bin/nvidia.*, /usr/bin/Xair, /usr/bin/Xorg, /usr/bin/Xvnc, /usr/bin/Xephyr, /usr/bin/x11vnc, /usr/X11R6/bin/X, /usr/bin/Xwayland, /usr/X11R6/bin/Xorg, /usr/X11R6/bin/Xipaq, /usr/libexec/Xorg.bin, /usr/X11R6/bin/XFree86, /usr/libexec/Xorg.wrap, /usr/X11R6/bin/Xwrapper, /usr/libexec/gsd-backlight-helper

~~~

## Process Types

SELinux defines process types (domains) for each process running on the system

You can see the context of a process using the **-Z** option to **psbP**

Policy governs the access confined processes have to files. SELinux xserver policy is very flexible allowing users to setup their xserver processes in as secure a method as possible.

The following process types are defined for xserver:

**xserver_t**

Note: **semanage permissive -a xserver_t** can be used to make the process type xserver_t permissive. SELinux does not deny access to permissive process types, but the AVC (SELinux denials) messages are still generated.


## Booleans

SELinux policy is customizable based on least access required.  xserver policy is extremely flexible and has several booleans that allow you to manipulate the policy and run xserver with the tightest access possible.

If you want to allows XServer to execute writable memory, you must turn on the xserver_execmem boolean. Disabled by default.

**setsebool -P xserver_execmem 1**

If you want to support X userspace object manager, you must turn on the xserver_object_manager boolean. Disabled by default.

**setsebool -P xserver_object_manager 1**

If you want to deny user domains applications to map a memory region as both executable and writable, this is dangerous and the executable should be reported in bugzilla, you must turn on the deny_execmem boolean. Disabled by default.

**setsebool -P deny_execmem 1**

If you want to control the ability to mmap a low area of the address space, as configured by /proc/sys/vm/mmap_min_addr, you must turn on the mmap_low_allowed boolean. Disabled by default.

**setsebool -P mmap_low_allowed 1**

If you want to allow system to run with NIS, you must turn on the nis_enabled boolean. Disabled by default.

**setsebool -P nis_enabled 1**

If you want to disable kernel module loading, you must turn on the secure_mode_insmod boolean. Disabled by default.

**setsebool -P secure_mode_insmod 1**

If you want to allow unconfined executables to make their heap memory executable.  Doing this is a really bad idea. Probably indicates a badly coded executable, but could indicate an attack. This executable should be reported in bugzilla, you must turn on the selinuxuser_execheap boolean. Disabled by default.

**setsebool -P selinuxuser_execheap 1**

If you want to allow unconfined executables to make their stack executable.  This should never, ever be necessary. Probably indicates a badly coded executable, but could indicate an attack. This executable should be reported in bugzilla, you must turn on the selinuxuser_execstack boolean. Enabled by default.

**setsebool -P selinuxuser_execstack 1**


## Port Types

SELinux defines port types to represent TCP and UDP ports.

You can see the types associated with a port by using the following command:

**semanage port -l**

Policy governs the access confined processes have to these ports. SELinux xserver policy is very flexible allowing users to setup their xserver processes in as secure a method as possible.

The following port types are defined for xserver:

**xserver_port_t**

Default Defined Ports: tcp 6000-6020


## Managed Files

The SELinux process type xserver_t can manage files labeled with the following file types.  The paths listed are the default paths for these file types.  Note the processes UID still need to have DAC permissions.

**file_type**

all files on the system


## File Contexts

SELinux requires files to have an extended attribute to define the file type.

You can see the context of a file using the **-Z** option to **lsbP**

Policy governs the access confined processes have to these files. SELinux xserver policy is very flexible allowing users to setup their xserver processes in as secure a method as possible.

**STANDARD FILE CONTEXT**

SELinux defines the file context types for the xserver, if you wanted to store files with these types in a different paths, you need to execute the semanage command to specify alternate labeling and then use restorecon to put the labels on disk.

**semanage fcontext -a -t xserver_var_lib_t '/srv/xserver/content(/.*)?'**  
**restorecon -R -v /srv/myxserver_content**

Note: SELinux often uses regular expressions to specify labels that match multiple files.

_The following file types are defined for xserver:_

**xserver_etc_t**

- Set files with the xserver_etc_t type, if you want to store xserver files in the /etc directories.

**xserver_exec_t**

- Set files with the xserver_exec_t type, if you want to transition an executable to the xserver_t domain.

Paths:
~~~
/usr/bin/nvidia.*, /usr/bin/Xair, /usr/bin/Xorg, /usr/bin/Xvnc, /usr/bin/Xephyr, /usr/bin/x11vnc, /usr/X11R6/bin/X, /usr/bin/Xwayland, /usr/X11R6/bin/Xorg, /usr/X11R6/bin/Xipaq, /usr/libexec/Xorg.bin, /usr/X11R6/bin/XFree86, /usr/libexec/Xorg.wrap, /usr/X11R6/bin/Xwrapper, /usr/libexec/gsd-backlight-helper

~~~
**xserver_log_t**

- Set files with the xserver_log_t type, if you want to treat the data as xserver log data, usually stored under the /var/log directory.

Paths:

~~~
/var/[xgkw]dm(/.*)?, /usr/var/[xgkw]dm(/.*)?, /var/log/gdm(3)?(/.*)?, /var/log/Xorg.*, /var/log/XFree86.*, /var/log/lightdm(/.*)?, /var/log/nvidia-installer.log.*

~~~
**xserver_tmpfs_t**

- Set files with the xserver_tmpfs_t type, if you want to store xserver files on a tmpfs file system.

**xserver_var_lib_t**

- Set files with the xserver_var_lib_t type, if you want to store the xserver files under the /var/lib directory.

**xserver_var_run_t**

- Set files with the xserver_var_run_t type, if you want to store the xserver files under the /run or /var/run directory.

Paths:

~~~
/var/run/xorg(/.*)?, /var/run/video.rom
~~~

Note: File context can be temporarily modified with the chcon command.  If you want to permanently change the file context you need to use the **semanage fcontext** command.  This will modify the SELinux labeling database.  You will need to use **restorecon** to apply the labels.


## Commands

**semanage fcontext** can also be used to manipulate default file context mappings.

**semanage permissive** can also be used to manipulate whether or not a process type is permissive.

**semanage module** can also be used to enable/disable/install/remove policy modules.

**semanage port** can also be used to manipulate the port definitions

**semanage boolean** can also be used to manipulate the booleans

**system-config-selinux** is a GUI tool available to customize SELinux policy settings.


## Author

This manual page was auto-generated using **sepolicy manpage .**


## See Also

[selinux(8)](https://www.mankier.com/8/selinux), xserver(8), [semanage(8)](https://www.mankier.com/8/semanage), [restorecon(8)](https://www.mankier.com/8/restorecon), [chcon(1)](https://www.mankier.com/1/chcon), [sepolicy(8)](https://www.mankier.com/8/sepolicy), [setsebool(8)](https://www.mankier.com/8/setsebool)


## Info

23-03-24 SELinux Policy xserver

-   [Home](https://www.mankier.com/)
-   [Blog](https://www.mankier.com/blog/)
-   [About](https://www.mankier.com/about)
-   [![Twitter](https://www.mankier.com/img/twitter.svg)](https://twitter.com/mankier)
-   [![Email](https://www.mankier.com/img/email.svg)](mailto:jackson@mankier.com)