---
date: 2023-04-12 00:58
title: Experimenting With X-Windows
tags:
- clipboard
- selinux
---
http://selinuxproject.org/page/Experimenting_With_X-Windows


# Experimenting With X-Windows

[](http://selinuxproject.org/page/Experimenting_With_X-Windows#column-one)[](http://selinuxproject.org/page/Experimenting_With_X-Windows#searchInput)

## Contents

 [[hide](http://selinuxproject.org/page/Experimenting_With_X-Windows#)] 

-   [1 Experimenting with X-Windows](http://selinuxproject.org/page/Experimenting_With_X-Windows#Experimenting_with_X-Windows)
    -   [1.1 Section Overview](http://selinuxproject.org/page/Experimenting_With_X-Windows#Section_Overview)
    -   [1.2 Overview of Modules and Applications](http://selinuxproject.org/page/Experimenting_With_X-Windows#Overview_of_Modules_and_Applications)
        -   [1.2.1 The x_contexts Files and Supporting Loadable Module](http://selinuxproject.org/page/Experimenting_With_X-Windows#The_x_contexts_Files_and_Supporting_Loadable_Module)
        -   [1.2.2 The Select - Paste Applications and Loadable Module](http://selinuxproject.org/page/Experimenting_With_X-Windows#The_Select_-_Paste_Applications_and_Loadable_Module)
            -   [1.2.2.1 Test Conclusions](http://selinuxproject.org/page/Experimenting_With_X-Windows#Test_Conclusions)
            -   [1.2.2.2 Calling the XSELinux Functions](http://selinuxproject.org/page/Experimenting_With_X-Windows#Calling_the_XSELinux_Functions)
    -   [1.3 Building the X-Windows Select and Paste Examples](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_X-Windows_Select_and_Paste_Examples)
        -   [1.3.1 Building the x_contexts Files and Loadable Module](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_x_contexts_Files_and_Loadable_Module)
        -   [1.3.2 Building the X-select and X-paste Applications](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_X-select_and_X-paste_Applications)
        -   [1.3.3 Building the X-select and X-paste Loadable Module](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_X-select_and_X-paste_Loadable_Module)
        -   [1.3.4 Testing Derived Labels](http://selinuxproject.org/page/Experimenting_With_X-Windows#Testing_Derived_Labels)
            -   [1.3.4.1 Derived Object Test Conclusions](http://selinuxproject.org/page/Experimenting_With_X-Windows#Derived_Object_Test_Conclusions)
        -   [1.3.5 Testing Polyinstantiated Labels](http://selinuxproject.org/page/Experimenting_With_X-Windows#Testing_Polyinstantiated_Labels)
            -   [1.3.5.1 Polyinstantiated Object Test Conclusions](http://selinuxproject.org/page/Experimenting_With_X-Windows#Polyinstantiated_Object_Test_Conclusions)

# Experimenting with X-Windows

## Section Overview

The main objectives of this section are to:

-   Demonstrate the use of 'selections' using polyinstantiation and non- polyinstantiation services of the XSELinux Object Manager (OM) with simple Xlib simple select and paste applications.
-   Use the XSELinux OM SELinuxGet.. series of functions to display various context information that is available while executing the select and paste examples.
-   Build a simple menu driven test application that will allow all the SELinuxGet/Set.. functions to be called and view the results. This is shown in the [Building the XSELinux Function Test Application](http://selinuxproject.org/page/Building_the_XSELinux_Function_Test_Application "Building the XSELinux Function Test Application") section.

It is recommended that the [Notebook Source](http://taiga.selinuxproject.org/~rhaines/notebook-source-1.1.0-1.tar.gz) file is installed in your $HOME as this contains all the configuration files and source code required to produce the required modules. It also contains README and a simple Makefile for each section.

This section assumes the following:

-   The message filter modules have been removed before starting this exercise, however it is not mandatory.
-   SELinux is configured to use the modular-test policy in permissive mode initially to build the services. The modular-test policy is decribed in the [Building the Base Policy Module](http://selinuxproject.org/page/Building_a_Basic_Policy#Building_the_Base_Policy_Module "Building a Basic Policy") section.

  

## Overview of Modules and Applications

The loadable modules used to support these exercises are built using standard SELinux language statements and rules with customised x_contexts files to support the labeling of objects.

The test applications are written in 'C' and use the Xlib function library with Xdevice functions provided by the Xi library. There were a few problems encountered that are discussed in the [Calling the XSELinux Functions](http://selinuxproject.org/page/Experimenting_With_X-Windows#Calling_the_XSELinux_Functions) section.

### The x_contexts Files and Supporting Loadable Module

The source files required to build and manage the new x_contexts files and supporting loadable module are located in:

 ./notebook-source/x-windows/x-contexts-base-module

As the objective of the demonstration is to show how different entries in the x_contexts file affect the use of selections it was decided to build two x_contexts files based on those in the Reference Policy 20090730 build. To support the new entries created in these x_contexts files, required an additional loadable module (x_context_base.conf).

The x_contexts files are expanded to give each entry a unique label so that it could be detected in the audit log with audit2allow when in enforcing mode, a decision could then be made as to whether an allow or dontaudit rule would be added to the policy. Additional entries were also added just to experiment. A second copy of the file was made that had the poly_ keyword added to the property and selection entries to test polyinstantiation.

The only entry that caused problems during testing was the:

poly_property _XKB_RULES_NAMES .....

This entry had to have the poly_ keyword removed in the polyinstantiated file as it stopped various keys from working (up/down etc. keys) on the keyboard.

The new x_contexts files generated are called:

**x_contexts-file-with-new-labels** - This file is similar to that used by the reference policy. The select and paste policy uses the same method to manage the labeling as the reference policy - called derived labeling as the objects label is derived from an SELinux user name or a prefix (from the 'users_extra' configuration file), then use a type_transition to transition the object to the new label on creation. For example (using standard Refpolicy):

An x_contexts entry of:

event X11:MapNotifysystem_u:object_r:manage_xevent_t

and the ssh policy module (after expansion) having a type_transition statement generated by the build process of:

type_transition ssh_t manage_xevent_t : x_event ssh_manage_xevent_t;

will relabel any objects created from manage_xevent_t to ssh_manage_xevent_t.

**x_contexts-file-with-new-polylabels** - This is used to support polyinstantiated entries (note - the reference policy does not currently use polyinstantiated entries). With polyinstantiation enabled, the select and paste policy uses the type_member rule to enforce the selection to a specific domain (in this example the x_select_paste_t domain) as follows:

type_member x_select_paste_t primary_xselection_t : x_selection  x_select_paste_t; 

To support these new x_contexts file entries an additional policy module was built that defines a type for each entry and a corresponding allow rule. This module is called x_context_base.conf and must be loaded and active when the modular-test policy is loaded with either of the new x_contexts files. Failure to do this will probably result in the system hanging as it tries to load X-Windows with no defined type or allow rules for the new x_contexts file.

To experiment with additional x_context entries:

-   Add a new entry in the appropriate x_contexts file such as:

property WM_ZOOM_HINTS system_u:object_r:wm_zoom_hints_xproperty_t

or

poly_property WM_ZOOM_HINTS system_u:object_r:wm_zoom_hints_xproperty_t

-   Add new entries in the x_context_base.conf for the type and allow statements:

type wm_zoom_hints_xproperty_t;
allow unconfined_t wm_zoom_hints_xproperty_t : x_property *;

-   Run the make module command (in the ./x-windows/x-contexts-base-module directory) and copy over the appropriate x_contexts file to /etc/selinux/modular-test/contexts.

### The Select - Paste Applications and Loadable Module

The source files required to build and manage the application and loadable module are located in:

 ./notebook-source/x-windows/x-select+paste

There are two simple X-Windows applications that select (X-select) and paste (X-paste) “Hello World” using Xlib selection functions. When they are loaded they show the application name and their context in the title bar as shown in [Figure 1](http://taiga.selinuxproject.org/~rhaines/diagrams/Derived-Test1.png). Integrated with these applications are calls to the XSELinuxGet.. functions to return context information as the Xlib functions are executed.

The output from the applications can also be captured in a file by adding the capture file name as an argument:

~~~
X-select poly-demo.txt

# The output will be in poly-demo.txt, with some text also
# displayed on the screen.
~~~

When the two applications are built they are moved to /usr/local/bin and have the default label of unconfined_t. When they are both loaded in the unconfined_t domain, there are no enforced rules (i.e. there are no restrictions). If the x_select_paste.conf module is built and loaded, then when they are run as:

~~~
runcon -t x_select_paste_t X-select

and

runcon -t x_select_paste_t X-paste

Policy will be enforced as required depending on a boolean that when set to:

setsebool -P poly-selection false
~~~

and the x_contexts-file-with-new-labels file is installed as the x_contexts file, then the derived policy rules will be enforced.

If the boolean is set to:
~~~

setsebool -P poly-selection true
~~~

and the x_contexts-file-with-new-polylabels file is installed as the x_contexts file, then the polyinstantiated policy rules will be enforced.

#### Test Conclusions

After a number of experiments the following conclusions were reached:

-   Using the non-polyinstantiated x_contexts file (with poly-selection = FALSE), resulted in selections being seen across all windows whether running in unconfined_t or x_select_paste_t domains.
-   Using the polyinstantiated x_contexts file (with poly-selection = TRUE), resulted in selections being restricted to windows running in their own domains (e.g. if running the X-select in the unconfined_t domain and X-paste in the x_select_paste_t domains, the selected text will not be pasted).
-   If the following multiple selection entries are added to the x_contexts file, then the non poly_ entry takes precedence. This means that polyinstantiation for selections will not work (even if a different label is used for each entry).
~~~
# The poly and non-poly entries cannot be in the x_contexts
# file as the non-poly entry takes precedence:
poly_selection PRIMARY system_u:object_r:primary_xselection_t
selection      PRIMARY system_u:object_r:primary_xselection_t

# Even if different labels are used:
poly_selection PRIMARY system_u:object_r:poly_primary_xselection_t
selection      PRIMARY system_u:object_r:primary_xselection_t
~~~

Therefore the overall conclusion is that for non-MLS policies, the only effective way to control selections is using polyinstatiation with the type_member rule.

The reason for stating non-MLS policy is that the MLS policy uses mlsconstrain rules to manage restrictions. Various constrain rules were used for non-MLS policy testing but no satisfactory result could be obtained - do you know different !!!

Notes:

-   When using polyinstantiation the poly_ keyword must be present in the x_contexts file and there must be a corresponding type_member rule in the policy.
-   When analysing the output from the XSELinux function calls between non-polyinstantiated (or derived) and polyinstantiated services when the X-select and X-paste applications are running (apart from their context information), the only major difference was that when calling the SELinuxListSelections function, the polyinstantiated service had an additional PRIMARY entry as follows:

~~~
# Non-polyinstantiated (derived) running in x_select_paste_t:
#
Calling SELinuxListSelections (21) for this display:

SELinuxListSelections (1 of 10) - Atom: CLIPBOARD
Object Context: system_u:object_r:clipboard_xselection_t
Data Context: system_u:object_r:clipboard_xselection_t

SELinuxListSelections (2 of 10) - Atom: PRIMARY
Object Context: system_u:object_r:primary_xselection_t
Data Context: system_u:object_r:primary_xselection_t

# Polyinstantiated (derived) running in x_select_paste_t:
#
Calling SELinuxListSelections (21) for this display:

SELinuxListSelections (1 of 11) - Atom: CLIPBOARD
Object Context: system_u:object_r:clipboard_xselection_t
Data Context: system_u:object_r:clipboard_xselection_t

SELinuxListSelections (2 of 11) - Atom: PRIMARY
Object Context: system_u:object_r:primary_xselection_t
Data Context: system_u:object_r:primary_xselection_t

SELinuxListSelections (3 of 11) - Atom: PRIMARY
Object Context: system_u:object_r:x_select_paste_t
Data Context: system_u:object_r:x_select_paste_t
~~~

-   The Reference Policy does not use polyinstantiation but supports isolation only with the MLS policy where mlsconstrain rules are enforced (see the mlsconstrain x_selection entries in the mls configuration file).
-   Various constrain rules were tried to limit selections with the non-polyinstantiated x_contexts file, but no satisfactory solution was found - any offers !!, therefore when using non-MLS policy, the only way to limit selections is via polyinstantiation. Some example constrain rules tried that had the following results:

~~~
# Add constrain rule to base.conf:
constrain x_selection { read getattr } (t1 == unconfined_t);

# When running "runcon -t x_select_paste_t X-paste" it flags the following
# AVC entry in the Xorg.0.log file:

(WW) avc: denied { getattr } for request=X11:GetSelectionOwner comm=X-paste selection=PRIMARY scontext=user_u:unconfined_r:x_select_paste_t tcontext=system_u:object_r:primary_xselection_t tclass=x_selection

# When running X-paste (in unconfined_t) then no errors in log.

# Add constrain rule to base.conf:
constrain x_selection { read getattr } (t1 == secure_select); 
# Where secure_select is an attribute declared in base.conf

# With the following added to x_select_paste.conf:
require { attribute secure_select; .... }
typeattribute x_select_paste_t secure_select;

# When running "runcon -t x_select_paste_t X-paste" there are no errors in
# the log.

# When running X-paste (in unconfined_t) it flags the following AVC entry
# in the Xorg.0.log file:</nowiki>

(WW) avc: denied { getattr } for request=X11:GetSelectionOwner comm=X-paste selection=PRIMARY scontext=user_u:unconfined_r:unconfined_t tcontext=system_u:object_r:primary_xselection_t tclass=x_selection

~~~
#### Calling the XSELinux Functions

The X-select, X-paste and X-setest applications call the object manager XSELinuxGet/Set.. functions to get and set contexts as required. To use these functions the standard Xlib GetReq, _XSend and _XReply functions need to be called to manage the request / response sequences. As there are 23 functions it was decided to build these into a separate 'C' module called XSELinuxOMFunctions.c that is supported by a header file called Xlib-selinux.h. that are located in the ./x-windows/x-common directory.

The header file is based on the XSELinux extension source header xselinux.h and has been expanded to support the Xlib GetReq macro and associated functions. The only point to note is that the SELinuxQueryVersion request header structure size had to be set to 4 instead of 6 as the client_major and client_minor entries were not used and caused errors when added.

The error handling caused much grief (as not an Xlib expert), and it will be seen that there are a number of flags to indicate certain error sequences. The source code has plenty of comments regarding these and if anyone has better methods let the author know.

## Building the X-Windows Select and Paste Examples

To build and test the infrastructure to support modified x_contexts files for the X-Windows object manager, the following will be required:

-   The Base Module described in the [Building the Base Policy Module](http://selinuxproject.org/page/Building_a_Basic_Policy#Building_the_Base_Policy_Module "Building a Basic Policy") section. This will install the base policy module and supporting files in the /etc/selinux/modular-test area.
-   Two modified x_contexts files. The Reference Policy sample has been modified to capture additional entries and for each entry allocate its own unique object label. There is one file to support the way the Reference Policy (build 20090730) supports these objects[[1]](http://selinuxproject.org/page/Experimenting_With_X-Windows#cite_note-ref1-1), and the other has the additional 'poly_' keyword added to support polyinstantiated property and selection entries.

Important note - These sample x_contexts files must not be used with the reference policy as they are incompatible and will cause the system to hang when X-Windows is being loaded

-   A loadable module (x_context_base.conf) that contains the policy type statements and allow rules of the newly defined x_contexts file entries described in bullet b). This will allow the X-Windows object manager to load the new x_contexts file without any errors.
-   Two simple X-Windows applications - X-select to automatically select some text (Hello World), and X-paste to paste the text onto the screen. These applications use the minimum Xlib functions possible, however they also contain calls to the SELinux X-Windows functions that are built into the object manager to retrieve context information as the applications execute.
-   A loadable module (x_select_paste.conf) that contains the policy for enforcing the X-select and X-paste applications when running in the x_select_paste_t domain. This policy supports the polyinstantiated x_contexts file by setting a boolean (poly-selection) to TRUE and the the derived x_contexts file by setting the boolean to FALSE.

The build and testing will be carried out in the following stages:

-   Ensure that the modular-test base module has been built and tested as described in the [Building the Base Policy Module](http://selinuxproject.org/page/Building_a_Basic_Policy#Building_the_Base_Policy_Module "Building a Basic Policy") section.
-   Build the new x_contexts files and a loadable module (x_context_base.conf). The files to are available in the source file and located in the ./notebook-source/x-windows/x-contexts-base-module directory.
-   Build the X-select, X-paste applications and their supporting loadable module for running in the x_select_paste_t domain.
-   Install the derived (non-polyinstantiated) x_contexts file and test using the X-select and X-paste applications in various scenarios using the unconfined_t and x_select_paste_t domains, recording the results.
-   Install the polyinstantiated x_contexts file and test using the X-select and X-paste applications in various scenarios using the unconfined_t and x_select_paste_t domains, recording the results.

### Building the x_contexts Files and Loadable Module

Before building and installing these, ensure that the modular-test base module has been built, if it has proceed as follows:

-   Ensure you are logged on as root and SELinux is running in permissive mode (setenforce 0) to perform the build process. It is assumed that the files are built in the ./notebook-source/x-windows/x-contexts-base-module directory.
-   Produce a derived x_contexts file called [x_contexts-file-with-new-labels](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-contexts-base-module/x_contexts-file-with-new-labels)
-   Produce a polyinstantiated x_contexts file called [x_contexts-file-with-new-polylabels](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-contexts-base-module/x_contexts-file-with-new-polylabels).
-   Produce the policy file [x_context_base.conf](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-contexts-base-module/x_context_base.conf).
-   Compile, package and load the module as follows:

checkmodule -m x_context_base.conf -o x_context_base.mod
semodule_package -o x_context_base.pp -m x_context_base.mod
semodule -v -s modular-test -i x_context_base.pp

Use the semodule command to check the module has loaded as follows:
~~~

semodule -l

x_context_base 1.0.0
~~~

-   Copy the derived x_contexts-file-with-new-labels to the modular-test policy area as the new x_contexts file:

~~~
cp x_contexts-file-with-new-labels /etc/selinux/modular-test/contexts/x_contexts
~~~

-   Optionally clear the log file so that they are clear for easier reading after the reboot:

> /var/log/audit/audit.log

-   Ensure that SELinux is configured to run in permissive mode with the modular-test policy enabled, then reboot the system to ensure X-windows loads the new x_contexts file entries.

reboot

The system should reload with no errors, however if the screen should remain blank then the chances are that the x_contexts file is incorrect and the repair disk will be required to replace the x_contexts file with the one produced in the [Building the Base Policy Module](http://selinuxproject.org/page/Building_a_Basic_Policy#Building_the_Base_Policy_Module "Building a Basic Policy") section. Alternatively, reboot with a know good policy and check the modular-test policy x_contexts entries.

Run the setenforce 1 command and then check the audit log for USER_AVC errors (there should not be any errors).

Note that the x_contexts file currently loaded is the standard (non-poly) version.

### Building the X-select and X-paste Applications

Before building and installing these applications, ensure that the libraries and development packages have been installed.

The easiest way to build these applications is to use the notebook-source files (the X-select and X-paste code is in the ./notebook-source/x-windows/x-select+paste directory). The code to manage the XSELinux functions is quite long and also requires a header file (these are contained in the ./notebook-source/x-windows/x-common directory). The source files also contain a pre-compiled set of applications that only need to be copied to /usr/local/bin. However to build from scratch proceed as follows:

-   Ensure you are logged on as root and SELinux is running in permissive mode (setenforce 0) to perform the build process. It is assumed that the applications will be built in the ./notebook-source/x-windows/x-select+paste directory, but the XSELinux function call code will be in the ./notebook-source/x-windows/x-common directory as it is shared by the X-setest application as well.
-   In the ./notebook-source/x-windows/x-common directory, produce the header file with the following entries [Xlib-selinux.h](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-common/Xlib-selinux.h).
-   In the ./notebook-source/x-windows/x-common directory, produce the [XSELinuxOMFunctions.c](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-common/XSELinuxOMFunctions.c) source file.
-   In the ./notebook-source/x-windows/x-select+paste directory, produce the [X-select.c](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-select+paste/X-select.c)] source file.
-   In the ./notebook-source/x-windows/x-select+paste directory, produce the [X-paste.c](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-select+paste/X-paste.c) source file.
-   From the ./notebook-source/x-windows/x-select+paste directory, compile and link the X-select and X-paste applications as follows:

gcc X-paste.c ../x-common/XSELinuxOMFunctions.c -o X-paste -l selinux -l X11 -l Xi

gcc X-select.c ../x-common/XSELinuxOMFunctions.c -o X-select -l selinux -l X11 -l Xi

-   Copy the X-select and X-paste application binaries to /usr/local/bin as follows:

cp X-select /usr/local/bin
cp X-paste /usr/local/bin

-   The applications can be tested by calling them from separate virtual terminals, although they will only be running in the unconfined_t domain as shown in [Figure 1](http://taiga.selinuxproject.org/~rhaines/diagrams/Derived-Test1.png) (until the policy module is built as described in the next section). Note that the x_contexts file loaded in the previous section is the standard (non-poly) version.

### Building the X-select and X-paste Loadable Module

This loadable module is to enforce policy on the X-select and X-paste applications when they are run in the x_select_paste_t domain using the SELinux runcon commands as follows:
~~~
# Note the runcon commands would be run from different virtual
# terminals to activate and test the applications.

runcon -t x_select_paste_t X-select
runcon -t x_select_paste_t X-select
~~~

The policy has a poly-selection boolean that by default is set to FALSE and controls what policy rules are enforced depending on what verion of the x_contexts file is loaded (although note that the boolean does NOT control what file is loaded, that is a user copy function):

-   Testing the standard x_contexts file poly-selection = FALSE.
-   Testing the polyinstantiated x_contexts file poly-selection = TRUE.

The [Testing Derived Labels](http://selinuxproject.org/page/Experimenting_With_X-Windows#Testing_Derived_Labels) and [Testing Polyinstantiated Labels](http://selinuxproject.org/page/Experimenting_With_X-Windows#Testing_Polyinstantiated_Labels) sections run through a number of tests to check what happens with each setting.

To build the loadable module:

-   In the ./notebook-source/x-windows/x-select+paste directory, produce the [x_select_paste.conf](http://taiga.selinuxproject.org/~rhaines/notebook-source/x-windows/x-select+paste/x_select_paste.conf) policy configuration file.
-   Compile and load the policy module using the following SELinux commands:

checkmodule -m x_select_paste.conf -o x_select_paste.mod
semodule_package -o x_select_paste.pp -m x_select_paste.mod
semodule -v -s modular-test -i x_select_paste.pp

-   The policy modules loaded should now consist of the following:

semodule -l
x_context_base 1.0.0
x_select_paste 1.0.0

-   The system is now ready for testing various select / paste scenarios. Note that by default the poly-selection boolean is set to FALSE and the x_contexts-file-with-new-labels file has been installed as the /etc/selinux/modular-test/contexts/x_contexts file.

### Testing Derived Labels

The following steps will determine if the test set-up is correct:

-   Check the correct modules are loaded by:

~~~
semodule -l
x_context_base 1.0.0
x_select_paste 1.0.0

~~~
-   Check the Boolean is set correctly by:

~~~
getsebool poly-selection
poly-selection --> off

# If 'on', then run:
setsebool -P poly-selection false

~~~
-   Ensure the correct x_contexts file is installed. This can be done by checking that there are no poly_ entries in the /etc/selinux/modular-test/contexts/x_contexts file. If the file is not correct, then copy the correct version over by:

~~~
cp $HOME/notebook-source/x-windows/x-contexts-base-module/x_contexts-file-with-new-labels /etc/selinux/modular-test/contexts/x_contexts
~~~

-   If the X-select and X-paste applications were not built as described in the [Building the X-select and X-paste Applications](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_X-select_and_X-paste_Applications) section, then the executables can be copied from the ./notebook-source/x-windows/x_select+paste directory to the /usr/local/bin directory. They should default to unconfined_t that can be checked as follows:

~~~
ls -Z /usr/local/bin
-rwxr-xr-x. root root system_u:object_r:unconfined_t X-paste
-rwxr-xr-x. root root system_u:object_r:unconfined_t X-select
~~~

-   Open two virtual terminal sessions so that the applications can be run. A third can be opened for monitoring the audit log for errors.
-   Run setenforce 1 for enforcing mode.

**Test 1:**

The X-select and X-paste applications are called directly, one in each terminal session and will therefore run under the unconfined_t domain:

Terminal 1: X-paste Terminal 2: X-select

The results can be seen in [Figure 1](http://taiga.selinuxproject.org/~rhaines/diagrams/Derived-Test1.png) where "Hello World" is displayed on Terminal 1 (note that if any text has been selected by another window, then that text will probably be displayed instead of "Hello World").

There is other information displayed that shows the various context information using the SELinuxGet.. functions that can be examined if required.

To exit the applications 'Ctrl c' is used.

**Test 2:**

The applications are then loaded using runcon:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: runcon -t x_select_paste_t X-select

The results can be seen in [Figure 2](http://taiga.selinuxproject.org/~rhaines/diagrams/Derived-Test2.png) where "Hello World" is displayed on Terminal 1.

  
**Test 3:**

The applications are then loaded as follows:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: X-select

As shown in [Figure 3](http://taiga.selinuxproject.org/~rhaines/diagrams/Derived-Test3.png), the X-paste application still receives "Hello World", showing that selections are not blocked.

  
**Test 4:**

With this test the poly-selection boolean is set to TRUE:

setsebool -P poly-selection true

The applications are then loaded as follows:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: X-select

The results are the same as Test 3 in that “Hello World” is displayed.

#### Derived Object Test Conclusions

As can be seen the selected text can be pasted from both the unconfined_t and x_select_paste_t domains. This means that using standard reference policy type x_contexts file entries for selections, separation cannot be achieved (although note that the MLS version of reference policy may do - need to check one day).

If the policy is analysed, it will be seen that even though a type transition has been defined for the primary_xselection_t object:

-   Extracts from the x_select_paste.conf policy:

~~~
# Added type with derived name to transition new object instances:
type user_primary_xselection_t;

#Added type transition for the object:
type_transition x_select_paste_t primary_xselection_t : x_selection user_primary_xselection_t;

a new object is never created:

# audit2allow never indicated that an allow rule was needed
# like this (that would be required if a new instance was created)

allow x_select_paste_t user_ primary_xselection_t : x_selection { read ... };
~~~

  

### Testing Polyinstantiated Labels

The following steps will determine if the test set-up is correct:

-   Check the correct modules are loaded by:

~~~
semodule -l
x_context_base 1.0.0
x_select_paste 1.0.0
~~~

-   Check the Boolean is set correctly by:
~~~
getsebool poly-selection
poly-selection --> on

# If 'on', then run:
setsebool -P poly-selection true
~~~

-   Ensure the correct x_contexts file is installed. This can be done by checking that there are poly_ entries in the /etc/selinux/modular-test/contexts/x_contexts file. If the file is not correct, then copy the correct version over by:

~~~
cp $HOME/notebook-source/x-windows/x-contexts-base-module/x_contexts-file-with-new-polylabels /etc/selinux/modular-test/contexts/x_contexts
~~~

-   If the X-select and X-paste applications were not built as described in the [Building the X-select and X-paste Applications](http://selinuxproject.org/page/Experimenting_With_X-Windows#Building_the_X-select_and_X-paste_Applications) section, then the executables can be copied from the ./notebook-source/x-windows/x_select+paste directory to the /usr/local/bin directory. They should default to unconfined_t that can be checked as follows:

~~~
ls -Z /usr/local/bin
-rwxr-xr-x. root root system_u:object_r:unconfined_t X-paste
-rwxr-xr-x. root root system_u:object_r:unconfined_t X-select
~~~

-   Open two virtual terminal sessions so that the applications can be run. A third can be opened for monitoring the audit log for errors.
-   Run setenforce 1 for enforcing mode.

**Test 1:**

The X-select and X-paste applications are called directly, one in each terminal session and will therefore run under the unconfined_t domain:

Terminal 1: X-paste Terminal 2: X-select

The results can be seen in [Figure 4](http://taiga.selinuxproject.org/~rhaines/diagrams/Poly-Test1.png) where "Hello World" is displayed on Terminal 1 (note that if any text has been selected by another window, then that text will probably be displayed instead of "Hello World").

There is other information displayed that shows the various context information using the SELinuxGet.. functions that can be examined if required.

To exit the applications 'Ctrl c' is used.

  
**Test 2:**

The applications are then loaded using runcon:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: runcon -t x_select_paste_t X-select

The results can be seen in [Figure 5](http://taiga.selinuxproject.org/~rhaines/diagrams/Poly-Test2.png) where "Hello World" is displayed on Terminal 1.

  
**Test 3:**

The applications are then loaded as follows:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: X-select

As shown in [Figure 6](http://taiga.selinuxproject.org/~rhaines/diagrams/Poly-Test3.png), the X-paste application does NOT receive "Hello World” as the selections are blocked by the polyinstantiation functionality.

  
**Test 4:**

With this test the poly-selection boolean is set to FALSE:

setsebool -P poly-selection false

The applications are then loaded as follows:

Terminal 1: runcon -t x_select_paste_t X-paste Terminal 2: X-select

As shown in [Figure 7](http://taiga.selinuxproject.org/~rhaines/diagrams/Poly-Test4.png), the X-paste application running on terminal 1 does not receive "Hello World" for the following reasons:

-   The selections are being detected by the X-paste application because the type_member rule has been disabled, therefore polyinstantiation is not being enforced by the policy (as to enforce polyinstantiation both the poly_ entries in the x_contexts file is required plus a supporting type_member rule (and of course any allow rules)).
-   The application name and context is not displayed in the X-Window title bar and the terminal screen shows two error returns when getting the property context entries as shown below (the resourceID: 39 is WM_NAME - see Xatom.h).

Calling SELinuxGetPropertyContext (12) with WM_NAME for Property Owner Window:

The SELinuxGetPropertyContext (12) function returned an _XReply error:
BadMatch - Lookup failed for resourceID: 39

Calling SELinuxGetPropertyDataContext (13) with WM_NAME for Property Owner Window:
The SELinuxGetPropertyDataContext (13) function returned an _XReply error:
BadMatch - Lookup failed for resourceID: 39

-   If apol is used to view the Conditional Expressions for the policy, the following will be seen:

conditional expression 1: [poly-selection]

TRUE list:
allow x_select_paste_t wm_name_xproperty_t : x_property { write create }; [Disabled]
allow x_select_paste_t x_select_paste_t : x_selection { getattr setattr read }; [Disabled]
type_member x_select_paste_t primary_xselection_t : x_selection x_select_paste_t; [Disabled]

FALSE list:
type_transition x_select_paste_t wm_name_xproperty_t : x_property user_wm_name_xproperty_t;

Whereas, they should be:

conditional expression 1: [poly-selection]

TRUE list:
allow x_select_paste_t wm_name_xproperty_t : x_property { write create }; [Enabled]
allow x_select_paste_t x_select_paste_t : x_selection { getattr setattr read }; [Enabled]
type_member x_select_paste_t primary_xselection_t : x_selection x_select_paste_t; [Enabled]

FALSE list:
type_transition x_select_paste_t wm_name_xproperty_t : x_property user_wm_name_xproperty_t;

  

#### Polyinstantiated Object Test Conclusions

As can be seen the selected text cannot be pasted between the unconfined_t and x_select_paste_t domains. This means that using polyinstantiated entries will allow selections to be isolated.

If the policy is analysed, it will be seen that the policy enforces the separation with a type member rule. The X-Windows object manager / XACE manages the actual selection polyinstantiation.

~~~
# Extracts from the x_select_paste.conf policy:

# This type_member rules enforces polyinstantiation of the
# "poly_selection PRIMARY primary_xselection_t" x_contexts entry:
type_member x_select_paste_t primary_xselection_t : x_selection x_select_paste_t;

# Additional allow rules:
allow x_select_paste_t self:x_selection { getattr setattr read };
~~~

  

1.  [Jump up↑](http://selinuxproject.org/page/Experimenting_With_X-Windows#cite_ref-ref1_1-0) Also known as 'derived type' because the objects are assigned labels that are derivied from a name based on the SELinux user or a prefix (e.g. from the 'users_extra' configuration file) and then uses a type_transition statement to transition the object to the new label on creation.
