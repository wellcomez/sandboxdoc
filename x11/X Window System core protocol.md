---
date: 2023-05-05 00:47
title: X Window System core protocol
tags:
- x11
- clipboard
---






# X Window System core protocol


The **X Window System core protocol**[[1]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-sche-gett-1)[[2]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-rfc_1013-2)[[3]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-intr-3) is the base protocol of the [X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System"), which is a [networked](https://en.wikipedia.org/wiki/Computer_network "Computer network") [windowing system](https://en.wikipedia.org/wiki/Windowing_system "Windowing system") for [bitmap](https://en.wikipedia.org/wiki/Bitmap "Bitmap") displays used to build [graphical user interfaces](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface") on [Unix](https://en.wikipedia.org/wiki/Unix "Unix"), [Unix-like](https://en.wikipedia.org/wiki/Unix-like "Unix-like"), and other [operating systems](https://en.wikipedia.org/wiki/Operating_system "Operating system"). The X Window System is based on a [client–server model](https://en.wikipedia.org/wiki/Client%E2%80%93server_model "Client–server model"): a single [server](https://en.wikipedia.org/wiki/Server_(computing) "Server (computing)") controls the [input/output](https://en.wikipedia.org/wiki/Input/output "Input/output") hardware, such as the [screen](https://en.wikipedia.org/wiki/Computer_screen "Computer screen"), the [keyboard](https://en.wikipedia.org/wiki/Computer_keyboard "Computer keyboard"), and the [mouse](https://en.wikipedia.org/wiki/Computer_mouse "Computer mouse"); all application [programs](https://en.wikipedia.org/wiki/Computer_program "Computer program") act as [clients](https://en.wikipedia.org/wiki/Client_(computing) "Client (computing)"), interacting with the [user](https://en.wikipedia.org/wiki/User_(computing) "User (computing)") and with the other clients via the server. This interaction is regulated by the X Window System core protocol. Other [protocols](https://en.wikipedia.org/wiki/Protocol_(computing) "Protocol (computing)") related to the X Window System exist, both built at the top of the X Window System core protocol or as separate protocols.

In the X Window System core protocol, only four kinds of packets are sent, [asynchronously](https://en.wiktionary.org/wiki/asynchronously "wikt:asynchronously"), over the network: requests, replies, events, and errors. _Requests_ are sent by a client to the server to ask it to perform some operation (for example, create a new window) and to send back data it holds. _Replies_ are sent by the server to provide such data. _Events_ are sent by the server to notify clients of user activity or other occurrences they are interested in. _Errors_ are packets sent by the server to notify a client of errors occurred during processing of its requests. Requests may generate replies, events, and errors; other than this, the protocol does not mandate a specific order in which packets are sent over the network. Some extensions to the core protocol exist, each one having its own requests, replies, events, and errors.

X originated at [MIT](https://en.wikipedia.org/wiki/Massachusetts_Institute_of_Technology "Massachusetts Institute of Technology") in 1984 (its current release X11 appeared in September 1987). Its designers [Bob Scheifler](https://en.wikipedia.org/wiki/Bob_Scheifler "Bob Scheifler") and [Jim Gettys](https://en.wikipedia.org/wiki/Jim_Gettys "Jim Gettys") set as an early principle that its core protocol was to "create mechanism, not policy". As a result, the core protocol does not specify the interaction between clients and between a client and the user. These interactions are the subject of separate specifications,[[4]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-gett-4) such as the [ICCCM](https://en.wikipedia.org/wiki/ICCCM "ICCCM") and the [freedesktop.org](https://en.wikipedia.org/wiki/Freedesktop.org "Freedesktop.org") specifications, and are typically enforced automatically by using a given [widget set](https://en.wikipedia.org/wiki/Widget_set "Widget set").

## Overview

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/X_client_server_example.svg/250px-X_client_server_example.svg.png)](https://en.wikipedia.org/wiki/File:X_client_server_example.svg)

In this example, the X server takes input from a keyboard and mouse and displays to a screen. A [web browser](https://en.wikipedia.org/wiki/Web_browser "Web browser") and a [terminal emulator](https://en.wikipedia.org/wiki/Terminal_emulator "Terminal emulator") run on the user's workstation, and a terminal emulator runs on a remote server but under the control of the user's machine. Note that the remote application runs just as it would locally.

Communication between server and clients is done by exchanging packets over a [channel](https://en.wikipedia.org/wiki/Communication_channel "Communication channel"). The connection is established by the client (how the client is started is not specified in the protocol). The client also sends the first packet, containing the [byte order](https://en.wikipedia.org/wiki/Byte_order "Byte order") to be used and information about the version of the protocol and the kind of authentication the client expects the server to use. The server answers by sending back a packet stating the acceptance or refusal of the connection, or with a request for a further [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication"). If the connection is accepted, the acceptance packet contains data for the client to use in the subsequent interaction with the server.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Xcore-overview.svg/200px-Xcore-overview.svg.png)](https://en.wikipedia.org/wiki/File:Xcore-overview.svg)

[](https://en.wikipedia.org/wiki/File:Xcore-overview.svg "Enlarge")

An example interaction between a client and a server.

After connection is established, four types of packets are exchanged between client and server over the channel:

1.  _Request:_ The client requests information from the server or requests it to perform an action.
2.  _Reply:_ The server responds to a request. Not all requests generate replies.
3.  _Event:_ The server informs the client of an event, such as keyboard or mouse input, a window being moved, resized or exposed, etc.
4.  _Error:_ The server sends an error packet if a request is invalid. Since requests are queued, error packets generated by a request may not be sent immediately.

Request and reply packets have varying length, while event and error packets have a fixed length of 32 [bytes](https://en.wikipedia.org/wiki/Byte "Byte").

Request packets are numbered sequentially by the server as soon as it receives them: the first request from a client is numbered 1, the second 2, etc. The least significant 16 bits of the sequential number of a request is included in the reply and error packets generated by the request, if any. They are also included in event packets to indicate the sequential number of the request that the server is currently processing or has just finished processing.

## Windows

What is usually called a window in most [graphical user interfaces](https://en.wikipedia.org/wiki/Graphical_user_interface "Graphical user interface") is called a _top-level window_ in the X Window System. The term window is also used to denote windows that lie within another window, that is, the _subwindows_ of a _parent window_. Graphical elements such as [buttons](https://en.wikipedia.org/wiki/Button_(computing) "Button (computing)"), [menus](https://en.wikipedia.org/wiki/Menu_(computing) "Menu (computing)"), [icons](https://en.wikipedia.org/wiki/Icon_(computing) "Icon (computing)"), etc. can be realized using subwindows.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/49/Some_X_windows.svg/378px-Some_X_windows.svg.png)](https://en.wikipedia.org/wiki/File:Some_X_windows.svg)

A possible placement of some windows: 1 is the root window, which covers the whole screen; 2 and 3 are top-level windows; 4 and 5 are subwindows of 2. The parts of a window that are outside its parent are not visible.

A client can request the creation of a window. More precisely, it can request the creation of a subwindow of an existing window. As a result, the windows created by clients are arranged in a [tree](https://en.wikipedia.org/wiki/Tree_(graph_theory) "Tree (graph theory)") (a hierarchy). The root of this tree is the [root window](https://en.wikipedia.org/wiki/Root_window "Root window"), which is a special window created automatically by the server at startup. All other windows are directly or indirectly subwindows of the root window. The top-level windows are the direct subwindows of the root window. Visibly, the root window is as large as the virtual desktop, and lies behind all other windows.

The content of a window is not always guaranteed to be preserved over time. In particular, the window content may be destroyed when the window is moved, resized, covered by other windows, and in general made totally or partly non-visible. In particular, content is lost if the X server is not maintaining a _backing store_ of the window content. The client can request backing store for a window to be maintained, but there is no obligation for the server to do so. Therefore, clients cannot assume that backing store is maintained. If a visible part of a window has an unspecified content, an event is sent to notify the client that the window content has to be drawn again.

Every window has an associated set of _attributes_, such as the _geometry_ of the window (size and position), the background image, whether backing store has been requested for it, etc. The protocol includes requests for a client to inspect and change the attributes of a window.

Windows can be `InputOutput` or `InputOnly`. `InputOutput` windows can be shown on the screen and are used for drawing. `InputOnly` windows are never shown on the screen and are used only to receive input.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Xframe_%28en%29.png/350px-Xframe_%28en%29.png)](https://en.wikipedia.org/wiki/File:Xframe_(en).png)

[](https://en.wikipedia.org/wiki/File:Xframe_(en).png "Enlarge")

Anatomy of a [FVWM](https://en.wikipedia.org/wiki/FVWM "FVWM") window. The white area is the window as created and seen by the client application.

The decorative frame and [title bar](https://en.wikipedia.org/wiki/Title_bar "Title bar") (possibly including buttons) that is usually seen around windows are created by the [window manager](https://en.wikipedia.org/wiki/Window_manager "Window manager"), not by the client that creates the window. The window manager also handles input related to these elements, such as resizing the window when the user clicks and drags the window frame. Clients usually operate on the window they created disregarding the changes operated by the window manager. A change it has to take into account is that [re-parenting window managers](https://en.wikipedia.org/wiki/Re-parenting_window_manager "Re-parenting window manager"), which almost all modern window managers are, change the parent of top-level windows to a window that is not the root. From the point of view of the core protocol, the window manager is a client, not different from the other applications.

Data about a window can be obtained by running the `xwininfo` program. Passing it the `-tree` [command-line](https://en.wikipedia.org/wiki/Command-line "Command-line") argument, this program shows the tree of subwindows of a window, along with their identifiers and geometry data.

## Pixmaps and drawables

A [pixmap](https://en.wikipedia.org/wiki/Pixmap "Pixmap") is a region of memory that can be used for drawing. Unlike windows, pixmaps are not automatically shown on the screen. However, the content of a pixmap (or a part of it) can be transferred to a window and vice versa. This allows for techniques such as [double buffering](https://en.wikipedia.org/wiki/Double_buffering "Double buffering"). Most of the graphical operations that can be done on windows can also be done on pixmaps.

Windows and pixmaps are collectively named _drawables_, and their content data resides on the server. A client can however request the content of a drawable to be transferred from the server to the client or vice versa.

## Graphic contexts and fonts

The client can request a number of graphic operations, such as clearing an area, copying an area into another, drawing points, lines, rectangles, and text. Beside clearing, all operations are possible on all drawables, both windows and pixmaps.

Most requests for graphic operations include a _graphic context_, which is a structure that contains the parameters of the graphic operations. A graphic context includes the foreground color, the background color, the font of text, and other graphic parameters. When requesting a graphic operation, the client includes a graphic context. Not all parameters of the graphic context affect the operation: for example, the font does not affect drawing a line.

The core protocol specifies the use of server-side fonts.[[5]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-font-faq-5) Such fonts are stored as [files](https://en.wikipedia.org/wiki/Computer_file "Computer file"), and the server accesses them either directly via the local [filesystem](https://en.wikipedia.org/wiki/Filesystem "Filesystem") or via the network from another program called _font server_. Clients can request the list of fonts available to the server and can request a font to be loaded (if not already) or unloaded (if not used by other clients) by the server. A client can request general information about a font (for example, the font ascent) and the space a specific string takes when drawn with a specific font.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Xfontsel.png/450px-Xfontsel.png)](https://en.wikipedia.org/wiki/File:Xfontsel.png)

[](https://en.wikipedia.org/wiki/File:Xfontsel.png "Enlarge")

The `xfontsel` program allows the user to view the glyphs of a font.

The names of the fonts are arbitrary strings at the level of the X Window core protocol. The [X logical font description](https://en.wikipedia.org/wiki/X_logical_font_description "X logical font description") conventions[[6]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-logi-font-6) specify how fonts should be named according to their attributes. These conventions also specify the values of optional properties that can be attached to fonts.

The `xlsfonts` program prints the list of fonts stored in the server. The `xfontsel` program shows the glyphs of fonts, and allows the user to select the name of a font for pasting it in another window.

The use of server-side fonts is currently considered deprecated in favour of client-side fonts.[[7]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-herr-hopf-7) Such fonts are rendered by the client, not by the server, with the support of the [Xft](https://en.wikipedia.org/wiki/Xft "Xft") or [cairo](https://en.wikipedia.org/wiki/Cairo_(graphics) "Cairo (graphics)") libraries and the [XRender](https://en.wikipedia.org/wiki/XRender "XRender") extension. No specification on client-side fonts is given in the core protocol.

## Resources and identifiers

All data about windows, pixmaps, fonts, etc. are stored in the server. The client knows [identifiers](https://en.wikipedia.org/wiki/Identifier "Identifier") of these objects—integers it uses as names for them when interacting with the server. For example, if a client wishes a window to be created, it requests the server to create a window with a given identifier. The identifier can be later used by the client to request, for example, a string to be drawn in the window. The following objects reside in the server and are known by the client via a numerical identifier:

-   `Window`
-   `Pixmap`
-   `Font`
-   `Colormap` (a table of colors, described below)
-   `Graphic context`

These objects are called _resources_. When a client requests the creation of one such resource, it also specifies an identifier for it. For example, for creating a new window, the client specifies both the attributes of the window (parent, width, height, etc.) and the identifier to associate with the window.

Identifiers are 32-bit [integers](https://en.wikipedia.org/wiki/Integer "Integer") with their three most significant bits equal to zero. Every client has its own set of identifiers it can use for creating new resources. This set is specified by the server as two integers included in the acceptance packet (the packet it sends to the client to inform it that the connection is accepted). Clients choose identifiers that are in this set in such a way they do not clash: two objects among windows, pixmaps, fonts, colormaps, and graphic contexts cannot have the same identifier.

Once a resource has been created, its identifier is used by the client to request operations about it to the server. Some operations affect the given resource (for example, requests to move windows); others ask for resource data stored from the server (for example, requests for the attributes of windows).

Identifiers are unique to the server, not only to the client; for example, no two windows have the same identifier, even if created by two different clients. A client can access any object given its identifier. In particular, it can also access resources created by any other client, even if their identifiers are outside the set of identifiers it can create.

As a result, two clients connected to the same server can use the same identifier to refer to the same resource. For example, if a client creates a window of identifier `0x1e00021` and passes this number `0x1e00021` to another application (via any available means, for example by storing this number in a file that is also accessible to the other application), this other application is able to operate on the very same window. This possibility is for example exploited by the X Window version of [Ghostview](https://en.wikipedia.org/wiki/Ghostview "Ghostview"): this program creates a subwindow, storing its identifier in an [environment variable](https://en.wikipedia.org/wiki/Environment_variable "Environment variable"), and calls [Ghostscript](https://en.wikipedia.org/wiki/Ghostscript "Ghostscript"); this program draws the content of the [PostScript](https://en.wikipedia.org/wiki/PostScript "PostScript") file to show in this window.[[8]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-ghos-inte-8)

Resources are normally destroyed when the client that created them closes the connection with the server. However, before closing connection, a client can request the server not to destroy them.

## Events

Events are packets sent by the server to a client to communicate that something the client may be interested in has happened. For example, an event is sent when the user presses a key or clicks a mouse button. Events are not only used for input: for example, events are sent to indicate the creation of new subwindows of a given window.

Every event is relative to a window. For example, if the user clicks when the [pointer](https://en.wikipedia.org/wiki/Pointer_(computing_WIMP) "Pointer (computing WIMP)") is in a window, the event will be relative to that window. The event packet contains the identifier of that window.

A client can request the server to send an event to another client; this is used for communication between clients. Such an event is for example generated when a client requests the text that is currently selected: this event is sent to the client that is currently handling the window that holds the selection.

The `Expose` event is sent when an area of a window of destroyed and content is made visible. The content of a window may be destroyed in some conditions, for example, if the window is covered and the server is not maintaining a backing store. The server generates an `Expose` event to notify the client that a part of the window has to be drawn.

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Xevents.svg/450px-Xevents.svg.png)](https://en.wikipedia.org/wiki/File:Xevents.svg)

[](https://en.wikipedia.org/wiki/File:Xevents.svg "Enlarge")

An example of event: when a key is pressed in a window, an event is generated and sent to a client depending on its window event mask, which the client can change.

Most kinds of events are sent only if the client previously stated an interest in them. This is because clients may only be interested in some kind of events. For example, a client may be interested in keyboard-related events but not in mouse-related events. Some kinds of events are however sent to clients even if they have not specifically requested them.

Clients specify which kinds of events they want to be sent by setting an attribute of a window. For example, in order to redraw a window when its content has been destroyed, a client must receive the `Expose` events, which inform it that the window needs to be drawn again. The client will however be sent `Expose` events only if the client has previously stated its interest in these events, which is done by appropriately setting the _event [mask](https://en.wikipedia.org/wiki/Mask_(computing) "Mask (computing)")_ attribute of the window.

Different clients can request events on the same window. They can even set different event masks on the same window. For example, a client may request only keyboard events on a window while another client requests only mouse events on the same window. This is possible because the server, for each window, maintains a separate event mask for each client. However, there are some kinds of events that can only be selected by one client at time for each window. In particular, these events report mouse button clicks and some changes related to window management.

The `xev` program shows the events relative to a window. In particular, `xev -id WID` requests all possible events relative to the window of identifier `WID` and prints them.

## Example

The following is a possible example of interaction between a server and a program that creates a window with a black box in it and exits on a keypress. In this example, the server does not send any reply because the client requests do not generate replies. These requests could generate errors.

1.  The client opens the connection with the server and sends the initial packet specifying the byte order it is using.
2.  The server accepts the connection (no authorization is involved in this example) by sending an appropriate packet, which contains other information such as the identifier of the root window (e.g., `0x0000002b`) and which identifiers the client can create.
3.  The client requests the creation of a default graphic context with identifier `0x00200000` (this request, like the other requests of this example, does not generate replies from the server)
4.  The client requests the server to create a top-level window (that is, it specifies the parent to be the root window `0x0000002b`) with identifier `0x00200001`, size 200x200, position (10,10), etc.
5.  The client requests a change in the attributes of the window `0x00200001`, specifying it is interested in receiving `Expose` and `KeyPress` events.
6.  The client requests the window `0x00200001` to be mapped (shown on the screen)
7.  When the window is made visible and its content has to be drawn, the server sends the client an `Expose` event
8.  In response to this event, the client requests a box to be drawn by sending a `PolyFillRectangle` request with window `0x00200001` and graphic context `0x00200000`

If the window is covered by another window and uncovered again, assuming that backing store is not maintained:

1.  The server sends another `Expose` event to tell the client that the window has to be drawn again
2.  The client redraws the window by sending a `PolyFillRectangle` request

If a key is pressed:

1.  The server sends a `KeyPress` event to the client to notify it that the user has pressed a key
2.  The client reacts appropriately (in this case, it terminates)

## Colors

At the protocol level, a color is represented by a 32-bit unsigned integer, called a _pixelvalue_. The following elements affect the representation of colors:

1.  the [color depth](https://en.wikipedia.org/wiki/Color_depth "Color depth")
2.  the _colormap_, which is a table containing red, green, and blue intensity values
3.  the _visual type_, which specifies how the table is used to represent colors

In the easiest case, the colormap is a table containing a [RGB](https://en.wikipedia.org/wiki/RGB "RGB") triple in each row. A pixelvalue `x` represents the color contained in the `x`-th row of the table. If the client can change the entries in the colormap, this representation is identified by the `PseudoColor` _visual class_. The visual class `StaticColor` is similar, but the client cannot change the entries in the colormap.

There are a total of six possible visual classes, each one identifying a different way for representing an RGB triple with a pixelvalue. `PseudoColor` and `StaticColor` are two. Another two are `GrayScale` and `StaticGray`, which differ in that they only display shades of grey.

The two remaining visual classes differ from the ones above because they break pixelvalues in three parts and use three separate tables for the red, green, and blue intensity. According to this color representation, a pixelvalue is converted into an RGB triple as follows:

1.  the pixelvalue is seen as a sequence of [bits](https://en.wikipedia.org/wiki/Bit "Bit")
2.  this sequence is broken in three parts
3.  each of these three chunks of bits is seen as an integer and used as an index to find a value in each of three separate tables

This mechanism requires the colormap to be composed of three separate tables, one for each [primary color](https://en.wikipedia.org/wiki/Primary_color "Primary color"). The result of the conversion is still a triple of intensity values. The visual classes using this representation are the `DirectColor` and `TrueColor` ones, differing on whether the client can change colormaps or not.

These six mechanisms for representing colors with pixelvalues all require some additional parameters to work. These parameters are collected into a _visual type_, which contains a visual class and other parameters of the representation of colors. Each server has a fixed set of visualtypes, each one associated with a numerical identifier. These identifiers are 32-bit unsigned integers, but are not necessarily different from identifiers of resources or atoms.

When the connection from a client is accepted, the acceptance packet sent by the server contains a sequence of blocks, each one containing information about a single screen. For each screen, the relative block contains a list of other blocks, each one relative to a specific color depth that is supported by the screen. For each supported depth, this list contains a list of visualtypes. As a result, each screen is associated a number of possible depths, and each depth of each screen is associated a number of possible visual types. A given visual type can be used for more screens and for different depths.

For each visual type, the acceptance packet contains both its identifier and the actual parameters it contains (visual class, etc.) The client stores this information, as it cannot request it afterwards. Moreover, clients cannot change or create new visual types. Requests for creation of a new window include the depth and the identifier of the visual type to use for representing colors of this window.

Colormaps are used regardless of whether the hardware controlling the screen (e.g., a [graphic card](https://en.wikipedia.org/wiki/Graphic_card "Graphic card")) uses a [palette](https://en.wikipedia.org/wiki/Palette_(computing) "Palette (computing)"), which is a table that is also used for representing colors. Servers use colormaps even if the hardware is not using a palette. Whenever the hardware uses palettes, only a limited number of colormaps can be installed. In particular, a colormap is installed when the hardware shows colors according to it. A client can request the server to install a colormap. However, this may require the uninstalling of another colormap: the effect is that windows using the uninstalled colormap are not shown with the correct color, an effect dubbed _color flashing_ or _technicolor_. This problem can be solved using _standard colormaps_, which are colormaps with a predictable association between pixelvalues and colors. Thanks to this property, standard colormaps can be used by different applications.

The creation of colormaps is regulated by the [ICCCM](https://en.wikipedia.org/wiki/ICCCM "ICCCM") convention. Standard colormaps are regulated by the ICCCM and by the [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib") specification.

A part of the X colour system is the X Color Management System (xcms). This system was introduced with X11R6 Release 5 in 1991. This system consists of several additional features in xlib, found in the Xcms* series of functions. This system defines device independent color schemes which can be converted into device dependent RGB systems. The system consists of the xlib Xcms* functions and as well the X Device Color Characterization Convention (XDCCC) which describes how to convert the various device independent colour systems into device dependent RGB colour systems. This system supports the [CIEXYZ](https://en.wikipedia.org/wiki/CIEXYZ "CIEXYZ"), [xyY](https://en.wikipedia.org/wiki/XyY "XyY"), [CIELUV](https://en.wikipedia.org/wiki/CIELUV "CIELUV") and [CIELAB](https://en.wikipedia.org/wiki/CIELAB "CIELAB") and as well the [TekHVC](https://en.wikipedia.org/w/index.php?title=TekHVC&action=edit&redlink=1 "TekHVC (page does not exist)") colour systems. [[1]](http://insar.stanford.edu/~lharcke/programming/Xcms/), [[2]](http://tronche.com/gui/x/xlib/color/)

## Atoms

Atoms are 32-bit integers representing [strings](https://en.wikipedia.org/wiki/String_(computing) "String (computing)"). The protocol designers introduced atoms because they represent strings in a short and fixed size:[[9]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-icccm-9) while a string may be arbitrarily long, an atom is always a 32-bit integer. Atom brevity was exploited by mandating their use in the kinds of packets that are likely to be sent many times with the same strings; this results in a more efficient use of the network. The fixed size of atoms was exploited by specifying a fixed size for events, namely 32 bytes: fixed-size packets can contain atoms, while they cannot contain long strings.

Precisely, atoms are identifiers of strings stored in the server. They are similar to the identifiers of resources (Windows, Pixmaps, etc.) but differ from them in two ways. First, the identifiers of atoms are chosen by the server, not by the client. In other words, when a client requests the creation of a new atom, it only sends the server the string to be stored, not its identifier; this identifier is chosen by the server and sent back as a reply to the client. The second important difference between resources and atoms is that atoms are not associated with clients. Once created, an atom survives until the server quits or resets (this is not the default behavior of resources).

Atoms are identifiers and are therefore unique. However, an atom and a resource identifier can coincide. The string associated with an atom is called the _atom name_. The name of an atom cannot be changed after creation, and no two atoms can have the same name. As a result, the name of an atom is commonly used to indicate the atom: “the atom `ABCD`” means, more precisely, “the atom whose associated string is `ABCD`.” or “the atom whose name is `ABCD`.” A client can request the creation of a new atom and can request for the atom (the identifier) of a given string. Some atoms are _predefined_ (created by the server with given identifier and string).

Atoms are used for a number of purposes, mostly related to communication between different clients connected to the same server. In particular, they are used in association with the properties of windows, which are described below.

The list of all atoms residing in a server can be printed out using the program `xlsatoms`. In particular, this program prints each atom (the identifier, that is, a number) with its name (its associated string).

## Properties

Every window has a predefined set of attributes and a set of properties, all stored in the server and accessible to the clients via appropriate requests. Attributes are data about the window, such as its size, position, background color, etc. Properties are arbitrary pieces of data attached to a window. Unlike attributes, properties have no meaning at the level of the X Window core protocol. A client can store arbitrary data in a property of a window.

A property is characterized by a name, a [type](https://en.wikipedia.org/wiki/Datatype "Datatype"), and a value. Properties are similar to [variables](https://en.wikipedia.org/wiki/Variable_(programming) "Variable (programming)") in [imperative programming languages](https://en.wikipedia.org/wiki/Imperative_programming_language "Imperative programming language"), in that a client can create a new property with a given name and type and store a value in it. Properties are associated to windows: two properties with the same name can exist on two different windows while having different types and values.

The name, type, and value of a property are strings; more precisely, they are atoms, that is, strings stored in the server and accessible to the clients via identifiers. A client application can access a given property by using the identifier of the atom containing the name of the property.

Properties are mostly used for inter-client communication. For example, the property named `WM_NAME` (the property named by the atom whose associated string is `"WM_NAME"`) is used for storing the name of windows. [Window managers](https://en.wikipedia.org/wiki/Window_manager "Window manager") typically read this property to display the name of windows in their title bar.

Some types of inter-client communication use properties of the root window. For example, according to the [freedesktop](https://en.wikipedia.org/wiki/Freedesktop "Freedesktop") window manager specification,[[10]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-free-desk-10) window managers should store the identifier of the currently [active window](https://en.wikipedia.org/wiki/Active_window "Active window") in the property named `_NET_ACTIVE_WINDOW` of the root window. The [X resources](https://en.wikipedia.org/wiki/X_resources "X resources"), which contain [parameters](https://en.wikipedia.org/wiki/Parameter_(computer_science) "Parameter (computer science)") of programs, are also stored in properties of the root window; this way, all clients can access them, even if running on different computers.

The `xprop` program prints the properties of a given window; `xprop -root` prints the name, type, and value of each property of the root window.

## Mappings

[![](https://upload.wikimedia.org/wikipedia/commons/1/1c/Akey.jpg)](https://en.wikipedia.org/wiki/File:Akey.jpg)

This key always generates the same _keycode_, but the symbols `/`, `7`, and `{` are associated to three different _keysyms_.

In the X Window System, every individual, physical key is associated a number in the range 8–255, called its _keycode_. A keycode only identifies a key, not a particular character or term (e.g., "Page Up") among the ones that may be printed on the key. Each one of these characters or terms is instead identified by a _keysym_. While a keycode only depends on the actual key that is pressed, a keysym may depend, for example, on whether the Shift key or another [modifier](https://en.wikipedia.org/wiki/Modifier_key "Modifier key") was also pressed.

When a key is pressed or released, the server sends events of type `KeyPress` or `KeyRelease` to the appropriate clients. These events contain:

1.  the keycode of the pressed key
2.  the current state of the modifiers (Shift, Control, etc.) and mouse buttons

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Xkeyboard.svg/400px-Xkeyboard.svg.png)](https://en.wikipedia.org/wiki/File:Xkeyboard.svg)

[](https://en.wikipedia.org/wiki/File:Xkeyboard.svg "Enlarge")

Translation from keycode to keysym.

The server therefore sends the keycode and the modifier state without attempting to translate them into a specific character. It is a responsibility of the client to do this conversion. For example, a client may receive an event stating that a given key has been pressed while the Shift modifier was down. If this key would normally generate the character "a", the client (and not the server) associates this event to the character "A".

While the translation from keycodes to keysyms is done by the client, the table that represents this association is maintained by the server. Storing this table in a centralized place makes it accessible to all clients. Typical clients only request this mapping and use it for decoding the keycode and modifiers field of a key event into a keysym. However, clients can also change this mapping at will.

A modifier is a key that, when pressed, changes the interpretation of other keys. A common modifier is the [Shift key](https://en.wikipedia.org/wiki/Shift_key "Shift key"): when the key that normally produces a lowercase "a" is pressed together with Shift, it produces an uppercase "A". Other common modifiers are "Control", "Alt", and "Meta".

The X server works with at most eight modifiers. However, each modifier can be associated with more than one key. This is necessary because many keyboards have duplicated keys for some modifiers. For example, many keyboards have two "Shift" keys (one on the left and one on the right). These two keys produce two different keycodes when pressed, but the X server associates both with the "Shift" modifier.

For each of the eight modifiers, the X server maintains a list of the keycodes that it consider to be that modifier. As an example, if the list of the first modifier (the "Shift" modifier) contains the keycode `0x37`, then the key that produces the keycode `0x37` is considered a shift key by the X server.

The lists of modifier mappings is maintained by the X server but can be changed by every client. For example, a client can request the "[F1 key](https://en.wikipedia.org/wiki/F1_key "F1 key")" to be added to the list of "Shift" modifiers. From this point on, this key behaves like another shift modifier. However, the keycode corresponding to F1 is still generated when this key is pressed. As a result, F1 operates as it did before (for example, a help window may be opened when it is pressed), but also operates like the shift key (pressing "a" in a text editor while F1 is down adds "A" to the current text).

The X server maintains and uses a modifier mapping for the mouse buttons. However, the buttons can only be [permuted](https://en.wikipedia.org/wiki/Permutation "Permutation"). This is mostly useful for exchanging the leftmost and rightmost button for [left-handed](https://en.wikipedia.org/wiki/Left-handed "Left-handed") users.

The `xmodmap` program shows and changes the key, modifier, and mouse button mappings.

## Grabs

A _grab_ is a condition in which all keyboard or mouse events are sent to a single client. A client can request a grab of the keyboard, the mouse, or both: if the request is fulfilled by the server, all keyboard/mouse events are sent to the grabbing client until the grab is released. The other clients will not receive these events.

When requesting a grab, a client specifies a _grab window_: all events are sent to the grabbing client as if they were relative to the grab window. However, the other clients do not receive events even if they have selected them in the grab window. There are two kinds of grabs:

-   _active:_ the grab takes place immediately
-   _passive:_ the grab takes place only when a previously specified key or mouse button is pressed and terminates when it is released

[![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Xgrab.svg/450px-Xgrab.svg.png)](https://en.wikipedia.org/wiki/File:Xgrab.svg)

[](https://en.wikipedia.org/wiki/File:Xgrab.svg "Enlarge")

If the pointer or the keyboard are frozen, the events they generate are blocked in a queue. If they are grabbed, their events are rerouted to the grabbing client instead of the window that normally receives them. Pointer events can be discarded depending on an event mask.

A client can establish a grab over the keyboard, the pointer, or both. A request for grabbing can include a request for _freezing_ the keyboard or the pointer. The difference between grabbing and freezing is that grabbing changes the recipient of events, while freezing stops their delivery altogether. When a device is frozen, the events it generates are stored in a queue to be delivered as usual when the freeze is over.

For pointer events, an additional parameter affects the delivery of events: an event mask, which specifies which types of events are to be delivered and which ones are to be discarded.

The requests for grabbing include a field for specifying what happens to events that would be sent to the grabbing client even if it had not established the grab. In particular, the client can request them to be sent as usual or according to the grab. These two conditions are not the same as they may appear. For example, a client that would normally receive the keyboard events on a first window may request the keyboard to be grabbed by a second window. Events that would normally be sent to the first window may or may not be redirected to the grab window depending on the parameter in the grab request.

A client can also request the grab of the entire server. In this case, no request will be processed by the server except the ones coming from the grabbing client.

## Other

Other requests and events in the core protocol exist. The first kind of requests is relative to the parent relationship between windows: a client can request to change the parent of a window, or can request information about the parenthood of windows. Other requests are relative to the [selection](https://en.wikipedia.org/wiki/X_Window_selection "X Window selection"), which is however mostly governed by other protocols. Other requests are about the [input focus](https://en.wikipedia.org/wiki/Focus_(computing) "Focus (computing)") and the shape of the [pointer](https://en.wikipedia.org/wiki/Pointer_(user_interface) "Pointer (user interface)"). A client can also request the owner of a resource (window, pixmap, etc.) to be killed, which causes the server to terminate the connection with it. Finally, a client can send a [no-operation](https://en.wikipedia.org/wiki/NOP_(code) "NOP (code)") request to the server.

## Extensions

[![](https://upload.wikimedia.org/wikipedia/commons/9/96/Oclock.png)](https://en.wikipedia.org/wiki/File:Oclock.png)

[](https://en.wikipedia.org/wiki/File:Oclock.png "Enlarge")

The [shape extension](https://en.wikipedia.org/wiki/Shape_extension "Shape extension") allows oclock to create a round window.

The X Window core protocol was designed to be extensible. The core protocol specifies a mechanism for querying the available extensions and how extension requests, events, and errors packets are made.

In particular, a client can request the list of all available extensions for data relative to a specific extension. The packets of extensions are similar to the packets of the core protocol. The core protocol specifies that request, event, and error packets contain an integer indicating its type (for example, the request for creating a new window is numbered 1). A range of these integers are reserved for extensions.

## Authorization

Main article: [X Window authorization](https://en.wikipedia.org/wiki/X_Window_authorization "X Window authorization")

When the client initially establishes a connection with the server, the server can reply by either accepting the connection, refusing it, or requesting [authentication](https://en.wikipedia.org/wiki/Authentication "Authentication"). An authentication request contains the name of the authentication method to use. The core protocol does not specify the authentication process, which depends on the kind of authentication used, other than it ends with the server either sending an acceptance or a refusal packet.

During the regular interaction between a client and a server, the only requests related to authentication are about the _host-based access method_. In particular, a client can request this method to be enabled and can request reading and changing the list of hosts ([clients](https://en.wikipedia.org/wiki/Client_(computing) "Client (computing)")) that are authorized to connect. Typical applications do not use these requests; they are used by the `xhost` program to give a user or a [script](https://en.wikipedia.org/wiki/Shell_script "Shell script") access to the host access list. The host-based access method is considered insecure.

## Xlib and other client libraries

Main article: [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")

Most client programs communicate with the server via the [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib") client library. In particular, most clients use libraries such as [Xaw](https://en.wikipedia.org/wiki/Xaw "Xaw"), [Motif](https://en.wikipedia.org/wiki/Motif_(software) "Motif (software)"), [GTK+](https://en.wikipedia.org/wiki/GTK%2B "GTK+"), or [Qt](https://en.wikipedia.org/wiki/Qt_(toolkit) "Qt (toolkit)") which in turn use Xlib for interacting with the server. The use of Xlib has the following effects:

1.  Xlib makes the client synchronous with respect to replies and events:
    1.  the Xlib functions that send requests block until the appropriate replies, if any is expected, are received; in other words, an X Window client not using Xlib can send a request to the server and then do other operations while waiting for the reply, but a client using Xlib can only call an Xlib function that sends the request and wait for the reply, thus blocking the client while waiting for the reply (unless the client starts a new thread before calling the function);
    2.  while the server sends events [asynchronously](https://en.wiktionary.org/wiki/asynchronous "wikt:asynchronous"), Xlib stores events received by the client in a [queue](https://en.wikipedia.org/wiki/Queue_(data_structure) "Queue (data structure)"); the client program can only access them by explicitly calling functions of the X11 library; in other words, the client is forced to block or [busy-wait](https://en.wikipedia.org/wiki/Busy_waiting "Busy waiting") if expecting an event.
2.  Xlib does not send requests to the server immediately, but stores them in a queue, called the _output buffer_; the requests in the output buffer are actually sent when:
    1.  the program explicitly requests so by calling a library function such as `XFlush`;
    2.  the program calls a function that gives as a result something that involve a reply from the server, such as `XGetWindowAttributes`;
    3.  the program asks for an event in the event queue (for example, by calling `XNextEvent`) and the call blocks (for example, `XNextEvent` blocks if the queue is empty.)

Higher-level libraries such as [Xt](https://en.wikipedia.org/wiki/X_Toolkit_Intrinsics "X Toolkit Intrinsics") (which is in turn used by [Xaw](https://en.wikipedia.org/wiki/Xaw "Xaw") and [Motif](https://en.wikipedia.org/wiki/Motif_(software) "Motif (software)")) allow the client program to specify the [callback functions](https://en.wikipedia.org/wiki/Callback_(computer_science) "Callback (computer science)") associated with some events; the library takes care of polling the event queue and calling the appropriate function when required; some events such as those indicating the need of redrawing a window are handled internally by Xt.

Lower-level libraries, such as [XCB](https://en.wikipedia.org/wiki/XCB "XCB"), provide asynchronous access to the protocol, allowing better latency hiding.

## Unspecified parts

The X Window System core protocol does not mandate over inter-client communication and does not specify how windows are used to form the visual elements that are common in graphical user interfaces ([buttons](https://en.wikipedia.org/wiki/Button_(computing) "Button (computing)"), [menus](https://en.wikipedia.org/wiki/Menu_(computing) "Menu (computing)"), etc.). Graphical user interface elements are defined by client libraries realizing [widget toolkits](https://en.wikipedia.org/wiki/Widget_toolkit "Widget toolkit"). Inter-client communication is covered by other standards such as the [ICCCM](https://en.wikipedia.org/wiki/ICCCM "ICCCM") and [freedesktop](https://en.wikipedia.org/wiki/Freedesktop "Freedesktop") specifications.[[10]](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_note-free-desk-10)

Inter-client communication is relevant to [selections, cut buffers, and drag-and-drop](https://en.wikipedia.org/wiki/X_Window_selection "X Window selection"), which are the methods used by a user to transfer data from a window to another. Since the windows may be controlled by different programs, a protocol for exchanging this data is necessary. Inter-client communication is also relevant to [X window managers](https://en.wikipedia.org/wiki/X_window_manager "X window manager"), which are programs that control the appearance of the windows and the general [look-and-feel](https://en.wikipedia.org/wiki/Look-and-feel "Look-and-feel") of the graphical user interface.

### Session management

Yet another issue where inter-client communication is to some extent relevant is that of [session management](https://en.wikipedia.org/wiki/X_session_manager "X session manager").

How a user session starts is another issue that is not covered by the core protocol. Usually, this is done automatically by the [X display manager](https://en.wikipedia.org/wiki/X_display_manager "X display manager"). The user can however also start a session manually running the [xinit](https://en.wikipedia.org/wiki/Xinit "Xinit") or [startx](https://en.wikipedia.org/wiki/Startx "Startx") programs.

## See also

-   [X Window System protocols and architecture](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture "X Window System protocols and architecture")
-   [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")
-   [Intrinsics](https://en.wikipedia.org/wiki/X_Toolkit_Intrinsics "X Toolkit Intrinsics")
-   [Xnee](https://en.wikipedia.org/wiki/Xnee "Xnee") can be used to sniff the X Window System protocol

## References

1.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-sche-gett_1-0 "Jump up")** Robert W. Scheifler and James Gettys: _X Window System: Core and extension protocols, X version 11, releases 6 and 6.1_, Digital Press 1996, [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [1-55558-148-X](https://en.wikipedia.org/wiki/Special:BookSources/1-55558-148-X "Special:BookSources/1-55558-148-X")
2.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-rfc_1013_2-0 "Jump up")** RFC 1013
3.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-intr_3-0 "Jump up")** Grant Edwards. [An Introduction to X11 User Interfaces](http://www.visi.com/~grante/Xtut/)
4.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-gett_4-0 "Jump up")** Jim Gettys. [Open Source Desktop Technology Road Map](http://freedesktop.org/~jg/roadmap.html) [Archived](https://web.archive.org/web/20060102002339/http://freedesktop.org/~jg/roadmap.html) January 2, 2006, at the [Wayback Machine](https://en.wikipedia.org/wiki/Wayback_Machine "Wayback Machine")
5.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-font-faq_5-0 "Jump up")** ["comp.fonts FAQ: X11 Info"](http://www.faqs.org/faqs/fonts-faq/part15/). _www.faqs.org_.
6.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-logi-font_6-0 "Jump up")** Jim Flowers; Stephen Gildea (1994). ["X Logical Font Description Conventions"](https://web.archive.org/web/20050328124653/http://www.xfree86.org/current/xlfd.pdf) (PDF). _[Digital Equipment Corporation](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation "Digital Equipment Corporation")_. [X Consortium](https://en.wikipedia.org/wiki/X_Consortium "X Consortium"). Archived from [the original](http://www.xfree86.org/current/xlfd.pdf) (PDF) on March 28, 2005. Retrieved 2005-12-30.
7.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-herr-hopf_7-0 "Jump up")** Matthieu Herrb and Matthias Hopf. [New Evolutions in the X Window System](http://www.openbsd.org/papers/eurobsd2005/herrb-hopf.pdf).
8.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-ghos-inte_8-0 "Jump up")** ["Interface with ghostscript - GNU gv Manual"](https://www.gnu.org/software/gv/manual/html_node/Interface-with-ghostscript.html). _www.gnu.org_.
9.  **[^](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-icccm_9-0 "Jump up")** [David Rosenthal](https://en.wikipedia.org/wiki/David_S._H._Rosenthal "David S. H. Rosenthal"). [Inter-Client Communication Conventions Manual](https://en.wikipedia.org/wiki/Inter-Client_Communication_Conventions_Manual "Inter-Client Communication Conventions Manual"). MIT X Consortium Standard, 1989
10.  ^ [Jump up to:_**a**_](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-free-desk_10-0) [_**b**_](https://en.wikipedia.org/wiki/X_Window_System_core_protocol#cite_ref-free-desk_10-1) ["wm-spec"](https://www.freedesktop.org/wiki/Specifications/wm-spec/?action=show&redirect=Standards/wm-spec). _www.freedesktop.org_.

## External links

-   [X.Org Foundation](http://www.x.org/) (official home page) - [Mirror](http://xorg.freedesktop.org/wiki/) with the domain name 'freedesktop.org'.
-   [X Window System Internals](http://xwindow.angelfire.com/)
-   [Kenton Lee's pages on X Window and Motif](http://www.rahul.net/kenton/bib.html)
-   [X Window System Protocol, Version 11 (current Release)](http://www.x.org/releases/current/doc/xproto/x11protocol.html)

hide

-   [v](https://en.wikipedia.org/wiki/Template:XWinSys "Template:XWinSys")
-   [t](https://en.wikipedia.org/wiki/Template_talk:XWinSys "Template talk:XWinSys")
-   [e](https://en.wikipedia.org/w/index.php?title=Template:XWinSys&action=edit)

[X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System")

[Architecture](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture "X Window System protocols and architecture")

-   **Core protocol**
-   [X Window selection](https://en.wikipedia.org/wiki/X_Window_selection "X Window selection")
-   [X Window authorization](https://en.wikipedia.org/wiki/X_Window_authorization "X Window authorization")
-   [X11 color names](https://en.wikipedia.org/wiki/X11_color_names "X11 color names")
-   [X Input Method](https://en.wikipedia.org/wiki/X_Input_Method "X Input Method")
-   [Wayland](https://en.wikipedia.org/wiki/Wayland_(display_server_protocol) "Wayland (display server protocol)")

[![X11.svg](https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/X11.svg/64px-X11.svg.png)](https://en.wikipedia.org/wiki/File:X11.svg)

Extensions

-   [X Image Extension](https://en.wikipedia.org/wiki/X_Image_Extension "X Image Extension")
-   [X keyboard extension](https://en.wikipedia.org/wiki/X_keyboard_extension "X keyboard extension")
-   [RandR](https://en.wikipedia.org/wiki/RandR "RandR")
-   [X Rendering Extension](https://en.wikipedia.org/wiki/X_Rendering_Extension "X Rendering Extension")
-   [Shape extension](https://en.wikipedia.org/wiki/Shape_extension "Shape extension")
-   [Shared memory extension](https://en.wikipedia.org/wiki/MIT-SHM "MIT-SHM")
-   [X Display Manager Control Protocol](https://en.wikipedia.org/wiki/XDM_(display_manager) "XDM (display manager)")
-   [X video extension](https://en.wikipedia.org/wiki/X_video_extension "X video extension")
-   [X-Video Motion Compensation](https://en.wikipedia.org/wiki/X-Video_Motion_Compensation "X-Video Motion Compensation")
-   [AIGLX](https://en.wikipedia.org/wiki/AIGLX "AIGLX")
-   [GLX](https://en.wikipedia.org/wiki/GLX "GLX")
-   [Multi-Pointer X](https://en.wikipedia.org/wiki/Multi-Pointer_X "Multi-Pointer X")
-   [Display PostScript](https://en.wikipedia.org/wiki/Display_PostScript "Display PostScript")
-   [Composite](https://en.wikipedia.org/wiki/Composite_(graphics) "Composite (graphics)")
-   [Xinerama](https://en.wikipedia.org/wiki/Xinerama "Xinerama")

Components  
and notable  
implementations

[Display servers](https://en.wikipedia.org/wiki/Display_server "Display server")

-   **[X.Org Server](https://en.wikipedia.org/wiki/X.Org_Server "X.Org Server")**
-   [Cygwin/X](https://en.wikipedia.org/wiki/Cygwin/X "Cygwin/X")
-   [X-Win32](https://en.wikipedia.org/wiki/X-Win32 "X-Win32")
-   [X386](https://en.wikipedia.org/wiki/X386 "X386")
-   [XFree86](https://en.wikipedia.org/wiki/XFree86 "XFree86")
-   [XDarwin](https://en.wikipedia.org/wiki/XDarwin "XDarwin")
-   [Xming](https://en.wikipedia.org/wiki/Xming "Xming")
-   [Xsgi](https://en.wikipedia.org/wiki/Xsgi "Xsgi")
-   [Xsun](https://en.wikipedia.org/wiki/Xsun "Xsun")
-   [Xgl](https://en.wikipedia.org/wiki/Xgl "Xgl")
-   [Xephyr](https://en.wikipedia.org/wiki/Xephyr "Xephyr")
-   [XQuartz](https://en.wikipedia.org/wiki/XQuartz "XQuartz")
-   [XWayland](https://en.wikipedia.org/wiki/XWayland "XWayland")

Client libraries

-   [Xlib](https://en.wikipedia.org/wiki/Xlib "Xlib")
-   [XCB](https://en.wikipedia.org/wiki/XCB "XCB")
-   [X Toolkit Intrinsics](https://en.wikipedia.org/wiki/X_Toolkit_Intrinsics "X Toolkit Intrinsics")

[Display managers](https://en.wikipedia.org/wiki/X_display_manager "X display manager")  
[Session managers](https://en.wikipedia.org/wiki/X_session_manager "X session manager")

-   [GDM](https://en.wikipedia.org/wiki/GNOME_Display_Manager "GNOME Display Manager")
-   [KDM](https://en.wikipedia.org/wiki/KDE_Display_Manager "KDE Display Manager")
-   [SDDM](https://en.wikipedia.org/wiki/Simple_Desktop_Display_Manager "Simple Desktop Display Manager")
-   [XDM](https://en.wikipedia.org/wiki/XDM_(display_manager) "XDM (display manager)")
-   [LightDM](https://en.wikipedia.org/wiki/LightDM "LightDM")
-   [LXDM](https://en.wikipedia.org/wiki/LXDE "LXDE")
-   [SLiM](https://en.wikipedia.org/wiki/SLiM "SLiM")
-   [Entrance](https://en.wikipedia.org/wiki/Entrance_(display_manager) "Entrance (display manager)")
-   [WDM](https://en.wikipedia.org/wiki/WINGs_Display_Manager "WINGs Display Manager")

[Window managers](https://en.wikipedia.org/wiki/X_window_manager "X window manager")  
([comparison](https://en.wikipedia.org/wiki/Comparison_of_X_window_managers "Comparison of X window managers"))

[Compositing](https://en.wikipedia.org/wiki/Compositing_window_manager "Compositing window manager")

-   [Compiz](https://en.wikipedia.org/wiki/Compiz "Compiz")
-   [KWin](https://en.wikipedia.org/wiki/KWin "KWin")
-   [Metacity](https://en.wikipedia.org/wiki/Metacity "Metacity")
-   [Mutter](https://en.wikipedia.org/wiki/Mutter_(software) "Mutter (software)")
-   [Xfwm](https://en.wikipedia.org/wiki/Xfwm "Xfwm")
-   [Enlightenment](https://en.wikipedia.org/wiki/Enlightenment_(software) "Enlightenment (software)")

[Stacking](https://en.wikipedia.org/wiki/Stacking_window_manager "Stacking window manager")

-   [4Dwm](https://en.wikipedia.org/wiki/4Dwm "4Dwm")
-   [9wm](https://en.wikipedia.org/wiki/9wm "9wm")
-   [AfterStep](https://en.wikipedia.org/wiki/AfterStep "AfterStep")
-   [amiwm](https://en.wikipedia.org/wiki/Amiwm "Amiwm")
-   [Blackbox](https://en.wikipedia.org/wiki/Blackbox "Blackbox")
-   [CTWM](https://en.wikipedia.org/wiki/CTWM "CTWM")
-   [cwm](https://en.wikipedia.org/wiki/Cwm_(window_manager) "Cwm (window manager)")
-   [Fluxbox](https://en.wikipedia.org/wiki/Fluxbox "Fluxbox")
-   [FLWM](https://en.wikipedia.org/wiki/FLWM "FLWM")
-   [FVWM](https://en.wikipedia.org/wiki/FVWM "FVWM")
-   [IceWM](https://en.wikipedia.org/wiki/IceWM "IceWM")
-   [JWM](https://en.wikipedia.org/wiki/JWM "JWM")
-   [Matchbox](https://en.wikipedia.org/wiki/Matchbox_(window_manager) "Matchbox (window manager)")
-   [mwm](https://en.wikipedia.org/wiki/Motif_Window_Manager "Motif Window Manager")
-   [olwm](https://en.wikipedia.org/wiki/Olwm "Olwm")
-   [Openbox](https://en.wikipedia.org/wiki/Openbox "Openbox")
-   [Qvwm](https://en.wikipedia.org/wiki/Qvwm "Qvwm")
-   [Sawfish](https://en.wikipedia.org/wiki/Sawfish_(window_manager) "Sawfish (window manager)")
-   [swm](https://en.wikipedia.org/wiki/Swm "Swm")
-   [tvtwm](https://en.wikipedia.org/wiki/Tvtwm "Tvtwm")
-   [twm](https://en.wikipedia.org/wiki/Twm "Twm")
-   [vtwm](https://en.wikipedia.org/wiki/Vtwm "Vtwm")
-   [WindowLab](https://en.wikipedia.org/wiki/WindowLab "WindowLab")
-   [Window Maker](https://en.wikipedia.org/wiki/Window_Maker "Window Maker")
-   [wm2](https://en.wikipedia.org/wiki/Wm2 "Wm2")

[Tiling](https://en.wikipedia.org/wiki/Tiling_window_manager "Tiling window manager")

-   [awesome](https://en.wikipedia.org/wiki/Awesome_(window_manager) "Awesome (window manager)")
-   [dwm](https://en.wikipedia.org/wiki/Dwm "Dwm")
-   [i3](https://en.wikipedia.org/wiki/I3_(window_manager) "I3 (window manager)")
-   [Ion](https://en.wikipedia.org/wiki/Ion_(window_manager) "Ion (window manager)")
-   [ratpoison](https://en.wikipedia.org/wiki/Ratpoison "Ratpoison")
-   [wmii](https://en.wikipedia.org/wiki/Wmii "Wmii")
-   [xmonad](https://en.wikipedia.org/wiki/Xmonad "Xmonad")
-   [StumpWM](https://en.wikipedia.org/wiki/StumpWM "StumpWM")
-   [larswm](https://en.wikipedia.org/wiki/Larswm "Larswm")

Standards

-   [ICCCM](https://en.wikipedia.org/wiki/Inter-Client_Communication_Conventions_Manual "Inter-Client Communication Conventions Manual")
-   [EWMH](https://en.wikipedia.org/wiki/Extended_Window_Manager_Hints "Extended Window Manager Hints")
-   [XDS](https://en.wikipedia.org/wiki/Direct_Save_Protocol "Direct Save Protocol")
-   [freedesktop.org](https://en.wikipedia.org/wiki/Freedesktop.org "Freedesktop.org")

Applications

-   [xcalc](https://en.wikipedia.org/wiki/Xcalc "Xcalc")
-   [xclock](https://en.wikipedia.org/wiki/Xclock "Xclock")
-   [xedit](https://en.wikipedia.org/wiki/Xedit_(X11) "Xedit (X11)")
-   [xload](https://en.wikipedia.org/wiki/Xload "Xload")
-   [xterm](https://en.wikipedia.org/wiki/Xterm "Xterm")
-   [xeyes](https://en.wikipedia.org/wiki/Xeyes "Xeyes")
-   [Desktop environments (comparison)](https://en.wikipedia.org/wiki/Comparison_of_X_Window_System_desktop_environments "Comparison of X Window System desktop environments")

[Category](https://en.wikipedia.org/wiki/Help:Category "Help:Category"): 

-   [X Window System](https://en.wikipedia.org/wiki/Category:X_Window_System "Category:X Window System")

-   This page was last edited on 19 December 2022, at 21:34 (UTC).
-   Text is available under the [Creative Commons Attribution-ShareAlike License 3.0](https://en.wikipedia.org/wiki/Wikipedia:Text_of_the_Creative_Commons_Attribution-ShareAlike_3.0_Unported_License); additional terms may apply. By using this site, you agree to the [Terms of Use](https://foundation.wikimedia.org/wiki/Terms_of_Use) and [Privacy Policy](https://foundation.wikimedia.org/wiki/Privacy_policy). Wikipedia® is a registered trademark of the [Wikimedia Foundation, Inc.](https://www.wikimediafoundation.org/), a non-profit organization.

-   [Privacy policy](https://foundation.wikimedia.org/wiki/Privacy_policy)
-   [About Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:About)
-   [Disclaimers](https://en.wikipedia.org/wiki/Wikipedia:General_disclaimer)
-   [Contact Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Contact_us)
-   [Mobile view](https://en.m.wikipedia.org/w/index.php?title=X_Window_System_core_protocol&mobileaction=toggle_view_mobile)
-   [Developers](https://developer.wikimedia.org/)
-   [Statistics](https://stats.wikimedia.org/#/en.wikipedia.org)
-   [Cookie statement](https://foundation.wikimedia.org/wiki/Cookie_statement)

-   [![Wikimedia Foundation](https://en.wikipedia.org/static/images/footer/wikimedia-button.png)](https://wikimediafoundation.org/)
-   [![Powered by MediaWiki](https://en.wikipedia.org/static/images/footer/poweredby_mediawiki_88x31.png)](https://www.mediawiki.org/)

Toggle limited content width