---
date: 2023-04-22 00:59
title: Opensuse Leap, X, xauth, flatpak vs. kate – deviating selection of magic cookie entries in .Xauthority – I
tags:
-  XAUTHORITY
---

https://linux-blog.anracom.com/2023/01/12/opensuse-leap-x-xauth-flatpak-vs-kate-deviating-selection-of-magic-cookie-entries-in-xauthority-i/


# Opensuse Leap, X, xauth, flatpak vs. kate – deviating selection of magic cookie entries in .Xauthority – I

Posted on [12. January 2023](https://linux-blog.anracom.com/2023/01/12/opensuse-leap-x-xauth-flatpak-vs-kate-deviating-selection-of-magic-cookie-entries-in-xauthority-i/ "19:10") by [eremo](https://linux-blog.anracom.com/author/eremo/ "View all posts by eremo")

On an average Linux system xorg’s X services or (X)Wayland services are fundamental building blocks for graphical user interfaces like KDE. We use these services every day without thinking much about security measures and other processes in the background. But sometimes strange events wake you up from daily routine. This blog post got triggered by such an event – namely an odd and varying reaction of an X-client application to changes of the network name of a Linux host (running Opensuse Leap 15.3).

This post focuses on a standard cookie-based access mechanism of X-clients to local Xorg-services on the very same Linux host. The basic idea is that the X-server grants access to its socket and services only if the X-client presents a fitting “secret” cookie. The required magic cookie value is defined by the X-server when it is started. The X-client gets information about X-related cookies from a user specific file **~/.Xauthority** which may contain multiple entries. I will show that changing of the name of the host, i.e. the _hostname_, may have an unexpected impact on the subsequent start of X-clients **_and_** that different X-clients may react in deviating ways to a hostname change.

The ambiguity in the startup process of an X-client after a hostname change – even if the correct cookie is in principle available – is demonstrated by different reactions of a standard KDE application, namely **kate**, and a **flatpak**-based application, namely Blender. <mark style="background: #D2B3FFA6;">Flatpak is of interest as there are many complaints on the Internet regarding an unexplained denial of X-server access, especially on Opensuse systems.</mark>

We first look a bit into basic settings for a standard MIT-1 cookie based access mechanism. (For an in-depth introduction to X-server security readers will have to consult specific documentation available from Xorg). Afterward I will show that the _rules_ for selecting a relevant cookie entry in the file .Xauthority deviate between the applications named above. <mark style="background: #D2B3FFA6;">The analysis will show that there is an intimate relation of these rules with available sources of the hostname and .Xauthority entries for various hosts. Hostname changes during running X-sessions may, therefore, have an impact on the start of graphical applications. An unexpected negative impact may even occur after a restart of the DisplayManager.</mark>

In this post we look at the effects of hostname changes _during a running X-session_, e.g. a KDE-session. In further posts I will look a bit deeper into sequences of actions a user may perform following a hostname change – such as a simple logout from and a new login into a graphical session or a full restart of systemd’s graphical target. Even then the consequences of a hostname change may create confusion. If and when I find the time I may also look into aspects of Wayland access in combination with KDE applications and flatpak.

## What had happened? Denial and acceptance of Xorg-server access for a flatpak-based application after changing the host’s name …

Some days ago, I came to a location where I had to use a friend’s WLAN router to get an Internet connection. On my laptop _NetworkManager_ controls the WLAN access. NetworkManager has an option to submit a hostname to the router. As I do not want my standard hostname to be spread around I first changed it via YaST during a running KDE-session. Then I configured NetworkManager for the local WLAN and restarted the network. Access to the WLAN router and Internet worked as expected. However, when I tried to start a flatpak based Blender installation I got the message

**Invalid MIT-MAGIC-COOKIE-1 key Unable to open a display**

This appeared to be strange because the flatpak Blender installation had worked flawlessly before the change of the hostname and before WLAN access. The question that hits you as a normal Linux user is: What do a hostname change and a network restart have to do with the start of a flatpak application in an already running KDE session?

Then I changed the hostname again to a different string – and could afterward start flatpak Blender without any X-authorization problem. Confused? I was.

Then I combined the change of the hostname with an intermediate start of the display manager and/or a stop and restart of systemd’s “graphical target” – and got more disturbing results. So I thought: This is an interesting area worth of looking into it a bit deeper.

The error message, of course, indicated problems with an access to the display offered by the running X-server. Therefore, I wanted answers to the following questions:

-   Why did the magic cookie fail during an already running X-session? Had the X-access conditions not been handled properly already when I logged myself into my KDE session?
-   Why was the magic cookie information invalid the first time, but not during the second trial?
-   What impact has the (changed) hostname on the cookie-based X-authorization mechanism?
-   What can I check by which tool regarding xauth-problems?
-   What does flatpak exactly send to the X server when asking for access?
-   Which rules govern the cookie based X-authorization mechanism for other applications than flatpak-based applications?

I had always assumed that X socket access would follow clear rules – independent of a specific X-client. But after some simple initial tests I started wondering whether the observed variations regarding X access had something to do with entries in the **~/.Xauthority** file of the user which I had used to log into my KDE-session. And I also wondered whether a standard KDE application like “kate” would react to hostname changes in really the same way as a flatpak application.

## Changes of ~/.Xauthority entries …

During a series of experiments I started to manipulate the contents of the _~/.Xauthority_ file of the user of a running KDE session. Whilst doing so I compared the reaction of a typical KDE application like _**kate**_ with the behavior of a (sandboxed) **flatpak Blender installation**. And guess what: The reactions were indeed different.

You find the details of the experiments below. I have not yet analyzed the results with respect to potential security issues. But the variability in the selection and usage of .Xauthority entries by different applications appears a bit worrisome to me. In any case the deviating reactions at least have an impact upon whether an X-client application does start or does not start after one or multiple hostname changes. This makes an automatic handling of certain X-client starts by user scripts a bit more difficult than expected.

## “Transient” state of a Linux system regarding its hostname?

The basic problem is that a user can create a _transient_ system status regarding a change of the static name of a host: A respective entry in the file _/etc/hostname_ may differ from values which other resources of the running graphical desktop session may provide to the user or programs. _Transient_ is a somewhat problematic term. E.g. the command “hostnamectl” may show a _transient_ hostname for some seconds when you change the contents of /etc/hostname directly – until a (systemd) background process settles the status. So what exactly do I mean by “transient”?

By “**transient**” I mean that a change of the static hostname in /etc/hostname may not be reflected by environment variables containing hostname information, e.g. by the variables HOST and HOSTNAME. Conflicting hostname information may occur as long as we do not restart the display manager and/or restart the “graphical target” of systemd after a change of the hostname.

To my astonishment I had to learn that it is not self-evident what will happen in such transient situations regarding access authorization to an Xorg-server via a so called “**magic MIT-1 cookie**“. This is partially due to the fact that the file _“~/.Xauthority”_ may contain _multiple_ entries. See below.

## Objectives of this post

In this first post on the topics named above I will have a look at what happens

-   when we change the static hostname _during a running X-session_ (e.g. a KDE session),
-   when we change entries in the ~/.Xauthority file during a running X-session, in particular with respect to hostname(s), and try to start an X-client afterwards.

The second point will help us to identify _rules_ which X-client applications follow whilst choosing and picking up a cookie value among multiple entries in the file “~/.Xauthority”. We compare the behavior of _“kate”_ as an example for a KDE application with the reaction of a sandboxed _flatpak_ application (Blender). The astonishing result will be that the rules really differ. The rules for flatpak may prevent a start of a flatpak application although a valid entry may be present in .Xauthority.

## Our starting point: A consistent system-status regarding the hostname in a LAN or WLAN environment

The whole problem of an invalid cookie started with a hostname change ahead of a WLAN access. Why is a defined hostname important? How do we change it on an Opensuse system? What are potential sources on the system regarding information about the present hostname? And what should a consistent situation at the beginning of experiments with hostname changes look like?

In a typical structured LAN/WLAN there are multiple hosts wanting to interact with servers on the Internet, but also with each other or servers in (sub-) networks. As an administrator you may define separate sub-nets behind a common gateway. Members of a sub-net may be objects to routing and firewall restrictions regarding the communication with other hosts in the same or other sub-nets or servers on the Internet. As we humans operate with host-names rather than IP-addresses we may give hosts _unique names_ in a defined (sub-) network, use DHCP to statically or dynamically assign IP addresses (and maybe even host-names) and use DNS-services to translate hostnames and related FQDNs into IP-addresses. In a local LAN/WLAN you may have full control over all these ingredients and design a consistent landscape of named and interacting hosts. Plus a pre-designed network segregation, host- routers, gateways, gateway-/perimeter-firewalls, etc.

The situation may be different when you put your laptop in a foreign and potentially dangerous WLAN environment. In my case I use Networkmanager on Leap 15.3 (soon 15.4) to configure WLAN access. The WLAN routers in most networks I have to deal with most often offer both DHCP and DNS services. The IP-address is assigned dynamically. The router’s DNS server works as a forwarder with respect to the Internet. But regarding the local network, which the router controls, the DNS-service of the router may respect your wishes regarding a hostname or not. And you do not know what other hosts creep around in the network you become a member of – and how they see you with respect to your host’s name.

There are three important things I want to achieve in such a situation:

1.  As soon as a WLAN connection to a router gets up I want to establish firewall rules blocking all incoming traffic and limit the outgoing traffic as much as possible.
2.  I do not want the WLAN routers to mingle with the hostname set by me, because this name play a role in certain scripts and some host-internal virtual network environments.
3.  Other friendly hosts in the network may ping me under a certain defined hostname – which the DNS part of the WLAN router should be informed about.

All these things can be managed directly or indirectly by Networkmanager (and some additional scripts). In particular you can a start a script that installs netfilter rules for a certain pre-defined hostname and does further things – e.g. delete or supplement entries in “/etc/hosts”.

However, the host’s name has somehow to be defined ahead of the network connection. There are multiple options to do this. One is to edit the file “/etc/hostnames”. Another is offered by YaST on Opensuse systems. A third is a script of your own, which in addition may manage settings in /etc/hosts, e.g. regarding virtual networks controlled by yourself.

Under perfect circumstances you may achieve a status where you have a well defined static hostname _not_ touched by the WLAN router, a local host-based firewall controlling all required connections _and_ your chosen hostname has been accepted by the router and integrated into its own DNS domain.

Let us check this for the WLAN environment at one of my friends locations:
~~~
myself@xtux:~> hostname; cat /etc/hostname; echo $HOST; echo $HOSTNAME; echo $XAUTHLOCALHOSTNAME; echo $SESSION_MANAGER; hostnamectl
xtux
xtux
xtux
xtux
xtux
local/xtux:@/tmp/.ICE-unix/7289,unix/xtux:/tmp/.ICE-unix/7289
   Static hostname: xtux
         Icon name: computer-laptop
           Chassis: laptop
        Machine ID: ...... (COMMENT: long ID) 
           Boot ID: b5d... (COMMENT: long ID)
  Operating System: openSUSE Leap 15.3
       CPE OS Name: cpe:/o:opensuse:leap:15.3
            Kernel: Linux 5.3.18-150300.59.101-default
      Architecture: x86-64
myself@xtux:~> 

myself@xtux:~ # ping xtux
PING xtux.home (192.168.10.186) 56(84) bytes of data.
64 bytes from xtux.home (192.168.10.196): icmp_seq=1 ttl=64 time=0.030 ms
64 bytes from xtux.home (192.168.10.196): icmp_seq=2 ttl=64 time=0.060 ms
^C
--- xtux.home ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2013ms
~~~

(The COMMENTs are not real output, but were added by me.)  
You see that there is a whole variety of potential sources regarding information about the host’s name. In particular there are multiple environment variables. In the situation shown above all information sources agree about the host’s name, namely “xtux”. But you also see that the local domain name used by the WLAN router is a default one used by a certain router vendor. You would get the domain present name used also by issuing the command “dnsdomainname” at a shell-prompt.

An interesting question is: Which sources will register a hostname change during a running KDE session? Another interesting question is: Is the hostname used for local X-access authorization? The answers will be given by the experiments described below.

## Cookie based X-server access by the MIT-MAGIC-COOKIE-1 mechanism

<mark style="background: #D2B3FFA6;">X-server access can be controlled by a variety of mechanisms. The one we focus on here is a cookie-based access. The theory is that an X-server when it starts up queries the hostname and creates a “secret” cookie plus a hash defining a file where the server saves the cookie.</mark> Afterward any X-client must provide this specific _magic_ cookie when trying to get access to the X-server (more precisely to its socket). See e.g. a [Wikipedia](https://en.wikipedia.org/wiki/X_Window_authorization) article for these basic principles.

Let us check what happens when we start a X-server. On an Opensuse system a variety of systemd-services is associated with a pseudo-state “3” of the host which can be set by a command “init 3”. This “state” corresponds, of course, to a systemd target. Network and multiuser operations are provided. A command “init 5” then moves the system to the graphical target. This includes the start of the Display Manager – in my case _sddm_. sddm in turn starts xorg’s X server. On an Opensuse system see the file **/etc/sysconfig/displaymanager** for respective settings.

Now let us get some information from the system about these points. First we look at the command which had been used to start the X-server. As root:

~~~
xtux:~ # pgrep -a X
16690 /usr/bin/X -nolisten tcp -auth /run/sddm/{0ca1db02-e253-4f2b-972f-9b124764a65f} -background none -noreset -displayfd 18 -seat seat0 vt7

The path after the -auth option gives us the location of the file containing the “magic cookie”. We can analyze its contents by the **xauth** command:

xtux:~ # xauth -f /run/sddm/\{0ca1db02-e253-4f2b-972f-9b124764a65f\} list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
~~~

(Hint: The tab-key will help you to avoid retyping the long hash). Ok, here we have our secret cookie for further authentication of applications wanting to get access to the X-server’s socket.

The other side of the story is the present _user_ who opened an X-session – in my case a KDE session. Where does it or any X-clients, which the user later starts on a graphical desktop, get the knowledge about the required cookie from?

Answer: Whilst logging in via the DisplayManager an entry is written into or replaced inside the file **~/.Xauthority** (by a root controlled process). If this file does not exist it is created.

The .Xauthority file should normally not be edited as it contains non-interpretable characters. But the command “**xauth list**” helps again to present the contents in readable form. It automatically picks the file ~/.Xauthority:

myself@xtux:~> xauth list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0

We see the same cookie here as on the local X-server’s side. Any applications with graphical output can and should use this information to get access to the X-server. E.g. “ssh -X” can use this information to rewrite cookie information set during interaction with a remote system to the present local X-server cookie. And there are of course the sandboxed flatpak applications in their namespaces. Note that the screen used by definition is :0 on the local host.

Note further that the situation regarding .Xauthority is ideal: In our present situation it contains only one entry. On systems working a lot on different remote hosts this file normally contains multiple entries, in particular if you have opened X-connections to other hosts in the past. Or, if you have changed your hostname before …

## Access to the running X-server by a flatpak-based Blender installation

When we start a flatpak Blender installation this will just open the Blender interface on the hosts graphical desktop screen. We can close Blender directly afterward again.

myself@xtux:~> flatpak run org.blender.Blender &
[1] 23145
myself@xtux:~> Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender
myself@xtux:~> 

We shall later see that flatpak replaces a screen-number :99 used in the applications namespace to :0 when interacting with the locally running X-server. In this respect it seems to work similar to “ssh -X”.

## Disturbing the hostname settings

What happens if we disturb the initial consistent setup by changing the host’s name during a running X-session?

We have already identified potential sources of a mismatch regarding the hostname: /etc/hostname vs. a variety of environment variables vs. the DNS system.  
The reader has certainly also noticed that the entry in the ~/.Xauthority file starts with the present name of the host. So here we have yet another potential source of a mismatch after a change of the hostname.

There are obvious limitations: The present cookie value stored at the location /run/sddm/\{..} should not be overwritten. This may disturb running X-clients and the start of other application windows in sub-shells of the running KDE-session. Changing environment variables which contain the hostname, may be dangerous, too.

On the other side the entry in ~/.Xauthority may not fit the new hostname if the entry is not being adapted. Actually, changing the hostname via YaST on an Opensuse system leaves .Xauthority entries as they were. So how would a KDE application and a flatpak application react to such discrepancies?

## Basic Experiment: Change the hostname during a KDE session and try to run a flatpak-based Blender application afterward

There are multiple options to change the hostname. We could overwrite the contents of /etc/hostname directly as root. (And wait for some systemd action to note the difference) But that does not automatically change other network settings. Let us use therefore use YaST in the form of the graphical yast2.

[![](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast_network.png)](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast_network.png)

[![](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast2_warning.png)](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast2_warning.png)

[![](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast2_change-hostname.png)](https://linux-blog.anracom.com/wp-content/uploads/2023/01/yast2_change-hostname.png)

We just ignore the warning and define a new hostname “xmux”. Then we let yast2 do its job to reconfigure the network settings. Note that this will not interrupt an already running NetworkManager-controlled WLAN connection. Afterward we check our bash environment:

myself@xtux:~> hostname; cat /etc/hostname; echo $HOST; echo $HOSTNAME; echo $XAUTHLOCALHOSTNAME; echo $SESSION_MANAGER; hostnamectl
xmux
xmux
xtux
xtux
xtux
local/xtux:@/tmp/.ICE-unix/7289,unix/xtux:/tmp/.ICE-unix/7289
   Static hostname: xmux
         Icon name: computer-laptop
           Chassis: laptop
        Machine ID: .....   (COMMENT: unchangd long string) 
           Boot ID: 65c..   (COMMENT: changed long string)
  Operating System: openSUSE Leap 15.3
       CPE OS Name: cpe:/o:opensuse:leap:15.3
            Kernel: Linux 5.3.18-150300.59.101-default
      Architecture: x86-64
myself@xtux:~> xauth list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0

We got a new bootid, but neither the environment variables nor the contents of ~/.Xauthority have been changed. Will our flatpak-based _Blender_ run?  
Answer: Yes, it will – without any warnings! Will _kate_ run? Yes, it will, but with some warning:

myself@xtux:~> kate &
[1] 6685
myself@xtux:~> No protocol specified

The reaction of kate is a bit questionable. Obviously, it detects an unexpected discrepancy, but starts nevertheless.

There are 2 possible explanations regarding flatpak: 1) flatpack ignores the hostname in the .Xauthority entry. It just reacts to the screen number. 2) flatpak remembers the last successful cookie and uses it.

How can we test this?

## Preparation of further experiments: How to change the contents of .Xauthority manually

One way to test the reaction of applications to discrepancies of a changed hostname with cookie entries in .Xauthority is to _manually_ change entries in this file. This is not as easy as it may seem as this file has weird characters in it. But it is possible with the help of kate or kwrite. You need an editor which can grasp most of the symbols in a sufficient way. vi is not adequate.

BUT: You have to be very careful when copying entry lines. Identify the symbol sequence marking the beginning of an entry is a first important step. Note also: When you change a hostname of an entry you must use one of the _same_ length!  
AND: Keep a copy of the original .Xauthority-file somewhere safe during your experiments.

## Multiple entries in ~/.Xauthority

The file ~/.Xauthority can in principle have multiple entries for different hosts and different X-servers. Among other scenarios multiple entries are required to cover situations for

1.  for direct access of a local X-client via a network to other hosts’ X-servers
2.  for remote application access based on “ssh -X” and a display of the output on the local host’s X-server.

Furthermore: A restart of an X-server will lead to new _additional_ entries in ~/.Xauthority, whilst existing entries are kept there.

Therefore, it is wise to work with multiple entry lines in ~/.Xauthority for further experiments. However: Multiple entries with a combination of hostnames and cookie values open up a new degree of freedom:

The decision whether an X-client application gets successfully started or not will not only depend on a cookie match at the X-server, but also on the selection of an entry in the file ~/.Xauthority.

Hopefully, the results of experiments, which mix old, changed and freely chosen hostnames with valid and invalid cookie values will give us answers to the question of how a hostname change affects running X-sessions and freshly started X-clients. We are especially interested in the rules that guide an application when it must select a particular entry in .Xauthority among others. If we are lucky we will also get an idea about how an intermediate restart of the X-server after a hostname change may influence the start of X-clients afterward.

During the following experiments I try to formulate and improve rules regarding kate and flatpak applications with respect to the selection and usage of entries in .Xauthority.

Please note:

In all forthcoming experiments we only consider local applications which try to gain access to a locally running X-server!

The cookie of an entry in ~/.Xauthority is considered to be “correct” if it matches the magic cookie of the running X-server. Otherwise we regard it as “incorrect” with respect to the running X-server.

## Experiment 1: Change of the file ~/.Xauthority – leading entry for the original hostname with an incorrect cookie, second entry for new hostname with the correct cookie

I prepared a file /home/myself/.Xauthority (as root) during the running X-session with the following entries:

myself@xtux:~> xauth list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
myself@xtux:~> kate
myself@xtux:~> flatpak run org.blender.Blender &
[1] 28187
myself@xtux:~> Invalid MIT-MAGIC-COOKIE-1 keyUnable to open a display
^C
[1]+  Exit 134                flatpak run org.blender.Blender
myself@xtux:~>

You see that I have changed the hostname (xtux) of our original entry (with the correct cookie) to the meanwhile changed hostname (xmux). This entry is associated with the presently valid magic cookie. And I have added a leading entry with the original hostname, but a modified and therefore invalid cookie value.

Now, let us check what a KDE application like _kate_ would do afterward: The output above shows that it just started – without any warning. However, flatpak did and does NOT start:

myself@xtux:~> flatpak run org.blender.Blender &[1] 16686
myself@xtux:~> Invalid MIT-MAGIC-COOKIE-1 keyUnable to open a display

## Experiment 2: ~/.Xauthority with a first entry for the new hostname but with an invalid cookie, plus a second entry for the original hostname with the correct cookie

Let us change the entries to:

myself@xtux:~> xauth list
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
myself@xtux:~> kate
Invalid MIT-MAGIC-COOKIE-1 keymyself@xtux:~> flatpak run org.blender.Blender &
[1] 28371
myself@xtux:~> Invalid MIT-MAGIC-COOKIE-1 keyUnable to open a display
^C
[1]+  Exit 134                flatpak run org.blender.Blender
myself@xtux:~> 

It may seem that a line break is missing in the output and that kate did not start. But this is wrong:

-   Kate actually DID start! But it produced an alarming warning!
-   However flatpak did NOT start AND gave us a warning!

Meaning:

We got a clear indication that

-   different entries are used by our two applications and
-   that the potential discrepancies of the hostnames associated with the .Xauthority-entries in comparison to environment variables and /etc/hostname are handled differently by our two applications.

Why kate starts despite the clear warning is a question others have to answer. I see no direct security issue, but I have not really thought it through.

## Experiment 3: ~/.Xauthority with a leading entry for original hostname and the correct cookie plus entry for the new hostname but an incorrect cookie

Let us now change the order of the cookie entries:

myself@xtux:~> xauth list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
myself@xtux:~> kate
Invalid MIT-MAGIC-COOKIE-1 keymyself@xtux:~> flatpak run org.blender.Blender &
[1] 28571
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender
myself@xtux:~> 

kate again gives us a warning, but starts.  
And, oh wonder, flatpak now does start Blender in its namespace without any warning!

## Experiment 4: ~/.Xauthority with a leading entry for the new hostname and the correct cookie, plus an entry for the original hostname but an invalid cookie

Let us switch the hostnames again in the given order of the cookie entries. This gives us the last variation for mixing the new and the old hostnames with valid/invalid cookies :

myself@xtux:~> xauth list
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
myself@xtux:~> kate
myself@xtux:~> flatpak run org.blender.Blender &
[1] 28936
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender
myself@xtux:~> 

kate now starts without any warning. And also flatpak starts without warning.

## Intermediate, but insufficient interpretation – reason for further experiments

How can we interpret the results above? So far the results are consistent with the following rules:  
**kate:** Whenever the cookie associated with the present static hostname in .Xauthority matches the X-server’s cookie kate will start without warning. Otherwise it issues a warning, but starts nevertheless.  
**flatpak:** Whenever the first entry in .Xauthority provides a cookie that matches the X-server’s cookie for the session than flatpak starts an X-client program like Blender.

But things are more complicated than this. We also have to check what happens if we, for some reason, have entries in ~/.Xauthority that reflect a hostname neither present in /etc/hostname nor in environment variables. (Such an entry may have resulted from previous access trials to the X-servers of remote hosts or “ssh -X” connections.)

I will call such a hostname a locally “**_unknown_ hostname**” below. It admit that this is not the best wording, but it is a short one. A “**_known_ hostname**” instead is one either provided by environment variables _**or**_ being present in /etc/hostname.

## Experiment 5: ~/.Xauthority with a leading entry for an unknown hostname and the correct cookie, plus an entry for the new hostname but an incorrect cookie

Entries of the form

myself@xtux:~> xauth list
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44

reflect such a situation.

The reactions of both kate and flatpak are negative in the given situation:

myself@xtux:~> xauth list
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
myself@xtux:~> kate
Invalid MIT-MAGIC-COOKIE-1 keyNo protocol specified
qt.qpa.xcb: could not connect to display :0
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.
Failed to create wl_display (No such file or directory)
qt.qpa.plugin: Could not load the Qt platform plugin "wayland" in "" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.

Available platform plugins are: wayland-org.kde.kwin.qpa, eglfs, linuxfb, minimal, minimalegl, offscreen, vnc, wayland-egl, wayland, wayland-xcomposite-egl, wayland-xcomposite-glx, xcb.

Abgebrochen (Speicherabzug geschrieben)
myself@xtux:~> 
myself@xtux:~> flatpak run org.blender.Blender &
[1] 30565
myself@xtux:~> Invalid MIT-MAGIC-COOKIE-1 keyUnable to open a display
^C
[1]+  Exit 134                flatpak run org.blender.Blender
myself@xtux:~> 

Meaning:

_flatpak_ reacts allergic to entries with unknown hostnames and to entries with known hostnames, but with a wrong cookie.

## Experiment 6: ~/.Xauthority with a leading entry for the new hostname and the correct cookie, plus an entry for an unknown hostname with an invalid cookie

However:

myself@xtux:~> xauth list
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
myself@xtux:~> kate
myself@xtux:~> flatpak run org.blender.Blender &
[1] 30710
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender
myself@xtux:~> 

Both applications start without warning!

## Experiment 7: ~/.Xauthority with a leading entry for the original hostname and the correct cookie, plus an entry for an unknown hostname with an invalid cookie

The reaction of our applications changes again for the following settings:

myself@xtux:~> xauth list
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
myself@xtux:~> kate
No protocol specified
myself@xtux:~> flatpak run org.blender.Blender &
[1] 30859
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender
myself@xtux:~> 

Meaning:

If the hostname associated with the right cookie is present in the environment variables, but does not correspond to the contents of /etc/hostname then kate will start with some warning. Flatpak starts starts without warning.

## Experiment 8: ~/.Xauthority with a leading entry for an unknown hostname and an incorrect cookie, plus an entry for a known hostname with an invalid cookie

Switching entries and renaming confirms previous results:

myself@xtux:~> xauth list
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
xtux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
myself@xtux:~> kate
No protocol specified
myself@xtux:~> flatpak run org.blender.Blender &
[1] 31113
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

[1]+  Fertig                  flatpak run org.blender.Blender

And:

myself@xtux:~> xauth list
xfux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2a44
xmux/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
myself@xtux:~> kate
myself@xtux:~> flatpak run org.blender.Blender &
[1] 31348
myself@xtux:~> /run/user/1004/gvfs/ non-existent directory
Saved session recovery to '/tmp/quit.blend'

Blender quit

## Experiment 9: ~/.Xauthority with entries for unknown hostnames, only

Let us use unknown hostnames, only.

myself@xtux:~> xauth list
xfuxi/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
xruxi/unix:0  MIT-MAGIC-COOKIE-1  650ade473bc07c2e981d6174871c2ad0
myself@xtux:~> kate
No protocol specified
No protocol specified
qt.qpa.xcb: could not connect to display :0
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.
Failed to create wl_display (No such file or directory)
qt.qpa.plugin: Could not load the Qt platform plugin "wayland" in "" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.

Available platform plugins are: wayland-org.kde.kwin.qpa, eglfs, linuxfb, minimal, minimalegl, offscreen, vnc, wayland-egl, wayland, wayland-xcomposite-egl, wayland-xcomposite-glx, xcb.

Abgebrochen (Speicherabzug geschrieben)
myself@xtux:~> flatpak run org.blender.Blender &
[1] 29115
myself@xtux:~> No protocol specified
Unable to open a display
^C
[1]+  Exit 134                flatpak run org.blender.Blender

So, having unknown hostnames only, will lead to no X-access, neither for flatpak nor kate.

## Derived rules for the selection of an entry in ~/.Xauthority

So the rules for our two selected applications regarding the selection of an entry in ~/.Xauthority and resulting X-server access are more like described below:

-   **kate:** If an entry in .Xauthority has a known hostname and fits the X server’s cookie kate is started – with a warning, if the hostname does not fit the present static hostname (in /etc/hostname). If there is an additional deviating entry for the present static hostname, but with an incorrect cookie then the warning includes the fact that the cookie is invalid, but kate starts nevertheless.
-   **flatpak Blender:** The first entry which matches a known hostname (among the available ones from environment variables or from /etc/hosts) and which matches screen :0 is used to pick the respective cookie for X-server access. The application (X-client) only starts if the resulting cookie matches the X-server’s cookie.
-   **Both:** If ~/.Xauthority does not contain entries which match any of the known hostnames than both programs fail regarding X-access. kate does check for other possible sockets (e.g. for (X)Wayland) in this case.

When evaluating these rules with respect to security issues one should always keep in mind that .Xauthority-entries like the ones we have artificially constructed may have been the result of a sequence of hostname changes followed by restarts of the X-server. This will become clearer in the next post.

## Conclusion

By some simple experiments one can show that the access to an Xorg-server requested by a X-client application does not only depend on a cookie match but also on the combination of hostnames and associated magic cookie values offered by multiple entries in the file ~/.Xauthority. The rules by which an X-client application selects a specific entry may depend on the application and the rules may differ from those other applications follow. We have seen that at least a flatpak based Blender installation follows other rules than e.g. KDE’s kate. Therefore, changes of the hostname during a running X-session may have an impact on the startup of applications. E.g. if .Xauthority already contains an entry for the new hostname.

The attentive reader has, of course, noticed that the experiments described above alone do not explain the disturbing reaction of flatpak to hostname changes described in the beginning. These reactions had to do with already existing entries for certain hostnames in .Xauthority. Additional entries may in general be the result of previous (successful) accesses to remote hosts’ X-servers or previous local hostname changes followed by X-server restarts. In the next post I will, therefore, extend the experiments to intermediate starts of both the X-server and the graphical target after hostname changes.

## Links

[Opensuse Leap 15.3 documentation on xauth](https://doc.opensuse.org/documentation/leap/archive/15.3/security/html/book-security/cha-xauth.html)

[Stackoverflow question on “How does X11 authorization work? (MIT Magic Cookie)”](https://stackoverflow.com/questions/37157097/how-does-x11-authorization-work-mit-magic-cookie)

[Stackexchange question on Invalid MIT-MAGIC-COOKIE-1 keyxhost: unable to open display “:0”](https://unix.stackexchange.com/questions/649651/invalid-mit-magic-cookie-1-keyxhost-unable-to-open-display-0)

[Opensuse bug Bug 1137491 – flatpak/snap: “Invalid MIT-MAGIC-COOKIE-1 key” after resume (network/hostname changes?)](https://bugzilla.opensuse.org/show_bug.cgi?id=1137491)

Also see comment of the 25th og January, 2019, in an archived [Opensuse.org contribution](https://lists.opensuse.org/archives/list/users@lists.opensuse.org/thread/TUD73M5G6AX2CVNOR4K4TGBJLA5EMNPQ/)

And before we who love democracy and freedom forget it:  
The worst fascist, war criminal and killer living today is the Putler. He must be isolated at all levels, be denazified and sooner than later be imprisoned. Somebody who orders the systematic destruction of civilian infrastructure must be fought and defeated because he is a danger to mankind and principles of humanity. Long live a free and democratic Ukraine!

This entry was posted in [Leap 15.3](https://linux-blog.anracom.com/category/opensuse/leap-15-3/), [Netzwerk](https://linux-blog.anracom.com/category/netzwerk/), [Opensuse](https://linux-blog.anracom.com/category/opensuse/), [Security und Security-Tools](https://linux-blog.anracom.com/category/security-und-security-tools/) and tagged [.Xauthority entries](https://linux-blog.anracom.com/tag/xauthority-entries/), [denial of access to X-server](https://linux-blog.anracom.com/tag/denial-of-access-to-x-server/), [flatpak](https://linux-blog.anracom.com/tag/flatpak/), [hostname](https://linux-blog.anracom.com/tag/hostname/), [hostname changes](https://linux-blog.anracom.com/tag/hostname-changes/), [Invalid MIT-MAGIC-COOKIE-1](https://linux-blog.anracom.com/tag/invalid-mit-magic-cookie-1/), [kate](https://linux-blog.anracom.com/tag/kate/), [magic cookie](https://linux-blog.anracom.com/tag/magic-cookie/), [magic MIT-1 cookie](https://linux-blog.anracom.com/tag/magic-mit-1-cookie/), [Opensuse Leap](https://linux-blog.anracom.com/tag/opensuse-leap/), [X](https://linux-blog.anracom.com/tag/x/), [X access authorization](https://linux-blog.anracom.com/tag/x-access-authorization/), [X security](https://linux-blog.anracom.com/tag/x-security/), [X-server](https://linux-blog.anracom.com/tag/x-server/), [X-server access](https://linux-blog.anracom.com/tag/x-server-access/), [X11](https://linux-blog.anracom.com/tag/x11/), [xauth](https://linux-blog.anracom.com/tag/xauth/), [xauth list](https://linux-blog.anracom.com/tag/xauth-list/), [Xauthority](https://linux-blog.anracom.com/tag/xauthority/), [Xorg](https://linux-blog.anracom.com/tag/xorg/), [YaST](https://linux-blog.anracom.com/tag/yast/) by [eremo](https://linux-blog.anracom.com/author/eremo/). Bookmark the [permalink](https://linux-blog.anracom.com/2023/01/12/opensuse-leap-x-xauth-flatpak-vs-kate-deviating-selection-of-magic-cookie-entries-in-xauthority-i/ "Permalink to Opensuse Leap, X, xauth, flatpak vs. kate – deviating selection of magic cookie entries in .Xauthority – I").

[Datenschutz](https://linux-blog.anracom.com/datenschutz/) [Proudly powered by WordPress](https://wordpress.org/ "Semantic Personal Publishing Platform")

Dieser Blog verwendet nur technisch notwendige (Session-) Cookies. Bei weiterer Nutzung gehen wir von Ihrem Einverständnis aus. [Optionen / Informationen](https://linux-blog.anracom.com/datenschutz/) Verstanden