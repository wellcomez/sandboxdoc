---
date: 2023-04-17 16:35
title: X Window authorization
tags:
- x11
- XAUTHORITY
---



# X Window authorization


From Wikipedia, the free encyclopedia

In the [X Window System](https://en.wikipedia.org/wiki/X_Window_System "X Window System"), [programs](https://en.wikipedia.org/wiki/Computer_program "Computer program") run as X clients, and as such they connect to the X [display server](https://en.wikipedia.org/wiki/Display_server "Display server"), possibly via a [computer network](https://en.wikipedia.org/wiki/Computer_network "Computer network"). Since the network may be accessible to other [users](https://en.wikipedia.org/wiki/User_(computing) "User (computing)"), a method for forbidding access to programs run by users different from the one who is logged in is necessary.

There are five standard access control mechanisms that control whether a client application can connect to an X display server. They can be grouped in three categories:

1.  access based on host
2.  access based on cookie
3.  access based on user

Additionally, like every other network connection, [tunneling](https://en.wikipedia.org/wiki/Tunneling_protocol "Tunneling protocol") can be used.

## Host-based access

The host-based access method consists in specifying a set of hosts that are authorized to connect to the X display server. This system has inferior security, as it allows every user who has access to such a host to connect to the display. The **`xhost`** program and three [X Window System core protocol](https://en.wikipedia.org/wiki/X_Window_System_core_protocol "X Window System core protocol") requests are used to activate this mechanism and to display and change the list of authorized hosts. Improper use of `xhost` can inadvertently give every host on the Internet full access to an X display server.

## Cookie-based access

The cookie-based authorization methods are based on choosing a [magic cookie](https://en.wikipedia.org/wiki/Magic_cookie "Magic cookie") (an arbitrary piece of data) and passing it to the X display server when it is started; every client that can prove having knowledge of this cookie is then authorized connecting to the server.

These cookies are created by a separate program and stored in the file `.Xauthority` in the user's home directory, by default. As a result, every program run by the client on the local computer can access this file and therefore the cookie that is necessary for being authorized by the server. If the user wants to run a program from another computer on the network, the cookie has to be copied to that other computer. How the cookie is copied is a system-dependent issue: for example, on [Unix-like](https://en.wikipedia.org/wiki/Unix-like "Unix-like") platforms, [scp](https://en.wikipedia.org/wiki/Secure_copy "Secure copy") can be used to copy the cookie.

The two systems using this method are `MIT-MAGIC-COOKIE-1` and `XDM-AUTHORIZATION-1`. In the first method, the client simply sends the cookie when requested to authenticate. In the second method, a [secret key](https://en.wikipedia.org/wiki/Secret_key "Secret key") is also stored in the `.Xauthority` file. The client creates a string by concatenating the current time, a transport-dependent identifier, and the cookie, encrypts the resulting string, and sends it to the server.

The **xauth** application is a utility for accessing the `.Xauthority` file. The environment variable `XAUTHORITY` can be defined to override the name and location of that cookie file.

The [Inter-Client Exchange (ICE) Protocol](http://www.xfree86.org/current/ice.pdf) implemented by the [Inter-Client Exchange Library](https://www.x.org/releases/X11R7.7/doc/libICE/ICElib.html) for direct communication between X11 clients uses the same `MIT-MAGIC-COOKIE-1` authentication method, but has its own **iceauth** utility for accessing its own `.ICEauthority` file, the location of which can be overridden with the environment variable `ICEAUTHORITY`. [ICE](https://en.wikipedia.org/wiki/X_Window_System_protocols_and_architecture#Inter-client_communication "X Window System protocols and architecture") is used, for example, by [DCOP](https://en.wikipedia.org/wiki/Desktop_communication_protocol "Desktop communication protocol") and the X Session Management protocol (XSMP).

## User-based access

The user-based access methods work by authorizing specific users to connect to the server. When a client establishes a connection to a server, it has to prove being controlled by an authorized user.

The two methods based on authenticating users using networked identity management systems are `SUN-DES-1` and `MIT-KERBEROS-5`. The first system is based on a secure mechanism of the [ONC remote procedure call](https://en.wikipedia.org/wiki/Open_Network_Computing_Remote_Procedure_Call "Open Network Computing Remote Procedure Call") system developed in [SunOS](https://en.wikipedia.org/wiki/SunOS "SunOS"). The second mechanism is based on both client and server trusting a [Kerberos](https://en.wikipedia.org/wiki/Kerberos_(protocol) "Kerberos (protocol)") server.

A third method is limited to local connections, using system calls to ask the kernel what user is on the other end of a local socket. The `xhost` program can be used to add or remove `localuser` and `localgroup` entries with this method.[[1]](https://en.wikipedia.org/wiki/X_Window_authorization#cite_note-1)

## Tunneling

The [SSH](https://en.wikipedia.org/wiki/Secure_Shell "Secure Shell") utility (when invoked with option `-X` or option `ForwardX11`) tunnels X11 traffic from remotely invoked clients to the local server. It does so by setting at the remote site the `DISPLAY` environment variable to point to a local TCP socket opened there by sshd, which then tunnels the X11 communication back to ssh. Sshd then also calls xauth to add at the remote site an MIT-MAGIC-COOKIE-1 string into `.Xauthority` there, which then authorizes X11 clients there to access the ssh user's local X server.

X11 connections between client and server over a network can also be protected using other secure-channel protocols, such as [Kerberos](https://en.wikipedia.org/wiki/Kerberos_(protocol) "Kerberos (protocol)")/[GSSAPI](https://en.wikipedia.org/wiki/GSSAPI "GSSAPI") or [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security "Transport Layer Security"), although such options are now far more rarely used than SSH.

## References

1.  **[^](https://en.wikipedia.org/wiki/X_Window_authorization#cite_ref-1 "Jump up")** ["Server-interpreted Authentication Types "localuser" and "localgroup""](https://cgit.freedesktop.org/xorg/proto/x11proto/tree/specs/SIAddresses/localuser.txt). X.Org Foundation. Retrieved 16 January 2015.

## External links

-   [X security manual page](https://www.x.org/releases/current/doc/man/man7/Xsecurity.7.xhtml) (Xsecurity 7)