---
date: 2023-04-11 23:40
title: Shared clipboard on Linux without keylogging, etc. vulnerabilities
tags:
- clipboard 
---



# Shared clipboard on Linux without keylogging, etc. vulnerabilities?

Asked 8 years, 7 months ago



Viewed 780 times

8

[](https://security.stackexchange.com/posts/65755/timeline)

X has some serious security problems, not the least of which is that any program using a given display can log keystrokes from any other program on that display. (This can be demonstrated easily using xinput.)

Is there any way to mitigate this within a single X display, without losing essential things like shared clipboard support?

e.g. would it be possible for a window manager to intercept all keystrokes, and pass them on only to the program owning the currently selected window?

Also, is there at least any way to prevent programs from sending sequences of keystrokes to each other? As things are by default on almost all Linux distributions, a compromised program running in my account could send 'rm -rf /\n' to a shell prompt running as root.

I know Linux has mandatory access control frameworks aplenty, but for purposes of spying on a user, those are all AFAIK rendered moot by X having insecure design from day one. What steps can be taken to reduce this hazard?

-   [exploit](https://security.stackexchange.com/questions/tagged/exploit "show questions tagged 'exploit'")
-   [keyloggers](https://security.stackexchange.com/questions/tagged/keyloggers "show questions tagged 'keyloggers'")
-   [x11](https://security.stackexchange.com/questions/tagged/x11 "show questions tagged 'x11'")

[Share](https://security.stackexchange.com/q/65755 "Short permalink to this question")

[Improve this question](https://security.stackexchange.com/posts/65755/edit)

Follow

asked Aug 19, 2014 at 20:30

[![DanL4096's user avatar](https://www.gravatar.com/avatar/11e8cb3c62db3dbb7328c3be36dad985?s=64&d=identicon&r=PG&f=1)](https://security.stackexchange.com/users/51744/danl4096)

[DanL4096](https://security.stackexchange.com/users/51744/danl4096)

30711 gold badge22 silver badges99 bronze badges

-   1
    
    I am not sure if the following question can help you but you might want to read it [unix.stackexchange.com/questions/101697/…](http://unix.stackexchange.com/questions/101697/does-this-threat-still-exist-linux-keylogger-without-root-privileges "does this threat still exist linux keylogger without root privileges") 
    
    – [Ulkoma](https://security.stackexchange.com/users/31356/ulkoma "8,793 reputation")
    
     [Aug 19, 2014 at 22:35](https://security.stackexchange.com/questions/65755/shared-clipboard-on-linux-without-keylogging-etc-vulnerabilities#comment106801_65755) 
    
-   2
    
    Hmm. Calling this "authorized behavior" as in the response there is IMO just glossing over the problem, i.e. that X11 blows a gigantic hole in any mandatory access control setup, by letting graphical programs intercept and spoof each other's input. The whole point of MAC is to avoid such things. 
    
    – [DanL4096](https://security.stackexchange.com/users/51744/danl4096 "307 reputation")
    
     [Aug 20, 2014 at 13:02](https://security.stackexchange.com/questions/65755/shared-clipboard-on-linux-without-keylogging-etc-vulnerabilities#comment106894_65755)
    
-   I assume "use Wayland, and ditch X11 apps" is out of the question? 
    
    – [forest](https://security.stackexchange.com/users/106285/forest "65,673 reputation")
    
     [Apr 7, 2016 at 23:14](https://security.stackexchange.com/questions/65755/shared-clipboard-on-linux-without-keylogging-etc-vulnerabilities#comment218068_65755)
    

[Add a comment](https://security.stackexchange.com/questions/65755/shared-clipboard-on-linux-without-keylogging-etc-vulnerabilities# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

---

## 1 Answer


[](https://security.stackexchange.com/posts/116864/timeline)

<mark style="background: #D2B3FFA6;">There are some work being done in this direction. Some links/ideas that could help you:

The OS qubes [1] tries to fix it, using domains and in each isolated domain, you have a dummy X. It supports clipboard.</mark>

Others solutions[2] are being proposed to solve it.. however I don't know how it supports clipboard.

SELinux sandboxes[3] could be used as well. It very similar with OS qubes, but the main difference is that it uses the kernel to enforce isolation. Qubes uses XEN. First time that I tested, I wasn't able to copy and paste, but searching again, looks like it was fixed

References

[1] [https://www.qubes-os.org/doc/copy-paste/](https://www.qubes-os.org/doc/copy-paste/)

[2] [https://wiki.gnome.org/Projects/SandboxedApps](https://wiki.gnome.org/Projects/SandboxedApps)

[3] [https://www.linux.com/learn/tutorials/382226-run-applications-in-secure-sandboxes-with-selinux](https://www.linux.com/learn/tutorials/382226-run-applications-in-secure-sandboxes-with-selinux)

[Share](https://security.stackexchange.com/a/116864 "Short permalink to this answer")

[Improve this answer](https://security.stackexchange.com/posts/116864/edit)

Follow

# Does this threat still exist: Linux keylogger without root privileges
[Does this threat still exist: Linux keylogger without root privileges](https://unix.stackexchange.com/questions/101697/does-this-threat-still-exist-linux-keylogger-without-root-privileges)

Asked 9 years, 4 months ago



Viewed 2k times

4

[](https://unix.stackexchange.com/posts/101697/timeline)

[https://superuser.com/questions/301646/linux-keylogger-without-root-or-sudo-is-it-real](https://superuser.com/questions/301646/linux-keylogger-without-root-or-sudo-is-it-real)

Or it's long gone as most of the new distros implement SELinux by default

-   [security](https://unix.stackexchange.com/questions/tagged/security "show questions tagged 'security'")
-   [malware](https://unix.stackexchange.com/questions/tagged/malware "show questions tagged 'malware'")

[Share](https://unix.stackexchange.com/q/101697 "Short permalink to this question")

[Improve this question](https://unix.stackexchange.com/posts/101697/edit)

Follow

[edited Mar 20, 2017 at 10:18](https://unix.stackexchange.com/posts/101697/revisions "show all edits to this post")

[![Community's user avatar](https://www.gravatar.com/avatar/a007be5a61f6aa8f3e85ae2fc18dd66e?s=64&d=identicon&r=PG)](https://unix.stackexchange.com/users/-1/community)

[Community](https://unix.stackexchange.com/users/-1/community)Bot

1

asked Nov 18, 2013 at 23:07

[![Hinklo's user avatar](https://i.stack.imgur.com/RuFt4.png?s=64&g=1)](https://unix.stackexchange.com/users/48160/hinklo)

[Hinklo](https://unix.stackexchange.com/users/48160/hinklo)

25144 silver badges1111 bronze badges

-   1
    
    This has nothing to do with SELinux—and if you read the answers to that question, they give you easy tests to do on your own to check. But yes it still exists, its how X works. 
    
    – [derobert](https://unix.stackexchange.com/users/977/derobert "106,279 reputation")
    
     [Nov 18, 2013 at 23:25](https://unix.stackexchange.com/questions/101697/does-this-threat-still-exist-linux-keylogger-without-root-privileges#comment155060_101697) 
    

[Add a comment](https://unix.stackexchange.com/questions/101697/does-this-threat-still-exist-linux-keylogger-without-root-privileges# "Use comments to ask for more information or suggest improvements. Avoid answering questions in comments.")

---

## 2 Answers

Sorted by:

                                              Highest score (default)                                                                   Date modified (newest first)                                                                   Date created (oldest first)                              

6

[](https://unix.stackexchange.com/posts/101699/timeline)

I haven't watched the video, so I'm responding to the SU thread rather than the video it references.

If an attacker can run code on your machine as your user, then they can log your key presses.

Well, duh. All the applications you're running have access to your key presses. If you're typing stuff in your web browser, your web browser has access to your key presses.

Ah, you say, but what about logging key presses in another application? As long as the other application is running on the same X server, they can still be logged. X11 doesn't attempt to isolate applications — that's not its job. X11 allows programs to define global shortcuts, which is useful for input methods, to define macros, etc.

If the attacker can run code as your user, he can also read and modify your files, and cause all kinds of other harm.

This is not a threat. It's part of the normal expectations of a working system. If you allow an attacker to run code on your machine, your machine isn't safe anymore. It's like if you open your front door and allow an axe murderer in: if you then get cleaved in two, it's not because your front door is insecure.

SELinux is irrelevant here. SELinux attempts to contain unauthorized behavior, but after the initial exploit (which is not within SELinux's domain), everything is authorized behavior.

The keylogger can only log keys pressed by the infected user. (At least as long as the infected user doesn't type the sudo password.)

[Share](https://unix.stackexchange.com/a/101699 "Short permalink to this answer")

[Improve this answer](https://unix.stackexchange.com/posts/101699/edit)

Follow

answered Nov 18, 2013 at 23:33

[![Gilles 'SO- stop being evil''s user avatar](https://i.stack.imgur.com/cFyP6.jpg?s=64&g=1)](https://unix.stackexchange.com/users/885/gilles-so-stop-being-evil)

[Gilles 'SO- stop being evil'](https://unix.stackexchange.com/users/885/gilles-so-stop-being-evil)

792k190190 gold badges16331633 silver badges21362136 bronze badges

[Add a comment](https://unix.stackexchange.com/questions/101697/does-this-threat-still-exist-linux-keylogger-without-root-privileges# "Use comments to ask for more information or suggest improvements. Avoid comments like “+1” or “thanks”.")

---

2

[](https://unix.stackexchange.com/posts/259409/timeline)

Yes. Following attack can be performed:

1.  A user runs an untrusted application
2.  This application starts to intercept all the key strokes
3.  User runs `sudo command` in their terminal
4.  Password is acquired by the application and an adversary has the full control over the system

[Share](https://unix.stackexchange.com/a/259409 "Short permalink to this answer")

[Improve this answer](https://unix.stackexchange.com/posts/259409/edit)

Follow