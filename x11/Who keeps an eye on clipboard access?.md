---
date: 2023-04-12 10:49
title: Who keeps an eye on clipboard access?
tags:
- clipboard
---



# Who keeps an eye on clipboard access

https://news.ycombinator.com/item?id=30193091



  
  



## CyberShadow

  

Currently writing a clipboard manager[1], I've seen some things as well.

- So far ran into two applications which don't even implement the X11 clipboard specification (ICCCM section 2) correctly - xsel and Emacs (patches submitted).

- By far the worst offense I've seen in clipboard privacy on the Linux desktop is RedHat's virt-manager. It sends your clipboard AND selection content to all virtual machines, even when they are not focused, with no indication that it's happening, and with no GUI option to turn it off. This is at odds with the common practice of running untrusted code in virtual machines.

- X11 being network-transparent makes its protocol fairly malleable, so it's not difficult to bolt some privacy on top. hax11[2] has an option to restrict access to the primary selection for configured applications (though, for truly malicious applications, you may find X11's security model generally lacking).

[1]: [https://news.ycombinator.com/item?id=29808487](https://news.ycombinator.com/item?id=29808487) [2]: [https://github.com/CyberShadow/hax11](https://github.com/CyberShadow/hax11)



## thrwawy283 on Feb 5, 2022

  

I've always wanted an application dedicated to managing the clipboard. It would provide security and even offer interchange conversion that existing apps do not implement. For example: Copying HTML in Windows it has an associated mime type that receiving applications can recognize and choose to convert from.

I've always wanted a clipboard/paste manager - with history - like this, that would allow me to copy, manipulate, and paste or share that data to another application.

I don't know how this would look, but even the ability to pop open my paste manager and look at the copied contents, filter it, or split the text, or (silly stuff) convert text to emoji'fied content, or take a screenshot of that (text to bitmap), and create a new entry within the paste manager. For some of my friends it would be awesome to have a hex editor in there.

Perhaps there are 2 focuses: Isolating what is copied from applications until the user chooses to expose it, and the history function. Being able to send it to another app for manipulation and pulling it back is a 2nd responsibility. It would be /suave/ to have this all in the same GUI brought up by the compositor, though.



## indentit on Feb 5, 2022

  

For Windows, there is "ClipboardFusion" by Binary Fortress, which acts as a scriptable clipboard manager, and could tick most of your boxes.

I'm not sure how possible it would be on Windows for a third party app to perform isolation - maybe if it registers a hook for when clipboard content changes which is somehow guaranteed to run first before other applications and any hooks they may have, it could swallow the event and clear the clipboard so other processes can't see it, and then do something similar with paste events maybe?



## filoleg on Feb 5, 2022

  

While not scriptable and not super advanced, Windows 10 and Windows 11 have a native visual clipboard manager with history and some other basic features. And it works quite well.

Try pressing Win+V, and the OS will give you a prompt to enable clipboard manager in settings. Once you enabled it (has to be done once), next time you press Win+V, a panel will slide out from the right side, and you can see the last N entries in your clipboard (which could include text, images, etc.). You can pick anything from there and paste it, and you can also pin any item there so that it won’t be overwritten when the clipboard reaches the limit and starts writing over.



## 1vuio0pswjnm7 on Feb 8, 2022

  

[https://f-droid.org/en/packages/org.dslul.openboard.inputmet...](https://f-droid.org/en/packages/org.dslul.openboard.inputmethod.latin/)

This replacement keyboard for Android saves clips and allows the user to select and paste them into any application. Most importantly, it allows the user to delete the contents of the clipboard. It also allows the user to "pin" certain clips to be retained for future pasting. Clip storage.

If one needs a further reason to replace the keyboard, consider that the default keyboard on Google phones, Gboard, tries to phone home to Google. A keyboard that expects internet access. Incredible.



## y4mi on Feb 5, 2022

  

That does sound great.

While it's not what you're looking for, a lot of people also don't know that windows has a built-in copy paste history if you use windows+v instead of ctrl-v. That's quiet handy too, but a fast cry from what you've proposed there



## 6equj5 on Feb 5, 2022

  

>I don't know how this would look

Probably something like KDE's clipboard? ; p

[https://i.imgur.com/zXTLzoO.png](https://i.imgur.com/zXTLzoO.png)



## Rygian on Feb 5, 2022

  

> RedHat's virt-manager. It sends your clipboard AND selection content to all virtual machines

Do you know what's the CVE for that one?



## CyberShadow on Feb 5, 2022

  

Let's see where this goes:

[https://github.com/virt-manager/virt-manager/issues/358](https://github.com/virt-manager/virt-manager/issues/358)

Edit: found this, so maybe Red Hat considers this a feature which is working by design: [https://github.com/virt-manager/virt-manager/pull/166](https://github.com/virt-manager/virt-manager/pull/166)



## formerly_proven on Feb 5, 2022

  

Qubes distinguishes local and global clipboards for exactly this reason.



## alkonaut prev

  

That my web browser process can read my windows clipboard is fine, what’s important is that it prevents the web pages it has loaded from doing the same.

I can’t trust all web pages but trusting all my desktop programs is a much easier pill to swallow.

Windows could certainly tighten the security by optionally only allowing foreground/focused apps to access, or notifying the user when apps read the clipboard without being in focus or sent any input first.



## dethos on Feb 5, 2022

  

> Windows could certainly tighten the security by optionally only allowing foreground/focused apps to access, or notifying the user when apps read the clipboard without being in focus or sent any input first.

Author here, yes, this is indeed the point of the article. Desktop operating systems could/should have implemented such a feature a long time ago, it is clearly a security "blind spot" at the moment.



## IshKebab on Feb 5, 2022

  

Not really though. It makes sense for mobile OSes because they have proper isolation of apps, but desktop OSes don't have that. Apps can pretty much do anything.

What's the point of copy notification if apps can just read all your emails and steal all your files anyway.

Android and iOS both have this feature already. That's the best we can do for now.



## dethos on Feb 7, 2022

  

> What's the point of copy notification if apps can just read all your emails and steal all your files anyway.

We are discussing "paste" notifications. Knowing that an "app" is constantly pasting(fetching) the contents of the clipboard seems suspicious behavior and I as a user would like to be made aware of it.



## nitrogen on Feb 5, 2022

  

Desktop OSes all have some form of optional sandboxing now, so it makes sense to keep chipping away at the edges of the attack surface.



## IshKebab on Feb 6, 2022

  

Do people use them though? Both Mac and Windows have pretty much tied app sandboxing to 30% app store fees, which means the proposition is "rewrite your app to use incomplete APIs and then you can put it in our store where we'll take a big cut!"

Not very appealing. It only works on mobile because there wasn't an existing industry used to 2% fees and unrestricted APIs.

Unsurprisingly Microsoft has abandoned app store fees and mandatory sandboxing so their app store has any chance of being used. Not sure about Apple.



## jhugo on Feb 6, 2022

  

Can't apps use sandboxing with Apple's notary feature without needing to distro on the app store? and the incentive is the lack of a scary warning?



## IshKebab on Feb 6, 2022

  

You can get rid of the scary warning without having to sandbox though.



## kibwen on Feb 5, 2022

  

_> That my web browser process can read my windows clipboard is fine, what’s important is that it prevents the web pages it has loaded from doing the same._

To be clear for others who may be reading this, this statement isn't a hypothetical, web browsers _do_ prevent web pages from reading the clipboard.



## badsectoracula on Feb 5, 2022

  

> Windows could certainly tighten the security by optionally only allowing foreground/focused apps to access

That would break clipboard managers which are very useful tools to have in various occasions. E.g. in a previous job i worked at, some audio designers used a clipboard manager to quickly fill settings in various tool fields.

Fun fact: most clipboard managers tend to use global shortcut keys for cycling through the recent N copies (or for pasting specifically tagged content, e.g. copy something and mark it to be used for pasting after Shift+Alt+1 is hit, same for Shift+Alt+2 for something else, etc), which is another thing that you'd hear security concerns for :-P



## nsgi on Feb 5, 2022

  

Sure, but that should really be a permission you have to grant explicitly.

For global keyboard shortcuts an app should make commands available and it should be up to the user to tell their OS which shortcuts they want to give an app. Not just for security, but also for customisability and to avoid conflicts



## badsectoracula on Feb 5, 2022

  

> Sure, but that should really be a permission you have to grant explicitly.

That could work but still feels dodgy since if you can grant that permission to a clipboard manager, you can also grant it to something like a TikTok client or an Adobe DRM reader or whatever else program you want to use (ie. the reason you are using your computer in the first place) but also might want to snoop for your data.

IMO a better solution that wouldn't break existing clipboard usage is to have a second "secure" clipboard that you can lock as tight as you want with explicit permissions for reading it, notifications for writing to it and whatever else you want. Then password managers would copy data to that and text editors and browsers can have a "Copy Sensitive Data" (or something better worded) in addition to "Copy" that will place data there.

Sure some people will still use Copy but some people will also stick post-it notes with their passwords on their monitors and give thousands of dollars to Nigerian princes - you shouldn't to punish everyone for the ignorance of a few.

> For global keyboard shortcuts an app should make commands available and it should be up to the user to tell their OS which shortcuts they want to give an app. Not just for security, but also for customisability and to avoid conflicts

This only works if all applications are using the same framework and toolkit and target the same OS. Meanwhile notice how even when you limit yourself to applications targeting Windows alone there are tons of different frameworks - let alone cross platform applications that tend to use a lowest common denominator approach.

IMO if you want to get more secure functionality, you need to do it in a way that works alongside existing practice - hence containers and sandboxing for untrusted applications working alongside trusted applications that can do what they want. Unless you start everything from scratch on a brand new computing approach (like mobiles did and even then it barely works and despite people treating mobiles almost like consumables), trying to force a top-down approach to security is doomed to fail.



## laumars on Feb 5, 2022

  

> IMO a better solution that wouldn't break existing clipboard usage is to have a second "secure" clipboard that you can lock as tight as you want with explicit permissions for reading it, notifications for writing to it and whatever else you want.

If we are going to break standard APIs then why not do away with the clipboard for secrets entirely and have a global secrets store that applications can pull credentials from directly and securely instead of passing secrets via a clear text protocol.

This is how Linux and macOS work (albeit I wish 3rd party stores could natively integrate with them). This is how cloud computing works too (eg AWS Secrets Manager). Using a clipboard for copying secrets is completely the wrong approach regardless of how much you over-engineer your clipboard manager.



## badsectoracula on Feb 5, 2022

  

Note that what i refer to is not about breaking any standard API but adding a secondary one that is explicitly about secret/sensitive stuff. Current applications will of course be still using the clipboard but the situation wont be any worse than it currently is and new applications as well as applications currently active development can be made to use the new one, so things can be better.

IMO this is a much better approach and way more likely to be adopted than anything that breaks existing applications and workflows.

Also while people use passwords as an example, this is just an example. What you are copying can be some secret government-issued number (that you can't replace with anything else), some sensitive photo or anything else that you want to be exact at both ends of the operation.

If the source of the data to be placed in the secure clipboard (for lack of a better name) is some encrypted storage (like a local secrets manager) or a plain text file is irrelevant here.



## laumars on Feb 5, 2022

  

> Note that what i refer to is not about breaking any standard API but adding a secondary one that is explicitly about secret/sensitive stuff.

But it still requires developers to use a new API. So why get them to used a half arsed security enclave when you can design a proper secrets store instead?

> If the source of the data to be placed in the secure clipboard (for lack of a better name) is some encrypted storage (like a local secrets manager) or a plain text file is irrelevant here.

It’s very relevant because:

* clipboards are temporary which means you need to copy data into them. This creates an extra step for users to follow as opposed to applications querying the secrets store directly

* clipboards don’t have RBAC unlike password stores. Which means any application with permissions to read from the clipboard can read any secret that any other application had written to it. Your solution of having an additional permissions set where you chose which applications have that permission is solved with secrets store where each application only has permission to the secrets it creates. Thus zero risk of leakage

* secrets managers are encrypted, clipboard isn’t

* secrets managers can store multiple secrets, a clipboard cannot. Thus you then still need to have a secrets manager to copy your password into the clipboard anyway. So why not do away with that middleman entirely?

I get the incentive of wanting to “fix” the clipboard but if you’re going to introduce a new API for secrets then you might as well do it properly from the outset.



## badsectoracula on Feb 5, 2022

  

> But it still requires developers to use a new API. So why get them to used a half arsed security enclave when you can design a proper secrets store instead?

Because the entire point of this is _NOT_ to store something but to transfer something from one application to another without both applications explicitly knowing about each other (so they can't just communicate in a P2P fashion). The rest of your message is about storing data, which is not what the clipboard use is all about. Which is why i wrote it is irrelevant - you refer to something else.

E.g.

> clipboards are temporary which means you need to copy data into them. This creates an extra step for users to follow as opposed to applications querying the secrets store directly

The API i mention is not about permanently storing data, it is explicitly about having a secure _temporary_ store with the same(ish) UX as a clipboard but for secure data.

Applications can also use a secure store for that and the data that is being copied to the secure clipboard could also come from an application that stores its own data to it from there, but again this is a separate issue, this is orthogonal to what a discuss here.

> secrets managers are encrypted, clipboard isn’t

This is irrelevant as the data isn't stored elsewhere outside the memory of the process that would manage the secure clipboard's functionality (and of course you could encrypt that if you'd like but IMO that'd be pointless since it'd need to be decrypted at some point anyway and these would be meant for temporary storage - regardless of you do or not encrypt it though it'd be again orthogonal to the rest of what i describe).

> secrets managers can store multiple secrets, a clipboard cannot. Thus you then still need to have a secrets manager to copy your password into the clipboard anyway. So why not do away with that middleman entirely?

Because this isn't about storage.

> if you’re going to introduce a new API for secrets then you might as well do it properly from the outset.

That'd be a "proper" solution for a different problem, hence barely a solution at all.



## laumars on Feb 5, 2022

  

I get that you’re not storing something long term, but you are still storing it even if it is short term and thus presenting a risk. And you are still sharing secrets between applications thus presenting a risk. And all your suggestion is doing is offering a kludge around a solution you’ve already said is crappy, rather than switching to a robust and battle tested solution that literally every other platform already supports.

If you want to pass secrets securely between applications then you use a secrets manager. It’s how Linux and macOS work. It’s how secure systems in the cloud works. And thus it makes complete sense for Windows to follow suit. We aren’t talking about some theoretical concept here. It is absolutely how passing secrets between applications _should_ be done.

If you have scepticism about this approach then you need to read up in this topic rather than pushing for a half baked kludge around something you’ve already acknowledged isn’t fit for security.

Sorry if this sounds blunt but this is a solved problem you’re trying to re-engineer and your solution is worse than the industry standard in a number of ways which I’ve already highlighted. And ironically it isn’t even just worse for security but also worse for usability well (you’re creating administrative overhead with the user approving safe applications. This wouldn’t be needed with a secrets manager since each application only has visibility of its own enclave). And if you remember back to your first post, usability was the entire reason you designed this solution in the first place.



## badsectoracula on Feb 5, 2022

  

I do not know why you mention Windows, i never referred to any specific platform (you mention Linux, Linux does not actually have what you describe because Linux is a kernel and what you describe is something that would live on the userland - the userland heavily relies on what software is installed and many setups, like mine, do not have such a thing).

What i refer is how to provide a more secure approach to to THE SAME THING that people are already doing with clipboard copy/paste without breaking the versatility of the clipboard, workflows or even existing applications and can be supported with minimal changes in existing applications and pretty much zero re-learning by users. It is about being able to copy/paste stuff securely _anything_ that can already be copy/pasted between applications and not just passwords or other stuff you'd need to store permanently. It can even be made to work in a backwards compatible way - with some additional though minimal effort from the user - for applications that do not support the functionality.

What you refer to is having a different workflow, have applications add explicit support for the specific data mentioned and be accessed in a different way and up to the last reply you were referring to permanent storage.

You ask people to change how they use software, i ask them to use a different menu option for sensitive stuff. What exactly do you think is the more likely to happen?

(well, assuming anything would happen, in practice most likely nothing will change)

I do not have skepticism about what you refer to, i do not even think what you refer to is wrong for the stuff it is intended for, i am just not referring to the same stuff you do.



## laumars on Feb 5, 2022

  

> I do not know why you mention Windows, i never referred to any specific platform

Because this is already a solved problem on all popular platforms aside from Windows.

Plus others earlier in this discussion singled Windows out too (likely for the same reason I cited above).

> Linux does not actually have what you describe because Linux is a kernel and what you describe is something that would live on the userland - the userland heavily relies on what software is installed

It’s pretty normal for people to talk about Linux as a computing platform. You know this yourself so making the “it’s just a kernel” argument is next level pedantry.

> and many setups, like mine, do not have such a thing

I’d put money on you having one installed and not even realising it (eg gnome-keyring, which is a dependency for many desktop applications even without having gnome installed)

> What i refer is how to provide a more secure approach to to THE SAME THING that people are already doing with clipboard copy/paste without breaking the versatility of the clipboard

I understood what your approach was. The issue isn’t that I don’t understand it. The issue is that you are unwilling to accept the last 20 years of development in this field.

I mean have you never even used password management in Firefox / Chrome? Avoiding the need of clipboard for sharing secrets is a security and usability _feature_. Your solution is terrible in comparison and this is precisely why browsers have integrated password stores.

> What you refer to is having a different workflow, have applications add explicit support for the specific data mentioned and be accessed in a different way and up to the last reply you were referring to permanent storage.

Your solution was to add a new API. You stated that explicitly. You then said users should authorise which applications have authority to use that API, that’s a new workflow too. The standard approach (ie that way the industry works, this isn’t something I’ve just made up) allows applications to communicate directly to your secrets store. This doesn’t add a new workflow, it removes an existing one entirely.

Plus you still need to copy your passwords from somewhere to use your API so why bother with it in the first place? It’s literally just adding in a process for the sake of it. Except that process is insecure, a usability nightmare and contradicts decades of established solutions in this precise domain.

The fact that you’re refusing to even look into this concept is astonishing tbh

> am just not referring to the same stuff you do.

indeed, your stubbornly clinging onto a terrible idea and rejecting decades of industry best practices.

The idea you’re proposing has already been superseded by years of research and development towards much better solutions. And you can install them right now if you wanted. I’m not making this shit up.



## badsectoracula on Feb 6, 2022

  

> It’s pretty normal for people to talk about Linux as a computing platform. You know this yourself so making the “it’s just a kernel” argument is next level pedantry.

In this case the "pedantry" makes sense because...

> I’d put money on you having one installed and not even realising it (eg gnome-keyring, which is a dependency for many desktop applications even without having gnome installed)

...i do not have such a thing. I use plain Xorg with Window Maker and i have removed anything i deemed unnecessary from my PC.

> The issue isn’t that I don’t understand it.

No you do not understand it because you wrote:

> I mean have you never even used password management in Firefox / Chrome? Avoiding the need of clipboard for sharing secrets is a security and usability feature. Your solution is terrible in comparison and this is precisely why browsers have integrated password stores.

My solution is to *NOT* use the clipboard *exactly* because it is not secure for the reason mentioned in the *linked article*! Which is why i mention a *second* API to exist *alongside* the current one. The only reason i use the term "clipboard" is because from a usability perspective (for both the users and, for the most part, the programmers) the use will be the same so it is the closest to understand.

> Your solution was to add a new API. You stated that explicitly.

Yes i did. I explicitly wrote that new applications can use it, existing applications can be made to support it and with some minimal effort from the user even existing applications that do not support it can be made to do it.

> You then said users should authorise which applications have authority to use that API, that’s a new workflow too.

No i never mentioned that, in fact i never even mentioned how that part would work. Here, this is what i originally wrote: "that you can lock as tight as you want with explicit permissions for reading it, notifications for writing to it and whatever else you want."

> The standard approach (ie that way the industry works, this isn’t something I’ve just made up) allows applications to communicate directly to your secrets store.

Because...

> Plus you still need to copy your passwords from somewhere

...i do not...

> to use your API so why bother with it in the first place?

...refer to just passwords.

This is about *ANYTHING* that can go on the clipboard that can be sensitive. This is about stuff that is temporary. *_THIS IS NOT ABOUT PERMANENT STORAGE*_.

I already wrote that stuff, gave examples and yet you claim that i am the one who is "stubbornly clinging" to my idea.

How about following the HN commenting guideline about "Please respond to the strongest plausible interpretation of what someone says, not a weaker one that's easier to criticize. Assume good faith" and using your imagination to try and understand what i refer to instead of calling me stubborn?

If you do that you may realize that what i describe isn't even incompatible with secure stores and can be functionality that is provided by them.



## laumars on Feb 6, 2022

  

> ..i do not have such a thing. I use plain Xorg with Window Maker and i have removed anything i deemed unnecessary from my PC.

So you don’t use a web browser then? I guess your posts here must appear by magic.

> This is about _ANYTHING_ that can go on the clipboard that can be sensitive.

Hence why I’ve repeatedly used the term “secrets” and not “passwords”.

The password manager example was just an illustration because this entire concept seemed weirdly alien to you. But secrets stores are not just for passwords and nor do they need to hold secrets for long durations either.

Again, I implore you to actually do some reading on this topic before making daft assumptions. Look into Hashicorp Vault for example. Now I’m not suggesting everyone should manage their own Vault instance; but if you’re going to create a new API anyway then you might as well abstract that around similar tooling which is managed by the OS rather than configured by the user. I mean why reinvent the wheel (and badly too) when this approach is proven?

> How about following the HN commenting guideline about "Please respond to the strongest plausible interpretation of what someone says, not a weaker one that's easier to criticize.

You mean like your pedantry about Linux being a kernel when you knew full well the context that term was used in?

Your ego here is getting in the way of you learning some new technology (well, I say “new” but it really isn’t). This is already the direction the industry has already moved.



## badsectoracula on Feb 6, 2022

  

> So you don’t use a web browser then? I guess your posts here must appear by magic.

Of course i use a web browser. What i do not have is a generic store like the one you describe - my web browser does not provide an API for other applications in my desktop to access whatever is stored in it.

> Hence why I’ve repeatedly used the term “secrets” and not “passwords”.

And yet you used them explicitly for storing passwords.

> The password manager example was just an illustration because this entire concept seemed weirdly alien to you.

Clearly in your message here:

[https://news.ycombinator.com/item?id=30220104](https://news.ycombinator.com/item?id=30220104)

...you only refer to storing credentials (ie. passwords) and here:

[https://news.ycombinator.com/item?id=30220390](https://news.ycombinator.com/item?id=30220390)

you explicitly refer to "password stores".

Meanwhile from the very beginning, like in here:

[https://news.ycombinator.com/item?id=30220062](https://news.ycombinator.com/item?id=30220062)

i refer to "copying data" and even if it is in the context of passwords, i make explicit in the very first reply i made to you:

[https://news.ycombinator.com/item?id=30220227](https://news.ycombinator.com/item?id=30220227)

...that i do not refer to just passwords but anything that can be copied to the clipboard and passwords was just an example.

That you think that "concept seemed weirdly alien" to me can only be seen as an indication that you do not read what i write.

> You mean like your pedantry about Linux being a kernel when you knew full well the context that term was used in?

As i already wrote previously, that part was to make it clear that this isn't something you can depend to be there "on Linux" because not everyone's setup has one - like mine.

> Your ego here is getting in the way

Does it really? From my perspective you entered the discussion with a polemic tone and when i tried to explain my position you doubled down, ignoring what i wrote and even started writing that i do not even know what i have installed on my own computer.



## laumars on Feb 6, 2022

  

> Of course i use a web browser. What i do not have is a generic store like the one you describe - my web browser does not provide an API for other applications in my desktop to access whatever is stored in it.

Well that actually depends on the browser and OS. You’ll find some browsers actually use an existing system API for their password store.

But anyway, I wasn’t suggesting the final solution be a password manager. I just exampled that because you seemed oblivious to the benefits a secrets manager would have and password managers share an overlapping domain.

> > Hence why I’ve repeatedly used the term “secrets” and not “passwords”.

> And yet you used them explicitly for storing passwords.

Not just passwords. Any secrets. I’ve repeatedly said secrets and not passwords. Those secrets could be a password but they could also be private keys / certs and even just sensitive config. I’ve literally used secrets managers for these things too. And it is extremely easy to store any kind of secret because at the most basic level they’re just key value stores with a TTL and encrypted storage engine and secure API for 3rd party applications to query. Literally the end solution one would come to if they took your specs and fleshed them out to a secure and robust natural conclusion.

So yeah, you can put in any data you want.

You’d also know this if you spent even just 5 minutes researching this like I’ve repeatedly suggested.

> you explicitly refer to "password stores".

Yes, as an example of utility because you couldn’t grasp the concept and was too lazy to do any research.

Now that you’ve wrapped your head around the basics go look up Hashicorp Vault and you’ll get a sense for how a secrets manager is much more than just a password manager.

Now think about how that solution could be utilised to solve the same problem you’re identifying. And you’ll finally understand why I keep harping on about it.

> i refer to "copying data" and even if it is in the context of passwords, i make explicit in the very first reply i made to you:

Again, secrets stores aren’t just for passwords.

> That you think that "concept seemed weirdly alien" to me can only be seen as an indication that you do not read what i write.

I’ve been listening to you. But frankly communicating with you is like trying to draw blood from a stone because you keep insisting on having an opinion on a domain you clearly know Jack shit about and then refusing to spend even the smallest amount of time looking into any of the technologies being recommended.

The issue here is 100% you, not me. I build and use these technologies for a living and you clearly haven’t the foggiest on this topic.

> As i already wrote previously, that part was to make it clear that this isn't something you can depend to be there "on Linux" because not everyone's setup has one - like mine.

You could also not install support for the hypothetical API you’re imagining too. So your point here is moot.

This is why I was using Windows as the baseline for the conversation. It saves us from these stupid meta hypotheticals about “what if I chose not to install this thing I just moaned about wanted to install”…

> when i tried to explain my position you doubled down

You tried to tell me that 20 years of security technology didn’t exist. So yes, I did double down on the fact that you were wrong because you were and still are.

Also I love the hypocrisy of the statement that I’m “doubling down” and you’re just “explaining your position”. The way I saw it, you posted an idea, I came up with a better solution and you then doubled down that your crappy alternative was better for reasons and then posted a list of misunderstandings.

You’ve also pulled this hypocritical shit a few times too, accusing me of doing the very things youre already doing.

> ignoring what i wrote

I addressed your points literally. If you’d spent even 5 minutes researching this field you’d realise that.

And I’ve lost count of the number of times I’ve asked you to read up on this topic. It’s not an unreasonable request on my part.

> and even started writing that i do not even know what i have installed on my own computer.

Given you don’t even know what this technology is, I find it hard to believe that you’d know if it was installed or not.

Plus as a Linux developer and sysadmin myself, I know full well that it’s impossible to audit every single package that goes into a desktop installation (a minimal headless image is possible but not a multi-purpose desktop installation). And I’m the kind of person who has spent decades compiling frameworks, drivers and desktop software from source so if I can’t memorise every single dependency then there’s no hope for you. The fact you think it is possible honestly says more about where you sit on the Dunning-Kruger scale than it does about me.

Honestly, drop the ego and look this stuff up. It’s pretty cool technology. It wouldn’t suit your needs perfectly in its “off the shelf” state but it is the backend architecture that would best realise your vision. But until you do any research of your own you’ll find that we will just keep going round in circles.



## badsectoracula on Feb 7, 2022

  

> Well that actually depends on the browser and OS. You’ll find some browsers actually use an existing system API for their password store.

Right, but mine doesn't and you referred to my browser.

> But anyway, I wasn’t suggesting the final solution be a password manager.

In both your original reply to me and the follow up what you wrote about was storing passwords in a secret store to avoid copying them from somewhere else as that would have them go through via "a clear text protocol".

This was a complete misunderstanding of what i wrote about.

> I just exampled that because you seemed oblivious to the benefits a secrets manager would have and password managers share an overlapping domain.

I am not oblivious to what benefits secrets managers would have, they are only tangentially (if at all) relevant to what i discussed about.

> Not just passwords. Any secrets. I’ve repeatedly said secrets and not passwords.

You kept referring to passwords and how storing the passwords in a secret store and have it be accessible by the application would be better than having passwords be passed via a secure clipboard - which is completely and absolutely missing the entire point of what i was discussing about.

> So yeah, you can put in any data you want. You’d also know this if you spent even just 5 minutes researching this like I’ve repeatedly suggested.

Yes, of course i know that, if you weren't so sure about your preconceived notions about the other person you are making a bad attempt at discussing with, you'd actually have realized that.

If you also made an attempt to understand what the other person writes about you'd also realize that i also refer to having applications pass data in an encrypted way and not just passwords, despite you claiming that i did not understand that and this is why you "exampled that because i seemed oblivious to the benefits a secrets manager".

The other interpretation however is that you refer to something different that just sound similar because they both have to do with using passwords.

> I’ve been listening to you.

It doesn't feel that way at all.

> But frankly communicating with you is like trying to draw blood from a stone

But that is how i feel.

> The issue here is 100% you, not me.

From my perspective you are the one at fault for not even trying to understand what i was referring to. Thing is i know what your issue really is, you made some initial assumption about what i know (that you keep repeating) and what i was referring to and try to filter everything you read from me through that assumption. I mean you even spelled out here:

> Given you don’t even know what this technology is

I know what you'd like applications to do and where to store their secret information and how to work - as you wrote this isn't new technology and in fact personally i first used it in the early 2000s in KDE with KWallet and later with GNOME 2. But i never felt like bringing up (my) credentials or bringing up any experience i had with these because, from the very beginning, i knew that these have nothing to do with what i referred to.

When i wrote that these are irrelevant i didn't write it because i didn't knew what they are, i wrote it because they actually _are_ irrelevant to _the original idea i described_. Yes, if an application wants to store and then use and retrieve secrets (be it passwords, documents, photos or whatever), either stored by it or by another trusted application, a secrets manager would be preferable.

But i wasn't referring to that use case. What i referred to didn't even had the same way of interaction with the applications. What i wrote about was on improving the security for clipboard-like workflows _specifically_, to avoid the issues the clipboard has right now _without breaking any existing applications_ (e.g. clipboard managers - remember that other comments in the thread were about how to make "clipboard snooping" impossible, which would certainly stop those from working).

All that stuff should be obvious and i do not see why i'd have to spell them out when i keep writing that secure stores are only tangential to the examples i give and not what i write about.

And honestly...

> Honestly, drop the ego

...if after being confronted with someone claiming that what you keep on writing about is irrelevant to what they were referring to has you thinking as the only possibility for that is that _they_ lack (your) knowledge and not _you_ who might not trying to see things from a broader perspective, then i'm not sure who'd be the one with the ego issues.



## laumars on Feb 8, 2022

  

> In both your original reply to me and the follow up what you wrote about was storing passwords in a secret store to avoid copying them from somewhere else as that would have them go through via "a clear text protocol". This was a complete > misunderstanding of what i wrote about.

You keep saying “passwords” then saying you’re not taking about “passwords” and then saying I misunderstood you because I mentioned passwords yet I never actually mentioned passwords. Go back and read my original reply:

[https://news.ycombinator.com/item?id=30220104](https://news.ycombinator.com/item?id=30220104)

Is it possible that you’re conflating “secrets” with “passwords”? Because they’re not the same. The latter is a subgroup of the former

> I am not oblivious to what benefits secrets managers would have, they are only tangentially (if at all) relevant to what i discussed about.

I know that’s not what you discussed. My point was what you discussed is a crappy solution that has already been superseded with secrets stores to solve this over arching problem space. Thus your solution should incorporate secrets stores instead of reinventing them but badly.

> you made some initial assumption about what i know

With the greatest of respect we’ve had the following issues:

1. You’ve conflated “secrets” and “passwords”. Secrets is a term in infosec that refers to more than just passwords. It’s _the_ standard term for discussing sensitive content in this context. Which is why I’ve repeatedly used that term. And you’ve misunderstood it’s meaning completely and even made accusations that I’ve not understood that you’re describing more than just passwords because you’ve misunderstood the term “secrets”.

2. You didn’t realise that secrets stores have a TTL. That alone literally solves 80% of the problem you’ve got and does so right out of the box.

3. You conflated password managers and secret stores (ok, that one is partly my fault too because I used password managers as an example to loosely describe how a secrets store might work. But the fact I had to make that explanation is telling).

4. You forgot that browsers often use system APIs for password storage. I’m giving you the benefit of the doubt that you did know this originally (given your comment about kwallet) but you cannot deny that you did post earlier that you wanted an external API (with regards to browsers), forgetting that many do actually already do this.

5. You also claim that you know every single library that is installed on you desktop. This is at best a huge exaggeration. But realistically it’s either completely delusional or an out right lie.

I could go on. But suffice to say you’ve not exactly redeemed yourself as an authority on this topic despite being confident that everyone else is wrong.

> What i wrote about was on improving the security for clipboard-like workflows specifically, to avoid the issues the clipboard has right now without breaking any existing applications

I got that. The point you keep missing is that adding a new API breaks clipboard-like workflows anyway. So if you’re already breaking that then why not build your new API on top of a secrets store, give that data a short TTL and leverage already proven technology. The entire process can be streamlined from a user perspective so it even looks like a clipboard. Except it is secure.

> All that stuff should be obvious and i do not see why i'd have to spell them out when

What you’ve posted is obvious. The issue is you don’t understand how secrets managers work so defaulting to the position that they are clearly not suited.

Anyway, I can’t see this argument being resolved. You’re not going to research the topic and I’m not going to concede that you’re not just reinventing the wheel but badly. So maybe we just give up here?



## badsectoracula on Feb 10, 2022

  

> You keep saying “passwords” then saying you’re not taking about “passwords” and then saying I misunderstood you because I mentioned passwords yet I never actually mentioned passwords.

You also keep using the word "password" while claiming you are not using the word "password" like you did right now.

And of course what i just wrote, just like what you just wrote, is not an argument at all since this isn't about the existence of the word "password" in the text that was typed, but of what you are claiming.

From the very beginning i only used passwords as an example of something that can be placed in the secret clipboard. In the first reply to you i already made that clear - which is also something i pointed out in another reply later.

> Is it possible that you’re conflating “secrets” with “passwords”? Because they’re not the same. The latter is a subgroup of the former

Please read what i write, i do not conflate the two and this should have been obvious from the first reply i made to you where i write that the source for the data to be placed in the secure clipboard can come from a secrets manager.

> My point was what you discussed is a crappy solution that has already been superseded with secrets stores to solve this over arching problem space. Thus your solution should incorporate secrets stores instead of reinventing them but badly.

And this is why you do not understand what i wrote. Secret stores do not solve the same problems that the secret clipboard i described does.

> 1. You’ve conflated “secrets” and “passwords”.

As i already wrote, i did not, you just made that assumption. Even if i had no idea what they'd be it'd be a very stupid mistake to make since the name is practically self-describing in context.

> You didn’t realise that secrets stores have a TTL. That alone literally solves 80% of the problem you’ve got and does so right out of the box.

This has nothing to do with what i describe, aside perhaps from using it as a way for a secrets manager to implement the secure clipboard API - like i already mentioned several replies ago in [https://news.ycombinator.com/item?id=30220474](https://news.ycombinator.com/item?id=30220474)

> You conflated password managers and secret stores

I did not, i only brought up password managers as an example which i quickly made clear that they were only just one source for the data to be placed in the secure clipboard in the first reply i made to you.

> But the fact I had to make that explanation is telling

It is only telling that you do not bother to read and understand what the others are writing.

> You forgot that browsers often use system APIs for password storage.

...no? I never forgot anything like that, this is again an assumption you made. The only time i referred to a browser was for my browser and i already wrote that i only did to that, not to any potential browser that could exist. I have used other browsers in the past, like Safari, that does use system APIs for password storage (or at least that is what i assumed Safari was doing, i never really digged down on that).

> benefit of the doubt

Instead of trying to doubt me, you may actually want to try and understand what i am writing.

> but you cannot deny that you did post earlier that you wanted an external API (with regards to browsers)

What i describe is a different API because the functionality i describe is not the same as what a secrets store would provide. This is what i am trying to explain from the beginning.

> 5. You also claim that you know every single library that is installed on you desktop.

I never claimed such a thing, this is yet another assumption you make.

The only thing i claimed was that my setup does not have a secrets manager that other applications can use, like you originally described "Linux" having. Which is why i wrote what you took as pedantry, that Linux isn't just a single setup and setups without a secrets manager do exist - and brought up mine.

While i do not know every single library that is on my system, i do have a decent idea on what is in it there since i try to pay attention to what i install.

So unless you consider having libsqlite3 and libgcrypt available as shared objects passes as a secrets store API that applications can use (they can always pester me for a password/something every time they need to access the data), i am certain i do not have one.

> suffice to say you’ve not exactly redeemed yourself as an authority on this topic

I never claimed to be an authority on this topic either. In fact if you were to ask me, i'd be the first one to say i do not really know much about security (and it isn't really a topic i am interested), which is why i mainly focused on the functionality from a UX perspective and how people would use it rather than how the underlying system would be implemented.

> The point you keep missing is that adding a new API breaks clipboard-like workflows anyway.

It doesn't, the existing clipboard API would still be there. From the very first comment i brought that up, i mentioned that the secret clipboard would exist alongside the existing one as to not break anything that currently works.

It is even right after the bit you quoted in your first reply to me.

Here: "IMO a better solution that wouldn't break existing clipboard usage is to have a _second "secure" clipboard that you can lock as tight as you want_. Then password managers would copy data to that and text editors and browsers can have a "Copy Sensitive Data" (or something better worded) _in addition to "Copy"_ that will place data there.

In fact...

> why not build your new API on top of a secrets store, give that data a short TTL and leverage already proven technology.

...I even mentioned in another reply that what you describe could implement the API i describe.

> The entire process can be streamlined from a user perspective so it even looks like a clipboard.

Yes, like i already wrote in my original message: making something that, for lack of a better word, is essentially a "secure" separate clipboard, something that looks and behaves like a clipboard. The never specified how it would be implemented, only how it'd behave. I only wrote "lock as tight as you want with explicit permissions for reading it, notifications for writing to it and whatever else you want". I left the implementation up in the air.

Would it be implemented by a secrets store or some other service that communicates with a secrets store and sets up some low TTL or outright destroys the data after it has been pasted? Doesn't matter, it wasn't what i was describing, that would be some implementation detail.

Which is why i kept on and on and on and on repeating over and over that what i describe is not about secrets stores and is irrelevant, aside from perhaps having one implement it.

> The issue is you don’t understand how secrets managers work so defaulting to the position that they are clearly not suited.

I understand what they are used for and what they are used for is not for the functionality that i describe (there might be one that does have something similar that i do not know about but as i do not know it, i only refer to the concept in general), otherwise i wouldn't bring up the idea in the first place.

> Anyway, I can’t see this argument being resolved. You’re not going to research the topic and I’m not going to concede that you’re not just reinventing the wheel but badly. So maybe we just give up here?

If you are so hellbent in avoiding to understand what i write, sure, it doesn't sound like there is a reason to continue.



## pmontra on Feb 5, 2022

  

Of course if an app asks for some very dangerous permissions somebody will grant them to it even if they don't belong to the app's advertised functionality.

Naming and description could help a little: Full access to copy and paste (maybe somebody doesn't know what clipboard means), it allows this app to access everything is selected and copied including passwords, private data, etc. Only keyboards and clipboard managers should get this permission.



## badsectoracula on Feb 5, 2022

  

> Naming and description could help a little: Full access to copy and paste (maybe somebody doesn't know what clipboard means), it allows this app to access everything is selected and copied including passwords, private data, etc. Only keyboards and clipboard managers should get this permission.

I don't think the problem of users not reading the text in warning messages can be solved with having them read even more text.

Actually this is an old problem, people do not read message boxes, popups, warnings, etc:

[http://www.zuschlogin.com/?p=53](http://www.zuschlogin.com/?p=53)



## dolmen on Feb 7, 2022

  

The permission should be just "allow to read clipboard when the app doesn't have focus". Only clipboard managers should need this to read clipboard data in the background. Any other read of the clipboard should have been triggered by a user action in the app that requires the read, and that's what the OS should enforce.



## summm on Feb 5, 2022

  

Why does it feel dodgy? When I want to give Adobe DRM or TikTok those permissions, I should be able to. They just shouldn't have the option to force me, or else stop running. Do you really condone the trendy dumbing down to an user-hostile walled garden? It's even Windows nowadays: They make it super hard to change file associations, change it back to Edge on every update, and even don't let other programs support the user. All under the pretext of "protect the user from bad applications", but really misuse it to drive usage of their own apps. The same will happen with that clipboard function.



## badsectoracula on Feb 5, 2022

  

It feels dodgy because they give a false sense of security, i wrote about it in another reply: [https://news.ycombinator.com/item?id=30220373](https://news.ycombinator.com/item?id=30220373)

> Do you really condone the trendy dumbing down to an user-hostile walled garden?

The complete opposite in fact, i dislike the increasing usability nightmare that modern desktop OSes creep in in the name of protecting users who are going to do their thing regardless unless the relevant functionality is completely removed - at which point someone should stop, take a few steps back and think why people use computers in the first place.



## rcthompson prev

  

On recent Android versions you get a pop-up notification whenever an app accesses the clipboard. (I think it doesn't notify for the active app, or maybe it doesn't notify for explicit user-initiated clipboard interactions, I'm not sure exactly what the rules are.) If I saw any app repeatedly accessing the clipboard for no discernible reason, I would definitely uninstall it.



## adrianmonk on Feb 5, 2022

  

More info (from [https://developer.android.com/about/versions/12/behavior-cha...](https://developer.android.com/about/versions/12/behavior-changes-all#clipboard-access-notifications)):

> _Clipboard access notifications_

> _On Android 12 and higher, when an app calls getPrimaryClip() to access clip data from a different app for the first time, a toast message notifies the user of this clipboard access._

> _The text inside the toast message contains the following format: APP pasted from your clipboard._



## jeroenhd on Feb 5, 2022

  

Android killed background clipboard access in an Android update entirely, except for system and vendor applications. I had to root my phone to get clipboard sync working again in KDE connect.

I haven't seen the notification myself, but I'm still on Android 11. From what I can tell for user-installed applications, only the active application and the keyboard get clipboard access, regardless of permissions and capabilities declared in the app manifest.

I get why they did this but I think it's a bad choice not to allow the user to opt into clipboard management. Going back to manually typing TOTP codes was more annoying than rooting my phone.



## Hallucinaut on Feb 5, 2022

  

I have a Pixel 6 and I've noticed an app from some Wi-fi lights with an app called TCP Smart copies my clipboard whenever I open it. I was disturbed by this and definitely didn't do anything special to permit it, so that seems somewhat at odds with what you're saying?



## jeroenhd on Feb 5, 2022

  

Perhaps I misinterpreted; you're right. Foreground apps are perfectly allowed to use the clipboard, it's just background app access that got axed. Apps don't even need any permissions to respond to clipboard events!

The toasts were added to Android 12 ([https://developer.android.com/about/versions/12/behavior-cha...](https://developer.android.com/about/versions/12/behavior-changes-all#clipboard-access-notifications)) but the permission required to call from the background isn't available to normal apps. The API should only show for active applications or input methods, and non-IME inputs that call the API (i.e. foreground apps) will show a notification if you have this setting enabled.

Basically, it should only happen with the TCP Smart app open, and show when the TCP Smart app is using the clipboard. It shouldn't just randomly happen inside other apps, unless the devs behind that app are somehow bypassing Android's security features.



## c0l0 prev

  

I think Linux dynamic user probes would lend themselves quite well for something like this. Fooling around with bpftrace for a few minutes yielded this one-liner, which catches all the conscious X11 selection accesses (and a few unconscious ones, too :)) I provoked on my desktop during a few minutes of testing:

```
    sudo bpftrace -e 'uprobe:/usr/lib/libX11.so:XGetSelectionOwner{time("%H:%M:%S ");printf("%-6d\n", pid);}'
```

I am not familiar with Xlib/X11 at all, so I am not sure which library functions one would be most interested in to explore this much further.

Edit: Replaced `XSetSelectionOwner` with `XGetSelectionOwner` in bpftrace invocation.



## CyberShadow on Feb 5, 2022

  

I am not familiar with bpftrace (though that does look extremely potent), but XSetSelectionOwner is called when an application wants to make data available via the clipboard, not access it. Also not sure if that would also catch stuff with Xlib, and I guess it won't if it's statically linked (this is why I pivoted hax11 from hooking library calls to MITM-ing the connection).



## c0l0 on Feb 5, 2022

  

My bad, I actually had the "production" trace command set to look for `XGetSelectionOwner`, but mis-pasted into my comment ;) Will that fare any better? It did make a few more PIDs/culprits end up in the trace output for me after all.

You are correct that "bringing your own function"/shared object will break that particular bpftrace script, but I would assume one could find a way to set up a probe in the X server, where all clients messing with clipboard state via some kind of IPC have to pass thru? (Again, I can only wildly speculate how X11 works under the hood - kinda shameful that I never bothered to look over all these years :-S )



## CyberShadow on Feb 5, 2022

  

I think XConvertSelection would be more effective. Applications which use the XFixes extension to receive notifications of new owners won't need to use XGetSelectionOwner.



## bagels prev

  

Why can apps even read it in the first place, as opposed to the os sending it only to apps the user initiates a paste in?



## kelnos on Feb 5, 2022

  

Because the OS doesn't know when the user initiates a paste. The user tells an _application_ that they want to paste, and then the _application_ tells the OS to give it the contents of the clipboard. (Sort of; some windowing systems do it slightly differently, but that's the gist of it.) That's indistinguishable from the application telling the OS it wants to paste when the user hasn't requested it.

Sure, there are ways that you could design the system, such as making it so the only way to paste is for the user to hit a key combination, or perhaps click on or touch a UI element that is known to be controlled/hosted/owned by the OS. Then the OS can push the clipboard contents to the foreground application, rather than the application trying to pull it. But then that becomes very limiting, UX-wise. (For example, you wouldn't be able to put a "paste" item in a context menu.)

You could also have a permissions model, where the OS prompts for permission every time an app wants to fetch the clipboard contents (presumably with an "I trust this app" option to get it to stop asking). But that might be considered too intrusive, or possibly just ineffective, as people will often just mash at whatever button is closest to get a modal dialog to go away.

At any rate, we have these copy/paste mechanisms from when people didn't really think about local security in the way we do today. Redesigning things would break a lot of existing applications. It might be worth it, or it might not.



## pishpash on Feb 5, 2022

  

Forget a permissions model. How about an informed notification model, where the OS marks the application with a badge notification whenever it requests a paste? And you can see a history of what it requested?

Generally I find it odd that there is so much hand-wringing over basic transparency. As with companies giving away private data to third parties, before talking about consent, how about a paper trail to start with?



## edave64 on Feb 5, 2022

  

That is exactly what is discussed in the article. A way to notify the user if a program request the contents of the clipboard. iOS apperently does just that.

The thing with the clipboard is that the feature is ancient. And back then, most applications were generally very unconcerned with security. So now the genie is out of the bottle, and it's difficult to convince some people that it's a problem.

The article's POC works for X only, which, as far as I understand, doesn't even prevent applications from reading each others keystrokes, much less clipboard. That just wasn't any kind of priority when it was created



## jonnycomputer on Feb 5, 2022

  

anything similar for Mac OS?



## tonyg on Feb 5, 2022

  

> you wouldn't be able to put a "paste" item in a context menu

I don't know about that. One approach would be for the menu invocation to accept a capability object representing the active context's interpretations of "copy", "paste", "save" etc. (A "user agent" if you will!) The menu would then invoke methods on that object. That way, interactions triggered by the mouse would propagate the appropriate capability. Other uses of the menu would have to synthesise an object of the correct type, which they could do, but they wouldn't be able to forge access to the user's clipboard.

In general, capability style thinking benefits from removing _ambient authority_ such as a global clipboard, replacing it with an object explicitly representing the necessary authority in each context.



## robertlagrant on Feb 5, 2022

  

If you do that then why can't the program invoke methods on that object whenever it wants to?



## tonyg on Feb 5, 2022

  

Good question. You only pass it with an interaction event. (And you revoke the capability once the interaction is over.)

Not perfect: stops background pasting without interaction tho. To do better, the only way I can think of right now is having paste only triggerable by a secure context (e.g. keyboard shortcut, as above). Any mechanism by which the user can declare "I intend to paste" in so many words without the app being able to forge it.

But that needn't rule out context menu paste, I don't think, if there's a way for the secure context to unforgeably decorate the menu when it declares it wants a paste action.

It's a fun thing to think about the design of. I encourage us all to try to think creatively about it rather than quickly jumping to "it can't be done".



## tonyg on Feb 8, 2022

  

FWIW, for those coming across this thread later: There has been some research on these topics (of course). I asked the `cap-talk` list to jog my memory, and received pointers to the following interesting starting points:

- "Design of the EROS Trusted Window System", Shapiro, Vanderburgh Northup and Chizmadia. USENIX Security 2004. [https://www.usenix.org/legacy/publications/library/proceedin...](https://www.usenix.org/legacy/publications/library/proceedings/sec04/tech/full_papers/shapiro/shapiro.pdf)

- "A Capability Based Client: The DarpaBrowser", Stiegler and Miller. Technical Report, June 2002. [http://www.combex.com/papers/darpa-report/html/](http://www.combex.com/papers/darpa-report/html/)

The Shapiro et al. paper discusses many of the issues brought up in this thread, along with the solution they chose for the actual working software system they had implemented.



## robertlagrant on Feb 6, 2022

  

Sure, if the OS offers an "add a cut/copy/paste menu option here" as part of their native UI library (if they offer one) then that could work, and/or saying "I delegate cursor focus, cmd-x, cmd-c and cmd-v to the OS to control for clipboard access".

I just didn't understand how the specific mechanism you were describing would help.



## [dan-robertson](https://news.ycombinator.com/user?id=dan-robertson) [on Feb 5, 2022](https://news.ycombinator.com/item?id=30219378) | 

  

The way it works for webapps is that you are only allowed to read or write the clipboard in the event handler for certain events, but it isn’t really watertight and that sort of mechanism isn’t so feasible outside the web where you don’t have so much control on how programs handle events.



## BelleOfTheBall on Feb 5, 2022

  

Wow, I had no idea things worked this way. It makes sense but is also terrifying to think about, that I might be copy-pasting some sensitive data and then accidentally tab into some app and it'd just ghost-paste the data into its cache.



## progval on Feb 5, 2022

  

As other pointed out, you don't even need to tab into some app. The security model on desktop OSs is that by default, every app has access to everything owned by the current user, even memory of other apps or hijacking commands by adding scripts in ~/.local/bin.

It is slowly being worked on (on Linux, this is done via control groups (containerization), AppArmor, SELinux, ...) but takes time to carefully tune so legitimate existing apps don't crash because they expect access to something. IIRC, macOS and Windows are also working on this.

There is also Qubes OS, taking a more radical approach of isolating everything by default, like Android or iOS. But it can be painful to use, as apps are not designed to support it.



## bryanrasmussen on Feb 5, 2022

  

capabilities based security [http://www.skyhunter.com/marcs/capabilityIntro/index.html](http://www.skyhunter.com/marcs/capabilityIntro/index.html)



## badsectoracula on Feb 5, 2022

  

> Suppose you were running a capability-secure operation system, or that your mail system was written in a capability-secure programming language. In either case, each time an executable program in your email executed, each time it needed a capability, you the user would be asked whether to grant that capability or not. So Melissa, upon starting up, would first find itself required to ask you, "Can I read your address book?" Since you received the message from a trusted friend, perhaps you would say yes - neither Melissa nor anything else can hurt you just by reading the file. But this would be an unusual request from an email message, and should reasonably set you on guard.

> Next, Melissa would have to ask you, "Can I have a direct connection to the Internet?" At this point only the most naive user would fail to realize that this email message, no matter how strong the claim that it came from a friend, is up to no good purpose. You would say "No!"

That sounds like a usability nightmare.

Also it is something that users ignore even at the coarse level of application permissions on mobile phones - imagine having Every.Single.Action on a computer do that. Instead of just people ignoring those, they'd turn outright hostile to any application doing that.



## bryanrasmussen on Feb 5, 2022

  

>That sounds like a usability nightmare.

sure but any email system should not have to request the capabilities to do any of these things, so basically if a message came in and it started requesting these capabilities - it would only be malicious messages that would do it.

This is following the principle of least access/authority [http://wiki.erights.org/wiki/POLA](http://wiki.erights.org/wiki/POLA)



## badsectoracula on Feb 5, 2022

  

An email client certainly needs the capabilities mentioned there. The ILOVEYOU virus (which is what the page refers to) was an executable (a VBS script but any executable could do the same thing) that asked Outlook Express for the address book and use it to send mail with itself as an attachment. The issue with it was that Outlook Express was too happy to run programs attached to mail (also that Windows hide file extensions by default and... people tend to respond to affection - at the end of the day you can't solve social issues with technical solutions) without even a warning (though a warning would be most likely ignored anyway due to the social aspect of the message - which is also what would happen with the warning in the dialog mentioned in the linked page too).

And at some point you'll have to request these capabilities and the user will have to grant them. So the user, as is seen with mobile permissions nowadays that serve more to make developers feel good about providing security and pat each other on the back about how thoughtful they are (meanwhile there is a ton of malware on Android phones), will simply grant the permissions because they want to run the application - the reason they went into obtaining it in the first place.

You can see the issue clearly in that infamous video from Linus Tech Tips where Linus tried to install Steam and end up breaking his desktop despite lots of warnings from the system - he had a goal (to run the program) and in the pursuit of that goal he ignored pretty much every warning the OS gave him, including even forcing him to type "yes i know what i am doing".

These solutions are pointless and only help to make the programmers who implement feel good that they implemented them. Like the solution to problem above where now you have to make a special file in the root folder to do that, as if someone who ignores everything in the pursuit of a goal to type "yes i know what i am going" aren't going to type "sudo touch /.magic_file" (that they copied from a forum post).

The only way to stop people from doing something it to make it technically impossible to do it. But at that point you are starting to make tasks impossible to achieve and people use computers to do things and anything that goes towards that is missing the forest (people using computers to do things) for the tree (some things can be unsafe so let's not anyone do it).



## bryanrasmussen on Feb 5, 2022

  

so, I can't help but note that when I said an email client should not need to request these capabilities and anything that did would be malware, you said an email client definitely needed to request these capabilities and gave as the example the ILOVEYOU virus.



## badsectoracula on Feb 5, 2022

  

Yes?

In a system based on permissions (which is basically what a capability system is) somehow applications need to gain those permissions - so it makes sense that an email client will need to request them. Otherwise how is it going to work if it cannot access the internet, the address book or any other functionality it might need?

BTW the ILOVEYOU virus example is what the linked page brought up, not me. They call it "Love Bug" but that is just another name for the same virus:

[https://en.wikipedia.org/wiki/ILOVEYOU](https://en.wikipedia.org/wiki/ILOVEYOU)



## bryanrasmussen on Feb 6, 2022

  

yeah I'm old enough to be familiar with these things - is there some reason that you seem to think I'm in my early twenties?

>In a system based on permissions (which is basically what a capability system is) somehow applications need to gain those permissions - so it makes sense that an email client will need to request them. Otherwise how is it going to work if it cannot access the internet, the address book or any other functionality it might need?

first of all, most capability based security assumes applications have the permissions natural for them to achieve their task, thus an email client would have access to the address book. It would have access to read previous emails received, it would have access to get latest emails and send emails and display the emails. It might have permission to play a beep tone when email arrives. It would have all the normal common permissions required to be an email client.

second of all, the access for the address book was requested by an attachment that was opened. Not the email client. To quote the first link: "In either case, each time an executable program in your email executed, each time it needed a capability, you the user would be asked whether to grant that capability or not. " To reiterate, it is not the email client that needs to ask every time it wants access to the address book, it is any executable opened from it.

third of all, you have still not explained why opening an attachment would need those particular permissions. The linked article uses the virus specifically because something opened by an email client should not need those permissions! I mean I guess, since you said 'yes?' to my earlier question as to whether your argument was that those capabilities should be allowed because it was the kind of thing a virus asked for that is really your argument!?! Most people who talk about email security talk about what an awful application outlook is/was because of its bundling of the browser in to render html opening up all sorts of security holes but you seem to think that an email client (here I drop back to talking about the client instead of an attached executable) should be able to post to random websites data from the users address book based on instructions received in an email? I've never actually seen an email client that needed to do that? Why do you think that it needs to do that? I'm going in circles here but - because a famous virus needed to do it? Really?

Fourth of all, your earlier example of someone messing up their system by installing Steam when they shouldn't is probably not the same as giving the Love Bug access it shouldn't have, but even if it were, yes people can do stupid things, your argument seems to be that any system that tries to help people stop before they do the stupid things will be doomed to failure instead of the common belief that some people will be stopped in their stupidity and caused to reconsider.

It seems frankly weird because the only end to your argument that makes any sense would be no security whatsoever, because people will try to do stupid things thinking that the stupid thing they are attempting to do will somehow give them what they want.



## badsectoracula on Feb 7, 2022

  

> yeah I'm old enough to be familiar with these things - is there some reason that you seem to think I'm in my early twenties?

No, i don't think that. Why you'd think that? If it was due to me writing about the ILOVEYOU virus, i wrote it because from your reply i got the impression that you thought the "Love Bug" virus (mentioned in the article) would be something different than the ILOVEYOU virus. I don't think age has anything to do with that though.

> first of all, most capability based security assumes applications have the permissions natural for them to achieve their task [...] It would have all the normal common permissions required to be an email client.

Yes, i agree with that and understand it, but...

> second of all, the access for the address book was requested by an attachment that was opened. [...] To reiterate, it is not the email client that needs to ask every time it wants access to the address book, it is any executable opened from it. [...] you have still not explained why opening an attachment would need those particular permissions.

...the issue wasn't that the ILOVEYOU virus was an attachment, it was just a VBS script that was executable like any other executable could be - it could have been an EXE file instead and it'd be able to do the same things as the VBS script.

To make it clear, an attachment would not need those particular permissions, but in the case of the ILOVEYOU virus it wasn't its status as an attachment that was the problem.

The main issue, as far as Outlook Express was concerned, was that Outlook Express didn't provide any means to help users avoid these issues (which BTW is not the same as protecting them from the issues - one case empowers the users, the other treats them as powerless). And of course Windows made the issue worse with hiding filename extensions.

As an example, nowadays Thunderbird provides pretty much the same features that Outlook Express had, including displaying of HTML content and even allowing access to the address book (instead of using some API you can use SQLite3 - but that is available in pretty much every language out there and the data is still just plain text). However you can switch off the HTML display or use a simplified one, have images not be downloaded by default to protect from remote attacks and tracking and it displays prominent warnings whenever it detects you are trying to launch an executable (this isn't ideal since, as i wrote in a previous message, many users will ignore the message, but it is still better than -say- Gmail disallowing executables completely).

> I mean I guess, since you said 'yes?'

And again to be clear, i wrote "yes" to that the _email client_ would need those permissions (since that is what you wrote). The email client needs to be able to access the internet to function.

But the attachment is not part of the email client, it is just a file that in the case of the ILOVEYOU virus, it happened to be an executable. In a permission or capability-based system _that_ executable would need its own set of permissions that are independent of the email client.

Which goes back to what i wrote initially: those permissions will need to somehow be acquired and the most logical way - and what, e.g., mobile phones do nowadays, since that is the most widely available form of permissions-based OSes - is to ask the user.

But as the link i gave shows, most users do not pay attention to these requests - especially if they happen frequently - and just accept whatever is presented to them because what they have in mind isn't the requests and permissions to give, but performing the task the application would be for. The Linus Tech Tips video was a recent example of this of a user blindly accepting through every request and conformation presented him in the pursuit of the goal to do the task he had in mind - he wasn't interested in whatever the system was asking, he was interested in installing Steam.

> It seems frankly weird because the only end to your argument that makes any sense would be no security whatsoever

No, that is a defeatist attitude, my argument was really that the _UX_ of the capability system that was proposed in the linked page about capability systems was a usability nightmare because it flies in the face of how users actually behave when faced with them. They do not solve the issues, at best they just make developers feel like they're doing something good, at worst they work as a way for developers to pass the blame from themselves to the users for ignoring the warnings. Either way the user is not "saved".

There must be a better way that doesn't also remove any functionality from the system, but i don't really know what that would be. I'm certain though that a system that bombards users with confirmation dialogs aren't it since we have a ton of evidence that these do not work.



## wruza on Feb 5, 2022

  

_accidentally tab into some app_

Apps do not even have to be foreground to fetch clipboard contents, afair. On windows, a clipboard is shared with the entire session (i.e. everything that you or the system runs for you after you log on).



## [dan-robertson](https://news.ycombinator.com/user?id=dan-robertson) [on Feb 5, 2022](https://news.ycombinator.com/item?id=30219339) | 

  

You don’t need to tab into the app (on a desktop os at least). It’s generally possible to read the clipboard from the background. The app mightn’t even need a visible window.



## spiorf on Feb 5, 2022

  

> accidentally tab into some app

I think in most systems you don't even need to do that. The app should be able to get the clipboard while in the background.



## dolmen on Feb 7, 2022

  

"Redesigning things would break a lot of existing applications".

Only a few applications handle triggering of a Paste action by themself. Most just leave the OS/GUI framework/ handle events and only react to a Paste request event. They would not be affected if the OS stops allowing to read the clipboard while a Paste event isn't being processed.



## Findecanor on Feb 5, 2022

  

> For example, you wouldn't be able to put a "paste" item in a context menu

Maybe the menu item could be a special window/widget type specific for the "paste" action, controlled by the UI server. An app could be restricted in how many of these it could have and what to display in it.



## Findecanor on Feb 6, 2022

  

Follow-up: I got inspired to (re)read a few technical reports about security in windowing systems, and found that EROS' window system EWS [1] uses a variation of this method:

A "Paste" widget is overlaid with a special transparent window which when clicked authorises access to the clipboard while forwarding the click. Abuse of the mechanism is supposed to be controlled by the transparent window setting the mouse pointer to a specific shape when hovering over it: a user would be alerted if he sees that shape where it isn't supposed to be.

[1] <[http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.133....](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.133.2919)>



## jchw on Feb 5, 2022

  

IMO: you have to look at how OSes and window systems were designed and evolved to get a great answer here. Those processes resulted in systems where the OS or compositor or window system itself can’t really know when the user is pasting something or not. It could be changed, but it would have lots of implications globally, from rewriting framework code, to changing JS APIs.

Edge cases would be handled worse, like non-standard paste gestures or VM integration.



## tonyg on Feb 5, 2022

  

Indeed. Our OS security models will keep being broken until people start taking object capability design seriously ¯\_(ツ)_/¯



## lcnmrn on Feb 5, 2022

  

Clipboard should be a private and secure enclave (encrypted) in the first place. Only the OS should store and retrieve data from it.



## edave64 on Feb 5, 2022

  

The applications still need to retrieve data for the clipboard to be of any use to anyone. E.g. Editor widgets like to have a paste button. Edit > Paste is a fairly common pattern, too.

Without having the OS in charge of managing the entire UI, it's difficutlt to make that work



## dolmen on Feb 7, 2022

  

OK, but pasting should be triggered by an user action.

Aside from Clipboard managers (which should be handled like on screen keyboard or other alternative input methods) I don't see a reason to allow pasting (and so reading from the clipboard) from an window that doesn't have the input focus.



## edave64 on Feb 17, 2022

  

That's a good step, but I think in most windowing systems, windows can just take and give focus as they please, so that alone probably wouldn't be too effective.

It's probably a possible problem to solve, but fixing stuff that wasn't build with security in mind from ground up is always tediuos.



## Gigachad on Feb 5, 2022

  

It’s pretty safe in web browsers. Any app can write to it but the only way to read is if the user hits the paste shortcut.



## edave64 on Feb 5, 2022

  

That is not quite true. JS can read the clipboard on any direct user interaction using "Document.execCommand". Also, there is a newer Clipboard API that requires explicit user permission, but as far as I understand once it is given it allows arbitrary reads.



## kalleboo on Feb 5, 2022

  

This depends on the browser. In a privacy-conscious browser such as Safari, it will pop up a browser-controlled "paste" context menu that the user has to explicitly interact with.



## marcosdumay on Feb 5, 2022

  

On my browser (Firefox 96), the paste command seems to just return false and do nothing.



## joveian on Feb 5, 2022

  

Mozilla's documentation claims paste with Document.execCommand is disabled for web content (as it should be):

[https://developer.mozilla.org/en-US/docs/Web/API/Document/ex...](https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand)

On the wider issue, I'd like to only allow a keyboard combination from the window manager / compositor to paste to the focused window. X11 has three different clipboards and I think Wayland has two (I've only seen xsel use the X11 secondary selection) while I've only ever heard of the one clipboard on Windows.

The kitty terminal has some method of using the clipboard that works remotely (though all the magic kitty does inspired me to "alias ssh='st ssh'"). I'm not sure if anything other than kitty uses it. The permissions have a specific "ask" option, which seems like something the browsers should support for the Clipboard API.

[https://sw.kovidgoyal.net/kitty/kittens/clipboard/](https://sw.kovidgoyal.net/kitty/kittens/clipboard/)



## edave64 on Feb 5, 2022

  

Are you running it as part of a click handler?



## marcosdumay on Feb 6, 2022

  

No, I was trying it on the console, without any context.



## edave64 on Feb 6, 2022

  

As I said, it needs to be as a direct reaction to user interaction. Though from the other comments, it seems my knowledge here is outdated anyways



## kukx on Feb 5, 2022

  

It can be useful of you want to use a non standard way to paste stuff, example a right mouse button in putty.



## tlarkworthy prev

  

I have taken to copying a benign string after using a sensitive password and I minimize switching tabs during transport of said password.



## npteljes on Feb 5, 2022

  

That's a good practice. KeePass 2, KeePassX and KeePassXC, and KeePassDX on Android do this by default as well. It's even better to use the Auto-Type feature of these software, because then the password never gets on the clipboard in the first place.



## Gare on Feb 5, 2022

  

Some password management programs (I know for KeePassXC) clear the clipboard automatically after several seconds.



## SubiculumCode on Feb 5, 2022

  

This is good to know, but it still doesn't help if there is an application phone app, etc, that monitors the clipboard.



## mackatap on Feb 5, 2022

  

Bitwarden does as well, though it seems more like 20-30 seconds.



## gnabgib on Feb 5, 2022

  

It doesn't by default (perhaps it depends on which version you first installed) but it is available under File -> Settings -> Options: Clear Clipboard which can e set to never/10s/20s/30s/1m/2m/5m.



## photon12 prev

  

The Qubes clipboard model is one of the reasons I use that OS: [https://www.qubes-os.org/doc/how-to-copy-and-paste-text/](https://www.qubes-os.org/doc/how-to-copy-and-paste-text/)



## alexvoda prev

  

To what degree does Wayland solve this compared to X?



## jchw on Feb 5, 2022

  

I believe in Wayland, only foreground applications can access the clipboard. I can’t recall how poorly XWayland fares; I suspect it just bypasses the security boundary. Nonetheless, still useful, especially e.g. for sandboxed apps.

I don’t think clients can, without extensions, watch for who’s snooping the clipboard. However… it probably wouldn’t be difficult to integrate this into the compositor. You could even imagine a Wayland protocol for watching clipboard accesses, or god forbid, a dbus service for doing so.



## Arnavion on Feb 5, 2022

  

>I believe in Wayland, only foreground applications can access the clipboard.

To be precise, Wayland does not care who or what accesses the clipboard. Whether or not only foreground applications can access the clipboard is determined by which clients the particular compositor sends wl_data_offer events to.

So the answer to alexvoda's question is: Wayland doesn't solve the problem completely. wl_data_offer events are pushed from the compositor to the client rather than the client asking for a data offer. The compositor cannot know whether the client may want the data offer, so it has to send the event to every client regardless of whether the client ends up needing it or not. For most (if not all) compositors today, "every client" means "clients that are currently focused", so while a background application can't snoop your clipboard, simply focusing such an application would allow it to snoop.

Furthermore, extensions like wlr-data-control are specifically designed for background snooping, since they're intended to be used by clipboard managers. It's again up to the compositor to apply security to this, like only allowing a single specific process to be the data control manager and not arbitrary programs. For wlr-data-control I'm not aware of any compositor that applies such a policy; it would, at the very least, break `wl-paste`.



## emersion on Feb 5, 2022

  

XWayland as a whole is itself a Wayland client. So as long as any X11 window has focus then any other X11 window can steal the clipboard. But once a Wayland window is focused, X11 windows don't have access anymore.



## DavideNL prev

  

Related tool for macOS: [https://langui.net/clear-clipboard/](https://langui.net/clear-clipboard/)

_Clear Clipboard allows you to clear the clipboard content manually or automatically, preventing your clipboard data from being peeped while you are absent._

_You can chose to clear the clipboard content every few seconds and enable clearing clipboard on computer sleep / display sleep / screen lock to maximize protection of your sensitive clipboard data._



## nigwil_ on Feb 6, 2022

  

Could use Automator with AppleScript:

```
  x@Mac-mini-M1 ~ % cat ClearClipboard.scpt 
  on run
   tell application "System Events" to set the clipboard to ""
  end run
  x@Mac-mini-M1 ~ %
```



## tiku prev

  

I have removed the LinkedIn and Twitter app because they force directed the website to the app, but good to know that it could also see my clipboard and even track it perhaps..



## anderspitman prev

  

I've been thinking it would be fun to make a self-hosted web clipboard that lets you paste text/files into a simple web page, and have them available on all your devices. Sort of like cl1p.net but e2ee and it would maintain history.

You wouldn't need an account because your password could be hashed to yield a unique URL.

Only reason I haven't hacked it together already is because I don't trust myself to get the crypto right yet.

Anyone aware of something like this already existing?



## Damogran6 on Feb 5, 2022

  

I had thought about something similar, only with publishable subscriuptions so N people could share the same clipboard. Only I wanted the opposite: It would be ephemeral, only as long as you subscribed to the ‘feed’ would you see what was there, there would be no history, and pragmatically, no long term retention in the cloud…mostly as a cost savings measure.

I got so far as thinking you’d have two guids: One to subscribe to, and the other to revoke the feed.

It never got past the thought stage…and I made a logo and registered a URL that I’ve since let expire as the coding took longer than my attention span.



## tomerv on Feb 5, 2022

  

You're just a few steps away from reinventing Twitter!



## ZeroGravitas prev

  

This is part of the reason I don't set Vim to automatically fill the system clipboard and only do so on demand.

I think a few other geeky apps let you cut and paste internally as a separate thing from the global clipboard, which seems wise as a mitigation.



## abaines prev

  

With most installations of X11 / Xlib, there is also an extension library: libXFixes. With this, it is possible to receive an event whenever the clipboard (or any other selection) changes (even if our program is not the owner). [0]

I used this in the past when porting a Windows text editor to Linux that needed to know when the clipboard had changed.

In the C bindings, it corresponds to XFixesSetSelectionOwnerNotifyMask / XFixesSelectionNotifyEvent. There is also a SelectionNotify event in the core X11 protocol, but I'm pretty sure it is different (occurs in response to XConvertSelection - asking for the clipboard contents). Though it has been some time since I wrote the code I'm looking at.

[0]: [https://www.x.org/releases/current/doc/fixesproto/fixesproto...](https://www.x.org/releases/current/doc/fixesproto/fixesproto.txt) (6. Selection Tracking)



## CyberShadow on Feb 5, 2022

  

I think the goal here is to be notified when an application accesses the clipboard, not modifies it. You need to be the owner to do that (excluding some system-wide hack like patching the X server or MITM-ing all X11 connections).



## Jenda_ prev

  

At least, I have a script that runs every minute, and if the clipboard contents has not changed, it flushes it. This really helps in preventing accidental paste of sensitive stuff that remained in the clipboard.

I also have script that strips newlines (so I can relatively safely paste into terminal without executing anything) and formatting (so I can paste into WYSIWYG editors). All of this is done by simple xclip -out | ... | xclip -in.

There was a bug in stardict: [https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=806960](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=806960)

- if you selected something (i.e., copied to clipboard), it tried to look it up in a dictionary and display a translation (by default, just after startup)

- online dictionaries, including one available only over HTTP, were enabled by default

- so if you used a password manager that copies passwords into clipboard, they were immediately sent in an unencrypted request for the whole world to see

We desperately need an application firewall (something like OpenSnitch, but that's so difficult to configure correctly) and basic app isolation (like QubesOS, but that's heavy and pain to use) for Linux. It's a shame that most other OSes, despite having other problems, implement this correctly.



## rank0 prev

  

Is it possible to deny clipboard access to every application and only pass the info through stdin through the explicit ctrl-v or pbpaste/xclip/whatever?



## fsflover on Feb 5, 2022

  

Yes, on Qubes OS, [https://qubes-os.org](https://qubes-os.org/). It's based on VMs for secure and transparent compartmentalization.



## rank0 on Feb 5, 2022

  

I am familiar with Qubes, but I'm asking if this specific security control could be used elsewhere.



## phkahler prev

  

It would make sense to sniff clipbaod paste commands and offer the active app a one time read access for each user provided interaction.

Not sure about the value of requiring the same for writes, but it wouldn't hurt.

Any time an unauthorized read is attempted the user could be informed.

Not sure where this would get plumbed in. Needs support at the toolkit level (menu->edit->paste) and some higher level outside apps.

The idea is to obey the users implicit concent and flag everything else.



## root5 prev

  

As iOS and macOS share their clipboard over BLE, one can abuse this feature for various things like smart home automation.

[https://twitter.com/m33x/status/1489236070151737351](https://twitter.com/m33x/status/1489236070151737351)



## pierrefermat1 on Feb 5, 2022

  

If I read the tweets correctly this is only capable of capturing the event happening and not the content? I don't see this too much of a privacy issue



## throwntoday prev

  

iOS/macOS really need to add clipboard to privacy settings. I opened monopoly on iOS and the first thing it did was paste my clipboard to god knows where.



## jonnypotty prev

  

I used to support a terminal services/citrix environment and we had repeated instances of people trying to copy and paste data and getting other peoples data. Never got anywhere investigating and boss refused to go to Microsoft. I'd forgotten about it till I read this.



## beermonster prev

  

Is there a Little Snitch/open snitch/Lulu type tool for Clipboard access?



## dethos on Feb 5, 2022

  

As far as I'm aware, there isn't. And it would be a nice tool to have available, I suspect this could be integrated into existing clipboard managers.



## juglans prev

  

Bit Warden password manager app for Linux desktop has an option to clear the clipboard:

file -> settings [options]'clear clipboard': dropdown select range from 10 seconds to 5 mins or never.

Set & forget.



## opan on Feb 5, 2022

  

KeePassXC does this as well. Even if just copying the username.



## AshamedCaptain prev

  

> it seems that due to the nature of X11 it is not possible to know which running process owns the window that is accessing the clipboard. A shame.

You definitely can. Search for _NET_WM_PID. Now this being X11, this is again just a convention, and a client could put any information it wants in that property.

> While very useful, this raises a lot of security questions.

Really, it doesn't.

If these programs want to share information, they already can. If these programs want to spy on each other's memory contents, they already can. Unless you are running these programs as different users and with different home directories, which is not usual right now in desktop GNU/Linux (but is on Android).



## dethos on Feb 5, 2022

  

> You definitely can. Search for _NET_WM_PID. Now this being X11, this is again just a convention, and a client could put any information it wants in that property.

That was the reason I mentioned that "it is not possible to know", perhaps "for sure" is missing at the end of the sentence.

> Really, it doesn't.

As a user (at least in my opinion) it does.

> If these programs want to share information, they already can. If these programs want to spy on each other's memory contents, they already can. Unless you are running these programs as different users and with different home directories, which is not usual right now in desktop GNU/Linux (but is on Android).

The point is, when the user copies a piece of information he has a clear target for that information (clipboard is the tool he uses do copy from one window to another), why would all other apps have access to it?

It is the user that wants to share information not the apps.

Even taking account all those capabilities, many pieces of malware rely on detecting and replacing information on the clipboard, that is a clear indication that some sort of isolation is missing here, or at least, and indication regarding that the information was changed/accessed.



## AshamedCaptain on Feb 5, 2022

  

> The point is, when the user copies a piece of information he has a clear target for that information (clipboard is the tool he uses do copy from one window to another), why would all other apps have access to it?

Everything that is run by the user runs in the same context. There is little point in focusing on the clipboard, because any two programs running within the same user can do literally anything to each other. Including spying on each other keystrokes, modifying each other's text widgets contents, and faking whatever text/images they are displaying. The fact that they may alter the clipboard contents is irrelevant when they can literally monitor and inject keystrokes on each other.

This is the security model that we have right now at the desktop. Good luck trying to find a better one that just does not completely break UNIX and turns it into something like Android. One of the benefit of this model is that you (the user) have practically unlimited freedom in hooking the system in ways that nobody expects (e.g. a clipboard access detector), but then so does anybody else you decide to trust (by running their program in the same context).



## CyberShadow on Feb 5, 2022

  

This is why distributions are moving towards Wayland and container-izing most applications; this allows each application to be in its own sandbox with no capability of interacting with other applications without the user/system's explicit consent.



## destructionator on Feb 5, 2022

  

It is quite easy to do that with X as well. It doesn't transfer data at all until the clipboard data is requested, and at that point, the clipboard owner knows which window is requesting it and is free to ask the user or deny the request. No applications I'm aware of do this in practice though, beyond the normal content negotiation.



## CyberShadow on Feb 5, 2022

  

The protocol actually allows for applications to request the clipboard data to be delivered to someone else's window. I haven't tried it, but I think it would work.



## CyberShadow on Feb 5, 2022

  

> If these programs want to spy on each other's memory contents, they already can.

No, they cannot. As an unprivileged process, you cannot access other processes' memory, with major distributions' default settings, even if the two processes share the same UID. The only way to achieve that would be to modify some execution path (e.g. .bashrc) to launch an evil wrapper which runs the target process in a way that allows reading its memory.



## AshamedCaptain on Feb 5, 2022

  

Which distributions and which settings are you talking about?

If you can run gdb on that distribution, then you can definitely do it, even if it's just because they whitelist gdb in selinux/aa (in which case you can just script gdb). And I rather doubt it is a default, since ptrace-based sandboxing is a thing (even Firefox was using it).

And as for the second method, that's why I said same home directory. But there is a gazillion of ways. You can even meddle in practically all IPC done by any two processes of the same UID, e.g. attach to sysv or posix shm.



## CyberShadow on Feb 5, 2022

  

> Which distributions and which settings are you talking about?

kernel.yama.ptrace_scope = 1

> If you can run gdb on that distribution, then you can definitely do it, even if it's just because they whitelist gdb in selinux/aa (in which case you can just script gdb).

No, you can't. Try it.

> And I rather doubt it is a default, since ptrace-based sandboxing is a thing (even Firefox was using it).

You can trace child processes, but not any other process.



## AshamedCaptain on Feb 5, 2022

  

Which distribution enables this by default ?

EDIT: Apparently Ubuntu, but not Debian, SuSE, Arch, etc. Well, TIL.

> No, you can't. Try it.

You definitely can, it's just that that as you said, gdb just can't attach to anything but a child process; making gdb only work with a process it spawned itself.

Anyway, in addition to changing .profile, you still can do practically everything including modifying SHM segments, writing to pipes, sockets, etc. You could start closing feature by feature (e.g. remove /proc like Android), but for some reason it doesn't seem like the right approach.



## CyberShadow on Feb 5, 2022

  

> but for some reason it doesn't seem like the right approach.

The approach being used in containerization is namespaces. You can put new processes into a new IPC / user / PID / network / time / etc. namespace, which isolates them from the parent namespace. Once that's done and you can't mess with other processes via the filesystem / kernel, the remaining hole is servers with inadequate security models, such as X11.



## AshamedCaptain on Feb 5, 2022

  

What I mean is that if you are going to put your processes as a different user anyway (or a different user namespace), trying to break all the features that allow a user to manage same UID processes is unnecessary.



## backtoyoujim prev

  

Then what watches the clipboard watcher ?



## dolmen on Feb 7, 2022

  

The user.



## adrium prev

  

Does it really come as a surprise that apps are exploiting the clipboard?

TLDR: Drag and drop sensitive stuff. If necessary, copy only part of sensitive information, enter the rest manually.

The amount of usability obstacles in the name of security is getting ridiculous. For example, when I got a new company mac, I had to enter my keychain access password and grant access to folders countless times - and I still have to do that after one week of using it occasionally.

Similarly annoying are the cookie questions. In the beginning, I found it interesting and tried to reduce the number of cookies as much as possible - now I just click what ever button brings me to the site fastest (or don't click a button at all, if it is still possible to scroll and see about half of the page on mobile).

Don't you also feel some kind of fatigue?

Now please do not cripple the clipboard the same way. By the way: Qubes OS takes an interesting approach here. Similar to suggestions by some of the commenters. [1]

Don't get me wrong: I am extremely sensible to security - but also to usability. I want to use my daily-driver system conveniently and it should "just work" and I want to trust it fully [2]. I do not just install any trash app, simply because there is one available.

On the desktop:

1. Only applications from the official repos get installed. I may install other open source software occasionally. So yes, the trust lies with the distro.

2. Other software (potentially untrusted or good to be isolated) like banking or tax software or zoom get a proot environment or a new user profile.

3. Most software can be run in the browser anyway and it is actually quite a nice sandboxing tech. Web apps can not simply read the clipboard. [3]

4. Clear cookies on browser exit. I have a whitelist for about two or three sites to keep the state for convenience.

5. Browser extension that manages and fills passwords on request(!). No need to copy-paste around. [4]

6. If I need passwords elsewhere, I use drag and drop. I believe this is extremely convenient and very secure. That works 90% of the time, otherwise, I copy only part of the sensitive information and enter the rest manually.

On mobile:

1. Same goes for app installation: open-source only from F-Droid.

2. Other apps get put in a work profile and disabled when not used. [5] No trash like games and social media.

3. Do not copy paste sensitive information, but use IME apps (keyboard apps that actually "type" passwords). Personally, I like KeePassDX and sometimes use KDE Connect. [6]

Accounts:

1. I own several domains and mobile numbers, all companies that want my info get different data.

2. Fill in bogus information if possible.

I may have gone overboard with this :-D But I don't even care about MFA that much at this point... And it is an interesting experiment to see what company leaks data.

[1] [https://www.qubes-os.org/doc/how-to-copy-and-paste-text/](https://www.qubes-os.org/doc/how-to-copy-and-paste-text/) [2] Kubuntu fanboy here and Android still on version 8 though because of ROM customization [3] [https://developer.mozilla.org/en-US/docs/Web/API/Clipboard/r...](https://developer.mozilla.org/en-US/docs/Web/API/Clipboard/read) [4] [https://github.com/adrium/easypass](https://github.com/adrium/easypass) [5] [https://f-droid.org/en/packages/net.typeblog.shelter/](https://f-droid.org/en/packages/net.typeblog.shelter/) [6] [https://www.keepassdx.com/](https://www.keepassdx.com/) and [https://f-droid.org/en/packages/org.kde.kdeconnect_tp/](https://f-droid.org/en/packages/org.kde.kdeconnect_tp/) And I wrote a KeePass plugin to convert the passwords: [https://github.com/adrium/KeepassPfpConverter](https://github.com/adrium/KeepassPfpConverter)



## ummonk on Feb 5, 2022

  

"For example, when I got a new company mac, I had to enter my keychain access password and grant access to folders countless times - and I still have to do that after one week of using it occasionally."

Did you not setup Touch ID?



## adrium on Feb 5, 2022

  

Hmm... Yes, but I am only aware to use it for unlocking the computer...



## ummonk on Feb 6, 2022

  

Try using it the next time a password prompt comes up.



## octavqq prev

  

hehehe

  
  



  
